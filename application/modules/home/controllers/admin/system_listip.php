<?php

class System_listip extends Admin_Controller {

    public $user;
    public $classname = "system_listip";
    public $limit = 15;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $this->template->add_title('IP List | Config System');
        $object = $this->db->query("select * from ttp_ip Where type = 1 ORDER BY id desc")->result();
        $object_enabled = $this->db->query("select * from ttp_ip Where type=0")->result();
        $this->load->model('global_model','global');
        $myip = $this->global->getYourIP();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/system_listip/',
            'data' => $object,
            'data_enabled' => $object_enabled,
            'IsAdmin' => $this->user->IsAdmin,
            'myip' => $myip
        );
        $this->template->write_view('content', 'admin/system_listip', $data);
        $this->template->render();
    }

}

?>