<!--Add KPI | Update KPI-->
<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
    .body_content .containner .black .box_inner .block1_inner h1{
        font-size:13px;
    }
</style>
<?php
    $removeTr = "";
    $addOne = "";
    $disabled = "";
    $disabled_input = "";
    $disAdd = "";
    $addDVT = "";
    $readonly = "";
    if($market_status_active == 0){
        $addDVT = "addDVT";
    }else if($market_status_active == 1){
        
    }else if($market_status_active == 2){
        $disabled = "disabled";
        $readonly = "readonly";
    }else if($market_status_active == 3){
        
    }else if($market_status_active == 4){
        
    }else if($market_status_active == 5){
        $disAdd = "disabled";
        $disabled = "disabled";
        $readonly = "readonly";
        $disabled_input = "disabled";
    }else if($market_status_active == 6){
        
    }else if($market_status_active == 7){
        
    }
    
    if(isset($market_id)){

    }else{
        //update KPI
        $exitsData = $this->db->query("SELECT count(*) as count FROM ttp_marketing_campaign_details WHERE parent = $data->id")->row()->count;
        if($exitsData == "0"){
            $removeTr = "<a class='btn text-danger' onclick='removeTr(this)'><i class='fa fa-minus-circle'></i></a> ";
            $addOne = " onclick='addOne(this);'";
        }else{
            if($market_status_active != 0){
                $disabled_input = "disabled";
            }
            $disAdd = "disabled";
            //$disabled = "disabled";
        }
    }
    
?>
<div class="alert alert-success alert-dismissable showMessenger hidden">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <p class="Messenger"></p>
</div>
<form name="frmEditAdd" id="frmEditAdd" method="post">
<div class="table_data">
    <table id="table_data">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Tên KPI</th>
            <th class="text-center" colspan="3"><input type="text" autocomplete="off" id="kpi_name" name="kpi_name" value="<?=(isset($data->name) ? $data->name : '')?>" placeholder="Nhập tên KPI" class="form-control" <?=$readonly;?>/></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Mô tả cho KPI</th>
            <th class="text-center" colspan="3"><textarea id="kpi_desc" name="kpi_desc" placeholder="Nhập mô tả cho KPI..." class="form-control" <?=$readonly;?>><?=(isset($data->description) ? $data->description : '')?></textarea></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Thời gian bắt đầu</th>
            <th class="text-center" colspan="2">
                <div class="form-group">
                    <input type='text' name="kpi_start" id="kpi_start" placeholder="2016-01-01 00:00:00" value="<?=(isset($data->start) ? $data->start : '')?>" class="form-control" <?=$readonly;?>/>
                </div>
            </th>
            <th class="text-center col-xs-3"></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Thời gian kết thúc</th>
            <th class="text-center" colspan="2">
                <div class="form-group">
                        <input type='text' name="kpi_end" id='kpi_end' placeholder="2016-01-01 00:00:00"  value="<?=(isset($data->end) ? $data->end : '')?>" class="form-control" <?=$readonly;?>/>
                </div>
            </th>
            <th class="text-center"></th>
        </tr>
        <tr>
            <th class="text-center"><a class="btn" <?=$disAdd;?> onclick="addRow()"><i class="fa fa-plus-circle"></i> Thêm mục tiêu</a></th>
            <th class="text-center"><a class="btn <?=$addDVT;?> hidden" onclick="addDVT();" title="Thêm ĐVT" <?=$disabled;?>><i class="fa fa-plus"></i> Thêm ĐVT</a></th>
            <th class="text-center"><input type="text" class="form-control dvtname hidden" placeholder="Tên ĐVT"/></th>
            <th class="text-center"><a class="btn btn-danger btn-sm dvtbutton hidden"><i class="fa fa-save"></i> Tạo ĐVT</a></th>
        </tr>
        <?php
            $val = isset($data->value) ? json_decode($data->value) : array();
            $action_title = isset($data->action_title) ? json_decode($data->action_title) : array();
            $last = count($val);
            foreach($val as $key=>$item){
        ?>
        <tr <?=($key == $last-1) ? 'class="addRow"' : ''?>>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">
                <?php 
                if($key > 0) {?>
                <?=$removeTr;?>
                <?php } ?>
            </th>
            <th class="text-center">
                <input type="text" name="action_title[]" value="<?=(isset($action_title[$key]) ? $action_title[$key] : '')?>" class="form-control" placeholder="Tên mục tiêu" <?=$readonly;?>/>
            </th>
            <th class="text-center">
                <input type="text" id="kpi_focus" onkeyup="keyUp(this);" name="kpi_focus[]" value="<?=(isset($item) ? $item : '')?>" placeholder="Nhập mục tiêu" class="form-control" <?=$disabled_input;?> autocomplete="off"/>
            </th>
            <th class="text-center">
                <select class="form-control" name="symbol[]" <?=$disabled_input;?>>
                    <?php
                        $symbol = json_decode($data->symbol);
                        foreach($result_symbol as $sym){
                    ?>
                    <option value="<?=$sym->code?>" <?=($symbol[$key] == $sym->code) ? "selected":""?>><?=$sym->name?></option>
                    <?php } ?>
                </select>
            </th>
        </tr>
        <?php } ?>
        <?php
            if($edit != true){
                /*Thêm mới KPI*/
        ?>
        <tr class="addRow">
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3"></th>
            <th class="text-center">
                <input type="text" name="action_title[]" value="" class="form-control" placeholder="Tên mục tiêu"/>
            </th>
            <th class="text-center">
                <input type="text" id="kpi_focus" onkeyup="keyUp(this);" name="kpi_focus[]" value="<?=(isset($item) ? $item : '')?>" placeholder="0" class="form-control" autocomplete="off"/>
            </th>
            <th class="text-center">
                <select class="form-control" name="symbol[]">
                        <?php
                            $symbol = json_decode($data->symbol);
                            foreach($result_symbol as $sym){
                        ?>
                        <option value="<?=$sym->code?>"><?=$sym->name?></option>
                        <?php } ?>
                </select>
            </th>
        </tr>
        <?php
            }
        ?>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Ngân sách</th>
            <th class="text-center">
                <input type="text" id="kpi_money_need" name="kpi_money_need" value="<?=(isset($data->money_need) ? $data->money_need : '')?>" placeholder="Nhập ngân sách" class="form-control" <?=$disabled_input;?> autocomplete="off"/>
            </th>
            <th class="text-left">VNĐ</th>
            <?php
                if($edit == true){
            ?>
            <th class="text-center"><a class="btn btn-warning updateKPI" <?=$disabled;?>><i class="fa fa-save"></i> Cập nhật</a></th>
            <?php }else{ ?>
            <th class="text-center"><a class="btn btn-primary updateKPI"><i class="fa fa-save"></i> Thêm mới</a></th>
            <?php }?>
        </tr>
    </table>
</div>
    
    <input type="hidden" name="money_need" id="money_need" value=""/>
    <input type="hidden" name="id" id="id" value="<?=isset($data->id) ? $data->id : '';?>"/>
    <input type="hidden" name="market_id" id="market_id" value="<?=isset($market_id) ? $market_id : '';?>"/>
</form>
<script type="text/javascript">
    
var format = function(num){
   var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
   if(str.indexOf(".") > 0) {
       parts = str.split(".");
       str = parts[0];
   }
   str = str.split("").reverse();
   for(var j = 0, len = str.length; j < len; j++) {
       if(str[j] != ",") {
           output.push(str[j]);
           if(i%3 == 0 && j < (len - 1)) {
               output.push(",");
           }
           i++;
       }
   }
   formatted = output.reverse().join("");
   return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
};

function removeTr(ob){
    $(ob).closest('tr').remove();
}
function addOne(ac){
    $parentTR = $(ac).closest('tr');
    $parentTR.clone().insertAfter($parentTR);
    //$(ac).remove();
}
function addDVT(){
    $('.addDVT').addClass("disabled");
    $('.dvtname').removeClass("hidden");
    $('.dvtbutton').removeClass("hidden");
}

$('.dvtbutton').click(function(){
    $(this).addClass("disabled");
    var name = $('.dvtname').val();
    $.ajax({
        url: baselink+"marketing_campaign/addDVT",
        dataType: "html",
        type: "POST",
        data: "dvt="+name,
        success: function(result){
            if(result == "OK"){
                alert('Bạn đã tạo mới ĐVT thành công!');
                $('.addDVT').removeClass("disabled");
                $('.dvtname').addClass("hidden");
                $('.dvtbutton').addClass("hidden");
            }
            else{
                console.log(result);
            }
        }
    });
});

function addRow(){
    $.ajax({
            url: baselink+"marketing_campaign/loadTr",
            dataType: "html",
            type: "POST",
            data: "",
            success: function(result){
                $(result).insertAfter($('.addRow').closest('tr'));
        }
    });
}

function keyUp(ob){
    var val = $(ob).val();
    val = val.replace(/[^0-9]/g,'');
    $(ob).val(format(val));
}

$(document).ready(function () {
    setTimeout(function(){
        $('#kpi_start,#kpi_end').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1",
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        setTimeout(function(e){
            //keyUp(e);
            $("#kpi_money_need").keyup();
        });
    },100);
});


//$("#kpi_focus").keyup(function(e){
//    var val = $(this).val();
//        val = val.replace(/[^0-9]/g,'');
//    $(this).val(format(val));
//    $('#focus').val(val);
//});

$("#kpi_money_need").keyup(function(e){
    var val = $(this).val();
        val = val.replace(/[^0-9]/g,'');
    $(this).val(format(val));
    $('#money_need').val(val);
});

$('.updateKPI').click(function(){
    var id = "<?=isset($data->id);?>";
    var name = $('#kpi_name').val();
    var focus = $('#kpi_focus').val();
    var sUrl = "";
    if(id == ""){
        /*Insert KPI*/
        sUrl = baselink+"marketing_campaign/createKPI";
    }else{
        /*Update KPI*/
        sUrl = baselink+"marketing_campaign/updateKPI/?id="+id;
    }
    $(this).addClass("disabled",true);
    $(this).html("<i class='fa fa-refresh fa-spin'></i> Đang cập nhật");
    if(name == ""){
        alert('Vui lòng nhập tên');
        $(this).removeClass("disabled");
        $('.updateKPI').html("<i class='fa fa-save'></i> Thêm mới</a>");
        $('#kpi_name').focus();
    }else if(focus == "")
    {
        alert('Vui lòng nhập mục tiêu');
        $(this).removeClass("disabled");
        $('.updateKPI').html("<i class='fa fa-save'></i> Thêm mới</a>");
        $('#kpi_focus').focus();
    }else{
        setTimeout(function(){
        $.ajax({
            url: sUrl,
            dataType: "html",
            type: "POST",
            data: $('#frmEditAdd').serialize(),
            success: function(result){
                if(result == "OK"){
                    if(id == ""){
                        alert('Thêm mới thành công.');
                    }else{
                        alert('Cập nhật thành công.');
                    }
                    location.reload();
                }else{
                    alert('Lỗi hệ thống !!!');
                }
//                if(id == ""){
//                    $('.updateKPI').html("<i class='fa fa-save'></i> Thêm mới</a>");
//                    $('#kpi_name').val("");
//                    $('.Messenger').text("Thêm mới thành công !");
//                }else{
//                    $('.updateKPI').html("<i class='fa fa-save'></i> Cập nhật</a>");
//                    $('.Messenger').text("Cập nhật thành công !");
//                }
//                if(result == "OK"){
//                    $('.showMessenger').removeClass("hidden");
//                    $('.updateKPI').removeClass("disabled");
//                }else{
////                    console.log(result);
//                    alert('Lỗi hệ thống !!!');
//                }
//                setTimeout(function(){
//                    $('.showMessenger').addClass("hidden");
//                },2000);
            }
        });
        },1000);
        $("#close_overlay").click(function(){
            location.reload();
        });
    }
});
</script>