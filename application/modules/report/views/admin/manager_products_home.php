<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<form action="<?php echo $base_link ?>setsessionsearch" method="post">
					<span>Lọc dữ liệu</span>
					<select name="CategoriesID" class="form-control">
						<option value="">-- Nhóm ngành hàng --</option>
						<?php 
							$CategoriesID = $this->session->userdata("report_filter_products_CategoriesID");
							$arr_group = array();
							$arr = array();
							$categories = $this->db->query("select * from ttp_report_categories")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									$arr[$row->ID]['Title'] = $row->Title;
									$arr[$row->ID]['Path'] = $row->Path;
									$selected=$row->ID==$CategoriesID ? "selected='selected'" : '' ;
                                    if($row->ParentID==0){
                                    	$arr_group[$row->ID] = "<optgroup label='$row->Title'>";
                                    }else{
                                    	$path = explode('/',$row->Path);
                                    	$path = isset($path[0]) ? $path[0] : 0 ;
                                    	if($path>0)
                                    	$arr_group[$path] .= "<option value='$row->ID' $selected>$row->Title</option>";
                                    }
								}
								echo implode('</optgroup>',$arr_group).'</optgroup>';
							}
						?>
					</select>
					<input type="submit" class="btn btn-default" value="Filter" />
                    <a class="btn btn-default" href="<?php echo $base_link.'clearfilter' ?>">Clear Filter</a>
				</form>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary">Add new</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Mã SP</th>
					<th>Tên sản phẩm</th>
					<th>Ngành hàng</th>
					<th>Nhóm ngành hàng</th>
					<th>Action</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Title</td>";
						if(isset($arr[$row->CategoriesID]['Path']) && isset($arr[$row->CategoriesID]['Title'])){
							$nhomnganh = explode('/',$arr[$row->CategoriesID]['Path']);
							$nhomnganh = isset($nhomnganh[0]) ? $nhomnganh[0] : 0 ;
							echo isset($arr[$row->CategoriesID]['Title']) ? "<td>".$arr[$row->CategoriesID]['Title']."</td>" : "<td>--</td>";
							echo isset($arr[$nhomnganh]['Title']) ? "<td>".$arr[$nhomnganh]['Title']."</td>" : "<td>--</td>";
						}else{
							echo "<td>--</td>";
							echo "<td>--</td>";
						}
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>