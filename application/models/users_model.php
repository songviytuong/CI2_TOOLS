<?php

class Users_model extends CI_Model {

    var $tablename = 'ttp_user';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function getUsers($params = array()) {
        $this->db->select('ID,FirstName,LastName,Thumb,RoleID,GroupID,Color,UserName');
        if (!empty($params['userid'])) {
            $this->db->where('ID', $params['userid']);
        }
        if (!empty($params['ids'])) {
            $this->db->where_in('ID', $params['ids']);
        }
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineUsers($params = array()) {
        $users = $this->getUsers($params);
        $res = array();
        foreach ($users as $key => $row) {
            $res['ID'] = $row->ID;
            $res['FirstName'] = $row->FirstName;
            $res['LastName'] = $row->LastName;
            $res['Thumb'] = $row->Thumb;
            $res['RoleID'] = $row->RoleID;
            $res['GroupID'] = $row->GroupID;
            $res['Color'] = $row->Color;
        }
        return $res;
    }

    function defineUsersArray($params = array()) {
        $this->db->cache_on();
        $users = $this->getUsers($params);
        $res = array();
        foreach ($users as $key => $row) {
            $res[$key]['ID'] = $row->ID;
            $res[$key]['FirstName'] = $row->FirstName;
            $res[$key]['UserName'] = $row->UserName;
            $res[$key]['LastName'] = $row->LastName;
            $res[$key]['Thumb'] = $row->Thumb;
            $res[$key]['RoleID'] = $row->RoleID;
            $res[$key]['GroupID'] = $row->GroupID;
            $res[$key]['Color'] = $row->Color;
        }
        return $res;
    }

    function getUsersByGroupID($groupid = 1) {
        $this->db->cache_on();
        $this->db->select('ID,FirstName,Color');
        $this->db->where('GroupID', $groupid);
        $this->db->where('Published',1);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function getSupperUsers($id = 1, $token) {
        $this->db->cache_on();
        $this->db->select('ID,FirstName');
        $this->db->where('RoleID', 1);
        $this->db->where('IsAdmin', 1);
        if ($token) {
            $this->db->where('ID', $id);
        }
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

}
