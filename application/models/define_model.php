<?php
class Define_model extends CI_Model {

    var $tablename    = 'auc_define';
    var $tablename_ttp    = 'ttp_define';
    public function __construct()
    {
        parent::__construct();
    }
    
    function getDefine($orderby = 'Keyword', $sort = 'ASC') {
        $this->db->select('Keyword,Value');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result_array();
        $res = array();
        foreach($result as $key=>$item)
        {
            $res[$key]['Keyword'] = $item['Keyword'];
            if ($item['Value'] == "TRUE") {
                $res[$key]['Value'] = TRUE;
            } else if ($item['Value'] == "FALSE") {
                $res[$key]['Value'] = FALSE;
            } else {
                $res[$key]['Value'] = $item['Value'];
            }
        }
        return $res;
    }
    
    function get_order_status($group,$type,$orderby='name',$sort='asc')
    {
        $this->db->select('id, code, name, del_flg');
        $this->db->where('group',$group); 
        $this->db->where('type',$type); 
        $this->db->order_by($orderby, $sort);
        $res = $this->db->get($this->tablename_ttp)->result();
        return $res;
    }
    
    function get_list_by_group($code = null){
        if($code){
            $this->db->select('*');
            $this->db->where('group',$code); 
            $this->db->order_by('name', 'asc'); 
            $result = $this->db->get($this->tablename_ttp)->result();
            if($result != null)
            return $result;
        }
        return false;
    }

}