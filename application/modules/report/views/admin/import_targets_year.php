<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Chỉ tiêu kinh doanh</h1>
	    </div>
    </div>
    <div class="header_targets">
    	<div class="block1">
    		<div class="row">
    			<div class="form-group">
    				<div class="col-xs-4">
    					<label class="control-label col-xs-4">Chọn bộ phận</label>
    					<div class="col-xs-8">
    						<select id="department" class="form-control">
				    			<option value='0'> Tất cả phòng ban </option>
				    			<?php 
				    			$department = $this->db->query("select * from ttp_report_targets_department")->result();
				    			if(count($department)>0){
				    				foreach($department as $row){
				    					$selected = $current_department == $row->ID ? "selected='selected'" : '' ;
				    					echo "<option value='$row->ID' $selected>$row->Title</option>";
				    				}
				    			}
				    			?>
				    		</select>
    					</div>
    				</div>
    			</div>
    			<div class="form-group">
    				<div class="col-xs-4">
    					<label class="control-label col-xs-4">Năm tài chính</label>
    					<div class="col-xs-8">
    						<div class="input-group">
    						<select id="namtaichinh" class="form-control">
				    			<?php 
				    			$year = $this->db->query("select Year from ttp_report_targets_year order by Year ASC")->result();
				    			if(count($year)>0){
				    				foreach($year as $row){
				    					$selected = $row->Year==$current_year ? "selected='selected'" : '' ;
					    				echo "<option value='$row->Year' $selected>$row->Year</option>";
				    				}
				    			}else{
					    			$year = date('Y',time());
					    			for ($i=2014; $i < $year+1; $i++) { 
					    				$selected = $i==$current_year ? "selected='selected'" : '' ;
					    				echo "<option value='$i' $selected>$i</option>";
					    			}
				    			}
				    			?>
				    		</select>
				    		<span class="input-group-btn">
				    			<a class="btn btn-default" onclick="addyear(this)"><i class="fa fa-plus"></i></a>
				    		</span>
				    		</div>
    					</div>
    				</div>
				</div>
    		</div>
    	</div>
    	<?php 
    	$current_tab = $this->uri->segment(4);
    	?>
    	<div class="block2">
    		<a href="<?php echo $base_link; ?>" <?php echo $current_tab=='' ? 'class="current"' : '' ; ?>>Theo năm</a>
    		<a href="<?php echo $base_link."bymonth"; ?>" <?php echo $current_tab=='bymonth' ? 'class="current"' : '' ; ?>>Theo tháng</a>
    		<a href="<?php echo $base_link."byteam"; ?>" <?php echo $current_tab=='byteam' ? 'class="current"' : '' ; ?>>Theo nhóm trực thuộc</a>
    		<a href="<?php echo $base_link."byperson"; ?>" <?php echo $current_tab=='byperson' ? 'class="current"' : '' ; ?>>Theo cá nhân từng nhóm</a>
    	</div>
    </div>
    <div class="content_targets">
    	<div class="year_content">
    		<form action="<?php echo $base_link.'save_chitieu_year' ?>" method="post">
    		<div class="block1">
    			<a class="btn btn-danger" id="add_chitieu"><i class="fa fa-plus"></i> Chỉ tiêu</a>
    			<button class="btn btn-primary" id="save_chiteu" type="submit">Lưu thay đổi</button>
    		</div>
    		<div class="block2">
    			<div class="block2_1">
	    			<table>
	    			<tr>
	    				<th></th>
	    				<th>Tổng chỉ tiêu</th>
	    				<th>Đã phân bổ</th>
	    				<th>Chưa phân bổ</th>
	    				<th>Trạng thái</th>
	    			</tr>
	    			<?php 
	    			$result = $this->db->query("select * from ttp_report_targets_type")->result();
	    			if(count($result)>0){
	    				foreach($result as $row){
	    					$total_item = isset($data['type'.$row->ID]['Total']) ? $data['type'.$row->ID]['Total'] : 0 ;
	    					$remain_item = isset($data['type'.$row->ID]['Remain']) ? $data['type'.$row->ID]['Remain'] : $total_item ;
	    					$apply = $total_item - $remain_item;
	    					echo "<tr>";
	    					echo "<td>$row->Title</td>";
	    					echo "<td><input type='number' min='0' name='year[type$row->ID][Total]' class='total_$row->ID' value='$total_item' data-department='$row->ID' onchange='recal(this)' /> <a class='show_numberofrow' onclick='nhapdulieu(this)'>".number_format($total_item)."</a></td>";
	    					echo "<td><a class='total_phanbo_$row->ID'>".number_format($apply)."</a></td>";
	    					if($remain_item<0){
	    						echo "<td><a class='remain_phanbo_$row->ID'>0</a><input type='hidden' name='year[type$row->ID][Remain]' class='remain_$row->ID' value='$remain_item' /></td>";
	    					}else{
	    						echo "<td><a class='remain_phanbo_$row->ID'>".number_format($remain_item)."</a><input type='hidden' name='year[type$row->ID][Remain]' class='remain_$row->ID' value='$remain_item' /></td>";
	    					}
	    					if($remain_item==0)
	    					echo "<td class='message_department_$row->ID green'>Đã phân bổ hết</td>";
	    					if($remain_item<0)
	    					echo "<td class='message_department_$row->ID red'>Phân bổ vượt ".number_format(-$remain_item)."</td>";
	    					if($remain_item>0)
	    					echo "<td class='message_department_$row->ID'>Còn ".number_format($remain_item)." chưa phân bổ</td>";
	    					echo "</tr>";
	    				}
	    			}
	    			?>
	    			</table>
    			</div>
    			<div class="block2_2">
    				<?php 
    				if(count($department)>0){
    					echo "<h2>Chi tiết phân bổ cho từng phòng ban</h2>";
    					foreach($department as $row){
    						$display = $current_department==0 || $current_department==$row->ID ? "style='display:block'" : "style='display:none'" ;
    						echo "<div class='department_list_targets' $display><h3>$row->Title</h3>";
    						if(count($result)>0){
			    				foreach($result as $item){
			    					$total_item = isset($data['type'.$item->ID]['Department'][$row->ID]['Total']) ? $data['type'.$item->ID]['Department'][$row->ID]['Total'] : 0 ;
			    					echo "<div class='row'><span>$item->Title</span> <input type='number' min='0' name='year[type$item->ID][Department][$row->ID][Total]' class='department_$item->ID' data-department='$item->ID' value='$total_item' onchange='recal(this)' /> <a class='show_numberofrow' onclick='nhapdulieu(this)'>".number_format($total_item)."</a></div>";
			    				}
			    			}
    						echo "</div>";
    					}
    				}
    				?>
    			</div>
    		</div>
    	</div>
    </div>
    <div class="over_lay black">
    	<div class="box_inner" style="width:500px">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner">
    			<div class="row" style='margin:0px'>Tên loại chỉ tiêu</div>
    			<div class="row" style='margin:0px'><input type="text" id="Title_chitieu" class='form-control' /></div>
    			<div class="row" style='margin:0px'><a class="btn btn-danger" onclick="addchitieu(this)">Lưu thông tin</a></div>
    		</div>
    	</div>
    </div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
	var link = $("#baselink_report").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
	});

	$("#add_chitieu").click(function(){
		$(".over_lay .box_inner .block1_inner h1").html("Thêm loại chỉ tiêu");
		$(".over_lay").removeClass('in');
    	$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	});

	$("#department").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?department="+data;
	});

	$("#namtaichinh").change(function(){
		var data = $(this).val();
		window.location=link+"import_targets/set_fillter?year="+data;
	});

	function addchitieu(ob){
		$(ob).addClass('saving');
		var title = $("#Title_chitieu").val();
		if(title!=''){
			$.ajax({
            	url: link+"import_targets/add_type_chitieu",
	            dataType: "html",
	            type: "POST",
	            data: "Title="+title,
	            success: function(result){
	                if(result=='true'){
	                	location.reload();
	                }else{
	                	alert("Yêu cầu không được thực hiện . Vui lòng kiểm tra lại kết nối mạng .");
	                }
	                $(ob).removeClass('saving');
	            }
	        });
		}
	}

	function addyear(ob){
		$(ob).addClass('saving');
		$.ajax({
        	url: link+"import_targets/add_year",
            dataType: "html",
            type: "POST",
            data: "Year=ok",
            success: function(result){
                alert("Đã thêm năm tài chính tiếp theo .");
                location.reload();
            }
        });
        $(ob).removeClass('saving');
	}

	function nhapdulieu(ob){
		$(ob).parent().find("input").show();
		$(ob).parent().find("input").focus();
		$(ob).hide();
	}

	function recal(ob){
		money = parseInt($(ob).val());
		var data = $(ob).attr("data-department");
		total = parseInt($(".total_"+data).val());
		remain=0;
		$(".department_"+data).each(function(){
			remain = remain + parseInt($(this).val());
		});
		$(".total_phanbo_"+data).html(remain.format('a',3));
		remain = total-remain;
		$(ob).parent().find("a.show_numberofrow").html(money.format('a',3));
		$(ob).parent().find("a.show_numberofrow").show();
		$(ob).hide();
		$(".message_department_"+data).removeClass("green");
		$(".message_department_"+data).removeClass("red");
		if(remain==0){
			$(".message_department_"+data).addClass("green");
			$(".message_department_"+data).html("Đã phân bổ hết");
			$(".remain_phanbo_"+data).html(0);
		}
		if(remain<0){
			temp = -remain;
			$(".message_department_"+data).addClass("red");
			$(".message_department_"+data).html("Phân bổ vượt "+temp.format('a',3));
			$(".remain_phanbo_"+data).html(0);
		}
		if(remain>0){
			$(".message_department_"+data).html("Còn "+remain.format('a',3)+" chưa phân bổ");
			$(".remain_phanbo_"+data).html(remain.format('a',3));
		}
		$(".remain_"+data).val(remain);
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

</script>