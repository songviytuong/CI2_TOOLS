<table class="table">
	<tr>
		<th>MÃ SKU</th>
		<th>TÊN SẢN PHẨM</th>
		<th>ĐƠN VỊ</th>
		<th>SỐ LƯỢNG</th>
		<th>ĐƠN GIÁ MUA</th>
		<th>GIÁ TRỊ TỒN</th>
		<th class="text-center">HỦY</th>
	</tr>
	<?php 
	if(isset($data['products']) && count($data['products'])>0){
		$key = array_keys($data['products']);
		$arr_inventory = array();
		$inventory = $this->db->query("select * from ttp_report_inventory where ProductsID in(".implode(',',$key).") and LastEdited=1 and WarehouseID=$this->WarehouseID")->result();
		if(count($inventory)>0){
			foreach($inventory as $row){
				if(isset($arr_inventory[$row->ProductsID])){
					$arr_inventory[$row->ProductsID] = $arr_inventory[$row->ProductsID]+$row->OnHand;
				}else{
					$arr_inventory[$row->ProductsID] = $row->OnHand;
				}
			}
		}
		$products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
		if(count($products)>0){
			foreach ($products as $row) {
				$amount = $data['products'][$row->ID];
				$price = $row->RootPrice;
				$total = $amount*$price;
				$invent = isset($arr_inventory[$row->ID]) ? $arr_inventory[$row->ID] : 0 ;
				echo '<tr>
				<td>'.$row->MaSP.'</td>
				<td style="width:350px">
					<p>'.$row->Title.'</p>
				</td>
				<td>'.$row->Donvi.'</td>
				<td>
					<input type="number" value="'.$data['products'][$row->ID].'" onchange="inventory_change_products(this,'.$row->ID.')" style="width:100px" />
					<span style="color:#999;display: block;margin-top:8px">Tồn hiện tại : '.number_format($invent).'</span>
				</td>
				<td>'.number_format($price).'</td>
				<td>'.number_format($total).'</td>
				<td class="text-center"><a onclick="inventory_remove_products('.$row->ID.')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
			}
		}
	}else{
		echo "<tr><td colspan='7'>Vui lòng chọn sản phẩm</td></tr>"; 
	}
	?>
</table>