<script type="text/javascript" src="<?php echo base_url() ?>public/plugin/ckeditor/ckeditor.js"></script>

<div class="">

    <div class="tab_control col-md-12 col-sm-12 col-xs-12 hidden">

        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>

        <a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>

    </div>

    <div class="row">

        <form data-parsley-validate class="form-horizontal form-label-left" id="formdata" action="<?php echo $base_link . "update" ?>" method="post" enctype="multipart/form-data">

            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">

                    <div class="tab1 tabcontent">

                        <div class="x_title">

                            <h2>POST <small> / Edit post</small></h2>

                            <div class="clearfix"></div>

                        </div>

                        <div class="x_content">

                            <br />

                            <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />

                            <div class="form-group">

                                <label class="col-md-12 col-sm-12 col-xs-12">Published</label>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <div>

                                        <label style="padding-top:7px">

                                            <input type="radio" name="Published" value="1" <?php echo $data->Published == 1 ? "checked='checked'" : ''; ?>> &nbsp; Enable &nbsp;

                                            <input type="radio" name="Published" value="2" <?php echo $data->Published == 2 ? "checked='checked'" : ''; ?>> &nbsp; Pending &nbsp;

                                            <input type="radio" name="Published" value="0" <?php echo $data->Published == 0 ? "checked='checked'" : ''; ?>> Disable

                                        </label>

                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="form-group col-md-6">

                                    <label class="col-md-12 col-sm-12 col-xs-12" for="first-name">Select Categories <span class="required">*</span></label>

                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        <select name="CategoriesID" class="form-control">

                                            <?php
                                            $CategoriesRole = $this->user->CategoriesRole;

                                            $CategoriesRole = $CategoriesRole != '' ? json_decode($CategoriesRole, true) : array();

                                            $CategoriesRole = count($CategoriesRole) > 0 ? implode(",", $CategoriesRole) : "0";

                                            if ($this->user->IsAdmin == 1) {

                                                $result = $this->db->query("select ID,Title from ttp_categories")->result();
                                            } else {
                                                $result = $this->db->query("select ID,Title from ttp_categories")->result();
                                                //$result = $this->db->query("select ID,Title from ttp_categories where ID in($CategoriesRole)")->result();
                                            }

                                            if (count($result) > 0) {

                                                foreach ($result as $row) {

                                                    $categories = $data->CategoriesID == $row->ID ? "selected='selected'" : "";

                                                    echo "<option value='$row->ID' $categories>$row->Title</option>";
                                                }
                                            }
                                            ?>

                                        </select>

                                    </div>

                                </div>

                                <div class="form-group col-md-6 hidden">

                                    <label class="col-md-12 col-sm-12 col-xs-12">Select Landing Page <span class="required">*</span></label>

                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        <select name="PagelinksID" class="form-control">

                                            <option value='0'>-- Not Connect Page --</option>

<?php
$result = $this->db->query("select ID,Title from ttp_pagelinks")->result();

if (count($result) > 0) {

    foreach ($result as $row) {

        $landingpage = $data->PagelinksID == $row->ID ? "selected='selected'" : "";

        echo "<option value='$row->ID' $landingpage>$row->Title</option>";
    }
}
?>

                                        </select>

                                    </div>

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-md-12 col-sm-12 col-xs-12">Title (<span class="required">*</span>)</label>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <input type="text" id="CreateLink" required="required" class="form-control col-md-12 col-xs-12" name="Title" value="<?php echo str_replace('"', "'", $data->Title); ?>">

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-md-12 col-sm-12 col-xs-12">Post link (<span class="required">*</span>) <span id='loadingalias'><i class="fa fa-cog fa-spin"></i> Loading ...</span></label>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <input type="text" id="AliasLink" required="required" class="form-control col-md-12 col-xs-12" name="Alias" value="<?php echo $data->Alias ?>">

                                </div>

                            </div>

                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Description</label>
                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <textarea class="resizable_textarea form-control" name="Description" id="Description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 130px;"><?php echo $data->Description ?></textarea>

                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Youtube ID: (https://www.youtube.com/watch?v=<font color="red">ETCf0rgvCUA</font>)</label>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" id="YoutubeID" placeholder="ETCf0rgvCUA" class="form-control col-md-12 col-xs-12" name="YoutubeID" value="<?php echo $data->YoutubeID; ?>">
                                </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <textarea class="resizable_textarea form-control ckeditor" name="Introtext" id="Introtext"><?php echo $data->Introtext ?></textarea>

                                </div>

                            </div>

                        </div>

                        <!-- Tags  -->

                        <div class="x_title">

                            <h2>Tags <small> / enter keywords tags</small></h2>

                            <div class="clearfix"></div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12">

<?php
$valuetags = array();

if ($data->Tags != '') {

    $data->Tags = str_replace("[", "", $data->Tags);

    $data->Tags = str_replace("]", "", $data->Tags);

    $tags = $data->Tags;

    if ($tags != '') {

        $result = $this->db->query("select * from ttp_tags where ID in($tags)")->result();

        if (count($result) > 0) {

            foreach ($result as $item) {

                $valuetags[] = $item->Title;
            }
        }
    }
}
?>

                                <input type="text" class="form-control col-md-12 col-xs-12" name="Tags" value="<?php echo implode(",", $valuetags) ?>">

                            </div>

                        </div>

                    </div>

                    <!-- end VIETNAM -->

                    <div class="tab2 tabcontent">

                        <div class="x_title">

                            <h2>POST <small> / Edit post</small></h2>

                            <div class="clearfix"></div>

                        </div>

                        <div class="x_content">

                            <div class="form-group">

                                <label class="col-md-12 col-sm-12 col-xs-12">Title (<span class="required">*</span>)</label>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <input type="text" id="CreateLink_en" class="form-control col-md-12 col-xs-12" name="Title_en" value="<?php echo str_replace('"', "'", $data->Title_en); ?>">

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-md-12 col-sm-12 col-xs-12">Post link (<span class="required">*</span>) <span id='loadingalias'><i class="fa fa-cog fa-spin"></i> Loading ...</span></label>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <input type="text" id="AliasLink_en" class="form-control col-md-12 col-xs-12" name="Alias_en" value="<?php echo $data->Alias_en ?>">

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-md-12 col-sm-12 col-xs-12">Description</label>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <textarea class="resizable_textarea form-control" name="Description_en" id="Description_en" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 130px;"><?php echo $data->Description_en ?></textarea>

                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <textarea class="resizable_textarea form-control ckeditor" name="Introtext_en" id="Introtext_en"><?php echo $data->Introtext_en ?></textarea>

                                </div>

                            </div>

                        </div>

                        <!-- Tags  -->

                        <div class="x_title">

                            <h2>Tags <small> / enter keywords tags</small></h2>

                            <div class="clearfix"></div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12">

<?php
$valuetags = array();

if ($data->Tags_en != '') {

    $data->Tags_en = str_replace("[", "", $data->Tags_en);

    $data->Tags_en = str_replace("]", "", $data->Tags_en);

    $tags = $data->Tags_en;

    if ($tags != '') {

        $result = $this->db->query("select * from ttp_tags where ID in($tags)")->result();

        if (count($result) > 0) {

            foreach ($result as $item) {

                $valuetags[] = $item->Title;
            }
        }
    }
}
?>

                                <input type="text" class="form-control col-md-12 col-xs-12" name="Tags_en" value="<?php echo implode(",", $valuetags) ?>">

                            </div>

                        </div>



                    </div>

                    <!-- end ENGLISH -->

                    <!-- Image Thumb  -->

                    <div class="x_title">

                        <h2>IMAGE <small> / Add image thumb</small></h2>

                        <div class="clearfix"></div>

                    </div>

                    <div class="x_content"> 

                        <div class="form-group" id='neo1'>

                            <div class="images_from_server col-md-12 col-sm-12 col-xs-12"></div>

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image (600x400 px)</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div>

                                    <label>

                                        <span class="btn btn-default btn-file">

                                            Upload image from computer <input type="file" name="Image_upload" id="choosefile" onchange="viewimage()" /> 

                                        </span>

                                    </label>

                                </div>



                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Preview Image</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="dvPreview">

<?php
if (file_exists($data->Thumb)) {

    echo "<img src='$data->Thumb' style='max-width:300px;max-height:300px' />";
} else {

    echo '<span style="padding-top:9px;display:block">No image selected</span>';
}
?>

                                </div>

                            </div>

                        </div>



                    </div>

                    <div class="x_title" id="title_ext">

                        <h2>OPTION EXTENTSION <small> / Add new option</small></h2>



                        <div class="clearfix"></div>

                    </div>

                    <div class="x_content"  id="recive_ajax">

                                    <?php
                                    $jsondata = array();

                                    $json = json_decode($data->Data, true);

                                    if (count($json) > 0) {

                                        foreach ($json as $key => $value) {

                                            $jsondata[] = '"' . $key . '"';

                                            $form = '<textarea class="resizable_textarea form-control" name="' . $key . '" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;">' . $value . '</textarea>';

                                            if ($key == 'album') {

                                                $form = '<select name="album" class="form-control">';

                                                $result = $this->db->query("select * from ttp_album")->result();

                                                if (count($result) > 0) {

                                                    foreach ($result as $row) {

                                                        $selected = $row->ID == $value ? "selected='selected'" : '';

                                                        $form .= "<option value='$row->ID' $selected>$row->Title</option>";
                                                    }
                                                }

                                                $form .= '</select>';
                                            }

                                            if ($key == 'area') {

                                                $array = array(
                                                    0 => 'Tp. Hồ Chí Minh', 1 => 'Hà Nội', 2 => 'Bình Dương', 3 => 'Đà Nẵng', 4 => 'Hải Phòng', 5 => 'Long An', 6 => 'Bà Rịa Vũng Tàu', 7 => 'An Giang', 8 => 'Bắc Giang', 9 => 'Bắc Kạn', 10 => 'Bạc Liêu',
                                                    11 => 'Bắc Ninh', 12 => 'Bến Tre', 13 => 'Bình Định', 14 => 'Bình Phước', 15 => 'Bình Thuận ', 16 => 'Cà Mau', 17 => 'Cần Thơ', 18 => 'Cao Bằng', 19 => 'Đắk Lắk', 20 => 'Đắk Nông', 21 => 'Điện Biên',
                                                    22 => 'Đồng Nai', 23 => 'Đồng Tháp', 24 => 'Gia Lai', 25 => 'Hà Giang', 26 => 'Hà Nam', 27 => 'Hà Tĩnh', 28 => 'Hải Dương', 29 => 'Hậu Giang', 30 => 'Hòa Bình', 31 => 'Hưng Yên', 32 => 'Khánh Hòa',
                                                    33 => 'Kiên Giang', 34 => 'Kon Tum', 35 => 'Lai Châu', 36 => 'Lâm Đồng', 37 => 'Lạng Sơn', 38 => 'Lào Cai', 39 => 'Nam Định', 40 => 'Nghệ An', 41 => 'Ninh Bình', 42 => 'Ninh Thuận', 43 => 'Phú Thọ',
                                                    44 => 'Phú Yên', 45 => 'Quảng Bình', 46 => 'Quảng Nam', 47 => 'Quảng Ngãi', 48 => 'Quảng Ninh', 49 => 'Quảng Trị', 50 => 'Sóc Trăng', 51 => 'Sơn La', 52 => 'Tây Ninh', 53 => 'Thái Bình', 54 => 'Thái Nguyên',
                                                    55 => 'Thanh Hóa', 56 => 'Thừa Thiên Huế', 57 => 'Tiền Giang', 58 => 'Trà Vinh', 59 => 'Tuyên Quang', 60 => 'Vĩnh Long', 61 => 'Vĩnh Phúc', 62 => 'Yên Bái'
                                                );

                                                $form = '<select name="area" class="form-control">';

                                                foreach ($array as $item) {

                                                    $selected = $value == $item ? "selected='selected'" : '';

                                                    $form .= "<option value='$item' $selected>$item</option>";
                                                }

                                                $form .= '</select>';
                                            }

                                            if ($key == 'hot' || $key == 'useslide' || $key == 'showhome' || $key == 'featured' || $key == 'specialrecruitment') {

                                                $checkedtrue = $value == 'true' ? "checked='checked'" : "";

                                                $checkedfalse = $value != 'true' ? "checked='checked'" : "";

                                                $form = '<div>

                                            <label style="padding-top:7px">

                                                <input type="radio" name="' . $key . '" value="true" ' . $checkedtrue . '> &nbsp; Enable &nbsp;

                                                <input type="radio" name="' . $key . '" value="false ' . $checkedfalse . '"> Disable

                                            </label>

                                        </div>';
                                            }

                                            if ($key == 'position' || $key == 'worktime' || $key == 'position_en' || $key == 'worktime_en' || $key == 'year' || $key == 'timeline' || $key == 'number' || $key == 'block' || $key == 'changelink' || $key == 'changelink_en' || $key == 'deathline' || $key == 'video') {

                                                $form = '<input type="text" required="required" class="form-control col-md-7 col-xs-12" name="' . $key . '" value="' . $value . '">';
                                            }



                                            echo '<div class="form-group">

                                <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x]</a> ' . $key . '</label>

                                <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div>

                                        ' . $form . '

                                    </div>

                                </div>

                            </div>';
                                        }
                                    }
                                    ?>

                    </div>

                    <div class="x_content">

                        <div class="form-group" id="control_addfield">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Add new field</label>

                            <div class="col-md-3 col-sm-3 col-xs-12">

                                <select class="form-control" id="optionvalue">

                                    <option value="hot">Hot - Check box </option>

                                    <option value="showhome">Showhome - Check box </option>

                                    <option value="featured">Featured - Check box </option>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                <button type="submit" class="btn btn-success">Save</button>

                                <button type="button" class="btn btn-default" id="preview">Preview (vi)</button>

                                <button type="button" class="btn btn-primary" id="addfield">Add Field</button>

                                <button type="button" class="btn btn-primary" id="addoption">Add New Field</button>

                                <div class="description_code"><a class="question">Description field code <i class="fa fa-question-circle"></i></a>

                                    <div class='list_code'>

                                        <table>

                                            <tr>

                                                <th>Code Field</th>

                                                <th>Type Field</th>

                                                <th>Description Field</th>

                                            </tr>

                                            <tr>

                                                <td>hot</td>

                                                <td>Radio check</td>

                                                <td>Kích hoạt bài post làm tin tiêu điểm</td>

                                            </tr>

                                            <tr>

                                                <td>showhome</td>

                                                <td>Radio check</td>

                                                <td>Kích hoạt bài post hiển thị ở trang chủ - chỉ áp dụng cho bài post thuộc danh mục tin truyền thông</td>

                                            </tr>

                                            <tr>

                                                <td>featured</td>

                                                <td>Radio check</td>

                                                <td>Kích hoạt bài post làm bài viết nổi bật</td>

                                            </tr>

                                        </table>

                                    </div>

                                </div>

                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                <button type="button" class="btn btn-default hidden" id="preview_en">Preview (en)</button>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-xs-12">
<?php
$history = $this->db->query("select * from ttp_post_history where PostID=$data->ID order by ID DESC LIMIT 0,15")->result();
if (count($history) > 0) {
    echo "<table id='table_data' class='table table-bordered'>
							    			<tr style='background-color:#F6F6f6'>
							    				<th>Last Edited</th>
							    				<th class=\"col-xs-2\">Owner</th>
							    				<th class=\"col-xs-1\">Action</th>
                                                                                        <th>History</th>";
    if ($this->user->IsAdmin) {
        echo "<th class=\"col-xs-1\"></th>";
    }
    echo "</tr>";
    foreach ($history as $row) {
        $this->load->model('users_model', 'users');
        $Users = $this->users->defineUsers(($row->Owner) ? $row->Owner : 1);
        echo "<tr>
                                                    <td>" . date('d/m/Y H:i', strtotime($row->Created)) . "</td>
                                                        <td>" . ($Users['FirstName']) . "</td>
                                                        <td>" . $row->Type . "</td>
                                                        <td>" . $row->Notes . "</td>";
        if ($this->user->IsAdmin) {
            echo "<td class=\"text-center\"><a title=\"Xóa\" href=\"/administrator/home/site_post/deletehistory/" . $row->ID . "\"><i class=\"fa fa-trash-o\"></i></a></td>";
        }
        echo "</tr>";
    }
    echo "</table>";
}
?>
                    </div>
                </div>

            </div>

        </form>

    </div>

</div>

<div class="preview_box">

    <div class="preview_header">Header

        <a class="close_preview">[x] Close</a>

    </div>

    <div class="preview_content">

        <div class="preview_left">



        </div>

        <div class="preview_right">Right Content</div>

    </div>

</div>

<style>

                        <?php
                        if (count($json) > 0) {

                            echo "#title_ext{display:block;}";
                        } else {

                            echo "#title_ext{display:none;}";
                        }
                        ?>

    a{cursor: pointer}

    #addfield{display:none;}

    #control_addfield{display:none;}

    #loadingalias{display:none;}

    .question i{font-size:20px;}

    .question:hover{cursor: pointer;text-decoration: none}

    .description_code{display: inline-block;position: relative;}

    .description_code .list_code:after{content:'';width:30px;height:30px;bottom:-15px;background: #F4F4F4;display:block;position: absolute;left: 221px;transform:rotate(45deg);-moz-transform:rotate(45deg);-webkit-transform:rotate(45deg);border-bottom: 1px solid #E1e1e1;border-right: 1px solid #E1e1e1;}

    .description_code .list_code{position: absolute;bottom:45px;left:-100px;width:500px;background: #F4F4F4;border:1px solid #E1e1e1;padding:20px;display:none;}

    .description_code .list_code table{width: 100%;border-collapse: collapse;}

    .description_code .list_code table tr td{border:1px solid #E1e1e1;padding:5px 10px;background: #FFF}

    .description_code .list_code table tr th{border:1px solid #E1e1e1;padding:5px 10px;background: #FFF;width:55px}

</style>

<script>

    var open_gallery = true;

    $(document).ready(function () {

        var array = [<?php echo implode(',', $jsondata); ?>];



        $("#brownimages").click(function () {

            $(".images_from_server").toggle();

            if (open_gallery == true) {

                load_image_in_page(0);

            }

            $(document).scrollTop($("#neo1").offset().top);

        });



        $(".question").click(function () {

            $(".description_code .list_code").toggle();

        });



        $("#addoption").click(function () {

            $("#title_ext").show();

            $("#addfield").show();

            $("#control_addfield").show();

            $(this).hide();

        });



        $("#addfield").click(function () {

            var optionvalue = $("#optionvalue").val();

            if (array.indexOf(optionvalue) == -1) {

                array.push(optionvalue);

                if (optionvalue != '') {

                    $.ajax({

                        url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_option_input/' ?>" + optionvalue + "/0",

                        success: function (result) {

                            if (result != 'fasle') {

                                $("#recive_ajax").append(result);

                                $("#optionname").val('');

                            }

                        }

                    });

                }

            }

        });



        $("#choosefile").change(function () {

            var Fileinput = document.getElementById("choosefile");

            var file = Fileinput.files[0];

            var imageType = /image.*/

            var dvPreview = $(".dvPreview");

            if (file.type.match(imageType)) {

                var reader = new FileReader();

                reader.onload = function (e) {

                    var img = $("<img />");

                    img.attr("style", "max-height:400px;max-width: 400px");

                    img.attr("src", e.target.result);

                    dvPreview.html(img);

                }

                reader.readAsDataURL(file);

            } else {

                console.log("Not an Image");

            }

        });



        $("#CreateLink").change(function () {

            var data = $(this).val();

            getalias(data);

        });



        $("#AliasLink").change(function () {

            var data = $(this).val();

            var title = $("#CreateLink").val();

            data = data != '' ? data : title;

            getalias(data);

        });



        $("#CreateLink_en").change(function () {

            var data = $(this).val();

            getalias_en(data);

        });



        $("#AliasLink_en").change(function () {

            var data = $(this).val();

            var title = $("#CreateLink_en").val();

            data = data != '' ? data : title;

            getalias_en(data);

        });



        $("#preview").click(function () {

            var introtext = CKEDITOR.instances.Introtext.getData();

            var Title = $("#CreateLink").val();

            var Description = $("#Description").val();

            if (Title != '' && introtext != '') {

                $(".preview_left").html("<h3 class='preview_title'>" + Title + "</h3><div class='preview_description'>" + Description + "</div>" + introtext);

                $(".preview_box").show();

            } else {

                alert("Vui lòng nhập dữ liệu trước khi preview");

            }

        });



        $("#preview_en").click(function () {

            var introtext = CKEDITOR.instances.Introtext_en.getData();

            var Title = $("#CreateLink_en").val();

            var Description = $("#Description_en").val();

            if (Title != '' && introtext != '') {

                $(".preview_left").html("<h3 class='preview_title'>" + Title + "</h3><div class='preview_description'>" + Description + "</div>" + introtext);

                $(".preview_box").show();

            } else {

                alert("Vui lòng nhập dữ liệu trước khi preview");

            }

        });



        $(".close_preview").click(function () {

            $(".preview_box").hide();

        });



        setInterval(autosave, 60000);

    });





    function autosave() {

        var introtext = CKEDITOR.instances.Introtext.getData();

        var Title = $("#CreateLink").val();

        var Description = $("#Description").val();

        if (Title != '' && introtext != '') {

            var formData = {

                'Title': Title,

                'Description': Description,

                'Introtext': introtext,

                'ID': <?php echo $data->ID ?>

            };

            $.ajax({

                url: "<?php echo base_url() . ADMINPATH . '/home/site_post/autosave/' ?>",

                dataType: "html",

                type: "POST",

                data: formData,

                success: function (result) {},

                error: function () {

                    alert("Hệ thống tự động lưu dữ liệu không hoạt động do đường truyền có vấn đề . Vui lòng lưu lại bài viết để tránh mất dữ liệu !");

                }

            });

        }

    }



    function removeoption(ob) {

        var formgroup = $(ob).parent("label").parent("div");

        formgroup.remove();

    }



    function getalias(path) {

        if (path != '') {

            $.ajax({

                url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_alias/' ?>",

                dataType: "html",

                type: "POST",

                data: "ob=post&data=" + path + "&ID=<?php echo $data->ID ?>",

                beforeSend: function () {

                    $("#loadingalias").show();

                },

                success: function (result) {

                    if (result != false) {

                        $("#AliasLink").val(result);

                    }

                    $("#loadingalias").hide();

                }

            });

        } else {

            $("#AliasLink").val('');

        }

    }



    function getalias_en(path) {

        if (path != '') {

            $.ajax({

                url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_alias/' ?>",

                dataType: "html",

                type: "POST",

                data: "ob=post&lang=en&data=" + path + "&ID=<?php echo $data->ID ?>",

                beforeSend: function () {

                    $("#loadingalias_en").show();

                },

                success: function (result) {

                    if (result != false) {

                        $("#AliasLink_en").val(result);

                    }

                    $("#loadingalias_en").hide();

                }

            });

        } else {

            $("#AliasLink_en").val('');

        }

    }



    function load_image_in_page(page) {

        $.ajax({

            url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_view_brown_images/' ?>" + page,

            dataType: "html",

            type: "POST",

            data: "",

            success: function (result) {

                $(".images_from_server").html(result);

            }

        });

        open_gallery = false;

    }



    function choose_image_from_server(ob) {

        var data = $(ob).attr('data');

        $(".images_from_server").toggle();

        $(".dvPreview").html("<img src='" + data + "' /><input type='hidden' name='Thumb' value='" + data + "' />");

        $("#choosefile").replaceWith($("#choosefile").clone());

    }



    function viewimage() {

        var Fileinput = document.getElementById("choosefile");

        var file = Fileinput.files[0];

        var imageType = /image.*/

        var dvPreview = $(".dvPreview");

        dvPreview.html("");

        if (file.type.match(imageType)) {

            var reader = new FileReader();

            reader.onload = function (e) {

                var img = $("<img />");

                img.attr("style", "max-height:400px;max-width: 400px");

                img.attr("src", e.target.result);

                dvPreview.html(img);

            }

            reader.readAsDataURL(file);

        } else {

            console.log("Not an Image");

        }

    }

</script>

