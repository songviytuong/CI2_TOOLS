<?php 
class Warehouse_inventory_export extends Admin_Controller { 
 
    public $limit = 30;
    public $user;
    public $classname="warehouse_inventory_export";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){}

    public function request_picking(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Tools');
        $warehouse_list = $this->db->query("select ID,MaKho,Title from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
        $arr_warehouse = array();
        $arr_warehouse[] = 0;
        if(count($warehouse_list)>0){
            foreach($warehouse_list as $row){
                $arr_warehouse[] = $row->ID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and d.ID in(".implode(',',$arr_warehouse).")";
        $WarehouseID = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
        $supplier = $WarehouseID>0 ? $supplier." and d.ID=$WarehouseID" : $supplier ;
        $result = $this->db->query("select a.ID,a.MaDH,b.Amount,b.ProductsID,b.Price,b.Status,c.Donvi,c.Title,b.TimeRequest,b.BranchID,b.ID as IDDetails,d.MaKho from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_warehouse d where b.WarehouseID=d.ID and a.ID=b.OrderID and b.ProductsID=c.ID and b.Status=0 and a.Status=3 $supplier order by a.ID ASC")->result();

        $data = array(
            'data'      => $result,
            'WarehouseID'=> $WarehouseID,
            'WarehouseList'=> $warehouse_list,
            'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_export/'
        );
        $view = "admin/warehouse_inventory_export_request_picking";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function readytopick(){
        $warehouse_list = $this->db->query("select ID,MaKho,Title from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
        $arr_warehouse = array();
        $arr_warehouse[] = 0;
        if(count($warehouse_list)>0){
            foreach($warehouse_list as $row){
                $arr_warehouse[] = $row->ID;
            }
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $check = $this->db->query("select a.ID from ttp_report_order_send_supplier a,ttp_report_warehouse b where a.WarehouseID=b.ID and a.ID=$ID and b.ID in(".implode(',',$arr_warehouse).") and Status=0")->row();
        if($check){
            $data = array(
                'TimeAccept' => date('Y-m-d H:i:s'),
                'UserAccept' => $this->user->ID,
                'Status'     => 1
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_order_send_supplier",$data);
            echo "true";
        }else{
            echo "false";
        }
    }

    public function sale(){
        $startday = $this->session->userdata("import_warehouse_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_warehouse_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $searchkey = isset($_GET['search_key']) ? mysql_real_escape_string($_GET['search_key']) : '' ;
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $bonus='';
        if($this->user->Channel==0){
            $bonus = " and a.TypeExport=0";
        }
        if($this->user->Channel==1){
            $bonus = " and a.TypeExport in(1,2)";
        }
        if($this->user->Channel==2){
            $bonus = " and a.TypeExport=3";
        }
        if($this->user->UserType==7 || $this->user->IsAdmin==1){
            $bonus = "";
        }
        $payment = isset($_GET['Payment']) ? $_GET['Payment'] : -1 ;
        if($payment>=0){
            $temp = (int)$payment;
            $bonus = " and a.TransferMoney=$temp";
        }else{
            $bonus = "";
        }
        $nav = $this->db->query("select count(1) as nav from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d,ttp_report_customer e where e.ID=b.CustomerID and a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and Hinhthucxuatkho in(0,1) $bonus and (a.MaXK like '%$searchkey%' or e.Name like '%$searchkey%') $bonus")->row();
        $result = $this->db->query("select a.*,b.MaDH,c.UserName,d.MaKho as MaKho,b.ID as IDDH,b.Status from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d,ttp_report_customer e where e.ID=b.CustomerID and a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and Hinhthucxuatkho in(0,1) $bonus and (a.MaXK like '%$searchkey%' or e.Name like '%$searchkey%') $bonus order by a.ID DESC $limit_str")->result();
        $nav = $nav ? $nav->nav : 0;

        $data = array(
            'data'      => $result,
            'base_link' => base_url().ADMINPATH.'/report/import_order/',
            'start'     => $start,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'payment'   => $payment,
            'find'      => $nav,
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/import_order/export_warehouse',5,$nav,$this->limit)
        );
        $view = 'admin/import_order_export_warehouse';
        $this->template->add_title('Phiếu xuất kho đã xuất');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function internal(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        
        $status_leader = 0;
        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter_inventory_import');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $statusbonus = "";
        $statusbonus = $this->user->UserType==9 ? " and a.Status=2" : $statusbonus ;
        $orderby = "a.Ngaydathang DESC";
        $nav = $this->db->query("
            select DISTINCT a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau 
            from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e 
            where a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID=9996 and a.SourceID=11 and KenhbanhangID=7 $fillter $statusbonus")->row();
        
        $result = $this->db->query("select DISTINCT a.ID,a.MaDH,a.UserID,a.Status,a.Chiphi,a.Note,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e where a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID=11 and KenhbanhangID=7 and a.CustomerID=9996 $fillter $statusbonus order by $orderby $limit_str")->result();
        
        $total = $nav ? $nav->Total : 0;
        $chiphi = $nav ? $nav->Chiphi : 0;
        $soluongban = $nav ? $nav->SoluongBan : 0 ;
        $chietkhau = $nav ? $nav->Chietkhau : 0 ;
        $nav = $nav ? $nav->nav : 0;

        $data = array(
            'status_leader'=>$status_leader,
            'fill_data' => $this->session->userdata('fillter_inventory_import'),
            'data'      => $result,
            'total'     => $total,
            'SoluongSP' => $soluongban,
            'chiphi'    => $chiphi,
            'chietkhau' => $chietkhau,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'fillter'   => $fillter,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_export/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal',5,$nav,$this->limit)
        );
        $view = 'admin/warehouse_inventory_export_internal_order_home';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function internal_export(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d where a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and a.Hinhthucxuatkho=2")->row();
        $result = $this->db->query("select a.*,b.MaDH,c.UserName,d.MaKho as MaKho,b.ID as IDDH from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d where a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and a.Hinhthucxuatkho=2 order by a.ID DESC $limit_str")->result();
        $nav = $nav ? $nav->nav : 0;

        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_export/',
            'start'     =>  $start,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal',5,$nav,$this->limit)
        );
        $view = 'admin/warehouse_inventory_export_internal_home';
        $this->template->add_title('Phiếu xuất kho tiêu dùng nội bộ');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function internal_export_excel(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $datediff = floor(strtotime($startday)/(60*60*24)) - floor(strtotime($stopday)/(60*60*24));
        $datediff=abs($datediff);
        if($datediff>60){
            echo "<h1>Only export data up to 60 days</h1>";
            return;
        }
        $result = $this->db->query("select a.MaXK,a.Ngayxuatkho,a.Lydoxuatkho,e.MaSP,e.Title,c.Amount,f.ShipmentCode,d.MaKho as MaKho from ttp_report_export_warehouse a,ttp_report_order b,ttp_report_orderdetails c,ttp_report_warehouse d,ttp_report_products e,ttp_report_shipment f where a.KhoID=d.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and a.Hinhthucxuatkho=2 and b.CustomerID=9996 and b.ID=c.OrderID and c.ProductsID=e.ID and c.ShipmentID=f.ID order by a.Ngayxuatkho ASC")->result();
        ini_set('memory_limit', '3500M');
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày PXK')
                        ->setCellValue('B1', 'Mã xuất kho')
                        ->setCellValue('C1', 'Lý do xuất')
                        ->setCellValue('D1', 'SKU')
                        ->setCellValue('E1', 'Tên SKU')
                        ->setCellValue('F1', 'Lô xuất') 
                        ->setCellValue('G1', 'Số lượng xuất');
            $i=2;
            foreach($result as $row){
                $row->Ngayxuatkho = date('d/m/Y',strtotime($row->Ngayxuatkho));
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row->Ngayxuatkho)
                        ->setCellValue('B'.$i, $row->MaXK)
                        ->setCellValue('C'.$i, $row->Lydoxuatkho)
                        ->setCellValue('D'.$i, $row->MaSP)
                        ->setCellValue('E'.$i, $row->Title)
                        ->setCellValue('F'.$i, $row->ShipmentCode)
                        ->setCellValue('G'.$i, $row->Amount);
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('XUATKHOCHOTANGHUY');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="XUATKHOCHOTANGHUY.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Add Order | Report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_export/'
        );
        $view = 'admin/warehouse_inventory_export_internal_add_order';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(!is_numeric($id)) return;
        $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
        if($result){
            $arr_message = array("UserID"=>$this->user->ID,"Level"=>$this->user->UserType,"Time"=>time());
            $this->template->add_title('Edit Order | Import Tools');
            $data = array(
                'data'      => $result,
                'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_export/'
            );
            $view = 'admin/warehouse_inventory_export_internal_edit_order' ;
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/warehouse_inventory_export/");
        }
    }

    public function add_new_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $UserID = $this->user->ID;
        $Ngayban = isset($_POST["Ngayban"]) ? $_POST["Ngayban"] : date('m/d/Y',time());
        $Ngayban = strtotime($Ngayban);
        $Ngayban = date('Y-m-d',$Ngayban);
        $Gioban = date('H:i:s',time());
        $Loaikhachhang = 1;
        $CustomerID = 9996;
        $SourceID = 11;
        $CityID = 30;
        $DistrictID = 45;
        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';
        $Address = isset($_POST["Address"]) ? $_POST["Address"] : '';
        $phantramchietkhau = isset($_POST["phantramchietkhau"]) ? $_POST["phantramchietkhau"] : 0;
        $chiphivanchuyen = isset($_POST["chiphivanchuyen"]) ? $_POST["chiphivanchuyen"] : 0;
        $Tinhtrangdonhang = isset($_POST["Tinhtrangdonhang"]) ? $_POST["Tinhtrangdonhang"] : 2;
        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';
        $Nguoinhanhang = isset($_POST["Nguoinhanhang"]) ? $_POST["Nguoinhanhang"] : '';
        $Phongban = isset($_POST["Phongban"]) ? $_POST["Phongban"] : '';
        $KhoID = isset($_POST["KhoID"]) ? $_POST["KhoID"] : 0;
        $KenhbanhangID = 7;
        $PTthanhtoan = 0;
        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();
        $Amout = isset($_POST["Amout"]) ? $_POST["Amout"] : array();
        $Price = isset($_POST["Price"]) ? $_POST["Price"] : array();
        $PriceDown = isset($_POST["GiaCK"]) ? $_POST["GiaCK"] : array();
        $ShipmentID = isset($_POST["ShipmentID"]) ? $_POST["ShipmentID"] : array();
        $arr_note = array('Note'=>$Note,'Nguoinhanhang'=>$Nguoinhanhang,'Phongban'=>$Phongban);
        if($UserID>0){
            if(count($ProductsID)>0 && count($Amout)>0){
                $arr = array();
                $total = 0;
                $slsp = 0;
                $arr_products = array();
                foreach($ProductsID as $key=>$row){
                    $Shipment = isset($ShipmentID[$key]) ? $ShipmentID[$key] : 0 ;
                    $amounttemp = isset($Amout[$key]) ? $Amout[$key] : 0 ;
                    if(isset($arr_products[$row][$Shipment])){
                        $amounttemp = $amounttemp+$arr_products[$row][$Shipment];
                    }else{
                        $arr_products[$row][$Shipment] = $amounttemp;
                    }
                    $check = $this->db->query("select Available from ttp_report_inventory where ProductsID=$row and WarehouseID=$KhoID and ShipmentID=$Shipment and LastEdited=1")->row();
                    if($check){
                        if($check->Available<$amounttemp){
                            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                            echo "<script>alert('Yêu cầu khởi tạo không thành công ! Một trong các sản phẩm trong yêu cầu của bạn ko đủ số lượng để bán !');window.location='".$_SERVER['HTTP_REFERER']."'</script>";
                            return;
                        }
                    }else{
                        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                            echo "<script>alert('Yêu cầu khởi tạo không thành công ! Một trong các sản phẩm trong yêu cầu của bạn ko đủ số lượng để bán !');window.location='".$_SERVER['HTTP_REFERER']."'</script>";
                        return;
                    }
                }
                foreach($ProductsID as $key=>$row){
                    $dongia = isset($Price[$key]) ? $Price[$key] : 0 ;
                    $soluong = isset($Amout[$key]) ? $Amout[$key] : 0 ;
                    $km = isset($Khuyenmai[$key]) ? $Khuyenmai[$key] : '' ;
                    $giaCK = isset($PriceDown[$key]) ? $PriceDown[$key] : 0 ;
                    $Shipment = isset($ShipmentID[$key]) ? $ShipmentID[$key] : 0 ;
					$thanhtien = $dongia*$soluong;
                    $slsp = $thanhtien>=0 ? $slsp+$soluong : $slsp ;
                    $total +=$thanhtien;
                    $products = $this->db->query("select a.ID,a.DateInventory,a.Available,a.OnHand,b.RootPrice from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.ShipmentID=$Shipment and a.ProductsID=$row and a.WarehouseID=$KhoID and a.LastEdited=1 order by a.DateInventory DESC")->row();
					if($products){
                        $arr[] = "(OrderIDV,$row,$dongia,".$soluong.",$thanhtien,'$km',$giaCK,$Shipment,".$products->RootPrice.")";
                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row and ShipmentID=$Shipment and WarehouseID=$KhoID and LastEdited=1");
                        if($products->DateInventory!=date('Y-m-d')){
                            $available = $products->Available - $soluong;
                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row,$products->OnHand,$available,$KhoID,$Shipment,'".date('Y-m-d')."',1)");
                            $data_log = array(
                                'ProductsID'    =>$row,
                                'OnHand'        =>$products->OnHand,
                                'Available'     =>$products->Available-$soluong,
                                'WarehouseID'   =>$KhoID,
                                'ShipmentID'    =>$Shipment,
                                'DateInventory' =>date('Y-m-d'),
                                'Type'          =>"Available",
                                'Amount'        =>$soluong,
                                'Action'        =>'export_warehouse insert 1',
                                'Method'        =>'export warehouse'
                            );
                            $this->write_log_inventory($data_log);
                        }else{
                            $this->db->query("update ttp_report_inventory set Available=Available-".$soluong.",LastEdited=1 where ProductsID=$row and ShipmentID=$Shipment and DateInventory='".$products->DateInventory."' and WarehouseID=$KhoID");
                            $data_log = array(
                                'ProductsID'    =>$row,
                                'OnHand'        =>$products->OnHand,
                                'Available'     =>$products->Available-$soluong,
                                'WarehouseID'   =>$KhoID,
                                'ShipmentID'    =>$Shipment,
                                'DateInventory' =>$products->DateInventory,
                                'Type'          =>"Available",
                                'Amount'        =>$soluong,
                                'Action'        =>'export_warehouse update 2',
                                'Method'        =>'export warehouse'
                            );
                            $this->write_log_inventory($data_log);
                        }
                    }
                }
                if(count($arr)>0){
                    $max = $this->db->query("select max(ID) as ID from ttp_report_order")->row();
                    $max = $max ? $max->ID+1 : 1 ;
                    $monthuser = date("m",time());
                    $yearuser = date("Y",time());
                    $userorder = $this->db->query("select count(ID) as SL from ttp_report_order where MONTH(Ngaydathang)=$monthuser and YEAR(Ngaydathang)=$yearuser and UserID=".$UserID)->row();
                    $userorder = $userorder ? $userorder->SL+1 : 1 ;
                    $monyear = date('Ym',strtotime($Ngayban.' '.$Gioban));
                    $idbyUser = str_pad($userorder, 5, '0', STR_PAD_LEFT);
                    $MaDH = 'DH'.$monyear."_".$idbyUser."_".$UserID;
                    
                    if(isset($_POST['giachietkhau'])){
                        $giachietkhau = (int)$_POST['giachietkhau'];
                    }else{
                        $giachietkhau = $phantramchietkhau*($total/100);
                    }
                    
                    $data = array(
                        'ID'            =>$max,
                        'Ngaydathang'   =>$Ngayban.' '.$Gioban,
                        'MaDH'          =>$MaDH,
                        'CustomerID'    =>$CustomerID,
                        'AddressOrder'  =>$Address,
                        'CityID'        =>$CityID,
                        'DistrictID'    =>$DistrictID,
                        'Status'        =>2,
                        'Note'          =>json_encode($arr_note),
                        'UserID'        =>$UserID,
                        'SourceID'      =>$SourceID,
                        'Ghichu'        =>$Ghichu,
                        'Chiphi'        =>$chiphivanchuyen,
                        'CustomerType'  =>$Loaikhachhang,
                        'SoluongSP'     =>$slsp,
                        'Total'         =>$total,
                        'Chietkhau'     =>$giachietkhau,
                        'KenhbanhangID' =>$KenhbanhangID,
                        'Payment'       =>$PTthanhtoan,
                        'KhoID'         =>$KhoID,
                        'HistoryEdited' =>date('Y-m-d H:i:s',time())
                    );
                    $this->db->insert("ttp_report_order",$data);
                    $OrderID = $this->db->insert_id();
                    $arr = "insert into ttp_report_orderdetails(OrderID,ProductsID,Price,Amount,Total,Khuyenmai,PriceDown,ShipmentID,ImportPrice) values".implode(',',$arr);
                    $arr = str_replace('OrderIDV',$OrderID,$arr);
                    $this->db->query($arr);
                    $datahis = array(
                        'OrderID'   =>$OrderID,
                        'Thoigian'  =>date('Y-m-d H:i:s',time()),
                        'Status'    =>$Tinhtrangdonhang,
                        "Ghichu"    =>$Ghichu,
                        "UserID"    =>$this->user->ID
                    );
                    $this->db->insert('ttp_report_orderhistory',$datahis);
                    $messagelog = date('H:i:s',time())." => User '{$this->user->UserName}' maked orders with OrderID($MaDH)\n";
                    file_put_contents("log/report/".date('d-m-Y').".txt",$messagelog."\n",FILE_APPEND);
                    redirect(base_url().ADMINPATH.'/report/warehouse_inventory_export/printorder/'.$OrderID);
                }
            }
        }
        redirect(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal');
    }

    public function update_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $IsChangeOrder = isset($_POST["IsChangeOrder"]) ? $_POST["IsChangeOrder"] : 0;
        $ID = isset($_POST["IDOrder"]) ? $_POST["IDOrder"] : 0;
        $Address = isset($_POST["Address"]) ? $_POST["Address"] : '';
        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';
        $Tinhtrangdonhang = isset($_POST["Tinhtrangdonhang"]) ? $_POST["Tinhtrangdonhang"] : 2;
        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';
        $Nguoinhanhang = isset($_POST["Nguoinhanhang"]) ? $_POST["Nguoinhanhang"] : '';
        $Phongban = isset($_POST["Phongban"]) ? $_POST["Phongban"] : '';
        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();
        $Amout = isset($_POST["Amout"]) ? $_POST["Amout"] : array();
        $ShipmentID = isset($_POST["ShipmentID"]) ? $_POST["ShipmentID"] : array();
        $arr_note = array('Note'=>$Note,'Nguoinhanhang'=>$Nguoinhanhang,'Phongban'=>$Phongban);
        if($this->user->IsAdmin==1 || $this->user->UserType==2 || $this->user->UserType==7 || $this->user->UserType==8){
            $order = $this->db->query("select a.* from ttp_report_order a,ttp_report_customer b where a.ID=$ID and a.CustomerID=b.ID and SourceID=11 and KenhbanhangID=7")->row();
            if($order){
                    if($order->Status==1 || $order->Status==0){
                        redirect(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal');
                    }
                    if($order->Status==2 && $this->user->UserType==2){
                        redirect(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal');
                    }
                    if($order->Status==3){
                        if($Tinhtrangdonhang!=0){
                            redirect(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal');
                        }
                    }

                    $data = array(
                        'AddressOrder'  =>$Address,
                        'Status'        =>$Tinhtrangdonhang,
                        'Note'          =>json_encode($arr_note),
                        'Ghichu'        =>$Ghichu,
                        'HistoryEdited' =>date('Y-m-d H:i:s')
                    );
                    $this->db->where("ID",$ID);
                    $this->db->update("ttp_report_order",$data);
                    $statuslog = 0;
                    $messagelog = date('H:i:s',time())." => User '{$this->user->UserName}' change data from OrderID($order->MaDH)\n";
                    $logchange = array();
                    foreach($data as $key=>$value){
                        if($order->$key!=$value)
                            $logchange[] = "    $key : from '".$order->$key."' => $value\n";
                    }
                    if(count($logchange)>0){
                        $statuslog=1;
                        $logchange = implode("",$logchange);
                        $messagelog = $messagelog.$logchange;
                    }
                    if($statuslog==1){
                        file_put_contents("log/report/".date('d-m-Y',time()).".txt",$messagelog."\n",FILE_APPEND);
                    }

                    if($Tinhtrangdonhang==0){
                        $details = $this->db->query("select Amount,ProductsID,ShipmentID,ImportPrice from ttp_report_orderdetails where OrderID=$ID")->result();
                        $this->change_inventory($order,$details,1);
                    }
                    if($order->Status!=$Tinhtrangdonhang){
                        $datahis = array(
                            'OrderID'=>$ID,
                            'Thoigian'=>date('Y-m-d H:i:s',time()),
                            'Status'=>$Tinhtrangdonhang,
                            "Ghichu"=>$Ghichu,
                            "UserID"    =>$this->user->ID
                        );
                        $this->db->insert('ttp_report_orderhistory',$datahis);
                        if($Tinhtrangdonhang==1){
                            $details = $this->db->query("select Amount,ProductsID,ShipmentID from ttp_report_orderdetails where OrderID=$ID")->result();
                            if(count($details)>0){
                                foreach($details as $row){
                                    $check_exists = $this->db->query("select ID,Available,OnHand,DateInventory from ttp_report_inventory where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
                                    if($check_exists){
                                        if($check_exists->DateInventory==Date('Y-m-d')){
                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");
                                            $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,LastEdited=1 where ID=$check_exists->ID");
                                            $data_log = array(
                                                'ProductsID'    =>$row->ProductsID,
                                                'OnHand'        =>$check_exists->OnHand,
                                                'Available'     =>$check_exists->Available+$row->Amount,
                                                'WarehouseID'   =>$order->KhoID,
                                                'ShipmentID'    =>$row->ShipmentID,
                                                'DateInventory' =>$check_exists->DateInventory,
                                                'Type'          =>"Available",
                                                'Amount'        =>$row->Amount,
                                                'Action'        =>'export_warehouse update 3',
                                                'Method'        =>'export warehouse id '.$ID
                                            );
                                            $this->write_log_inventory($data_log);
                                        }else{
                                            $Available = $check_exists->Available+$row->Amount;
                                            $OnHand = $check_exists->OnHand;
                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");
                                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                            $data_log = array(
                                                'ProductsID'    =>$row->ProductsID,
                                                'OnHand'        =>$check_exists->OnHand,
                                                'Available'     =>$Available,
                                                'WarehouseID'   =>$order->KhoID,
                                                'ShipmentID'    =>$row->ShipmentID,
                                                'DateInventory' =>$check_exists->DateInventory,
                                                'Type'          =>"Available",
                                                'Amount'        =>$row->Amount,
                                                'Action'        =>'export_warehouse insert 4',
                                                'Method'        =>'export warehouse id '.$ID
                                            );
                                            $this->write_log_inventory($data_log);
                                        }
                                    }
                                }
                            }
                        }
                    }
            }
        }
        redirect(base_url().ADMINPATH.'/report/warehouse_inventory_export/internal');
    }

    public function get_products(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '' ;
        $KhoID = isset($_POST['KhoID']) ? mysql_real_escape_string($_POST['KhoID']) : 0 ;
        if($Title!='' && $Title!='*'){ 
            if($KhoID>0){
                $result = $this->db->query("select a.*,sum(b.Available) as Available from ttp_report_products a,ttp_report_inventory b where b.Available>=0 and b.LastEdited=1 and b.WarehouseID=$KhoID and a.ID=b.ProductsID and (a.Title like '%$Title%' or a.MaSP like '%$Title%') group by a.ID limit 0,50")->result();
            }else{
                $result = $this->db->query("select a.* from ttp_report_products a where (a.Title like '%$Title%' or a.MaSP like '%$Title%') limit 0,50")->result();
            }
        }else{
            if($KhoID>0){
                $result = $this->db->query("select a.*,sum(b.Available) as Available from ttp_report_products a,ttp_report_inventory b where b.Available>=0 and b.LastEdited=1 and b.WarehouseID=$KhoID and a.ID=b.ProductsID group by a.ID limit 0,50")->result();
            }else{
                $result = $this->db->query("select a.* from ttp_report_products a where 1=1 limit 0,50")->result();
            }
        }
        if(count($result)>0){
            $colsavilable = $KhoID>0 ? "<th>Số lượng còn</th>" : '' ;
            $str = "<div class='tools_search_products'>
                        <span>Tìm kiếm sản phẩm: </span>
                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' onchange='input_search_products(this)' /></span>
                    </div>
                    <table><tr><th></th><th>Mã Sản phẩm</th><th>Sản phẩm</th>$colsavilable
                    <th>
                        <select class='select_products' onchange='fillter_categories(this)'><option value='0'>-- Tất cả ngành hàng --</option>__</select>
                    </th></tr>";
            $categories = $this->db->query("select * from ttp_report_categories")->result();
            $arr_categories = array();
            if(count($categories)){
                foreach($categories as $row){
                    $arr_categories[$row->ID] = $row->Title;
                }
            }
            $option = array();
            foreach($result as $row){
                $row->CategoriesID = json_decode($row->CategoriesID,true);
                $row->CategoriesID = is_array($row->CategoriesID) ? $row->CategoriesID : array() ;
                $class='';
                $current_categories = array();
                if(count($row->CategoriesID)){
                    foreach($row->CategoriesID as $item){
                        if(isset($arr_categories[$item])){
                            $current_categories[] = $arr_categories[$item];
                            if(!isset($option[$item]))
                            $option[$item] = "<option value='".$item."'>".$arr_categories[$item]."</option>";
                        }
                        $class.=" categories_$item";
                    }
                }
                $str.="<tr class='trcategories $class'>";
                $str.="<td style='width:30px'><input type='checkbox' class='selected_products' data-id='$row->ID' data-code='$row->MaSP' data-name='$row->Title' data-price='$row->Price' /></td>";
                $str.="<td style='width: 130px;'>".$row->MaSP."</td>";
                $str.="<td>".$row->Title."</td>";
                if($KhoID>0){
                    $str.=$row->Available>0 ? "<td style='width: 100px;'>".number_format($row->Available)."</td>" : "<td>## Hết Hàng ##</td>" ;
                }
                $str.="<td style='width:170px'>".implode(',',$current_categories)."</td>";
                $str.="</tr>";
            }
            $option = implode('',$option);
            echo str_replace("__",$option,$str);
            echo "</table>";
            echo "<div class='fixedtools'><a class='btn btn-box-inner' onclick='add_products()'>Đưa vào đơn hàng</a></div>";
            return;
        }
        echo "false";
    }

    public function printorder($id=0){
        if($this->user->IsAdmin==1 || $this->user->UserType==2 || $this->user->UserType==8){
            if($id>0){
                $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle,e.Title as Source,f.Title as Kenh,g.Title as Thanhpho,h.Title as Quanhuyen,i.Title as Area from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d,ttp_report_source e,ttp_report_saleschannel f,ttp_report_city g,ttp_report_district h,ttp_report_area i where g.AreaID=i.ID and a.DistrictID=h.ID and a.CityID=g.ID and a.KenhbanhangID=f.ID and a.SourceID=e.ID and a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
                if($result){
                    if($result->Status==3 || $result->Status==0){
                        $this->template->add_title('Preview Order | Import Tools');
                        $data = array(
                            'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_export/',
                            'data'      => $result
                        );
                        $view = 'admin/warehouse_inventory_export_internal_print_order' ;
                        $this->template->write_view('content',$view,$data);
                        $this->template->render();
                        return;
                    }
                }
            }
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_export/internal' ;
            redirect($referer);
        }else{
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_export/internal' ;
            redirect($referer);
        }
    }

    public function lapphieuxuatkho($id=0){
        if($this->user->IsAdmin==1 || $this->user->UserType==2 || $this->user->UserType==8 || $this->user->UserType==7){
            if($id>0){
                $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
                if($result){
                    if($result->Status==3 || $result->Status==0){
                        $this->template->add_title('Created Bill | Import Tools');
                        $data = array(
                            'base_link' => base_url().ADMINPATH.'/report/import/',
                            'data'      => $result
                        );
                        $view = 'admin/warehouse_inventory_export_internal_create_bill' ;
                        $this->template->write_view('content',$view,$data);
                        $this->template->render();
                        return;
                    }
                }
            }
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_export/internal' ;
            redirect($referer);
        }else{
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_export/internal' ;
            redirect($referer);
        }
    }

    public function get_shipment_arr($ProductsID=0,$WarehouseID=0,$Amount=1){
        if($ProductsID>0 && $WarehouseID>0){
            $products = $this->db->query("select a.ShipmentID,a.Available,a.DateInventory,a.OnHand,c.RootPrice from ttp_report_inventory a,ttp_report_shipment b,ttp_report_products c where a.ProductsID=c.ID and a.ShipmentID=b.ID and a.ProductsID=$ProductsID and a.WarehouseID=$WarehouseID and a.Available>0 and a.LastEdited=1 and b.DateExpiration >= '".date('Y-m-d')."' order by b.DateExpiration ASC")->result();
            if(count($products)>0){
                $arr = array();
                foreach($products as $row){
                    if($row->Available>=$Amount){
                        $arr[$row->ShipmentID] = array(
                                'OnHand'        =>$row->OnHand,
                                'Available'     =>$row->Available-$Amount,
                                'Amount'        =>$Amount,
                                'DateInventory' =>$row->DateInventory,
                                'Price'         =>$row->RootPrice*$Amount
                            );
                        return $arr;
                    }else{
                        $arr[$row->ShipmentID] = array(
                            'OnHand'        =>$row->OnHand,
                            'Available'     =>0,
                            'Amount'        =>$row->Available,
                            'DateInventory' =>$row->DateInventory,
                            'Price'         =>$row->RootPrice*$Amount
                        );
                        $Amount = $Amount-$row->Available;
                    }
                }
                return $arr;
            }
        }
        return array();
    }

    public function change_inventory($order,$details,$status=0){
        if($order){
            if(count($details)>0){
                foreach($details as $row){
                    $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
                    if($check_exists){
                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1");
                        if($check_exists->DateInventory==date('Y-m-d')){
                            if($status==0){
                                $this->db->query("update ttp_report_inventory set Available=Available-$row->Amount,OnHand=OnHand-$row->Amount,LastEdited=1 where ID=$check_exists->ID");
                                $data_log = array(
                                    'ProductsID'    =>$row->ProductsID,
                                    'OnHand'        =>$check_exists->OnHand-$row->Amount,
                                    'Available'     =>$check_exists->Available-$row->Amount,
                                    'WarehouseID'   =>$order->KhoID,
                                    'ShipmentID'    =>$row->ShipmentID,
                                    'DateInventory' =>$check_exists->DateInventory,
                                    'Type'          =>"All",
                                    'Amount'        =>$row->Amount,
                                    'Action'        =>'export_warehouse update 5',
                                    'Method'        =>'export warehouse id '.$order->ID
                                );
                                $this->write_log_inventory($data_log);
                            }else{
                                $this->db->query("update ttp_report_inventory set OnHand=OnHand-$row->Amount,LastEdited=1 where ID=$check_exists->ID");
                                $data_log = array(
                                    'ProductsID'    =>$row->ProductsID,
                                    'OnHand'        =>$check_exists->OnHand-$row->Amount,
                                    'Available'     =>$check_exists->Available-$row->Amount,
                                    'WarehouseID'   =>$order->KhoID,
                                    'ShipmentID'    =>$row->ShipmentID,
                                    'DateInventory' =>$check_exists->DateInventory,
                                    'Type'          =>"OnHand",
                                    'Amount'        =>$row->Amount,
                                    'Action'        =>'export_warehouse update 6',
                                    'Method'        =>'export warehouse id '.$order->ID
                                );
                                $this->write_log_inventory($data_log);
                            }
                        }else{
                            if($status==0){
                                $Available = $check_exists->Available-$row->Amount;
                                $OnHand = $check_exists->OnHand-$row->Amount;
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                $data_log = array(
                                    'ProductsID'    =>$row->ProductsID,
                                    'OnHand'        =>$OnHand,
                                    'Available'     =>$Available,
                                    'WarehouseID'   =>$order->KhoID,
                                    'ShipmentID'    =>$row->ShipmentID,
                                    'DateInventory' =>$check_exists->DateInventory,
                                    'Type'          =>"All",
                                    'Amount'        =>$row->Amount,
                                    'Action'        =>'export_warehouse insert 7',
                                    'Method'        =>'export warehouse id '.$order->ID
                                );
                                $this->write_log_inventory($data_log);
                            }else{
                                $Available = $check_exists->Available;
                                $OnHand = $check_exists->OnHand-$row->Amount;
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");         
                                $data_log = array(
                                    'ProductsID'    =>$row->ProductsID,
                                    'OnHand'        =>$OnHand,
                                    'Available'     =>$Available,
                                    'WarehouseID'   =>$order->KhoID,
                                    'ShipmentID'    =>$row->ShipmentID,
                                    'DateInventory' =>$check_exists->DateInventory,
                                    'Type'          =>"OnHand",
                                    'Amount'        =>$row->Amount,
                                    'Action'        =>'export_warehouse insert 8',
                                    'Method'        =>'export warehouse id '.$order->ID
                                );
                                $this->write_log_inventory($data_log);                       
                            }
                        }
                    }else{
                        $Available = 0 ;
                        $OnHand = 0 ;
                        $Available = $Available-$row->Amount;
                        $OnHand = $OnHand-$row->Amount;
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                    }
                }
            }
        }
    }

    public function get_shipment_by_productsID($id=0,$warehouse=0){
        $result = $this->db->query("select a.Available,b.ShipmentCode,b.ID from ttp_report_inventory a,ttp_report_shipment b where a.ProductsID=$id and a.WarehouseID=$warehouse and a.LastEdited=1 and a.Available>0 and a.ShipmentID=b.ID")->result();
        if(count($result)>0){
            echo "<option value='0'>Chọn lô xuất hàng</option>";
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->ShipmentCode</option>";
            }
        }else{
            echo "<option value='0'>## Hết hàng ##</option>";
        }
    }

    public function get_available_by_shipment($id=0,$warehouse=0,$shipment=0){
        $result = $this->db->query("select a.Available from ttp_report_inventory a where a.ProductsID=$id and a.WarehouseID=$warehouse and a.ShipmentID=$shipment and a.LastEdited=1 and a.Available>0")->row();
        echo $result ? "Số lượng còn : ".number_format($result->Available) : "## Hết hàng ##" ;
    }

    public function get_another_shipment(){
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 0 ;
        $ShipmentID = isset($_POST['ShipmentID']) ? (int)$_POST['ShipmentID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $Amount = isset($_POST['Amount']) ? (int)$_POST['Amount'] : 0 ;
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $check = $this->db->query("select a.ShipmentID,b.ShipmentCode,a.Available from ttp_report_inventory a,ttp_report_shipment b where a.ShipmentID=b.ID and a.WarehouseID=$WarehouseID and a.ShipmentID!=$ShipmentID and a.ProductsID=$ProductsID and a.LastEdited=1 and a.Available>=$Amount order by a.DateInventory ASC")->result();
        if($check){
            if(count($check)>0){
                foreach($check as $row){
                    echo "<li><a title='Thay đổi lô cũ thành lô $row->ShipmentCode' onclick='replace_shipment($ID,$row->ShipmentID)'>$row->ShipmentCode <i>($row->Available)</i></a></li>";
                }
            }
        }else{
            echo "<li><small>Không có lựa chọn khác</small></li>";
        }
    }

    public function replace_amount(){
        $Amount = isset($_POST['Amount']) ? (int)$_POST['Amount'] : 0 ;
        if($Amount<0){ 
            echo "Tham số đầu vào sai !";
            return;
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $details = $this->db->query("select a.*,b.KhoID from ttp_report_orderdetails a,ttp_report_order b where a.OrderID=b.ID and a.ID=$ID")->row();
        if($details){
            if($details->Amount<$Amount){
                $temp = $Amount-$details->Amount;
                $check_exists = $this->db->query("select ID,Available,OnHand,DateInventory from ttp_report_inventory where ProductsID=$details->ProductsID and ShipmentID=$details->ShipmentID and WarehouseID=$details->KhoID and LastEdited=1 and Available>=$temp order by DateInventory DESC limit 0,1")->row();
            }elseif($details->Amount>$Amount){
                $check_exists = $this->db->query("select ID,Available,OnHand,DateInventory from ttp_report_inventory where ProductsID=$details->ProductsID and ShipmentID=$details->ShipmentID and WarehouseID=$details->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
            }else{
                echo "OK";
                return;
            }
            if($check_exists){
                if($check_exists->DateInventory==Date('Y-m-d')){
                    $temp = $details->Amount-$Amount;
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                    $this->db->query("update ttp_report_inventory set Available=Available+$temp,LastEdited=1 where ID=$check_exists->ID");
                    $data_log = array(
                        'ProductsID'    =>$details->ProductsID,
                        'OnHand'        =>$check_exists->OnHand,
                        'Available'     =>$check_exists->Available+$temp,
                        'WarehouseID'   =>$details->KhoID,
                        'ShipmentID'    =>$details->ShipmentID,
                        'DateInventory' =>$check_exists->DateInventory,
                        'Type'          =>"Available",
                        'Amount'        =>$temp,
                        'Action'        =>'export_warehouse update 9',
                        'Method'        =>'replace amount id '.$details->OrderID
                    );
                    $this->write_log_inventory($data_log);     
                }else{
                    $temp = $details->Amount-$Amount;
                    $Available = $check_exists->Available+$temp;
                    $OnHand = $check_exists->OnHand;
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($details->ProductsID,$OnHand,$Available,$details->KhoID,$details->ShipmentID,'".date('Y-m-d')."',1)");
                    $data_log = array(
                        'ProductsID'    =>$details->ProductsID,
                        'OnHand'        =>$check_exists->OnHand,
                        'Available'     =>$check_exists->Available+$temp,
                        'WarehouseID'   =>$details->KhoID,
                        'ShipmentID'    =>$details->ShipmentID,
                        'DateInventory' =>$check_exists->DateInventory,
                        'Type'          =>"Available",
                        'Amount'        =>$temp,
                        'Action'        =>'export_warehouse insert 10',
                        'Method'        =>'replace amount id '.$details->OrderID
                    );
                    $this->write_log_inventory($data_log);    
                }
                $this->db->query("update ttp_report_orderdetails set Amount=$Amount where ID=$ID");
                echo "OK";
            }else{
                echo "Không đủ số lượng yêu cầu .";
                return;
            }
        }
    }

    public function remove_products(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        if($ID>0){
            $details = $this->db->query("select a.*,b.KhoID from ttp_report_orderdetails a,ttp_report_order b where a.ID=$ID and a.OrderID=b.ID and b.Status=4")->row();
            if($details){
                $check_exists = $this->db->query("select ID,Available,OnHand,DateInventory from ttp_report_inventory where ProductsID=$details->ProductsID and ShipmentID=$details->ShipmentID and WarehouseID=$details->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
                if($check_exists){
                    if($check_exists->DateInventory==Date('Y-m-d')){
                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                        $this->db->query("update ttp_report_inventory set Available=Available+$details->Amount,LastEdited=1 where ID=$check_exists->ID");
                        $data_log = array(
                            'ProductsID'    =>$details->ProductsID,
                            'OnHand'        =>$check_exists->OnHand,
                            'Available'     =>$check_exists->Available+$details->Amount,
                            'WarehouseID'   =>$details->KhoID,
                            'ShipmentID'    =>$details->ShipmentID,
                            'DateInventory' =>$check_exists->DateInventory,
                            'Type'          =>"Available",
                            'Amount'        =>$details->Amount,
                            'Action'        =>'export_warehouse update 11',
                            'Method'        =>'remove products id '.$details->OrderID
                        );
                        $this->write_log_inventory($data_log);    
                    }else{
                        $Available = $check_exists->Available+$details->Amount;
                        $OnHand = $check_exists->OnHand;
                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($details->ProductsID,$OnHand,$Available,$details->KhoID,$details->ShipmentID,'".date('Y-m-d')."',1)");
                        $data_log = array(
                            'ProductsID'    =>$details->ProductsID,
                            'OnHand'        =>$check_exists->OnHand,
                            'Available'     =>$check_exists->Available+$details->Amount,
                            'WarehouseID'   =>$details->KhoID,
                            'ShipmentID'    =>$details->ShipmentID,
                            'DateInventory' =>$check_exists->DateInventory,
                            'Type'          =>"Available",
                            'Amount'        =>$details->Amount,
                            'Action'        =>'export_warehouse insert 12',
                            'Method'        =>'remove products id '.$details->OrderID
                        );
                        $this->write_log_inventory($data_log); 
                    }
                }
                $this->db->query("delete from ttp_report_orderdetails where ID=$ID");
                echo "OK";
                return;
            }
        }
        echo "FALSE";
    }

    public function replace_shipment(){
        $ShipmentID = isset($_POST['ShipmentID']) ? (int)$_POST['ShipmentID'] : 0 ;
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        if($ID>0 && $ShipmentID>0){
            $details = $this->db->query("select a.*,b.KhoID,b.MaDH,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_order b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=b.ID and a.ID=$ID")->row();
            if($details){
                $data = $this->db->query("select a.ID,a.Available,a.OnHand,a.DateInventory,b.ShipmentCode from ttp_report_inventory a,ttp_report_shipment b where a.ShipmentID=b.ID and a.ProductsID=$details->ProductsID and a.ShipmentID=$ShipmentID and a.WarehouseID=$details->KhoID and a.LastEdited=1 order by a.DateInventory DESC limit 0,1")->row();
                if($data){
                    if($data->Available>=$details->Amount){
                        /* Reput Available */
                        $check_exists = $this->db->query("select ID,Available,OnHand,DateInventory from ttp_report_inventory where ProductsID=$details->ProductsID and ShipmentID=$details->ShipmentID and WarehouseID=$details->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
                        if($check_exists){
                            if($check_exists->DateInventory==Date('Y-m-d')){
                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                                $this->db->query("update ttp_report_inventory set Available=Available+$details->Amount,LastEdited=1 where ID=$check_exists->ID");
                                $data_log = array(
                                    'ProductsID'    =>$details->ProductsID,
                                    'OnHand'        =>$check_exists->OnHand,
                                    'Available'     =>$check_exists->Available+$details->Amount,
                                    'WarehouseID'   =>$details->KhoID,
                                    'ShipmentID'    =>$details->ShipmentID,
                                    'DateInventory' =>$check_exists->DateInventory,
                                    'Type'          =>"Available",
                                    'Amount'        =>$details->Amount,
                                    'Action'        =>'export_warehouse update 13',
                                    'Method'        =>'replace shipment id '.$details->OrderID
                                );
                                $this->write_log_inventory($data_log); 
                            }else{
                                $Available = $check_exists->Available+$details->Amount;
                                $OnHand = $check_exists->OnHand;
                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($details->ProductsID,$OnHand,$Available,$details->KhoID,$details->ShipmentID,'".date('Y-m-d')."',1)");
                                $data_log = array(
                                    'ProductsID'    =>$details->ProductsID,
                                    'OnHand'        =>$check_exists->OnHand,
                                    'Available'     =>$check_exists->Available+$details->Amount,
                                    'WarehouseID'   =>$details->KhoID,
                                    'ShipmentID'    =>$details->ShipmentID,
                                    'DateInventory' =>$check_exists->DateInventory,
                                    'Type'          =>"Available",
                                    'Amount'        =>$details->Amount,
                                    'Action'        =>'export_warehouse insert 14',
                                    'Method'        =>'replace shipment id '.$details->OrderID
                                );
                                $this->write_log_inventory($data_log); 
                            }
                        }else{
                            echo "False";
                            return;
                        }

                        /* Replace Shipment */
                        if($data->DateInventory==Date('Y-m-d')){
                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                            $this->db->query("update ttp_report_inventory set Available=Available-$details->Amount,LastEdited=1 where ID=$data->ID");
                            $data_log = array(
                                'ProductsID'    =>$details->ProductsID,
                                'OnHand'        =>$check_exists->OnHand,
                                'Available'     =>$check_exists->Available-$details->Amount,
                                'WarehouseID'   =>$details->KhoID,
                                'ShipmentID'    =>$details->ShipmentID,
                                'DateInventory' =>$check_exists->DateInventory,
                                'Type'          =>"Available",
                                'Amount'        =>$details->Amount,
                                'Action'        =>'export_warehouse update 15',
                                'Method'        =>'replace shipment id '.$details->OrderID
                            );
                            $this->write_log_inventory($data_log); 
                        }else{
                            $Available = $data->Available-$details->Amount;
                            $OnHand = $data->OnHand;
                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");
                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($details->ProductsID,$OnHand,$Available,$details->KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                            $data_log = array(
                                'ProductsID'    =>$details->ProductsID,
                                'OnHand'        =>$check_exists->OnHand,
                                'Available'     =>$check_exists->Available-$details->Amount,
                                'WarehouseID'   =>$details->KhoID,
                                'ShipmentID'    =>$details->ShipmentID,
                                'DateInventory' =>$check_exists->DateInventory,
                                'Type'          =>"Available",
                                'Amount'        =>$details->Amount,
                                'Action'        =>'export_warehouse update 16',
                                'Method'        =>'replace shipment id '.$details->OrderID
                            );
                            $this->write_log_inventory($data_log); 
                        }
                        $this->db->query("update ttp_report_orderdetails set ShipmentID=$ShipmentID where ID=$ID");
                        echo "OK";
                        $messagelog = date('H:i:s',time())." => User '{$this->user->UserName}' change shipment $details->ShipmentID($details->ShipmentCode) -> $ShipmentID($data->ShipmentCode) Order($details->MaDH) \n";
                        file_put_contents("log/report/".date('d-m-Y',time()).".txt",$messagelog."\n",FILE_APPEND);
                        return;
                    }else{
                        echo "False";
                        return;
                    }
                }
            }
        }
        echo "False";
    }

    public function export_another_template($id=0){
        $order = $this->db->query("select a.MaXK,a.Lydoxuatkho,b.Note,c.MaKho from ttp_report_export_warehouse a,ttp_report_order b,ttp_report_warehouse c where b.KhoID=c.ID and a.OrderID=b.ID and b.ID=$id")->row();
        if($order){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $note = json_decode($order->Note);
            $note_txt = isset($note->Note) ? $note->Note : "--" ;
            $phongban_txt = isset($note->Phongban) ? $note->Phongban : "--" ;
            $nguoinhanhang_txt = isset($note->Nguoinhanhang) ? $note->Nguoinhanhang : "--" ;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"PHIẾU XUẤT KHO SỐ : $order->MaXK");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2',"Phòng ban");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2',$phongban_txt);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3',"Người nhận hàng");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3',$nguoinhanhang_txt);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4',"Lý do xuất");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4',$note_txt);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6',"STT")
                                                ->setCellValue('B6',"Mã SKU")
                                                ->setCellValue('C6',"Tên sản phẩm")
                                                ->setCellValue('D6',"Đơn vị tính")
                                                ->setCellValue('E6',"Lô hàng")
                                                ->setCellValue('F6',"Số lượng");
            $details = $this->db->query("select a.*,b.MaSP,b.Title,b.Donvi,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where c.ID=a.ShipmentID and a.ProductsID=b.ID and a.OrderID=$id")->result();
            if(count($details)>0){
                $i=7;
                foreach($details as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $row->MaSP)
                        ->setCellValue('C'.$i, $row->Title)
                        ->setCellValue('D'.$i, $row->Donvi)
                        ->setCellValue('E'.$i, $row->ShipmentCode)
                        ->setCellValue('F'.$i, $row->Amount);
                    $i++;
                }
            }
            $i--;
            $objPHPExcel->getActiveSheet()->getStyle("A6:F6")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => "deffdc"),'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $objPHPExcel->getActiveSheet()->getStyle("A6:F6")->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $objPHPExcel->getActiveSheet()->getStyle("A7:F$i")->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(18);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B2:F2');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B3:F3');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:F4');
            
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.date('d-m-Y').'-'.'-PHIEUXUATKHO.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "This order is not exists !";
        }
    }

    public function export_sale_template($id=0){
        $order = $this->db->query("select a.MaXK,a.Lydoxuatkho,b.Note,b.AddressOrder,c.MaKho,d.Name,d.Phone1,e.UserName from ttp_report_export_warehouse a,ttp_report_order b,ttp_report_warehouse c,ttp_report_customer d,ttp_user e where b.UserID=e.ID and d.ID=b.CustomerID and b.KhoID=c.ID and a.OrderID=b.ID and b.ID=$id")->row();
        if($order){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1',"PHIẾU XUẤT KHO SỐ : $order->MaXK");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2',"Tên khách hàng");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2',$order->Name);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2',"Người tạo đơn hàng");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2',$order->UserName);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3',"Số điện thoại");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3',$order->Phone1);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4',"Ghi chú đơn hàng");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B4',$order->Note);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A5',"Địa chỉ giao hàng");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B5',$order->AddressOrder);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A7',"STT")
                                                ->setCellValue('B7',"Mã SKU")
                                                ->setCellValue('C7',"Tên sản phẩm")
                                                ->setCellValue('D7',"Đơn vị tính")
                                                ->setCellValue('E7',"Lô hàng")
                                                ->setCellValue('F7',"Số lượng");
            $details = $this->db->query("select a.*,b.MaSP,b.Title,b.Donvi,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where c.ID=a.ShipmentID and a.ProductsID=b.ID and a.OrderID=$id")->result();
            if(count($details)>0){
                $i=8;
                $j=1;
                foreach($details as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $row->MaSP)
                        ->setCellValue('C'.$i, $row->Title)
                        ->setCellValue('D'.$i, $row->Donvi)
                        ->setCellValue('E'.$i, $row->ShipmentCode)
                        ->setCellValue('F'.$i, $row->Amount);
                    $i++;
                    $j++;
                }
            }
            $i--;
            $objPHPExcel->getActiveSheet()->getStyle("A7:F7")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' => "deffdc"),'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $objPHPExcel->getActiveSheet()->getStyle("A7:F7")->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $objPHPExcel->getActiveSheet()->getStyle("A7:F$i")->applyFromArray(array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(18);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B3:F3');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:F4');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:F5');
            
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.date('d-m-Y').'-'.'-PHIEUXUATKHO.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "This order is not exists !";
        }
    }
}
?>