<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Thêm khách hàng mới</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên khách hàng</span></div>
					<div class='block2'><input type='text' class="form-control" name="Name" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Số điện thoại (1)</span></div>
					<div class='block2'><input type='text' class="form-control" name="Phone1" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Số điện thoại (2)</span></div>
					<div class='block2'><input type='text' class="form-control" name="Phone2" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Địa chỉ khách hàng</span></div>
					<div class='block2'><input type='text' class="form-control" name="Address" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Tuổi</span></div>
					<div class='block2'><input type='text' class="form-control" name="Age" /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Ngày sinh</span></div>
					<div class='block2'><input type='text' class="form-control" name="Birthday" placeholder="dd/mm/yyyy" /></div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>