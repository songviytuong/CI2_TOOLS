<div class="page-title">
    <div class="title_left">
        <h3></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>LANGUAGES <small> / view all language</small></h2>
                <ul class="navbar-right panel_toolbox">
                    <?php if($langid == 2) : ?>
                    <li><a href="<?php echo $base_link . "add" ?>" class="btn btn-success" role="button">Add New</a></li>
                    <?php endif; ?>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>
                                STT
                            </th>
                            <th>Keyword</th>
                            <th>Translate</th>
                            <th class="text-right no-link last"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($data) > 0) {
                            $i = $start;
                            foreach ($data as $row) {
                                $nav1 = $this->db->query("select count(ID) as cnt from ttp_languages_translate WHERE LangID = 1 and Keyword = '" . $row->Keyword . "'")->row()->cnt;
                                $i++;
                                ?>
                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <?php echo $i; ?>
                                    </td>
                                    <td class="a-right a-right "><input class="form-control" type="text" value="<?= $row->Keyword ?>" readonly=""/></td>
                                    <td class="a-right a-right "><input id="tran_<?= $row->Keyword ?>" class="form-control" type="text" value="<?= $row->Translate ?>"/></td>
                                    <td class="text-right last">
                                        <?php if (!empty($nav1) == 0) : ?>
                                            <span class="duplicate btn btn-sm" data-key="<?= $row->Keyword; ?>"> <i class="fa fa-copy"></i> Duplicate </span>
                                        <?php endif; ?>
                                        <span class="save btn btn-sm" rel="<?= $row->Keyword; ?>" id="<?= $this->uri->segment(5); ?>"> <i class="fa fa-save"></i> Save </span>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                <?php if (count($data) > 0) echo $nav; ?>
            </div>
        </div>
    </div>
</div>
