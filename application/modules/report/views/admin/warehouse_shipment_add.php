<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Tạo mới lô hàng cho sản phẩm</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Lưu nội dung</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Chọn sản phẩm : </span></div>
					<div class='block2'>
						<select name="ProductsID" class="form-control">
							<?php 
							$result = $this->db->query("select Title,ID,MaSP from ttp_report_products")->result();
							if(count($result)>0){
								foreach($result as $row){
									echo "<option value='$row->ID'>$row->MaSP - $row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã lô hàng : </span></div>
					<div class='block2'><input type='text' class="form-control" name="ShipmentCode" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Ngày sản xuất : </span></div>
					<div class='block2'><input type='text' class="form-control" name="DateProduction" id="DateProduction" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Hạn sử dụng : </span></div>
					<div class='block2'><input type='text' class="form-control" name="DateExpiration" id="DateExpiration" required /></div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
    .daterangepicker{width: auto;}
    .row{margin:0px;}
</style>
<script>
	$(document).ready(function () {
        $('#DateProduction,#DateExpiration').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });
</script>