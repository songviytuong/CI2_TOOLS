<?php

class Operation_model extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    public static function PickStatus() {
        $arr = array(
            'Wait Pick',
            'Pick Finish',
            'Pick Partial',
            'Full OOS',
            'Partial OOS',
        );
        return $arr;
    }

    public static function PickUpAction() {
        $arr = array(
            'View Detail',
            'Cancel Order',
            'Replace missed item with other',
            'Process anyway',
        );
        sort($arr);
        return $arr;
    }
    
    #All Store List
    function getAllStoreList() {
        $this->db->select('*,w.ID as ID, w.Title as Name, w.Address_maps as FullAddress,c.Title as CityName');
        $this->db->from('ttp_report_warehouse w');
        $this->db->join('ttp_report_city c', 'c.ID = w.CityID', 'inner');
        $result = $this->db->get()->result();
        return $result;
    }

    function StoreOption(){
        $data_AllStoreList = $this->getAllStoreList();
        $arr = array();
        if ($data_AllStoreList) {
            foreach ($data_AllStoreList as $key => $row) {
                $json = $row;
                $arr[$key]['store_id'] = isset($json->ID) ? $json->ID : -1;
                $arr[$key]['store_code'] = isset($json->MaKho) ? $json->MaKho : '';
                $arr[$key]['store_name'] = isset($json->Name) ? $json->Name : '';
                $arr[$key]['store_lat'] = isset($json->Lat) ? $json->Lat : '';
                $arr[$key]['store_long'] = isset($json->Long) ? $json->Long : '';
                $arr[$key]['store_address'] = isset($json->Address) ? $json->Address : '';
                $arr[$key]['store_phone'] = isset($json->Phone1) ? $json->Phone1 : '';
                $arr[$key]['store_image'] = isset($json->Image) ? ROOT_URL.'/'.$json->Image : '';
                $arr[$key]['store_cityname'] = isset($json->CityName) ? $json->CityName : "";
                $arr[$key]['store_address_map'] = isset($json->FullAddress) ? $json->FullAddress : "";
                $arr[$key]['store_open'] = isset($json->Open) ? $json->Open : "";
            }
        }
        return $arr;
    }

}
