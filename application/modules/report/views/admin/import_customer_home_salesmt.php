<div class="containner">
		<div class="import_orderlist">
			<div class="block2">
	    		<div class="block_2_1">
	    			<a class="btn btn-primary" href="<?php echo $base_link.'add_sales'; ?>"><i class="fa fa-plus"></i> KHÁCH HÀNG</a>
	    			<form action="<?php echo $base_link ?>setsessionsearch" method="post">
						<input type="text" name="Search" placeholder="Search..." style="height: 36px;padding: 5px 10px;border: 1px solid #ccc;float:left" />
						<button type="submit" class="btn btn-default" style="height: 36px;border-radius: 0px 5px 5px 0px;padding: 5px 10px;background: #EEE;border-left: none;"><i class="fa fa-search"></i> Fillter</button>
	                    <a href="<?php echo $base_link.'clearfilter' ?>" style='margin-left: 10px;text-decoration: underline;'>Clear Filter</a>
					</form>
	    		</div>
	    		<div class="block_2_2" style="text-align:right;line-height: 35px;">
	    			<select style="height:32px;width:128px;border:1px solid #c1c1c1;margin-right:10px;margin-left:5px" id="channel" onchange="setsearch(this)">
	    				<?php 
	    				$TypeChanel = $this->session->userdata("report_filter_customer_TypeChanel");
	    				?>
	    				<option value='0' <?php echo $TypeChanel==0 || $TypeChanel=='' ? "selected='selected'" : '' ; ?>>-- Loại kênh GT --</option>
	    				<option value='1' <?php echo $TypeChanel==1 ? "selected='selected'" : '' ; ?>>-- Loại kênh MT --</option>
	    				<option value='4' <?php echo $TypeChanel==4 ? "selected='selected'" : '' ; ?>>-- Loại kênh TD --</option>
	    				<option value='5' <?php echo $TypeChanel==5 ? "selected='selected'" : '' ; ?>>-- Loại kênh NB --</option>
	    			</select>
	    			<select style="height:32px;width:128px;border:1px solid #c1c1c1;margin-right:10px;margin-left:5px" id="system" onchange="setsearch(this)">
	    				<option value='0'>-- Tất cả hệ thống --</option>
	    				<?php 
	    				$SystemID = $this->session->userdata("report_filter_customer_SystemID");
	    				$system = $this->db->query("select * from ttp_report_system")->result();
	    				if(count($system) >0){
	    					foreach($system as $row){
	    						$selected = $row->ID==$SystemID ? "selected='selected'" : '' ;
	    						echo "<option value='$row->ID' $selected>$row->Title</option>";
	    					}
	    				}
	    				?>
	    				
	    			</select>
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>Kênh</th>
						<th>Hệ thống</th>
						<th>Khu vực</th>
						<th>Cửa hàng</th>
						<th>Địa chỉ</th>
						<th>Công nợ</th>
					</tr>
					<?php 
					$area = $this->db->query("select * from ttp_report_area")->result();
					$arr_area = array();
					if(count($area)>0){
						foreach($area as $row){
							$arr_area[$row->ID] = $row->Title;
						}
					}
					$system = $this->db->query("select * from ttp_report_system")->result();
					$arr_system = array();
					if(count($system)>0){
						foreach($system as $row){
							$arr_system[$row->ID] = $row->Title;
						}
					}
					$arr_chanel = array(0=>'Online',1=>'GT',2=>'MT');
					$arr_type = array(1=>'Đại lý',0=>'Nhà phân phối');
					if(count($data)>0){
						$i=$start;
						foreach($data as $row){
							$area = isset($arr_area[$row->AreaID]) ? $arr_area[$row->AreaID] : '--' ;
							$chanel = isset($arr_chanel[$row->Type]) ? $arr_chanel[$row->Type] : '--' ;
							$system = isset($arr_system[$row->SystemID]) ? $arr_system[$row->SystemID] : '--' ;
							$i++;
							echo "<tr>";
							echo "<td style='width:30px;text-align:center;background:#F7F7F7'><a href='{$base_link}edit_sales/$row->ID'>$i</a></td>";
							echo "<td style='width:80px'><a href='{$base_link}edit_sales/$row->ID'>$chanel</a></td>";
							echo "<td style='width:150px'><a href='{$base_link}edit_sales/$row->ID'>$system</a></td>";
							echo "<td style='width:150px'><a href='{$base_link}edit_sales/$row->ID'>$area</a></td>";
							echo "<td style='width:auto'><a href='{$base_link}edit_sales/$row->ID'>$row->Name</a></td>";
							echo "<td style='width:auto'><a href='{$base_link}edit_sales/$row->ID'>$row->Address</a></td>";
							echo "<td style='width:80px;text-align:right'><a href='{$base_link}edit_sales/$row->ID'>".number_format($row->Dept)."</a></td>";
							echo "</tr>";
						}
					}else{
						$keywords = $keywords!='' ? '"<b>'.$keywords.'</b>"' : $keywords ;
						echo "<tr><td colspan='7'>Không tìm thấy dữ liệu $keywords.</td></tr>";
					}
					?>
				</table>
				<?php if(count($data)>0) echo $nav; ?>
			</div>
		</div>
</div>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;}
	.body_content .containner table tr td:nth-child(5){max-width: 255px}
</style>
<script>
	function setsearch(ob){
		var Channel = $("#channel").val();
		var SystemID = $("#system").val();
		window.location = '<?php echo base_url().ADMINPATH."/report/import_customer/setsessionsearch/?TypeChanel=" ?>'+Channel+"&SystemID="+SystemID;
	}
</script>
