<?php

class Post_model extends CI_Model {

    var $tablename = 'ttp_post';

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getAllPost($params, $orderby = 'ID', $sort = 'DESC') {
        $categories_special = $this->db->query("select * from ttp_categories where Published=1 and Data like '%\"block\":\"customers\"%'")->row();
        $CategoriesID = $categories_special ? $categories_special->ID : 0;
        $this->db->select('*');
        if (!empty($params['keyword'])) {
            $this->db->like('Title', $params['keyword']);
            $this->db->or_like('Introtext', $params['keyword']);
        }
        if (!empty($params['post_special'])) {
            $this->db->Where('CategoriesID', $CategoriesID);
            $this->db->like('Data', "\"hot\":\"true\"");
            $this->db->limit(1, 0);
        } else
        if (!empty($params['post_4special'])) {
            $this->db->Where('CategoriesID', $CategoriesID);
            $this->db->like('Data', "\"hot\":\"true\"");
            $this->db->limit(4, 1);
        } else
        if (!empty($params['post_featured'])) {
            $this->db->Where('CategoriesID', $CategoriesID);
            $this->db->like('Data', "\"featured\":\"true\"");
            $this->db->limit(5, 0);
        } else
        if (!empty($params['random'])) {
             $this->db->order_by('rand()');
             $this->db->limit(4);
        }

        $this->db->where('Published', 1);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function getCountSearch($params) {
        $this->db->from($this->tablename);
        if (!empty($params['keyword'])) {
            $this->db->like('Title', $params['keyword']);
            $this->db->or_like('Introtext', $params['keyword']);
        }
        $this->db->where('Published', 1);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    function getPostCounterByDate($params = array()){
        $this->db->from($this->tablename);
        $this->db->where('MONTH(Created)',$params['month']);
        $this->db->where('YEAR(Created)',$params['year']);
        $this->db->where('Published', 1);
        if(!empty($params['Owner'])){
            $this->db->where('Owner', $params['Owner']);
        }
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    function defineAllPost($params = array()) {
        $post_data = $this->getAllPost($params);
        $res = array();
        foreach ($post_data as $key => $row) {
            $res[$key]['ID'] = isset($row->ID) ? $row->ID : '';
            $res[$key]['Title'] = isset($row->Title) ? $row->Title : '';
            $res[$key]['Description'] = isset($row->Description) ? $row->Description : '';
            $res[$key]['Content'] = isset($row->Introtext) ? $row->Introtext : '';
            $res[$key]['PostDate'] = isset($row->Created) ? date_format(date_create($row->Created), 'd/m/Y H:i') : '';
            $res[$key]['Url'] = isset($row->Alias) ? base_url() . 'cam-nang-auchan/' . $row->Alias . page_extension : '';
            $res[$key]['Thumb'] = !empty($row->Thumb) ? UPLOAD_URL . '/' . $row->Thumb : 'public/auchan/v2/images/no-image_post.jpg';
        }
        return $res;
    }

}
