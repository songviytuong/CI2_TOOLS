<div class="box_inner">
    <div class="block1_inner"><h1>Thông tin người giao nhận hàng hóa</h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    <div class="block2_inner">
		<?php 
			$sex = $info->Gender==0 ? "Nữ" : "Nam" ;
		    echo "<div class='col-xs-3 text-center'><img src='".$info->Thumb."' style='width:110px;height:110px;border-radius:50%;margin:20px 0px' /></div>";
		    echo "<div class='col-xs-4'>
		        <h4>$info->FirstName $info->LastName</h4>
		        <p>Giới tính : $sex</p>
		        <p>Số điện thoại : $info->Phone</p>
		        <p>Email : $info->Email</p>
		        <p>Cấp độ nhân viên : chuyên nghiệp</p>
		    </div>";
		    $month = date('m');
		    $year = date('Y');
		    $pending = $this->db->query("select DISTINCT OrderID from ttp_report_order_send_supplier where MONTH(TimeReciver)='$month' and YEAR(TimeReciver)='$year' and UserReciver=$id and Status=2")->result();
		    $pending = count($pending);
		    $success = $this->db->query("select DISTINCT OrderID from ttp_report_order_send_supplier where MONTH(TimeReciver)='$month' and YEAR(TimeReciver)='$year' and UserReciver=$id and Status=3")->result();
		    $success = count($success);
		    $false = $this->db->query("select DISTINCT OrderID from ttp_report_order_send_supplier where MONTH(TimeReciver)='$month' and YEAR(TimeReciver)='$year' and UserReciver=$id and Status=4")->result();
		    $false = count($false);
		    echo "<div class='col-xs-5'>
		        <h4>Thành tích trong tháng</h4>
		        <p><span style='float:left;width:200px;'>Đơn hàng giao thành công </span>: ".number_format($success)." đơn hàng</p>
		        <p><span style='float:left;width:200px;'>Đơn hàng hủy </span>: ".number_format($false)." đơn hàng</p>
		        <p><span style='float:left;width:200px;'>Số đơn hàng đang giao </span>: ".number_format($pending)." đơn hàng</p>
		        <p>Đánh giá : 
		            <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>
		            <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>
		            <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>
		            <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>
		            <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>
		        </p>
		    </div>";
		    echo "<div class='col-xs-12'><hr><h4>Đơn hàng đang chuẩn bị lấy</h4></div><div class='col-xs-12'>";
		    if($this->user->UserType!=4 && $this->user->IsAdmin==0){
		    	$order = $this->db->query("select b.MaDH,c.Title,c.BarcodeClient,a.Amount,c.Donvi from ttp_report_order_send_supplier a,ttp_report_order b,ttp_report_products c,ttp_report_branch d,ttp_user_supplier e where d.ID=a.BranchID and d.SupplierID=e.SupplierID and e.UserID=".$this->user->ID." and a.OrderID=b.ID and a.ProductsID=c.ID and a.Status=1 and a.UserReciver=".$id)->result();
		    }else{
		    	$order = $this->db->query("select b.MaDH,c.Title,c.BarcodeClient,a.Amount,c.Donvi from ttp_report_order_send_supplier a,ttp_report_order b,ttp_report_products c where a.OrderID=b.ID and a.ProductsID=c.ID and a.Status=1 and a.UserReciver=".$id)->result();
		    }
		    if(count($order)>0){
		    	echo "<table class='table'><tr><th>STT</th><th>Mã đơn hàng</th><th>Mã sản phẩm</th><th>Tên sản phẩm</th><th>DV</th><th class='text-right'>Số lượng</th></tr>";
		    	$i=1;
		    	foreach($order as $row){
		    		echo "<tr>";
		    		echo "<td style='width:30px'>$i</td>";
		    		echo "<td style='width:100px'>$row->MaDH</td>";
		    		echo "<td style='width:100px'>$row->BarcodeClient</td>";
		    		echo "<td style='width:250px'>$row->Title</td>";
		    		echo "<td style='width:50px'>$row->Donvi</td>";
		    		echo "<td style='width:80px;text-align:right'>$row->Amount</td>";
		    		echo "</tr>";
		    		$i++;
		    	}
		    	echo "</table>";
		    }
		    echo "</div>";
		?>
	</div>
</div>
<script>
	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});
</script>