$(document).ready(function () {
    
    var winHeight = $(window).height();
    $('.body_content').css('min-height', winHeight);
    /*
     *	Warning delete
     */
    $(".delete").click(function () {
        if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
            return false;
        }
    });

    $(".select_day_week_month a").click(function () {
        var data = $(this).attr("data");
        $(".select_day_week_month a").removeClass("active");
        $(this).addClass("active");
        $(".plotchart_item_primary").hide();
        $("#" + data).show();
    });

    $(".value_excute,#cancel_showcompare").click(function () {
        $(".box_dropdown").toggle();
    });

    $("#active_vs").click(function () {
        if (this.checked === true) {
            $("#reportrange1").show();
            $("#vs_selectbox").removeProp('disabled');
            $("#vs_selectbox").css({'opacity': '1'});
        } else {
            $("#reportrange1").hide();
            $("#vs_selectbox").prop("disabled", true);
            $("#vs_selectbox").css({'opacity': '0.6'});
        }
    });

    $("#close_message_warning").click(function () {
        $(".warning_message").slideUp('slow');
    });

    $("#vs_selectbox").change(function () {
        var data = $(this).val();
        if (data == 'lastmonth') {
            $('#reportrange1 span').html(moment().subtract(1, 'month').startOf('month').format('MMMM D, YYYY') + " - " + moment().subtract(1, 'month').endOf('month').format('MMMM D, YYYY'));
            $(".value_excute_vs span").html(moment().subtract(1, 'month').startOf('month').format('DD/MM/YYYY') + " - " + moment().subtract(1, 'month').endOf('month').format('DD/MM/YYYY'));
            $(".value_excute_vs").show();
        }
        if (data == 'last7day') {
            $('#reportrange1 span').html(moment().subtract(6, 'days').format('MMMM D, YYYY') + " - " + moment().format('MMMM D, YYYY'));
            $(".value_excute_vs span").html(moment().subtract(6, 'days').format('DD/MM/YYYY') + " - " + moment().format('DD/MM/YYYY'));
            $(".value_excute_vs").show();
        }
        if (data == 'last30day') {
            $('#reportrange1 span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + " - " + moment().format('MMMM D, YYYY'));
            $(".value_excute_vs span").html(moment().subtract(29, 'days').format('DD/MM/YYYY') + " - " + moment().format('DD/MM/YYYY'));
            $(".value_excute_vs").show();
        }
    });

    $("#showcompare").click(function () {
        $(".box_dropdown").toggle();
        $(".over_lay").fadeIn();
        var active_vs = $("#active_vs:checked").length > 0 ? 1 : 0;
        var data = $(".value_excute span").html();
        var data_vs = $(".value_excute_vs span").html();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink + "report/set_day",
            dataType: "html",
            type: "POST",
            data: "group1=" + data + '&active=' + active_vs + "&group2=" + data_vs,
            success: function (result) {
                if (result == "OK") {
                    location.reload();
                } else {
                    $(".over_lay").fadeOut();
                    $(".warning_message").slideDown('slow');
                }
                console.log(result);
            }
        });
    });

    $('button[type="submit"]').click(function () {
        var status_required = 0;
        $(".required").each(function () {
            var check = $(this).val();
            if (check == '') {
                status_required = 1;
                $(this).css({'border': '1px solid rgb(232, 111, 111)'});
            }
        });
        if (status_required == 1) {
            $(this).removeClass('saving');
            $(this).find('i').show();
        } else {
            $(this).find('i').hide();
            $(this).addClass("saving");
        }
    });

    var sitebar_open = "true";
    $("#closesitebar").click(function () {
        sitebar_open = "false";
        $(".sitebar").addClass("closesitebar");
        $(this).addClass("closesitebar");
        $("#opensitebar").addClass("opensitebar");
        $(".containner").addClass("opensitebar");
        $(".copyright").addClass("opensitebar");
        $(".warning_message").addClass("opensitebar");
    });

    $("#opensitebar").click(function () {
        sitebar_open = "true";
        $(".sitebar").removeClass("closesitebar");
        $(this).removeClass("opensitebar");
        $("#closesitebar").removeClass("closesitebar");
        $(".containner").removeClass("opensitebar");
        $(".copyright").removeClass("opensitebar");
        $(".warning_message").removeClass("opensitebar");
    });

    var status_edited = 0;
    $(".btn-activeedit").click(function () {
        if (status == 0) {
            $(".disableedit").hide();
            $(this).parent('.btn_group').find("a").hide();
            $(this).parent('.btn_group').find("button").hide();
            $(this).hide();
            $(this).attr('data', 1);
            $(".btn-update,.btn-cancel").show();
            status_edited = 1;
        } else {
            $(".disableedit").hide();
            $(this).parent('.btn_group').find("a").show();
            $(this).parent('.btn_group').find("button").show();
            $(this).hide();
            $(this).attr('data', 1);
            $(".btn-update").show();
            status_edited = 0;
        }
    });

});



var GPSTimeout = 10; //init global var NOTE: I noticed that 10 gives me the quickest result but play around with this number to your own liking
//function to be called where you want the location with the callback(position)
function getLocation(callback)
{
    if (navigator.geolocation)
    {
        var clickedTime = (new Date()).getTime(); //get the current time
        GPSTimeout = 10; //reset the timeout just in case you call it more then once
        ensurePosition(callback, clickedTime); //call recursive function to get position
    }
    return true;
}

//recursive position function
function ensurePosition(callback, timestamp)
{
    if (GPSTimeout < 6000)//set at what point you want to just give up
    {
        //call the geolocation function
        navigator.geolocation.getCurrentPosition(
                function (position) //on success
                {
                    //if the timestamp that is returned minus the time that was set when called is greater then 0 the position is up to date
                    if (position.timestamp - timestamp >= 0)
                    {
                        GPSTimeout = 10; //reset timeout just in case
                        callback(position); //call the callback function you created
                    } else //the gps that was returned is not current and needs to be refreshed
                    {
                        GPSTimeout += GPSTimeout; //increase the timeout by itself n*2
                        ensurePosition(callback, timestamp); //call itself to refresh
                    }
                },
                function () //error: gps failed so we will try again
                {
                    GPSTimeout += GPSTimeout; //increase the timeout by itself n*2
                    ensurePosition(callback, timestamp);//call itself to try again
                },
                {maximumAge: 0, timeout: GPSTimeout}
        )
    }
}


function show_table_report(ob, table) {
    var cla = $(ob).attr('class');
    $("." + cla).css({'color': '#27c'});
    $(ob).css({'color': '#555'});
    $(".table_report").hide();
    $("." + table).show();
}

var temp = "";

function showsitebar(ob) {
    temp = $(ob).parent('a').parent('li').parent('ul').parent('.block2').html();
    $(ob).parent('a').parent('li').parent('ul').parent('.block2').html("<label class='backtomainmenu-mobile' onclick='backtomainmenu(this)'><i class='fa fa-arrow-circle-left' aria-hidden='true'></i> Quay lại</label>" + $(".sitebar").html());
}

function backtomainmenu(ob) {
    $(ob).parent('.block2').html(temp);
}

/**
 * Function : dump()
 * Arguments: The data - array,hash(associative array),object
 *    The level - OPTIONAL
 * Returns  : The textual representation of the array.
 * This function was inspired by the print_r function of PHP.
 * This will accept some data as the argument and return a
 * text that will be a more readable version of the
 * array/hash/object that is given.
 * Docs: http://www.openjs.com/scripts/others/dump_function_php_print_r.php
 */
function dump(arr, level) {
    var dumped_text = "";
    if (!level)
        level = 0;

    var level_padding = "";
    for (var j = 0; j < level + 1; j++)
        level_padding += "    ";

    if (typeof (arr) == 'object') {
        for (var item in arr) {
            var value = arr[item];

            if (typeof (value) == 'object') {
                dumped_text += level_padding + "'" + item + "' ...\n";
                dumped_text += dump(value, level + 1);
            } else {
                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
            }
        }
    } else {
        dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
    }
    return dumped_text;
}