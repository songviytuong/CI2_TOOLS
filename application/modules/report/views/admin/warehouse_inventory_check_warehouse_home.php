<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách đợt kiểm kho</h1>
			</div>
			<div class="block2">
				<a href="<?php echo base_url().ADMINPATH.'/report/warehouse/create_check_warehouse' ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Tạo mới</a>
			</div>
		</div>
		<div class="table_data">
			<table class="table table-bordered">
				<tr>
					<th>STT</th>
					<th>Ngày kiểm kho</th>
					<th>Tiêu đề đợt kiểm kho</th>
					<th>Người khởi tạo</th>
					<th>Kho kiểm hàng</th>
					<th>Trạng thái</th>
					<th>Hành động</th>
				</tr>
				<?php
				$arr_status = array(0=>"Đang xử lý",1=>"Đã hoàn tất");
				if(count($data)>0){
					$i=1;
					$links = base_url().ADMINPATH.'/report/warehouse';
					foreach($data as $row){
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>".date('d/m/Y H:i:s',strtotime($row->Created))."</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->UserName</td>";
						echo "<td>$row->MaKho</td>";
						echo "<td>".$arr_status[$row->Status]."</td>";
						$str = $row->Status==0 ? " | <a class='delete' ref='$links/delete_check_warehouse/$row->ID'><i class='fa fa-times'></i> Delete</a>" : "" ;
						echo "<td><a href='$links/edit_check_warehouse/$row->ID'><i class='fa fa-pencil'></i> Edit</a>$str</td>";
						echo "</tr>";
						$i++;
					}
				}
				?>
			</table>
		</div>
	</div>
</div>