<div class="sub_menu height">
	<div class="find_record">
		<?php 
		$to = $start+$limit>$total ? $total : $start+$limit ;
		?>
		Tìm thấy <?php echo number_format($total) ?></b> phiếu chuyển hàng.
		<a href="<?php echo ADMINPATH.'/pos/pos_inventory_export/export_data' ?>"><i class="fa fa-download"></i> Xuất file excel toàn bộ tháng này</a> | <a href="<?php echo ADMINPATH.'/pos/pos_inventory_export/export_data/1' ?>"><i class="fa fa-download"></i> Xuất file excel ngày hôm nay</a>
		<a class="btn btn-danger pull-right" style="margin-right:20px" href="<?php echo base_url().ADMINPATH.'/pos/pos_inventory_export/create' ?>"><i class="fa fa-plus"></i> Thực hiện chuyển hàng</a>
	</div>
</div>
<div class="containner height">
	<div class="table_div">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã phiếu lưu chuyển</th>
				<th style="width:150px;">Kho chuyển</th>
				<th style="width:150px;">Ngày chuyển</th>
				<th style="width:150px;">Kho nhập</th>
				<th style="width:150px;">Ngày nhập</th>
				<th>Số loại sản phẩm</th>
				<th>Ghi chú</th>
				<th>Trạng thái</th>
			</tr>
			<tr>
				<?php 
				$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
				$arr_warehouse = array();
				if(count($warehouse)>0){
					foreach ($warehouse as $row) {
						$arr_warehouse[$row->ID] = $row->MaKho;
					}
				}
				$arr = array(
					4=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Thành công</a>',
					3=>'<a class="status-button"><i class="fa fa-circle text-warning" aria-hidden="true"></i> Chờ nhập</a>',
					1=>'<a class="status-button"><i class="fa fa-circle text-danger" aria-hidden="true"></i> Đã hủy</a>',
					0=>'<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Nháp</a>'
				);
				if(count($data)>0){
					$i=$start+1;
					foreach ($data as $row) {
						$row->Status = isset($arr[$row->Status]) ? $arr[$row->Status] : '--' ;
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$row->OrderCode.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$arr_warehouse[$row->WarehouseSender].'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$row->ExportDate.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$arr_warehouse[$row->WarehouseReciver].'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$row->ImportDate.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$row->TotalProducts.'</a></td>
						<td style="width:120px;"><a href="'.ADMINPATH.'/pos/pos_inventory_export/view_order/'.$row->ID.'">'.$row->Note.'</a></td>
						<td style="width:120px;">'.$row->Status.'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
		</table>
		<?php echo str_replace('href="#"','',$nav); ?>
	</div>
</div>