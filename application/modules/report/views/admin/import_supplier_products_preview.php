<?php 
	if(!isset($data)){
		if(isset($error) && $error!=''){
			echo "<div class='alert alert-danger text-center'>$error</div>";
		}
	}else{
		if(count($data)>0){
			$products = $this->db->query("select BarcodeClient,Title,RootPrice from ttp_report_products where SupplierID=$supplier")->result();
			$arr_products = array();
			if(count($products)>0){
				foreach($products as $row){
					$arr_products[$row->BarcodeClient] = 1;
				}
			}
			$total = count($products);
			$arr_upload_code = array();
			$notin = 0;
			$in = 0;
			foreach($data as $row){
				$row[0] = str_replace(' ','',$row[0]);
				$arr_upload_code[$row[0]] = 1;
				if(!isset($arr_products[$row[0]])){
					$notin++;
				}else{
					$in++;
				}
			}
			$notfound=0;
			if(count($arr_products)>0){
				foreach($arr_products as $key=>$value){
					if(!isset($arr_upload_code[$key])){
						$notfound++;
					}
				}
			}
	?>
			<div class="row">
		    	<div class="col-xs-1"><span class="icon_list" style="background: #fbf3cc;">2</span></div>
		    	<label class="col-xs-2 control-label">Kết quả xem trước</label>
		    	<div class="col-xs-4 specialcol4">
		    		<p>Bạn đã cập nhật <?php echo count($data) ?> sản phẩm</p>
					<p>- có <b><?php echo $in ?></b> / <b><?php echo count($data) ?></b> sản phẩm hiện có của UCC</p>
					<p>- và <b><?php echo $notin ?></b> sản phẩm có mã mới </p>
		    	</div>
		    	<div class="col-xs-4 specialcol4" style="float:right;">
		    		<p><b><?php echo $notfound ?></b> / <b><?php echo count($arr_products) ?></b> sản phẩm hiện có trong UCC nhưng không có trong DS bạn vừa cập nhật. Sẽ được hiểu là không có hàng hay không thay đổi về giá bán?</p>
		    		<a onclick="activeme(this,0)"><i class="fa fa-check-square" aria-hidden="true"></i> Không có hàng</a>
		    		<a onclick="activeme(this,1)"><i class="fa fa-check-square active" aria-hidden="true"></i> Không thay đổi giá bán</a>
		    	</div>
		    </div>
		    <div class="row">
		    	<div class="col-xs-1"><span class="icon_list" style='background: #ffd2d2;'>3</span></div>
		    	<label class="col-xs-11 control-label">
		    		<input type='checkbox' name="agree" checked='checked' style="margin-right:5px" /> Tôi đã kiểm tra kỹ các nội dung thông tin sản phẩm vừa cập nhật ở trên và đồng ý gửi cho UCC
		    	</label>
		    </div>
		    <div class="row">
		    	<div class="col-xs-1"></div>
		    	<div class="col-xs-11">
		    		<button class="btn btn-danger" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Gửi bảng cập nhật cho UCC</button>
		    		<a class="btn btn-default" onclick="closethisbox()"><i class="fa fa-times" aria-hidden="true"></i> Hủy bảng cập nhật</a>
		    	</div>
		    </div>
		    <input type="hidden" name="TotalProducts" value="<?php echo count($data) ?>" />
		    <input type="hidden" name="InUCC" value="<?php echo $in ?>" />
		    <input type="hidden" name="NotInUCC" value="<?php echo $notin ?>" />
		    <input type="hidden" name="RemainUCC" value="<?php echo $notfound ?>" />
		    <input type="hidden" id="RemainStatus" name="RemainStatus" value="1" />
	<?php 
			echo "
			<hr>
    		<h4>Danh sách sản phẩm xem trước</h4>
    		<table class='table table-bordered'>
			<tr>
				<th>STT</th>
				<th>Mã SP</th>
				<th>Tên sản phẩm NCC</th>
				<th>ĐVT</th>
				<th>Giá bán lẻ (bao gồm VAT)</th>
				<th>Giá bán theo đơn vị chuẩn</th>
				<th>Giá bán cho UCC</th>
				<th>Tình trạng</th>
			</tr>";
			$i=1;
			$datajson = array();
			foreach($data as $row){
				$td = "td" ;
				$first = "treach" ;
				if(!isset($arr_products[$row[0]])){
					$status_data = 1;
					$status = '<a class="text-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tạo mới</a>';
				}else{
					$status = '<a class="text-primary"><i class="fa fa-check-circle"></i> Cập nhật</a>';
					$status_data = 0;
				}
				$datajson[] = array(
					$row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$status_data
				);
				echo "<tr class='$first'>
					<$td>$i</$td>
					<$td class='masp_td'>".$row[0]."</$td>
					<$td class='title_td'>".$row[1]."</$td>
					<$td class='donvi_td'>".$row[2]."</$td>
					<$td class='price_td text-right'>".number_format($row[3])."</$td>
					<$td class='baseprice_td'>".$row[4]."</$td>
					<$td class='rootprice_td text-right'>".number_format($row[5])."</$td>
					<$td>$status</$td>
				</tr>";
				$i++;
			}
			echo "<input type='hidden' name='data' value='".json_encode($datajson)."' />";
			echo "</table>";
		}
	}
?>
<script>
	function activeme(ob,data){
		$(ob).parent('.specialcol4').find("i").removeClass('active');
		$(ob).find('i').addClass("active");
		$("#RemainStatus").val(data);
	}

	function closethisbox(){
		location.reload();
	}
</script>
    