$(".showmenu").click(function(){
	var data = $(this).attr('data');
	if(data=='out'){
		$(".primary_menu,.sub_menu,.containner,.hiddenmenu").addClass("in");
		$(this).attr("data","in");
	}else{
		$(this).attr("data","out");
		$(".primary_menu,.sub_menu,.containner,.hiddenmenu").removeClass("in");
	}
});

function disablescrollsetup(){
	$("body").css({'height':'auto','overflow-y':'scroll'});
}

function enablescrollsetup(){
	$(window).scrollTop(70);
	$("body").css({'height':'100%','overflow-y':'hidden'});
	h = window.innerHeight;
	h = h-200;
	$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
}


function sales_openbill(ob){
	if(order<8){
		order = order+1;
		stt = stt+1;
		$.post(baseurl+"pos_sales/draftorder",function(data,status){
			ordercurrent = data.order;
			$(".list-open-bill").find('a').removeClass('current');
			$(".list-open-bill").append('<a class="btn current control-bill-'+data.order+'"><b onclick="sales_open_order('+data.order+')">Đơn hàng '+stt+'</b><span onclick="sales_closebill('+data.order+')"><i class="fa fa-times"></i></span></a>');
			sales_open_order(ordercurrent);
        }, "json");
	}else{
		alert("Bạn chỉ được phép xử lý tối đa 8 đơn hàng cùng 1 lúc !");
	}
}

function sales_open_order(i){
	ordercurrent = i;
	$('.list-open-bill').find('.btn').removeClass("current");
	$('.list-open-bill').find('.control-bill-'+i).addClass("current");
	sales_load_bill_draft();
	sales_load_customers_draft();
}

function sales_closebill(i){
	var r = confirm("Bạn có chắc muốn hủy đơn hàng này !");
	if (r == true) {
		$.post(baseurl+"pos_sales/remove_draftorder",{order:i},function(data){
			order = order-1;
			$('.list-open-bill').find('.control-bill-'+i).remove();
			if(data>0){
				sales_open_order(data);
			}else{
				location.reload();
			}
	    },"text");
	}
}

function sales_closebill_not_confirm(i){
	$.post(baseurl+"pos_sales/remove_draftorder",{order:i},function(data){
		order = order-1;
		$('.list-open-bill').find('.control-bill-'+i).remove();
		if(data>0){
			sales_open_order(data);
			sales_load_customers_draft(data);
		}else{
			location.reload();
		}
    },"text");
}

function sales_closebill_button(){
	sales_closebill_not_confirm(ordercurrent)
}

var type_search = 0;

function sales_change_search_type(type){
	if(type==0){
		$("#dropdownMenu1").html('<i class="fa fa-search" aria-hidden="true"></i> Tìm tự động <span class="caret"></span>');
	}else{
		$("#dropdownMenu1").html('<i class="fa fa-barcode" aria-hidden="true"></i> Quét barcode <span class="caret"></span>');
	}
	type_search = type;
}

function sales_get_products_list(ob){
	var keywords = $(ob).val();
	if(type_search==0){
		if(keywords.length>2){
			$.post(baseurl+"pos_sales/get_products_list", {keywords: keywords}, function(result){
		        $(ob).parent('div').find("#auto_complete").html(result).removeClass('hidden');
		    });
		}else{
			$(ob).parent('div').find("#auto_complete").html('').addClass('hidden');
		}
	}else{
		$.post(baseurl+"pos_sales/get_products_by_barcode", {order:ordercurrent,keywords: keywords}, function(result){
			$("#keywords").val('');
	        if(result.error==0){
	        	sales_load_bill_draft();
	        }else{
	        	alert("Không tìm thấy sản phẩm có mã này !");
	        }
	    },"json");
	}
}

function sales_apply_products(id){
	$.post(baseurl+"pos_sales/add_products_draftorder", {order:ordercurrent,products:id}, function(result){
		sales_load_bill_draft();
		$("#auto_complete").addClass("hidden");
		$("#keywords").val('');
    },"text");
}

function sales_load_bill_draft(){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	$.post(baseurl+"pos_sales/load_bill_draft", {order:ordercurrent}, function(result){
		$(".containner .block1 .data_table #table_content").html(result);
    },"html");
}

function sales_load_customers_draft(){
	$.post(baseurl+"pos_sales/load_customers_draft", {order:ordercurrent}, function(result){
		$(".order_info #sales_bill_CustomerName").val(result.Name);
		$(".order_info #sales_bill_CustomerPayment").val(result.Payment);
		$(".order_info #sales_bill_CustomerPhone").val(result.Phone);
		$(".order_info #sales_bill_CustomerNote").val(result.Note);
		$(".order_info #sales_bill_TransferType").val(result.transport);
		if(result.id==0){
			document.getElementById('label-1').checked = true;
			$("#btn-view-history-order").addClass('hidden');
			$("#btn-view-history-order").attr("data-id",0);
		}else{
			document.getElementById('label-2').checked = true;
			$("#btn-view-history-order").removeClass('hidden');
			$("#btn-view-history-order").attr("data-id",result.id);
		}
		$("#sales_bill_Address").val(result.Address);
		$("#sales_bill_CityID").val(result.city);
		$("#sales_bill_DictrictID").val(result.district);
		$("#sales_bill_TransportID").val(result.transportid);
		$("#sales_bill_TransferID").val(result.transfer);
		if(result.Active==0){
			$("#btn_createorder").removeClass("hidden");
			$("#btn_printorder").addClass("hidden");
			$("#btn_closeorder").addClass("hidden");
			$(".no_action").addClass("hidden");
		}else{
			$(".no_action").removeClass("hidden");
			$("#btn_createorder").addClass("hidden");
			$("#btn_printorder").attr("href",baseurl+"pos_sales/print_order/"+result.Active);
			$("#btn_printorder").removeClass("hidden");
			$("#btn_closeorder").removeClass("hidden");
		}
		if(result.transport==0){
			$("#shiping_info").addClass('hidden');
		}else{
			$("#shiping_info").removeClass('hidden');
		}
    },"json");
}

function sales_reset_bill_draft(){
	var r = confirm("Bạn có chắc muốn làm lại đơn hàng này !");
	if (r == true) {
		$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
		$.post(baseurl+"pos_sales/reset_bill_draft", {order:ordercurrent}, function(result){
			sales_load_bill_draft();
	    },"html");
	}
}

function sales_add_reduce(ob){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	var reduce = $(ob).val();
	$.post(baseurl+"pos_sales/add_reduce_bill_draft", {order:ordercurrent,reduce:reduce}, function(result){
		sales_load_bill_draft();
    },"html");
}

function sales_drop_bill_draft(){
	sales_closebill(ordercurrent);
}

function sales_apply_name_bill(ob){
	var customertype = $('input[name=CustomerType]:checked').val();
	var name = $(ob).val();
	if(customertype==1){
		$.post(baseurl+"pos_sales/apply_name_draft", {order:ordercurrent,name:name}, function(result){},"html");
	}else{
		if(name!=''){
			$.post(baseurl+"pos_sales/get_customer_by_name_phone", {name:name}, function(result){
				if(result!=''){
	            	$(".over_lay .box_inner").css({'margin-top':'50px'});
	            	$(".over_lay .box_inner .block1_inner h1").html("Danh sách khách hàng");
	            	$(".over_lay .box_inner .block2_inner").html(result);
	            	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
	    			$(".over_lay").addClass('in');
	            }else{
	            	alert("Không tìm thấy dữ liệu theo yêu cầu.");
	            }
			},"html");
		}
	}
}

function sales_select_this_custom(ob,id){
	$(".over_lay").hide();
	disablescrollsetup();
	$.post(baseurl+"pos_sales/apply_customer_id", {order:ordercurrent,id:id}, function(result){
		if(result.error==0){
			$("#sales_bill_CustomerName").val(result.Name);
			$("#sales_bill_CustomerPhone").val(result.Phone);
			$("#sales_bill_Address").val(result.Address);
			$("#btn-view-history-order").attr('data-id',id).removeClass('hidden');
			$("#GroupID").val(result.GroupID);
			if(result.SourceID>0){
				$("#SourceID").val(result.SourceID);
			}
			if(result.KenhbanhangID>0){
				$("#KenhbanhangID").val(result.KenhbanhangID);
			}
		}else{
			alert("Khách hàng không tồn tại.");
		}
	},"json");
}

function sales_change_customer_type(ob){
	var customertype = $(ob).val();
	if(customertype==0){
		$.post(baseurl+"pos_sales/apply_customer_id", {order:ordercurrent,id:0}, function(result){
			$("#sales_bill_CustomerName").val('');
			$("#sales_bill_CustomerPhone").val('');
			$("#sales_bill_CustomerAddress").val('');
		},"json");
	}else{
		$.post(baseurl+"pos_sales/apply_customer_id", {order:ordercurrent,id:0}, function(result){
			$("#sales_bill_CustomerName").val('');
			$("#sales_bill_CustomerPhone").val('');
			$("#sales_bill_CustomerAddress").val('');
		},"json");
		$("#btn-view-history-order").attr('data-id',0).addClass('hidden');
	}
}

function sales_apply_phone_bill(ob){
	var customertype = $('input[name=CustomerType]:checked').val();
	var phone = $(ob).val();
	if(customertype==1){
		$.post(baseurl+"pos_sales/apply_phone_draft", {order:ordercurrent,phone:phone}, function(result){},"html");
	}else{
		if(phone!=''){
			$.post(baseurl+"pos_sales/get_customer_by_name_phone", {phone:phone}, function(result){
				if(result!=''){
	            	$(".over_lay .box_inner").css({'margin-top':'50px'});
	            	$(".over_lay .box_inner .block1_inner h1").html("Danh sách khách hàng");
	            	$(".over_lay .box_inner .block2_inner").html(result);
	            	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
	    			$(".over_lay").addClass('in');
	            }else{
	            	alert("Không tìm thấy dữ liệu theo yêu cầu.");
	            }
			},"html");
		}
	}
}

function sales_view_history_order(ob){
	var idcus = $(ob).attr('data-id');
	$.post(baseurl+"pos_sales/get_order_by_customer", {id:idcus}, function(result){
		if(result!=''){
        	$(".over_lay .box_inner").css({'margin-top':'50px'});
        	$(".over_lay .box_inner .block1_inner h1").html("Danh sách đơn hàng đã đặt");
        	$(".over_lay .box_inner .block2_inner").html(result);
        	$(".over_lay").removeClass('in');
	    	$(".over_lay").fadeIn('fast');
			$(".over_lay").addClass('in');
        }else{
        	alert("Không tìm thấy dữ liệu theo yêu cầu.");
        }
	},"html");
}

function sales_apply_address_bill(ob){
	var address = $(ob).val();
	$.post(baseurl+"pos_sales/apply_address_draft", {order:ordercurrent,address:address}, function(result){},"html");
}

function sales_apply_transfer_bill(ob){
	var user = $(ob).val();
	$.post(baseurl+"pos_sales/apply_user_draft", {order:ordercurrent,user:user}, function(result){},"html");
}

function sales_apply_transport_bill(ob){
	var transport = $(ob).val();
	$.post(baseurl+"pos_sales/apply_transportid_draft", {order:ordercurrent,transport:transport}, function(result){},"html");
}

function sales_remove_products(id){
	$.post(baseurl+"pos_sales/remove_products_draftorder", {order:ordercurrent,products:id}, function(result){
		sales_load_bill_draft();
	});
}

function sales_change_products(ob,id){
	var amount = $(ob).val();
	$.post(baseurl+"pos_sales/change_products_draftorder", {order:ordercurrent,products:id,amount:amount}, function(result){
		sales_load_bill_draft();

	});
}

function sales_change_price(ob,id){
	var price = $(ob).val();
	$.post(baseurl+"pos_sales/change_products_price_draftorder", {order:ordercurrent,products:id,price:price}, function(result){
		sales_load_bill_draft();
	});
}

function sales_add_chiphi(ob){
	var chiphi = $(ob).val();
	$.post(baseurl+"pos_sales/add_chiphi_draftorder", {order:ordercurrent,chiphi:chiphi}, function(result){
		sales_load_bill_draft();
	});
}

function sales_choose_transport(ob){
	var transport = $(ob).val();
	if(transport==1){
		$("#shiping_info").removeClass("hidden");
	}else{
		$("#shiping_info").addClass("hidden");
	}
	$.post(baseurl+"pos_sales/apply_transport_draft", {order:ordercurrent,transport:transport}, function(result){},"html");
}

function sales_apply_district_bill(ob){
	var district = $(ob).val();
	$.post(baseurl+"pos_sales/apply_district_draft", {order:ordercurrent,district:district}, function(result){
		sales_load_bill_draft();
	},"html");
}

function sales_save_change_order(ob,id){
	var name = $("#order_name").val();
	var phone = $("#order_phone").val();
	var city = $("#order_city").val();
	var district = $("#order_district").val();
	var address = $("#order_address").val();
	var transport = $("#order_transportid").val();
	var transfer = $("#order_transfer").val();
	var transporttype = $("#order_transporttype").val();
	var note = $("#order_note").val();
	$(ob).addClass("saving");
	$.post(baseurl+"pos_sales/save_change_order", {
		id:id,
		name:name,
		phone:phone,
		city:city,
		address:address,
		transport:transport,
		transfer:transfer,
		transporttype:transporttype,
		district:district,
		note:note
	}, function(result){
		$(ob).removeClass("saving");
		if(result.error==0){
			$(".alert").removeClass("alert-danger").addClass("alert-success").removeClass('hidden').html(result.message);
		}else{
			$(".alert").removeClass("alert-success").addClass("alert-danger").removeClass('hidden').html(result.message);
		}
	},"json");
}

function sales_save_change_discount(ob,discount,id){
	var t = prompt("Điền giá trị chiết khấu cần thay đổi", discount);
	if (t != null) {
		$.post(baseurl+"pos_sales/save_change_discount_order", {order:id,discount:t},function(result){
			sales_save_change_details_order(id);
		},"html");
	}
}

function sales_save_change_price(ob,price,row,id){
	var t = prompt("Điền đơn giá cần thay đổi", price);
	if (t != null) {
		if(t!=price){
			$.post(baseurl+"pos_sales/save_change_price_order", {order:id,id:row,price:t},function(result){
				sales_save_change_details_order(id);
			},"html");
		}
	}
}

function sales_save_change_amount(ob,amount,row,id){
	var t = prompt("Điền số lượng cần thay đổi", amount);
	if (t != null) {
		if(t!=amount){
			$.post(baseurl+"pos_sales/save_change_amount_order", {order:id,id:row,amount:t},function(result){
				sales_save_change_details_order(id);
			},"html");
		}
	}
}

function sales_save_change_chiphi(ob,chiphi,id){
	var t = prompt("Điền chi phí cần thay đổi", chiphi);
	if (t != null) {
		$.post(baseurl+"pos_sales/save_change_chiphi_order", {order:id,chiphi:t},function(result){
			sales_save_change_details_order(id);
		},"html");
	}
}

function sales_save_change_details_order(id){
	$.post(baseurl+"pos_sales/load_details_order", {order:id},function(result){
		$(".table_div").html(result);
	},"html");
}

function sales_hidden_transport(ob){
	var data = $(ob).val();
	if(data==0){
		$(".hidden-transport").addClass("hidden");
	}else{
		$(".hidden-transport").removeClass("hidden");
	}
}

function sales_change_branch(ob,id){
	var text = $(ob).html();
	$("#dropdownMenu2").html('<i class="fa fa-map-marker" aria-hidden="true"></i> Tại kho '+text+' <span class="caret"></span>');
	$.post(baseurl+"pos_sales/change_branch", {id:id}, function(result){
		sales_load_bill_draft();
	},"html");
}

function sales_create_order(ob){
	var chiphi = $("#feeship").val();
	var KenhbanhangID = $("#KenhbanhangID").val();
	var SourceID = $("#SourceID").val();
	var GroupID = $("#GroupID").val();
	var name = $("#sales_bill_CustomerName").val();
	var phone = $("#sales_bill_CustomerPhone").val();
	var payment = $("#sales_bill_CustomerPayment").val();
	var note = $("#sales_bill_CustomerNote").val();
	var transporttype = $("#sales_bill_TransferType").val();
	if(transporttype==1){
		var address = $("#sales_bill_Address").val();
		if(address==''){
			alert("Vui lòng điền địa chỉ giao hàng !");
			$("#sales_bill_Address").focus();
			return;
		}
	}
	if(name==''){
		alert("Vui lòng điền tên khách hàng !");
		$("#sales_bill_CustomerName").focus();
		return;
	}
	$(".fade_excuting").removeClass("hidden");
	$.post(baseurl+"pos_sales/create_order", {order:ordercurrent,name:name,phone:phone,payment:payment,note:note,chiphi:chiphi,SourceID:SourceID,KenhbanhangID:KenhbanhangID,GroupID:GroupID,address:address}, function(result){
		if(result.error==0){
			$(ob).parent("div").find('a').removeClass('hidden');
			$(ob).parent("div").find('a.print_button').attr("href",baseurl+"pos_sales/print_order/"+result.id);
			$(ob).addClass('hidden');
			$(".no_action").removeClass("hidden");
		}	
		alert(result.message);
		$(".fade_excuting").addClass("hidden");
	},"json");
}

function sales_active_managermoney(ob,id,state){
	$.post(baseurl+"pos_sales/active_managermoney", {order:id,state:state}, function(result){
		$(ob).html(result);
	},'html');
}

function sales_active_financemoney(ob,id,state){
	$.post(baseurl+"pos_sales/active_financemoney", {order:id,state:state}, function(result){
		$(ob).html(result);
	},'html');
}

/* =============== Check inventory ============ */
/*
*	All function check inventory controllers
*/

function inventory_get_products_list(ob){
	var keywords = $(ob).val();
	if(type_search==0){
		if(keywords.length>2){
			$.post(baseurl+"pos_inventory/get_products_list", {keywords: keywords}, function(result){
		        $(ob).parent('div').find("#auto_complete").html(result).removeClass('hidden');
		    });
		}else{
			$(ob).parent('div').find("#auto_complete").html('').addClass('hidden');
		}
	}else{
		$.post(baseurl+"pos_inventory/get_products_by_barcode", {order:ordercurrent,keywords: keywords}, function(result){
			$("#keywords").val('');
	        if(result.error==0){
	        	sales_load_bill_draft();
	        }else{
	        	alert("Không tìm thấy sản phẩm có mã này !");
	        }
	    },"json");
	}
}

function inventory_apply_products(id){
	$.post(baseurl+"pos_inventory/add_products_draftorder", {order:ordercurrent,products:id}, function(result){
		inventory_load_bill_draft();
		$("#auto_complete").addClass("hidden");
		$("#keywords").val('');
    },"text");
}

function inventory_load_bill_draft(){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	$.post(baseurl+"pos_inventory/load_bill_draft", {order:ordercurrent}, function(result){
		$(".containner .block1 .data_table #table_content").html(result);
    },"html");
}

function inventory_change_products(ob,id){
	var amount = $(ob).val();
	$.post(baseurl+"pos_inventory/change_products_draftorder", {order:ordercurrent,products:id,amount:amount}, function(result){
		inventory_load_bill_draft();

	});
}

function inventory_remove_products(id){
	$.post(baseurl+"pos_inventory/remove_products_draftorder", {order:ordercurrent,products:id}, function(result){
		inventory_load_bill_draft();
	});
}

function inventory_closebill(i){
	var r = confirm("Bạn có chắc muốn hủy phiếu kiểm này hàng !");
	if (r == true) {
		$.post(baseurl+"pos_inventory/remove_draftorder",{order:i},function(data){
			order = order-1;
			$('.list-open-bill').find('.control-bill-'+i).remove();
			if(data>0){
				inventory_open_order(data);
			}else{
				location.reload();
			}
	    },"text");
	}
}

function inventory_open_order(i){
	ordercurrent = i;
	$('.list-open-bill').find('.btn').removeClass("current");
	$('.list-open-bill').find('.control-bill-'+i).addClass("current");
	inventory_load_bill_draft();
	inventory_load_customers_draft();
}

function inventory_reset_bill_draft(){
	var r = confirm("Bạn có chắc muốn làm lại đơn hàng này !");
	if (r == true) {
		$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
		$.post(baseurl+"pos_inventory/reset_bill_draft", {order:ordercurrent}, function(result){
			sales_load_bill_draft();
	    },"html");
	}
}

function inventory_load_customers_draft(){
	$.post(baseurl+"pos_inventory/load_customers_draft", {order:ordercurrent}, function(result){
		$(".order_info #inventory_bill_Type").val(result.Type);
		$(".order_info #inventory_bill_WarehouseID").val(result.WarehouseID);
		$(".order_info #inventory_bill_CustomerNote").val(result.Title);
		if(result.Active==0){
			$("#btn_createorder").removeClass("hidden");
			$("#btn_printorder").addClass("hidden");
			$("#btn_closeorder").addClass("hidden");
			$(".no_action").addClass("hidden");
		}else{
			$(".no_action").removeClass("hidden");
			$("#btn_createorder").addClass("hidden");
			$("#btn_printorder").attr("href",baseurl+"pos_inventory/print_order/"+result.Active);
			$("#btn_printorder").removeClass("hidden");
			$("#btn_closeorder").removeClass("hidden");
		}
		if(result.transport==0){
			$("#shiping_info").addClass('hidden');
		}else{
			$("#shiping_info").removeClass('hidden');
		}
    },"json");
}

function inventory_drop_bill_draft(){
	inventory_closebill(ordercurrent);
}

function inventory_openbill(ob){
	if(order<8){
		order = order+1;
		stt = stt+1;
		$.post(baseurl+"pos_inventory/draftorder",function(data,status){
			ordercurrent = data.order;
			$(".list-open-bill").find('a').removeClass('current');
			$(".list-open-bill").append('<a class="btn current control-bill-'+data.order+'"><b onclick="inventory_open_order('+data.order+')">Phiếu kiểm '+stt+'</b><span onclick="inventory_closebill('+data.order+')"><i class="fa fa-times"></i></span></a>');
			inventory_open_order(ordercurrent);
        }, "json");
	}else{
		alert("Bạn chỉ được phép xử lý tối đa 8 đơn hàng cùng 1 lúc !");
	}
}

function inventory_create_order(ob){
	var title = $("#inventory_bill_CustomerNote").val();
	var warehouse = $("#inventory_bill_WarehouseID").val();
	var typeinvent = $("#inventory_bill_Type").val();
	if(title==''){
		alert("Vui lòng nội dung kiểm hàng !");
		$("#inventory_bill_CustomerNote").focus();
		return;
	}
	$(".fade_excuting").removeClass("hidden");
	$.post(baseurl+"pos_inventory/create_order", {order:ordercurrent,title:title,warehouse:warehouse,typeinvent:typeinvent}, function(result){
		if(result.error==0){
			$(ob).parent("div").find('a').removeClass('hidden');
			$(ob).parent("div").find('a.print_button').attr("href",baseurl+"pos_inventory/print_order/"+result.id);
			$(ob).addClass('hidden');
			$(".no_action").removeClass("hidden");
		}	
		alert(result.message);
		$(".fade_excuting").addClass("hidden");
	},"json");
}

function inventory_closebill_button(){
	inventory_closebill_not_confirm(ordercurrent)
}

function inventory_closebill_not_confirm(i){
	$.post(baseurl+"pos_inventory/remove_draftorder",{order:i},function(data){
		order = order-1;
		$('.list-open-bill').find('.control-bill-'+i).remove();
		if(data>0){
			inventory_open_order(data);
			inventory_load_customers_draft(data);
		}else{
			location.reload();
		}
    },"text");
}

/* =============== Purchase Order ============ */
/*
*	All function purchase order controllers
*/

function purchase_order_openbill(ob){
	if(order<6){
		order = order+1;
		stt = stt+1;
		$.post(baseurl+"pos_inventory_import/draftorder",function(data,status){
			ordercurrent = data.order;
			$(".list-open-bill").find('a').removeClass('current');
			$(".list-open-bill").append('<a class="btn current control-bill-'+data.order+'"><b onclick="purchase_order_open_order('+data.order+')">Phiếu nhập hàng '+stt+'</b><span onclick="purchase_order_closebill('+data.order+')"><i class="fa fa-times"></i></span></a>');
			purchase_order_open_order(ordercurrent);
        }, "json");
	}else{
		alert("Bạn chỉ được phép xử lý tối đa 6 đơn hàng cùng 1 lúc !");
	}
}

function purchase_order_open_order(i){
	ordercurrent = i;
	$('.list-open-bill').find('.btn').removeClass("current");
	$('.list-open-bill').find('.control-bill-'+i).addClass("current");
	purchase_order_load_bill_draft();
	purchase_order_load_customers_draft();
}

function purchase_order_closebill(i){
	var r = confirm("Bạn có chắc muốn hủy đơn hàng này !");
	if (r == true) {
		$.post(baseurl+"pos_inventory_import/remove_draftorder",{order:i},function(data){
			order = order-1;
			$('.list-open-bill').find('.control-bill-'+i).remove();
			if(data>0){
				ordercurrent = data;
				purchase_order_open_order(data);
			}else{
				location.reload();
			}
	    },"text");
	}
}

function purchase_order_closebill_not_confirm(i){
	$.post(baseurl+"pos_inventory_import/remove_draftorder",{order:i},function(data){
		order = order-1;
		$('.list-open-bill').find('.control-bill-'+i).remove();
		if(data>0){
			purchase_order_open_order(data);
		}else{
			location.reload();
		}
    },"text");
}

function purchase_order_closebill_button(){
	purchase_order_closebill_not_confirm(ordercurrent)
}

function purchase_order_change_search_type(type){
	if(type==0){
		$("#dropdownMenu1").html('<i class="fa fa-search" aria-hidden="true"></i> Tìm tự động <span class="caret"></span>');
	}else{
		$("#dropdownMenu1").html('<i class="fa fa-barcode" aria-hidden="true"></i> Quét barcode <span class="caret"></span>');
	}
	type_search = type;
}

function purchase_order_get_products_list(ob){
	var keywords = $(ob).val();
	if(type_search==0){
		if(keywords.length>=2){
			$.post(baseurl+"pos_inventory_import/get_products_list", {keywords: keywords}, function(result){
		        $(ob).parent('div').find("#auto_complete").html(result).removeClass('hidden');
		    });
		}else{
			$(ob).parent('div').find("#auto_complete").html('').addClass('hidden');
		}
	}else{
		$.post(baseurl+"pos_inventory_import/get_products_by_barcode", {order:ordercurrent,keywords: keywords}, function(result){
			$("#keywords").val('');
	        if(result.error==0){
	        	purchase_order_load_bill_draft();
	        }else{
	        	alert("Không tìm thấy sản phẩm có mã này !");
	        }
	    },"json");
	}
}

function purchase_order_apply_products(id){
	$.post(baseurl+"pos_inventory_import/add_products_draftorder", {order:ordercurrent,products:id}, function(result){
		purchase_order_load_bill_draft();
		$("#auto_complete").addClass("hidden");
		$("#keywords").val('');
    },"text");
}

function purchase_order_load_bill_draft(){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	$.post(baseurl+"pos_inventory_import/load_bill_draft", {order:ordercurrent}, function(result){
		$(".containner .block1 .data_table #table_content").html(result);
    },"html");
}

function purchase_order_load_customers_draft(){
	$.post(baseurl+"pos_inventory_import/load_customers_draft", {order:ordercurrent}, function(result){
		$(".order_info #purchase_order_bill_Code").val(result.Number);
		$(".order_info #purchase_order_bill_WarehouseID").val(result.WarehouseID);
		$(".order_info #ProductionID").val(result.Production);
		$(".order_info #purchase_order_bill_CustomerNote").val(result.Note);
		if(result.Active==0){
			$("#btn_createorder").removeClass("hidden");
			$("#btn_printorder").addClass("hidden");
			$("#btn_closeorder").addClass("hidden");
		}else{
			$("#btn_createorder").addClass("hidden");
			$("#btn_printorder").attr("href",baseurl+"pos_inventory/print_order/"+result.Active);
			$("#btn_printorder").removeClass("hidden");
			$("#btn_closeorder").removeClass("hidden");
		}
    },"json");
}

function purchase_order_reset_bill_draft(){
	var r = confirm("Bạn có chắc muốn làm lại phiếu nhập này !");
	if (r == true) {
		$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
		$.post(baseurl+"pos_inventory_import/reset_bill_draft", {order:ordercurrent}, function(result){
			purchase_order_load_bill_draft();
	    },"html");
	}
}

function purchase_order_add_reduce(ob){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	var reduce = $(ob).val();
	$.post(baseurl+"pos_inventory_import/add_reduce_bill_draft", {order:ordercurrent,reduce:reduce}, function(result){
		purchase_order_load_bill_draft();
    },"html");
}

function purchase_order_drop_bill_draft(){
	purchase_order_closebill(ordercurrent);
}

function purchase_order_apply_code_bill(ob){
	var code = $(ob).val();
	$.post(baseurl+"pos_inventory_import/apply_code_draft", {order:ordercurrent,code:code}, function(result){},"html");
}

function purchase_order_apply_warehouse(ob){
	var warehouse = $(ob).val();
	$.post(baseurl+"pos_inventory_import/apply_warehouse_draft", {order:ordercurrent,warehouse:warehouse}, function(result){
		purchase_order_load_bill_draft();
	},"html");
}

function purchase_order_apply_production_bill(ob){
	var production = $(ob).val();
	$.post(baseurl+"pos_inventory_import/apply_production_draft", {order:ordercurrent,production:production}, function(result){},"html");
}

function purchase_order_apply_note_bill(ob){
	var note = $(ob).val();
	$.post(baseurl+"pos_inventory_import/apply_note_draft", {order:ordercurrent,note:note}, function(result){},"html");
}

function purchase_order_remove_products(id){
	$.post(baseurl+"pos_inventory_import/remove_products_draftorder", {order:ordercurrent,products:id}, function(result){
		purchase_order_load_bill_draft();
	});
}

function purchase_order_change_products(ob,id){
	var amount = $(ob).val();
	$.post(baseurl+"pos_inventory_import/change_products_draftorder", {order:ordercurrent,products:id,amount:amount}, function(result){
		purchase_order_load_bill_draft();

	});
}

function purchase_order_change_price(ob,id){
	var price = $(ob).val();
	$.post(baseurl+"pos_inventory_import/change_products_price_draftorder", {order:ordercurrent,products:id,price:price}, function(result){
		purchase_order_load_bill_draft();
	});
}

function purchase_order_create_order(ob){
	var code = $("#purchase_order_bill_Code").val();
	var warehouse = $("#purchase_order_bill_WarehouseID").val();
	var production = $("#ProductionID").val();
	var note = $("#purchase_order_bill_CustomerNote").val();
	$(".fade_excuting").removeClass("hidden");
	$.post(baseurl+"pos_inventory_import/create_order", {order:ordercurrent,code:code,warehouse:warehouse,production:production,note:note}, function(result){
		if(result.error==0){
			$(ob).parent("div").find('a').removeClass('hidden');
			$(ob).parent("div").find('a.print_button').attr("href",baseurl+"pos_inventory_import/print_order/"+result.id);
			$(ob).remove();
		}	
		alert(result.message);
		$(".fade_excuting").addClass("hidden");
	},"json");
}



/* =============== Transfer warehouse ============ */
/*
*	All function transfer warehouse controllers
*/

function transfer_warehouse_openbill(ob){
	if(order<6){
		order = order+1;
		stt = stt+1;
		$.post(baseurl+"pos_inventory_export/draftorder",function(data,status){
			ordercurrent = data.order;
			$(".list-open-bill").find('a').removeClass('current');
			$(".list-open-bill").append('<a class="btn current control-bill-'+data.order+'"><b onclick="transfer_warehouse_open_order('+data.order+')">Phiếu nhập hàng '+stt+'</b><span onclick="transfer_warehouse_closebill('+data.order+')"><i class="fa fa-times"></i></span></a>');
			transfer_warehouse_open_order(ordercurrent);
        }, "json");
	}else{
		alert("Bạn chỉ được phép xử lý tối đa 6 đơn hàng cùng 1 lúc !");
	}
}

function transfer_warehouse_open_order(i){
	ordercurrent = i;
	$('.list-open-bill').find('.btn').removeClass("current");
	$('.list-open-bill').find('.control-bill-'+i).addClass("current");
	transfer_warehouse_load_bill_draft();
	transfer_warehouse_load_customers_draft();
}

function transfer_warehouse_closebill(i){
	var r = confirm("Bạn có chắc muốn đóng đơn hàng này !");
	if (r == true) {
		$.post(baseurl+"pos_inventory_export/remove_draftorder",{order:i},function(data){
			order = order-1;
			$('.list-open-bill').find('.control-bill-'+i).remove();
			if(data>0){
				ordercurrent = data;
				transfer_warehouse_open_order(data);
			}else{
				location.reload();
			}
	    },"text");
	}
}

function transfer_warehouse_closebill_not_confirm(i){
	$.post(baseurl+"pos_inventory_export/remove_draftorder",{order:i},function(data){
		order = order-1;
		$('.list-open-bill').find('.control-bill-'+i).remove();
		if(data>0){
			transfer_warehouse_open_order(data);
		}else{
			location.reload();
		}
    },"text");
}

function transfer_warehouse_closebill_button(){
	transfer_warehouse_closebill_not_confirm(ordercurrent)
}

function transfer_warehouse_change_search_type(type){
	if(type==0){
		$("#dropdownMenu1").html('<i class="fa fa-search" aria-hidden="true"></i> Tìm tự động <span class="caret"></span>');
	}else{
		$("#dropdownMenu1").html('<i class="fa fa-barcode" aria-hidden="true"></i> Quét barcode <span class="caret"></span>');
	}
	type_search = type;
}

function transfer_warehouse_get_products_list(ob){
	var keywords = $(ob).val();
	if(type_search==0){
		if(keywords.length>=2){
			$.post(baseurl+"pos_inventory_export/get_products_list", {keywords: keywords}, function(result){
		        $(ob).parent('div').find("#auto_complete").html(result).removeClass('hidden');
		    });
		}else{
			$(ob).parent('div').find("#auto_complete").html('').addClass('hidden');
		}
	}else{
		$.post(baseurl+"pos_inventory_export/get_products_by_barcode", {order:ordercurrent,keywords: keywords}, function(result){
			$("#keywords").val('');
	        if(result.error==0){
	        	transfer_warehouse_load_bill_draft();
	        }else{
	        	alert("Không tìm thấy sản phẩm có mã này !");
	        }
	    },"json");
	}
}

function transfer_warehouse_apply_products(id){
	$.post(baseurl+"pos_inventory_export/add_products_draftorder", {order:ordercurrent,products:id}, function(result){
		transfer_warehouse_load_bill_draft();
		$("#auto_complete").addClass("hidden");
		$("#keywords").val('');
    },"text");
}

function transfer_warehouse_load_bill_draft(){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	$.post(baseurl+"pos_inventory_export/load_bill_draft", {order:ordercurrent}, function(result){
		$(".containner .block1 .data_table #table_content").html(result);
    },"html");
}

function transfer_warehouse_load_customers_draft(){
	$.post(baseurl+"pos_inventory_export/load_customers_draft", {order:ordercurrent}, function(result){
		$(".order_info #transfer_warehouse_bill_Code").val(result.Number);
		$(".order_info #transfer_warehouse_bill_WarehouseID").val(result.WarehouseID);
		$(".order_info #ProductionID").val(result.Production);
		$(".order_info #transfer_warehouse_bill_CustomerNote").val(result.Note);
		if(result.Active==0){
			$("#btn_createorder").removeClass("hidden");
			$("#btn_printorder").addClass("hidden");
			$("#btn_closeorder").addClass("hidden");
		}else{
			$("#btn_createorder").addClass("hidden");
			$("#btn_printorder").attr("href",baseurl+"pos_inventory/print_order/"+result.Active);
			$("#btn_printorder").removeClass("hidden");
			$("#btn_closeorder").removeClass("hidden");
		}
    },"json");
}

function transfer_warehouse_reset_bill_draft(){
	var r = confirm("Bạn có chắc muốn làm lại phiếu nhập này !");
	if (r == true) {
		$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
		$.post(baseurl+"pos_inventory_export/reset_bill_draft", {order:ordercurrent}, function(result){
			transfer_warehouse_load_bill_draft();
	    },"html");
	}
}

function transfer_warehouse_add_reduce(ob){
	$(".containner .block1 .data_table #table_content").html("<span class='loading'></span>");
	var reduce = $(ob).val();
	$.post(baseurl+"pos_inventory_export/add_reduce_bill_draft", {order:ordercurrent,reduce:reduce}, function(result){
		transfer_warehouse_load_bill_draft();
    },"html");
}

function transfer_warehouse_drop_bill_draft(){
	transfer_warehouse_closebill(ordercurrent);
}

function transfer_warehouse_apply_warehousesender(ob){
	var warehouse = $(ob).val();
	var reciver = $("#transfer_warehouse_bill_WarehouseReciver").val();
	if(warehouse==reciver){
		alert("Kho xuất và kho nhận không được trùng nhau !");
		return;
	}
	$.post(baseurl+"pos_inventory_export/apply_warehousesender_draft", {order:ordercurrent,warehouse:warehouse}, function(result){
		transfer_warehouse_load_bill_draft();
	},"html");
}

function transfer_warehouse_apply_warehousereciver(ob){
	var warehouse = $(ob).val();
	$.post(baseurl+"pos_inventory_export/apply_warehousereciver_draft", {order:ordercurrent,warehouse:warehouse}, function(result){},"html");
}

function transfer_warehouse_apply_note_bill(ob){
	var note = $(ob).val();
	$.post(baseurl+"pos_inventory_export/apply_note_draft", {order:ordercurrent,note:note}, function(result){},"html");
}

function transfer_warehouse_remove_products(id){
	$.post(baseurl+"pos_inventory_export/remove_products_draftorder", {order:ordercurrent,products:id}, function(result){
		transfer_warehouse_load_bill_draft();
	});
}

function transfer_warehouse_change_products(ob,id){
	var amount = $(ob).val();
	$.post(baseurl+"pos_inventory_export/change_products_draftorder", {order:ordercurrent,products:id,amount:amount}, function(result){
		transfer_warehouse_load_bill_draft();
	});
}

function transfer_warehouse_change_price(ob,id){
	var price = $(ob).val();
	$.post(baseurl+"pos_inventory_export/change_products_price_draftorder", {order:ordercurrent,products:id,price:price}, function(result){
		transfer_warehouse_load_bill_draft();
	});
}

function transfer_warehouse_change_import_products(ob,id){
	var amount = $(ob).val();
	$.post(baseurl+"pos_inventory_export/change_products_import", {amount:amount,id:id}, function(result){
		if(result.warning==1){
			$(ob).parent('td').find('i').removeClass('hidden');
		}else{
			$(ob).parent('td').find('i').addClass('hidden');
		}
	},'json');
}

function transfer_warehouse_drop_import(ob,id){
	$(ob).addClass('saving');
	$.post(baseurl+"pos_inventory_export/drop_products_import", {id:id}, function(result){
		if(result.error==0){
			location.reload();
		}else{
			alert(result.message);
		}
	},'json');
}

function transfer_warehouse_accept_import(ob,id){
	$(ob).addClass('saving');
	$.post(baseurl+"pos_inventory_export/accept_products_import", {id:id}, function(result){
		if(result.error==0){
			location.reload();
		}else{
			alert(result.message);
		}
	},'json');
}

function transfer_warehouse_create_order(ob){
	var WarehouseSender = $("#transfer_warehouse_bill_WarehouseSender").val();
	var WarehouseReciver = $("#transfer_warehouse_bill_WarehouseReciver").val();
	var note = $("#transfer_warehouse_bill_CustomerNote").val();
	$(".fade_excuting").removeClass("hidden");
	$.post(baseurl+"pos_inventory_export/create_order", {order:ordercurrent,WarehouseSender:WarehouseSender,WarehouseReciver:WarehouseReciver,note:note}, function(result){
		if(result.error==0){
			$(ob).parent("div").find('a').removeClass('hidden');
			$(ob).parent("div").find('a.print_button').attr("href",baseurl+"pos_inventory_export/print_order/"+result.id);
			$(ob).remove();
			alert(result.message);
			window.location = baseurl+"pos_inventory_export";
		}else{
			alert(result.message);
		}
		$(".fade_excuting").addClass("hidden");
	},"json");
}
