<div class="sub_menu">
	<div class="list-open-bill pull-left">
		<?php 
		$files = scandir('log/pos/inventory');
		$first = '';
		if(count($files)>0){
			$i=1;
			foreach($files as $row){
				if($row!='.' && $row!='..'){
					$name = explode('_',$row);
					$name = str_replace('.txt','',$name[2]);
					if($i==1){
						$first = $name;
					}
					$current = $i==1 ? 'current' : '' ;
					echo '<a class="btn control-bill-'.$name.' '.$current.'"><b onclick="inventory_open_order('.$name.')">Phiếu kiểm '.$i.'</b> <span onclick="inventory_closebill('.$name.')"><i class="fa fa-times"></i></span></a>';
					$i++;
				}
			}
			echo '<script>
					var stt = '.number_format($i-1).';
					var order = '.number_format($i-1).';
					var ordercurrent = '.$first.';
				</script>';
		}
		?>
	</div>
	<a onclick="inventory_openbill(this)" class="btn"><b><i class="fa fa-plus"></i></b></a>
</div>
<div class="containner">
	<div class="block1">
		<div class="data_table">
			<div class="tools">
				<div class="row">
					<div class="col-xs-4" style="position:relative;">
						<input type="text" class="form-control" placeholder="Nhập tên sản phẩm cần tìm ..." onchange="inventory_get_products_list(this)" id="keywords" autocomplete="off" />
						<div id="auto_complete" class='hidden auto_complete'></div>
					</div>
					<div class="col-xs-4">
						<div class="dropdown">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    <i class="fa fa-search" aria-hidden="true"></i> Tìm tự động
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						    <li><a onclick="sales_change_search_type(0)"><i class="fa fa-search" aria-hidden="true"></i> Tìm tự động</a></li>
						    <li><a onclick="sales_change_search_type(1)"><i class="fa fa-barcode" aria-hidden="true"></i> Quét Barcode</a></li>
						  </ul>
						</div>
					</div>
					<div class="col-xs-4 text-right">
						<a onclick="inventory_reset_bill_draft()" class="btn btn-default"><i class="fa fa-reply-all" aria-hidden="true"></i> Làm lại</a>
						<a onclick="inventory_drop_bill_draft()" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hủy phiếu kiểm</a>
					</div>
				</div>
			</div>
			<div id="table_content" style="position:relative">
				<table class="table">
					<tr>
						<th>MÃ SKU</th>
						<th>TÊN SẢN PHẨM</th>
						<th>ĐƠN VỊ</th>
						<th>SỐ LƯỢNG</th>
						<th>ĐƠN GIÁ MUA</th>
						<th>GIÁ TRỊ TỒN</th>
						<th class="text-center">HỦY</th>
					</tr>
					<?php 
					if($first!=''){
						$data = file_get_contents('log/pos/inventory/draft_order_'.$first.'.txt');
						$data = json_decode($data,true);
						if(isset($data['products']) && count($data['products'])>0){
							$key = array_keys($data['products']);
							$arr_inventory = array();
							$inventory = $this->db->query("select * from ttp_report_inventory where ProductsID in(".implode(',',$key).") and LastEdited=1 and WarehouseID=$this->WarehouseID")->result();
							if(count($inventory)>0){
								foreach($inventory as $row){
									if(isset($arr_inventory[$row->ProductsID])){
										$arr_inventory[$row->ProductsID] = $arr_inventory[$row->ProductsID]+$row->OnHand;
									}else{
										$arr_inventory[$row->ProductsID] = $row->OnHand;
									}
								}
							}
							$products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
							if(count($products)>0){
								foreach ($products as $row) {
									$amount = $data['products'][$row->ID];
									$price = $row->RootPrice;
									$total = $amount*$price;
									$invent = isset($arr_inventory[$row->ID]) ? $arr_inventory[$row->ID] : 0 ;
									echo '<tr>
									<td>'.$row->MaSP.'</td>
									<td style="width:350px">
										<p>'.$row->Title.'</p>
									</td>
									<td>'.$row->Donvi.'</td>
									<td>
										<input type="number" value="'.$data['products'][$row->ID].'" style="width:100px" onchange="inventory_change_products(this,'.$row->ID.')" />
										<span style="color:#999;display: block;margin-top:8px">Tồn hiện tại : '.number_format($invent).'</span>
									</td>
									<td>'.number_format($price).'</td>
									<td>'.number_format($total).'</td>
									<td class="text-center"><a onclick="inventory_remove_products('.$row->ID.')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
								}
							}
						}else{
							echo "<tr><td colspan='7'>Vui lòng chọn sản phẩm</td></tr>"; 
						}
					}
					?>
				</table>
			</div>
			<?php 
			$active = isset($data['customer']['Active']) && $data['customer']['Active']>0 ? 'hidden' : '' ;
			$inactive = isset($data['customer']['Active']) && $data['customer']['Active']==0 ? 'hidden' : '' ;
			?>
			<div class="no_action <?php echo $inactive ?>"></div>
		</div>
	</div>
	<div class="block2">
		<div class="order_info">
			<h3>Thông tin phiếu kiểm kho</h3>
			<hr>
			<?php 
			$title = isset($data['customer']['Title']) ? $data['customer']['Title'] : '' ;
			$type = isset($data['customer']['Type']) ? $data['customer']['Type'] : 0 ;
			$warehouseid = isset($data['customer']['WarehouseID']) ? $data['customer']['WarehouseID'] : 0 ;
			?>
			<div class="row">
				<label class="control-label col-xs-6">
					Hình thức kiểm kho
				</label>
				<label class="control-label col-xs-6">
					Kiểm tại kho
				</label>
				<div class="col-xs-6">
					<select name="Payment" id="inventory_bill_Type" class="form-control">
						<option value="0" <?php echo $type==0 ? 'selected="selected"' : '' ; ?>>Kiểm toàn bộ</option>
						<option value="1" <?php echo $type==1 ? 'selected="selected"' : '' ; ?>>Chỉ hàng trong phiếu</option>
					</select>
				</div>
				<div class="col-xs-6">
					<select name="Payment" id="inventory_bill_WarehouseID" class="form-control">
						<?php 
						$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
						if(count($warehouse)>0){
							foreach($warehouse as $row){
								$selected = $row->ID==$this->WarehouseID ? 'selected="selected"' : '' ;
								echo "<option value='$row->ID' $selected>$row->Title</option>";
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-12">
					Nội dung kiểm hàng
				</label>
				<div class="col-xs-12">
					<textarea name="Note" id="inventory_bill_CustomerNote" rows="3" class="form-control" placeholder="Nhập ghi chú trên phiếu kiểm hàng (nếu có) ..."><?php  echo $title ?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php 
					$href = isset($data['customer']['Active']) && $data['customer']['Active']>0 ? ADMINPATH.'/pos/pos_sales/print_order/'.$data['customer']['Active'] : '#' ;
					?>
					<a id="btn_createorder" onclick="inventory_create_order(this)" class="btn btn-success <?php echo $active ?>"><i class="fa fa-check-square" aria-hidden="true"></i> Hoàn tất</a>
					<a href="<?php echo $href ?>" id="btn_printorder" target="_blank" class="btn btn-primary <?php echo $inactive ?> print_button"><i class="fa fa-print" aria-hidden="true"></i> In hóa đơn</a>
					<a onclick="inventory_closebill_button(this)" id="btn_closeorder" class="btn btn-default <?php echo $inactive ?>"><i class="fa fa-times" aria-hidden="true"></i> Đóng hóa đơn</a>
				</div>
			</div>
			<div class="no_action <?php echo $inactive ?>""></div>
		</div>
	</div>
</div>