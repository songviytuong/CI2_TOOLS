<?php
class Finance_export extends Admin_Controller {
    public $user;
    public $classname="finance_export";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/finance_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Finance Export Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $limit_str = "limit $start,$this->limit";
        $result = $this->db->query("select * from ttp_report_finance_export where Dayfinance>='$startday' and Dayfinance<='$stopday' order by ID DESC")->result();
        $nav = $this->db->query("select count(1) as nav,sum(Price) as Total from ttp_report_finance_export where Dayfinance>='$startday' and Dayfinance<='$stopday'")->row();
        $total = $nav ? $nav->Total : 0 ;
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'total'     => $total,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'base_link' =>  base_url().ADMINPATH.'/report/finance_export/',
            'start'     =>  $start,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/finance_export/index',5,$nav,$this->limit)
        );
        $view = 'admin/finance_export_home';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function get_order(){
        $key = isset($_POST['key']) ? mysql_real_escape_string($_POST['key']) : '' ;
        if($key!=''){
            $key = " and a.POCode like '%$key%' or b.Title like '%$key%'";
        }
        $result = $this->db->query("select a.*,b.Title from ttp_report_perchaseorder a,ttp_report_production b where a.ProductionID=b.ID $key order by a.ID DESC limit 0,20")->result();
        if(count($result)>0){
            echo "<table class='table table-hover'><tr><th>MÃ PO</th><th>NHÀ CUNG CẤP</th><th>SỐ CHỨNG TỪ</th><th>GIÁ TRỊ</th><th>NGÀY KHỞI TẠO</th><th>THANH TOÁN</th></tr>";
            foreach($result as $row){
                $pay = $row->TotalPrice-$row->Reduce;
                echo "<tr onclick='select_this_order(\"$row->POCode\",$pay)'>
                        <td>$row->POCode</td>
                        <td>$row->Title</td>
                        <td>$row->HardCode</td>
                        <td>".number_format($row->TotalPrice-$row->Reduce)."</td>
                        <td>$row->Created</td>
                        <td>".number_format($row->PaymentMoney)."</td>
                    </tr>";
            }
            echo "</table>";
        }
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Finance Export Tools');
        $view = 'admin/finance_export_add';
        $this->template->write_view('content',$view);
        $this->template->render();
    }

    public function edit_export($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->IsAdmin==1){
            $result = $this->db->query("select * from ttp_report_finance_export where ID=$id")->row();
        }else{
            $result = $this->db->query("select * from ttp_report_finance_export where ID=$id and UserID=".$this->user->ID)->row(); 
        }
        if($result){
            $this->template->add_title('Finance Export Tools');
            $data = array(
                'data'=>$result
            );
            $view = 'admin/finance_export_edit';
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $DayFinance = isset($_POST['DayFinance']) ? $_POST['DayFinance'] : date('Y-m-d') ;
        $BankAccountID = isset($_POST['BankAccountID']) ? $_POST['BankAccountID'] : 0 ;
        $MoneyImport = isset($_POST['MoneyImport']) ? $_POST['MoneyImport'] : 0 ;
        $TypeImport = isset($_POST['TypeImport']) ? $_POST['TypeImport'] : 0 ;
        $SourceImportID = isset($_POST['SourceImport']) ? $_POST['SourceImport'] : 0 ;
        $PaymentImport = isset($_POST['PaymentImport']) ? $_POST['PaymentImport'] : 0 ;
        $CashID = isset($_POST['CashID']) ? $_POST['CashID'] : 0 ;
        $OrderCode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $ReciveToID = isset($_POST['ReciveToID']) ? $_POST['ReciveToID'] : 0 ;
        $OrderID = 0;
        $Harcode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $response = array("error"=>0,"message"=>"");
        if($MoneyImport<=0){
            $response = array("error"=>1,"message"=>"Số tiền chi bạn nhập vào không hợp lệ !");
        }else{
            if($PaymentImport==1 && $BankAccountID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn tài khoản ngân hàng !");
            }
            if($PaymentImport==0 && $CashID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn quỹ tiền mặt !");
            }
            if($SourceImportID==0){
                $check = $this->db->query("select ID,POCode from ttp_report_perchaseorder where POCode='$OrderCode'")->row();
                if($check){
                    $OrderID = $check->ID;
                    $Harcode = $check->POCode;
                }else{
                    $response = array("error"=>1,"message"=>"Mã chứng từ không hợp lệ !");
                }
            }
            if($ReciveToID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn người giao dịch");
            }
        }
        if($response['error']==0){
            $sql = array(
                'Dayfinance'    => $DayFinance,
                'UserID'        => $this->user->ID,
                'TypeFinance'   => $TypeImport,
                'SourceFinance' => $SourceImportID,
                'Price'         => $MoneyImport,
                'Payment'       => $PaymentImport,
                'AccountPayment'=> $BankAccountID,
                'OrderID'       => $OrderID,
                'Hardcode'      => $Harcode,
                'Note'          => $Note,
                'CashPayment'   => $CashID,
                'PersonID'      => $ReciveToID,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_report_finance_export',$sql);
            if($TypeImport==0){
                if($OrderID>0){
                    $this->db->query("update ttp_report_perchaseorder set PaymentMoney=$MoneyImport where ID=$OrderID");
                }
                if($PaymentImport==0){
                    $this->db->query("update ttp_report_finance_cash set Price=Price-$MoneyImport where ID=$CashID");
                    $this->cash_flow($MoneyImport,$CashID,'down');
                }else{
                    $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$MoneyImport where ID=$BankAccountID");
                    $this->bankpayment_flow($MoneyImport,$BankAccountID,'down');
                }
            }
        }
        echo json_encode($response);
    }

    public function bankpayment_flow($money=0,$account=0,$type='up'){
        if($account>0 && $money>0){
            $check = $this->db->query("select * from ttp_report_finance_bankaccount_flow where Dayfinance='".date('Y-m-d')."' and BankAccountID=$account")->row();
            if($check){
                if($type=="up"){
                    $this->db->query("update ttp_report_finance_bankaccount_flow set Price=Price+$money where ID=$check->ID");
                }
                if($type=="down"){
                    $this->db->query("update ttp_report_finance_bankaccount_flow set Price=Price-$money where ID=$check->ID");
                }
            }else{
                $current = $this->db->query("select * from ttp_report_finance_bankaccount_flow where BankAccountID=$account and Dayfinance<'".date('Y-m-d')."' order by Dayfinance DESC")->row();
                $current = $current ? $current->Price : 0 ;
                if($type=="up"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'BankAccountID' => $account,
                        'Price'         => $current+$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_bankaccount_flow",$data);
                }
                if($type=="down"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'BankAccountID' => $account,
                        'Price'         => $current-$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_bankaccount_flow",$data);
                }
            }
        }
    }

    public function cash_flow($money=0,$account=0,$type='up'){
        if($account>0 && $money>0){
            $check = $this->db->query("select * from ttp_report_finance_cash_flow where Dayfinance='".date('Y-m-d')."' and CashAccountID=$account")->row();
            if($check){
                if($type=="up"){
                    $this->db->query("update ttp_report_finance_cash_flow set Price=Price+$money where ID=$check->ID");
                }
                if($type=="down"){
                    $this->db->query("update ttp_report_finance_cash_flow set Price=Price-$money where ID=$check->ID");
                }
            }else{
                $current = $this->db->query("select * from ttp_report_finance_cash_flow where CashAccountID=$account and Dayfinance<'".date('Y-m-d')."' order by Dayfinance DESC")->row();
                $current = $current ? $current->Price : 0 ;
                if($type=="up"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'CashAccountID' => $account,
                        'Price'         => $current+$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_cash_flow",$data);
                }
                if($type=="down"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'CashAccountID' => $account,
                        'Price'         => $current-$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_cash_flow",$data);
                }
            }
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $DayFinance = isset($_POST['DayFinance']) ? $_POST['DayFinance'] : date('Y-m-d') ;
        $BankAccountID = isset($_POST['BankAccountID']) ? $_POST['BankAccountID'] : 0 ;
        $MoneyImport = isset($_POST['MoneyImport']) ? $_POST['MoneyImport'] : 0 ;
        $TypeImport = isset($_POST['TypeImport']) ? $_POST['TypeImport'] : 0 ;
        $SourceImportID = isset($_POST['SourceImport']) ? $_POST['SourceImport'] : 0 ;
        $PaymentImport = isset($_POST['PaymentImport']) ? $_POST['PaymentImport'] : 0 ;
        $CashID = isset($_POST['CashID']) ? $_POST['CashID'] : 0 ;
        $OrderCode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $OrderID = 0;
        $Harcode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $ReciveToID = isset($_POST['ReciveToID']) ? $_POST['ReciveToID'] : 0 ;
        $response = array("error"=>0,"message"=>"");
        if($MoneyImport<=0){
            $response = array("error"=>1,"message"=>"Số tiền chi bạn nhập vào không hợp lệ !");
        }else{
            if($PaymentImport==1 && $BankAccountID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn tài khoản ngân hàng !");
            }
            if($PaymentImport==0 && $CashID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn quỹ tiền mặt !");
            }
            if($SourceImportID==0){
                $check = $this->db->query("select ID,POCode from ttp_report_perchaseorder where POCode='$OrderCode'")->row();
                if($check){
                    $OrderID = $check->ID;
                    $Harcode = $check->POCode;
                }else{
                    $response = array("error"=>1,"message"=>"Mã chứng từ không hợp lệ !");
                }
            }
            if($ReciveToID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn người nộp tiền");
            }
        }
        if($response['error']==0){
            $import = $this->db->query("select * from ttp_report_finance_export where ID=$ID")->row();
            if($import){
                if($PaymentImport==1){
                    $CashID = 0;
                }else{
                    $BankAccountID = 0;
                }
                $sql = array(
                    'Dayfinance'    => $DayFinance,
                    'TypeFinance'   => $TypeImport,
                    'SourceFinance' => $SourceImportID,
                    'Price'         => $MoneyImport,
                    'Payment'       => $PaymentImport,
                    'AccountPayment'=> $BankAccountID,
                    'OrderID'       => $OrderID,
                    'Hardcode'      => $Harcode,
                    'Note'          => $Note,
                    'PersonID'      => $ReciveToID,
                    'CashPayment'   => $CashID
                );
                $this->db->where("ID",$ID);
                $this->db->update('ttp_report_finance_export',$sql);
                if($TypeImport==1 && $import->TypeFinance==0){
                    if($import->Payment==0){
                        $this->db->query("update ttp_report_finance_cash set Price=Price+$import->Price where ID=$import->CashPayment");
                        $this->cash_flow($import->Price,$import->CashPayment,'up');
                    }else{
                        $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$import->Price where ID=$import->AccountPayment");
                        $this->bankpayment_flow($import->Price,$import->AccountPayment,'up');
                    }
                    if($import->OrderID>0){
                        $this->db->query("update ttp_report_perchaseorder set PaymentMoney=0 where ID=$import->ID");
                    }
                }
                if($TypeImport==0 && $import->TypeFinance==1){
                    if($PaymentImport==0){
                        $this->db->query("update ttp_report_finance_cash set Price=Price-$MoneyImport where ID=$CashID");
                        $this->cash_flow($MoneyImport,$CashID,'down');
                    }else{
                        $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$MoneyImport where ID=$BankAccountID");
                        $this->bankpayment_flow($MoneyImport,$BankAccountID,'down');
                    }
                    $this->db->query("update ttp_report_perchaseorder set PaymentMoney=$MoneyImport where ID=$import->ID");
                }
                if($TypeImport==0 && $import->TypeFinance==0){
                    if($PaymentImport==$import->Payment){
                        if($PaymentImport==0){
                            if($CashID==$import->CashPayment){
                                $this->db->query("update ttp_report_finance_cash set Price=Price+$import->Price-$MoneyImport where ID=$CashID");
                                $temp = $import->Price-$MoneyImport;
                                if($temp>0){
                                    $this->cash_flow($temp,$CashID,'up');
                                }else{
                                    $temp = -$temp;
                                    $this->cash_flow($temp,$CashID,'down');
                                }
                            }else{
                                $this->db->query("update ttp_report_finance_cash set Price=Price+$import->Price where ID=$import->CashPayment");
                                $this->cash_flow($import->Price,$import->CashPayment,'up');
                                $this->db->query("update ttp_report_finance_cash set Price=Price-$MoneyImport where ID=$CashID");
                                $this->cash_flow($MoneyImport,$CashID,'down');
                            }
                        }else{
                            if($BankAccountID==$import->AccountPayment){
                                $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$import->Price-$MoneyImport where ID=$BankAccountID");
                                $temp = $import->Price-$MoneyImport;
                                if($temp>0){
                                    $this->bankpayment_flow($temp,$BankAccountID,'up');
                                }else{
                                    $temp = -$temp;
                                    $this->bankpayment_flow($temp,$BankAccountID,'down');
                                }
                            }else{
                                $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$import->Price where ID=$import->AccountPayment");
                                $this->bankpayment_flow($import->Price,$import->AccountPayment,'up');
                                $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$MoneyImport where ID=$BankAccountID");
                                $this->bankpayment_flow($MoneyImport,$BankAccountID,'down');
                            }
                        }
                    }else{
                        if($PaymentImport==0){
                            $this->db->query("update ttp_report_finance_cash set Price=Price-$MoneyImport where ID=$CashID");
                            $this->cash_flow($MoneyImport,$CashID,'down');
                            $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$import->Price where ID=$import->AccountPayment");
                            $this->bankpayment_flow($import->Price,$import->AccountPayment,'up');
                        }else{
                            $this->db->query("update ttp_report_finance_cash set Price=Price+$import->Price where ID=$import->CashPayment");
                            $this->cash_flow($import->Price,$import->CashPayment,'up');
                            $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$MoneyImport where ID=$BankAccountID");
                            $this->bankpayment_flow($MoneyImport,$BankAccountID,'down');
                        }
                    }
                    $this->db->query("update ttp_report_perchaseorder set PaymentMoney=$MoneyImport where ID=$import->ID");
                }
            }
        }
        echo json_encode($response);
    }

    public function delete(){
        $data = isset($_POST['data']) ? (int)$_POST['data'] : 0 ;
        $import = $this->db->query("select * from ttp_report_finance_export where ID=$data")->row();
        if($import){
            if($import->TypeFinance==0){
                if($import->Payment==0){
                    $this->db->query("update ttp_report_finance_cash set Price=Price+$import->Price where ID=$import->CashPayment");
                    $this->cash_flow($import->Price,$import->CashPayment,'up');
                }else{
                    $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$import->Price where ID=$import->AccountPayment");
                    $this->bankpayment_flow($import->Price,$import->AccountPayment,'up');
                }
                $this->db->query("update ttp_report_perchaseorder set PaymentMoney=0 where ID=$import->OrderID");
            }
            $this->db->query("delete from ttp_report_finance_export where ID=$data");
        }
    }

    public function check_order_code(){
        $data = isset($_POST['data']) ? $_POST['data'] : '' ;
        $check = $this->db->query("select ID from ttp_report_perchaseorder where POCode='$data'")->row();
        $response = array("error"=>1,'data'=>"select ID from ttp_report_perchaseorder where POCode='$data'");
        if($check){
            $response = array("error"=>0,'data'=>"select ID from ttp_report_perchaseorder where POCode='$data'");
        }
        echo json_encode($response);
    }

    public function export(){
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $result = $this->db->query("select * from ttp_report_finance_export where Dayfinance>='$startday' and Dayfinance<='$stopday'")->result();
        if(count($result)>0){
            $arr_cash = array();
            $cash = $this->db->query("select * from ttp_report_finance_cash")->result();
            if(count($cash)>0){
                foreach ($cash as $row) {
                    $arr_cash[$row->ID] = $row->Title;
                }
            }

            $arr_bank = array();
            $bank = $this->db->query("select * from ttp_report_finance_bankaccount")->result();
            if(count($bank)>0){
                foreach ($bank as $row) {
                    $arr_bank[$row->ID] = $row->Title;
                }
            }

            $arr_finance_type = $this->lib->get_config_define("finance_export","finance_export_type");
            $arr_finance_source = $this->lib->get_config_define("finance_export","finance_export_source");
            $arr_payment = array(0=>"Tiền mặt",1=>"Chuyển khoản");

            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày chi')
                        ->setCellValue('B1', 'Loại phiếu chi')
                        ->setCellValue('C1', 'Nguồn chi')
                        ->setCellValue('D1', 'Mã chứng từ')
                        ->setCellValue('E1', 'Số tiền')
                        ->setCellValue('F1', 'Thanh toán')
                        ->setCellValue('G1', 'Quỹ / tài khoản chi')
                        ->setCellValue('H1', 'Ghi chú');
            $arr_payment = array(0=>"COD",1=>"Chuyển khoản");
            $arr_type = $this->lib->get_config_define('status','order');
            $i=2;
            foreach($result as $row){
                if($row->Payment==0){
                    $h = isset($arr_cash[$row->CashPayment]) ? $arr_cash[$row->CashPayment] : "--";
                }else{
                    $h = isset($arr_bank[$row->AccountPayment]) ? $arr_bank[$row->AccountPayment] : "--";
                }
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, date('d/m/Y',strtotime($row->Dayfinance)))
                        ->setCellValue('B'.$i, $arr_finance_type[$row->TypeFinance])
                        ->setCellValue('C'.$i, $arr_finance_source[$row->SourceFinance])
                        ->setCellValue('D'.$i, $row->Hardcode)
                        ->setCellValue('E'.$i, $row->Price)
                        ->setCellValue('F'.$i, $arr_payment[$row->Payment])
                        ->setCellValue('G'.$i, $h)
                        ->setCellValue('H'.$i, $row->Note);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('RESULT_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data !";
        }
    }
}
