<?php 
$segment = $this->uri->segment(4);

$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

/*
*   Lấy danh sách đơn hàng đã đối soát & thanh toán với nhà cung cấp
*/

$payment_ok = $this->db->query("select ProductsID,ExportID,SupplierID from ttp_report_payment_supplier where SupplierID=$SupplierID")->result();
$arr_payment_ok = array();
if(count($payment_ok)>0){
    foreach($payment_ok as $row){
        $arr_payment_ok[$row->ProductsID][$row->ExportID][$row->SupplierID] = 1;
    }
}

/*
*   Lấy danh sách lô hàng và hàng hóa đã mua của nhà cung cấp theo PO
*/

$arr_shipment = array();
$arr_shipment_title = array();
$shipment = $this->db->query("select DISTINCT c.ProductsID,c.ShipmentID,c.PriceCurrency,c.ValueCurrency,c.VAT,b.ShipmentCode from ttp_report_inventory_import a,ttp_report_perchaseorder_details c,ttp_report_shipment b where b.ID=c.ShipmentID and a.POID=c.POID and a.ProductionID=$SupplierID")->result();
if(count($shipment)>0){
    foreach($shipment as $row){
        $arr_shipment[$row->ProductsID][$row->ShipmentID] = ($row->PriceCurrency*$row->ValueCurrency) + ((($row->PriceCurrency*$row->ValueCurrency)/100)*$row->VAT);
        $arr_shipment_title[$row->ProductsID][$row->ShipmentID] = $row->ShipmentCode;
    }
}

/*
*   Lấy danh sách hàng đã nhập trong kỳ
*/

$arr_shipment_import = array();
$shipment = $this->db->query("select c.ProductsID,c.ShipmentID,c.Amount from ttp_report_inventory_import a,ttp_report_inventory_import_details c,ttp_report_shipment b where b.ID=c.ShipmentID and a.ID=c.ImportID and a.ProductionID=$SupplierID and a.Status=4 and date(a.NgayNK)>='$startday' and date(a.NgayNK)<='$stopday'")->result();
if(count($shipment)>0){
    foreach($shipment as $row){
        if(isset($arr_shipment_import[$row->ProductsID][$row->ShipmentID])){
            $arr_shipment_import[$row->ProductsID][$row->ShipmentID] = $arr_shipment_import[$row->ProductsID][$row->ShipmentID]+$row->Amount;
        }else{
            $arr_shipment_import[$row->ProductsID][$row->ShipmentID] = $row->Amount;
        }
    }
}

/*
*   Lấy danh sách chi tiết sản phẩm đã bán lẻ
*/

$str = $TrademarkID!=0 ? " and a.TrademarkID=$TrademarkID" : "" ;
$str_combo = "" ;

$arr_shipment_ID = array();
$arr_products = array();
$products = $this->db->query("select a.MaSP,a.ID,a.Title,a.Donvi,d.MaXK,d.ID as ExportID,c.Total,c.Amount,c.Price,c.ShipmentID,d.TransferMoney from ttp_report_products a,ttp_report_order b,ttp_report_orderdetails c,ttp_report_export_warehouse d where b.ID=d.OrderID and a.ID=c.ProductsID and b.ID=c.OrderID and date(d.DateTransferMoney)>='".date('Y-m-d',strtotime($startday))."' and date(d.DateTransferMoney)<='".date('Y-m-d',strtotime($stopday))."' and a.VariantType in(0,1) and b.CustomerID!=9996 and b.KhoID=$WarehouseID $str")->result();
if(count($products)>0){
    foreach($products as $row){
        if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
            $arr_shipment_ID[] = $row->ShipmentID;
            if(!isset($arr_products[$row->ID.'_'.$row->ShipmentID])){
                $arr_products[$row->ID.'_'.$row->ShipmentID]['MaSP'] = $row->MaSP;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Title'] = $row->Title;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Donvi'] = $row->Donvi;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Shipment'] = $arr_shipment_title[$row->ID][$row->ShipmentID];
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Totalamount'] = $row->Amount;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Success'] = 0;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting'] = 0;
                if($row->TransferMoney==1){
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Success'] = 1;
                }else{
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting'] = 1;
                }
            }else{
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Totalamount'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Totalamount']+$row->Amount;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Total'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                if($row->TransferMoney==1){
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Success'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Success']+1;
                }else{
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting']+1;
                }
            }
        }
    }
}

/*
*   Lấy danh sách chi tiết sản phẩm đã bán gộp trong combo
*/

$products = $this->db->query("select a.MaSP,a.ID,a.Title,f.Title as Combo,a.Donvi,d.MaXK,d.ID as ExportID,c.Amount,c.Price,c.ShipmentID,d.TransferMoney from ttp_report_products a,ttp_report_order b,ttp_report_orderdetails_bundle c,ttp_report_export_warehouse d,ttp_report_orderdetails e,ttp_report_products f where e.OrderID=b.ID and e.ID=c.DetailsID and e.ProductsID=f.ID and b.ID=d.OrderID and a.ID=c.ProductsID and date(d.DateTransferMoney)>='".date('Y-m-d',strtotime($startday))."' and date(d.DateTransferMoney)<='".date('Y-m-d',strtotime($stopday))."' and a.VariantType in(0,1) and b.CustomerID!=9996 and b.KhoID=$WarehouseID $str_combo")->result();
if(count($products)>0){
    foreach($products as $row){
        if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
            if(!isset($arr_products[$row->ID.'_'.$row->ShipmentID])){
                $arr_products[$row->ID.'_'.$row->ShipmentID]['MaSP'] = $row->MaSP;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Title'] = $row->Title;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Donvi'] = $row->Donvi;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Shipment'] = $arr_shipment_title[$row->ID][$row->ShipmentID];
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Totalamount'] = $row->Amount;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Success'] = 0;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting'] = 0;
                if($row->TransferMoney==1){
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Success'] = 1;
                }else{
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting'] = 1;
                }
            }else{
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Totalamount'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Totalamount']+$row->Amount;
                $arr_products[$row->ID.'_'.$row->ShipmentID]['Total'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                if($row->TransferMoney==1){
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Success'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Success']+1;
                }else{
                    $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting'] = $arr_products[$row->ID.'_'.$row->ShipmentID]['Waiting']+1;
                }
            }
        }
    }
}

/*
*   Tính tồn đầu kỳ của từng sản phẩm & lô hàng
*/

$arr_shipment_ID = array_unique($arr_shipment_ID);
$arr_shipment_ID = count($arr_shipment_ID)>0 ? implode(',',$arr_shipment_ID) : '0' ;
$start_inventory = date('Y-m-d',strtotime($startday)-3600*24);
$stop_inventory = date('Y-m-d',strtotime($startday)-3600*24*31);
$arr_inventory_first = array();
$inventory = $this->db->query("select ProductsID,ShipmentID,OnHand from ttp_report_inventory where DateInventory<='$start_inventory' and DateInventory>='$stop_inventory' and ShipmentID in($arr_shipment_ID) order by DateInventory DESC")->result();
if(count($inventory)>0){
    foreach($inventory as $row){
        if(!isset($arr_inventory_first[$row->ProductsID][$row->ShipmentID])){
            $arr_inventory_first[$row->ProductsID][$row->ShipmentID] = $row->OnHand;
        }
    }
}

/*
*   Tính tồn cuối kỳ của từng sản phẩm & lô hàng
*/

$arr_inventory_last = array();
$stop_inventory_last = date('Y-m-d',strtotime($stopday));
$inventory = $this->db->query("select ProductsID,ShipmentID,OnHand from ttp_report_inventory where DateInventory<='$stop_inventory_last' and DateInventory>='$stop_inventory' and ShipmentID in($arr_shipment_ID) order by DateInventory DESC")->result();
if(count($inventory)>0){
    foreach($inventory as $row){
        if(!isset($arr_inventory_last[$row->ProductsID][$row->ShipmentID])){
            $arr_inventory_last[$row->ProductsID][$row->ShipmentID] = $row->OnHand;
        }
    }
}

?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1><a style="<?php echo $segment=="report_supplier" ? "color:#f96868" : "" ; ?>" href="<?php echo base_url().ADMINPATH.'/report/report_sales/report_supplier' ?>">BÁO CÁO KINH DOANH NCC</a> | <a style="<?php echo $segment=="report_supplier_payment" ? "color:#f96868" : "" ; ?>" href="<?php echo base_url().ADMINPATH.'/report/report_sales/report_supplier_payment' ?>">BÁO CÁO CÔNG NỢ NCC</a></h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Thời gian báo cáo <span class='pull-right'>:</span> </label>
        <label class="col-xs-4"><?php echo "Từ <b style='margin-right:10px;margin-left:10px'>".date('d/m/Y',strtotime($startday))."</b> đến <b style='margin-left:10px'>".date('d/m/Y',strtotime($stopday)); ?></b></label>
        <div class="col-xs-6 text-right">
            <?php 
            $querystr = "?".http_build_query($_GET);
            ?>
            <a href="<?php echo base_url().ADMINPATH.'/report/report_sales/export_report_supplier'.$querystr ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export excel</a>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Tên nhà cung cấp  <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="SupplierID" onchange="changefillter()">
                <option value="0">-- Chọn nhà cung cấp --</option>
                <?php 
                $supplier = $this->db->query("select ID,Title from ttp_report_production order by Title ASC")->result();
                if(count($supplier)>0){
                    foreach($supplier as $row){
                        $selected = $row->ID==$SupplierID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Kho bán hàng <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="WarehouseID" onchange="changefillter()">
                <option value="0">-- Chọn kho bán hàng --</option>
                <?php 
                $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
                if(count($warehouse)>0){
                    foreach($warehouse as $row){
                        $selected = $row->ID==$WarehouseID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label class="control-label col-xs-2">Thương hiệu <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="TrademarkID" onchange="changefillter()">
                <option value="0">-- Tất cả thương hiệu --</option>
                <?php 
                if($SupplierID>0){
                    $trademark = $this->db->query("select DISTINCT a.ID,a.Title from ttp_report_trademark a,ttp_report_inventory_import b,ttp_report_inventory_import_details c,ttp_report_products d where a.ID=d.TrademarkID and d.ID=c.ProductsID and c.ImportID=b.ID and b.ProductionID=$SupplierID and b.KhoID=$WarehouseID order by a.Title ASC")->result();
                }else{
                    $trademark = $this->db->query("select ID,Title from ttp_report_trademark order by Title ASC")->result();
                }
                if(count($trademark)>0){
                    foreach($trademark as $row){
                        $selected = $row->ID==$TrademarkID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
        
    </div>
    <div class="row" style="margin: 0px;overflow-x:scroll;">
        <div class="row" style="width:2000px;margin:0px">
            <table class="table table-bordered" style="width:auto;max-width: 9999px">
                <tr>
                    <th rowspan="2">STT</th>
                    <th rowspan="2">Mã SP</th>
                    <th rowspan="2">Tên SP </th>
                    <th rowspan="2">Đơn vị</th>
                    <th rowspan="2">Lô hàng</th>
                    <th colspan="2">Tồn đầu kỳ</th>
                    <th colspan="2">Nhập trong kỳ</th>
                    <th colspan="2">Xuất bán trong kỳ <br>(chưa thành công)</th>
                    <th colspan="2">Xuất bán trong kỳ <br>(thành công)</th>
                    <th colspan="2">Tồn kho cuối kỳ <br>(on-hand) </th>
                    <th colspan="2">Còn nợ chưa TT lần trước</th>
                    <th colspan="2">Đề nghị thanh toán lần này</th>
                    <th colspan="2">Đề nghị giữ lại</th>
                </tr>
                <tr>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                    <th>SL</th>
                    <th>GT</th>
                </tr>
                <?php 
                if(count($arr_products)>0){
                    $i=1;
                    foreach($arr_products as $key=>$row){
                        $keyvalue = explode('_', $key);
                        $ID_row = isset($keyvalue[0]) ? $keyvalue[0] : 0 ;
                        $ShipmentID_row = isset($keyvalue[1]) ? $keyvalue[1] : 0 ;
                        $price_single = isset($arr_shipment[$ID_row][$ShipmentID_row]) ? $arr_shipment[$ID_row][$ShipmentID_row] : 0 ;

                        /*
                        *   Tồn đầu kỳ & giá trị tồn đầu kỳ
                        */
                        $row_inventory_first = isset($arr_inventory_first[$ID_row][$ShipmentID_row]) ? $arr_inventory_first[$ID_row][$ShipmentID_row] : 0 ;
                        $row_inventory_first_price = $row_inventory_first*$price_single;

                        /*
                        *   Tồn cuối kỳ & giá trị tồn cuối kỳ
                        */
                        $row_inventory_last = isset($row_inventory_last[$ID_row][$ShipmentID_row]) ? $row_inventory_last[$ID_row][$ShipmentID_row] : 0 ;
                        $row_inventory_last_price = $row_inventory_last*$price_single;

                        /*
                        *   Xuất bán thành công
                        */
                        $row_export_success_amount = $row['Success'];
                        $row_export_success_price = $price_single*$row['Success'];

                        /*
                        *   Xuất bán chưa thành công
                        */
                        $row_export_waiting_amount = $row['Waiting'];
                        $row_export_waiting_price = $price_single*$row['Waiting'];

                        /*
                        *   Nhập trong kỳ
                        */
                        $row_import_waiting_amount = isset($arr_shipment_import[$ID_row][$ShipmentID_row]) ? $arr_shipment_import[$ID_row][$ShipmentID_row] : 0 ;
                        $arr_shipment_import_price = $price_single*$row_import_waiting_amount;

                        echo "<tr>
                                <td style='width:30px'>".$i."</td>
                                <td style='width:100px'>".$row['MaSP']."</td>
                                <td style='width:200px'>".$row['Title']."</td>
                                <td style='width:80px'>".$row['Donvi']."</td>
                                <td style='width:120px'>".$row['Shipment']."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_inventory_first)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_inventory_first_price)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_import_waiting_amount)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($arr_shipment_import_price)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_export_waiting_amount)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_export_waiting_price)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_export_success_amount)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_export_success_price)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_inventory_last)."</td>
                                <td style='width:120px;text-align:right;'>".number_format($row_inventory_last_price)."</td>
                                <td style='width:120px'></td>
                                <td style='width:120px'></td>
                                <td style='width:120px'></td>
                                <td style='width:120px'></td>
                                <td style='width:120px'></td>
                                <td style='width:120px'></td>
                            </tr>";
                        $i++;
                    }
                }
                ?>
                
            </table>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .table tr th{background:#EEE;vertical-align: middle !important;text-align: center}
</style>
<script type="text/javascript">

    $(document).ready(function () {

        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });
    

    function changefillter(){
        var WarehouseID = $("#WarehouseID").val();
        var SupplierID = $("#SupplierID").val();
        var TrademarkID = $("#TrademarkID").val();
        window.location = "<?php echo base_url().ADMINPATH.'/report/report_sales/report_supplier_payment?' ?>"+"WarehouseID="+WarehouseID+"&SupplierID="+SupplierID+"&TrademarkID="+TrademarkID;
    }

</script>
