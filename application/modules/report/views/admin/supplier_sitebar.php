<?php 
$url = base_url().ADMINPATH.'/report';
$segment = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <?php 
    if($this->user->IsAdmin==1 || $this->user->UserType==16){
    ?>
    <p class="title"><i class="fa fa-users"></i> Đối tác bán hàng</p>
    <ul>
        <?php 
        if($this->user->IsAdmin==1){
        ?>
        <li><a href="<?php echo $url.'/partner_affiliate/voucher' ?>" <?php echo $segment=='partner_affiliate' && $segment_current_bonus=='voucher' ? "class='active'" : '' ; ?>>Danh sách voucher</a></li>
        <?php 
        }
        ?>
        <li><a href="<?php echo $url.'/partner_affiliate/information' ?>" <?php echo $segment=='partner_affiliate' && ($segment_current_bonus=='information' || $segment_current_bonus=='information_user') ? "class='active'" : '' ; ?>>Thông tin tài khoản</a></li>
        <li><a href="<?php echo $url.'/partner_affiliate/create_link' ?>" <?php echo $segment=='partner_affiliate' && $segment_current_bonus=='create_link' ? "class='active'" : '' ; ?>>Tạo link riêng</a></li>
        <li><a href="<?php echo $url.'/partner_affiliate/report' ?>" <?php echo $segment=='partner_affiliate' && $segment_current_bonus=='report' ? "class='active'" : '' ; ?>>Doanh thu tổng quan</a></li>
        <li><a href="<?php echo $url.'/partner_affiliate/report_details' ?>" <?php echo $segment=='partner_affiliate' && $segment_current_bonus=='report_details' ? "class='active'" : '' ; ?>>Doanh thu chi tiết</a></li>
        <li><a href="<?php echo $url.'/partner_affiliate/analytics' ?>" <?php echo $segment=='partner_affiliate' && $segment_current_bonus=='analytics' ? "class='active'" : '' ; ?>>Thống kê truy cập</a></li>
    </ul>
    <?php 
    }
    ?>
    <?php 
    if($this->user->IsAdmin==1 || $this->user->UserType==14){
    ?>
    <p class="title"><i class="fa fa-archive fa-fw" aria-hidden="true"></i> Hàng hóa</p>
    <ul>
        <li><a href="<?php echo $url.'/partner_supplier/products_home' ?>" <?php echo $segment_current_bonus=="products_home" ? "class='active'" : '' ; ?>>Sản phẩm đang hợp tác</a></li>
        <li><a href="<?php echo $url.'/partner_supplier/update_products_home' ?>" <?php echo $segment_current_bonus=="products" || $segment_current_bonus=="update_products_home" || $segment_current_bonus=="view_data_update_supplier" ? "class='active'" : '' ; ?>>Cập nhật DS sản phẩm</a></li>
    </ul>
    <p class="title"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i> Xử lý đơn hàng</p>
    <ul>
        <li><a href="<?php echo $url.'/partner_supplier/request_available' ?>" <?php echo $segment_current_bonus=="request_available" || $segment_current_bonus=="" ? "class='active'" : '' ; ?>>Xác nhận có hàng</a></li>
        <li><a href="<?php echo $url.'/partner_supplier/request_picking' ?>" <?php echo $segment_current_bonus=="request_picking" ? "class='active'" : '' ; ?>>Xác nhận soạn hàng</a></li>
        <li><a href="<?php echo $url.'/partner_supplier/waiting_pick' ?>" <?php echo $segment_current_bonus=="waiting_pick" ? "class='active'" : '' ; ?>>Chờ vận chuyển lấy hàng</a></li>
        <li><a href="<?php echo $url.'/partner_supplier/picked' ?>" <?php echo $segment_current_bonus=="picked" ? "class='active'" : '' ; ?>>Đã giao vận chuyển</a></li>
    </ul>
    <p class="title"><i class="fa fa-users fa-fw" aria-hidden="true"></i> Đối soát thanh toán</p>
    <ul>
        <li><a href="<?php echo $url.'/partner_supplier/payment_home' ?>" <?php echo $segment_current_bonus=="payment_home" ? "class='active'" : '' ; ?>>Kiểm tra đối soát</a></li>
        <li><a href="<?php echo $url.'/partner_supplier/payment_history' ?>" <?php echo $segment_current_bonus=="payment_history" ? "class='active'" : '' ; ?>>Lịch sử thanh toán</a></li>
    </ul>
    <p class="title"><i class="fa fa-line-chart fa-fw" aria-hidden="true"></i> Báo cáo</p>
    <ul>
        <li><a href="<?php echo $url.'/partner_supplier/supplier_report_overview' ?>" <?php echo $segment_current_bonus=="supplier_report_overview" ? "class='active'" : '' ; ?>>Tổng quan</a></li>
        <li><a href="<?php echo $url.'/partner_supplier/supplier_report_products' ?>" <?php echo $segment_current_bonus=="supplier_report_products" ? "class='active'" : '' ; ?>>Theo sản phẩm</a></li>
    </ul>
    <?php 
    }
    ?>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>
<script src="public/admin/js/notify.js"></script>
<script type="text/javascript">
    $(".sitebar ul li p i").click(function(){
        $(".sitebar ul li ul").slideUp();
        $(".sitebar ul li p i.fa").removeClass('fa-caret-up').addClass('fa-caret-down');
        $(this).removeClass('fa-caret-down').addClass('fa-caret-up');
        $(this).parent('p').parent('li').find('ul').slideDown();
    });


    var getcurrentorder = function(){
        $.ajax({
            url: "<?php echo $url ?>/partner_supplier/get_notify",
            dataType: "json",
            type: "POST",
            data: "",
            success: function(result){
                if(result.status!=false){
                    notifyNewOrder(result.title,result.image,result.message);
                }
            }
        });
    }
    setInterval(getcurrentorder,15000);

    function notifyNewOrder(title,image,message){
        notify(title, {
            body: message,
            icon: image,
            onclick: function(e) {},
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }
</script>
