<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$total_get = 0;
$total_ship = 0;

$shipperlist = $this->db->query("select a.UserName,a.ID from ttp_user a,ttp_user_transport b where b.UserID=a.ID")->result();

$picking_status = $this->lib->get_config_define("status","picking_status",1);
$arr_status = array();
if(count($picking_status)>0){
    foreach ($picking_status as $row) {
        $arr_status[$row->code] = array('name'=>$row->name,'color'=>$row->color);
    }
}

$arr_branch = array();
$branch = $this->db->query("select * from ttp_report_branch")->result();
if(count($branch)>0){
	foreach($branch as $row){
		$arr_branch[$row->ID] = $row->Title;
	}
}

$arr_warehouse = array();
$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
if(count($warehouse)>0){
	foreach($warehouse as $row){
		$arr_warehouse[$row->ID] = $row->MaKho;
	}
}

$arr_order = array();
$arr_order_success = array();
$arr_order_false = array();
$arr_order_wait = array();
$arr_address = array();
$arr_address1 = array();
if(count($data)>0){
	foreach($data as $row){
		if(!isset($arr_order[$row->OrderID])){
			$arr_order[$row->OrderID]=1;
		}
		if($row->OrderStatus==0){
			if(!isset($arr_order_success[$row->OrderID])){
				$arr_order_success[$row->OrderID]=1;
			}
		}elseif($row->OrderStatus==1){
			if(!isset($arr_order_false[$row->OrderID])){
				$arr_order_false[$row->OrderID]=1;
			}
		}else{
			if(!isset($arr_order_wait[$row->OrderID])){
				$arr_order_wait[$row->OrderID]=1;
			}
		}
		if($row->BranchID>0){
			if(!isset($arr_address[$row->BranchID][$row->OrderID])){
				$arr_address[$row->BranchID][$row->OrderID] = 1;
			}
		}
		if($row->WarehouseID>0){
			if(!isset($arr_address1[$row->WarehouseID][$row->OrderID])){
				$arr_address1[$row->WarehouseID][$row->OrderID] = 1;
			}
		}
	}
}
?>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<div class="btn-group" style="margin-right:8px">
                <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> UExpress</a>
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                <ul class="dropdown-menu">
					<?php 
					$transport = $this->db->query("select * from ttp_report_transport")->result();
					if(count($transport)>0){
						foreach($transport as $row){
							$icon = $row->ID==$express ? 'fa-dot-circle-o' : 'fa-circle-o' ;
							$color = $row->ID==$express ? "#090" : '#ccc' ;
							echo '<li><a href="'.$base_link.'report_sales/report_transport?express='.$row->ID.'&shipper='.$shipper.'"><i class="fa '.$icon.'" style="margin-right:10px;color:'.$color.'" aria-hidden="true"></i> '.$row->Title.'</a></li>';
						}
					}
					?>
                </ul>
            </div>
            <div class="btn-group" style="margin-right:8px">
                <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Tất cả nhân viên</a>
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                	<?php 
                	$icon = 0==$shipper ? "fa-dot-circle-o" : 'fa-circle-o' ;
                	$color = 0==$shipper ? "#090" : '#ccc' ;
                	?>
                	<li><a href="<?php echo $base_link.'report_sales/report_transport?express='.$express.'&shipper=0' ?>"><i class="fa <?php echo $icon ?>" style="margin-right:10px;color:<?php echo $color ?>" aria-hidden="true"></i> Tất cả nhân viên</a></li>
					<?php 
					if(count($shipperlist)>0){
						foreach($shipperlist as $row){
							$icon = $row->ID==$shipper ? 'fa-dot-circle-o' : 'fa-circle-o' ;
							$color = $row->ID==$shipper ? "#090" : '#ccc' ;
							echo '<li><a href="'.$base_link.'report_sales/report_transport?express='.$express.'&shipper='.$row->ID.'"><i class="fa '.$icon.'" style="margin-right:10px;color:'.$color.'" aria-hidden="true"></i> '.$row->UserName.'</a></li>';
						}
					}
					?>
                </ul>
            </div>
	    </div>
	    <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
	    </div>
    </div>
    
    <div class="row" style="padding-top:20px;">
    	<div class="col-xs-5">
	    	<div class="col-xs-6" style='border-right:1px solid #eee;margin-bottom:25px;margin-top:25px;'>
	    		<p>TỔNG SỐ ĐƠN HÀNG</p>
	    		<h2><?php echo number_format(count($arr_order)) ?></h2>
	    	</div>
	    	<div class="col-xs-6" style="margin-bottom:25px;margin-top:25px;">
	    		<p>SỐ ĐƠN THÀNH CÔNG</p>
	    		<h2><?php echo number_format(count($arr_order_success)) ?></h2>
	    	</div>
	    	<div class="col-xs-6" style='border-right:1px solid #eee'>
	    		<p>SỐ ĐƠN ĐANG XỬ LÝ</p>
	    		<h2><?php echo number_format(count($arr_order_wait)) ?></h2>
	    	</div>
	    	<div class="col-xs-6">
	    		<p>SỐ ĐƠN HỦY</p>
	    		<h2><?php echo number_format(count($arr_order_false)) ?></h2>
	    	</div>
    	</div>
    	<div class="col-xs-7">
    		<div id="piechart" style="width: 100%; height: 300px;"></div>
    	</div>
    </div>
    <div class="row" style="margin:0px;">
    	<table class="table">
    		<tr>
    			<th>Mã DH</th>
    			<th>Mã SP</th>
    			<th>Trạng thái</th>
    			<th>Nhận hàng tại</th>
    			<th>Ngày nhận hàng</th>
    			<th>Cập nhật cuối</th>
    			<th>Nhân viên</th>
    			<th>Đơn vị</th>
    		</tr>
    		<?php 
    		$state = 0;
    		if(count($data)>0){
    			foreach($data as $row){
    				if($state!=$row->OrderID){
    					$state=$row->OrderID;
    					$row->MaDH = $row->MaDH;
    				}else{
    					$row->MaDH='--';
    				}
    				$status_row = isset($arr_status[$row->Status]) ? "<span style='padding:5px 10px;border-radius:20px;background:#f5f5f5'><span style='display:inline-block;width:8px;height:8px;background:#".$arr_status[$row->Status]['color'].";margin-right:5px;border-radius:50%'></span>".$arr_status[$row->Status]['name']."</span>" : '--' ;
    				$branchtitle = isset($arr_branch[$row->BranchID]) ? $arr_branch[$row->BranchID] : '--' ;
    				$branchtitle = isset($arr_warehouse[$row->WarehouseID]) ? $arr_warehouse[$row->WarehouseID] : $branchtitle ;
    				echo "<tr>";
    				echo "<td>$row->MaDH</td>";
    				echo "<td>$row->MaSP</td>";
    				echo "<td>$status_row</td>";
    				echo "<td>$branchtitle</td>";
    				echo "<td>".date('d/m/Y',strtotime($row->TimeReciver))."</td>";
    				echo "<td>$row->TimeSuccess</td>";
    				echo "<td>$row->UserName</td>";
    				echo "<td>$row->Title</td>";
    				echo "</tr>";
    			}
    		}
    		?>
    	</table>
    </div>
    <div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<style>
	.table tr th:first-child,.table tr td:first-child{padding-left:0px;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    location.reload();
                }
            }); 
        });
    });
</script>
<!-- /datepicker -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data1 = google.visualization.arrayToDataTable([
      <?php 
      	echo "['Địa điểm lấy hàng', 'Số lượng đơn hàng']";
        if(count($arr_address)>0){
            foreach($arr_address as $key=>$row){
            	$name = isset($arr_branch[$key]) ? $arr_branch[$key] : '' ;
                echo ",['".$name."',".count($row)."]";
            }
        }
        if(count($arr_address1)>0){
            foreach($arr_address1 as $key=>$row){
            	$name = isset($arr_warehouse[$key]) ? $arr_warehouse[$key] : '' ;
                echo ",['".$name."',".count($row)."]";
            }
        }
      ?>
    ]);

    var options1 = {
      title: 'Thống kê địa điểm lấy hàng'
    };

    var chart1 = new google.visualization.PieChart(document.getElementById('piechart'));
    chart1.draw(data1, options1);
  }
</script>