<?php 
$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
$arr_warehouse = array();
if(count($warehouse)>0){
	foreach ($warehouse as $row) {
		$arr_warehouse[$row->ID] = $row->MaKho;
	}
}
?>
<div class="sub_menu">
	<div class="find_record">
		<a style="padding:0px 5px" href="<?php echo ADMINPATH.'/pos/pos_inventory_export/' ?>"><i class="fa fa-arrow-left" style="margin-right:5px"></i> Quay lại</a>
	</div>
</div>
<div class="containner">
	<div class="order_info">
		<div class="row">
			<div class="col-xs-8">
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Mã phiếu chuyển</span>: <?php echo $data->OrderCode ?></div>
				<?php 
				$arr = array(
					4=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Thành công</a>',
					3=>'<a class="status-button"><i class="fa fa-circle text-warning" aria-hidden="true"></i> Chờ nhập</a>',
					1=>'<a class="status-button"><i class="fa fa-circle text-danger" aria-hidden="true"></i> Đã hủy</a>',
					0=>'<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Nháp</a>'
				);
				?>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Trạng thái</span>: <?php echo $arr[$data->Status]; ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Thời gian</span>: <?php echo date('d/m/Y H:i',strtotime($data->Created)) ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Người tạo phiếu</span>: <?php echo $data->UserName ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Kho xuất</span>: <?php echo $arr_warehouse[$data->WarehouseSender] ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Kho nhập</span>: <?php echo $arr_warehouse[$data->WarehouseReciver] ?></div>
				
			</div>
			<div class="col-xs-4">
				<textarea placeholder="Ghi chú ..."><?php echo $data->Note ?></textarea>
			</div>
		</div>
	</div>
	<div class="table_div" style="margin-top:20px">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã SKU</th>
				<th>Tên sản phẩm</th>
				<th>Đơn vị</th>
				<th>Yêu cầu xuất</th>
				<th>Số lượng xuất</th>
				<th>Số lượng nhận</th>
			</tr>
			<tr>
				<?php 
				$total = 0;
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,b.RootPrice from ttp_report_transferorder_details a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
				if(count($details)>0){
					$i=1;
					foreach ($details as $row) {
						$total = $total+($row->TotalExport*$row->RootPrice);
						$warning = $row->TotalExport!=$row->TotalImport ? '<i style="color:#e4442a" class="fa fa-warning" title="Số lượng xuất và nhập không khớp"></i>' : '<i style="color:#e4442a" class="fa fa-warning hidden" title="Số lượng xuất và nhập không khớp"></i>' ;
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td>'.$row->MaSP.'</td>
						<td>'.$row->Title.'</td>
						<td>'.$row->Donvi.'</td>
						<td>'.$row->Request.'</td>
						<td>'.$row->TotalExport.'</td>
						<td><input type="number" value="'.$row->TotalImport.'" onchange="transfer_warehouse_change_import_products(this,'.$row->ID.')" />'.$warning.'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
			<tr>
				<td colspan="6" class="text-right"><b>SỐ LOẠI SẢN PHẨM: </b></td>
				<td><b><?php echo number_format(count($details)) ?></b></td>
			</tr>
			<tr>
				<td colspan="6" class="text-right"><b>GIÁ TRỊ PHIẾU CHUYỂN: </b></td>
				<td><b><?php echo number_format($total) ?></b></td>
			</tr>
		</table>
		<hr>
		<div class="row text-right">
			<div class="col-xs-12">
			<?php 
				if($data->Status==3){
					?>
					<a class="btn btn-danger" onclick="transfer_warehouse_drop_import(this,<?php echo $data->ID ?>)"><i class="fa fa-times"></i> Hủy đơn hàng</a>
					<a class="btn btn-success" onclick="transfer_warehouse_accept_import(this,<?php echo $data->ID ?>)"><i class="fa fa-check"></i> Xác nhận nhập hàng</a>
					<?php
				}
			?>
			<a class="btn btn-primary" href="<?php echo ADMINPATH.'/pos/pos_inventory_export/print_order/'.$data->ID ?>"><i class="fa fa-print"></i> In hóa đơn</a>
			</div>
		</div>
	</div>
</div>
