<?php
class Training_chapter_model extends CI_Model
{
    private $_tableName = "ttp_training_chapter";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getListByPartId($partid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($partid !=null){
            $sql .=" and partid IN ($partid)";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function insert($data){
        return $this->db->insert($this->_tableName,$data);
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }

    public function getListByCourseByPart($courseid=0,$partid=0)
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($courseid >0){
            $sql .=" and courseid = $courseid";
        }
        if($partid >0){
            $sql .=" and partid = $partid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
}?>