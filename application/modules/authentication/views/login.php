<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>System Authentication | Login</title> 
        <link href="<?php echo base_url() ?>public/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>public/admin/css/animate.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>public/admin/css/style_login.css" rel="stylesheet">
        <style>
            .warning{text-align:center;padding:5px 10px;text-align:center;color:#FFF;background: rgba(0,0,0,0.6);position: relative;z-index: 1}
        </style>
        <script src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>public/admin/js/script_admin.js?v=1"></script>
    </head>
    <body>
        <?php
        $error = $this->uri->segment(3);
        if ($error == 'error') {
            echo "<div class='warning'>User hoặc Password không chính xác vui lòng kiểm tra lại !.</div>";
        }
        ?>
        <div class="box">
            <div class="left_box">
                <form method="post" action="<?php echo base_url() . ADMINPATH ?>/check-login">
                    <h1>CMS Administrator</h1>
                    <div><input type="text" class="form-control" placeholder="Username" name="username" required id="username" autocomplete="off"/></div>
                    <div><input type="password" class="form-control" placeholder="Password" required name="password" id="password" autocomplete="off"/></div>
                    <div class="row">
                        <div class="col-lg-3"><button type="submit" name="commit" class="btn btn-danger btn-sm submit" />Login</button></div>
                        <div class="col-lg-9 text-right accept_label">
                            
                            <?php if ($_ci_vars['type'] == 1) : ?>
                                <span class="btn btn-success">Wait to be accepted before you start</span>
                            <?php endif; ?>
                            <?php if ($_ci_vars['type'] == 2): ?>
                                Click <a class="accept btn btn-success btn-sm">here</a> to Accept Login
                            <?php endif; ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="right_box">
                <img src="<?php echo base_url() ?>public/login/images/logo.png" />
                <h2>Auchan Retail Vietnam</h2>
                <h5>Your IP: <?php echo $_ci_vars['yourip']; ?> </h5>
                <h5>Referer : <?php echo $_ci_vars['referer']; ?></h5>
            </div>
        </div>
    </body>
</html>