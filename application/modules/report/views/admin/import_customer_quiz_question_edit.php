<div class="alert alert-sm hidden" id="alert-message">Lưu thông tin thành công!</div>
<input type="hidden" id="ObjectID" value="<?php echo $data->ID ?>" />
<input type="hidden" id="ParentID" value="<?php echo $data->ParentID ?>" />
<div class="row">
	<div class="col-xs-12">
		<p><b>Nội dung câu hỏi</b></p>
		<div class="form-group">
			<input type="text" id="Title" class="form-control" placeholder="Nhập nội dung câu hỏi" value="<?php echo $data->Title ?>" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-3">
		<p><b>Cấp độ câu hỏi</b></p>
		<div class="form-group">
			<select class="form-control" id="Level">
				<option value="1" <?php echo $data->Level==1 ? 'selected="selected"' : '' ; ?>>Cấp độ 1</option>
				<option value="2" <?php echo $data->Level==2 ? 'selected="selected"' : '' ; ?>>Cấp độ 2</option>
				<option value="3" <?php echo $data->Level==3 ? 'selected="selected"' : '' ; ?>>Cấp độ 3</option>
			</select>
		</div>
	</div>
	<div class="col-xs-3">
		<p><b>Điểm số cho câu hỏi</b></p>
		<div class="form-group">
			<input type="number" class="form-control" placeholder="Nhập số điểm" value="<?php echo $data->Point ?>" id="Point" />
		</div>
	</div>
	<div class="col-xs-4">
		<?php 
		if($data->ParentID==0){
		?>	
		<p><b>Chọn hình ảnh minh họa cho câu hỏi</b></p>
		<div class="form-group">
			<input type="file" onchange="changeimages(this)" />
		</div>
		<?php 
		}else{
		?>
	</div>
	<div class="col-xs-3">
			<p><b>Là đáp án đúng</b></p>
			<div class="form-group">
				<select class="form-control" id="IsTrue">
					<option value="0" <?php echo $data->IsTrue==0 ? "selected='selected'" : '' ; ?>>False</option>
					<option value="1" <?php echo $data->IsTrue==1 ? "selected='selected'" : '' ; ?>>True</option>
				</select>
			</div>
		</div>
		<?php
		}
		?>
	</div>
	<div class="col-xs-2">
		<?php
		$thumb = file_exists($this->lib->get_thumb($data->PrimaryImage)) ? $this->lib->get_thumb($data->PrimaryImage) : '' ;
		echo "<img class='img-responsive' id='PrimaryImage' src='$thumb' />";
		?>
	</div>
</div>
<?php 
if($data->ParentID==0){
?>
<hr>
<h4>Danh sách câu trả lời</h4>
<?php 
	$answer = $this->db->query("select * from ttp_event_quiz_question where ParentID=$data->ID")->result();
	if(count($answer)>0){
		$i=1;
		foreach ($answer as $row) {
			$true = $row->IsTrue==1 ? 'checked="checked"' : '' ;
			echo '<div class="row">
					<div class="col-xs-9">
						<input type="radio" name="IsTrue" '.$true.' style="margin-right:10px" id="data'.$i.'">
						<label for="data'.$i.'">'.$row->Title.'</label>
					</div>
					<div class="col-xs-3">
						<a onclick="loadeditform('.$row->ID.')"><i class="fa fa-pencil"></i> Edit</a> | 
						<a onclick="remove_answer(this,'.$row->ID.')"><i class="fa fa-trash"></i> Remove</a>
					</div>
				</div>';
			$i++;
		}
	}
}
?>
<hr>
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-danger" onclick="save(this)"><i class="fa fa-pencil"></i> Lưu thông tin</a> 
		<?php 
		if($data->ParentID==0){
			echo '<a class="btn btn-success" onclick="addnew('.$data->ID.')"><i class="fa fa-plus"></i> Thêm câu trả lời</a>';
		}else{
			echo '<a class="btn btn-default" onclick="loadeditform('.$data->ParentID.')"><i class="fa fa-long-arrow-left"></i> Quay lại câu hỏi</a>';
		}
		?>
	</div>
</div>
<script>
	function save(ob){
		var data = {
			Title: $("#Title").val(),
			IsTrue: $("#IsTrue").val(),
			Point: $("#Point").val(),
			Level: $("#Level").val(),
			ParentID: $("#ParentID").val(),
			ID:    $("#ObjectID").val()
		};
		if($("#Title").val()==''){
			$("#Title").focus();
			return;
		}
		$(ob).addClass("saving");
		$.ajax({
			url: "<?php echo base_url().ADMINPATH.'/report/import_customer/' ?>save_question",
            type: "POST",
            data: "Data="+JSON.stringify(data),
            dataType: "json",
            success: function(result){
            	$(ob).removeClass("saving");
                if(result.error==0){
                	$("#alert-message").html("Lưu thông tin thành công!").removeClass('hidden').removeClass('alert-danger').addClass('alert-success');
                }else{
                	$("#alert-message").html("Lưu không thành công!").removeClass('hidden').removeClass('alert-success').addClass('alert-danger');
                }
            }
		});
	}

	function remove_answer(ob,id){
		$.ajax({
			url: "<?php echo base_url().ADMINPATH.'/report/import_customer/' ?>remove_answer",
            type: "POST",
            data: "ID="+id,
            dataType: "text",
            success: function(result){
            	$(ob).parent('.col-xs-3').parent('.row').remove();
            }
		});
	}

	function changeimages(ob){
		var file = $(ob)[0].files[0];
		var fd = new FormData();
		fd.append('file', file);
		fd.append('ID',<?php echo $data->ID ?>);
	    var xhr = new XMLHttpRequest();
		xhr.open('POST', "<?php echo base_url().ADMINPATH.'/report/import_customer/' ?>"+"changeimagerow", true);
	    xhr.onload = function() {
		    if (this.status == 200) {
		        if(xhr.responseText!="False"){
		        	$("#PrimaryImage").attr('src',xhr.responseText);
		        }else{
		        	alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu . Vui lòng kiểm tra lại đường truyền !");
		        }
		    };
	    };
	    xhr.send(fd);
	};

</script>