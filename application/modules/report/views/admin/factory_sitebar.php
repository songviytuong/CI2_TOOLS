<?php 
$url = base_url().ADMINPATH.'/report';
$segment = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-th fa-fw"></i> Quản lý sản xuất</p>
    <ul>
        <li><a href="<?php echo $url.'/factory/qrcode' ?>" <?php echo $segment_current_bonus=="qrcode" ? "class='active'" : '' ; ?>>Danh mục QR code</a></li>
        <li><a href="<?php echo $url.'/factory/barcode' ?>" <?php echo $segment_current_bonus=="barcode" ? "class='active'" : '' ; ?>>Danh mục Barcode thùng</a></li>
        <li><a href="<?php echo $url.'/factory/map_qrcode_barcode' ?>" <?php echo $segment_current_bonus=="map_qrcode_barcode" ? "class='active'" : '' ; ?>>Quét QR Code & thùng</a></li>
        <li><a href="<?php echo $url.'/factory/map_qrcode_barcode_single' ?>" <?php echo $segment_current_bonus=="map_qrcode_barcode_single" ? "class='active'" : '' ; ?>>Quét QR Code lẻ</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>

<script>
    $(".sitebar ul li p i").click(function(){
        $(".sitebar ul li ul").slideUp();
        $(".sitebar ul li p i.fa").removeClass('fa-caret-up').addClass('fa-caret-down');
        $(this).removeClass('fa-caret-down').addClass('fa-caret-up');
        $(this).parent('p').parent('li').find('ul').slideDown();
    });
</script>
