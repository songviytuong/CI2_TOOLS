<?php
$numday_start_group1 = (strtotime(date('Y-m-d', time())) - strtotime($startday)) / (3600 * 24);
$numday_stop_group1 = (strtotime(date('Y-m-d', time())) - strtotime($stopday)) / (3600 * 24);
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1><a href="<?=$base_link;?>"><span style="text-decoration: underline;">SALES ORDER</span></a> | <a href="<?=$base_link.'pickup_plan';?>">PICK-UP PLAN</a></h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <table class="table table-bordered table-hovered">
                <tr style="background:#EEE;">
                    <th style="text-align: center; vertical-align: middle;"><input type="checkbox" /></th>
                    <th style="text-align: center; vertical-align: middle;">#</th>
                    <th style="text-align: center;">Order #
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">Order Date
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">Customer
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">Address
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">City
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">Dist
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">Wd
                        <br/><input type="text" class="form-control"/>
                    </th>
                    <th style="text-align: center;">Item</th>
                    <th style="text-align: center; width:150px;">Default Store
                        <br/><select class="form-control">
                            <option>---</option>
                        </select>
                    </th>
                    <th style="text-align: center; width:150px;">Pick Status
                        <br/><select class="form-control">
                            <option value="-1" selected="" disabled="">-- Select --</option>
                            <?php
                                foreach($pick_status as $item){
                            ?>
                            <option><?=$item;?></option>
                            <?php } ?>
                        </select>
                    </th>
                    <th style="text-align: center; vertical-align: middle; width:150px;">Action
                    <br/>
                        <select class="form-control">
                            <option value="-1" selected="" disabled="">-- Select --</option>
                            <?php
                                foreach($store_option as $item){
                            ?>
                            <option><?= $item['store_name'];?></option>
                            <?php } ?>
                        </select>
                    </th>
                </tr>
                <?php 
                $i=0;
                foreach($orders_data as $item) : 
                    $i++;
                    ?>
                <tr>
                    <td style="text-align: center; vertical-align: middle;"><input type="checkbox" /></td>
                    <td style="text-align: center; vertical-align: middle;"><?=$i;?></td>
                    <td style="text-align: center; vertical-align: middle;"><?=$item['MaDH'];?></td>
                    <td><?=$item['Ngaydathang'];?></td>
                    <td><?=$item['Name'];?></td>
                    <td><?=$item['AddressOrder'];?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: center; vertical-align: middle;"><?=$item['SoluongSP'];?></td>
                    <td style="text-align: center; vertical-align: middle;">Trường Sơn</td>
                    <td style="text-align: center; vertical-align: middle;">Draft</td>
                    <td style="text-align: center; vertical-align: middle;"><a href="">Change Store</a></td>
                </tr>
                <?php endforeach; ?>
            </table>
            <div class="text-right"><button class="btn btn-success">Build Pick-Up Plan</button></div>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url() . ADMINPATH . "/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1 == 0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')"; ?>,
            endDate: <?php echo $numday_stop_group1 == 0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')"; ?>,
            minDate: '01/01/2016',
            maxDate: '<?php echo date("m/d/Y", time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1 == 0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')"; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1 == 0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')"; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>