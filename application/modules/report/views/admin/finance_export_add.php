<div class="containner">
	<div class="row">
		<div class="col-xs-12"><h4 style="margin-top:0px;"><b>Thông tin phiếu chi</b></h4></div>
	</div>
	<hr style="margin: 0px 0px 15px 0px;padding:0px;">
	<div class="row">
		<label class="col-xs-2 text-right">Loại phiếu chi :</label>
		<div class="col-xs-3">
			<p><label for="TypeImport01"><input type="radio" name="Type" id="TypeImport01" checked="checked" value="0" /> Thực chi</label><br><label for="TypeImport02"><input type="radio" name="Type" id="TypeImport02" value="1" /> Dự kiến chi</label></p>
		</div>
	</div>
	<div class="row">
		<label class="col-xs-2 text-right">Ngày chi tiền :</label>
		<div class="col-xs-3">
			<input type="text" class="form-control" id="DayFinance" value="<?php echo date('Y-m-d') ?>">
		</div>
		<label class="col-xs-2 text-right">Người giao dịch :</label>
		<div class="col-xs-3">
			<select id="ReciveToID" class="form-control">
				<option value="0">-- Chọn người giao dịch --</option>
				<?php 
				$userlist = $this->db->query("select * from ttp_report_finance_person order by Type")->result();
				if(count($userlist)>0){
					$temp = '';
					$i=1;
					foreach($userlist as $row){
						if($temp!=$row->Type){
							$temp = $row->Type;
							if($i>1){
								echo "</optgroup>";
							}
							$title = $row->Type==1 ? "Tổ chức" : "Cá nhân" ;
							echo "<optgroup label='$title'>";
						}
						echo "<option value='$row->ID'>$row->Name</option>";
						$i++;
					}
					echo "</optgroup>";
				}
				?>
			</select>
		</div>
		<div class="col-xs-2">
			<button class="btn btn-default" data-toggle="modal" data-target="#myModal_addperson"><i class="fa fa-plus"></i> Thêm người</button>
		</div>
	</div>
    <div class="row">
		<label class="col-xs-2 text-right">Nguồn chi :</label>
		<div class="col-xs-3">
			<select name="" id="SourceImportID" class="form-control" onchange="change_source(this)">
				<?php 
				$arr_finance_source = $this->lib->get_config_define("finance_export","finance_export_source",0,'code','asc');
				if(count($arr_finance_source)>0){
					foreach ($arr_finance_source as $key => $value) {
						echo '<option value="'.$key.'">'.$value.'</option>';
					}
				}
				?>
			</select>
		</div>
		<label class="col-xs-2 text-right">Mã chứng từ :</label>
		<div class="col-xs-3">
			<input type="text" class="form-control" id="OrderCode" onchange="check_order_code(this)">
			<a style="padding:5px 0px;display:block;" onclick="find_ordercode()"><i class="fa fa-search" aria-hidden="true"></i> <small>Tìm kiếm chứng từ</small></a>
		</div>
		<div class="col-xs-2" id="warning-code"></div>
	</div>
	<div class="row">
		<div class="col-xs-12"><h4><b>Thông tin thanh toán</b></h4></div>
	</div>
	<hr style="margin: 0px 0px 15px 0px;padding:0px;">
	<div class="row">
		<label class="col-xs-2 text-right">Số tiền chi :</label>
		<div class="col-xs-3">
			<input type="number" class="form-control" id="MoneyImport" value="0">
		</div>
	</div>
	<div class="row">
		<label class="col-xs-2 text-right">Hình thức thanh toán :</label>
		<div class="col-xs-3">
			<p>
				<label for="PaymentImport01"><input type="radio" name="PaymentImport" id="PaymentImport01" value="0" onclick="ChangePaymentType(this)" checked="checked" /> Tiền mặt</label><br>
				<label for="PaymentImport02"><input type="radio" name="PaymentImport" id="PaymentImport02" value="1" onclick="ChangePaymentType(this)" /> Chuyển khoản</label>
			</p>
		</div>
	</div>
	<div class="row">
		<label class="col-xs-2 text-right">Quỹ tiền mặt :</label>
		<div class="col-xs-3 text-mute">
			<select name="BankAccount" id="CashID" class="form-control">
				<option value="0">-- Chọn quỹ tiền --</option>
				<?php 
				$bank = $this->db->query("select * from ttp_report_finance_cash")->result();
				if(count($bank)>0){
					foreach ($bank as $row) {
						echo "<option value='$row->ID'>$row->Title (".number_format($row->Price)."đ)</option>";
					}
				}
				?>
			</select>
		</div>
		<label class="col-xs-2 text-right">Tài khoản ngân hàng :</label>
		<div class="col-xs-3 text-mute">
			<select name="BankAccount" id="BankAccountID" class="form-control" readonly="true">
				<option value="0">-- Chọn số tài khoản -- </option>
				<?php 
				$bank = $this->db->query("select * from ttp_report_finance_bankaccount")->result();
				if(count($bank)>0){
					foreach ($bank as $row) {
						echo "<option value='$row->ID'>$row->Title (".number_format($row->Price)."đ)</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<div class="row">
		<label class="col-xs-2 text-right">Diễn giải phiếu chi :</label>
		<div class="col-xs-10">
			<textarea class="form-control" rows="3" id="Note"></textarea>
		</div>
	</div>
	<div class="row">
		<label class="col-xs-2"></label>
		<div class="col-xs-2">
			<button class="btn btn-primary" onclick="save_information(this)">Lưu thông tin</button>
		</div>
		<div class="col-xs-8" id="warning-form"></div>
	</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tìm kiếm mã chứng từ lập phiếu chi</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-xs-4">
        		<input type="text" class="form-control" style="margin-bottom:15px;" placeholder="Nhập từ khóa muốn tìm kiếm" onchange="find_order(this)" />
        	</div>
        	<div class="col-xs-8"><p style="padding:8px 0px;"><small>(*) Có thể tìm kiếm theo mã po hoặc nhà cung cấp</small></p></div>
        </div>
        <div class="row" id="content-load-order"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="myModal_addperson">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Thêm người giao dịch</h4>
      </div>
      <div class="modal-body">
        <p>Đối tượng người dùng</p>
        <div>
        	<p><label for="TypePerson0"><input type="radio" name="TypePerson" id="TypePerson0" value="0" checked="checked"> Cá nhân</label><label style="margin-left:15px;" for="TypePerson1"><input type="radio" name="TypePerson" id="TypePerson1" value="1"> Tổ chức</label></p>
        </div>
        <hr>
        <p>Tên cá nhân / tổ chức</p>
        <div><input type="text" class="form-control" id="NamePerson" /></div>
        <p style="margin-top: 20px;">Liên kết tài khoản</p>
        <div>
        	<select id="ConnectID" class="form-control">
        		<option value="0">-- Không có liên kết --</option>
        		<?php 
        		$userlist = $this->db->query("select ID,UserName from ttp_user")->result();
        		if(count($userlist)>0){
        			foreach($userlist as $row){
        				echo "<option value='$row->ID'>$row->UserName</option>";
        			}
        		}
        		?>
        	</select>
        </div>
      </div>
      <div class="modal-footer">
      	<div class="alert alert-danger hidden" id="alert_add_person_box"></div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="add_person_finance(this)">Lưu thông tin</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style>
	.text-mute{color:#ccc;}
	.daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#DayFinance').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

    function add_person_finance(ob){
    	$(ob).addClass("saving");
		var data = {
			NamePerson:$("#NamePerson").val(),
			TypePerson:$("input:radio[name='TypePerson']:checked").val(),
			ConnectID:$("#ConnectID").val()
		};
		$.post("<?php echo base_url().ADMINPATH.'/report/finance_import/add_new_person' ?>",data,function(result){
			$(ob).removeClass("saving");
			if(result.error==1){
				$("#alert_add_person_box").removeClass('hidden').html(result.message);
			}else{
				$("#alert_add_person_box").addClass('hidden').html('');
				$("#myModal_addperson").modal("hide");
				$("#ReciveToID").load("<?php echo base_url().ADMINPATH.'/report/finance_import/load_new_person/' ?>"+result.id);
			}
		},"json");
    }

    function select_this_order(MaDH,Pay){
    	$("#myModal").modal("hide");
    	$("#MoneyImport").val(Pay);
    	$("#OrderCode").val(MaDH);
    }

    function find_order(ob){
    	var data = $(ob).val();
    	$.post("<?php echo base_url().ADMINPATH.'/report/finance_export/get_order' ?>",{key:data},function(result){
    		$(".modal-body #content-load-order").html(result);
    	});
    }

    function find_ordercode(){
    	$("#myModal").modal("show");
    	$.post("<?php echo base_url().ADMINPATH.'/report/finance_export/get_order' ?>",function(result){
    		$(".modal-body #content-load-order").html(result);
    	});
    }

	function change_source(ob){
		var data = $(ob).val();
		if(data==1){
			$("#warning-code").html("");
		}
	}

	function ChangePaymentType(ob){
		var data = $(ob).val();
		if(data==0){
			$("#CashID").attr("readonly",false);
			$("#BankAccountID").attr("readonly",true);
		}else{
			$("#CashID").attr("readonly",true);
			$("#BankAccountID").attr("readonly",false);
		}
	}

	function check_order_code(ob){
		var data = $(ob).val();
		var SourceImportID = $("#SourceImportID").val();
		if(SourceImportID==0){
			$.post("<?php echo base_url().ADMINPATH.'/report/finance_export/check_order_code' ?>",{data:data},function(result){
				if(result.error==0){
					$("#warning-code").html('<p style="line-height:24px;" class="text-success"><i class="fa fa-check"></i> Mã hợp lệ</p>');
				}else{
					$("#warning-code").html('<p style="line-height:24px;" class="text-danger"><i class="fa fa-warning"></i> Mã không hợp lệ</p>');
				}
			},'json');
		}
	}

	function save_information(ob){
		$(ob).addClass("saving");
		var data = {
			BankAccountID:$("#BankAccountID").val(),
			MoneyImport:$("#MoneyImport").val(),
			SourceImport:$("#SourceImportID").val(),
			PaymentImport:$("input:radio[name='PaymentImport']:checked").val(),
			TypeImport:$("input:radio[name='Type']:checked").val(),
			OrderCode:$("#OrderCode").val(),
			Note:$("#Note").val(),
			DayFinance:$("#DayFinance").val(),
			CashID:$("#CashID").val(),
			ReciveToID:$("#ReciveToID").val()
		};
		$.post("<?php echo base_url().ADMINPATH.'/report/finance_export/add_new' ?>",data,function(result){
			if(result.error==1){
				$(ob).removeClass("saving");
				$("#warning-form").html('<p style="line-height:36px;" class="text-danger"><i class="fa fa-warning"></i> '+result.message+'</p>');
			}else{
				window.location = "<?php echo base_url().ADMINPATH.'/report/finance_export/' ?>";
			}
		},"json");
	}
</script>