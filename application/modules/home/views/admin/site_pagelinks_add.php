<div class="">
    
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>
    </div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel tab1 tabcontent">
                <div class="x_title">
                    <h2>PAGE LINKS <small> / Add new page</small></h2>
                    <div class="clearfix"></div>
                </div>
                
                <div class="x_content">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">ClassName <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="ClassName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Link 
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="Link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Title <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="Title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Published" value="1" checked="checked"> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Published" value="0"> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">STT After Page <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="STT" class="form-control">
                                    <option value='0'>-- Show first --</option>
                                    <?php 
                                        $result = $this->db->query("select STT,Title from ttp_pagelinks")->result();
                                        if(count($result)>0){
                                            foreach($result as $row){
                                                echo "<option value='$row->STT'>$row->Title</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                </div>
                <div class="x_title">
                    <h2>META TAGS <small> / Config meta tag</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">MetaTitle
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="MetaTitle">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Keywords</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="MetaKeywords" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="MetaDescription" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Ext</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="MetaExt" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                </div>
            </div>
            <!-- end VIETNAM -->
            <div class="x_panel tab2 tabcontent">
                <div class="x_title">
                    <h2>PAGE LINKS <small> / Add new page</small></h2>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Title </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="Title_en">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Link 
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="Link_en">
                            </div>
                        </div>
                </div>
                <div class="x_title">
                    <h2>META TAGS <small> / Config meta tag</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">MetaTitle
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" name="MetaTitle_en">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Keywords</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="MetaKeywords_en" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Description</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="MetaDescription_en" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Meta Ext</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="MetaExt_en" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
