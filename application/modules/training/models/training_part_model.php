<?php
class Training_part_model extends CI_Model
{
    private $_tableName = "ttp_training_part";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getListByScienceId($scienceid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($scienceid !=null){
            $sql .=" and scienceid = $scienceid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function insert($data){
        return $this->db->insert($this->_tableName,$data);
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    
}?>