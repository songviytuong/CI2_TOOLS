<div class="containner opensitebar">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÊM KHO MỚI</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Lưu thông tin</button>
				</div>
			</div>
			<div class="box_content_warehouse">
				<div class="block1">
					<div class="row" style="margin:0px">
						<div class="form-group">
							<label class="col-xs-3 control-label">Mã kho hàng </label>
							<label class="col-xs-1 control-label"></label>
							<label class="col-xs-3 control-label">Tên kho hàng </label>
							<label class="col-xs-1 control-label"></label>
							<label class="col-xs-3 control-label">Phân loại kho hàng </label>
							<label class="col-xs-1 control-label"></label>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<div class="col-xs-3">
								<input type='text' name='MaKho' class="form-control required" required />
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-3">
								<input type='text' name='Title' class="form-control required" required />
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-3">
								<select name="" class="form-control">
									<option>-- Chọn phân loại kho --</option>
								</select>
							</div>
							<div class="col-xs-1"></div>
							
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<label class="col-xs-3 control-label">Màu sắc đại diện kho </label>
							<label class="col-xs-1 control-label"></label>
							<label class="col-xs-3 control-label">Số dt liên hệ thủ kho (1)</label>
							<label class="col-xs-1 control-label"></label>
							<label class="col-xs-3 control-label">Số dt liên hệ thủ kho (1) </label>
							<label class="col-xs-1 control-label"></label>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<div class="col-xs-3">
								<input type='color' name="Color" class="form-control" />
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-3">
								<input type='text' name='Phone1' class="form-control" />
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-3">
								<input type='text' name='Phone2' class="form-control" />
							</div>
							<div class="col-xs-1"></div>
							
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<label class="col-xs-3 control-label">Khu vực kho </label>
							<label class="col-xs-1 control-label"></label>
							<label class="col-xs-3 control-label">Tỉnh / Thành phố</label>
							<label class="col-xs-1 control-label"></label>
							<label class="col-xs-3 control-label">Quận / Huyện</label>
							<label class="col-xs-1 control-label"></label>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<div class="col-xs-3">
								<select name='AreaID' id="Khuvuc" class="form-control">
									<option value="">-- Chọn khu vực --</option>
				    				<?php 
				    				$area = $this->db->query("select ID,Title from ttp_report_area")->result();
				    				if(count($area)>0){
				    					foreach($area as $row){
				    						$selected = $row->ID==$data->AreaID ? "selected='selected'" : '' ;
				    						echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
								</select>
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-3">
								<select name='CityID' id="Tinhthanh" class="form-control">
									<option value="">-- Chọn tỉnh thành --</option>
								</select>
							</div>
							<div class="col-xs-1"></div>
							<div class="col-xs-3">
								<select name='DistrictID' id="Quanhuyen" class="form-control">
									<option value="">-- Chọn quận huyện --</option>
								</select>
							</div>
							<div class="col-xs-1"></div>
							
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<label class="col-xs-6 control-label">Địa chỉ thực tế kho hàng</label>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<div class="col-xs-12">
								<input type='text' name='Address' placeholder="Nhập địa chỉ thực tế của kho hàng .." class="form-control" />
							</div>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<div class="col-xs-6"><h3>Tài khoản quản lý kho</h3></div>
						</div>
					</div>
					<hr style="margin:0px;padding:0px">
					<div class="row" style="margin:0px">
						<div class="form-group">
							<label class="col-xs-12 control-label">Chọn tài khoản sau đó bấm "add" để thêm tài khoản đó vào danh sách quản lý kho .</label>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<?php 
							$user = $this->db->query("select ID,UserName from ttp_user where UserType=2 or UserType=8")->result();
							if(count($user)>0){
								$user_list = array();
								echo "<div class='col-xs-3'><select id='user_warehouse' class='form-control'>";
								foreach($user as $row){
									$user_list[$row->ID] = $row->UserName;
									echo "<option value='$row->ID'>$row->UserName</option>";
								}
								echo "</select></div>";
								echo "<div class='col-xs-9'><a class='btn btn-primary add_user' style='margin-left:10px;' onclick='add_user()'><i class='fa fa-plus'></i> ADD</a></div><div class='col-xs-12' style='margin-top:10px'></div>";
							}
							?>
						</div>
					</div>
					<div class="row" style="margin:0px">
						<div class="form-group">
							<div class="col-xs-6"><h3>Tài khoản quét qrcode tại kho này</h3></div>
						</div>
					</div>
					<hr style="margin:0px;padding:0px">
					<div class="row" style="margin:0px">
						<div class="form-group">
							<?php 
							$qrcode = $this->db->query("select ID,UserName from ttp_user where UserType=12 and Published=1")->result();
							if(count($qrcode)>0){
								foreach($qrcode as $row){
									echo "<div class='col-xs-2'><input name='Qrcode[]' value='$row->ID' type='checkbox' /> $row->UserName</div>";
								}
							}else{
								echo "<div class='alert alert-danger'>Hệ thống hiện tại không có danh sách tài khoản loại quét mã QR Code !</div>";
							}
							?>
						</div>
					</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
</div>
<script type="text/javascript">
	var link = "<?php echo base_url().ADMINPATH.'/report/' ?>";

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_city_by_area",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_district_by_city",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	                var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            	$("#KhoID").html(jsonobj.WarehouseHtml);
	            }
	        });
		}
	});

	$("#add_position_to_warehouse").click(function(){
		$("#table_data .last_tr").remove();
		$("#table_data").append("<tr><td><input type='checkbox' class='selected_products' /></td><td><input type='text' name='Position[]' /></td><td><input type='text' name='Rack[]' /></td><td><input type='text' name='Colum[]' /></td><td><input type='text' name='Row[]' /></td></tr>");
	});

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				$(this).parent('td').parent('tr').remove();
			}
		});
	});

	var celldata = [];

	function add_user(){
		var user = $("#user_warehouse").val();
		var name = $("#user_warehouse option:selected" ).text();
		if(jQuery.inArray( "data"+user, celldata )<0){
			$("#user_warehouse").parent('td').append("<li class='list_owner'>"+name+"<a onclick='remove_user(this,"+user+")'>[x]</a> <input type='hidden' name='Manager[]' value='"+user+"' /></li>");
			celldata.push("data"+user);
		}
	}

	function remove_user(ob,user){
		$(ob).parent('li').remove();
		var index = celldata.indexOf("data"+user);
		celldata.splice(index, 1);
	}
</script>
</div>