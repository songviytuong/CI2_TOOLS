<?php 
class Partner_affiliate extends Admin_Controller { 
 
    public $user;
    public $classname="partner_affiliate";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/supplier_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function index(){
        
    }

    public function set_day(){
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("affiliate_startday",$startday);
                    $this->session->set_userdata("affiliate_stopday",$stopday);
                }
            }
        }
        echo 'OK';
    }

    public function report(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Affiliate | Partners');
        $startday = $this->session->userdata("affiliate_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("affiliate_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $result = $this->db->query("select ID,Total,AffiliateCost,Chiphi,Reduce,Status,Chietkhau,Ngaydathang from ttp_report_order where date(Ngaydathang)>='$startday' and date(Ngaydathang)<='$stopday' and AffiliateID=".$this->user->ID)->result();
        $customers = $this->db->query("select CustomerID from ttp_report_order where date(Ngaydathang)<'$startday' and AffiliateID=".$this->user->ID)->result();
        $data = array(
            'data'      => $result,
            'oldcustomer'=> $customers,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'base_link' => base_url().ADMINPATH.'/report/partner_affiliate/'
        );
        $view = "admin/import_affiliate_report";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function report_details(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Affiliate | Partners');
        $startday = $this->session->userdata("affiliate_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("affiliate_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $result = $this->db->query("select a.ID,a.Total,a.AffiliateCost,a.Chiphi,a.Reduce,a.Status,a.Chietkhau,a.Ngaydathang,a.DateSuccess,b.Name,a.CustomerID from ttp_report_order a,ttp_report_customer b where date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and b.ID=a.CustomerID and a.AffiliateID=".$this->user->ID."")->result();
        $customers = $this->db->query("select CustomerID,count(ID) as Total from ttp_report_order where date(Ngaydathang)<'$startday' group by CustomerID")->result();
        $data = array(
            'data'      => $result,
            'startday'  => $startday,
            'oldcustomer'=> $customers,
            'stopday'   => $stopday,
            'base_link' => base_url().ADMINPATH.'/report/partner_affiliate/'
        );
        $view = "admin/import_affiliate_report_details";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function analytics(){
    	$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Affiliate | Partners');
        $startday = $this->session->userdata("affiliate_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("affiliate_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $sql = $this->user->IsAdmin==1 ? "" : " and a.AffiliateID=".$this->user->ID;
        $result = $this->db->query("select a.*,b.Title,b.MaSP from ttp_report_analytics a,ttp_report_products b where a.ProductsID=b.ID and date(a.AnalyticsDay)>='$startday' and date(a.AnalyticsDay)<='$stopday' $sql")->result();
        $sql = $this->user->IsAdmin==1 ? "" : " and a.AffiliateID>0";
        $ordersuccess = $this->db->query("select count(ID) as total from ttp_report_order a where date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' $sql")->row();
        $ordersuccess = $ordersuccess ? $ordersuccess->total : 0 ;
        $data = array(
        	'ordersuccess'=> $ordersuccess,
            'data'      => $result,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'base_link' => base_url().ADMINPATH.'/report/partner_affiliate/'
        );
        $view = "admin/import_affiliate_analytics";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function voucher(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Affiliate | Partners');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";

        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $str = "";
        if($keywords!=''){
            $str = " where Code='$keywords'";
        }
        $nav = $this->db->query("select count(1) as nav from ttp_report_voucher $str")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $result = $this->db->query("select * from ttp_report_voucher $str order by ID DESC $limit_str")->result();

        $data = array(
            'data'      => $result,
            'start'     =>  $start,
            'find'      =>  $nav,
            'base_link' =>  base_url().ADMINPATH.'/report/partner_affiliate/',
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/partner_affiliate/voucher',5,$nav,$this->limit)
        );
        $view = "admin/import_affiliate_voucher";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function create_link(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Affiliate create links | Partners');
        
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/partner_affiliate/'
        );
        $view = "admin/import_affiliate_create_link";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function information(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1){
            $this->template->add_title('Affiliate information | Partners');
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";

            $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
            $str = "";
            if($keywords!=''){
                $str = " and UserName like '%$keywords%'";
            }
            $nav = $this->db->query("select count(1) as nav from ttp_user where UserType=16 $str")->row();
            $nav = $nav ? $nav->nav : 0 ;
            $result = $this->db->query("select * from ttp_user where UserType=16 $str order by ID DESC $limit_str")->result();
            $data = array(
                'data'      => $result,
                'start'     =>  $start,
                'find'      =>  $nav,
                'base_link' =>  base_url().ADMINPATH.'/report/partner_affiliate/',
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/partner_affiliate/information',5,$nav,$this->limit)
            );
            $view = "admin/import_affiliate_list";
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH.'/report/partner_affiliate/information_user/'.$this->user->ID);
        }
    }

    public function update_infomation(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        if($ID==$this->user->ID){
            $Fullname = isset($_POST['Fullname']) ? $_POST['Fullname'] : '' ;
            $Birthday = isset($_POST['Birthday']) ? $_POST['Birthday'] : '' ;
            $CMND = isset($_POST['CMND']) ? $_POST['CMND'] : '' ;
            $Bankcode = isset($_POST['Bankcode']) ? $_POST['Bankcode'] : '' ;
            $Banktitle = isset($_POST['Banktitle']) ? $_POST['Banktitle'] : '' ;
            $MST = isset($_POST['MST']) ? $_POST['MST'] : '' ;
            $Oldpassword = isset($_POST['Oldpassword']) ? $_POST['Oldpassword'] : '' ;
            $Newpassword = isset($_POST['Newpassword']) ? $_POST['Newpassword'] : '' ;
            $data = array(
                'Fullname'=>$Fullname,
                'Birthday'=>$Birthday,
                'CMND'=>$CMND,
                'Bankcode'=>$Bankcode,
                'Banktitle'=>$Banktitle,
                'MST'=>$MST
            );
            $sql = array(
                'Data'=>json_encode($data)
            );
            $error_pass = 0;
            if($Newpassword!='' && strlen($Newpassword)>5){
                if($this->user->Password==sha1($Oldpassword)){
                    $sql['Password'] = sha1($Newpassword);
                    $error_pass = 2;
                }else{
                    $error_pass=1;
                }
            }
            if(isset($_FILES['Image_upload'])){
                if($_FILES['Image_upload']['error']==0){
                    $this->upload_to = "user_thumb/$NewID";
                    $sql['Thumb'] = $this->upload_image_single();
                }
            }
            $this->db->where('ID',$ID);
            $this->db->update("ttp_user",$sql);
            $str = $error_pass>0 ? '?state='.$error_pass : '' ;
            redirect(ADMINPATH.'/report/partner_affiliate/information_user/'.$ID.$str);
        }
        echo "You can't update information of difference account .";
    }

    public function information_user($id=0){
        if($id==$this->user->ID){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
            $this->template->add_title('Affiliate information | Partners');
            $info = $this->db->query("select * from ttp_user where ID=$id")->row();
            $data = array(
                'info'      => $info,
                'base_link' => base_url().ADMINPATH.'/report/partner_affiliate/'
            );
            $view = "admin/import_affiliate_information";
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }
    }

    public function edit_affiliate($id=0){
        if($this->user->IsAdmin==1){
            $info = $this->db->query("select * from ttp_user where ID=$id")->row();
            $data = array(
                'info'      => $info
            );
            $view = "admin/import_affiliate_viewaffiliate";
            $this->load->view($view,$data);
        }
    }

    public function active_create_link(){
        $link = isset($_GET['link']) ? strip_tags($_GET['link']) : '' ;
        echo "$link?affi=".$this->user->ID;
    }

    public function add_voucher(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->load->view("import_affiliate_add_voucher");
    }

    public function edit_voucher($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $data = $this->db->query("select * from ttp_report_voucher where ID=$id")->row();
        if($data){
            $this->load->view("import_affiliate_edit_voucher",array('data'=>$data));
        }
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $check = $this->db->query("select ID from ttp_report_order where VoucherID=$id")->row();
        if(!$check){
            $this->db->query("delete from ttp_report_voucher where ID=$id");
            redirect(ADMINPATH."/report/partner_affiliate/voucher");
        }else{
            echo "Can't delete this voucher because this voucher has used. <a href='".ADMINPATH."/report/partner_affiliate/voucher"."'>Go back</a>";
        }
    }

    public function update_voucher(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $Startday = isset($_POST['Startday']) ? $_POST['Startday'] : '' ;
        $Stopday = isset($_POST['Stopday']) ? $_POST['Stopday'] : '' ;
        $UseOne = isset($_POST['UseOne']) ? (int)$_POST['UseOne'] : 0 ;
        $Amount = isset($_POST['Amount']) ? (int)$_POST['Amount'] : 0 ;
        $PercentReduce = isset($_POST['PercentReduce']) ? (int)$_POST['PercentReduce'] : 0 ;
        $PriceReduce = isset($_POST['PriceReduce']) ? (int)$_POST['PriceReduce'] : 0 ;
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        if($PercentReduce<=0 && $PriceReduce<=0){
            echo "Cảnh báo : % giảm giá hoặc giá trị giảm buộc phải có giá trị !";
            return;
        }
        if($Stopday==''){
            echo "Cảnh báo : bạn phải điền thông tin ngày hết hiệu lực của voucher !";
            return;
        }
        $data = array(
            'Startday'  =>$Startday,
            'Stopday'   =>$Stopday,
            'PercentReduce'=>$PercentReduce,
            'PriceReduce'   =>$PriceReduce,
            'UseOne'        =>$UseOne
        );
        $this->db->where('ID',$ID);
        $this->db->update('ttp_report_voucher',$data);
        redirect(ADMINPATH."/report/partner_affiliate/voucher");
    }

    public function create_voucher(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Startday = isset($_POST['Startday']) ? $_POST['Startday'] : '' ;
        $Stopday = isset($_POST['Stopday']) ? $_POST['Stopday'] : '' ;
        $UseOne = isset($_POST['UseOne']) ? (int)$_POST['UseOne'] : 0 ;
        $Amount = isset($_POST['Amount']) ? (int)$_POST['Amount'] : 0 ;
        $PercentReduce = isset($_POST['PercentReduce']) ? (int)$_POST['PercentReduce'] : 0 ;
        $PriceReduce = isset($_POST['PriceReduce']) ? (int)$_POST['PriceReduce'] : 0 ;
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        if($PercentReduce<=0 && $PriceReduce<=0){
            echo "Cảnh báo : % giảm giá hoặc giá trị giảm buộc phải có giá trị !";
            return;
        }
        if($Stopday==''){
            echo "Cảnh báo : bạn phải điền thông tin ngày hết hiệu lực của voucher !";
            return;
        }
        if($Amount>200){
            echo "Cảnh báo : bạn chỉ có thể tạo 1 lần tối đa 200 voucher !";
            return;
        }
        $max = $this->db->query("select max(ID) as ID from ttp_report_voucher")->row();
        $max = $max ? $max->ID+1 : 1 ;
        $available = $this->db->query("select Code from ttp_report_voucher where Status=0")->result();
        $arr_available = array();
        if(count($available)>0){
            foreach($available as $row){
                $arr_available[$row->Code] = '';
            }
        }
        $arr = array();
        if($Amount>0){
            $i=0;
            while ($i<$Amount) {
                $str = substr(strtoupper(sha1($max)),0,8);
                if(!isset($arr[$str]) && !isset($arr_available[$str])){
                    $arr[$str] = '';
                    $i++;
                    $max++;
                }
            }
        }
        $sql = array();
        $created = date('Y-m-d H:i:s');
        foreach($arr as $row=>$key){
            $sql[] = "('$row','$Startday','$Stopday','$PercentReduce','$PriceReduce','$created',".$this->user->ID.",$UseOne)";
        }
        if(count($sql)>0){
            $sql = "insert into ttp_report_voucher(Code,Startday,Stopday,PercentReduce,PriceReduce,Created,UserID,UseOne) values".implode(',',$sql);
            $this->db->query($sql);
        }
        redirect(ADMINPATH."/report/partner_affiliate/voucher");
    }
}
?>