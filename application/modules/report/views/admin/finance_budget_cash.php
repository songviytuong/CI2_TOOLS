<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>Báo cáo tồn quỹ tiền mặt</h1>
	    </div>
	    <div class="block2">
	    	<div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<div class="btn-group">
                <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> <?php echo $cashtitle ?></a>
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <?php 
                    $cash = $this->db->query("select * from ttp_report_finance_cash")->result();
                    if(count($cash)>0){
                    	foreach($cash as $row){
                    		echo "<li><a href=''>$row->Title (".number_format($row->Price)."đ)</a></li>";
                    	}
                    }
                    ?>
                </ul>
            </div>
    	</div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<table class="table table-bordered table-hovered">
    			<tr style="background:#EEE;">
    				<th colspan="3" class="text-center">NỘI DUNG</th>
    				<th style="width:200px;">SỐ TIỀN</th>
    			</tr>
    			<tr>
    				<th colspan="4">THU TIỀN MẶT</th>
    			</tr>
    			<tr>
    				<th>STT</th>
    				<th>NGÀY THU TIỀN</th>
    				<th>CHỨNG TỪ</th>
    				<th></th>
    			</tr>
    			<?php 
    			$total_import = 0;
    			$total_export = 0;
    			if(count($import)>0){
    				$total = 0;
    				$i=1;
    				foreach($import as $row){
    					$total = $total+$row->Price;
    					$row->Note = $row->Note!='' ? "($row->Note)" : '' ;
    					$row->Hardcode = $row->Hardcode!='' ? $row->Hardcode : '--' ;
    					echo "<tr>
    						<td>$i</td>
							<td>".date('d/m/Y',strtotime($row->Dayfinance))."</td>
							<td>$row->Hardcode $row->Note</td>
							<td>".number_format($row->Price)."</td>
    					</tr>";
    					$i++;
    				}
    				$total_import = $total;
    				echo "<tr><th class='text-right' colspan='3'>TỔNG CỘNG SỐ TIỀN THU</th><th>".number_format($total)."</th></tr>";
    			}else{
    				echo "<tr><td colspan='4' class='text-center'>Không tìm thấy dữ liệu</td></tr>";
    			}
    			?>
    			<tr>
    				<th colspan="4">CHI TIỀN MẶT</th>
    			</tr>
    			<tr>
    				<th>STT</th>
    				<th>NGÀY CHI TIỀN</th>
    				<th>CHỨNG TỪ</th>
    				<th></th>
    			</tr>
    			<?php 
    			if(count($export)>0){
    				$total = 0;
    				$i=1;
    				foreach($export as $row){
    					$total = $total+$row->Price;
    					$row->Hardcode = $row->Hardcode!='' ? $row->Hardcode : '--' ;
    					$row->Note = $row->Note!='' ? "($row->Note)" : '' ;
    					echo "<tr>
    						<td>$i</td>
							<td>".date('d/m/Y',strtotime($row->Dayfinance))."</td>
							<td>$row->Hardcode $row->Note</td>
							<td>".number_format($row->Price)."</td>
    					</tr>";
    					$i++;
    				}
    				$total_export = $total;
    				echo "<tr><th class='text-right' colspan='3'>TỔNG CỘNG SỐ TIỀN CHI</th><th>".number_format($total)."</th></tr>";
    			}else{
    				echo "<tr><td colspan='4' class='text-center'>Không tìm thấy dữ liệu</td></tr>";
    			}
    			?>
    			<tr>
    				<th colspan="4" style="background:#EEE;" class="text-center">TỔNG KẾT</th>
    			</tr>
    			<?php 
    			$cash_old = 0;
    			$cash_first = $this->db->query("select * from ttp_report_finance_cash_flow where Dayfinance<'$startday' and CashAccountID=$cashid order by Dayfinance DESC limit 0,1")->row();
    			if($cash_first){
    				$cash_old = $cash_first->Price;
    			}
    			?>
    			<tr>
    				<th colspan="3" class="text-right">TIỀN MẶT ĐẦU KỲ</th>
    				<th><?php echo number_format($cash_old) ?></th>
    			</tr>
    			<tr>
    				<th colspan="3" class="text-right">THU TIỀN MẶT TRONG KỲ</th>
    				<th><?php echo number_format($total_import) ?></th>
    			</tr>
    			<tr>
    				<th colspan="3" class="text-right">CHI TIỀN MẶT TRONG KỲ</th>
    				<th><?php echo number_format($total_export) ?></th>
    			</tr>
    			<tr>
    				<th colspan="3" class="text-right">TIỀN MẶT CUỐI KỲ</th>
    				<th><?php echo number_format($cash_old-$total_export+$total_import) ?></th>
    			</tr>
    		</table>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2016',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });

    function edit(id){
    	window.location = "<?php echo base_url().ADMINPATH.'/report/finance_export/edit_export/' ?>"+id;
    }
</script>