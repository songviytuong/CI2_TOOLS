<?php 
class Warehouse extends Admin_Controller { 
 	public $user;
 	public $classname="warehouse";

    public function __construct() { 
        parent::__construct(); 
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$this->template->add_title('Warehouse | Report Tools');
		$this->template->write_view('content','admin/warehouse_content');
		$this->template->render();
	}

    public function report_inventory(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_warehouse_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_warehouse_startday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $import = $this->db->query("select a.NgayNK,a.ProductionID,a.KhoID,b.ProductsID,b.Amount,b.TotalVND,c.Title,d.Title as ProductionName from ttp_report_inventory_import a,ttp_report_inventory_import_details b,ttp_report_products c,ttp_report_production d where a.ID=b.ImportID and a.ProductionID=d.ID and b.ProductsID=c.ID and date(a.NgayNK)>='$startday' and date(a.NgayNK)<='$stopday' order by a.ProductionID ASC")->result();
        $export = $this->db->query("select a.Ngaydathang,b.Amount,b.ProductsID,c.Title from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.Status=0")->result();
        $inventory = $this->db->query("select * from ttp_report_inventory where date(DateInventory)>='$startday' and date(DateInventory)<='$stopday' and LastEdited=1")->result();
        $purchase = $this->db->query("select a.DatePO,a.ProductionID,b.ProductsID,b.Amount,b.TotalVND from ttp_report_perchaseorder a,ttp_report_perchaseorder_details b where a.ID=b.POID and date(a.DatePO)>='$startday' and date(a.DatePO)<='$stopday' order by a.ProductionID ASC")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse/',
            'start'     =>  $start,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'import'    => $import,
            'export'    => $export,
            'inventory' => $inventory,
            'purchase'  => $purchase
        );
        $this->template->add_title('Warehouse report | Warehouse Report Tools');
        $view = 'warehouse_report';
        $this->template->write_view('content','admin/'.$view,$data);
        $this->template->render();
    }

    public function preview_log($date=''){
        if(file_exists('log/warehouse/'.$date.'.txt')){
            $data = file_get_contents('log/warehouse/'.$date.'.txt');
            $data = explode('\n',$data);
            $result = array(
                'data'=>$data,
                'date'=>$date,
                'base_link' => base_url().ADMINPATH.'/report/warehouse/preview_log/'.$date.'/'
            );
            $this->load->view("warehouse_preview_log",$result);
        }
    }

    public function get_products_list(){
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select MaSP,Title,ID from ttp_report_products where Title like '%$keywords%' or MaSP like '%$keywords%' limit 0,20")->result();
        if(count($result)>0){
            foreach ($result as $key => $value) {
                echo "<p><a onclick='apply_products(this,$value->ID)'>($value->MaSP) | $value->Title</a></p>";
            }
        }
    }

    public function inventory_details(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $product = isset($_GET['product']) ? (int)$_GET['product'] : 0 ;
        $sql = $product>0 ? " and b.ID=$product" : '' ;
        $result = $this->db->query("select a.*,b.Title,b.MaSP,c.MaKho from ttp_report_inventory_details a,ttp_report_products b,ttp_report_warehouse c where a.WarehouseID=c.ID and a.ProductsID=b.ID".$sql)->result();
        $data = array(
            'Data'  => $result,
            'Products'=>$product
        );
        $this->template->write_view('content','admin/warehouse_inventory_products_details',$data);
        $this->template->render();
    }

    public function put_products(){
        $id = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $check = $this->db->query("select * from ttp_report_inventory_details where ID=$id")->row();
        if($check){
            if($check->Published==0){
                $this->db->query("update ttp_report_inventory_details set Published=1,LastEdited='".date('Y-m-d H:i:s')."' where ID=$id");
                echo "a";
            }else{
                $this->db->query("update ttp_report_inventory_details set Published=0,LastEdited='".date('Y-m-d H:i:s')."' where ID=$id");
                echo "b";
            }
        }
    }

    public function add_inventory_details(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_products_details_add');
        $this->template->render();
    }

    public function save_inventory_details(){
        $warehouse = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 0 ;
        $products = isset($_POST['productsid']) ? (int)$_POST['productsid'] : 0 ;
        $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : array() ;
        if($products>0 && $warehouse>0){
            if(count($Amount)>0){
                $sql = array();
                foreach ($Amount as $key => $value) {
                    if($value>0){
                        $rownote = isset($Note[$key]) ? $Note[$key] : 0 ;
                        $sql[] = "($warehouse,$products,$value,'$rownote','".date('Y-m-d H:i:s')."')";
                    }
                }
                if(count($sql)>0){
                    $sql = "insert into ttp_report_inventory_details(WarehouseID,ProductsID,Amount,ProductsNote,Created) values".implode(',', $sql);
                    $this->db->query($sql);
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse/inventory_details');
    }
    
    public function inventory(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $str = "";
        $sku_fill = isset($_GET['type']) ? $_GET['type'] : -1 ;
		$str = $sku_fill>=0 ? $str." and b.TypeProducts='$sku_fill'" : $str ;
        $SKU = isset($_GET['SKU'])  ? $_GET['SKU'] : 0 ;
        $str = $SKU!=0 ? $str." and b.ID='$SKU'" : $str ;
		$ShipmentID = isset($_GET['ShipmentID']) ? $_GET['ShipmentID'] : '' ;
		$str = $ShipmentID!='' ? $str." and c.ShipmentCode='$ShipmentID'" : $str ;
		$WarehouseID = isset($_GET['WarehouseID']) ? $_GET['WarehouseID'] : 0 ;
		$str = $WarehouseID!=0 ? $str." and a.WarehouseID=$WarehouseID" : $str ;
        if($this->user->UserType==2 || $this->user->UserType==8){
            $result = $this->db->query("select a.*,b.MaSP,b.RootPrice,b.Title,c.ShipmentCode,c.DateExpiration,d.MaKho from ttp_report_inventory a,ttp_report_products b,ttp_report_shipment c,ttp_report_warehouse d where d.Manager like '%\"".$this->user->ID."\"%' and a.ProductsID=b.ID and a.ShipmentID=c.ID and a.WarehouseID=d.ID and a.LastEdited=1 and a.OnHand>=0 and a.Available>0 $str order by a.ProductsID ASC")->result();
        }else{
            $result = $this->db->query("select a.*,b.RootPrice,b.MaSP,b.Title,c.ShipmentCode,c.DateExpiration,d.MaKho from ttp_report_inventory a,ttp_report_products b,ttp_report_shipment c,ttp_report_warehouse d where a.ProductsID=b.ID and a.ShipmentID=c.ID and a.WarehouseID=d.ID and a.LastEdited=1 and a.OnHand>=0 and a.Available>0 $str order by a.ProductsID ASC")->result();
        }
		$data = array(
            'Data'  => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_products_home',$data);
        $this->template->render();
    }

    public function check_warehouse(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $result = $this->db->query("select a.*,b.UserName,c.MaKho from ttp_report_inventory_check a,ttp_user b,ttp_report_warehouse c where a.UserID=b.ID and a.WarehouseID=c.ID order by a.ID DESC")->result();
        $data = array(
            'data'  => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_check_warehouse_home',$data);
        $this->template->render();
    }

    public function create_check_warehouse(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_check_warehouse_add');
        $this->template->render();
    }

    public function add_check_warehouse(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 0 ;
        $Type = isset($_POST['Type']) ? $_POST['Type'] : 0 ;
        if($Title!='' && $WarehouseID>0){
            $data = array(
                'Title'         => $Title,
                'WarehouseID'   => $WarehouseID,
                'Type'          => $Type,
                'Status'        => 0,
                'Created'       => date('Y-m-d H:i:s'),
                'LastEdited'    => date('Y-m-d H:i:s'),
                'UserID'        => $this->user->ID
            );
            $this->db->insert("ttp_report_inventory_check",$data);
            $id = $this->db->insert_id();
            $datahis = array(
                'CheckID'   => $id,
                'UserID'    => $this->user->ID,
                'Status'    => 0,
                'TimeChange'=> date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_inventory_check_history",$datahis);
            redirect(base_url().ADMINPATH."/report/warehouse/edit_check_warehouse/$id");
        }
    }

    public function delete_check_warehouse($ID=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_report_inventory_check where ID=$ID")->row();
        if($result){
            if($result->Status==0){
                $this->db->query("delete from ttp_report_inventory_check where ID=$ID");
                $this->db->query("delete from ttp_report_inventory_check_details where CheckID=$ID");
                $this->db->query("delete from ttp_report_inventory_check_history where CheckID=$ID");
            }
        }
        redirect(base_url().ADMINPATH."/report/warehouse/check_warehouse");
    }

    public function edit_check_warehouse($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select a.*,b.Title as TenKho,b.MaKho from ttp_report_inventory_check a,ttp_report_warehouse b where a.WarehouseID=b.ID and a.ID=$id")->row();
        if($result){
            if($result->Status==0){
                $inventory = $this->db->query("select a.*,b.Donvi,b.MaSP,b.Title,c.ShipmentCode,c.DateExpiration from ttp_report_inventory a,ttp_report_products b,ttp_report_shipment c where a.ProductsID=b.ID and a.ShipmentID=c.ID and a.WarehouseID=$result->WarehouseID and a.LastEdited=1 and b.TypeProducts=$result->Type order by a.ProductsID ASC")->result();
            }else{
                $inventory = $this->db->query("select a.*,b.Donvi,b.MaSP,b.Title,c.ShipmentCode,c.DateExpiration from ttp_report_inventory_check_details a,ttp_report_products b,ttp_report_shipment c where a.ProductsID=b.ID and a.ShipmentID=c.ID and a.CheckID=$id")->result();
            }
            $data = array(
                'data'      =>$result,
                'inventory' =>$inventory
            );
            $this->template->write_view('content','admin/warehouse_inventory_check_warehouse_edit',$data);
            $this->template->render();
        }
    }

    public function add_details_check_warehouse(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $OldData = isset($_POST['OldData']) ? $_POST['OldData'] : array() ;
        $NewData = isset($_POST['NewData']) ? $_POST['NewData'] : array() ;
        $IDData = isset($_POST['IDData']) ? $_POST['IDData'] : array() ;
        $result = $this->db->query("select * from ttp_report_inventory_check where ID=$ID and Status=0")->row();
        if($result){
            $this->db->query("update ttp_report_inventory_check set Status=1,LastEdited='".date('Y-m-d H:i:s')."' where ID=$ID");
            $data_sql = array();
            foreach($OldData as $key=>$value){
                $item = explode('_',$key);
                if(count($item)==2){
                    $ProductsID = $item[0];
                    $ShipmentID = $item[1];
                    if($ProductsID>0 && $ShipmentID>0){
                        if(isset($NewData[$ProductsID.'_'.$ShipmentID]) && isset($IDData[$ProductsID.'_'.$ShipmentID])){
                            $Amount = $NewData[$ProductsID.'_'.$ShipmentID];
                            $IDInventory = $IDData[$ProductsID.'_'.$ShipmentID];
                            $data_sql[] = "($ID,$result->WarehouseID,$ProductsID,$ShipmentID,$value,$Amount)";
                            $this->db->query("update ttp_report_inventory set Available=$Amount,OnHand=$Amount where ID=$IDInventory");
                        }
                    }
                }
            }
            if(count($data_sql)>0){
                $data_sql = "insert into ttp_report_inventory_check_details(CheckID,WarehouseID,ProductsID,ShipmentID,OnHandOld,OnHand) values".implode(',',$data_sql);
                $this->db->query($data_sql);
            }
            $datahis = array(
                'CheckID'   => $ID,
                'UserID'    => $this->user->ID,
                'Status'    => 1,
                'TimeChange'=> date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_inventory_check_history",$datahis);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function report_inventory_import(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Inventory | Report Tools');
        $fromdate = $this->session->userdata("import_startday");
        $fromdate = $fromdate!='' ? $fromdate : date('Y-m-01',time()) ;
        $todate = $this->session->userdata("import_stopday");
        $todate = $todate!='' ? $todate : date('Y-m-d',time()) ;
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 1 ;
        $Type = isset($_GET['Type']) ? (int)$_GET['Type'] : 0 ;
        if($Type==2){
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.TotalImport) as TotalAmount from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_warehouse c,ttp_report_products d where b.ProductsID=d.ID and a.ID=b.OrderID and a.WarehouseReciver=c.ID and date(a.ImportDate)>='$fromdate' and date(a.ImportDate)<='$todate' and a.Status =4 and a.WarehouseReciver=$WarehouseID group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }else{
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.Amount) as TotalAmount,sum(b.TotalVND) as TotalPrice from ttp_report_inventory_import a,ttp_report_inventory_import_details b,ttp_report_warehouse c,ttp_report_products d where b.ProductsID=d.ID and a.ID=b.ImportID and a.KhoID=c.ID and date(a.LastEdited)>='$fromdate' and date(a.LastEdited)<='$todate' and a.Status in(2,3,4) and a.KhoID=$WarehouseID and a.Type=$Type group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }
        $result = $this->db->query($sql)->result();
        $data = array(
            'FromDate'      => $fromdate,
            'ToDate'        => $todate,
            'WarehouseID'   => $WarehouseID,
            'Type'          => $Type,
            'data'          => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_report_inventory_import',$data);
        $this->template->render();
    }

    public function report_inventory_export(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Warehouse Export | Report Tools');
        $fromdate = $this->session->userdata("import_startday");
        $fromdate = $fromdate!='' ? $fromdate : date('Y-m-01',time()) ;
        $todate = $this->session->userdata("import_stopday");
        $todate = $todate!='' ? $todate : date('Y-m-d',time()) ;
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 1 ;
        $Type = isset($_GET['Type']) ? (int)$_GET['Type'] : 0 ;
        if($Type==2){
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.TotalImport) as TotalAmount from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_warehouse c,ttp_report_products d where b.ProductsID=d.ID and a.ID=b.OrderID and a.WarehouseSender=c.ID and date(a.ExportDate)>='$fromdate' and date(a.ExportDate)<='$todate' and a.Status in(3,4) and a.WarehouseSender=$WarehouseID group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }elseif($Type==1){
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.Amount) as TotalAmount from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status=0 and a.KhoID=$WarehouseID and a.CustomerID=9996 group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }else{
            $sql = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,sum(b.Amount) as TotalAmount from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status in(7,9,1,0) and a.KhoID=$WarehouseID and a.CustomerID!=9996 group by b.ProductsID";
            $this->session->set_userdata("sql_export",$sql);
        }
        $result = $this->db->query($sql)->result();
        $data = array(
            'FromDate'      => $fromdate,
            'ToDate'        => $todate,
            'WarehouseID'   => $WarehouseID,
            'Type'          => $Type,
            'data'          => $result
        );
        $this->template->write_view('content','admin/warehouse_inventory_report_inventory_export',$data);
        $this->template->render();
    }

    public function load_details_report_export(){
        $fromdate = $this->session->userdata("import_startday");
        $fromdate = $fromdate!='' ? $fromdate : date('Y-m-01',time()) ;
        $todate = $this->session->userdata("import_stopday");
        $todate = $todate!='' ? $todate : date('Y-m-d',time()) ;
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 1 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 1 ;
        $result = $this->db->query("select Title from ttp_report_products where ID=$ProductsID")->row();
        if($result){
            $sql1 = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,d.Donvi,f.Code,f.TransferDate from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_warehouse c,ttp_report_products d,ttp_report_transferorder_mobilization f where a.ID=f.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.WarehouseSender=c.ID and date(f.TransferDate)>='$fromdate' and date(f.TransferDate)<='$todate' and a.Status in(3,4) and a.WarehouseSender=$WarehouseID and b.ProductsID=$ProductsID order by b.ProductsID";
            $data1 = $this->db->query($sql1)->result();
            $sql2 = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,d.Donvi,e.MaXK,e.Ngayxuatkho from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status=0 and a.KhoID=$WarehouseID and a.CustomerID=9996 and b.ProductsID=$ProductsID order by e.Ngayxuatkho ASC";
            $data2 = $this->db->query($sql2)->result();
            $sql3 = "select b.*,c.MaKho,d.MaSP,d.Title as Tensanpham,d.Donvi,e.MaXK,e.Ngayxuatkho,a.TransportID from ttp_report_order a,ttp_report_orderdetails b,ttp_report_warehouse c,ttp_report_products d,ttp_report_export_warehouse e where a.ID=e.OrderID and b.ProductsID=d.ID and a.ID=b.OrderID and a.KhoID=c.ID and date(e.Ngayxuatkho)>='$fromdate' and date(e.Ngayxuatkho)<='$todate' and a.Status in(7,9,1,0) and a.KhoID=$WarehouseID and a.CustomerID!=9996 and b.ProductsID=$ProductsID order by e.Ngayxuatkho ASC";
            $data3 = $this->db->query($sql3)->result();
            $data = array(
                'Products'  =>$result,
                'data1'     =>$data1,
                'data2'     =>$data2,
                'data3'     =>$data3
            );
            $this->load->view('warehouse_report_inventory_export_details',$data);
        }
    }
}
?>
