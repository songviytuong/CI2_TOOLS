<?php 
class Factory extends Admin_Controller { 
 	public $user;
 	public $classname="factory";

    public function __construct() { 
        parent::__construct(); 
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/factory_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
	
	public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$this->template->add_title('Factory | Report Tools');
		$this->template->write_view('content','admin/factory_content');
		$this->template->render();
	}

    public function qrcode(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $type = isset($_GET['type']) ? $_GET['type'] : -1 ;
        $temptype = $type;
        $type = $type==-1 ? "" : " and Status=$type" ;
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string(trim($_GET['keywords'])) : '' ;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_factory_qrcode where Code like '%$keywords%' $type")->row();
        $nav = $nav ? $nav->nav : 0;
        $status = $this->db->query("select Status,count(1) as total from ttp_report_factory_qrcode where Code like '%$keywords%' $type group by Status")->result();
        $result = $this->db->query("select * from ttp_report_factory_qrcode where Code like '%$keywords%' $type order by ID DESC $limit_str")->result();
        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/factory/qrcode/',
            'start'     =>  $start,
            'type'      =>  $temptype,
            'find'      =>  $nav,
            'status'    =>  $status,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/factory/qrcode',5,$nav,$this->limit)
        );    

        $this->template->add_title('QR CODE | Report Tools');
        $this->template->write_view('content','admin/factory_qrcode',$data);
        $this->template->render();
    }

    public function barcode(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string(trim($_GET['keywords'])) : '' ;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_factory_barcode where Code like '%$keywords%'")->row();
        $nav = $nav ? $nav->nav : 0;
        $status = $this->db->query("select Status,count(1) as total from ttp_report_factory_barcode where Code like '%$keywords%' group by Status")->result();
        $result = $this->db->query("select * from ttp_report_factory_barcode where Code like '%$keywords%' order by ID DESC $limit_str")->result();
        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/factory/barcode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'status'    =>  $status,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/factory/barcode',5,$nav,$this->limit)
        );

        $this->template->add_title('BARCODE | Report Tools');
        $this->template->write_view('content','admin/factory_barcode',$data);
        $this->template->render();
    }

    public function map_qrcode_barcode(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('MAP BARCODE vs QRCODE | Report Tools');
        $result = $this->db->query("select Title,ID,MaSP from ttp_report_products where CategoriesID like '%\"62\"%'")->result();
        $data = array(
            'data'      =>$result,
            'baselink'  =>base_url().ADMINPATH.'/report/factory/'
        );
        $this->template->write_view('content','admin/factory_mapping',$data);
        $this->template->render();
    }

    public function map_qrcode_barcode_single(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('DEFINE QRCODE | Report Tools');
        $data = array(
            'baselink'  =>base_url().ADMINPATH.'/report/factory/'
        );
        $this->template->write_view('content','admin/factory_mapping_single',$data);
        $this->template->render();
    }
    
    public function load_tankage($id=0){
        echo "<table><tr><th>STT</th><th>Mã QR Code</th><th class='width150'>QR Code Image</th><th class='width50'></th></tr>";
        if($id>0){
            $result = $this->db->query("select Tankage from ttp_report_products where ID=$id")->row();
            if($result){
                if($result->Tankage>0){
                    for ($i=1; $i <= $result->Tankage; $i++) { 
                        $autofocus = $i==1 ? "autofocus" : "" ;
                        echo "<tr>";
                        echo "<td><span style='width:20px;height:20px;background:#555;color:#FFF;text-align:center;display:block;line-height: 22px;border-radius:2px'>$i</span></td>";
                        echo "<td><input type='text' class='form-control qrcodeinput qrcodeinput$i' data='$i' $autofocus onchange='enterqrcode(this)' /></td>";
                        echo "<td class='qrcodeimage'><img style='width:50px;display:block;margin:auto' src='' class='hidden' /></td>";
                        echo "<td class='text-success'><i class='fa fa-check-square-o hidden' style='font-size:20px !important;color:#76CE35'></i></td>";
                        echo "</tr>";
                    }
                }
            }
        }
        echo "</table>";
    }

    public function check_qrcode(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];            
        }
        $result = $this->db->query("select ID from ttp_report_factory_qrcode where Code=$code and ProductsID=0 and WarehouseID=0 and OrderID=0 and Status=0")->row();
        $data = array("error"=>null);
        if($result){
            $data['error'] = $result->ID;
        }
        echo json_encode($data);
    }

    public function check_barcode(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $result = $this->db->query("select ID from ttp_report_factory_barcode where Code='$code' and Status=0")->row();
        $data = array("error"=>null);
        if($result){
            $data['error'] = $result->ID;
        }
        echo json_encode($data);
    }

    public function load_import_barcode_form(){
        $this->load->view("admin/factory_import_barcode_form");
    }

    public function load_import_qrcode_form(){
        $this->load->view("admin/factory_import_barcode_form");
    }

    public function load_preview_trace($id=0){
        if($id!=0){
            $result = $this->db->query("select * from ttp_report_factory_qrcode_trace where QRID=$id")->result();
            $object = $this->db->query("select a.*,b.MaSP,c.ShipmentCode from ttp_report_factory_qrcode a,ttp_report_products b,ttp_report_shipment c where c.ID=a.ShipmentID and a.ProductsID=b.ID and a.ID=$id")->row();
            if($object){
                $this->load->view("admin/factory_qrcode_trace",array('trace'=>$result,'data'=>$object));
            }
        }
    }

    public function upload_import(){
        $file = isset($_FILES['file']['tmp_name']) ? $_FILES['file']['tmp_name'] : "";
        $name = isset($_FILES['file']['name']) ? $_FILES['file']['name'] : "";
        if($name!=''){
            $ext = explode('.',$name);
            $ext = $ext[count($ext)-1];
            if($ext!='xls' && $ext!='xlsx'){
                $this->load->view("admin/factory_import_barcode_form",array('error'=>'Định dạng file không được chấp nhận. Vui lòng chọn file theo đúng định dạng quy định.'));
                return;
            }
        }
        if(file_exists($file)){
            $result = array();
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow         = $worksheet->getHighestRow();
                $highestColumn      = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 1; $row <= $highestRow; $row ++) {
                    $data = array();
                    for($key=0;$key<=$highestColumnIndex;$key++){
                        $value = trim($worksheet->getCellByColumnAndRow($key, $row)->getValue());
                        $data[] = $value;
                    }
                    $result[] = $data;
                }
            }
            $this->load->view("admin/factory_import_barcode_form",array('data'=>$result,'filename'=>$name));
        }else{
            $this->load->view("admin/factory_import_barcode_form");
        }
    }

    public function active_upload_import(){
        $result = isset($_POST['data']) ? $_POST['data'] : '[]' ;
        $result = json_decode($result);
        $Bill = isset($_POST['bill']) ? $_POST['bill'] : 0 ;
        $arr = array();
        if(count($result)>0){
            $create = date('Y-m-d H:i:s');
            foreach($result as $row){
                if($row->qrcode!=''){
                    $arr[] = "('$row->qrcode','$create',$Bill,1)";
                }
            }
            $arr = implode(',', $arr);
            $this->db->query("insert into ttp_report_factory_qrcode(Code,Created,MobilizationID,Published) values".$arr);
            $data = array("Error"=>null);
        }else{
            $data = array("Error"=>"Không có thông tin để lưu vào hệ thống .");
        }
        echo json_encode($data);
    }

    public function active_upload_import_barcode(){
        $result = isset($_POST['data']) ? $_POST['data'] : '[]' ;
        $result = json_decode($result);
        $arr = array();
        if(count($result)>0){
            $max = $this->db->query("select max(ID) as max from ttp_report_factory_barcode")->row();
            $max = $max ? $max->max+1 : 1 ;
            $create = date('Y-m-d H:i:s');
            foreach($result as $row){
                if($row->id>0 && $row->soluong>0){
                    for ($i=0; $i < $row->soluong; $i++) { 
                        $strid = "603".str_pad($max, 9, '0', STR_PAD_LEFT)."2";
                        $arr[] = "($row->id,1,'$create','$strid')";
                        $max++;
                    }
                }
            }
            $arr = implode(',', $arr);
            $this->db->query("insert into ttp_report_factory_barcode(ProductsID,Published,Created,Code) values".$arr);
            $data = array("Error"=>null);
        }else{
            $data = array("Error"=>"Không có thông tin để lưu vào hệ thống .");
        }
        echo json_encode($data);
    }

    public function qrcode_generator($id=0){
        if($id>0){
            $result = $this->db->query("select * from ttp_report_factory_qrcode where ID=$id")->row();
            if($result){
                $this->load->library('ci_qr_code');
                $this->config->load('qr_code');
                $qr_code_config = array();
                $qr_code_config['cacheable'] = $this->config->item('cacheable');
                $qr_code_config['cachedir'] = $this->config->item('cachedir');
                $qr_code_config['imagedir'] = $this->config->item('imagedir');
                $qr_code_config['errorlog'] = $this->config->item('errorlog');
                $qr_code_config['ciqrcodelib'] = $this->config->item('ciqrcodelib');
                $qr_code_config['quality'] = $this->config->item('quality');
                $qr_code_config['size'] = $this->config->item('size');
                $qr_code_config['black'] = $this->config->item('black');
                $qr_code_config['white'] = $this->config->item('white');
                $this->ci_qr_code->initialize($qr_code_config);

                $image_name = $result->ID . ".png";
                $params['data'] = $result->Code;
                $params['level'] = 'K';
                $params['size'] = 3;

                $params['savename'] = FCPATH . $qr_code_config['imagedir'] . $image_name;
                $this->ci_qr_code->generate($params);

                $this->data['qr_code_image_url'] = base_url() . $qr_code_config['imagedir'] . $image_name;
                $file = $params['savename'];
                if(file_exists($file)){
                    readfile($file);
                    unlink($file);
                    exit;
                }
            }
        }
    }

    public function barcode_generator($code=''){
        if($code!=''){
            $this->load->library('zend');
            $this->zend->load('Zend/Barcode');
            Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
        }
    }

    public function template_barcode(){
        $result = $this->db->query("select Title,ID,MaSP from ttp_report_products where CategoriesID like '%\"62\"%'")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'ID')
                        ->setCellValue('B1', 'Mã SKU')
                        ->setCellValue('C1', 'Tên thùng')
                        ->setCellValue('D1', 'Số lượng barcode tạo');
            $objPHPExcel->getActiveSheet()->getStyle("A1:D1")->getFill()->applyFromArray(array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array(
                    'rgb' => "FBFF00"
                ),
                'font'  => array(
                    'bold'  => true,
                    'color' => array('rgb' => '0000FF')
                ),
                'borders' => array(
                    'allborders' => array(
                      'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                  )
            ));
            $i=2;
            foreach($result as $row){
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row->ID)
                        ->setCellValue('B'.$i, $row->MaSP)
                        ->setCellValue('C'.$i, $row->Title)
                        ->setCellValue('D'.$i, 0);
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="template_import_barcode.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');
            
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !. Can't export data .";
        }
    }

    public function save_mapping(){
        $Barcode = isset($_POST['barcode']) ? (int)$_POST['barcode'] : 0 ;
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 0 ;
        $ShipmentID = isset($_POST['ShipmentID']) ? (int)$_POST['ShipmentID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $Carton = isset($_POST['Carton']) ? (int)$_POST['Carton'] : 0 ;
        $WarehouseText = isset($_POST['WarehouseTitle']) ? mysql_real_escape_string($_POST['WarehouseTitle']) : '' ;
        $Qrcode = isset($_POST['qrcode']) ? $_POST['qrcode'] : array() ;
        $Qrcode = json_decode($Qrcode);
        $data = array('error'=>null);
        if(count($Qrcode)>0){
            $Str_barcode = $this->db->query("select * from ttp_report_factory_barcode where ID=$Barcode and Status=0")->row();
            if($Str_barcode){
                $str = array();
                $created = date('Y-m-d H:i:s');
                $i=0;
                foreach($Qrcode as $row){
                    if($row>0){
                        $this->db->query("update ttp_report_factory_qrcode set BarcodeID=$Barcode,Status=1,WarehouseID=$WarehouseID,ShipmentID=$ShipmentID,ProductsID=$ProductsID where ID=$row");
                        $str[] = "('Quét QRCODE bỏ vào thùng hàng có mã BARCODE $Str_barcode->Code tại kho / nhà máy $WarehouseText','$created',$row,".$this->user->ID.",$WarehouseID)";
                        $i++;
                    }
                }
                $str = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID) values".implode(',',$str);
                $this->db->query("update ttp_report_factory_barcode set Status=1,ProductsID=$Carton where ID=$Barcode");
                $this->db->query($str);
                $check_exists = $this->db->query("select * from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$WarehouseID and LastEdited=1")->row();
                if($check_exists){
                    $data['message'] = 1;
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$WarehouseID and LastEdited=1");
                    if($check_exists->DateInventory==date('Y-m-d')){
                        $this->db->query("update ttp_report_inventory set Available=Available+$i,OnHand=OnHand+$i,LastEdited=1 where ID=$check_exists->ID");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check_exists->OnHand+$i,
                            'Available'     =>$check_exists->Available+$i,
                            'WarehouseID'   =>$WarehouseID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"All",
                            'Amount'        =>$i,
                            'Action'        =>'update',
                            'Method'        =>'Scan qrcode warehouse 1'
                        );
                        $this->write_log_inventory($data_log);
                    }else{
                        $OnHand = $check_exists->OnHand+$i;
                        $Available = $check_exists->Available+$i;
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$OnHand,
                            'Available'     =>$Available,
                            'WarehouseID'   =>$WarehouseID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"All",
                            'Amount'        =>$i,
                            'Action'        =>'insert',
                            'Method'        =>'Scan qrcode warehouse 2'
                        );
                        $this->write_log_inventory($data_log);
                    }
                }else{
                    $data['message'] = 2;
                    $OnHand = $i;
                    $Available = $i;
                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
                    $data_log = array(
                        'ProductsID'    =>$ProductsID,
                        'OnHand'        =>$OnHand,
                        'Available'     =>$Available,
                        'WarehouseID'   =>$WarehouseID,
                        'ShipmentID'    =>$ShipmentID,
                        'DateInventory' =>date('Y-m-d'),
                        'Type'          =>"All",
                        'Amount'        =>$i,
                        'Action'        =>'insert',
                        'Method'        =>'Scan qrcode warehouse 3'
                    );
                    $this->write_log_inventory($data_log);
                }
            }else{
                $data['error'] = "Mã barcode không được chấp nhận !";
            }
        }
        echo json_encode($data);
    }

    public function save_mapping_single(){
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 0 ;
        $ShipmentID = isset($_POST['ShipmentID']) ? (int)$_POST['ShipmentID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $WarehouseText = isset($_POST['WarehouseTitle']) ? mysql_real_escape_string($_POST['WarehouseTitle']) : '' ;
        $Qrcode = isset($_POST['data']) ? $_POST['data'] : '' ;
        $code = explode('=', $Qrcode);
        if(count($code)==2){
            $Qrcode = (int)$code[1];
        }else{
            $Qrcode = $code[0];            
        }
        $result = $this->db->query("select ID from ttp_report_factory_qrcode where Code=$Qrcode and ProductsID=0 and WarehouseID=0 and OrderID=0 and Status=0")->row();
        $data = array("error"=>null);
        if($result){
            if($WarehouseID==0 || $ShipmentID=='' || $ProductsID==''){
                $data['error'] = "Bắt buộc phải chọn kho ,lô hàng ,loại sản phẩm !";
            }else{
                $created = date('Y-m-d H:i:s');
                $this->db->query("update ttp_report_factory_qrcode set Status=4,WarehouseID=$WarehouseID,ShipmentID=$ShipmentID,ProductsID=$ProductsID where ID=$result->ID");
                $str[] = "('Quét QRCODE hàng lẻ tại kho / nhà máy $WarehouseText','$created',$result->ID,".$this->user->ID.",$WarehouseID)";
                $str = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID) values".implode(',',$str);
                $this->db->query($str);
                $check_exists = $this->db->query("select * from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$WarehouseID and LastEdited=1")->row();
                $i=1;
                if($check_exists){
                    $data['message'] = 1;
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$WarehouseID and LastEdited=1");
                    if($check_exists->DateInventory==date('Y-m-d')){
                        $this->db->query("update ttp_report_inventory set Available=Available+$i,OnHand=OnHand+$i,LastEdited=1 where ID=$check_exists->ID");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check_exists->OnHand+$i,
                            'Available'     =>$check_exists->Available+$i,
                            'WarehouseID'   =>$WarehouseID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"All",
                            'Amount'        =>$i,
                            'Action'        =>'update',
                            'Method'        =>'Scan qrcode warehouse 1'
                        );
                        $this->write_log_inventory($data_log);
                    }else{
                        $OnHand = $check_exists->OnHand+$i;
                        $Available = $check_exists->Available+$i;
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$OnHand,
                            'Available'     =>$Available,
                            'WarehouseID'   =>$WarehouseID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"All",
                            'Amount'        =>$i,
                            'Action'        =>'insert',
                            'Method'        =>'Scan qrcode warehouse 2'
                        );
                        $this->write_log_inventory($data_log);
                    }
                }else{
                    $data['message'] = 2;
                    $OnHand = $i;
                    $Available = $i;
                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
                    $data_log = array(
                        'ProductsID'    =>$ProductsID,
                        'OnHand'        =>$OnHand,
                        'Available'     =>$Available,
                        'WarehouseID'   =>$WarehouseID,
                        'ShipmentID'    =>$ShipmentID,
                        'DateInventory' =>date('Y-m-d'),
                        'Type'          =>"All",
                        'Amount'        =>$i,
                        'Action'        =>'insert',
                        'Method'        =>'Scan qrcode warehouse 3'
                    );
                    $this->write_log_inventory($data_log);
                }
            }
        }else{
            $data['error'] = "Mã QR Code không được chấp nhận . Vui lòng thử lại nhé !";
        }
        echo json_encode($data);
    }

    public function created_barcode(){
        $Number = isset($_POST['Number']) ? (int)$_POST['Number'] : 0 ;
        if($Number<=1000){
            $max = $this->db->query("select max(ID) as max from ttp_report_factory_barcode")->row();
            $max = $max ? $max->max+1 : 1 ;
            $create = date('Y-m-d H:i:s');
            $arr = array();
            for ($i=0; $i < $Number; $i++) { 
                $strid = "603".str_pad($max, 9, '0', STR_PAD_LEFT)."2";
                $arr[] = "(1,'$create','$strid')";
                $max++;
            }
            $arr = implode(',', $arr);
            $this->db->query("insert into ttp_report_factory_barcode(Published,Created,Code) values".$arr);
        }
    }

    public function export_barcode($number=0){
        if($number>0){
            $result = $this->db->query("select Code from ttp_report_factory_barcode where Status=0 limit 0,$number")->result();
            if(count($result)>0){
                error_reporting(E_ALL);
                ini_set('display_errors', TRUE);
                ini_set('display_startup_errors', TRUE);
                if (PHP_SAPI == 'cli')
                    die('This example should only be run from a Web Browser');
                require_once 'public/plugin/PHPExcel.php';
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                             ->setLastModifiedBy("Maarten Balliauw")
                                             ->setTitle("Office 2007 XLSX Test Document")
                                             ->setSubject("Office 2007 XLSX Test Document")
                                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                             ->setKeywords("office 2007 openxml php")
                                             ->setCategory("Test result file");
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A1', 'ean13');
                $i=2;
                foreach($result as $row){
                        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$i, $row->Code);
                        $i++;
                }
                $objPHPExcel->getActiveSheet()->setTitle('BARCODE_LIST');
                $objPHPExcel->setActiveSheetIndex(0);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="BARCODE_LIST.xls"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header ('Cache-Control: cache, must-revalidate');
                header ('Pragma: public');
                
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                ob_end_clean();
                $objWriter->save('php://output');
                exit;
            }else{
                echo "Data is empty !. Can't export data .";
            }
        }
    }

    public function load_shipment_by_products($id=0){
        $result = $this->db->query("select a.ShipmentCode,a.ID,b.MaSP from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and a.ProductsID=$id")->result();
        echo '<option value="0">-- Chọn lô sản xuất --</option>';
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->MaSP - $row->ShipmentCode</option>";
            }
        }
    }

    /*
    * Scan import products
    */
    public function scan_from_import($id=0,$WarehouseID=0){
        $import_code = $this->db->query("select count(ID) as sl from ttp_report_factory_qrcode_trace where ActionType=1 and ActionID=$id")->row();
        $import_code = $import_code ? $import_code->sl : 0 ;
        $result = $this->db->query("select sum(a.Request) as sl from ttp_report_inventory_import_details a,ttp_report_products b where a.ImportID=$id and a.ProductsID=b.ID and b.CodeManage=1")->row();
        if($result){
            echo "
                <div class='row'>
                <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                <div class='col-xs-8'>
                    <h1>QR Scaner</h1>
                    <p>Tổng cộng $import_code/$result->sl sản phẩm đã được quét</p>
                    <input type='text' class='form-control' onchange='enterqrcode(this)' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                    <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>
                    <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                    <div class='alert alert-danger hidden'></div>
                </div>
                </div>";
            echo '<script>$("#autofocus").focus();</script>';
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function scan_from_export_transfer($id=0){
        $import_code = $this->db->query("select count(ID) as sl from ttp_report_factory_qrcode_trace where ActionType=3 and ActionID=$id")->row();
        $import_code = $import_code ? $import_code->sl : 0 ;
        $result = $this->db->query("select sum(a.Request) as sl from ttp_report_transferorder_details a,ttp_report_products b where a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->row();
        $amount = $result ? $result->sl : 0 ;
        if($amount>0){
            $full = $import_code==$amount ? "<div class='alert alert-success'><i class='fa fa-stack-overflow' style='margin-right:5px;font-size: 17px;'></i> Đã quét đủ số lượng sản phẩm , bạn có thể sử dụng thao tác xuất kho .</div>" : "<input type='text' class='form-control' onchange='enterqrcode(this)' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                    <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>" ;
            echo "
                <div class='row'>
                <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                <div class='col-xs-8'>
                    <h1>QR Scaner</h1>
                    <p>Tổng cộng <b style='font-size:17px'>$import_code/$amount</b> sản phẩm đã được quét</p>
                    $full
                    <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                    <div class='alert alert-danger hidden'></div>
                </div>
                </div>";
            echo $import_code==$amount ? '<script>$("#exportbutton").removeClass("hidden")</script>' : '<script>$("#autofocus").focus();</script>';
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function scan_from_import_transfer($id=0){
        $import_code = $this->db->query("select count(ID) as sl from ttp_report_factory_qrcode_trace where ActionType=4 and ActionID=$id")->row();
        $import_code = $import_code ? $import_code->sl : 0 ;
        $result = $this->db->query("select sum(a.TotalExport) as sl from ttp_report_transferorder_details a,ttp_report_products b where a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->row();
        $amount = $result ? $result->sl : 0 ;
        if($amount>0){
            $remain = $amount-$import_code;
            $full = $import_code==$amount ? "<div class='alert alert-success'><i class='fa fa-stack-overflow' style='margin-right:5px;font-size: 17px;'></i> Đã quét đủ số lượng sản phẩm , bạn có thể sử dụng thao tác nhập kho .</div>" : "<input type='text' class='form-control' onchange='enterqrcode(this)' remain='$remain' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                    <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>" ;
            echo "
                <div class='row'>
                <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                <div class='col-xs-8'>
                    <h1>QR Scaner</h1>
                    <p>Tổng cộng <b style='font-size:17px'>$import_code/$amount</b> sản phẩm đã được quét</p>
                    $full
                    <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                    <div class='alert alert-danger hidden'></div>
                </div>
                </div>";
            echo $import_code==$amount ? '<script>$("#exportbutton").removeClass("hidden");</script>' : '<script>$("#autofocus").focus();</script>';
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function check_import_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $WarehouseID = isset($_POST['WarehouseID']) ? mysql_real_escape_string($_POST['WarehouseID']) : 0 ;
        $ImportID = isset($_POST['ImportID']) ? (int)$_POST['ImportID'] : 0 ;
        $data = array("error"=>true);
        $import = $this->db->query("select a.MaNK,b.Title,a.Type from ttp_report_inventory_import a,ttp_report_warehouse b where a.KhoID=b.ID and a.ID=$ImportID")->row();
        $warehouse = " and WarehouseID!=$WarehouseID" ;
        if($import){
            $defire = $this->db->query("select name from ttp_define where type='warehouse' and `group`='import' and code=$import->Type")->row();
            $defire = $defire ? "($defire->name)" : '' ;
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập kho $import->Title theo yêu cầu nhập kho số $import->MaNK $defire";
            $UserID = $this->user->ID;
            $result = $this->db->query("select ID from ttp_report_factory_qrcode a where a.Code='$code' and a.Status=1 $warehouse")->row();
            if($result){
                $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$WarehouseID where ID=$result->ID");
                $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ImportID,$type)");
                $data['error']  = false;
            }else{
                $result = $this->db->query("select a.ID,a.BarcodeID from ttp_report_factory_qrcode a,ttp_report_factory_barcode b where b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=1 $warehouse")->result();
                if(count($result)>0){
                    $barid = 0;
                    $data['error']  = false;
                    foreach($result as $row){
                        $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$WarehouseID where ID=$row->ID");
                        $barid = $row->BarcodeID;
                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$ImportID,$type)");
                    }
                    $this->db->query("update ttp_report_factory_barcode set WarehouseID=$WarehouseID where ID=$barid");
                }
            }
        }
        echo json_encode($data);
    }

    public function check_transfer_export_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $WarehouseID = isset($_POST['WarehouseID']) ? mysql_real_escape_string($_POST['WarehouseID']) : 0 ;
        $TransferID = isset($_POST['ImportID']) ? (int)$_POST['ImportID'] : 0 ;
        $data = array("error"=>true);
        $import = $this->db->query("select a.OrderCode,b.MaKho,c.MaKho as KhoReciver from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseReciver=c.ID and a.WarehouseSender=b.ID and a.ID=$TransferID and a.Status=2")->row();
        $warehouse = " and a.WarehouseID=$WarehouseID" ;
        if($import){
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code xuất từ $import->MaKho đến $import->KhoReciver theo yêu cầu $import->OrderCode (lưu chuyển kho)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select ID from ttp_report_factory_qrcode a where a.Code='$code' and a.Status=1 $warehouse")->row();
            if($result){
                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=3 and QRID=$result->ID")->row();
                if(!$check){
                    $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$TransferID,3)");
                    $data['error']  = false;
                }
            }else{
                $result = $this->db->query("select a.ID,a.BarcodeID from ttp_report_factory_qrcode a,ttp_report_factory_barcode b where b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=1 $warehouse")->result();
                if(count($result)>0){
                    $data['error']  = false;
                    foreach($result as $row){
                        $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=3 and QRID=$row->ID")->row();
                        if(!$check){
                            $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$TransferID,3)");
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function check_transfer_import_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $WarehouseID = isset($_POST['WarehouseID']) ? mysql_real_escape_string($_POST['WarehouseID']) : 0 ;
        $TransferID = isset($_POST['ImportID']) ? (int)$_POST['ImportID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true);
        $import = $this->db->query("select a.OrderCode,b.MaKho,a.WarehouseReciver,c.MaKho as KhoReciver from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseReciver=c.ID and a.WarehouseSender=b.ID and a.ID=$TransferID and a.Status=3")->row();
        $warehouse = " and a.WarehouseID=$WarehouseID" ;
        if($import){
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập từ $import->MaKho đến $import->KhoReciver theo yêu cầu $import->OrderCode (lưu chuyển kho)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select ID from ttp_report_factory_qrcode a where a.Code='$code' and a.Status=1 $warehouse")->row();
            if($result){
                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=4 and QRID=$result->ID")->row();
                if(!$check){
                    $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver where ID=$result->ID");
                    $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$TransferID,4)");
                    $data['error']  = false;
                }
            }else{
                $result = $this->db->query("select a.ID,a.BarcodeID,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=1 $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $check = $this->db->query("select count(ID) as sl from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType in(3,4) and QRID=$row->ID")->row();
                                if($check->sl==1){
                                    $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver where ID=$row->ID");
                                    $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$TransferID,4)");
                                }
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function loadhistorybox($id = 0){
        $result = $this->db->query("select a.MaDH,a.Total,a.Chietkhau,a.Chiphi,a.AddressOrder,a.Reduce from ttp_report_order a where a.CustomerID=$id order by a.ID DESC limit 0,10")->result();
        echo "<table><tr><th>STT</th><th>Mã đơn hàng</th><th>Địa chỉ giao hàng</th><th>Giá trị</th></tr>";
        if(count($result)>0){
            $i=1;
            foreach($result as $row){
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>$row->MaDH</td>";
                echo "<td>$row->AddressOrder</td>";
                echo "<td>".number_format($row->Total-$row->Chietkhau+$row->Chiphi-$row->Reduce)."</td>";
                echo "</tr>";
                $i++;
            }
        }
        echo "</table>";
    }

    public function test_code(){
        echo '<div class="row" style="background: #FFF;overflow:hidden">';
        for($i=1;$i<30;$i++){
            echo "<div style='margin:30px 50px;float:left;text-align:center'><img src='http://localhost/trantoanphat/administrator/report/factory/qrcode_generator/$i' /><p>Mã $i</p></div>";
        }
        echo "<div style='clear:both'></div>";
        for($i=1;$i<9;$i++){
            echo "<div style='margin:50px;float:left'><img src='http://localhost/trantoanphat/administrator/report/factory/barcode_generator/60300000000".$i."2' /></div>";
        }
        echo '</div>';
    }

    public function box_add_shipment($id=0){
        $result = $this->db->query("select ID,Title from ttp_report_products where ID=$id")->row();
        if($result){
            $this->load->view("warehouse_inventory_shipment_add",array('data'=>$result));
        }else{
            echo "<p>Dữ liệu gửi lên không chính xác !.</p>";
        }
    }

    public function save_shipment(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'UserID'        => $this->user->ID,
                'ShipmentCode'  => $ShipmentCode,
                'ProductsID'    => $ID,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_shipment",$data);
            $ID = $this->db->insert_id();
            $arr = array("ID"=>$ID,"Title"=>$ShipmentCode,"Error"=>"");
            echo json_encode($arr);
        }else{
            echo json_encode(array("Error"=>"1"));
        }
    }

    public function get_bill_by_warehouse($id=0){
        $result = $this->db->query("select a.Code,a.ID from ttp_report_transferorder_mobilization a,ttp_report_transferorder b where a.OrderID=b.ID and WarehouseReciver=$id");
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->Code</option>";
            }
        }
    }
}