<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1><?= $title ?></h1>
        </div>
        <div class="block2">
            <?php if($rest == 1) { ?>
            <a class="btn btn-default" onclick="importphoto(this)">Import Photo</a>
            <?php } else if($rest == 2) { ?>
            <a class="btn btn-success" onclick="syncall(this)">Sync Products From METI</a>
            <?php } ?>
            <?php
            if ($this->user->IsAdmin) {
                ?>
                <a class="btn btn-danger" onclick="importall(this)">Tạo mới toàn bộ</a>
                <a class="btn btn-primary" onclick="updateall(this)">Cập nhật toàn bộ</a>
            <?php } ?>
        </div>
    </div>
    <div class="row" style="margin:0px;">
        <table class="table">
            <?php
            $status_arr = array(0 => 'Không hiển thị', 1 => 'Hiển thị');
            $print_arr = array(0 => 'Không in', 1 => 'Có in');
            $products_type = $this->lib->get_config_define("products", "productstype");
            $arr_products = array(0 => "Sản phẩm đơn", 1 => "Sản phẩm biến thể", 3 => "Sản phẩm combo", 4 => "Sản phẩm nhóm");
            $categories = $this->db->query("select Title,Path,ID from ttp_report_categories")->result();
            $arr_categories = array();
            if (count($categories) > 0) {
                foreach ($categories as $row) {
                    $arr_categories[$row->ID] = $row->Title;
                }
            }

            $Supplier = $this->db->query("select * from ttp_report_production")->result();
            $arr_supplier = array();
            if (count($Supplier) > 0) {
                foreach ($Supplier as $row) {
                    $arr_supplier[$row->ID] = $row->Title;
                }
            }

            $Brand = $this->db->query("select ID,Title from ttp_report_trademark")->result();
            $arr_brand = array();
            if (count($Brand) > 0) {
                foreach ($Brand as $row) {
                    $arr_brand[$row->ID] = $row->Title;
                }
            }
            $arr_input = array(2, 3, 4, 7, 11, 12, 13, 14, 15, 16);
            if (count($data) > 0) {
                $i = 1;
                foreach ($data as $row) {
                    echo $i == 1 ? "<tr>" : "<tr class='rowdata rowdata$i' data='$i'>";
                    $k = 1;
                    foreach ($row as $key => $value) {
                        if ($i == 1) {
                            echo "<th>" . $value . "</th>";
                        } else {
                            $title = $value;
                            if ($k >= 8 && $k <= 10) {
                                $title = isset($arr_categories[$value]) ? $arr_categories[$value] : $title;
                            }
                            if ($k == 17) {
                                $title = isset($arr_products[$value]) ? $arr_products[$value] : $title;
                            }
                            if ($k == 18) {
                                $title = isset($products_type[$value]) ? $products_type[$value] : $title;
                            }
                            if ($k == 5) {
                                $title = isset($arr_supplier[$value]) ? $arr_supplier[$value] : $title;
                            }
                            if ($k == 6) {
                                $title = isset($arr_brand[$value]) ? $arr_brand[$value] : $title;
                            }
                            if ($k == 19) {
                                $title = isset($status_arr[$value]) ? $status_arr[$value] : $title;
                            }
                            if ($k == 20) {
                                $title = isset($print_arr[$value]) ? $print_arr[$value] : $title;
                            }
                            echo in_array($k, $arr_input) ? "<td><input type='text' style='width:100%;border:none;border-bottom:1px solid #ccc' class='col$k' value='" . $value . "' /></td>" : "<td>$title <input type='hidden' class='col$k' value='$value' /></td>";
                        }
                        $k++;
                    }
                    echo $i > 1 ? "<td><a class='iconremove' onclick='removerow(this)'><i class='fa fa-times'></i></a></td>" : "<th></th>";
                    echo "</tr>";
                    $i++;
                }
            }
            ?>
        </table>
    </div>
</div>
<script>
    var data = [];
    var activelink = "";

    function removerow(ob) {
        $(ob).parent('td').parent('tr').remove();
    }

    function repair() {
        var k = 1;
        $(".rowdata").each(function () {
            var iddata = $(this).attr('data');
            data[k] = iddata;
            ++k;
        });
        $(".table input").css({'background': 'none', 'border': 'none'});
        $(".iconremove").hide();
    }

    function importall() {
        repair();
        activelink = "importall";
        send_post(1, data[1]);
    }

    function importphoto() {
        repair();
        activelink = "importphoto";
        photo_import(1, data[1]);
    }

    function photo_import(key, num) {
        var datajson = {
            SKU: $(".rowdata" + num + " .col1").val(),
            Imported: $(".rowdata" + num + " .col2").val()
        };
        if (key >= data.length) {
            return false;
        }
        $.ajax({
            url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/' ?>" + activelink,
            dataType: "html",
            type: "POST",
            data: "data=" + JSON.stringify(datajson),
            beforeSend: function () {
                $(".rowdata" + num).css({'background': '#fffcef'});
            },
            success: function (result) {
//                console.log(result);
                if (result == 'OK') {
                    $(".rowdata" + num).css({'background': '#eaffea'});
                } else {
                    $(".rowdata" + num).css({'background': '#fff2f2'});
                    $(".rowdata" + num + " .iconremove").show();
                    $(".rowdata" + num + " .iconremove").html(result);
                }
            }
        }).always(function () {
            next = ++key;
            photo_import(next, data[next]);
        });
    }

    function updateall() {
        repair();
        activelink = "updateall";
        send_post(1, data[1]);
    }

    function syncall() {
        repair();
        activelink = "syncall";
        sync_post(1, data[1]);
    }

    function sync_post(key, num) {
        var datajson = {
            SKU: $(".rowdata" + num + " .col1").val(),
            EAN: $(".rowdata" + num + " .col2").val(),
            Name: $(".rowdata" + num + " .col3").val(),
        };
        if (key >= data.length) {
            return false;
        }
        $.ajax({
            url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/' ?>" + activelink,
            dataType: "html",
            type: "POST",
            data: "data=" + JSON.stringify(datajson),
            beforeSend: function () {
                $(".rowdata" + num).css({'background': '#fffcef'});
            },
            success: function (result) {
//                console.log(result);
                if (result == 'OK') {
                    $(".rowdata" + num).css({'background': '#eaffea'});
                } else {
                    $(".rowdata" + num).css({'background': '#fff2f2'});
                    $(".rowdata" + num + " .iconremove").show();
                    $(".rowdata" + num + " .iconremove").html(result);
                }
            }
        }).always(function () {
            next = ++key;
            sync_post(next, data[next]);
        });
    }

    function send_post(key, num) {
        var datajson = {
            ID: $(".rowdata" + num + " .col1").val(),
            MaSP: $(".rowdata" + num + " .col2").val(),
            Title: $(".rowdata" + num + " .col3").val(),
            Donvi: $(".rowdata" + num + " .col4").val(),
            SupplierID: $(".rowdata" + num + " .col5").val(),
            TrademarkID: $(".rowdata" + num + " .col6").val(),
            BarcodeClient: $(".rowdata" + num + " .col7").val(),
            CategoriesID1: $(".rowdata" + num + " .col8").val(),
            CategoriesID2: $(".rowdata" + num + " .col9").val(),
            CategoriesID3: $(".rowdata" + num + " .col10").val(),
            RootPrice: $(".rowdata" + num + " .col11").val(),
            Price: $(".rowdata" + num + " .col12").val(),
            BasePrice: $(".rowdata" + num + " .col13").val(),
            SpecialPrice: $(".rowdata" + num + " .col14").val(),
            SpecialStartday: $(".rowdata" + num + " .col15").val(),
            SpecialStopday: $(".rowdata" + num + " .col16").val(),
            VariantType: $(".rowdata" + num + " .col17").val(),
            TypeProducts: $(".rowdata" + num + " .col18").val(),
            Status: $(".rowdata" + num + " .col19").val(),
            PrintPriceList: $(".rowdata" + num + " .col20").val(),
            SocialPrice: $(".rowdata" + num + " .col21").val()
        };
        if (key >= data.length) {
            return false;
        }
        $.ajax({
            url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/' ?>" + activelink,
            dataType: "html",
            type: "POST",
            data: "data=" + JSON.stringify(datajson),
            beforeSend: function () {
                $(".rowdata" + num).css({'background': '#fffcef'});
            },
            success: function (result) {
                console.log(result);
                if (result == 'OK') {
                    $(".rowdata" + num).css({'background': '#eaffea'});
                } else {
                    $(".rowdata" + num).css({'background': '#fff2f2'});
                    $(".rowdata" + num + " .iconremove").show();
                    $(".rowdata" + num + " .iconremove").html(result);
                }
            }
        }).always(function () {
            next = ++key;
            send_post(next, data[next]);
        });
    }
</script>