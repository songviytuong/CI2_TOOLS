<?php
class Finance_import extends Admin_Controller {
    public $user;
    public $classname="finance_import";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/finance_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Finance Import Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $limit_str = "limit $start,$this->limit";
        $result = $this->db->query("select * from ttp_report_finance_import where Dayfinance>='$startday' and Dayfinance<='$stopday' order by ID DESC")->result();
        $nav = $this->db->query("select count(1) as nav,sum(Price) as Total from ttp_report_finance_import where Dayfinance>='$startday' and Dayfinance<='$stopday'")->row();
        $total = $nav ? $nav->Total : 0 ;
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'total'     => $total,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'base_link' =>  base_url().ADMINPATH.'/report/finance_import/',
            'start'     =>  $start,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/finance_import/index',5,$nav,$this->limit)
        );
        $view = 'admin/finance_import_home';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function get_order(){
        $key = isset($_POST['key']) ? mysql_real_escape_string($_POST['key']) : '' ;
        if($key!=''){
            $key = " where MaDH like '%$key%' or Phone like '%$key%' or Name like '%$key%'";
        }
        $result = $this->db->query("select * from ttp_report_order $key order by ID DESC limit 0,20")->result();
        if(count($result)>0){
            echo "<table class='table table-hover'><tr><th>MÃ ĐƠN HÀNG</th><th>KHÁCH NHẬN HÀNG</th><th>SỐ ĐIỆN THOẠI</th><th>GIÁ TRỊ</th><th>NGÀY KHỞI TẠO</th><th>NHẬN TIỀN</th></tr>";
            $arr_money = array(
              1=>'<i class="fa fa-circle text-success" aria-hidden="true"></i> Đã thu',
                0=>'<i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa thu'
            );
            foreach($result as $row){
                $pay = $row->Total-$row->Chietkhau+$row->Chiphi;
                echo "<tr onclick='select_this_order(\"$row->MaDH\",$pay)'>
                        <td>$row->MaDH</td>
                        <td>$row->Name</td>
                        <td>$row->Phone</td>
                        <td>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</td>
                        <td>$row->Ngaydathang</td>
                        <td><a class='status-button'>".$arr_money[$row->FinanceMoney]."</a></td>
                    </tr>";
            }
            echo "</table>";
        }
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Finance Import Tools');
        $view = 'admin/finance_import_add';
        $this->template->write_view('content',$view);
        $this->template->render();
    }

    public function edit_import($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if($this->user->IsAdmin==1){
            $result = $this->db->query("select * from ttp_report_finance_import where ID=$id")->row();
        }else{
            $result = $this->db->query("select * from ttp_report_finance_import where ID=$id and UserID=".$this->user->ID)->row(); 
        }
        if($result){
            $this->template->add_title('Finance Import Tools');
            $data = array(
                'data'=>$result
            );
            $view = 'admin/finance_import_edit';
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }
    }

    public function load_new_person($id=0){
        echo '<option value="0">-- Chọn người nộp --</option>';
        $userlist = $this->db->query("select * from ttp_report_finance_person order by Type")->result();
        if(count($userlist)>0){
            $temp = '';
            $i=1;
            foreach($userlist as $row){
                if($temp!=$row->Type){
                    $temp = $row->Type;
                    if($i>1){
                        echo "</optgroup>";
                    }
                    $title = $row->Type==1 ? "Tổ chức" : "Cá nhân" ;
                    echo "<optgroup label='$title'>";
                }
                $selected = $id==$row->ID ? 'selected="selected"' : '' ;
                echo "<option value='$row->ID' $selected>$row->Name</option>";
                $i++;
            }
            echo "</optgroup>";
        }
    }

    public function add_new_person(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $NamePerson = isset($_POST['NamePerson']) ? trim($_POST['NamePerson']) : '' ;
        $TypePerson = isset($_POST['TypePerson']) ? $_POST['TypePerson'] : 0 ;
        $ConnectID = isset($_POST['ConnectID']) ? $_POST['ConnectID'] : 0 ;
        $response = array('error'=>0,'message'=>'');
        if($NamePerson==''){
            $response['error'] = 1;
            $response['message'] = 'Vui lòng điền tên cá nhân / tổ chức';
        }
        if($response['error']==0){
            $sql = array(
                'Name'  => $NamePerson,
                'Type'  => $TypePerson,
                'ConnectID' => $ConnectID,
                'Created'   => date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_finance_person",$sql);
            $id = $this->db->insert_id();
            $response['id'] = $id;
        }
        echo json_encode($response);
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $DayFinance = isset($_POST['DayFinance']) ? $_POST['DayFinance'] : date('Y-m-d') ;
        $BankAccountID = isset($_POST['BankAccountID']) ? $_POST['BankAccountID'] : 0 ;
        $MoneyImport = isset($_POST['MoneyImport']) ? $_POST['MoneyImport'] : 0 ;
        $TypeImport = isset($_POST['TypeImport']) ? $_POST['TypeImport'] : 0 ;
        $SourceImportID = isset($_POST['SourceImport']) ? $_POST['SourceImport'] : 0 ;
        $PaymentImport = isset($_POST['PaymentImport']) ? $_POST['PaymentImport'] : 0 ;
        $CashID = isset($_POST['CashID']) ? $_POST['CashID'] : 0 ;
        $OrderCode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $ReciveToID = isset($_POST['ReciveToID']) ? $_POST['ReciveToID'] : 0 ;
        $OrderID = 0;
        $Harcode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $response = array("error"=>0,"message"=>"");
        if($MoneyImport<=0){
            $response = array("error"=>1,"message"=>"Số tiền thu bạn nhập vào không hợp lệ !");
        }else{
            if($PaymentImport==1 && $BankAccountID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn tài khoản ngân hàng !");
            }
            if($PaymentImport==0 && $CashID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn quỹ tiền mặt !");
            }
            if($SourceImportID==0){
                $check = $this->db->query("select ID,MaDH from ttp_report_order where MaDH='$OrderCode'")->row();
                if($check){
                    $OrderID = $check->ID;
                    $Harcode = $check->MaDH;
                }else{
                    $response = array("error"=>1,"message"=>"Mã chứng từ không hợp lệ !");
                }
            }
            if($ReciveToID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn người nộp tiền");
            }
        }
        if($response['error']==0){
            $sql = array(
                'Dayfinance'    => $DayFinance,
                'UserID'        => $this->user->ID,
                'TypeFinance'   => $TypeImport,
                'SourceFinance' => $SourceImportID,
                'Price'         => $MoneyImport,
                'Payment'       => $PaymentImport,
                'AccountPayment'=> $BankAccountID,
                'OrderID'       => $OrderID,
                'Hardcode'      => $Harcode,
                'Note'          => $Note,
                'CashPayment'   => $CashID,
                'PersonID'      => $ReciveToID,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_report_finance_import',$sql);
            if($TypeImport==0){
                if($OrderID>0){
                    $this->db->query("update ttp_report_order set FinanceMoney=1 where ID=$OrderID");
                }
                if($PaymentImport==0){
                    $this->db->query("update ttp_report_finance_cash set Price=Price+$MoneyImport where ID=$CashID");
                    $this->cash_flow($MoneyImport,$CashID,'up');
                }else{
                    $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$MoneyImport where ID=$BankAccountID");
                    $this->bankpayment_flow($MoneyImport,$BankAccountID,'down');
                }
            }
        }
        echo json_encode($response);
    }

    public function bankpayment_flow($money=0,$account=0,$type='up'){
        if($account>0 && $money>0){
            $check = $this->db->query("select * from ttp_report_finance_bankaccount_flow where Dayfinance='".date('Y-m-d')."' and BankAccountID=$account")->row();
            if($check){
                if($type=="up"){
                    $this->db->query("update ttp_report_finance_bankaccount_flow set Price=Price+$money where ID=$check->ID");
                }
                if($type=="down"){
                    $this->db->query("update ttp_report_finance_bankaccount_flow set Price=Price-$money where ID=$check->ID");
                }
            }else{
                $current = $this->db->query("select * from ttp_report_finance_bankaccount_flow where BankAccountID=$account and Dayfinance<'".date('Y-m-d')."' order by Dayfinance DESC")->row();
                $current = $current ? $current->Price : 0 ;
                if($type=="up"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'BankAccountID' => $account,
                        'Price'         => $current+$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_bankaccount_flow",$data);
                }
                if($type=="down"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'BankAccountID' => $account,
                        'Price'         => $current-$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_bankaccount_flow",$data);
                }
            }
        }
    }

    public function cash_flow($money=0,$account=0,$type='up'){
        if($account>0 && $money>0){
            $check = $this->db->query("select * from ttp_report_finance_cash_flow where Dayfinance='".date('Y-m-d')."' and CashAccountID=$account")->row();
            if($check){
                if($type=="up"){
                    $this->db->query("update ttp_report_finance_cash_flow set Price=Price+$money where ID=$check->ID");
                }
                if($type=="down"){
                    $this->db->query("update ttp_report_finance_cash_flow set Price=Price-$money where ID=$check->ID");
                }
            }else{
                $current = $this->db->query("select * from ttp_report_finance_cash_flow where CashAccountID=$account and Dayfinance<'".date('Y-m-d')."' order by Dayfinance DESC")->row();
                $current = $current ? $current->Price : 0 ;
                if($type=="up"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'CashAccountID' => $account,
                        'Price'         => $current+$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_cash_flow",$data);
                }
                if($type=="down"){
                    $data = array(
                        'Dayfinance'    => date('Y-m-d'),
                        'CashAccountID' => $account,
                        'Price'         => $current-$money,
                        'Created'       => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_finance_cash_flow",$data);
                }
            }
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $DayFinance = isset($_POST['DayFinance']) ? $_POST['DayFinance'] : date('Y-m-d') ;
        $BankAccountID = isset($_POST['BankAccountID']) ? $_POST['BankAccountID'] : 0 ;
        $MoneyImport = isset($_POST['MoneyImport']) ? $_POST['MoneyImport'] : 0 ;
        $TypeImport = isset($_POST['TypeImport']) ? $_POST['TypeImport'] : 0 ;
        $SourceImportID = isset($_POST['SourceImport']) ? $_POST['SourceImport'] : 0 ;
        $PaymentImport = isset($_POST['PaymentImport']) ? $_POST['PaymentImport'] : 0 ;
        $CashID = isset($_POST['CashID']) ? $_POST['CashID'] : 0 ;
        $OrderCode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $OrderID = 0;
        $Harcode = isset($_POST['OrderCode']) ? $_POST['OrderCode'] : '' ;
        $ReciveToID = isset($_POST['ReciveToID']) ? $_POST['ReciveToID'] : 0 ;
        $response = array("error"=>0,"message"=>"");
        if($MoneyImport<=0){
            $response = array("error"=>1,"message"=>"Số tiền thu bạn nhập vào không hợp lệ !");
        }else{
            if($PaymentImport==1 && $BankAccountID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn tài khoản ngân hàng !");
            }
            if($PaymentImport==0 && $CashID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn quỹ tiền mặt !");
            }
            if($SourceImportID==0){
                $check = $this->db->query("select ID,MaDH from ttp_report_order where MaDH='$OrderCode'")->row();
                if($check){
                    $OrderID = $check->ID;
                    $Harcode = $check->MaDH;
                }else{
                    $response = array("error"=>1,"message"=>"Mã chứng từ không hợp lệ !");
                }
            }
            if($ReciveToID==0){
                $response = array("error"=>1,"message"=>"Vui lòng chọn người nộp tiền");
            }
        }
        if($response['error']==0){
            $import = $this->db->query("select * from ttp_report_finance_import where ID=$ID")->row();
            if($import){
                if($PaymentImport==1){
                    $CashID = 0;
                }else{
                    $BankAccountID = 0;
                }
                $sql = array(
                    'Dayfinance'    => $DayFinance,
                    'TypeFinance'   => $TypeImport,
                    'SourceFinance' => $SourceImportID,
                    'Price'         => $MoneyImport,
                    'Payment'       => $PaymentImport,
                    'AccountPayment'=> $BankAccountID,
                    'OrderID'       => $OrderID,
                    'Hardcode'      => $Harcode,
                    'Note'          => $Note,
                    'PersonID'      => $ReciveToID,
                    'CashPayment'   => $CashID
                );
                $this->db->where("ID",$ID);
                $this->db->update('ttp_report_finance_import',$sql);
                if($TypeImport==1 && $import->TypeFinance==0){
                    if($import->Payment==0){
                        $this->db->query("update ttp_report_finance_cash set Price=Price-$import->Price where ID=$import->CashPayment");
                        $this->cash_flow($import->Price,$import->CashPayment,'down');
                    }else{
                        $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$import->Price where ID=$import->AccountPayment");
                        $this->bankpayment_flow($import->Price,$import->AccountPayment,'down');
                    }
                    if($OrderID>0){
                        $this->db->query("update ttp_report_order set FinanceMoney=0 where ID=$import->OrderID");
                    }
                }
                if($TypeImport==0 && $import->TypeFinance==1){
                    if($PaymentImport==0){
                        $this->db->query("update ttp_report_finance_cash set Price=Price+$MoneyImport where ID=$CashID");
                        $this->cash_flow($MoneyImport,$CashID,'up');
                    }else{
                        $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$MoneyImport where ID=$BankAccountID");
                        $this->bankpayment_flow($MoneyImport,$BankAccountID,'up');
                    }
                    if($OrderID>0){
                        $this->db->query("update ttp_report_order set FinanceMoney=1 where ID=$OrderID");
                    }
                }
                if($TypeImport==0 && $import->TypeFinance==0){
                    if($PaymentImport==$import->Payment){
                        if($PaymentImport==0){
                            if($CashID==$import->CashPayment){
                                $this->db->query("update ttp_report_finance_cash set Price=Price-$import->Price+$MoneyImport where ID=$CashID");
                                $temp = $import->Price+$MoneyImport;
                                $this->cash_flow($temp,$CashID,'down');
                            }else{
                                $this->db->query("update ttp_report_finance_cash set Price=Price-$import->Price where ID=$import->CashPayment");
                                $this->cash_flow($import->Price,$import->CashPayment,'down');
                                $this->db->query("update ttp_report_finance_cash set Price=Price+$MoneyImport where ID=$CashID");
                                $this->cash_flow($MoneyImport,$CashID,'up');
                            }
                        }else{
                            if($BankAccountID==$import->AccountPayment){
                                $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$import->Price+$MoneyImport where ID=$BankAccountID");
                                $temp = $import->Price+$MoneyImport;
                                $this->bankpayment_flow($temp,$BankAccountID,'down');
                            }else{
                                $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$import->Price where ID=$import->AccountPayment");
                                $this->bankpayment_flow($import->Price,$import->AccountPayment,'down');
                                $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$MoneyImport where ID=$BankAccountID");
                                $this->bankpayment_flow($MoneyImport,$BankAccountID,'up');
                            }
                        }
                    }else{
                        if($PaymentImport==0){
                            $this->db->query("update ttp_report_finance_cash set Price=Price+$MoneyImport where ID=$CashID");
                            $this->cash_flow($MoneyImport,$CashID,'up');
                            $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$import->Price where ID=$import->AccountPayment");
                            $this->bankpayment_flow($import->Price,$import->AccountPayment,'down');
                        }else{
                            $this->db->query("update ttp_report_finance_cash set Price=Price-$import->Price where ID=$import->CashPayment");
                            $this->cash_flow($import->Price,$import->CashPayment,'down');
                            $this->db->query("update ttp_report_finance_bankaccount set Price=Price+$MoneyImport where ID=$BankAccountID");
                            $this->bankpayment_flow($MoneyImport,$BankAccountID,'up');
                        }
                    }
                    if($OrderID>0){
                        $this->db->query("update ttp_report_order set FinanceMoney=1 where ID=$OrderID");
                    }
                }
            }
        }
        echo json_encode($response);
    }

    public function delete(){
        $data = isset($_POST['data']) ? (int)$_POST['data'] : 0 ;
        $import = $this->db->query("select * from ttp_report_finance_import where ID=$data")->row();
        if($import){
            if($import->TypeFinance==0){
                if($import->Payment==0){
                    $this->db->query("update ttp_report_finance_cash set Price=Price-$import->Price where ID=$import->CashPayment");
                    $this->cash_flow($import->Price,$import->CashPayment,'down');
                }else{
                    $this->db->query("update ttp_report_finance_bankaccount set Price=Price-$import->Price where ID=$import->AccountPayment");
                    $this->bankpayment_flow($import->Price,$import->AccountPayment,'down');
                }
                $this->db->query("update ttp_report_order set FinanceMoney=0 where ID=$import->OrderID");
            }
            $this->db->query("delete from ttp_report_finance_import where ID=$data");
        }
    }

    public function check_order_code(){
        $data = isset($_POST['data']) ? $_POST['data'] : '' ;
        $check = $this->db->query("select ID from ttp_report_order where MaDH='$data'")->row();
        $response = array("error"=>1,'data'=>"select ID from ttp_report_order where MaDH='$data'");
        if($check){
            $response = array("error"=>0,'data'=>"select ID from ttp_report_order where MaDH='$data'");
        }
        echo json_encode($response);
    }

    public function export(){
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $result = $this->db->query("select * from ttp_report_finance_import where Dayfinance>='$startday' and Dayfinance<='$stopday'")->result();
        if(count($result)>0){
            $arr_cash = array();
            $cash = $this->db->query("select * from ttp_report_finance_cash")->result();
            if(count($cash)>0){
                foreach ($cash as $row) {
                    $arr_cash[$row->ID] = $row->Title;
                }
            }

            $arr_bank = array();
            $bank = $this->db->query("select * from ttp_report_finance_bankaccount")->result();
            if(count($bank)>0){
                foreach ($bank as $row) {
                    $arr_bank[$row->ID] = $row->Title;
                }
            }

            $arr_finance_type = $this->lib->get_config_define("finance_import","finance_import_type");
            $arr_finance_source = $this->lib->get_config_define("finance_import","finance_import_source");
            $arr_payment = array(0=>"Tiền mặt",1=>"Chuyển khoản");

            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày thu')
                        ->setCellValue('B1', 'Loại phiếu thu')
                        ->setCellValue('C1', 'Nguồn thu')
                        ->setCellValue('D1', 'Mã chứng từ')
                        ->setCellValue('E1', 'Số tiền')
                        ->setCellValue('F1', 'Thanh toán')
                        ->setCellValue('G1', 'Quỹ / tài khoản thu')
                        ->setCellValue('H1', 'Ghi chú');
            $arr_payment = array(0=>"COD",1=>"Chuyển khoản");
            $arr_type = $this->lib->get_config_define('status','order');
            $i=2;
            foreach($result as $row){
                if($row->Payment==0){
                    $h = isset($arr_cash[$row->CashPayment]) ? $arr_cash[$row->CashPayment] : "--";
                }else{
                    $h = isset($arr_bank[$row->AccountPayment]) ? $arr_bank[$row->AccountPayment] : "--";
                }
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, date('d/m/Y',strtotime($row->Dayfinance)))
                        ->setCellValue('B'.$i, $arr_finance_type[$row->TypeFinance])
                        ->setCellValue('C'.$i, $arr_finance_source[$row->SourceFinance])
                        ->setCellValue('D'.$i, $row->Hardcode)
                        ->setCellValue('E'.$i, $row->Price)
                        ->setCellValue('F'.$i, $arr_payment[$row->Payment])
                        ->setCellValue('G'.$i, $h)
                        ->setCellValue('H'.$i, $row->Note);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('RESULT_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data !";
        }
    }
}
