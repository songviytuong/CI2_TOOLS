<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
    .body_content .containner .black .box_inner .block1_inner h1{
        font-size:13px;
    }
    .daterangepicker{width: auto;}
</style>
<?php
    $disabled = "disabled";
?>
<div class="alert alert-success alert-dismissable showMessenger hidden">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <p class="Messenger"></p>
</div>
<form name="frmPlanerEdit" id="frmPlanerEdit" method="post">
<div class="table_data">
    <table id="table_data">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-left col-xs-3">Nội dung hành động <font class="text-danger">(*)</font>:</th>
            <th class="text-center" colspan="2"></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center" colspan="3"><textarea id="planer_name" name="planer_name" placeholder="Nhập mô tả kế hoạch..." class="form-control ckeditor">Nhập hành động...</textarea></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Thời gian bắt đầu <font class="text-danger">(*)</font>:</th>
            <th class="text-center">
                <div class="form-group">
                    <input type='text' name="planer_start" id='planer_start' placeholder="<?=date('Y-m-d h:s:i',time());?>" value="" class="form-control" />
                </div>
            </th>
            <th class="text-center"></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3"><small>Thời gian kết thúc <font class="text-danger">(*)</font>:</small></th>
            <th class="text-center">
                <div class="form-group">
                    <input type='text' name="planer_end" id='planer_end' placeholder="<?=date('Y-m-d h:s:i',time());?>" value="" class="form-control" />
                </div>
            </th>
            <th class="text-center"></th>
        </tr>
        <?php
            $moneyData = $dataPar->money_need;
            $symbolData = json_decode($dataPar->symbol);
            $valueData = json_decode($dataPar->value);
            $dataTitle = json_decode($dataPar->action_title);
            $last = count($valueData);
            for($i=0;$i<$last;$i++){
                foreach($result_symbol as $sym){
                    if($sym->code == $symbolData[$i]){
                        $configSymbol = $sym->name;
                    }
                }
        ?>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-left col-xs-4"><span style="font-size:11px;">Tên mục tiêu:</span><input type="text" name="action_title[]" class="form-control" value="<?=$dataTitle[$i];?>" placeholder="Nhập hành động" disabled=""/></th>
            <th class="text-left">
                <span style="font-size:11px;">Giá trị/ kế hoạch (<?=$configSymbol;?>):<input type="hidden" id="symbol" name="symbol[]" value="<?=$symbolData[$i];?>"/></span>
                <div class='input-group'>
                <input type="text" id="planer_focus" onkeyup="keyUp(this);" name="planer_focus[]" value="" placeholder="0" class="form-control"/>
                <span class="input-group-addon" style="min-width:100px;">/ <?=number_format($valueData[$i]);?></span>
                </div>
            </th>
            <th class="text-left"><span style="font-size:11px;">Thực tế:</span><input type="text" name="planer_action[]" value="" class="form-control" placeholder="0" onkeyup="keyUp(this);" <?=$disabled;?>/></th>
        </tr>
        <?php } ?>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3"></th>
            <th class="text-left">
                <span style="font-size:11px;">Chi phí/ Ngân sách (VNĐ):</span>
                <div class='input-group'>
                    <input type="text" id="planer_money" name="planer_money" value="" placeholder="0" class="form-control"/>
                    <span class="input-group-addon" style="min-width:100px;">/ <?=number_format($moneyData);?></span>
                </div>
            </th>
            <th class="text-left">
                <span style="font-size:11px;">Thực tế:</span><input type="text" name="money_action" value="" class="form-control" placeholder="0" onkeyup="keyUp(this);" <?=$disabled;?>/>
            </th>
        </tr>
        <tr>
            <th colspan="2"></th>
            <th><a class="btn btn-success updatePlaner"><i class="fa fa-save"></i> Thêm mới</a></th>
        </tr>
    </table>
    <input type="hidden" name="planer_id" id="planer_id" value="0"/>
    <input type="hidden" name="parent" id="parent" value="<?=$parent;?>"/>
    <input type='hidden' name="market_id" id="market_id" value="<?=$market_id;?>" />
    <input type="hidden" name="money" id="money" value=""/>
</div>
</form>
<script type="text/javascript">
function keyUp(ob){
    var val = $(ob).val();
    val = val.replace(/[^0-9]/g,'');
    $(ob).val(format(val));
}

    var format = function(num){
       var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
       if(str.indexOf(".") > 0) {
           parts = str.split(".");
           str = parts[0];
       }
       str = str.split("").reverse();
       for(var j = 0, len = str.length; j < len; j++) {
           if(str[j] != ",") {
               output.push(str[j]);
               if(i%3 == 0 && j < (len - 1)) {
                   output.push(",");
               }
               i++;
           }
       }
       formatted = output.reverse().join("");
       return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
   };
   
   CKEDITOR.replace('planer_name', {
    toolbar: [
        {name : 'clipboard', items: ['Undo','Redo','Bold','Italic','Underline','Heading']},
        {name: 'paragraph', items: ['Undo'] ,groups: [ 'Undo', 'list', 'indent', 'blocks', 'align', 'bidi' ],  groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-','Cut','Copy','Paste','PasteText','PasteFromWord','-'] },
    ],
    height: 100
});

    $(document).ready(function () {
        $('#planer_start,#planer_end').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1",
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        setTimeout(function(){
            //$("#planer_focus").keyup();
            $("#planer_money").keyup();
        });
    });

    $('.updatePlaner').click(function(){
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        $(this).html("<i class='fa fa-refresh fa-spin'></i> Đang thêm mới");
        $(this).addClass("disabled",true);
        sUrl = baselink+"marketing_campaign/addPlaner";
        var start = $('#planer_start').val();
        var end = $('#planer_end').val();
        var focus = $('#planer_focus').val();
        var money = $('#planer_money').val();
        if(start == ""){
            $('#planer_start').focus();
            $('.updatePlaner').removeClass("disabled");
            $('.updatePlaner').html("<i class='fa fa-save'></i> Thêm mới");
        }else if(end == ""){
            $('#planer_end').focus();
            $('.updatePlaner').removeClass("disabled");
            $('.updatePlaner').html("<i class='fa fa-save'></i> Thêm mới");
        }else if(focus == ""){
            $('#planer_focus').focus();
            $('.updatePlaner').removeClass("disabled");
            $('.updatePlaner').html("<i class='fa fa-save'></i> Thêm mới");
        }else if(money == ""){
            $('#planer_money').focus();
            $('.updatePlaner').removeClass("disabled");
            $('.updatePlaner').html("<i class='fa fa-save'></i> Thêm mới");
        } else {
            setTimeout(function(){
                $.ajax({
                    url: sUrl,
                    dataType: "html",
                    type: "POST",
                    data: $('#frmPlanerEdit').serialize(),
                    success: function(result){
                        if(result == "OK"){
                            alert('Thêm mới thành công !!!');
                            location.reload();
                            //$('.Messenger').text("Thêm mới thành công !");
                            //$('.showMessenger').removeClass("hidden");
                            //$('.updatePlaner').removeClass("disabled");
                        }else{
                            console.log(result);
                            alert('Lỗi hệ thống !!!');
                        }
        //                setTimeout(function(){
        //                    $('.showMessenger').addClass("hidden");
        //                    location.reload();
        //                },2000);
                    }
                });
            },1000);
        }
    });
    
    $("#close_overlay").click(function(){
        location.reload();
    });

    $("#planer_money").keyup(function(e){
    var val = $(this).val();
        val = val.replace(/[^0-9]/g,'');
        $(this).val(format(val));
        $('#money').val(val);
    });
    
    $("#planer_focus").keyup(function(e){
    var val = $(this).val();
        val = val.replace(/[^0-9]/g,'');
        $(this).val(format(val));
        $('#focus').val(val);
    });
</script>