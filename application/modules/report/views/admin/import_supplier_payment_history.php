<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>LỊCH SỬ THANH TOÁN</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
            <a class="btn btn-primary pull-left" href="<?php echo $base_link."download_payment_history" ?>" style="position: absolute;margin-left: -150px;top: 0px;"><i class="fa fa-download"></i> Export excel</a>
	    </div>
    </div>
    <div class="row" style="margin:0px">
        <table class="table table-bordered">
            <tr>
                <th>STT</th>
                <th>Ngày thanh toán</th>
                <th>Mã đơn hàng</th>
                <th>Mã SP</th>
                <th>Tên sản phẩm</th>
                <th>Đơn vị</th>
                <th>Số lượng</th>
                <th>Đơn giá</th>
                <th>Thành tiền</th>
                <th>Trạng thái</th>
            </tr>
        <?php 
        if(count($data)>0){
            $arr_products = array();
            foreach($data as $row){
                    $total_row = $row->Amount*$row->Price;
                    $time = date('d_m_Y',strtotime($row->UCCPaid));
                    if(isset($arr_products[$time])){
                        $arr_products[$time]['Data'][] = array(
                            'MaDH'          =>$row->MaDH,
                            'MaSP'          =>$row->BarcodeClient,
                            'Donvi'         =>$row->Donvi,
                            'Title'         =>$row->Title,
                            'Amount'        =>$row->Amount,
                            'Price'         =>$row->Price,
                            'Total'         =>$total_row,
                            'UCC'           =>$row->UCC,
                            'NCC'           =>$row->NCC
                        );
                    }else{
                        $arr_products[$time] = array('Title'=>date('d/m/Y',strtotime($row->UCCPaid)),'Data'=>array());
                        $arr_products[$time]['Data'][] = array(
                            'MaDH'          =>$row->MaDH,
                            'MaSP'          =>$row->BarcodeClient,
                            'Donvi'         =>$row->Donvi,
                            'Title'         =>$row->Title,
                            'Amount'        =>$row->Amount,
                            'Price'         =>$row->Price,
                            'Total'         =>$total_row,
                            'UCC'           =>$row->UCC,
                            'NCC'           =>$row->NCC
                        );
                    }
            }
            $i=1;
            $ncc_array = $this->lib->get_config_define("payment","ncc",0,"code");
            foreach($arr_products as $row){
                if(count($row['Data'])>0){
                    $colspan = count($row['Data']);
                    $k=1;
                    $total_products = 0;
                    $total_price = 0;
                    foreach($row['Data'] as $item){
                        if($k>1){
                            echo "<tr>";
                            echo "<td>".$item['MaDH']."</td>";
                            echo "<td>".$item['MaSP']."</td>";
                            echo "<td>".$item['Title']."</td>";
                            echo "<td>".$item['Donvi']."</td>";
                            echo "<td class='text-right'>".$item['Amount']."</td>";
                            echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                            echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                            echo "<td>".$ncc_array[$item['NCC']]."</td>";
                            echo "</tr>";
                        }else{
                            echo "<tr>";
                            echo "<td rowspan='$colspan'>$i</td>";
                            echo "<td rowspan='$colspan'>".$row['Title']."</td>";
                            echo "<td>".$item['MaDH']."</td>";
                            echo "<td>".$item['MaSP']."</td>";
                            echo "<td>".$item['Title']."</td>";
                            echo "<td>".$item['Donvi']."</td>";
                            echo "<td class='text-right'>".$item['Amount']."</td>";
                            echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                            echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                            echo "<td>".$ncc_array[$item['NCC']]."</td>";
                            echo "</tr>";
                        }
                        $total_products = $total_products+$item['Amount'];
                        $total_price = $total_price+$item['Total'];
                        $k++;
                    }
                    echo "<tr style='background:#f2ffd2'>
                        <td colspan='6'></td>
                        <td class='text-right'><b>".number_format($total_products)."</b></td>
                        <td></td>
                        <td class='text-right'><b>".number_format($total_price)."</b></td>
                        <td></td>
                    </tr>";
                }
                $i++;
            }
        }else{
            echo "<tr><td colspan='11'>Không tìm thấy dữ liệu</td></tr>";
        }
        ?>
        </table>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
	$(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                location.reload();
	            }
	        });	
		});
    });

</script>