<div class="containner">
	<form action="<?php echo $base_link.'update_infomation' ?>" method="post" enctype="multipart/form-data">
	<h3 style="margin-top:0px">Thông tin tài khoản <button type='submit' class='btn btn-danger pull-right'><i class='fa fa-check-square'></i> Cập nhật thông tin</button></h3>
	<hr>
	<div class="row state_affiliate">
		<a>Đăng ký</a>
		<a class='arrow'><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		<a>Duyệt</a>
		<a class='arrow'><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		<a>Đóng phí thành viên</a>
		<a class='arrow'><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		<a>Thành viên chính thức</a>
		<a class='arrow'><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		<a>Nâng cấp thành viên</a>
	</div>
	<hr>
	<?php 
	$data = json_decode($info->Data);
	?>
		<input type='hidden' name='ID' value='<?php echo $info->ID ?>' />
		<div class="row" style="margin:5px 0px">
			<div class="form-group">
				<div class="col-xs-8">
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Họ và tên : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Fullname' value="<?php echo isset($data->Fullname) ? $data->Fullname : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Ngày sinh : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Birthday' value="<?php echo isset($data->Birthday) ? $data->Birthday : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Số CMND : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='CMND' value="<?php echo isset($data->CMND) ? $data->CMND : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Số tài khoản NH : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Bankcode' value="<?php echo isset($data->Bankcode) ? $data->Bankcode : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Tên ngân hàng : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Banktitle' value="<?php echo isset($data->Banktitle) ? $data->Banktitle : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Mã số thuế cá nhân : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='MST' value="<?php echo isset($data->MST) ? $data->MST : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Cấp độ hiện tại : </label>
						<div class="col-xs-3">
							<select class="form-control" name="Level">
								<?php 
								$arr = $this->lib->get_config_define("affiliate","level",1);
								if(count($arr)>0){
									foreach($arr as $row){
										$selected = $info->Level==$row->code ? "selected='selected'" : '' ;
										echo '<option value="'.$row->code.'" '.$selected.'>'.$row->name.'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Ngày đăng ký </label>
						<label for="" class="col-xs-6 control-label">: <?php echo isset($info->Created) ? $info->Created : '' ; ?></label>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Ngày được duyệt </label>
						<label for="" class="col-xs-6 control-label">: <?php echo isset($data->Acceptdate) ? $data->Acceptdate : '' ; ?></label>
					</div>
					<hr>
					<h4 style="margin:0px;">Mật khẩu bảo mật</h4>
					<hr>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Mật khẩu cũ : </label>
						<div class="col-xs-6">
							<input type='password' class='form-control' name='Oldpassword' placeholder="Nhập mật khẩu hiện tại" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Mật khẩu mới : </label>
						<div class="col-xs-6">
							<input type='password' class='form-control' name='Newpassword' placeholder="Mật khẩu mới phải trên 6 ký tự ." />
						</div>
					</div>
					<?php 
					if(isset($_GET['state'])){
						if($_GET['state']==2){
							echo '<div class="alert alert-success">Bạn vừa cập nhật mật khẩu thành công</div>';
						}
						if($_GET['state']==1){
							echo '<div class="alert alert-danger">Cập nhật mật khẩu không thành công !. Mật khẩu cũ không đúng hoặc mật khẩu mới không hợp lệ .</div>';
						}
					}
					?>
					
				</div>
				<div class="col-xs-1"></div>
				<div class="col-xs-3" style="background: #f3f3f3;position:relative;min-height:100px">
					<div class="dvPreview">
						<img src="<?php echo $info->Thumb ? $info->Thumb : 'public/admin/images/user.png' ; ?>" class="img-responsive" style="width:100%;" />
					</div>
					<a class="choosefile">
						<i class="fa fa-camera" style="color:#999"></i>
						Thay đổi hình đại diện
						<input type="file" name="Image_upload" id="choosefile" />
					</a>
				</div>
			</div>
		</div>
	</form>
</div>
<style>
	.form-group{margin-bottom:15px !important;overflow: hidden}
	.choosefile{position: absolute;
    right: 10px;
    bottom: 10px;
    overflow: hidden;
    background: rgba(0,0,0,0.7);
    color: #FFF;
    padding: 0px 10px;
    border-radius: 4px;
    line-height: 35px;}
    .choosefile:hover{color:#fff !important;}
	.choosefile i{    font-size: 20px;
    color: #FFF !important;
    margin-right: 10px;
    float: left;
    margin: 8px 10px 0px 0px;}
	.choosefile input{position: absolute;left: 0px;padding-left: 200px;height: 50px;outline: none;border: none;top:0px;cursor: pointer;}
</style>
<script>
    $(document).ready(function(){
        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if(file.type.match(imageType)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("class", "img-responsive");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            }else{
                console.log("Not an Image");
            }
        });
    });
</script>