<div class="containner opensitebar">
    <div class="manager">
        <form action="<?php echo $base_link . 'update' ?>" method="POST" enctype="multipart/form-data">
            <input type='hidden' name="ID" value='<?php echo $data->ID ?>' />
            <div class="fillter_bar">                
                <div class="block1">
                    <h1>CHỈNH SỬA THÔNG TIN KHO</h1>
                </div>                
                <div class="block2">                    
                    <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Lưu thông tin</button>
                </div>            
            </div>            
            <div class="box_content_warehouse">                
                <div class="block1">                    
                    <div class="row" style="margin:0px">                        
                        <div class="form-group">                            
                            <label class="col-xs-3 control-label">Mã kho hàng </label>                            
                            <label class="col-xs-1 control-label"></label>                            
                            <label class="col-xs-3 control-label">Tên kho hàng </label>                           
                            <label class="col-xs-1 control-label"></label>                           
                            <label class="col-xs-3 control-label">Phân loại kho hàng </label>    
                            <label class="col-xs-1 control-label"></label>                 
                        </div>               
                    </div>             
                    <div class="row" style="margin:0px">    
                        <div class="form-group">             
                            <div class="col-xs-3">           
                                <input type='text' name='MaKho' value='<?php echo $data->MaKho ?>' class="form-control required" required />                            </div>                            <div class="col-xs-1"></div>                            <div class="col-xs-3">                                <input type='text' name='Title' value='<?php echo $data->Title ?>' class="form-control required" required />                            </div>                            <div class="col-xs-1"></div>                            <div class="col-xs-3">                                <select name="" class="form-control">                                    <option>-- Chọn phân loại kho --</option>                                </select>                            </div>                            <div class="col-xs-1"></div>                        </div>                    </div>                    <div class="row" style="margin:0px">                        <div class="form-group">                            <label class="col-xs-3 control-label">Màu sắc đại diện kho </label>                            <label class="col-xs-1 control-label"></label>                            <label class="col-xs-3 control-label">Số dt liên hệ thủ kho (1)</label>                            <label class="col-xs-1 control-label"></label>                            <label class="col-xs-3 control-label">Số dt liên hệ thủ kho (1) </label>                            <label class="col-xs-1 control-label"></label>                        </div>                    </div>                    <div class="row" style="margin:0px">                        <div class="form-group">                            <div class="col-xs-3">                                <input type='color' name="Color" value="<?php echo $data->Color ?>" class="form-control" />                            </div>                            <div class="col-xs-1"></div>                            <div class="col-xs-3">                                <input type='text' name='Phone1' value='<?php echo $data->Phone1 ?>' class="form-control" />                            </div>                            <div class="col-xs-1"></div>                            <div class="col-xs-3">                                <input type='text' name='Phone2' value='<?php echo $data->Phone2 ?>' class="form-control" />                            </div>                            <div class="col-xs-1"></div>                        </div>                    </div>                    <div class="row" style="margin:0px">                        <div class="form-group">                            <label class="col-xs-3 control-label">Khu vực kho </label>                            <label class="col-xs-1 control-label"></label>                            <label class="col-xs-3 control-label">Tỉnh / Thành phố</label>                            <label class="col-xs-1 control-label"></label>                            <label class="col-xs-3 control-label">Quận / Huyện</label>                            <label class="col-xs-1 control-label"></label>                        </div>                    </div>                    <div class="row" style="margin:0px">                        <div class="form-group">                            <div class="col-xs-3">                                <select name='AreaID' id="Khuvuc" class="form-control">                                    <option value="">-- Chọn khu vực --</option>                                    <?php                                    $area = $this->db->query("select ID,Title from ttp_report_area")->result();                                    if (count($area) > 0) {                                        foreach ($area as $row) {                                            $selected = $row->ID == $data->AreaID ? "selected='selected'" : '';                                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                                        }
                                    }
                                    ?>                                
                                </select> 
                            </div>                        
                            <div class="col-xs-1"></div> 
                            <div class="col-xs-3">        
                                <select name='CityID' id="Tinhthanh" class="form-control">                                    <?php
                                    $city = $this->db->query("select ID,Title from ttp_report_city where ID=$data->CityID")->row();
                                    if ($city) {
                                        echo "<option value='$city->ID'>$city->Title</option>";
                                    } else {
                                        echo '<option value="">-- Chọn Tỉnh/Thành --</option>';
                                    }
                                    ?>                             
                                </select>                           
                            </div>                         
                            <div class="col-xs-1"></div>   
                            <div class="col-xs-3">        
                                <select name='DistrictID' id="Quanhuyen" class="form-control"> 
                                   <?php
                                    $district = $this->db->query("select ID,Title from ttp_report_district where ID=$data->DistrictID")->row();
                                    if ($district) {
                                        echo "<option value='$district->ID'>$district->Title</option>";
                                    } else {
                                        echo '<option value="">-- Chọn Quận/Huyện --</option>';
                                    }
                                    ?>                               
                                </select>                        
                            </div>                           
                            <div class="col-xs-1"></div>  
                        </div>                   
                    </div>                
                    <div class="row" style="margin:0px">     
                        <div class="form-group">           
                            <label class="col-xs-6 control-label">Địa chỉ thực tế kho hàng</label>  
                        </div>                
                    </div>                 
                    <div class="row" style="margin:0px">      
                        <div class="form-group">            
                            <div class="col-xs-3">                    
                                <input type='text' name='Address' value='<?php echo $data->Address ?>' class="form-control" />                      
                            </div>               
                            <div class="col-xs-1"></div>    
                            <div class="col-xs-3">               
                                <div class="input-group">          
                                    <input type='text' name='Lat' value='<?php echo $data->Lat ?>' class="form-control" /> 
                                    <span class="input-group-btn">                                     
                                        <span type="button" class="btn btn-info">Latitude</span>  
                                    </span>                              
                                </div>                  
                            </div>                     
                            <div class="col-xs-1"></div>            
                            <div class="col-xs-3">                        
                                <div class="input-group">                  
                                    <input type='text' name='Long' value='<?php echo $data->Long ?>' class="form-control" />    
                                    <span class="input-group-btn">           
                                        <span type="button" class="btn btn-info">Longitude</span>     
                                    </span>                       
                                </div>                    
                            </div>               
                        </div>    
                    </div>                   
                    <div class="row" style="margin:0px">     
                        <div class="form-group">               
                            <div class="col-xs-3">               
                                <div class="input-group">        
                                    <input type='text' name='Address_maps' value='<?php echo $data->Address_maps ?>' class="form-control" />              
                                    <span class="input-group-btn">                                       
                                        <span type="button" class="btn btn-danger verify_latlong">Verify</span>    
                                    </span>                               
                                </div>              
                            </div>  
                        </div>                  
                    </div>      
                    <div class="row" style="margin:0px">      
                        <div class="form-group">               
                            <div class="col-xs-6"><h3>Hình ảnh</h3></div>      
                        </div>                  
                    </div>                  
                    <div class="row" style="margin:0px">  
                        <div class="form-group">           
                            <div class="col-xs-3">         
                                <div class="form-group">        
                                    <span class="btn btn-default btn-file btn-primary">     
                                        Browse <input type="file" name="Image_upload" id="choosefile">   
                                    </span>          
                                </div>                           
                            </div>                       
                        </div>                  
                    </div> 
                                <div class="row" style="margin:0px">  
                        <div class="form-group">           
                            <div class="col-xs-3">         
                                <div class="form-group">        
                                    <div class="dvPreview">   
                                        <?php
                                        if (file_exists($data->Image)) {
                                            echo "<img src='$data->Image' style='max-width:300px' />";
                                        } else {
                                            echo '<span style="padding-top:9px;display:block">No image selected</span>';
                                        }
                                        ?>                              
                                    </div>                           
                                </div>                           
                            </div>                       
                        </div>                  
                    </div>
                    <hr style="margin:0px;padding:0px">                 
                    <div class="row" style="margin:0px">  
                        <div class="form-group">                  
                            <div class="col-xs-6"><h3>Tài khoản quản lý kho</h3></div>  
                        </div>                  
                    </div>                   
                    <hr style="margin:0px;padding:0px">  
                    <div class="row" style="margin:0px">    
                        <div class="form-group">              
                            <label class="col-xs-12 control-label">Chọn tài khoản sau đó bấm "add"</label>  
                        </div>                 
                    </div>                 
                    <div class="row" style="margin:0px">    
                        <div class="form-group">             
                            <?php
                            $user = $this->db->query("select ID,UserName from ttp_user where UserType=2 or UserType=8")->result();
                            if (count($user) > 0) {
                                $user_list = array();
                                echo "<div class='col-xs-3'><select id='user_warehouse' class='form-control'>";
                                foreach ($user as $row) {
                                    $user_list[$row->ID] = $row->UserName;
                                    echo "<option value='$row->ID'>$row->UserName</option>";
                                } echo "</select></div>";
                                echo "<div class='col-xs-9'><a class='btn btn-primary add_user' style='margin-left:10px;' onclick='add_user()'><i class='fa fa-plus'></i> ADD</a></div><div class='col-xs-12' style='margin-top:10px'>";
                                $arr_user = json_decode($data->Manager, true);
                                $data_user = array();
                                if (is_array($arr_user) && count($arr_user) > 0) {
                                    foreach ($arr_user as $row) {
                                        $data_user[] = "'data$row'";
                                        $name = isset($user_list[$row]) ? $user_list[$row] : '--';
                                        echo "<li class='list_owner btn btn-default' style='margin-right:10px'>$name <a onclick='remove_user(this,$row)'>[x]</a> <input type='hidden' name='Manager[]' value='$row' /></li>";
                                    }
                                } echo "</div>";
                            }
                            ?>                        </div>                    </div>                    <div class="row" style="margin:0px">                        <div class="form-group">                            <div class="col-xs-6"><h3>Tài khoản quét qrcode tại kho này</h3></div>                        </div>                    </div>                    <hr style="margin:0px;padding:0px">                    <div class="row" style="margin:0px">                        <div class="form-group">                            <?php
                            $permission = $this->db->query("select * from ttp_report_qrcode_warehouse where WarehouseID=$data->ID")->result();
                            $arr_user = array();
                            if (count($permission) > 0) {
                                foreach ($permission as $row) {
                                    $arr_user[$row->UserID] = 1;
                                }
                            } $qrcode = $this->db->query("select ID,UserName from ttp_user where UserType=12 and Published=1")->result();
                            if (count($qrcode) > 0) {
                                foreach ($qrcode as $row) {
                                    $checked = isset($arr_user[$row->ID]) ? "checked='checked'" : "";
                                    echo "<div class='col-xs-2'><input name='Qrcode[]' value='$row->ID' type='checkbox' $checked /> $row->UserName </div>";
                                }
                            } else {
                                echo "<div class='alert alert-danger'>Hệ thống hiện tại không có danh sách tài khoản loại quét mã QR Code !</div>";
                            }
                            ?>                  
                        </div>
                    </div>  
                </div>          
            </div>      
        </form>  
        <input type='hidden' id='baselink' value='<?php echo $base_link ?>' />  
    </div>    
    <div class="over_lay black">    
        <div class="box_inner">    
            <div class="block1_inner"><h1></h1>
                <a id="close_overlay"><i class="fa fa-times"></i></a>
            </div>  
            <div class="block2_inner"></div>
        </div>   
    </div>

</div>

<script type = "text/javascript" >
    var link = "<?php echo base_url() . ADMINPATH . '/report/' ?>";
    $("#close_overlay").click(function () {
        $(".over_lay").hide();
        disablescrollsetup();
    });
    $("#show_thaotac").click(function () {
        $(this).parent('li').find('ul').toggle();
    });

    function checkfull(ob) {
        if (ob.checked === true) {
            $("#table_data .selected_products").each(function () {
                var parent = $(this).parent('td').parent('tr');
                parent.find('input[type="checkbox"]').prop("checked", true);
            });
        } else {
            $("#table_data .selected_products").each(function () {
                var parent = $(this).parent('td').parent('tr');
                parent.find('input[type="checkbox"]').prop("checked", false);
            });
        }
    }
    $("#Khuvuc").change(function () {
        var ID = $(this).val();
        if (ID != '') {
            $.ajax({
                url: link + "import_order/get_city_by_area",
                dataType: "html",
                type: "POST",
                context: this,
                data: "ID=" + ID,
                success: function (result) {
                    $("#Tinhthanh").html(result);
                }
            });
        }
    });
    $("#Tinhthanh").change(function () {
        var ID = $(this).val();
        if (ID != '') {
            $.ajax({
                url: link + "import_order/get_district_by_city",
                dataType: "html",
                type: "POST",
                context: this,
                data: "ID=" + ID,
                success: function (result) {
                    var jsonobj = JSON.parse(result);
                    $("#Quanhuyen").html(jsonobj.DistrictHtml);
                    $("#KhoID").html(jsonobj.WarehouseHtml);
                }
            });
        }
    });
    $("#add_position_to_warehouse").click(function () {
        $("#table_data .last_tr").remove();
        $("#table_data").append("<tr><td><input type='checkbox' class='selected_products' data='' /></td><td><input type='text' name='Position[]' /></td><td><input type='text' name='Rack[]' /></td><td><input type='text' name='Colum[]' /></td><td><input type='text' name='Row[]' /></td></tr>");
    });
    $("#delete_row_table").click(function () {
        $(this).parent('li').parent('ul').toggle();
        var list = "";
        $("#table_data .selected_products").each(function () {
            if (this.checked === true) {
                var ID = $(this).attr('data');
                if (ID != '') {
                    list = list + "|" + ID;
                }
                $(this).parent('td').parent('tr').remove();
            }
        });
        if (list != '') {
            $.ajax({
                url: link + "warehouse_warehouse/delete_position",
                dataType: "html",
                type: "POST",
                context: this,
                data: "list=" + list,
                success: function (result) {}
            });
        }
    });

    function enablescrollsetup() {
        $(window).scrollTop(70);
        $("body").css({
            'height': '100%',
            'overflow-y': 'hidden'
        });
        h = window.innerHeight;
        h = h - 200;
        $(".over_lay .box_inner .block2_inner").css({
            "max-height": h + "px"
        });
    }

    function disablescrollsetup() {
        $("body").css({
            'height': 'auto',
            'overflow-y': 'scroll'
        });
    }

    function edit_row(ob, ID) {
        enablescrollsetup();
        $(".over_lay .box_inner").css({
            "width": "500px"
        });
        $(".over_lay .box_inner .block1_inner h1").html("Chỉnh sửa thông tin khu vực");
        $(".over_lay .box_inner .block2_inner").load(link + 'warehouse_warehouse/edit_position/' + ID);
        $(".over_lay").show();
    }
    var celldata = [<?php echo implode(',', $data_user) ?>];

    function add_user() {
        var user = $("#user_warehouse").val();
        var name = $("#user_warehouse option:selected").text();
        if (jQuery.inArray("data" + user, celldata) < 0) {
            $("#user_warehouse").parent('.col-xs-3').parent('.form-group').find('.col-xs-12').append("<li class='list_owner btn btn-default'>" + name + "<a onclick='remove_user(this," + user + ")'>[x]</a> <input type='hidden' name='Manager[]' value='" + user + "' /></li>");
            celldata.push("data" + user);
        }
    }

    function remove_user(ob, user) {
        $(ob).parent('li').remove();
        var index = celldata.indexOf("data" + user);
        celldata.splice(index, 1);
    }
    $('.verify_latlong').click(function () {
        var add = $('input[name="Address_maps"]').val();
        add = add.replace(/ /g, "+");
        getLatLong(add);
    });

    function getLatLong(add) {

        $.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + add + '&sensor=true', function (data) {
            if (data.status == "OK") {
                $('input[name="Lat"]').val(data.results[0].geometry.location.lat);
                $('input[name="Long"]').val(data.results[0].geometry.location.lng);
            }
        });
    }
    $("#choosefile").change(function () {
        var Fileinput = document.getElementById("choosefile");
        var file = Fileinput.files[0];
        var imageType = /image.*/
        var dvPreview = $(".dvPreview");
        if (file.type.match(imageType)) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $("<img />");
                img.attr("style", "max-height:300px;max-width: 400px");
                img.attr("src", e.target.result);
                dvPreview.html(img);
            }
            reader.readAsDataURL(file);
        } else {
            console.log("Not an Image");
        }
    });
</script>