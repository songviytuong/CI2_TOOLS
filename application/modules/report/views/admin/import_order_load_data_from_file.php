<?php 
	if(!isset($data)){
		if(isset($error) && $error!=''){
			echo "<div class='alert alert-danger text-center'>$error</div>";
		}
		echo "
			<form id='form-file' method=l'post' enctype='multipart/form-data'>
			<p class='text-center' style='color:#ccc;font-size:150px'><i class='fa fa-cloud-upload' aria-hidden='true'></i></p>
			<h4 class='text-center'>Vui lòng chọn file import từ máy tính</h4>
			<p class='text-center'><button class='btn btn-primary' style='position:relative;overflow:hidden;margin-top:20px'><input type='file' name='file' id='file_import' onchange='activeimport()' style='position:absolute;left:0px;top:0px;padding-left: 200px;height: 36px;cursor: pointer;' /><i class='fa fa-upload'></i> Brown file</button></p>
			<p class='text-center' style='margin-top:20px'>(*) chỉ chấp nhận file excel (xls,xlsx)</p></form>
		";
	}else{
		if(count($data)>0){
			echo "<div class='alert alert-success text-center'>Nội dung chi tiết file <b>\"$filename\"</b> bạn vừa tải lên hệ thống . Vui lòng kiểm tra lại thật kỹ trước khi nhấn lệnh nhập dữ liệu . <br>
				<p class='text-center' style='margin-top: 10px;'><a class='btn btn-success' onclick='active_import(this)'><i class='fa fa-upload'></i> Nhập vào hệ thống</a></p>
			</div>";
			echo "<div class='row'><div class='col-xs-6'><select class='form-control' onchange='changewarehouse(this)'>";
			$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
			if(count($warehouse)>0){
				foreach($warehouse as $row){
					echo "<option value='$row->ID'>$row->MaKho</option>";
				}
			}
			echo "</select></div></div>";
			echo "<table> 
			<tr>
				<th>STT</th>
				<th>Mã QR CODE</th>
				<th>Nội dung QR CODE</th>
				<th></th>
			</tr>";
			$i=1;
			foreach($data as $row){
				$td = $i==1 ? "th" : "td" ;
				$first = $i==1 ? "" : "treach" ;
				echo "<tr class='$first'>
					<$td>$i</$td>
					<$td class='qrcode_td'>".$row['Barcode']."</$td>
					<$td class='description_td'>".$row['Description']."</$td>
					<$td style='width:30px'><a title='Xóa mã QR CODE này ?' onclick='rmtr(this)'><i class='fa fa-times' aria-hidden='true'></i></a></$td>
				</tr>";
				$i++;
			}
			echo "</table>";
		}else{
			echo "
				<form id='form-file' method=l'post' enctype='multipart/form-data'>
				<p class='text-center' style='color:#ccc;font-size:150px'><i class='fa fa-cloud-upload' aria-hidden='true'></i></p>
				<h4 class='text-center'>Vui lòng chọn file import từ máy tính</h4>
				<p class='text-center'><button class='btn btn-primary' style='position:relative;overflow:hidden;margin-top:20px'><input type='file' name='file' id='file_import' onchange='activeimport()' style='position:absolute;left:0px;top:0px;padding-left: 200px;height: 36px;cursor: pointer;' /><i class='fa fa-upload'></i> Brown file</button></p>
				<p class='text-center' style='margin-top:20px'>(*) chỉ chấp nhận file excel (xls,xlsx)</p></form>
			";
		}
	}
?>