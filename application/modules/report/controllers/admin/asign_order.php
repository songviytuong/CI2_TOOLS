<?php 
class Asign_order extends Admin_Controller { 
	public $user;
    public $classname="asign_order";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/import_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function test(){
    	$this->template->write_view('content','test');
        $this->template->render();
    }

    public function changeprice(){
    	$ID = isset($_GET['ID']) ? (int)$_GET['ID'] : 0 ;
    	$price = isset($_GET['price']) ? $_GET['price'] : 0 ;
    	$this->db->query("update ttp_report_orderdetails_bundle set Price=$price where ID=$ID");
    	echo "OK";
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $Type = isset($_GET['Type']) ? $_GET['Type'] : 0 ;

        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $orderby = "a.Ngaydathang DESC";

        if($Type==0){
            $nav = $this->db->query("
                select a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau 
                from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_city d,ttp_user e,ttp_report_history_assign f where f.OrderID=a.ID and f.UserID=b.ID and f.DestinationUserID=e.ID and a.CityID=d.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 $fillter")->row();
            $result = $this->db->query("select a.ID,a.MaDH,a.Status,a.OrderType,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.SoluongSP,a.Total,a.Ngaydathang,a.Reduce,a.Chiphi,c.Name,d.Title as Thanhpho,a.KhoID,b.UserName as User1,e.UserName as User2,f.Created from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_city d,ttp_user e,ttp_report_history_assign f where f.OrderID=a.ID and f.UserID=b.ID and f.DestinationUserID=e.ID and a.CityID=d.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 $fillter order by $orderby $limit_str")->result();
        }else{
            $nav = $this->db->query("
                select a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau from ttp_report_order a,ttp_report_customer c,ttp_report_city d where a.CityID=d.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.UserID=2 $fillter")->row();
            $result = $this->db->query("select a.ID,a.MaDH,a.Status,a.OrderType,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.SoluongSP,a.Total,a.Ngaydathang,a.Reduce,a.Chiphi,c.Name,d.Title as Thanhpho from ttp_report_order a,ttp_report_customer c,ttp_report_city d where a.CityID=d.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.UserID=2 $fillter order by $orderby $limit_str")->result();
        }

        $total = $nav ? $nav->Total : 0;
        $chiphi = $nav ? $nav->Chiphi : 0;
        $soluongban = $nav ? $nav->SoluongBan : 0 ;
        $chietkhau = $nav ? $nav->Chietkhau : 0 ;
        $nav = $nav ? $nav->nav : 0;

        $data = array(
            'fill_data' => $this->session->userdata('fillter'),
            'data'      => $result,
            'total'     => $total,
            'SoluongSP' => $soluongban,
            'chiphi'    => $chiphi,
            'chietkhau' => $chietkhau,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'fillter'   => $fillter,
            'type'      => $Type,
            'base_link' =>  base_url().ADMINPATH.'/report/import_order/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/asign_order/index',5,$nav,$this->limit)
        );       
        $view = 'admin/asign_order_home';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
}