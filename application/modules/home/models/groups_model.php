<?php

class Groups_model extends CI_Model {

    var $tablename = 'auc_groups';

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function getGroups() {
        $this->db->select('*');
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    public function defineGroups() {
        $groups = $this->getGroups();
        $res = array();
        foreach ($groups as $key => $row) {
            $res[$key]['ID'] = $row->ID;
            $res[$key]['Name'] = $row->Name;
        }
        return $res;
    }

}
