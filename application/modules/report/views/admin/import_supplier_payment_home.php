<?php
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$NCC = isset($_GET['NCC']) ? $_GET['NCC'] : 0 ;
$BranchID = isset($_GET['BranchID']) ? $_GET['BranchID'] : 0 ;
$CategoriesID = isset($_GET['CategoriesID']) ? $_GET['CategoriesID'] : 0 ;
$TrademarkID = isset($_GET['TrademarkID']) ? $_GET['TrademarkID'] : 0 ;
$filltype = isset($_GET['filltype']) ? $_GET['filltype'] : 0 ;
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>ĐỐI SOÁT THANH TOÁN</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="row">
        <label for="" class="col-xs-2 control-label">Trạng thái đối soát NCC</label>
        <div class="col-xs-4">
            <select class="form-control" id="NCC" onchange="changefillter()">
                <option value='-1'>-- Tất cả trạng thái --</option>
                <?php
                $ncc_array = $this->lib->get_config_define("payment","ncc",0,"code");
                if(count($ncc_array)>0){
                    foreach($ncc_array as $key=>$row){
                        $selected = $NCC==$key ? "selected='selected'" : "" ;
                        echo "<option value='$key' $selected>$row</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label for="" class="col-xs-2 control-label">Cửa hàng lấy hàng</label>
        <div class="col-xs-4">
            <select class="form-control" id="BranchID" onchange="changefillter()">
                <option value="0">-- Tất cả cửa hàng --</option>
                <?php
                if($this->user->IsAdmin==1){
                    $supplier_list = $this->db->query("select a.* from ttp_report_branch a")->result();
                }else{
                    $supplier_list = $this->db->query("select a.* from ttp_report_branch a,ttp_user_supplier b where a.SupplierID=b.SupplierID and b.UserID=".$this->user->ID)->result();
                }
                $arr_supplier = array();
                $arr_supplier[] = 0;
                if(count($supplier_list)>0){
                    foreach($supplier_list as $row){
                        $selected = $BranchID==$row->ID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                        $arr_supplier[] = $row->SupplierID;
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row">
        <label for="" class="col-xs-2 control-label">Ngành hàng</label>
        <div class="col-xs-4">
            <select class="form-control" id="CategoriesID" onchange="changefillter()">
                <option value="0">-- Tất cả ngành hàng --</option>
                <?php
                if($this->user->IsAdmin==1){
                    $categories = $this->db->query("select a.ID,a.Title from ttp_report_categories a where a.IsLast=1 order by a.Title ASC")->result();
                }else{
                    echo implode(',',$arr_supplier);
                    $categories = $this->db->query("select DISTINCT a.ID,a.Title from ttp_report_categories a,ttp_report_products b,ttp_report_products_categories c where a.IsLast=1 and c.CategoriesID=a.ID and c.ProductsID=b.ID and b.SupplierID in(".implode(',',$arr_supplier).") order by a.Title ASC")->result();
                }
                if(count($categories)>0){
                    foreach($categories as $row){
                        $selected = $CategoriesID==$row->ID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label for="" class="col-xs-2 control-label">Thương hiệu</label>
        <div class="col-xs-4">
            <select class="form-control" id="TrademarkID" onchange="changefillter()">
                <option value="0">-- Tất cả thương hiệu --</option>
                <?php
                $trademark = $this->db->query("select DISTINCT a.ID,a.Title from ttp_report_trademark a,ttp_report_products b where a.ID=b.TrademarkID and b.SupplierID in(".implode(',',$arr_supplier).") order by Title ASC")->result();
                if(count($trademark)>0){
                    foreach($trademark as $row){
                        $selected = $TrademarkID==$row->ID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }

                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin:0px">
        <div style="margin:15px 0px">
            <input type="radio" <?php echo $filltype==0 ? 'checked="checked"' : '' ; ?> onclick="changefilltype(0)" /> Theo sản phẩm <input type="radio" <?php echo $filltype==1 ? 'checked="checked"' : '' ; ?> style="margin-left:20px" onclick="changefilltype(1)" /> Theo ngày lấy hàng
            <input type="hidden" id="filltype" value="<?php echo $filltype ?>" />
            <a href="<?php echo $base_link.'download_payment_home?'.http_build_query($_GET) ?>" class="btn-sm btn-primary pull-right"><i class="fa fa-download"></i> Export excel</a>
            <?php
            if($this->user->IsAdmin==1){
            ?>
            <a class="btn-sm btn-success pull-right" onclick="showimport(0)" style="margin-right:10px"><i class="fa fa-upload"></i> NCC đối soát</a>
            <a class="btn-sm btn-danger pull-right" onclick="showimport(1)" style="margin-right:10px"><i class="fa fa-money" aria-hidden="true"></i> UCC thanh toán</a>
            <a class="btn-sm btn-warning pull-right" onclick="showimport(2)" style="margin-right:10px"><i class="fa fa-check-square" aria-hidden="true"></i> NCC đã nhận tiền</a>
            <?php
            }
            ?>
        </div>
        <table class="table table-bordered">
            <tr>
                <?php
                if($filltype==0){
                ?>
                <th rowspan="2">STT</th>
                <th rowspan="2">Mã SP</th>
                <th rowspan="2">Tên sản phẩm</th>
                <th rowspan="2">Đơn vị</th>
                <th rowspan="2">Mã đơn hàng</th>
                <th rowspan="2">Ngày lấy hàng</th>
                <th rowspan="2">Số lượng</th>
                <th rowspan="2">Đơn giá</th>
                <th rowspan="2">Thành tiền</th>
                <th rowspan="2">Chiết khấu</th>
                <th colspan="2">Kiểm tra</th>
                <th colspan="2" rowspan="2">Phiếu GH</th>
                <?php
                }else{
                ?>
                <th rowspan="2">STT</th>
                <th rowspan="2">Ngày lấy hàng</th>
                <th rowspan="2">Mã đơn hàng</th>
                <th rowspan="2">Mã SP</th>
                <th rowspan="2">Tên sản phẩm</th>
                <th rowspan="2">Đơn vị</th>
                <th rowspan="2">Số lượng</th>
                <th rowspan="2">Đơn giá</th>
                <th rowspan="2">Thành tiền</th>
                <th rowspan="2">Chiết khấu</th>
                <th colspan="2">Kiểm tra</th>
                <th colspan="2" rowspan="2">Phiếu GH</th>
                <?php
                }
                ?>
            </tr>
            <tr>
                <th>UCC</th>
                <th>NCC</th>
            </tr>

        <?php
        if(count($data)>0){
            $arr_products = array();
            foreach($data as $row){
                $categories_json = json_decode($row->CategoriesID);
                $state=0;
                if($CategoriesID>0){
                    if(!in_array($CategoriesID,$categories_json)){
                        $state=1;
                    }
                }
                if($state==0){
                    $total_row = $row->Amount*$row->Price;
                    if($filltype==0){
                        if(isset($arr_products[$row->ID])){
                            $arr_products[$row->ID]['Data'][] = array(
                                'DetailsID'     =>$row->DetailsID,
                                'DeliveryCode'  =>$row->DeliveryCode,
                                'MaDH'          =>$row->MaDH,
                                'TimeReciver'   =>date('d/m/Y',strtotime($row->TimeReciver)),
                                'Amount'        =>$row->Amount,
                                'Price'         =>$row->Price,
                                'Total'         =>$total_row,
                                'UCC'           =>$row->UCC,
                                'NCC'           =>$row->NCC,
                                'Reduce'        =>$row->Reduce
                            );
                        }else{
                            $arr_products[$row->ID] = array('Title'=>$row->Title,'Donvi'=>$row->Donvi,'MaSP'=>$row->BarcodeClient,'Data'=>array());
                            $arr_products[$row->ID]['Data'][] = array(
                                'DetailsID'     =>$row->DetailsID,
                                'DeliveryCode'  =>$row->DeliveryCode,
                                'MaDH'          =>$row->MaDH,
                                'TimeReciver'   =>date('d/m/Y',strtotime($row->TimeReciver)),
                                'Amount'        =>$row->Amount,
                                'Price'         =>$row->Price,
                                'Total'         =>$total_row,
                                'UCC'           =>$row->UCC,
                                'NCC'           =>$row->NCC,
                                'Reduce'        =>$row->Reduce
                            );
                        }
                    }else{
                        $time = date('d_m_Y',strtotime($row->TimeReciver));
                        if(isset($arr_products[$time])){
                            $arr_products[$time]['Data'][] = array(
                                'DetailsID'     =>$row->DetailsID,
                                'DeliveryCode'  =>$row->DeliveryCode,
                                'MaDH'          =>$row->MaDH,
                                'MaSP'          =>$row->BarcodeClient,
                                'Donvi'         =>$row->Donvi,
                                'Title'         =>$row->Title,
                                'Amount'        =>$row->Amount,
                                'Price'         =>$row->Price,
                                'Total'         =>$total_row,
                                'UCC'           =>$row->UCC,
                                'NCC'           =>$row->NCC,
                                'Reduce'        =>$row->Reduce
                            );
                        }else{
                            $arr_products[$time] = array('Title'=>date('d/m/Y',strtotime($row->TimeReciver)),'Data'=>array());
                            $arr_products[$time]['Data'][] = array(
                                'DetailsID'     =>$row->DetailsID,
                                'DeliveryCode'  =>$row->DeliveryCode,
                                'MaDH'          =>$row->MaDH,
                                'MaSP'          =>$row->BarcodeClient,
                                'Donvi'         =>$row->Donvi,
                                'Title'         =>$row->Title,
                                'Amount'        =>$row->Amount,
                                'Price'         =>$row->Price,
                                'Total'         =>$total_row,
                                'UCC'           =>$row->UCC,
                                'NCC'           =>$row->NCC,
                                'Reduce'        =>$row->Reduce
                            );
                        }
                    }
                }
            }
            $i=1;
            $ucc_array = $this->lib->get_config_define("payment","ucc");
            foreach($arr_products as $row){
                if(count($row['Data'])>0){
                    $colspan = count($row['Data']);
                    $k=1;
                    $total_products = 0;
                    $total_price = 0;
                    $total_reduce = 0;
                    foreach($row['Data'] as $item){
                        $item['DeliveryCode'] = $item['DeliveryCode'] =='' ? '--' : $item['DeliveryCode'] ;
                        if($filltype==0){
                            if($k>1){
                                echo "<tr>";
                                echo "<td>".$item['MaDH']."</td>";
                                echo "<td>".$item['TimeReciver']."</td>";
                                echo "<td class='text-right'>".$item['Amount']."</td>";
                                echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Reduce'])."</td>";
                                echo "<td>".$ucc_array[$item['UCC']]."</td>";
                                echo "<td>".$ncc_array[$item['NCC']]."</td>";
                                echo "<td><a onclick='changeDeliveryCode(this,".$item['DetailsID'].")'>".$item['DeliveryCode']."</a></td>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<td rowspan='$colspan'>$i</td>";
                                echo "<td rowspan='$colspan'>".$row['MaSP']."</td>";
                                echo "<td rowspan='$colspan'>".$row['Title']."</td>";
                                echo "<td rowspan='$colspan'>".$row['Donvi']."</td>";
                                echo "<td>".$item['MaDH']."</td>";
                                echo "<td>".$item['TimeReciver']."</td>";
                                echo "<td class='text-right'>".$item['Amount']."</td>";
                                echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Reduce'])."</td>";
                                echo "<td>".$ucc_array[$item['UCC']]."</td>";
                                echo "<td>".$ncc_array[$item['NCC']]."</td>";
                                echo "<td><a onclick='changeDeliveryCode(this,".$item['DetailsID'].")'>".$item['DeliveryCode']."</a></td>";
                                echo "</tr>";
                            }
                        }else{
                            if($k>1){
                                echo "<tr>";
                                echo "<td>".$item['MaDH']."</td>";
                                echo "<td>".$item['MaSP']."</td>";
                                echo "<td>".$item['Title']."</td>";
                                echo "<td>".$item['Donvi']."</td>";
                                echo "<td class='text-right'>".$item['Amount']."</td>";
                                echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Reduce'])."</td>";
                                echo "<td>".$ucc_array[$item['UCC']]."</td>";
                                echo "<td>".$ncc_array[$item['NCC']]."</td>";
                                echo "<td><a onclick='changeDeliveryCode(this,".$item['DetailsID'].")'>".$item['DeliveryCode']."</a></td>";
                                echo "</tr>";
                            }else{
                                echo "<tr>";
                                echo "<td rowspan='$colspan'>$i</td>";
                                echo "<td rowspan='$colspan'>".$row['Title']."</td>";
                                echo "<td>".$item['MaDH']."</td>";
                                echo "<td>".$item['MaSP']."</td>";
                                echo "<td>".$item['Title']."</td>";
                                echo "<td>".$item['Donvi']."</td>";
                                echo "<td class='text-right'>".$item['Amount']."</td>";
                                echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                                echo "<td class='text-right'>".number_format($item['Reduce'])."</td>";
                                echo "<td>".$ucc_array[$item['UCC']]."</td>";
                                echo "<td>".$ncc_array[$item['NCC']]."</td>";
                                echo "<td><a onclick='changeDeliveryCode(this,".$item['DetailsID'].")'>".$item['DeliveryCode']."</a></td>";
                                echo "</tr>";
                            }
                        }
                        $total_products = $total_products+$item['Amount'];
                        $total_price = $total_price+$item['Total'];
                        $total_reduce = $total_reduce+$item['Reduce'];
                        $k++;
                    }
                    echo "<tr style='background:#f2ffd2'>
                        <td colspan='6'></td>
                        <td class='text-right'><b>".number_format($total_products)."</b></td>
                        <td></td>
                        <td class='text-right'><b>".number_format($total_price)."</b></td>
                        <td class='text-right'><b>".number_format($total_reduce)."</b></td>
                        <td colspan='3'></td>
                    </tr>";
                }
                $i++;
            }
        }else{
            echo "<tr><td colspan='13'>Không tìm thấy dữ liệu</td></tr>";
        }
        ?>
        </table>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
	$(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                location.reload();
	            }
	        });
		});
    });

    function changefillter(){
        var NCC = $("#NCC").val();
        var BranchID = $("#BranchID").val();
        var CategoriesID = $("#CategoriesID").val();
        var TrademarkID = $("#TrademarkID").val();
        var filltype = $("#filltype").val();
        window.location = "<?php echo ADMINPATH.'/report/partner_supplier/payment_home?NCC=' ?>"+NCC+"&BranchID="+BranchID+"&CategoriesID="+CategoriesID+"&TrademarkID="+TrademarkID+"&filltype="+filltype;
    }

    function changefilltype(ob){
        $("#filltype").val(ob);
        changefillter();
    }

    var baselink = $("#baselink_report").val();

    function showimport(ob){
        $(".over_lay").addClass("black");
        $(".over_lay").load(baselink+"load_ncc_payment/"+ob);
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    function activeimport(ob){
        var Fileinput = document.getElementById("file_import");
        var file = Fileinput.files[0];
        var fd = new FormData();
        fd.append('file', file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', baselink+'upload_import_payment/'+ob, true);
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                $(".over_lay .box_inner .block2_inner").css({'overflow-x':'hidden'});
                $(".over_lay .box_inner .block2_inner #form-file p:first-child").html('<div class="loader">Loading...</div>');
                $(".over_lay .box_inner .block2_inner #form-file h4").html("Đang tải file lên hệ thống ...");
            }
        };
        xhr.onload = function() {
            if (this.status == 200) {
                $(".over_lay .box_inner .block2_inner").html(xhr.responseText);
            }else{
                console.log(xhr.responseText);
            }
        };
        xhr.send(fd);
    }

    function rmtr(ob){
        $(ob).parent('td').parent('tr').remove();
    }

    function active_import(ob){
        $(ob).addClass("saving");
        $(ob).html('Loading...');
        var arr = new Array();
        $(".over_lay .box_inner .block2_inner table tr.treach").each(function(){
            var ID = $(this).find('.td0').html();
            var Status = $(this).find('.td7').html();
            if(ID!=''){
                var jsonArg1 = new Object();
                jsonArg1.ID = ID;
                jsonArg1.NCC = Status;
                arr.push(jsonArg1);
            }
        });
        $.ajax({
            dataType: "html",
            data: "data="+JSON.stringify(arr),
            url: baselink+'ncc_payment_active',
            cache: false,
            method: 'POST',
            success: function(result) {
                console.log(result);
                if(result=="true"){
                    location.reload();
                }else{
                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
                }
            },
            error:function(result){
                if(result.status==500){
                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
                }
            }
        });
    }

    function active_import_ucc(ob){
        $(ob).addClass("saving");
        $(ob).html('Loading...');
        var arr = new Array();
        $(".over_lay .box_inner .block2_inner table tr.treach").each(function(){
            var ID = $(this).find('.td0').html();
            var Status = $(this).find('.td7').html();
            if(ID!=''){
                var jsonArg1 = new Object();
                jsonArg1.ID = ID;
                jsonArg1.NCC = Status;
                arr.push(jsonArg1);
            }
        });
        $.ajax({
            dataType: "html",
            data: "data="+JSON.stringify(arr),
            url: baselink+'ucc_payment_active',
            cache: false,
            method: 'POST',
            success: function(result) {
                console.log(result);
                if(result=="true"){
                    location.reload();
                }else{
                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
                }
            },
            error:function(result){
                if(result.status==500){
                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
                }
            }
        });
    }

    function changeDeliveryCode(ob,ID){
      var getValue = prompt("Nhập mã phiếu giao hàng từ nhà cung cấp (nếu có)");
      if(getValue!=null){
        $.ajax({
            dataType: "html",
            data: "ID="+ID+"&Code="+getValue,
            url: baselink+'change_deliverycode',
            method: 'POST',
            success: function(result) {
                if(result=="true"){
                    $(ob).html(getValue);
                }else{
                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
                }
            },
            error:function(result){
                alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
            }
        });
      }
    }
</script>
