<div class="page-title">
    <div class="title_left">
        <h3>SYSTEM GROUP
            <small></small>
        </h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>List IP <small> / view all module</small></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th class="col-lg-2">ID</th>
                            <th class="col-lg-2">IP</th>
                            <th class="col-lg-2">Created</th>
                            <th>Published</th>
                            <th class=" no-link last text-right"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($data) > 0) {
                            $i = 0;
                            foreach ($data as $row) {
                                $i++;
                                ?>
                                <tr class="even pointer">
                                    <td><?php echo $row->id; ?></td>
                                    <td><?php echo $row->ip; ?></td>
                                    <td><?php echo $row->created; ?></td>
                                    <td class="a-right a-right status"><?php echo $row->type == 0 ? "Enable" : "Disable"; ?></td>
                                    <td class=" last text-right">
                                        <a class="<?php echo $row->type == 0 ? "done" : "accept"; ?>" data-ip="<?= $row->ip ?>"><i class="fa <?php echo $row->type == 0 ? "fa-check" : "fa-pencil-square-o"; ?>"></i> <?php echo $row->type == 0 ? "Done" : "Accept"; ?> </a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a class='delete_link' data-ip="<?= $row->ip ?>" data-id="<?= $row->id ?>"><i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
                
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th class="col-lg-2">ID</th>
                            <th class="col-lg-2">IP</th>
                            <th class="col-lg-2">Accepted</th>
                            <th>Published</th>
                            <th class=" no-link last text-right"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($data_enabled) > 0) {
                            $i = 0;
                            foreach ($data_enabled as $row) {
                                $i++;
                                ?>
                                <tr class="even pointer tr<?=$row->id;?>">
                                    <td><?php echo $row->id; ?></td>
                                    <td><?php echo $row->ip; ?></td>
                                    <td><?php echo $row->created; ?></td>
                                    <td class="a-right a-right status"><?php echo $row->type == 0 ? "Enable" : "Disable"; ?></td>
                                    <td class=" last text-right">
                                        <a class="<?php echo $row->type == 0 ? "done" : "accept"; ?>" data-ip="<?= $row->ip ?>"><i class="fa <?php echo $row->type == 0 ? "fa-check" : "fa-pencil-square-o"; ?>"></i> <?php echo $row->type == 0 ? "Done" : "Accept"; ?> </a>
                                        <?php if($row->ip != $myip) :?>
                                        &nbsp;&nbsp;&nbsp;
                                        <a class='delete_link' data-ip="<?= $row->ip ?>" data-id="<?= $row->id ?>"><i class="fa fa-trash-o"></i> Delete</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
