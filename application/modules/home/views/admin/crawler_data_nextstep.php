<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CRAWL <small> / Get data from destination</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"> 
                    <div class="form-group">
                        <div class='row rowheader'>
                            <div class='col-md-1 col-sm-1 col-xs-12'>STT</div>
                            <div class='col-md-8 col-sm-8 col-xs-12'>Destination</div>
                            <div class='col-md-2 col-sm-2 col-xs-12'>Website Crawl</div>
                            <div class='col-md-1 col-sm-1 col-xs-12'>Status</div>
                        </div>
                        <?php 
                        if(count($data)>0){
                            $i=1;
                            $data = array_unique($data);
                            foreach($data as $row){
                                if($row!=""){
                                    $ext = explode('.',$row);
                                    $ext = count($ext>0) ? $ext[count($ext)-1] : '' ;
                                    if($ext=='xml'){
                                        $xml = file_get_contents($row);
                                        $xml = explode("<loc>",$xml);

                                        foreach($xml as $item){
                                            $item = explode('</loc>',$item);
                                            if(isset($item[1])){
                                                $str = explode('/',$item[0],4);
                                                $type = isset($str[2]) ? $str[2] : '' ;
                                                $link = isset($str[3]) ? $str[3] : '' ;
                                                if($link!=''){
                                                    echo "<div class='row rowlist'>";
                                                    echo "<div class='col-md-1 col-sm-1 col-xs-12'>$i</div>
                                                            <div class='col-md-8 col-sm-8 col-xs-12'>$link</div>
                                                            <div class='col-md-2 col-sm-2 col-xs-12' id='type$i'>$type</div>
                                                            <div class='col-md-1 col-sm-1 col-xs-12 pink' id='waitting$i'>Waiting</div>
                                                            <input type='hidden' id='link$i' value='".$item[0]."' />
                                                            ";
                                                    echo "</div>";
                                                    $i++;
                                                }
                                            }
                                        }
                                    }else{
                                        $str = explode('/',$row,4);
                                        $type = isset($str[2]) ? $str[2] : '' ;
                                        $link = isset($str[3]) ? $str[3] : '' ;
                                        if($link!=''){
                                            echo "<div class='row rowlist'>";
                                            echo "<div class='col-md-1 col-sm-1 col-xs-12'>$i</div>
                                                    <div class='col-md-8 col-sm-8 col-xs-12'>$link</div>
                                                    <div class='col-md-2 col-sm-2 col-xs-12' id='type$i'>$type</div>
                                                    <div class='col-md-1 col-sm-1 col-xs-12 pink' id='waitting$i'>Waiting</div>
                                                    <input type='hidden' id='link$i' value='".$row."' />";
                                            echo "</div>";
                                            $i++;
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <button type="submit" class="btn btn-success" id="Crawl">Crawl data</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .rowlist{padding:8px 0px;border-bottom:1px solid #eee;}
    .rowheader{padding:8px 0px;border-bottom:1px solid #777;}
    .red{color: red;}
    .blue{color:blue;}
    .pink{color:pink;}
</style>
<script>
    function send_ajax(num, index)
    {
        if (index >= num ){
            window.location="<?php echo $base_link ?>";
            return false;
        }
        $('#waitting'+index).removeClass('pink').addClass('red').html('Sending...');
        var link = $("#link"+index).val();
        var type = $("#type"+index).html();
        $.ajax({
            url : '<?php echo $base_link."crawldata" ?>',
            type : 'POST',
            dataType:"text",
            data : "link="+link+"&type="+type,
            success : function(result)
            {
                if(result=="false"){
                    $('#waitting'+index).html('False');
                }else{
                    $('#waitting'+index).removeClass('red').addClass('blue');
                    $('#waitting'+index).html('Finished');
                }
            }
        })
        .always(function(){
            send_ajax(num, ++index);
        });
    }

    $('#Crawl').click(function()
    {  
        var num = <?php echo $i ?>;
        $(this).hide();
        send_ajax(num, 1);
    });
</script>