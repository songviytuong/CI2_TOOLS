<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Thêm nhà cung cấp</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <label for="" class="control-label col-xs-4">Tên nhà cung cấp</label>
                            <div class="col-xs-8">
                                <input type='text' class="form-control" name="Title" required />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label for="" class="control-label col-xs-4">Mã nhà cung cấp</label>
                            <div class="col-xs-8">
                                <input type='text' class="form-control" name="Code" required />
                            </div>
                        </div>
                    </div>
				</div>
				
				<div class="row">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
							<label for="" class="control-label col-xs-4">Điện thoại</label>
							<div class="col-xs-8">
								<input type='text' class="form-control" name="Phone" />
							</div>
						</div>
                        <div class="col-xs-12 col-sm-6">
							<label for="" class="control-label col-xs-4">Fax</label>
							<div class="col-xs-8">
								<input type='text' class="form-control" name="Fax" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="row">
						<div class="col-xs-12">
							<label for="" class="control-label col-xs-2">Địa chỉ</label>
							<div class="col-xs-10">
								<input type='text' class="form-control" name="Address" required />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12">
							<label for="" class="control-label col-xs-2">Trạng thái hoạt động</label>
							<div class="col-xs-9">
								<input type='radio' name="Published" value="1" checked="true" /> Enable 
								<input type='radio' name="Published" value="0" /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin:0px">
					<div class="form-group">
						<div class="col-xs-6"><h3>Tài khoản quản lý nhà cung cấp</h3></div>
					</div>
				</div>
				<hr style="margin:0px;padding:0px">
				<div class="row" style="margin:0px">
					<div class="form-group">
						<label class="col-xs-12 control-label">Chọn tài khoản sau đó bấm "add" để thêm tài khoản đó vào danh sách quản lý nhà cung cấp .</label>
					</div>
				</div>
				<div class="row" style="margin:0px">
					<div class="form-group">
						<?php 
						$user = $this->db->query("select ID,UserName from ttp_user")->result();
						if(count($user)>0){
							$user_list = array();
							echo "<div class='col-xs-3'><select id='user_warehouse' class='form-control'>";
							foreach($user as $row){
								$user_list[$row->ID] = $row->UserName;
								echo "<option value='$row->ID'>$row->UserName</option>";
							}
							echo "</select></div>";
							echo "<div class='col-xs-9'><a class='btn btn-primary add_user' style='margin-left:10px;' onclick='add_user()'><i class='fa fa-plus'></i> ADD</a></div><div class='col-xs-12' style='margin-top:10px'></div>";
						}
						?>
					</div>
				</div>
				<div class="row userplace">

				</div>
			</div>
		</form>
	</div>
</div>


<script>
	var celldata = [];

	function add_user(){
		var user = $("#user_warehouse").val();
		var name = $("#user_warehouse option:selected" ).text();
		if(jQuery.inArray( "data"+user, celldata )<0){
			$(".userplace").append("<div class='col-xs-2'>"+name+"<a onclick='remove_user(this,"+user+")'>[x]</a> <input type='hidden' name='Manager[]' value='"+user+"' /></div>");
			celldata.push("data"+user);
		}
	}

	function remove_user(ob,user){
		$(ob).parent('div.col-xs-2').remove();
		var index = celldata.indexOf("data"+user);
		celldata.splice(index, 1);
	}
</script>