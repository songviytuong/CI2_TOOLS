<?php
class Giaohangnhanh extends CI_Model {

    //-->Giao hàng nhanh    
    private $apiUrl;
    private $clientID;
    private $password;
    private $apiKey;
    private $apiSecretKey;
    private $_ghn = null;
    private $_sessionToken = null;
    //-->End Giao hàng nhanh
    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $ghn_constants = unserialize(GHNPUB);
        $this->apiUrl       = $ghn_constants["apiUrl"];
        $this->clientID     = $ghn_constants["clientID"];
        $this->password     = $ghn_constants["password"];
        $this->apiKey       = $ghn_constants["apiKey"];
        $this->apiSecretKey = $ghn_constants["apiSecretKey"];
    }
    
    public function getSessionToken(){
        $this->_ghn = new Ghn($this->apiUrl, $this->clientID, $this->password, $this->apiKey, $this->apiSecretKey);
        $this->_sessionToken = $this->_ghn->SignIn();
        return $this->_sessionToken;
    }
    
    public function getGhn(){
        $this->_ghn = new Ghn($this->apiUrl, $this->clientID, $this->password, $this->apiKey, $this->apiSecretKey);
        return $this->_ghn;
    }

}