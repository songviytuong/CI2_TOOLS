<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Email extends CI_Email {

     var $CI;
     var $_mail;
     
     function __construct()
     {
        parent::__construct();
        $CI =& get_instance(); 
     }
     
     function config($data){
        $this->_mail=array(
            "from_sender"       => "votrunghieu007@gmail.com",
            "name_sender"       => "Ứng viên gửi thông tin ứng tuyển từ trantoanphat.vn",
            "subject_sender"    => "Ứng viên gửi thông tin ứng tuyển từ trantoanphat.vn",
        );

        if(isset($data['subject_sender'])){
            $this->_mail['subject_sender'] = $data['subject_sender'];
        }
        if(isset($data['from_sender'])){
            $this->_mail['from_sender'] = $data['from_sender'];
        }
        $this->_mail['to_receiver'] = $data['to_receiver'];
        $this->_mail['message'] = $data['message'];
        $this->_mail['smtp_user'] = $data['user'];
        $this->_mail['smtp_pass'] = $data['password'];
        $this->_mail['protocol'] = $data['protocol'];
        $this->_mail['smtp_host'] = $data['smtp_host'];
        $this->_mail['smtp_port'] = $data['smtp_port'];
     }
     
    function sendmail(){
        $config['protocol']    = $this->_mail['protocol'];
        $config['smtp_host']    = $this->_mail['smtp_host'];
        $config['smtp_port']    = $this->_mail['smtp_port'];
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $this->_mail['smtp_user'];
        $config['smtp_pass']    = $this->_mail['smtp_pass'];
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->initialize($config);
        $this->from($this->_mail['from_sender'], $this->_mail['name_sender']);
        $this->to($this->_mail['to_receiver']); 
        
        $this->subject($this->_mail['subject_sender']);
        $this->message($this->_mail['message']);
        $this->send();
    }
	
}
