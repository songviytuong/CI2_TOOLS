<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <!--<a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>-->
    </div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel tab1 tabcontent">
                    <div class="x_title">
                        <h2>E-Brochure <small> / Edit item</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="ID" value="<?php echo isset($data) ? $data->ID : 0; ?>" />
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="Name" type="text" class="form-control" name="Name" value="<?= isset($data) ? $data->Name : '' ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="Description" type="text" class="form-control" name="Description" value="<?= isset($data) ? $data->Description : '' ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start/End</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input id="Start" type="text" class="form-control" name="Start" value="<?= isset($data) ? $data->Start : '' ?>"/>

                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input id="End" type="text" class="form-control"  name="End" value="<?= isset($data) ? $data->End : '' ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image
                                <span class="btn btn-default btn-file btn-primary">
                                    Browse <input type="file" name="Image_upload" id="choosefile_s" />
                                </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="dvPreview_s">
                                    <?php
                                    if (!empty($data->Image_S) && file_exists($data->Image_S)) {
                                        echo "<img src='$data->Image_S' style='max-width:100%' />";
                                    } else {
                                        echo '<span style="padding-top:9px;display:block">No image selected</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <select name="store[]" multiple class="form-control">
                                        <?php
                                        echo $data->Store;
                                        foreach ($warehouse as $row) {
                                            if ($data->Store) {
                                                ?>
                                                <option value="<?= $row['city_id'] ?>" <?= (in_array($row['city_id'], json_decode($data->Store))) ? "selected='selected'" : ""; ?>><?= $row['city_name'] ?></option>
                                            <?php } else { ?>
                                                <option value="<?= $row['city_id'] ?>"><?= $row['city_name'] ?></option>
                                        <?php }} ?>
                                    </select>
                                </div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</form>
</div>
</div>

<script>
    $(document).ready(function () {
        $('#Start,#End').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD 00:00:00',

        });

        $("#choosefile_s").change(function () {
            var Fileinput = document.getElementById("choosefile_s");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview_s");
            if (file.type.match(imageType)) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:300px;max-width: 400px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            } else {
                console.log("Not an Image");
            }
        });

        $("#choosefile_f").change(function () {
            var Fileinput = document.getElementById("choosefile_f");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview_f = $(".dvPreview_f");
            if (file.type.match(imageType)) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:300px;max-width: 400px");
                    img.attr("src", e.target.result);
                    dvPreview_f.html(img);
                }
                reader.readAsDataURL(file);
            } else {
                console.log("Not an Image");
            }
        });

    });
</script>