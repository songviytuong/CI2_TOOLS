<form action="" method="POST" class="form_inner_popup">
        <!-- warning message -->
    	<div class="alert alert_error hidden"><h5></h5><p></p></div>  
        <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
        <input type="hidden" name="id_chapter" id="id_chapter" value="<?php echo $Chapter ? $Chapter->id : 0; ?>"></input>
        <input type="hidden" name="id_course" id="id_course" value="<?php echo $id ?>"></input>
	    <div class="row">
	        <div class="col-xs-12">
	            <div class="form-group">
	                <label class="col-xs-2 control-label">Tên chương : </label>
	                <div class="col-xs-10">
	                   <input type="text" class="form-control" name="title_chapter_new" id="title_chapter_new" value="<?php echo $Chapter ? $Chapter->title : '' ; ?>" placeholder="Nhập tên chương...."></input>
	                </div>
	             </div>
	        </div>
	    </div>
	    <div class="row" style="padding:5px 0px"></div>
	   	<div class="row">
	        <div class="col-xs-12">
	            <div class="form-group">
	                <label class="col-xs-2 control-label">Phần : </label>
	                <div class="col-xs-10">
	                   <select class="form-control" name='id_part'>
	                   		<option value='0'>-- Chọn phần --</option>
		                   <?php 
		                   	$PartID = $Chapter ? $Chapter->partid : 0 ;
		                   	if(count($Part)>0){
		                   		foreach($Part as $row){
		                   			$selected = $PartID==$row->id ? 'selected="selected"' : '' ;
		                   			echo "<option value='$row->id' $selected>$row->title</option>";
		                   		}
		                   	}
		                   ?>
	                   </select>
	                </div>
	             </div>
	        </div>
	    </div>
	    <div class="fixedtools">
            <a class="btn btn-primary btn-box-inner pull-right" onclick="saves_chapter(this)"><i class="fa fa-floppy-o"></i> Lưu</a>
        </div>
</form>
<script>
	function saves_chapter(){
        var title = $("#title_chapter_new").val();
        var chapterid = $("#chapterid").val();
        var link = $('#link').val();
        $.post(link+"/training/save_chapter/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(msg != "ok"){
                    $(".form_inner_popup .alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                    $(".form_inner_popup .alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ");
                    $(".form_inner_popup .alert_error p").html(msg);
                }else{
                	if(chapterid>0){
                		var rowCount = $('table#table_data_chapter tr:last').index()+1;
                  		var stt = $('table#table_data_chapter tr:last').index()+1;
                  		var table = document.getElementById("table_data_chapter");
                  		var row = table.insertRow(rowCount);
                  		row.insertCell(0).innerHTML = stt;
                  		row.insertCell(1).innerHTML = "<span class='data_change data_change_'"+id+">"+title+"</span>";
                  		row.insertCell(2).innerHTML = "<a class='td_link_edit' onclick='edit_chapter(this,"+id+")' title='Chỉnh sửa' onclick='edit_part()'><i class='fa fa-pencil-square-o'></i> Chỉnh sửa</a><a onclick='del_chapter(this,"+id+")'><i class='fa fa-trash-o fa-lg'></i> Xóa</a>";
                  		$(".over_lay").hide();
				        disablescrollsetup();
				        $(".over_lay .block2_inner").html('');
				        $(window).scrollTop($(document).height());
                  	}else{
                  		$(".form_inner_popup .alert_error").removeClass('alert-danger').addClass("alert-success").removeClass('hidden');
                        $(".form_inner_popup .alert_error h5").hide();
                        $(".form_inner_popup .alert_error p").html("<i class='fa fa-check-circle'></i> Lưu dữ liệu thành công !");
                        $(".data_change_"+id).html(title);
                  	}
                }
            });
        }, 'json');
    }

</script>