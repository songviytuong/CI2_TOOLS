<script src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>
<style>
	table{width:100%;border-collapse:collapse;}td{border:1px solid #E1e1e1;padding:5px;}
	table tr:hover td{background:#a3ffdd;}
	.fillter{position:absolute;top:15px;right:10px;}
	.fillter select{padding:5px 5px;border:1px solid #ccc;border-radius:3px;color:#555;width:130px;}
	*{outline:none;}
</style>
<?php echo "<h1>Report Inventory $date</h1>"; ?>
<table>
	<tr style='background:#EEE'>
		<td>ProductsID</td>
		<td>Shipment</td>
		<td>Available</td>
		<td>OnHand</td>
		<td>Amount</td>
		<td>Change Type</td>
		<td>Warehouse</td>
		<td>Action</td>
		<td>Method</td>
		<td>User</td>
		<td>Time</td>
	</tr>
	<?php 
	$products = isset($_GET['products']) ? $_GET['products'] : 0 ;
	$shipment = isset($_GET['shipment']) ? $_GET['shipment'] : 0 ;
	$warehouse = isset($_GET['warehouse']) ? $_GET['warehouse'] : 0 ;
	$td1 = array();
	$td2 = array();
	$td3 = array();
	foreach($data as $row){
	    $item = json_decode($row);
	    if(count($item)>0){
	    	if(!in_array($item->ProductsID,$td1)){
	    		$td1[] = $item->ProductsID!='' ? $item->ProductsID : 0 ;
	    	}
	    	if(!in_array($item->ShipmentID,$td2)){
	    		$td2[] = $item->ShipmentID;
	    	}
	    	if(!in_array($item->WarehouseID,$td3)){
	    		$td3[] = $item->WarehouseID;
	    	}
	        $color = $item->Available>$item->OnHand ? "background:#ffcfc7" : "" ;
	        if($products!=0 && $shipment!=0 && $warehouse!=0){
	        	$display = $products==$item->ProductsID && $shipment==$item->ShipmentID && $warehouse==$item->WarehouseID ? 1 : 0;
	        }elseif($products!=0 && $shipment!=0 && $warehouse==0){
	        	$display = $products==$item->ProductsID && $shipment==$item->ShipmentID ? 1 : 0;
	        }elseif($products!=0 && $shipment==0 && $warehouse!=0){
	        	$display = $products==$item->ProductsID && $warehouse==$item->WarehouseID ? 1 : 0;
	        }elseif($products==0 && $shipment!=0 && $warehouse!=0){
	        	$display = $shipment==$item->ShipmentID && $warehouse==$item->WarehouseID ? 1 : 0;
	        }elseif($products!=0 && $shipment==0 && $warehouse==0){
	        	$display = $products==$item->ProductsID ? 1 : 0;
	        }elseif($products==0 && $shipment!=0 && $warehouse==0){
	        	$display = $shipment==$item->ShipmentID ? 1 : 0;
	        }elseif($products==0 && $shipment==0 && $warehouse!=0){
	        	$display = $warehouse==$item->WarehouseID ? 1 : 0;
	        }elseif($products==0 && $shipment==0 && $warehouse==0){
	        	$display = 1;
	        }
	        if($display==1){
		        echo "<tr style='$color;'>";
				echo "<td>".$item->ProductsID."</td>";
				echo "<td>".$item->ShipmentID."</td>";
				echo "<td>".$item->Available."</td>";
				echo "<td>".$item->OnHand."</td>";
				echo "<td>".$item->Amount."</td>";
				echo "<td>".$item->Type."</td>";
				echo "<td>".$item->WarehouseID."</td>";
				echo "<td>".$item->Action."</td>";
				echo "<td>".$item->Method."</td>";
				echo "<td>".$item->UserName."</td>";
				echo "<td>".$item->Time."</td>";
		        echo "</tr>";
	        }
	    }
	}
	?>
</table>
<div class="fillter">
	<select id="products" onchange="changefillter()">
		<option value='0'>Chọn sản phẩm</option>
		<?php 
			$str =  implode(',',$td1);
			if($str!=''){
				$productsdata = $this->db->query("select ID,MaSP,Title from ttp_report_products where ID in($str)")->result();
				if(count($productsdata)>0){
					foreach($productsdata as $row){
						$selected=$row->ID==$products ? "selected='selected'" : "" ;
						echo "<option value='$row->ID' $selected>$row->MaSP - $row->Title</option>";
					}
				}
			}
		?>
	</select>
	<select id="shipment" onchange="changefillter()">
		<option value='0'>Chọn lô</option>
		<?php 
			$str =  implode(',',$td2);
			if($str!=''){
				$shipmentdata = $this->db->query("select ID,ShipmentCode from ttp_report_shipment where ID in($str)")->result();
				if(count($shipmentdata)>0){
					foreach($shipmentdata as $row){
						$selected=$row->ID==$shipment ? "selected='selected'" : "" ;
						echo "<option value='$row->ID' $selected>$row->ShipmentCode</option>";
					}
				}
			}
		?>
	</select>
	<select id="warehouse" onchange="changefillter()">
		<option value='0'>Chọn kho</option>
		<?php 
			$str =  implode(',',$td3);
			if($str!=''){
				$warehousedata = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in($str)")->result();
				if(count($warehousedata)>0){
					foreach($warehousedata as $row){
						$selected=$row->ID==$warehouse ? "selected='selected'" : "" ;
						echo "<option value='$row->ID' $selected>$row->MaKho</option>";
					}
				}
			}
		?>
	</select>
</div>
<script type="text/javascript">
	function changefillter(){
		var products = $("#products").val();
		var shipment = $("#shipment").val();
		var warehouse = $("#warehouse").val();
		window.location="<?php echo $base_link ?>"+"?products="+products+"&shipment="+shipment+"&warehouse="+warehouse;
	}
</script>