<div class="containner">
    <div class="manager">
        <form id='form' method="POST">
            <div class="fillter_bar">
                <div class="block1">
                    <h1>NHẬP SỐ DƯ ĐẦU KỲ</h1>
                </div>
                <div class="block2">
                    
                </div>
            </div>
            <div class="box_content_warehouse_import">
                <div class="block1">
                    <table class='table1_balance'>
                        <tr>
                            <td>Ngày kiểm kho:</td>
                            <td><input type='text' name='BalanceDate' id='ImportBalanceDate' value='<?php echo date('Y-m-d') ?>' /></td>
                            <td>Tại kho:</td>
                            <td>
                                <select id="WarehouseList" name='WarehouseID'>
                                    <?php 
                                    $first = "CHƯA CHỌN KHO";
                                    $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
                                    if(count($warehouse)>0){
                                        $i=1;
                                        foreach($warehouse as $row){
                                            $first = $i==1 ? $row->MaKho : $first ;
                                            echo "<option value='$row->ID'>$row->MaKho</option>";
                                            $i++;
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="block2">
                    <a class='btn btn-default' id="add_products_to_order"><i class="fa fa-plus"></i> SẢN PHẨM</a>
                    <a class='btn btn-default' id="update_products_to_order"><i class="fa fa-refresh"></i> CẬP NHẬT</a>
                </div>
                <div class='clear'></div>
                <div class="block3">
                    <table id="table2_balance">
                        <tr>
                            <th rowspan='2'>Mã SP</th>
                            <th rowspan='2'>Tên sản phẩm</th>
                            <th rowspan='2'>ĐVT</th>
                            <th rowspan='2'>Số lô</th>
                            <th rowspan='2'>SL tồn đầu kỳ</th>
                            <th colspan='4'>Vị trí trong kho <span class='name_warehouse'><?php echo $first ?></span></th>
                            <th rowspan='2'></th>
                        </tr>
                        <tr>
                            <th>Khu vực</th>
                            <th>Tên kệ</th>
                            <th>Tên cột</th>
                            <th>Tên hàng</th>
                        </tr>
                        <tr class='last_tr'>
                            <td colspan='10' style='text-align:center'>Vui lòng thêm sản phẩm vào đây .</td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
</div>

<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#ImportBalanceDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });

        $('#ImportBalanceDate').on('apply.daterangepicker', function(ev, picker) {
            change_fillter();
        });
    });

    var link = $("#baselink").val();

    $("#close_overlay").click(function(){
        $(".over_lay").hide();
        disablescrollsetup();
    });

    function enablescrollsetup(){
        $(window).scrollTop(70);
        $("body").css({'height':'100%','overflow-y':'hidden'});
        h = window.innerHeight;
        h = h-200;
        $(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
    }

    function disablescrollsetup(){
        $("body").css({'height':'auto','overflow-y':'scroll'});
    }

    $("#add_products_to_order").click(function(){
        enablescrollsetup();
        $(".over_lay .box_inner").css({"width":"850px"});
        $(".over_lay .box_inner .block2_inner").html("");
        var changelink = link.replace("warehouse_inventory_opening_balance","warehouse_inventory_import");
        $.ajax({
            url: changelink+"get_products/1",
            dataType: "html",
            type: "POST",
            context: this,
            data: "",
            success: function(result){
                if(result!='false'){
                    $(".over_lay").show();
                    $(".over_lay .box_inner").css({'margin-top':'50px'});
                    $(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
                    $(".over_lay .box_inner .block2_inner").html(result);
                }else{
                    alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
    });

    var celldata = [];
    var sttrow = 2;
    function add_products(){
        $("tr.last_tr").remove();
        $(".over_lay .box_inner .block2_inner .selected_products").each(function(){
            if(this.checked===true){
                var data_name = $(this).attr('data-name');
                var data_price = $(this).attr('data-price');
                var data_code = $(this).attr('data-code');
                var data_id = $(this).attr('data-id');
                var data_donvi = $(this).attr('data-donvi');
                var numtd = $(this).parent('td').parent('tr').find('.numbertd').val();
                for (var i = 0; i < numtd; i++) {
                    /*if(jQuery.inArray( "data"+data_id, celldata )<0){*/
                        var table = document.getElementById("table2_balance");
                        var row = table.insertRow(sttrow);
                        row.insertCell(0).innerHTML=data_code+"<input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
                        row.insertCell(1).innerHTML=data_name;
                        row.insertCell(2).innerHTML=data_donvi;
                        row.insertCell(3).innerHTML="<select class='select_shipment' data='"+data_id+"' onchange='add_shipment(this,"+data_id+")' name='ShipmentID[]'></select>";
                        row.insertCell(4).innerHTML="<input type='text' class='BeginBalance' value='0'  name='Amount[]' />";
                        row.insertCell(5).innerHTML="<select class='select_position' data='"+data_id+"' onchange='reload_rack(this)'><option value=''>-- Chọn --</option></select>";
                        row.insertCell(6).innerHTML="<select class='select_rack' data='"+data_id+"' onchange='reload_cols(this)'><option value=''>-- Chọn --</option></select>";
                        row.insertCell(7).innerHTML="<select class='select_col' data='"+data_id+" 'onchange='reload_row(this)'><option value=''>-- Chọn --</option></select>";
                        row.insertCell(8).innerHTML="<select class='select_row' data='"+data_id+"' name='PositionID[]'><option value=''>-- Chọn --</option></select>";
                        row.insertCell(9).innerHTML="<a onclick='remove_row(this,"+data_id+")'><i class='fa fa-times'></i></a>";
                        sttrow=sttrow+1;
                        celldata.push("data"+data_id);
                        load_shipment(data_id);
                    /*}*/
                }
            }
        });
        load_position();
        $(".over_lay").hide();
        disablescrollsetup();
    }

    function remove_row(ob,id){
        /*celldata = jQuery.grep(celldata, function(value) {
          return value != "data"+id;
        });
        sttrow = sttrow-1;*/
        $(ob).parent('td').parent('tr').remove();
    }

    function add_shipment(ob,id){
        var changelink = link.replace("warehouse_inventory_opening_balance","warehouse_inventory_import");
        var data = $(ob).val();
        if(data=="add"){
            enablescrollsetup();
            $(".over_lay .box_inner").css({"width":"500px"});
            $(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
            $(".over_lay .box_inner .block2_inner").html("");
            $(".over_lay .box_inner .block2_inner").load(changelink+"box_add_shipment/"+id);
            $(".over_lay").show();
        }
    }

    function save_shipment(ob,id){
        var changelink = link.replace("warehouse_inventory_opening_balance","warehouse_inventory_import");
        $(ob).addClass("saving");
        var ShipmentCode = $(".ShipmentCode").val();
        var DateProduction = $(".DateProduction").val();
        var DateExpiration = $(".DateExpiration").val();
        if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
            alert("Vui lòng điền đầy đủ thông tin !");
            $(ob).removeClass("saving");
            return false;
        }
        $.ajax({
            url: changelink+"save_shipment",
            dataType: "html",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
                load_shipment(id);
                $(".over_lay").hide();
                disablescrollsetup();
                $(ob).removeClass("saving");
            }
        });
    }

    function load_shipment(id){
        var changelink = link.replace("warehouse_inventory_opening_balance","warehouse_inventory_import");
        $(".select_shipment").each(function(){
            if(id==0){
                var data = $(this).attr('data');
                $(this).load(changelink+"load_shipment_by_products/"+data);
            }else{
                var data = $(this).attr('data');
                if(data==id){
                    $(this).load(changelink+"load_shipment_by_products/"+data);
                }
            }
        });
    }

    function load_position(){
        var WarehouseID = $('#WarehouseList').val();
        $(".select_position").each(function(){
            $(this).load(link+"load_position/?WarehouseID="+WarehouseID);
            $(this).change();
        });
    }

    function reload_rack(ob){
        var data = $(ob).val();
        $.ajax({
            url: link+"load_rack_by_position",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data,
            success: function(result){
                $(ob).parent('td').parent('tr').find('.select_rack').html(result);
            }
        });
    }

    function reload_cols(ob){
        var position = $(ob).parent('td').parent('tr').find('.select_position').val();
        var rack = $(ob).val();
        $.ajax({
            url: link+"load_cols_by_rack",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Position="+position+"&Rack="+rack,
            success: function(result){
                $(ob).parent('td').parent('tr').find('.select_col').html(result);
            }
        });
    }

    function reload_row(ob){
        var rack = $(ob).parent('td').parent('tr').find('.select_rack').val();
        var position = $(ob).parent('td').parent('tr').find('.select_position').val();
        var col = $(ob).val();
        $.ajax({
            url: link+"load_row_by_col",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Position="+position+"&Rack="+rack+"&Col="+col,
            success: function(result){
                $(ob).parent('td').parent('tr').find('.select_row').html(result);
            }
        });
    }

    function input_search_products(ob){
        var data = $(ob).val();
        var changelink = link.replace("warehouse_inventory_opening_balance","warehouse_inventory_import");
        $.ajax({
            url: changelink+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data,
            success: function(result){
                if(result!='false'){
                    $(".over_lay .box_inner .block2_inner").html(result);           
                }else{
                    alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
    }

    $("#WarehouseList").change(function(){
        var warehouse = $(this).val();
        var text = $(this).find('option:selected').text();
        $("span.name_warehouse").html(text);
        load_position();
    });

    $("#update_products_to_order").click(function(){
        $(this).find('i').hide();
        $(this).addClass('saving');
        $('#form').attr('action',link+'add_new');
        $('#form').submit();
    });

    function change_fillter(){
        var WarehouseID = $("#WarehouseList").val();
        var CheckedDay = $("#ImportBalanceDate").val();
        var baselink = $("#baselink_report").val();
        window.location=link+"checkredirect/"+WarehouseID+"/"+CheckedDay;
    }
</script>
