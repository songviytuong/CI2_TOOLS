<?php 
	$district = $this->db->query("select * from ttp_report_district where CityID=30")->result();
	$arr_district = array();
	if(count($district)>0){
		foreach($district as $row){
			$arr_district[$row->ID] = $row->Title;
		}
	}
	$current_dis = isset($_GET['dis']) ? (int)$_GET['dis'] : 0 ;
	$current = isset($arr_district[$current_dis]) ? $arr_district[$current_dis] : 'Tất cả quận / huyện' ;
?>
<div class="containner">
		<div class="import_orderlist">
			<div class="block2 row">
	    		<div class="block_2_1 col-xs-12">
	    			<h3 style="margin:0px;" class="pull-left">Danh sách khách hàng nhận thưởng</h3><a style="margin:0px" class="btn btn-primary pull-right" href="<?php echo $base_link.'download_lucky_number' ?>"><i class="fa fa-download"></i> Export data</a>
	    			<div class="btn-group pull-right">
			            <a class="btn btn-default" style="margin:0px"><i class="fa fa-sort" aria-hidden="true"></i> <?php echo $current ?></a>
			            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
			            <ul class="dropdown-menu">
			            	<li style="<?php echo $current_dis==0 ? "background:#a8dec6" : "" ; ?>"><a href="<?php echo $base_link.'lucky_number' ?>">Tất cả quận / huyện</a></li>
			                <?php 
			                if(count($district)>0){
								foreach($district as $row){
									$selected = $current_dis==$row->ID ? "background:#a8dec6" : "" ;
									echo "<li style='$selected'><a href='".$base_link."lucky_number?dis=$row->ID'>$row->Title</a></li>";
								}
							}
			                ?>
			            </ul>
			        </div>
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>Tên khách hàng</th>
						<th>Số điện thoại</th>
						<th>Email</th>
						<th>Địa chỉ</th>
						<th>Quận / Huyện</th>
						<th>OTP</th>
						<th>Thời gian</th>
						<th>Status</th>
						<th>Order</th>
					</tr>
					<?php
					if(count($data)>0){
						$i=$start+1;
						foreach ($data as $row) {
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>$row->Name</td>";
							echo "<td>$row->Phone</td>";
							echo "<td>$row->Email</td>";
							echo "<td><a title='$row->Address'>$row->Address</a></td>";
							$dis = isset($arr_district[$row->DistrictID]) ? $arr_district[$row->DistrictID] : '--' ;
							echo "<td>$dis</td>";
							echo $row->Success==1 ? "<td>OK</td>" : "<td>False</td>";
							echo "<td>".date('d-m H:i',strtotime($row->Created))."</td>";
							$status = $row->ID>315 ? 'Cơ hội trúng' : 'Đã trúng' ;
							echo "<td>$status</td>";
							$icon = $row->Status==0 ? "<i class='fa fa-check-square' style='color:#ddd'></i>" : "<i class='fa fa-check-square' style='color:#090'></i>" ;
							echo "<td>
									<a onclick='has_order_luckynumber(this,$row->ID)' data-state='$row->Status'>$icon</a>
									<a onclick='add_note_luckynumber(this,$row->ID)' title='$row->Note'><i class='fa fa-pencil' style='color:#777;margin-left:5px'></i></a>
							</td>";
							echo "</tr>";
							$i++;
						}
					}
          			?>
				</table>
				<?php
				$httpbuild = http_build_query($_GET);
		        echo str_replace('href=','onclick="nav(this)" data-get="?'.$httpbuild.'" data=',$nav);
				?>
			</div>
		</div>
</div>
<script>
	function has_order_luckynumber(ob,id){
		var status = $(ob).attr('data-state');
		if(status==1){
			var temp=0;
		}else{
			var temp=1;
		}
		$(ob).attr('data-state',temp);
		$(ob).load("<?php echo $base_link.'has_order_luckynumber/' ?>"+id+"/"+temp);
	}

	function add_note_luckynumber(ob,id){
		var note = prompt("Nhập ghi chú khách hàng");
		if (note != null) {
		    var data = {
				Note:note,
				ID:id
			};
			$.ajax({
				type: 'POST',
				url: '<?php echo $base_link.'add_note_luckynumber' ?>',
				dataType: 'json',
				data: 'Data='+JSON.stringify(data),
				success: function (result) {
					console.log(data);
					console.log(result);
					if(result.error==0){
						alert("Lưu thành công");
					}
				}
			});
		}
	}
</script>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width:150px}
	.body_content .containner table tr td:nth-child(5){max-width: 200px}
</style>
