<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update_export_warehouse' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa thông tin phiếu xuất kho</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Mã Phiếu Xuất Kho</span></div>
					<div class='block2'><input type='text' class="form-control" value="<?php echo isset($data->MaXK) ? $data->MaXK : '' ; ?>" readonly="true" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Hình thức xuất kho</span></div>
					<?php 
					$arr_hinhthuc = array(0=>"NB",1=>"TA");
					?>
					<div class='block2'><input type='text' class="form-control" value="<?php echo isset($arr_hinhthuc[$data->Hinhthucxuatkho]) ? $arr_hinhthuc[$data->Hinhthucxuatkho] : '--' ; ?>" readonly="true" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã Đơn Hàng</span></div>
					<div class='block2'><input type='text' class="form-control" name="OrderID" value="<?php echo isset($data->OrderID) ? $data->OrderID." - ".$data->MaDH : '' ; ?>" readonly="true" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Đơn hàng của user</span></div>
					<div class='block2'><input type='text' class="form-control" value="<?php echo isset($data->UserName) ? $data->UserName : '' ; ?>" readonly="true" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Lý do xuất kho</span></div>
					<div class='block2'><input type='text' class="form-control" name="Lydoxuatkho" value="<?php echo isset($data->Lydoxuatkho) ? $data->Lydoxuatkho : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">TKNO</span></div>
					<div class='block2'><input type='text' class="form-control" name="TKNO" value="<?php echo isset($data->TKNO) ? $data->TKNO : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">TKCO</span></div>
					<div class='block2'><input type='text' class="form-control" name="TKCO" value="<?php echo isset($data->TKCO) ? $data->TKCO : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">KPP</span></div>
					<div class='block2'><input type='text' class="form-control" name="KPP" value="<?php echo isset($data->KPP) ? $data->KPP : '' ; ?>" required /></div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>