<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p><b>CÔNG TY CP DƯỢC PHẨM HOA THIÊN PHÚ</b></p>
				<p>10 Nguyễn Cửu Đàm, P.Tân Sơn Nhì, Q.Tân Phú, TPHCM</p>
				<p>Hotline: 19006033</p>
				<p>Email: support@hoathienphu.com.vn</p>
			</div>
			<div class="block1_2">
				<?php
				$export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$data->ID")->row();
				if($export){
					echo '<a id="print_page"><i class="fa fa-print"></i> In Phiếu</a>';
				}

				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $Type = $data->OrderType==0 ? 'and TypeExport=0' : '' ;
                $Type = $data->OrderType==3 ? 'and TypeExport=3' : $Type ;
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=1 $Type")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                $show = $data->OrderType==0 ? 'OL' : '' ;
                $show = $data->OrderType==3 ? 'GS' : $show ;
                $max = "BH".$show.$thisyear.$thismonth.'.TA.'.str_pad($max, 5, '0', STR_PAD_LEFT);
				?>
				<h1>Phiếu soạn hàng</h1>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaXK : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<p>Mẫu số : 02-VT</p>
				<p>(Ban hành theo TT200/2014/TT-BTC ngày 22/12/2014 của bộ trưởng BTC)</p>
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $export ? $export->TKNO : "1311" ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $export ? $export->TKCO : "5115" ; ?>" /></td>
					</tr>
					<tr>
						<td>KPP</td>
						<td><input type="text" id="KPP" value="<?php echo $export ? $export->KPP : "OL" ; ?>" /></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Xuất tại kho</li>
				<li> :
					<?php echo $data->KhoTitle ?>
					<input type="hidden" id="KhoID" value="<?php echo $data->KhoID ?>" />
				</li>
			</div>
			<div class="row">
				<li>Địa chỉ giao hàng</li>
				<li class="special">: <?php echo $data->AddressOrder ?></li>
			</div>
			<div class="row">
				<li>Ghi chú khách hàng:</li>
				<li class="special"> : <?php echo $data->Note ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th>Số TT</th>
					<th>Tên sản phẩm</th>
					<th>Mã số</th>
					<th>Barcode hệ thống</th>
					<th>Barcode NCC</th>
					<th>Đơn vị tính</th>
					<th>Số lô hàng</th>
					<th>Số lượng</th>
				</tr>
				<?php
				$arr_data = array();
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,a.Barcode,a.BarcodeClient,b.*,c.ShipmentCode from ttp_report_products a,ttp_report_orderdetails b,ttp_report_shipment c where b.ShipmentID=c.ID and a.ID=b.ProductsID and b.OrderID=$data->ID and a.VariantType in(0,1)")->result();
				if(count($details)>0){
					foreach($details as $row){
						if(isset($arr_data[$row->ProductsID.'_'.$row->ShipmentID])){
							$arr_data[$row->ProductsID.'_'.$row->ShipmentID]->Amount = $arr_data[$row->ProductsID.'_'.$row->ShipmentID]->Amount+$row->Amount;
						}else{
							$arr_data[$row->ProductsID.'_'.$row->ShipmentID] = $row;
						}
					}
				}

				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,a.Barcode,a.BarcodeClient,b.*,c.ShipmentCode from ttp_report_products a,ttp_report_orderdetails_bundle b,ttp_report_shipment c where b.ShipmentID=c.ID and a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
				if(count($details)>0){
					foreach($details as $row){
						if(isset($arr_data[$row->ProductsID.'_'.$row->ShipmentID])){
							$arr_data[$row->ProductsID.'_'.$row->ShipmentID]->Amount = $arr_data[$row->ProductsID.'_'.$row->ShipmentID]->Amount+$row->Amount;
						}else{
							$arr_data[$row->ProductsID.'_'.$row->ShipmentID] = $row;
						}
					}
				}

				$i=1;
				$total_quantity = 0;
				if(count($arr_data)>0){
					foreach($arr_data as $row){
						$row->BarcodeClient = $row->BarcodeClient!='' ? $row->BarcodeClient : '--' ;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Barcode</td>";
						echo "<td>$row->BarcodeClient</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td>$row->ShipmentCode</td>";
						echo "<td>".number_format($row->Amount)."</td>";
						echo "</tr>";
						$total_quantity = $total_quantity+$row->Amount;
						$i++;
					}
				}
				$k = $i<20 ? 20-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				?>

				<tr>
					<td colspan='7' style="text-align:right">TỔNG CỘNG</td>
					<td style="text-align:center"><?php echo number_format($total_quantity) ?></td>
				</tr>
			</table>
		</div>

		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
			</div>

			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
		</div>
	</div>
</div>
<script>

	$("#print_page").click(function(){
		window.print();
	});

	$(document).on('keydown', function(e) {
	    if(e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
	        e.cancelBubble = true;
	        e.preventDefault();
	        e.stopImmediatePropagation();
	    }
	});
</script>
