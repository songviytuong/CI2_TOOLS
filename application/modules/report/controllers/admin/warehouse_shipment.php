<?php 
class Warehouse_shipment extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_shipment";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string(trim($_GET['keywords'])) : '' ;
        $nav = $this->db->query("select count(1) as nav from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and (a.ShipmentCode like '%$keywords%' or b.MaSP like '%$keywords%' or b.Title like '%$keywords%')")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Title from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and (a.ShipmentCode like '%$keywords%' or b.MaSP like '%$keywords%' or b.Title like '%$keywords%') order by b.ID ASC $limit_str")->result();
        $data = array(
            'base_link' => base_url().ADMINPATH.'/report/warehouse_shipment/',
            'data'      => $object,
            'keywords'  => str_replace('+',' ',$keywords),
            'start'     => $start,
            'find'      => $nav,
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_shipment/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Shipment | Master data Report tools');
        $this->template->write_view('content','admin/warehouse_shipment_home',$data);
        $this->template->render();
	}

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Shipment add | Master data report tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_shipment/'
        );
        $this->template->write_view('content','admin/warehouse_shipment_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'ProductsID'    => $ProductsID,
                'ShipmentCode'  => $ShipmentCode,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'Created'       => date('Y-m-d H:i:s'),
                'UserID'        => $this->user->ID
            );
            $this->db->insert("ttp_report_shipment",$data);
        }
        redirect(ADMINPATH.'/report/warehouse_shipment/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_shipment where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_shipment where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Shipment | Master data report tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/warehouse_shipment/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/warehouse_shipment_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'ProductsID'    => $ProductsID,
                'ShipmentCode'  => $ShipmentCode,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'UserID'        => $this->user->ID
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_shipment",$data);
        }
        redirect(ADMINPATH.'/report/warehouse_shipment/');
    }
}
?>
