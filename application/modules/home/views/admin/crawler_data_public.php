<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="transfer_post">
                    <div class="x_content">
                        <div class="form-group">
                            <label class="col-md-5 col-sm-5 col-xs-12" style="text-align:right;margin-top: 7px;">Choose site</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <select id="Site" class="form-control">
                                    <option value="">-- No Select --</option>
                                    <option value="http://tools.ucancook.vn/postsite">UCANCOOK.VN</option>
                                    <?php 
                                    
                                    //for ($i=1; $i < 11; $i++) { 
                                        //echo '<option value="http://localhost/postsite/">Site0'.$i.'.com</option>';
                                    //}
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="x_title">
                                <h2>POST <small> / Select available post transfer</small></h2>
                                <select class="form-control" id="change_site">
                                    <option value="">-- All Site --</option>
                                    <?php 
                                    $site = $this->db->query("select * from ttp_crawler_site")->result();
                                    if(count($site)>0){
                                        foreach($site as $row){
                                            echo "<option value='$row->Site'>$row->Site</option>";
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="clearfix"></div>
                            </div>
                            <table id="crawl_data_post" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                    <tr class="headings">
                                        <th>STT</th>
                                        <th>Post </th>
                                        <th>Preview </th>
                                        <th class="no-link last" style="text-align:center"><input class="checkfull_option" type="checkbox" /></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    if(count($data)>0){
                                        $i=0;
                                        foreach($data as $row){ 
                                            $json = json_decode($row->Data,true);
                                            $i++;
                                    ?>
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <?php echo $i; ?>
                                                </td>
                                                <td>
                                                    <?php echo $json['Title']; ?><br>
                                                    <?php 
                                                        $site = explode("/",$row->Destination);
                                                        $type = isset($site[2]) ? $site[2] : '' ;
                                                        echo isset($json['Categories']) ? "<small>$type » ".str_replace(" / "," » ",$json['Categories'])."</small>" : "<small>$type</small>";
                                                    ?>
                                                </td>
                                                <td class="a-right a-right <?php echo "status".$row->ID ?>" style="text-align:center"><a class="crawl_preview" data="<?php echo $row->ID ?>"><i class="fa fa-search"></i></a></td>
                                                <td class="last" style="text-align:center"><input class="rowcheckbox <?php echo "row".$row->ID ?>" type="checkbox" value="<?php echo $row->ID ?>" /></td>
                                            </tr>
                                    <?php 
                                        }
                                        if(count($data)==4){
                                            echo '<tr class="even pointer"><td colspan="4" style="text-align:center"><a onclick="loadmore(this)" data="'.$i.'">Load more post</a></td></tr>';
                                        }
                                    }else{
                                        ?>
                                        <tr class="even pointer">
                                            <td class="a-center" colspan="4" style="text-align:center">No post available .</td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- end left -->
                        <div class="col-md-3 col-sm-3 col-xs-12 cat_block">
                            <div class="x_title">
                                <h2>Categories <small> / Add post to cat</small></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="cat_reciver"><p>Choose site to load categories</p></div>
                            <button class="btn btn-primary" id="transfer" style="margin-bottom:15px">Transfer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="crawl_preview_box">
    <div class="crawl_preview_containner">
        <div class="header_crawl_box">
            <h2>PREVIEW POST</h2>
            <a id="close_crawl_preview"><i class="fa fa-times"></i></a>
        </div>
        <div class="content_crawl_box">
            
        </div>
    </div>
</div>
<style type="text/css">
    a{cursor: pointer}
    .cat_block{background: #f6f6f6;border:1px solid #E1e1e1;box-sizing: border-box;}
    .cat_reciver{}
    .cat_reciver ul{padding-left:0px;}
    .cat_reciver ul li{margin:0px; padding:5px 0px;display: block}
    .cat_reciver ul li input{margin-right:5px;}
    .cat_reciver ul li ul{padding-left:20px;}
</style>
<script>
    $("#change_site").change(function(){
        var data = $(this).val();
        $.ajax({
            url: "<?php echo $base_link.'/load_post_available_from_site' ?>",
            dataType: "html",
            type: "POST",
            data: "Site="+data,
            beforeSend: function(){
                $("table tbody").html("<tr><td colspan='4'><p>Loading ...</p></td></tr>");
            },
            success: function(result){
                if(result!='false'){
                    $("table tbody").html(result);
                }else{
                    $("table tbody").html("<p>No post from this site .</p>");
                }
            }
        });
    });

    $(".checkfull_option").change(function(){
        if(this.checked===true){
            $(".rowcheckbox").prop('checked', true);
        }else{
            $(".rowcheckbox").prop('checked', false);
        }
    });

    $("#Site").change(function(){
        var data = $(this).val();
        if(data!=''){
            $.ajax({
                url: "<?php echo $base_link.'/get_categories_from_site' ?>",
                dataType: "html",
                type: "POST",
                data: "Site="+data,
                beforeSend: function(){
                    $(".cat_reciver").html("<p>Loading ...</p>");
                },
                success: function(result){
                    if(result!='false'){
                        $(".cat_reciver").html(result);
                    }else{
                        $(".cat_reciver").html("<p>No categories on this site .</p>");
                    }
                }
            });
        }else{
            $(".cat_reciver").html("<p>Choose site to load categories</p>");
        }
    });

    $(".crawl_preview").click(function(){
        var data = $(this).attr("data");
        if(data!=''){
            $.ajax({
                url: "<?php echo $base_link.'/get_crawl_post_by_id' ?>",
                dataType: "html",
                type: "POST",
                data: "ID="+data,
                beforeSend: function(){
                    $(".crawl_preview_box .content_crawl_box").html("<p>Loading ...</p>");
                },
                success: function(result){
                    if(result!='false'){
                        $(".crawl_preview_box .content_crawl_box").html(result);
                        $(".crawl_preview_box").show();
                    }else{
                        $(".crawl_preview_box .content_crawl_box").html("<p>This post is not available .</p>");
                    }
                }
            });
        }
    });

    $("#close_crawl_preview").click(function(){
        $(".crawl_preview_box").hide();
    });

    $("#transfer").click(function(){
        var site = $("#Site").val();
        var post = [];
        var cat = [];
        $(this).addClass("saving");
        $(".rowcheckbox").each(function() {
           if ($(this).is(":checked")) {
               post.push($(this).val());
           }
        });
        $(".cat_reciver input").each(function() {
           if ($(this).is(":checked")) {
               cat.push($(this).val());
           }
        });
        temppost = post.join(",");
        cat = cat.join(",");
        if(temppost!='' && cat!='' && site!=''){
            send_post(0,post,site,cat);
        }else{
            alert("Vui lòng chọn bài viết và danh mục để thực hiện transfer về site đích !");
            $("#transfer").html("Transfer");
            $("#transfer").removeClass("saving");
        }
    });

    function send_post(num,post,site,cat){
        if(post.length==num){
            $("#transfer").html("Transfer");
            $("#transfer").removeClass("saving");
            return false;
        }
        $(".status"+post[num]).html("Sending ...");
        $.ajax({
            url: "<?php echo $base_link.'/send_post' ?>",
            dataType: "html",
            type: "POST",
            data: "Site="+site+"&post="+post[num]+"&cat="+cat,
            beforeSend: function(){
                $("#transfer").html("Post "+post[num]+" sending ...");
            },
            success: function(result){
                $(".row"+post[num]).parent("td").parent("tr").fadeOut();
                $(".row"+post[num]).remove();
            }
        }).always(function(){
            send_post(++num,post,site,cat);
        });
    }

    function loadmore(ob){
        var site = $("#change_site").val();
        var stt = $(ob).attr('data');
        var parent = $(ob).parent("td").parent("tr");
        $.ajax({
            url: "<?php echo $base_link.'/load_more_post' ?>",
            dataType: "html",
            type: "POST",
            data: "Site="+site+"&stt="+stt,
            beforeSend: function(){
                $(ob).parent("td").html("<p>Loading ...</p>");
            },
            success: function(result){
                parent.remove();
                if(result!='false'){
                    $("table tbody").append(result);
                }else{
                    alert("No post");
                }
            }
        });
    }
</script>