<?php
class Training_user_model extends CI_Model
{
    private $_tableName = "ttp_training_user";
    public function getList($userid,$groupid="")
    {
        $sql = "SELECT * FROM ttp_training_user a,ttp_training b WHERE a.userid = $userid and a.courseid = b.id";
        if($groupid !=null){
            $sql .=" and b.groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getList2($userid,$kq="")
    {
        $sql = "SELECT * FROM ttp_training_user a,ttp_training b WHERE a.enabled = 1 and a.userid = $userid and a.courseid = b.id";
        if($kq !=null){
            $sql .=" and b.title like '%$kq%'";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getListAlluser($groupid="")
    {
        $sql = "SELECT *,a.id as idcourse FROM ttp_training_user a,ttp_training b WHERE a.enabled = 1 and a.courseid = b.id and b.enabled =1 ";
        if($groupid !=null){
            $sql .=" and b.groupid like '%$groupid%'";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
    public function getDetailByCourse($userid,$courseid)
    {
        $sql = "SELECT * FROM ttp_training_user a,ttp_training b WHERE a.userid = $userid and a.courseid = $courseid and a.courseid = b.id";
        $result = $this->db->query($sql)->row_array();
        return isset($result) ? $result : '';
        
    }
    public function updateStatusCourseID($userid,$courseid,$status,$enddate='')
    {
        $sql = "update $this->_tableName set status = $status ";
        if($enddate !=null){
            $sql .=",enddate = '$enddate' ";
        }
        $sql.= " WHERE userid = $userid and courseid = $courseid";
        $result = $this->db->query($sql);
        return isset($result[0]) ? $result[0] : '';
        
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] :'';
    }
    public function insert($data){
        return $this->db->insert($this->_tableName,$data);
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    public function excute($query){
        return $this->db->query($query);
    }
    public function getDetailByCourseUser($userid,$courseid)
    {
        $sql = "SELECT * FROM $this->_tableName WHERE userid = $userid and courseid = $courseid and enabled = 1";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : '';
        
    }
}?>