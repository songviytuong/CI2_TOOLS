<link href="public/pos/css/style.css" rel="stylesheet">
<div class="containner">
	<div class="sub_menu">
		<div class="list-open-bill pull-left">
			<?php 
			$files = scandir('log/pos/sales');
			$first = '';
			if(count($files)>0){
				$i=1;
				foreach($files as $row){
					if($row!='.' && $row!='..'){
						$name = explode('_',$row);
						$name = str_replace('.txt','',$name[2]);
						if($i==1){
							$first = $name;
						}
						$current = $i==1 ? 'current' : '' ;
						echo '<a class="btn control-bill-'.$name.' '.$current.'"><b onclick="sales_open_order('.$name.')">Đơn hàng '.$i.'</b> <span onclick="sales_closebill('.$name.')"><i class="fa fa-times"></i></span></a>';
						$i++;
					}
				}
				echo '<script>
						var stt = '.number_format($i-1).';
						var order = '.number_format($i-1).';
						var ordercurrent = '.$first.';
					</script>';
			}
			?>
		</div>
		<a onclick="sales_openbill(this)" class="btn"><b><i class="fa fa-plus"></i></b></a>
	</div>
	<?php 
	if($first!=''){
		$data = file_get_contents('log/pos/sales/draft_order_'.$first.'.txt');
		$data = json_decode($data,true);
	}
	$active = isset($data['customer']['Active']) && $data['customer']['Active']>0 ? 'hidden' : '' ;
	$inactive = isset($data['customer']['Active']) && $data['customer']['Active']==0 ? 'hidden' : '' ;
	?>
	<div class="block2">
		<div class="order_info">
			<?php 
			$GroupID = isset($data['customer']['GroupID']) ? $data['customer']['GroupID'] : '' ;
			$KenhbanhangID = isset($data['customer']['KenhbanhangID']) ? $data['customer']['KenhbanhangID'] : '' ;
			$SourceID = isset($data['customer']['SourceID']) ? $data['customer']['SourceID'] : '' ;
			$name = isset($data['customer']['Name']) ? $data['customer']['Name'] : '' ;
			$idcus = isset($data['customer']['id']) ? $data['customer']['id'] : 0 ;
			$phone = isset($data['customer']['Phone']) ? $data['customer']['Phone'] : '' ;
			$payment = isset($data['customer']['Payment']) ? $data['customer']['Payment'] : 0 ;
			$note = isset($data['customer']['Note']) ? $data['customer']['Note'] : '' ;
			$address = isset($data['customer']['Address']) ? $data['customer']['Address'] : '' ;
			$districtid = isset($data['customer']['district']) ? $data['customer']['district'] : 0 ;
			$transfer = isset($data['customer']['transfer']) ? $data['customer']['transfer'] : 0 ;
			$transport_type = isset($data['transport']) ? $data['transport'] : 0 ;
			?>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<h4>Thông tin giao hàng</h4>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 text-right">
					<?php 
					$href = isset($data['customer']['Active']) && $data['customer']['Active']>0 ? ADMINPATH.'/pos/pos_sales/print_order/'.$data['customer']['Active'] : '#' ;
					?>
					<a id="btn_createorder" onclick="sales_create_order(this)" class="btn btn-success hidden-xs <?php echo $active ?>"><i class="fa fa-check-square" aria-hidden="true"></i> Hoàn tất</a>
					<a href="<?php echo $href ?>" id="btn_printorder" target="_blank" class="btn btn-primary hidden-xs <?php echo $inactive ?> print_button"><i class="fa fa-print" aria-hidden="true"></i> In hóa đơn</a>
					<a onclick="sales_closebill_button(this)" id="btn_closeorder" class="btn btn-default hidden-xs <?php echo $inactive ?>"><i class="fa fa-times" aria-hidden="true"></i> Đóng đơn hàng</a>
				</div>
			</div>
			<hr style="margin: 5px 0px;">
			<div class="no_action <?php echo $inactive ?>""></div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Nguồn khách hàng</label>
						</div>
						<div class="col-xs-12">
							<select name="SourceID" id="SourceID" class="form-control">
								<?php
			    				$source = $this->db->query("select * from ttp_report_source where ID!=11")->result();
			    				if(count($source)>0){
			    					foreach ($source as $row) {
			    						$selected = $row->ID==$SourceID ? 'selected="selected"' : '' ;
			    						echo "<option value='$row->ID' $selected>$row->Title</option>";
			    					}
			    				}
			    				?>
			    			</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12"><label class="control-label">Kênh bán hàng</label></div>
						<div class="col-xs-12">
							<select name="KenhbanhangID" id="KenhbanhangID" class="form-control">
			    				<?php
			    				$kenhbanhang = $this->db->query("select * from ttp_report_saleschannel where ID!=7")->result();
			    				if(count($kenhbanhang)>0){
			    					foreach($kenhbanhang as $row){
			    						$selected = $row->ID==$KenhbanhangID ? 'selected="selected"' : '' ;
			    						echo "<option value='$row->ID' $selected>$row->Title</option>";
			    					}
			    				}
			    				?>
			    			</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12"><label class="control-label">Nhóm khách hàng</label></div>
						<div class="col-xs-12">
							<select name="GroupID" id="GroupID" class="form-control">
								<?php 
								$group = $this->db->query("select * from ttp_report_customer_group")->result();
								if(count($group)>0){
									foreach($group as $row){
										$selected = $row->ID==$GroupID ? 'selected="selected"' : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<label><b>Loại khách hàng :</b> </label> <div class="hidden-sm hidden-md hidden-lg"></div>
					<label for="label-1" style="margin-left:10px;margin-right:15px;"><input onclick="sales_change_customer_type(this)" type="radio" name="CustomerType" value="1" id="label-1" <?php echo $idcus==0 ? 'checked="checked"' : '' ; ?> /> Khách hàng mới</label>
					<label for="label-2"><input onclick="sales_change_customer_type(this)" type="radio" id="label-2" name="CustomerType" value="0" <?php echo $idcus>0 ? 'checked="checked"' : '' ; ?> /> Khách hàng cũ</label>
					<a class="btn-sm btn-info <?php echo $idcus>0 ? '' : 'hidden' ; ?>" id="btn-view-history-order" data-id="<?php echo $idcus ?>" style="margin-left:10px;" onclick="sales_view_history_order(this)">Xem lịch sử đặt hàng</a>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Tên khách hàng</label>
						</div>
						<div class="col-xs-12">
							<input type="text" class="form-control" name="Name" placeholder="Nhập tên khách hàng ..." id="sales_bill_CustomerName" autocomplete="off" onchange="sales_apply_name_bill(this)" value="<?php  echo $name ?>" />
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Số điện thoại</label>
						</div>
						<div class="col-xs-12">
							<input type="text" class="form-control" name="Phone" placeholder="Nhập sđt khách hàng ..." id="sales_bill_CustomerPhone" autocomplete="off" onchange="sales_apply_phone_bill(this)" value="<?php  echo $phone ?>" />
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Hình thức thanh toán</label>
						</div>
						<div class="col-xs-12">
							<select name="Payment" id="sales_bill_CustomerPayment" class="form-control">
								<option value="0">Tiền mặt</option>
								<option value="1">Chuyển khoản</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Hình thức nhận hàng</label>
						</div>
						<div class="col-xs-12">
							<select name="Payment" id="sales_bill_TransferType" onchange="sales_choose_transport(this)" class="form-control">
								<option value="0" <?php echo $transport_type==0 ? 'selected="selected"' : '' ; ?>>Nhận tại shop</option>
								<option value="1" <?php echo $transport_type==1 ? 'selected="selected"' : '' ; ?>>Giao tận nơi</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row <?php echo $transport_type==0 ? 'hidden' : '' ; ?>" id="shiping_info">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Tỉnh / Thành phố</label>
						</div>
						<div class="col-xs-12">
							<select name="Payment" id="sales_bill_CityID" class="form-control">
								<option value="30">Hồ Chí Minh</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Quận / Huyện</label>
						</div>
						<div class="col-xs-12">
							<select name="Payment" id="sales_bill_DictrictID" class="form-control" onchange="sales_apply_district_bill(this)">
								<?php 
								$district = $this->db->query("select * from ttp_report_district where CityID=30")->result();
								if(count($district)>0){
									foreach ($district as $key => $value) {
										$select = $value->ID==$districtid ? 'selected="selected"' : '' ;
										echo "<option value='$value->ID' $select>$value->Title</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Đơn vị vận chuyển</label>
						</div>
						<div class="col-xs-12">
							<select name="Payment" id="sales_bill_TransportID" class="form-control" onchange="sales_apply_transport_bill(this)">
								<?php 
								$transport = $this->db->query("select * from ttp_report_transport")->result();
								if(count($transport)>0){
									foreach ($transport as $key => $value) {
										echo "<option value='$value->ID'>$value->Title</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Nhân viên vận chuyển</label>
						</div>
						<div class="col-xs-12">
							<select name="Payment" id="sales_bill_TransferID" class="form-control" onchange="sales_apply_transfer_bill(this)">
								<?php 
								$user_transport = $this->db->query("select a.UserName,a.ID from ttp_user a,ttp_user_transport b where a.ID=b.UserID")->result();
								if(count($user_transport)>0){
									foreach ($user_transport as $key => $value) {
										$selected = $transfer==$value->ID ? 'selected="selected"' : '' ;
										echo "<option value='$value->ID' $selected>$value->UserName</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-12">
							<label class="control-label">Địa chỉ giao hàng</label>
						</div>
						<div class="col-xs-12">
							<textarea id="sales_bill_Address" onchange="sales_apply_address_bill(this)" class="form-control" rows="3" placeholder="Số nhà, tên đường, phường / xã"><?php echo $address ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-12">
					Ghi chú thêm
				</label>
				<div class="col-xs-12">
					<textarea name="Note" id="sales_bill_CustomerNote" rows="3" class="form-control" placeholder="Nhập ghi chú trên hóa đơn (nếu có) ..."><?php  echo $note ?></textarea>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<h4>Thông tin chi tiết giỏ hàng</h4>
		</div>
	</div>
	<div class="block1">
		<div class="data_table">
			<div class="tools">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="position:relative;">
						<input type="text" class="form-control" placeholder="Nhập tên sản phẩm cần tìm ..." onchange="sales_get_products_list(this)" id="keywords" autocomplete="off" />
						<div id="auto_complete" class='hidden auto_complete'></div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
						<div class="dropdown pull-left hidden-xs">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    <i class="fa fa-search" aria-hidden="true"></i> Tìm tự động
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						    <li><a onclick="sales_change_search_type(0)"><i class="fa fa-search" aria-hidden="true"></i> Tìm tự động</a></li>
						    <li><a onclick="sales_change_search_type(1)"><i class="fa fa-barcode" aria-hidden="true"></i> Quét Barcode</a></li>
						  </ul>
						</div>
						<div class="dropdown pull-left hidden-xs" style="margin-left:10px;">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-map-marker" aria-hidden="true"></i> Tại kho DOHATO
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
						    <?php 
						    $warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
						    if(count($warehouse)>0){
						    	foreach($warehouse as $row){
						    		if($row->ID==$this->WarehouseID){
						    			echo '<script>$("#dropdownMenu2").html("<i class=\'fa fa-map-marker\' aria-hidden=\'true\'></i> Tại kho '.$row->MaKho.' <span class=\'caret\'></span>")</script>';
						    		}
						    		echo "<li><a onclick='sales_change_branch(this,$row->ID)'>$row->MaKho</a></li>";
						    	}
						    }
						    ?>
						  </ul>
						</div>
					</div>
				</div>
			</div>
			<div class="no_action <?php echo $inactive ?>"></div>
		</div>
		<div class="row data_table">
			<div class="col-xs-12">
				<div id="table_content" style="position:relative">
					<table class="table">
						<tr>
							<th class="hidden-xs">HÌNH ẢNH</th>
							<th>TÊN SẢN PHẨM</th>
							<th>ĐƠN VỊ</th>
							<th>SỐ LƯỢNG</th>
							<th>ĐƠN GIÁ</th>
							<th>THÀNH TIỀN</th>
							<th class="text-center">HỦY</th>
						</tr>
						<?php 
						$total_order = 0;
						$total_products = 0;
						$total_reduce = 0;
						$total_chiphi = 0;
						if($first!=''){
							if(isset($data['reduce'])){
								$total_reduce = $data['reduce'];
							}
							if(isset($data['chiphi'])){
								$total_chiphi = $data['chiphi'];
							}
							if(isset($data['products']) && count($data['products'])>0){
								$key = array_keys($data['products']);
								$arr_inventory = array();
								$inventory = $this->db->query("select * from ttp_report_inventory where ProductsID in(".implode(',',$key).") and LastEdited=1 and WarehouseID=$this->WarehouseID")->result();
								if(count($inventory)>0){
									foreach($inventory as $row){
										if(isset($arr_inventory[$row->ProductsID])){
											$arr_inventory[$row->ProductsID] = $arr_inventory[$row->ProductsID]+$row->OnHand;
										}else{
											$arr_inventory[$row->ProductsID] = $row->OnHand;
										}
									}
								}
								$total_products = count($key);
								$products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
								if(count($products)>0){
									foreach ($products as $row) {
										$invent = isset($arr_inventory[$row->ID]) ? $arr_inventory[$row->ID] : 0 ;
										$amount = $data['products'][$row->ID];
										$price = date('Y-m-d')>=date('Y-m-d',strtotime($row->SpecialStartday)) && date('Y-m-d')<=date('Y-m-d',strtotime($row->SpecialStopday)) ? $row->SpecialPrice : $row->Price ;
										$diff = '';
										if(isset($data['price'][$row->ID])){
											if($data['price'][$row->ID]<$price){
												$diff = $price-$data['price'][$row->ID];
												$diff = '<span style="color:#999;display:block;margin-top:8px">Đã giảm : '.number_format($diff).'</span>';
											}
											$price = $data['price'][$row->ID];
										}
										$total = $amount*$price;
										$total_order += $total;
										echo '<tr>
										<td class="hidden-xs"><img src="'.$this->lib->get_thumb($row->PrimaryImage).'" /></td>
										<td style="width:350px">
											<p>'.$row->Title.'</p>
											<span style="color:#999">Mã sản phẩm : '.$row->MaSP.'</span>
										</td>
										<td>'.$row->Donvi.'</td>
										<td>
											<input type="number" value="'.$data['products'][$row->ID].'" style="width:100px" onchange="sales_change_products(this,'.$row->ID.')" />
											<span style="color:#999;display: block;margin-top:8px">Hiện còn : '.$invent.'</span>
										</td>
										<td><input type="number" value="'.$price.'" onchange="sales_change_price(this,'.$row->ID.')" />'.$diff.'
										</td>
										<td>'.number_format($total).'</td>
										<td class="text-center"><a onclick="sales_remove_products('.$row->ID.')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
									}
								}
							}else{
								echo "<tr><td colspan='8'>Vui lòng chọn sản phẩm</td></tr>"; 
							}
						}
						?>
						<tr>
							<th colspan="5" class="text-right">TỔNG SỐ SẢN PHẨM</th>
							<th colspan="3">: <?php echo number_format($total_products) ?></th>
						</tr>
						<tr>
							<th colspan="5" class="text-right">GIÁ TRỊ ĐƠN HÀNG</th>
							<th colspan="3">: <?php echo number_format($total_order) ?></th>
						</tr>
						<tr>
							<th colspan="5" class="text-right">GIẢM GIÁ TRÊN ĐƠN HÀNG</th>
							<th colspan="3">: <input name="Reduce" onchange="sales_add_reduce(this)" type="number" value="<?php echo $total_reduce ?>" /></th>
						</tr>
						<tr>
							<?php 
							$cal_fee = $total_order-$total_reduce;
							if($transport_type==1){
								$fee = $this->lib->getfee($districtid,$cal_fee);
								$temp = $total_chiphi>0 ? $total_chiphi : $fee ;
							}else{
								$temp = $total_chiphi;
							}
							?>
							<th colspan="5" class="text-right">CHI PHÍ GIAO HÀNG</th>
							<th colspan="3">: <input onchange="sales_add_chiphi(this)" type="number" id="feeship" value="<?php echo $temp; ?>" /></th>
						</tr>
						<tr style="font-size: 17px">
							<th colspan="5" class="text-right">TỔNG TIỀN PHẢI TRẢ</th>
							<th colspan="3">: <?php echo number_format($total_order-$total_reduce+$temp) ?>đ</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="row hidden-sm hidden-md hidden-lg" style="padding-bottom:20px;">
			<div class="col-xs-12">
				<a id="btn_createorder" onclick="sales_create_order(this)" class="btn btn-success <?php echo $active ?>"><i class="fa fa-check-square" aria-hidden="true"></i> Hoàn tất</a>
				<a href="<?php echo $href ?>" id="btn_printorder" target="_blank" class="btn btn-primary <?php echo $inactive ?> print_button"><i class="fa fa-print" aria-hidden="true"></i> In hóa đơn</a>
				<a onclick="sales_closebill_button(this)" id="btn_closeorder" class="btn btn-default <?php echo $inactive ?>"><i class="fa fa-times" aria-hidden="true"></i> Đóng đơn hàng</a>
			</div>
		</div>
	</div>
	<div class="fade_excuting hidden"></div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<script>
	var baseurl = "<?php echo base_url().ADMINPATH.'/pos/' ?>";
</script>

<script src="public/pos/js/scripts.js"></script>
<script>
	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});
</script>