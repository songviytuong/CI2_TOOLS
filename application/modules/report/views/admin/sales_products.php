<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$baselink = ADMINPATH.'/report/import_order/';
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <button class="btn btn-default" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
            <a class="btn btn-primary" role="button"><i class="fa fa-download"></i> Export to Excel</a>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="filltertools">
        <form action="<?php echo $base_link."setfillter" ?>" method="post">
            <?php 
                $arr_fieldname = array(0=>"c.Name",1=>"a.CustomerType",2=>"e.ID",3=>"d.ID",4=>"b.ID",5=>"a.SoluongSP",6=>"a.Total",7=>"a.Chiphi",8=>"a.Status",9=>"a.UserID",10=>"a.TransportID",11=>"c.Phone1",12=>"a.MaDH",15=>"i.MaSP",16=>"i.Title",17=>"i.CategoriesID",18=>"i.TradeMarkID");
                $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                $arr_showfieldname = array(0=>"Tên khách hàng",1=>"Loại khách hàng",2=>"Khu vực",3=>"Tỉnh thành",4=>"Quận huyện",5=>"Số lượng sản phẩm",6=>"Tổng tiền hàng",7=>"Chi phí vận chuyển",8=>"Trạng thái đơn hàng",9=>"Nhân viên khởi tạo",10=>"Đối tác vận chuyển",11=>"Số điện thoại",12=>"Mã đơn hàng",15=>"Mã SKU",16=>"Tên sản phẩm",17=>"Nhóm ngành hàng",18=>"Thương hiệu",19=>"Nhà cung cấp");
                $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                $fill_data_arr = explode(" and ",$fill_data);
                if(count($fill_data_arr)>0 && $fill_data!=''){
                    $temp_tools=0;
                    foreach($fill_data_arr as $row){
                        $param = explode(' ',$row,3);
                        $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                        $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                        $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                        $param_value = isset($param[2]) ? $param[2] : '' ;
                        $param_value = str_replace("\'","",$param_value);
                        $param_value = str_replace("'","",$param_value);
                        $param_value = str_replace("%","",$param_value);
                        $param_value = str_replace('"',"",$param_value);
            ?> 
            <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                <div class="list_toolls col-xs-2">
                    <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                    <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                    <ul class="dropdownbox">
                        
                        <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                            <ul>
                                <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                <li><a onclick="setfield(this,11,'sodienthoai')">Số điện thoại</a></li>
                            </ul>
                        </li>
                        <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                            <ul>
                                <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                            </ul>
                        </li>
                        <li><span onclick="showul(this)"><b class="caret"></b> Sản phẩm</span>
                            <ul>
                                <li><a onclick="setfield(this,15,'sku')">Mã SKU</a></li>
                                <li><a onclick="setfield(this,16,'tensanpham')">Tên sản phẩm</a></li>
                                <li><a onclick="setfield(this,17,'nhomnganhhang')">Nhóm ngành hàng</a></li>
                                <li><a onclick="setfield(this,18,'thuonghieu')">Thương hiệu</a></li>
                                <!--<li><a onclick="setfield(this,17,'nhacungcap')">Nhà cung cấp</a></li>-->
                            </ul>
                        </li>
                        <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                            <ul>
                                <li><a onclick="setfield(this,12,'madh')">Mã đơn hàng</a></li>
                                <li><a onclick="setfield(this,9,'user')">Nhân viên khởi tạo</a></li>
                                <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="list_toolls reciveroparation col-xs-2">
                    <select class="oparation form-control" name="FieldOparation[]">
                        <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                        <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                        <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                        <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                        <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                        <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                        <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                    </select>
                </div>
                <div class="list_toolls reciverfillter col-xs-3">
                    <?php 
                    if($value_field==2 || $value_field==3 || $value_field==4){
                        if($value_field==2){
                            $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();    
                        }
                        if($value_field==3){
                            $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();    
                        }
                        if($value_field==4){
                            $result = $this->db->query("select * from ttp_report_district order by Title ASC")->result();    
                        }
                        if(count($result)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($result as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==8) {
                        echo "<select name='FieldText[]' class=' form-control'>";
                        echo $param_value==10 ? "<option value='10' selected='selected'>Đơn hàng chờ hủy</option>" : "<option value='10'>Đơn hàng chờ hủy</option>" ;
                        echo $param_value==9 ? "<option value='9' selected='selected'>Đơn hàng chuyển sang nv điều phối</option>" : "<option value='9'>Đơn hàng chuyển sang nv điều phối</option>" ;
                        echo $param_value==8 ? "<option value='8' selected='selected'>Đơn hàng bị trả về</option>" : "<option value='8'>Đơn hàng bị trả về</option>" ;
                        echo $param_value==7 ? "<option value='7' selected='selected'>Chuyển sang bộ phận giao hàng</option>" : "<option value='7'>Chuyển sang bộ phận giao hàng</option>" ;
                        echo $param_value==6 ? "<option value='6' selected='selected'>Đơn hàng bị trả về từ kế toán</option>" : "<option value='6'>Đơn hàng bị trả về từ kế toán</option>" ;
                        echo $param_value==5 ? "<option value='5' selected='selected'>Đơn hàng chờ kế toán duyệt</option>" : "<option value='5'>Đơn hàng chờ kế toán duyệt</option>" ;
                        echo $param_value==4 ? "<option value='4' selected='selected'>Đơn hàng bị trả về từ kho</option>" : "<option value='4'>Đơn hàng bị trả về từ kho</option>" ;
                        echo $param_value==3 ? "<option value='3' selected='selected'>Đơn hàng mới chờ kho duyệt</option>" : "<option value='3'>Đơn hàng mới chờ kho duyệt</option>" ;
                        echo $param_value==2 ? "<option value='2' selected='selected'>Đơn hàng nháp</option>" : "<option value='2'>Đơn hàng nháp</option>" ;
                        echo $param_value==0 ? "<option value='0' selected='selected'>Đơn hàng thành công</option>" : "<option value='0'>Đơn hàng thành công</option>" ;
                        echo $param_value==1 ? "<option value='1' selected='selected'>Đơn hàng hủy</option>" : "<option value='1'>Đơn hàng hủy</option>" ;
                        $fill_temp_value = $param_value==10 ? "Đơn hàng chờ hủy" : $fill_temp_value ;
                        $fill_temp_value = $param_value==9 ? "Đơn hàng chuyển sang nv điều phối" : $fill_temp_value ;
                        $fill_temp_value = $param_value==8 ? "Đơn hàng bị trả về" : $fill_temp_value ;
                        $fill_temp_value = $param_value==7 ? "Chuyển sang bộ phận giao hàng" : $fill_temp_value ;
                        $fill_temp_value = $param_value==6 ? "Đơn hàng bị trả về từ kế toán" : $fill_temp_value ;
                        $fill_temp_value = $param_value==5 ? "Đơn hàng chờ kế toán duyệt" : $fill_temp_value ;
                        $fill_temp_value = $param_value==4 ? "Đơn hàng bị trả về từ kho" : $fill_temp_value ;
                        $fill_temp_value = $param_value==3 ? "Đơn hàng mới chờ kho duyệt" : $fill_temp_value ;
                        $fill_temp_value = $param_value==2 ? "Đơn hàng nháp" : $fill_temp_value ;
                        $fill_temp_value = $param_value==1 ? "Đơn hàng hủy" : $fill_temp_value ;
                        $fill_temp_value = $param_value==0 ? "Đơn hàng thành công" : $fill_temp_value ;
                        echo "</select>";
                    }elseif ($value_field==9) {
                        $userlist = $this->db->query("select ID,UserName from ttp_user where UserType=1")->result();
                        if(count($userlist)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($userlist as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->UserName : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->UserName</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==17) {
                        $categories = $this->db->query("select ID,Title from ttp_report_categories where IsLast=1")->result();
                        if(count($categories)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($categories as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==18) {
                        $categories = $this->db->query("select ID,Title from ttp_report_trademark")->result();
                        if(count($categories)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($categories as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==1) {
                        echo "<select name='FieldText[]' class=' form-control'>
                                <option value='0'>Khách hàng mới</option>
                                <option value='1'>Khách hàng cũ</option>
                            </select>";
                        $fill_temp_value = $param_value==1 ? "Khách hàng cũ" : $fill_temp_value ;
                        $fill_temp_value = $param_value==0 ? "Khách hàng mới" : $fill_temp_value ;
                    }elseif ($value_field==10) {
                        echo "<select name='FieldText[]' class=' form-control'>";
                        $transport = $this->db->query("select * from ttp_report_transport")->result();
                        foreach($transport as $row){
                            $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                            $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                        }
                        echo "</select>";
                    }else{
                        $fill_temp_value = $param_value ;
                        echo '<input type="text" name="FieldText[]" class="form-control" id="textsearch" value="'.$param_value.'" />';
                    }
                    ?>
                </div>
                <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
            </div>
            <?php   
                    $temp_tools++;
                    $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                    $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                    }
                }else{
            ?>
                <div class="row base_row">
                    <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                    <div class="list_toolls col-xs-2">
                        <input type="hidden" class="FieldName" name="FieldName[]" />
                        <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                        <ul class="dropdownbox">
                            
                            <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                    <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                    <li><a onclick="setfield(this,11,'sodienthoai')">Số điện thoại</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                <ul>
                                    <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                    <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                    <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Sản phẩm</span>
                                <ul>
                                    <li><a onclick="setfield(this,15,'sku')">Mã SKU</a></li>
                                    <li><a onclick="setfield(this,16,'tensanpham')">Tên sản phẩm</a></li>
                                    <li><a onclick="setfield(this,17,'nhomnganhhang')">Nhóm ngành hàng</a></li>
                                    <li><a onclick="setfield(this,18,'thuonghieu')">Thương hiệu</a></li>
                                    <!--<li><a onclick="setfield(this,16,'nhacungcap')">Nhà cung cấp</a></li>-->
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,12,'madh')">Mã đơn hàng</a></li>
                                    <li><a onclick="setfield(this,9,'user')">Nhân viên khởi tạo</a></li>
                                    <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                    <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                    <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                    <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                    <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1">Bằng</option>
                            <option value="0">Có chứa</option>
                            <option value="2">Khác</option>
                            <option value="3">Lớn hơn</option>
                            <option value="4">Nhỏ hơn</option>
                            <option value="5">Lớn hơn hoặc bằng</option>
                            <option value="6">Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-3">
                        <input type="text" class=" form-control" name="FieldText[]" id="textsearch" />
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
            <?php
                }
            ?>
            <div class="add_box_data"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-11">
                            <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                            <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="quick_view row">
        <div class="col-xs-12 col-sm-6 col-md-5">
            <canvas id="canvas_pie"></canvas>
            <div class='current_view_chart'>Biểu đồ : <b>Doanh số theo ngành hàng</b></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-7">
            <div class="row">
                <?php 
                $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
                ?>
                <div class="col-xs-12 col-md-6"><input type='checkbox' value="0" <?php echo $current_view==0 ? 'checked="checked"' : "" ; ?> onclick="change_viewtype(0)" /> Theo ngày đặt hàng</div>
                <div class="col-xs-12 col-md-6"><input type='checkbox' value="1" <?php echo $current_view==1 ? 'checked="checked"' : "" ; ?> onclick="change_viewtype(1)" /> Theo ngày cập nhật trạng thái</div>
                <div class="col-xs-12">
                    <?php 
                    $order_type = isset($_GET['order_type']) ? (int)$_GET['order_type'] : 0 ;
                    ?>
                </div>
                <input type="hidden" id="value_view_type" value="<?php echo $current_view ?>">
                <input type="hidden" id="value_order_type" value="<?php echo $order_type ?>">
            </div>
            <h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Theo ngành hàng</h3>
            <?php 
            $arr = array();
            $arr_total = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
            $categories = $this->db->query("select ID,Title,ParentID from ttp_report_categories where IsLast=1")->result();
            if(count($categories)>0){
            	foreach($categories as $row){
            		$arr[$row->ID] = array('Title'=>$row->Title,'Total'=>0,'ParentID'=>$row->ParentID,'SLDH'=>0,'SLSP'=>0);
            	}
            }
            $arr_brand_title = array();
            $arr_brand = array();
            $brand = $this->db->query("select ID,Title from ttp_report_trademark")->result();
            if(count($brand)>0){
            	foreach($brand as $row){
            		$arr_brand_title[$row->ID] = $row->Title;
            	}
            }
            $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B");
            if(count($data)>0){
                foreach($data as $row){
                	$json_categories = json_decode($row->CategoriesID,true);
                	$json_categories = is_array($json_categories) ? $json_categories : array() ;
                	if(count($json_categories)>0){
	                    $arr_total['SLDH'] = $arr_total['SLDH']+1;
	                    $arr_total['SLSP'] = $arr_total['SLSP']+$row->SoluongSP;
	                    $arr_total['Total'] = $arr_total['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
	                    foreach ($json_categories as $value) {
	                    	if(isset($arr[$value])){
		                        $arr[$value]['SLDH'] = $arr[$value]['SLDH']+1;
		                        $arr[$value]['SLSP'] = $arr[$value]['SLSP']+$row->SoluongSP;
		                        $arr[$value]['Total'] = $arr[$value]['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
		                    }
	                    }
                    }
                    if(isset($arr_brand[$row->TrademarkID])){
                    	$arr_brand[$row->TrademarkID]['SLDH'] = $arr_brand[$row->TrademarkID]['SLDH']+1;
                        $arr_brand[$row->TrademarkID]['SLSP'] = $arr_brand[$row->TrademarkID]['SLSP']+$row->SoluongSP;
                        $arr_brand[$row->TrademarkID]['Total'] = $arr_brand[$row->TrademarkID]['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                    }else{
                    	$arr_brand[$row->TrademarkID]['SLDH'] = 1;
                        $arr_brand[$row->TrademarkID]['SLSP'] = $row->SoluongSP;
                        $arr_brand[$row->TrademarkID]['Total'] = $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                        $arr_brand[$row->TrademarkID]['Title'] = isset($arr_brand_title[$row->TrademarkID]) ? $arr_brand_title[$row->TrademarkID] : '--' ;
                    }
                }
            }
            $arr_chart = array();
            if(count($arr)>0){
                echo "<table class='table'>
                        <tr>
                            <th>Tên ngành hàng</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                $i = 0;
                foreach($arr as $key=>$value){
                	if($value['SLDH']>0 && $value['SLSP']>0){
	                    $percent = $arr_total['Total']==0 ? 0 : round($value['Total']/($arr_total['Total']/100),1);
	                    echo "<tr>";
	                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$i]."'></span>".$value['Title']."</td>";
	                    echo "<td>".number_format($value['SLDH'])."</td>";
	                    echo "<td>".number_format($value['SLSP'])."</td>";
	                    echo "<td>".number_format($value['Total'])."</td>";
	                    echo "<td>".$percent."%</td>";
	                    echo "</tr>";
	                    $arr_chart[] = "{
	                                        value: ".$percent.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: '".$value['Title']."'
	                                    }";
	                    $i++;
                    }
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }
            ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5">
            <div style="margin-bottom:50px"></div>
            <canvas id="canvas_pie1"></canvas>
            <div class='current_view_chart'>Biểu đồ : <b>Doanh số theo thương hiệu</b></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-7">
            <h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Theo thương hiệu</h3>
            <?php 

            $arr_chart1 = array();
            if(count($arr_brand)>0){
                echo "<table class='table'>
                        <tr>
                            <th>Tên thương hiệu</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                $i = 0;
                foreach($arr_brand as $key=>$value){
                	if($value['SLDH']>0 && $value['SLSP']>0){
	                    $percent = $arr_total['Total']==0 ? 0 : round($value['Total']/($arr_total['Total']/100),1);
	                    echo "<tr>";
	                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$i]."'></span>".$value['Title']."</td>";
	                    echo "<td>".number_format($value['SLDH'])."</td>";
	                    echo "<td>".number_format($value['SLSP'])."</td>";
	                    echo "<td>".number_format($value['Total'])."</td>";
	                    echo "<td>".$percent."%</td>";
	                    echo "</tr>";
	                    $value['Title'] = str_replace("'",'',$value['Title']);
	                    $arr_chart1[] = "{
	                                        value: ".$percent.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: 'Thương hiệu ".$value['Title']."'
	                                    }";
	                    $i++;
                    }
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }
            ?>
        </div>
        <div class="col-xs-12">
            <div class="table_data">
        	<h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Doanh thu - lợi nhuận</h3>
        	<?php 
        	$arr_products = array();
        	$arr_area = array();
        	if(count($data_prodducts)>0){
        		foreach($data_prodducts as $row){
        			if(!isset($arr_products[$row->ProductsID])){
        				$categories_products = json_decode($row->CategoriesID,true);
        				$categories_products = is_array($categories_products) ? $categories_products : array() ;
        				$str = array();
        				if(count($categories_products)>0){
        					foreach($categories_products as $item){
        						if(isset($arr[$item]['Title'])){
        							$str[] = $arr[$item]['Title'];
        						}
        					}
        				}
        				$arr_products[$row->ProductsID] = array(
        					'Title'=>$row->Title,
        					'Order'=>array($row->OrderID),
        					'SLSP'=>$row->Amount,
        					'Total'=>$row->Total,
        					'RootPrice'=>$row->RootPrice*$row->Amount,
        					'Price'=>$row->Price*$row->Amount,
        					'Brand'=>$row->Brand,
        					'Categories'=>implode(',',$str),
        					'Area' =>array(
        						$row->AreaID => array(
        							'SLSP'	=> $row->Amount,
        							'Total' => $row->Total
        						)
        					)
        				);
        			}else{
        				$arr_products[$row->ProductsID]['Order'][] = $row->OrderID;
        				$arr_products[$row->ProductsID]['Order'] = array_unique($arr_products[$row->ProductsID]['Order']);
        				$arr_products[$row->ProductsID]['SLSP'] = $arr_products[$row->ProductsID]['SLSP']+$row->Amount;
        				$arr_products[$row->ProductsID]['Total'] = $arr_products[$row->ProductsID]['Total']+$row->Total;
        				$arr_products[$row->ProductsID]['RootPrice'] = $arr_products[$row->ProductsID]['RootPrice']+($row->RootPrice*$row->Amount);
        				$arr_products[$row->ProductsID]['Price'] = $arr_products[$row->ProductsID]['Price']+($row->Price*$row->Amount);
        				if(isset($arr_products[$row->ProductsID]['Area'][$row->AreaID])){
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['SLSP'] += $row->Amount;
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['Total'] += $row->Total;
        				}else{
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['SLSP'] = $row->Amount;
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['Total'] = $row->Total;
        				}
        			}
        			if(isset($arr_area[$row->AreaID])){
    					$arr_area[$row->AreaID]['SLSP'] = $arr_area[$row->AreaID]['SLSP']+$row->Amount;
    					$arr_area[$row->AreaID]['Total'] = $arr_area[$row->AreaID]['Total']+$row->Total;
    				}else{
    					$arr_area[$row->AreaID] = array(
							'SLSP'	=> $row->Amount,
							'Total' => $row->Total
						);
    				}
        		}
        	}
        	echo "<table class='table'><tr>
        		<th>STT</th>
				<th>Ngành hàng</th>
				<th>Tên sản phẩm</th>
				<th>Brand</th>
				<th>SL ĐH</th>
				<th>SL SP</th>
				<th>Tổng doanh số</th>
				<th>Đơn giá TB</th>
				<th>Giá vốn</th>
				<th>Lợi nhuận</th>
				<th>% LN</th>
        	</tr>";
        	$total_arr_products = array('Order'=>0,'SLSP'=>0,'Total'=>0,'Price'=>0,'RootPrice'=>0);
        	if(count($arr_products)>0){
        		$i = 1;
        		foreach($arr_products as $key=>$row){
        			$row['Title'] = str_replace("\'","'",$row['Title']);
        			$row['Brand'] = str_replace("\'","'",$row['Brand']);
        			echo "<tr>";
        			echo "<td class='text-center'>$i</td>";
        			echo "<td class='width150'>".$row['Categories']."</td>";
        			echo "<td><a onclick='show_order_by_products($key)'>".$row['Title']."</a></td>";
        			echo "<td>".$row['Brand']."</td>";
        			echo "<td class='width50'>".count($row['Order'])."</td>";
        			echo "<td class='width50'>".number_format($row['SLSP'])."</td>";
        			echo "<td class='width100'>".number_format($row['Total'])."</td>";
        			echo $row['SLSP']==0 ? "<td>0</td>" : "<td>".number_format($row['Price']/$row['SLSP'])."</td>";
        			echo "<td class='width100'>".number_format($row['RootPrice'])."</td>";
        			echo "<td class='width100'>".number_format($row['Total']-$row['RootPrice'])."</td>";
        			echo $row['Total']==0 ? "<td>0.00 %</td>" : "<td class='width100'>".number_format((($row['Total']-$row['RootPrice'])/$row['Total'])*100,2)." %</td>";
        			echo "</tr>";
        			$total_arr_products['SLSP'] = $total_arr_products['SLSP'] + $row['SLSP'];
        			$total_arr_products['Total'] = $total_arr_products['Total'] + $row['Total'];
        			$total_arr_products['Price'] = $total_arr_products['Price'] + $row['Price'];
        			$total_arr_products['RootPrice'] = $total_arr_products['RootPrice'] + $row['RootPrice'];
        			$i++;
        		}
        	}
        	echo "<tr>";
			echo "<th colspan='5'>Tổng cộng</th>";
			echo "<th class='width50'>".number_format($total_arr_products['SLSP'])."</th>";
			echo "<th class='width100'>".number_format($total_arr_products['Total'])."</th>";
			echo $total_arr_products['SLSP']==0 ? "<th>0</th>" : "<th>".number_format($total_arr_products['Price']/$total_arr_products['SLSP'])."</th>";
			echo "<th class='width100'>".number_format($total_arr_products['RootPrice'])."</th>";
			echo "<th class='width100'>".number_format($total_arr_products['Total']-$total_arr_products['RootPrice'])."</th>";
			echo $total_arr_products['Total']==0 ? "<th>0.00 %</th>" : "<th class='width100'>".number_format((($total_arr_products['Total']-$total_arr_products['RootPrice'])/$total_arr_products['Total'])*100,2)." %</th>";
			echo "</tr>";
        	echo "</table>";
        	?>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="table_data">
        	<h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Doanh thu theo khu vực</h3>
        	<?php 
        	echo "<table><tr><th colspan='4'></th>";
        	$area = $this->db->query("select * from ttp_report_area where Title !='Undefine'")->result();
        	if(count($area)>0){
				foreach($area as $row){
					echo "<th colspan='2' class='text-center'>$row->Title</th>";
				}
			}
        	echo "</tr><tr><th>STT</th><th>Ngành hàng</th><th>Tên sản phẩm</th><th>Brand</th>";
			if(count($area)>0){
				foreach($area as $row){
					echo "<th>SL</th>";
					echo "<th>Doanh số</th>";
				}
			}
        	echo "</tr>";
        	if(count($arr_products)>0){
        		$i = 1;
        		foreach($arr_products as $row){
        			$row['Title'] = str_replace("\'","'",$row['Title']);
        			$row['Brand'] = str_replace("\'","'",$row['Brand']);
        			echo "<tr>";
        			echo "<td class='text-center'>$i</td>";
        			echo "<td class='width100'>".$row['Categories']."</td>";
        			echo "<td>".$row['Title']."</td>";
        			echo "<td>".$row['Brand']."</td>";
        			if(count($area)>0){
						foreach($area as $item){
							echo isset($row['Area'][$item->ID]['SLSP']) ? "<td>".number_format($row['Area'][$item->ID]['SLSP'])."</td>" : "<td>0</td>";
							echo isset($row['Area'][$item->ID]['Total']) ? "<td>".number_format($row['Area'][$item->ID]['Total'])."</td>" : "<td>0</td>";
						}
					}
        			$i++;
        		}
        	}
        	echo "<tr>";
			echo "<th colspan='4'>Tổng cộng</th>";
			$area = $this->db->query("select * from ttp_report_area where Title !='Undefine'")->result();
        	if(count($area)>0){
				foreach($area as $row){
					echo isset($arr_area[$row->ID]['SLSP']) ? "<th>".number_format($arr_area[$row->ID]['SLSP'])."</th>" : "<th>0</th>";
					echo isset($arr_area[$row->ID]['Total']) ? "<th>".number_format($arr_area[$row->ID]['Total'])."</th>" : "<th>0</th>";
				}
			}
			echo "</tr>";
        	echo "</table>";
        	?>
            </div>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var sharePiePolorDoughnutData = [
        <?php 
        echo implode(',', $arr_chart);
        ?>
    ];

    var sharePiePolorDoughnutData1 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];
    
    $(document).ready(function () {
        window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_pie1").getContext("2d")).Pie(sharePiePolorDoughnutData1, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });

    function change_viewtype(type){
        var value_order_type = $("#value_order_type").val();
        window.location="<?php echo current_url().'?view_type=' ?>"+type+"&order_type="+value_order_type;
    }

    function changetype(ob){
        var value_view_type = $("#value_view_type").val();
        var typeorder = $(ob).val();
        window.location="<?php echo current_url().'?order_type=' ?>"+typeorder+"&view_type="+value_view_type;
    }

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"import_order/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="vung" || fieldname=="tinhthanh" || fieldname=="quanhuyen" || fieldname=='loaikhachhang' || fieldname=='trangthaidonhang' || fieldname=='doitacvanchuyen' || fieldname=='user' || fieldname=='nhomnganhhang' || fieldname=='thuonghieu'){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="tenkhachhang" || fieldname=="sodienthoai" || fieldname=="madh"){
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="0">Có chứa</option><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    function show_order_by_products(products){
        var baselink = $("#baselink_report").val();
        $("#bs-example-modal-lg").modal("show");
        $("#bs-example-modal-lg .modal-body").load(baselink+"report/load_order_by_products/"+products);
    }   
</script>
<div class="modal fade" id="bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Danh sách đơn hàng</h4>
        </div>
        <div class="modal-body">
            ...
        </div>
    </div>
  </div>
</div>
<style>
    .version{display:none;}
    .copyright{display:none;}
    .modal .table tr{font-size: 12px;}
    .modal .table tr th,.modal .table tr td{white-space: nowrap;max-width: 200px;text-overflow:ellipsis;overflow:hidden;}
</style>