<div class="containner">
	<div class="row">
		<div class="form-group">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable">
					<h4><?php echo $data->name ?> !</h4>
					<p><?php echo $data->description ?></p>
					<a class="btn btn-success" style="margin-top:10px" href="<?php echo base_url().ADMINPATH.'/report/report_sales/export_search_case_1' ?>"><i class="fa fa-download"></i> Export excel</a>
				</div>
			</div>
		</div>
	</div>
	<?php 
	$arr_new = array();
	$arr_old = array();
	
	if(count($result1)>0){
		foreach($result1 as $row){
			$arr_new[$row->ID] = 1;
		}
	}

	$start = $page==0 ? 0 : $page*50 ;

	$i=1;
	if(count($result2)>0){
		foreach($result2 as $row){
			if(!isset($arr_new[$row->ID]) && $row->Total<=5000000){
				if($i>=$start && $i<=($start+50)){
					$arr_old[] = $row;
				}
				$i++;
			}
		}
	}
	$record = $i-1;
	$numpage = ($i-1)/50;

	?>
	<div class="row">
		<div class="form-group">
			<div class="col-xs-12">
				<table class="table table-hover">
					<tr>
						<th>STT</th>
						<th>Tên khách hàng</th>
						<th style='width:120px'>Số điện thoại</th>
						<th>Địa chỉ</th>
						<th style='width:120px'>Giá trị đơn hàng</th>
					</tr>
					<?php 
					if(count($arr_old)>0){
						$i=$start+1;
						foreach($arr_old as $row){
							echo "<tr>
									<td>$i</td>
									<td>$row->Name</td>
									<td>$row->Phone1</td>
									<td>$row->Address</td>
									<td class='text-right'>".number_format($row->Total)."</td>
								</tr>";
							$i++;
						}
					}
					?>
				</table>
				<nav>
					<?php
					$temp = round($numpage) ;
					if($temp>1){
						echo "<ul class='pagination'>";
						$numpage = round($numpage);
						echo ($page-5)>1 ? "<li><a href='$base_link/0'>« First</a></li>" : "" ;
						for ($i=0; $i < $temp; $i++) { 
							if($i>=($page-5) && $i<=($page+5)){
								$j = $i+1;
								$active = $i==$page ? "active" : "" ;
								echo "<li class='$active'><a href='$base_link/$i'>$j</a></li>";
							}
						}
						$numpage = $numpage-1;
						echo ($page+5)<($numpage) ? "<li><a href='$base_link/$numpage'>Last »</a></li>" : "" ;
						echo "</ul>";
					}
					?>
				</nav>
				<h5>Tìm thấy <b><?php echo number_format($record) ?></b> khách hàng phù hợp điều kiện.</h5>
			</div>
		</div>
	</div>
</div>
<style>
	.row{margin:0px;}
	.row .form-group .table tr th{border-top:none;}
</style>