<?php

class Site_tags extends Admin_Controller {

    public $user;
    public $classname = "site_tags";
    public $limit = 15;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
//        $query = "SELECT Alias FROM ttp_pagelinks WHERE Classname='pages'";
//        $query = "SELECT Alias FROM ttp_report_products WHERE Title !='' and Alias !='' and Published = 1 and Status = 1";
        $query = "SELECT Alias FROM ttp_post WHERE Published = 1";
        $result = $this->db->query($query)->result();
        $html= "<div>";
        foreach ($result as $row) {
            $html .= "<url>
  <loc>https://auchan.vn/cam-nang-auchan/$row->Alias/</loc>
  <priority>0.75</priority>
</url>";
        }
        $html .= "</div>";
        echo $html;
        echo "<pre>";
        var_dump($result);
        exit();

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('Tags list | Config Site');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_tags")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_tags order by Created DESC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_tags/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_tags/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/site_tags_home', $data);
        $this->template->render();
    }

    public function search() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        if (isset($_GET['keyword'])) {
            $keyword = $this->lib->fill_data($_GET['keyword']);
            $str = "select * from ttp_tags";
            $str = $keyword != '' ? $str . " where Title like '%" . urldecode($keyword) . "%'" : $str;
            $this->template->add_title('Tìm kiếm dữ liệu');
            $data = array(
                'data' => $this->db->query($str)->result(),
                'nav' => '',
                'start' => 0,
                'base_link' => base_url() . ADMINPATH . '/home/site_tags/',
            );
            $this->template->write_view('content', 'admin/site_tags_home', $data);
            $this->template->render();
            return;
        }
        redirect(ADMINPATH . '/home/site_tags/');
    }

    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $this->db->query("delete from ttp_tags where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add Tags | Config Site');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_tags/'
        );
        $this->template->write_view('content', 'admin/site_tags_add', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title;
        $Link = $this->lib->alias($Alias);
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $check = $this->db->query("select * from ttp_tags where Alias='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_categories where Alias='$Link' or Alias_en='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_post where Alias='$Link' or Alias_en='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_pagelinks where Alias='$Link' or Alias='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        if ($Title != '') {
            $data = array(
                'Published' => $Published,
                'Title' => $Title,
                'Alias' => $Link,
                'Created' => date("Y-m-d H:i:s"),
                'LastEdited' => date("Y-m-d H:i:s")
            );
            $arr = array();
            foreach ($_POST as $key => $value) {
                if (!array_key_exists($key, $data)) {
                    $key = trim($key);
                    $key = strtolower($key);
                    $arr[$key] = $value;
                }
            }
            $arr = json_encode($arr);
            $data['Data'] = $arr;
            $this->db->insert("ttp_tags", $data);
        }
        redirect(ADMINPATH . '/home/site_tags/');
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_tags where ID=$id")->row();
            if (!$result)
                return;
            $this->template->add_title('Add Page | Config Site');
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_tags/',
                'data' => $result
            );
            $this->template->write_view('content', 'admin/site_tags_edit', $data);
            $this->template->render();
        }
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title;
        $Link = $this->lib->alias($Alias);
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $check = $this->db->query("select * from ttp_tags where Alias='$Link' and ID!=$ID")->row();
        if ($check) {
            $Link = $Link . "-" . $ID;
        }
        $check = $this->db->query("select * from ttp_categories where Alias='$Link' or Alias_en='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_post where Alias='$Link' or Alias_en='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_pagelinks where Alias='$Link' or Alias='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        if ($Title != '') {
            $data = array(
                'Published' => $Published,
                'Title' => $Title,
                'Alias' => $Link,
                'LastEdited' => date("Y-m-d H:i:s")
            );
            $arr = array();
            foreach ($_POST as $key => $value) {
                if (!array_key_exists($key, $data) && $key != "ID") {
                    $key = trim($key);
                    $key = strtolower($key);
                    $arr[$key] = $value;
                }
            }
            $arr = json_encode($arr);
            $data['Data'] = $arr;
            $this->db->where("ID", $ID);
            $this->db->update("ttp_tags", $data);
        }
        redirect(ADMINPATH . '/home/site_tags/');
    }

}

?>