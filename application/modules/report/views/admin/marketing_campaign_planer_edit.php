<!--Edit Planer-->
<?php
    $disabled = "";
    $readonly = "";
    $readonly_tt = "";
    $updatePlaner = "";
    $readonly_1 = "";
    if($market_status_active == 0){
        $updatePlaner = "updatePlaner";
        $readonly_tt = "readonly";
    }else if($market_status_active == 1){
        $readonly = "readonly";
        $updatePlaner = "updatePlaner";
        $readonly_1 = "readonly";
    }else if($market_status_active == 2){
        $readonly = "readonly";
        $readonly_1 = "readonly";
        $readonly_tt = "readonly";
        $disabled = "disabled";
    }else if($market_status_active == 3){
        $updatePlaner = "updatePlaner";
        $readonly_1 = "readonly";
    }else if($market_status_active == 4){
        $updatePlaner = "updatePlaner";
        $readonly_1 = "readonly";        
    }else if($market_status_active == 5){
        $disabled = "disabled";
        $readonly = "readonly";
        $readonly_1 = "readonly"; 
    }else if($market_status_active == 6){
        $readonly = "readonly";
        $updatePlaner = "updatePlaner";
        $readonly_1 = "readonly";
    }else if($market_status_active == 7){
        
    }
?>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
    .body_content .containner .black .box_inner .block1_inner h1{
        font-size:13px;
    }
    .daterangepicker{width: auto;}
</style>
<div class="alert alert-success alert-dismissable showMessenger hidden">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <p class="Messenger"></p>
</div>
<form name="frmPlanerEdit" id="frmPlanerEdit" method="post">
<div class="table_data">
    <table id="table_data">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-left col-xs-2" colspan="3">Nội dung kế hoạch:</th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center" colspan="3"><textarea id="planer_name" name="planer_name" placeholder="Nhập mô tả kế hoạch..." class="form-control" <?=$readonly;?>><?=isset($data->name) ? $data->name : '';?></textarea></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-2"></th>
            <th class="text-center">
                <div class="form-group text-left">
                    <span style="font-size:11px;">Kế hoạch bắt đầu:</span>
                    <input type='text' name="planer_start" id='planer_start' value="<?=isset($data->start) ? $data->start : '';?>" class="form-control" <?=$readonly;?>/>
                </div>
            </th>
            <th class="text-left col-xs-4">
                <span style="font-size:11px;">Thực tế bắt đầu:</span>
                <input type='text' name="action_start" id='action_start' placeholder="<?=date('Y-m-01 h:s:i',time());?>" value="<?=isset($data->action_start) ? $data->action_start : '';?>" class="form-control" <?=$readonly_tt;?>/>
            </th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-2"></th>
            <th class="text-center">
                <div class="form-group text-left">
                    <span style="font-size:11px;">Kế hoạch kết thúc:</span>
                    <input type='text' name="planer_end" id='planer_end' value="<?=isset($data->end) ? $data->end : '';?>" class="form-control" <?=$readonly;?>/>
                </div>
            </th>
            <th class="text-left">
                <span style="font-size:11px;">Thực tế kết thúc:</span>
                    <input type='text' name="action_end" id='action_end' placeholder="<?=date('Y-m-d h:s:i',time());?>"  value="<?=isset($data->action_end) ? $data->action_end : '';?>" class="form-control" <?=$readonly_tt;?>/>
            </th>
        </tr>
        <?php
            $parentData = json_decode($dataPar->value);
            $symbolData = json_decode($data->symbol);
            $valueData = json_decode($data->value);
            $moneyData = $dataPar->money_need;
            $dataTitle = json_decode($dataPar->action_title);
            $dataAction = json_decode($data->action);
            $last = count($valueData);
            for($i=0;$i<$last;$i++){
                foreach($result_symbol as $sym){
                    if($sym->code == $symbolData[$i]){
                        $configSymbol = $sym->name;
                    }
                }
        ?>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-left col-xs-3"><span style="font-size:11px;">Mục tiêu:</span><input type="text" class="form-control" value="<?=$dataTitle[$i];?>" disabled=""/></th>
            <th class="text-left">
                <span style="font-size:11px;">Mục tiêu/ kế hoạch (<?=$configSymbol;?>):<input type="hidden" id="symbol" name="symbol[]" value="<?=$symbolData[$i];?>"/></span>
                <div class='input-group'>
                <input type="text" id="planer_focus" onkeyup="keyUp(this);" name="planer_focus[]" value="<?=$valueData[$i];?>" placeholder="0" class="form-control" <?=$readonly_1;?>/>
                <span class="input-group-addon" style="min-width:100px;">/ <?=number_format($parentData[$i]);?></span>
                </div>
            </th>
            <th class="text-left"><span style="font-size:11px;">Thực tế:</span><input type="text" name="planer_action[]" value="<?=isset($dataAction[$i]) ? $dataAction[$i] : 0 ;?>" class="form-control" placeholder="0" onkeyup="keyUp(this);" <?=$readonly_tt;?>/></th>
        </tr>
        <?php } ?>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-2"></th>
            <th class="text-left">
                <span style="font-size:11px;">Chi phí/ Ngân sách (VNĐ):</span>
                <div class='input-group'>
                    <input type="text" id="planer_money" name="planer_money" value="<?=$money;?>" placeholder="Ngân sách thực hiện" class="form-control" <?=$readonly;?> <?=$readonly_1;?>/>
                    <span class="input-group-addon" style="min-width:100px;">/ <?=number_format($moneyData);?></span>
                </div>
            </th>
            <th class="text-left"><span style="font-size:11px;">Thực tế:</span><input type="text" name="money_action" value="<?=($data->money_action) ? $data->money_action : 0;?>" class="form-control" placeholder="0" onkeyup="keyUp(this);" <?=$readonly_tt;?>/></th>
        </tr>
        <tr>
            <th colspan="2" class="updated"></th>
            <th class="text-center">
                <?php
                    if(isset($edit) == true){
                ?>
                <a class="btn btn-success updatePlaner"><i class="fa fa-save"></i> Thêm mới</a>
                <?php } else { ?>
                <a class="btn btn-success form-control <?=$updatePlaner;?> <?=$disabled;?>"><i class="fa fa-save"></i> Cập nhật</a>
                <?php } ?>
            </th>
        </tr>
    </table>
    <input type="hidden" name="planer_id" id="planer_id" value="<?=isset($data->id) ? $data->id : 0;?>"/>
    <?php
            if(isset($edit) == true){
        ?>
    <input type="hidden" name="parent" id="parent" value="<?=$parent;?>"/>
    <input type='hidden' name="market_id" id="market_id" value="<?=$market_id;?>" />
            <?php } ?>
    <input type="hidden" name="money" id="money" value=""/>
</div>
</form>
<script type="text/javascript">
function keyUp(ob){
    var val = $(ob).val();
    val = val.replace(/[^0-9]/g,'');
    $(ob).val(format(val));
}

    var format = function(num){
       var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
       if(str.indexOf(".") > 0) {
           parts = str.split(".");
           str = parts[0];
       }
       str = str.split("").reverse();
       for(var j = 0, len = str.length; j < len; j++) {
           if(str[j] != ",") {
               output.push(str[j]);
               if(i%3 == 0 && j < (len - 1)) {
                   output.push(",");
               }
               i++;
           }
       }
       formatted = output.reverse().join("");
       return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
   };
   
   CKEDITOR.replace('planer_name', {
    toolbar: [
        {name : 'clipboard', items: ['Undo','Redo','Bold','Italic','Underline','Heading']},
        {name: 'paragraph', items: ['Undo'] ,groups: [ 'Undo', 'list', 'indent', 'blocks', 'align', 'bidi' ],  groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-','Cut','Copy','Paste','PasteText','PasteFromWord','-'] },
    ],
    height: 100
});

    $(document).ready(function () {
        $('#planer_start,#planer_end,#action_start,#action_end').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1",
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        setTimeout(function(){
//            var $inputs = $('#planer_focus :input');
//            $inputs.each(function (index){
//                alert(index + ': ' + $(this).attr('id'));
//            });
            //$("#planer_focus").keyup();
            $("#planer_money").keyup();
        });
    });

    $('.updatePlaner').click(function(){
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
    
        $(this).html("<i class='fa fa-refresh fa-spin'></i> Đang cập nhật");
        $(this).addClass("disabled",true);
        
        var sUrl="";
        sUrl = baselink+"marketing_campaign/updatePlaner";
        setTimeout(function(){
        $.ajax({
            url: sUrl,
            dataType: "JSON",
            type: "POST",
            data: $('#frmPlanerEdit').serialize(),
            success: function(result){
                $('.updatePlaner').html("<i class='fa fa-save'></i> Cập nhật</a>");
                if(result.status == "OK"){
                    $('.Messenger').text("Cập nhật thành công !");
                    $('.showMessenger').removeClass("hidden");
                    $('.updatePlaner').removeClass("disabled");
                    $('.updated').html("<i class='fa fa-calendar-o'></i> Cập nhật lúc: " + result.now);
                }else{
                    alert('Lỗi hệ thống !!!');
                }
                setTimeout(function(){
                    $('.showMessenger').addClass("hidden");
                },2000);
            }
        });
        },1000);
    });
    
    $("#close_overlay").click(function(){
        location.reload();
    });

    $("#planer_money").keyup(function(e){
    var val = $(this).val();
        val = val.replace(/[^0-9]/g,'');
        $(this).val(format(val));
        $('#money').val(val);
    });
    
    $("#planer_focus").keyup(function(e){
    var val = $(this).val();
        val = val.replace(/[^0-9]/g,'');
        $(this).val(format(val));
        $('#focus').val(val);
    });
</script>