<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>BẢNG TỒN KHO SẢN PHẨM</h1>
	    </div>
	    <div class="block2">
            <div class="row">
                <div class="col-xs-4">
                    <?php 
                    $type = isset($_GET['type']) ? $_GET['type'] : 1 ;
                    ?>
                    <select class="form-control" onchange="changetype(this)">
                        <option value="-1">-- Tất cả loại sp --</option>
                        <?php
                            $productype = $this->lib->get_config_define("products","productstype",1,"code");
                            foreach ($productype as $row) {
                                $selected = $type==$row->code ? 'selected="selected"' : '' ;
                                echo "<option value='$row->code' $selected>$row->name</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <select class="form-control" onchange="viewshipment(this)">
                        <option value="0">-- Tất cả lô --</option>
                        <?php 
                        $warehouse = $this->db->query("select DISTINCT ShipmentCode from ttp_report_shipment")->result();
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                echo "<option value='$row->ShipmentCode'>-- $row->ShipmentCode --</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-4">
                    <select class="form-control" onchange="viewwarehouse(this)">
                        <option value="0">-- Tất cả kho --</option>
                        <?php 
                        $warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                echo "<option value='$row->ID'>-- $row->MaKho --</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <?php 
    $arr_pending = array();
    $pending_out = $this->db->query("select a.* from ttp_report_order_send_supplier a,ttp_report_order b where a.Status=2 and a.WarehouseID>0 and a.OrderID=b.ID and b.Status!=1 and b.Status!=0")->result();
    if(count($pending_out)>0){
        foreach($pending_out as $row){
            if(isset($arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID])){
                $arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID] += $row->Amount;
            }else{
                $arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID] = $row->Amount;
            }
        }
    }
    ?>
    <div class="import_orderlist">
    	<div class="block3 table_data">
    		<table>
    			<tr>
    				<th>STT</th>
    				<th>Mã Sản phẩm</th>
    				<th>Tên sản phẩm</th>
    				<th style="width: 100px;">Mã kho</th>
    				<th>Mã lô</th>
                    <th style="width:110px">Ngày hết hạn</th>
    				<th>Available</th>
    				<th>OnHand</th>
                    <th>Giá trị tồn</th>
    			</tr>
    		<?php 
    		if(count($Data)>0){
    			$i = 1;
                $total = 0;
    			foreach($Data as $row){
                    $pending_out_row = isset($arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID]) ? $arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID] : 0 ;
    				echo "<tr class='rowtr row_$row->WarehouseID shipment_$row->ShipmentCode'>";
    				echo "<td>$i</td>";
    				echo "<td>$row->MaSP</td>";
    				echo "<td>$row->Title</td>";
    				echo "<td>$row->MaKho</td>";
    				echo "<td>$row->ShipmentCode</td>";
                    echo "<td>".date('d/m/Y',strtotime($row->DateExpiration))."</td>";
    				echo "<td style='text-align:right'>".number_format($row->Available,2)."</td>";
    				echo "<td style='text-align:right'>".number_format($row->OnHand-$pending_out_row,2)."</td>";
                    echo "<td style='text-align:right'>".number_format(($row->OnHand-$pending_out_row)*$row->RootPrice)."</td>";
    				echo "</tr>";
    				$i++;
                    $total = $total+(($row->OnHand-$pending_out_row)*$row->RootPrice);
    			}
                echo "<tr><td colspan='8' style='text-right:right'>TỔNG CỘNG</td><td>".number_format($total)."</td></tr>";
    		}else{
                echo "<tr><td colspan='9'>Không có dữ liệu tình trạng tồn kho của sản phẩm .</td></tr>";
            }
    		?>
    		</table>
    	</div>
	</div>
</div>
<style>
	table tr td:first-child{width:30px;text-align: center;background: #f7f7f7}
</style>
<script>
    var warehouse = 0;
    var shipment = 0;

    function viewwarehouse(ob){
        warehouse = $(ob).val();
        $("tr.rowtr").hide();
        if(warehouse==0){
            if(shipment==0){
                $("tr.rowtr").show();
            }else{
                $("tr.shipment_"+shipment).show();
            }
        }else{
            if(shipment==0){
                $("tr.row_"+warehouse).show();
            }else{
                $("tr.row_"+warehouse+".shipment_"+shipment).show();
            }
        }
    }

    function viewshipment(ob){
        shipment = $(ob).val();
        $("tr.rowtr").hide();
        if(shipment==0){
            if(warehouse==0){
                $("tr.rowtr").show();
            }else{
                $("tr.row_"+warehouse).show();
            }
        }else{
            if(warehouse==0){
                $("tr.shipment_"+shipment).show();
            }else{
                $("tr.row_"+warehouse+".shipment_"+shipment).show();
            }
        }
    }

    function changetype(ob){
        var data = $(ob).val();
        window.location = "<?php echo base_url().ADMINPATH."/report/warehouse/inventory?type=" ?>"+data;
    }
</script>
