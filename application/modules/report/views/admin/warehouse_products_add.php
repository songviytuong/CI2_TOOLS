<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner opensitebar">
	<div class="products_containner">
		<form action="<?php echo base_url().ADMINPATH."/report/warehouse_products/addnew" ?>" method="POST" id='products_form'>
			<div class="head_products">
				<div class="block1">
					<h1>THÔNG TIN SẢN PHẨM</h1>
				</div>
				<div class="block2">
					<span>Loại sản phẩm </span>
					<select name="VariantType" id='VariantType'>
						<option value="0">Sản phẩm đơn</option>
						<option value="1">Sản phẩm biến thể</option>
						<option value="2">Sản phẩm Bundle</option>
						<option value="3">Sản phẩm nhóm</option>
					</select>
				</div>
				<div class="block3">
					<a class="btn btn-danger" onclick="saveall(this)">Lưu & đóng</a>
					<button class="btn btn-primary" type="submit">Lưu & tiếp tục</button>
				</div>
			</div>
			<div class="content_products">
				<div class="control_tab">
					<a data="tab1" class="current">Thông tin chung</a>
					<a data="tab2">Giá bán</a>
					<a data="tab3">Website</a>
					<a data="tab4">Hình ảnh</a>
					<a data="tab11">Video</a>
					<a data="tab5">Tồn kho</a>
					<a data="tab6">Ngành hàng</a>
					<a data="tab7">Thuộc tính</a>
					<a data="tab8" style='display:none'>Biến thể</a>
					<a data="tab9">Dịch vụ quà tặng</a>
					<a data="tab10" style='display:none'>Gom nhóm</a>
				</div>
				<!-- Thông tin chung -->
				<div class="content_tab content_tab1">
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Mã sản phẩm
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="MaSP" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Loại hàng
								</label>
								<div class="col-xs-8">
									<?php 
									$productype = $this->lib->get_config_define("products","productstype",1,"code");
									?>
									<select name="TypeProducts" class="form-control">
										<?php 
										foreach ($productype as $row) {
											echo "<option value='$row->code'>$row->name</option>";
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">
									Tên sản phẩm
								</label>
								<div class="col-xs-10">
									<input type="text" class="form-control special_input" name="Title" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">
									Tên viết tắt
								</label>
								<div class="col-xs-10">
									<input type="text" class="form-control special_input" name="ShortName" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">
									Mô tả ngắn
								</label>
								<div class="col-xs-10">
									<input type="text" class="form-control special_input" name="Description" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Trọng lượng / Dung lượng
								</label>
								<div class="col-xs-3">
									<input type="text" class="form-control" name="Weight" />
								</div>
                                                            <label class="col-xs-1 control-label">
									
								</label>
                                                            <div class="col-xs-4">
                                                                <div class="input-group">
                                                                    <select name="Capacity" class="form-control">
                                                                        <?php
                                                                        $unit_capacity = $this->db->query("select * from ttp_report_unit_capacity")->result();
                                                                        if(count($unit_capacity)>0){
                                                                                foreach($unit_capacity as $row){
                                                                                        $selected = $data->Capacity==$row->Title ? "selected='selected'" : '' ;
                                                                                        echo "<option value='$row->Title' $selected>$row->Title</option>";
                                                                                }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                                        <button class="btn btn-default" type="button" onclick="add_capacity_unit(this)"><i class="fa fa-plus"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label text-center">
									Kích thước (cm)
								</label>
								<div class="col-xs-2">
									<div class="input-group">
										<input type="text" class="form-control" name="Length" placeholder="Dài" />
									</div>
								</div>
								<div class="col-xs-2">
									<div class="input-group">
										<input type="text" class="form-control" name="Width" placeholder="Rộng" />
									</div>
								</div>
								<div class="col-xs-2">
									<div class="input-group">
										<input type="text" class="form-control" name="Height" placeholder="Cao" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Là SP mới từ ngày
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="Startday" id="Input_Startday" placeholder="yyyy-mm-dd" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Là SP mới đến ngày
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="Stopday" id="Input_Stopday" placeholder="yyyy-mm-dd" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Tình trạng
								</label>
								<div class="col-xs-8">
									<select name="Published" class="form-control"><option value="1">Kích hoạt</option><option value="0">Ngưng kích hoạt</option></select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Cho phép hiển thị
								</label>
								<div class="col-xs-8">
									<select name="Show" class="form-control"><option value="0">No</option><option value="1">Yes</option></select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Nước sản xuất
								</label>
								<div class="col-xs-8">
									<select name="CountryID" class="form-control">
										<?php 
										$country = $this->db->query("select * from ttp_report_country")->result();
										if(count($country)>0){
											foreach($country as $row){
												echo "<option value='$row->ID'>$row->Title</option>";
											}
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Thương hiệu
								</label>
								<div class="col-xs-8">
									<select name="TrademarkID" class="form-control">
									<?php 
										$trademark = $this->db->query("select * from ttp_report_trademark")->result();
										if(count($trademark)>0){
											foreach($trademark as $row){
												echo "<option value='$row->ID'>$row->Title</option>";
											}
										}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Đơn vị tính
								</label>
								<div class="col-xs-8">
									<div class="input-group">
										<select name="Donvi" class="form-control" id="Donvi">
											<?php 
											$unit = $this->db->query("select * from ttp_report_unit")->result();
											if(count($unit)>0){
												foreach($unit as $row){
													echo "<option value='$row->Title'>$row->Title</option>";
												}
											}
											?>
										</select>
						                <span class="input-group-btn">
						                    <button class="btn btn-default" type="button" onclick="add_unit(this)"><i class="fa fa-plus"></i></button>
						                </span>
						            </div>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Sản phẩm chim mồi
								</label>
								<div class="col-xs-8">
									<select name="Chimmoi" class="form-control">
										<option value='0'>No</option>
										<option value='1'>Yes</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Sức chứa (nếu SP là thùng)
								</label>
								<div class="col-xs-8">
									<input type="number" class="form-control" name="Tankage" value="0" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Sắp xếp thứ tự site
								</label>
								<div class="col-xs-2">
									<input type="number" class="form-control" name="Orders" value="99999" />
								</div>
								<label class="col-xs-4 control-label text-center">
									Sắp xếp thứ tự cate
								</label>
								<div class="col-xs-2">
									<input type="number" class="form-control" name="OrdersCat" value="99999" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Nhà cung cấp
								</label>
								<div class="col-xs-8">
									<select name="SupplierID" class="form-control">
										<?php 
										$supplier = $this->db->query("select * from ttp_report_production")->result();
										if(count($supplier)>0){
											foreach($supplier as $row){
												echo "<option value='$row->ID'>$row->Title</option>";
											}
										}
										?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Barcode nhà cung cấp
								</label>
								<div class="col-xs-8">
									<input type="text" class="form-control" name="BarcodeClient" placeholder="Nhập mã Barcode từ nhà cung cấp ..." />
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Giá bán -->
				<div class="content_tab content_tab2">
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá bán</label>
								<div class="col-xs-8">
									<input type="text" name="Price" class="form-control" value="0" required />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá mua</label>
								<div class="col-xs-8">
									<input type="text" name="RootPrice" class="form-control" value="0" required/>
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Thuế VAT</label>
								<div class="col-xs-8">
									<select name="VAT" class="form-control">
										<option value="0">Không có</option>
										<option value="5">5%</option>
										<option value="10">10%</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá đặc biệt</label>
								<div class="col-xs-8">
									<input type="text" name="SpecialPrice" class="form-control" />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Từ ngày</label>
								<div class="col-xs-8">
									<input type="text" name="Special_startday" class="form-control" id="Input_Special_startday" placeholder="yyyy-mm-dd" />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Đến ngày</label>
								<div class="col-xs-8">
									<input type="text" name="Special_stopday" class="form-control" id="Input_Special_stopday" placeholder="yyyy-mm-dd" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá đ/vị chuẩn</label>
								<div class="col-xs-8">
									<input type="text" name="BasePrice" class="form-control" placeholder="ví dụ : 200,000 / kg" />
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá thị trường</label>
								<div class="col-xs-8">
									<input type="number" name="SocialPrice" class="form-control" placeholder="" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Nhóm giá : </label>
								<label class="col-xs-8 control-label">Nhóm khách hàng</label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá bán</label>
							</div>
						</div>
					</div>
					<div class="row" id="PriceGroup">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceGroup_Title[]" /></label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label"><input type="text" class="form-control" name="PriceGroup_Price[]" /></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<a class='btn btn-default' onclick="add_PriceGroup()"><i class="fa fa-plus"></i> Thêm nhóm giá</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label">Bước giá : </label>
								<label class="col-xs-8 control-label">Nhóm khách hàng</label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label">Giá bán</label>
								<label class="col-xs-1 control-label"></label>
								<label class="col-xs-2 control-label">Số lượng</label>
							</div>
						</div>
					</div>
					<div class="row" id="PriceRange">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceRange_Title[]" /></label>
							</div>
						</div>
						<div class="col-xs-8">
							<div class="form-group">
								<label class="col-xs-4 control-label"><input type="text" name="PriceRange_Price[]" class="form-control" /></label>
								<label class="col-xs-1 control-label"></label>
								<label class="col-xs-1 control-label"><input type="number" min="0" class="form-control" name="PriceRange_Amount[]" value="0" /></label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">
								<label class="col-xs-4 control-label"></label>
								<a class='btn btn-default' onclick="add_PriceRange()"><i class="fa fa-plus"></i> Thêm bước giá</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Thẻ Meta -->
				<div class="content_tab content_tab3">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Sản phẩm tiêu biểu</label>
								<div class="col-xs-2">
									<select class="form-control" name="Featured">
										<option value="0">Enable</option>
										<option value="1">Disable</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Meta Title</label>
								<div class="col-xs-10">
									<input type="text" name="MetaTitle" class="form-control" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Meta Keywords</label>
								<div class="col-xs-10">
									<input type="text" name="MetaKeywords" class="form-control" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Meta Description</label>
								<div class="col-xs-10">
									<input type="text" name="MetaDescription" class="form-control" />
								</div>
							</div>
						</div>
					</div>
					<div class="row hidden">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Hướng dẫn sử dụng sản phẩm</label>
							</div>
						</div>
					</div>
					<div class="row hidden">
						<div class="col-xs-12">
							<textarea class="resizable_textarea form-control ckeditor" name="Instruction"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label class="col-xs-2 control-label">Nội dung mô tả sản phẩm</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<textarea class="resizable_textarea form-control ckeditor" name="Content"></textarea>
						</div>
					</div>
				</div>
				<!-- Hình ảnh -->
				<div class="content_tab content_tab4">
					<div class='message_warning'>
						<i class="fa fa-exclamation-triangle"></i> Vui lòng lưu thông tin sản phẩm trước khi chọn hình ảnh để tránh lưu dữ liệu rác .
					</div>
				</div>
				<!-- Tồn kho -->
				<div class="content_tab content_tab5">
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Số lượng hiện tại
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLCurrent" class="form-control" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									SL báo hết hàng
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLOutOfStock" class="form-control" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									SL tối thiểu trong giỏ hàng
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLMinInCart" class="form-control" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									SL tối đa trong giỏ hàng
								</label>
								<div class="col-xs-8">
									<input type="number" min="0" name="SLMaxInCart" class="form-control" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Cho phép đặt hàng âm
								</label>
								<div class="col-xs-8">
									<select name="AddcartNegativeNumber" class="form-control">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label class="col-xs-4 control-label">
									Tình trạng tồn kho
								</label>
								<div class="col-xs-8">
									<select name="Available" class="form-control">
										<option value="1">Còn hàng</option>
										<option value="0">Hết hàng</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Nhón ngành hàng -->
				<div class="content_tab content_tab6">
							<?php 
							$arr_catgories = array();
							$arr_catgories_name = array();
							$categories = $this->db->query("select Title,ID from ttp_report_categories where ParentID=0")->result();
							if(count($categories)>0){
								echo "<ul>";
								foreach($categories as $row){
									echo "<li><span onclick='show_child_next(this)' data='$row->ID' class='label label-default'>+</span><input type='checkbox' value='$row->ID' name='CategoriesID[]' /> $row->Title</li>";
								}
								echo "</ul>";
							}
							?>
					
				</div>
				<!-- Thuộc tính -->
				<div class="content_tab content_tab7">
					<div class="row">
						<div class="col-xs-12">
							<h4>Danh sách thuộc tính</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-5">
							<div class="input-group">
								<a class="input-group-addon" onclick="select_properties()"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> Chọn thuộc tính</a>
								<select id="PropertiesID" class="form-control">
									<?php 
									$properties = $this->db->query("select ID,Title from ttp_report_properties where ParentID=0")->result();
									$arr_name_parent = array();
									if(count($properties) > 0){
										foreach($properties as $row){
											echo "<option value='$row->ID'>$row->Title</option>";
											$arr_name_parent[$row->ID] = $row->Title;
										}
									}
									?>
								</select>
								<span class="input-group-btn">
									<a class="btn btn-default" onclick="senddata('add_properties','Thêm thuộc tính mới','')"><i class="fa fa-plus"></i> Tạo mới</a>
								</span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-4"><h4>Tên thuộc tính</h4></div>
						<div class="col-xs-8"><h4>Giá trị thuộc tính</h4></div>
					</div>
					<div class="message_empty">Sản phẩm này chưa có thuộc tính . Vui lòng chọn thuộc tính cho sản phẩm này .</div>
				</div>
				<!-- Biến thể -->
				<div class="content_tab content_tab8">
					<div class='message_warning'>
						<i class="fa fa-exclamation-triangle"></i> Vui lòng lưu thông tin sản phẩm trước khi chọn hình ảnh để tránh lưu dữ liệu rác .
					</div>
				</div>
				<!-- Dịch vụ quà tặng -->
				<div class="content_tab content_tab9">
					<div>
						<div>
							<input type='checkbox' name="AcceptGift" /> Cho phép gói quà 
						</div>
						<div>
							<p>Chi phí gói quà</p>
							<input type='text' name="SaleGift" />
						</div>
					</div>
				</div>
				<!-- Gom nhóm sản phẩm bundel -->
				<div class="content_tab content_tab10">
					<div class='message_warning'>
						<i class="fa fa-exclamation-triangle"></i> Vui lòng lưu thông tin sản phẩm trước khi gom nhóm sản phẩm để tránh lưu dữ liệu rác .
					</div>
				</div>
				<!-- Video -->
				<div class="content_tab content_tab11">
					<div class='message_warning'>
						<i class="fa fa-exclamation-triangle"></i> Vui lòng lưu thông tin sản phẩm trước khi chọn video để tránh lưu dữ liệu rác .
					</div>
				</div>
			</div>
			<input type='hidden' name="currenttab" id="currenttab" value="<?php echo isset($_GET['tab']) ? $_GET['tab'] : '' ; ?>" />
		</form>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#Input_Startday,#Input_Stopday,#Input_Special_startday,#Input_Special_stopday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	$(".control_tab a").click(function(){
		$(".control_tab a").removeClass("current");
		var data = $(this).attr('data');
		$(this).addClass("current");
		$(".content_tab").hide();
		$(".content_"+data).show();
		$("#currenttab").val(data);
	});

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#VariantType").change(function(){
		var value = $(this).val();
		if(value==1){
			$(".control_tab a:nth-child(8)").show();
			$(".control_tab a:nth-child(10)").hide();
		}
		if(value==2){
			$(".control_tab a:nth-child(8)").hide();
			$(".control_tab a:nth-child(10)").show();
		}
		if(value!=2 && value!=1){
			$(".control_tab a:nth-child(8)").hide();
			$(".control_tab a:nth-child(10)").hide();
		}
	});

	function warning_variant(){
		alert("Vui lòng lưu sản phẩm và đảm bảo các thuộc tính phải có trước khi khởi tạo biến thể cho sản phẩm!");
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function close_PriceGroup(ob){
		$(ob).parent('div').parent('div').remove();
	}

	function add_PriceGroup(){
		$("#PriceGroup").append('<div class="col-xs-4"><div class="form-group"><label class="col-xs-4 control-label"></label><label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceGroup_Title[]" /></label></div></div><div class="col-xs-8"><div class="form-group"><label class="col-xs-4 control-label"><input type="text" class="form-control" name="PriceGroup_Price[]" /></label></div></div></div>');
	}

	function close_PriceRange(ob){
		$(ob).parent('div').parent('div').remove();
	}

	function add_PriceRange(){
		$("#PriceRange").append('<div class="col-xs-4"><div class="form-group"><label class="col-xs-4 control-label"></label><label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceRange_Title[]" /></label></div></div><div class="col-xs-8"><div class="form-group"><label class="col-xs-4 control-label"><input type="text" name="PriceRange_Price[]" class="form-control" /></label><label class="col-xs-1 control-label"></label><label class="col-xs-1 control-label"><input type="number" min="0" class="form-control" name="PriceRange_Amount[]" value="0" /></label></div></div>');
	}

	function check_full(ob){
		if(ob.checked===true){
            $(ob).parent('li').find("input").prop('checked', true);
        }else{
            $(ob).parent('li').find("input").prop('checked', false);
        }
	}

	function check_full_rowtab(ob,key){
		$("input.valuekey"+key).prop('checked', true);
	}

	function uncheck_full_rowtab(ob,key){
		$("input.valuekey"+key).prop('checked', false);
	}

	function show_child(ob){
		var current = $(ob).html();
		if(current==="+"){
			$(ob).html("-");
		}else{
			$(ob).html("+");
		}
		$(ob).parent('li').find("ul").toggle('fast');
	}

	var isloaddata = [];

	function show_child_next(ob){
		var ID = $(ob).attr('data');
		var current = $(ob).html();
			if(current==="+"){
				$(ob).html("-");
				if(ID!=''){
					if(jQuery.inArray( "data"+ID, isloaddata )<0){
						$.ajax({
					        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/get_child_categories' ?>",
					        dataType: "html",
					        type: "POST",
					        data: "ID="+ID,
					        success: function(result){
					        	if(result!='False'){
					        		$(ob).parent('li').append(result);
					        		isloaddata.push("data"+ID);
					        	}else{
					        		$(ob).html("");
					        		$(ob).css({"background":"#FFF","border":"1px solid #FFF"});
					        	}
					        }
					    });	
					}
		        }
			}else{
				$(ob).html("+");
			}
		$(ob).parent('li').find("ul").toggle("fast");
	}

	function senddata(url,title,data){
		enablescrollsetup();
		$(".over_lay .block1_inner h1").html(title);
		$(".over_lay .block2_inner").html("");
		$.ajax({
	        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/' ?>"+url,
	        dataType: "html",
	        type: "POST",
	        data: "Data="+data,
	        success: function(result){
	        	if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);
        			$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
        			$(".over_lay").addClass('in');
                }else{
                	alert("Không thực hiện được chức năng theo yêu cầu.");
                }
	        }
	    });
	}

	function save_properties(ob){
		$(ob).addClass("saving");
		var form = $(ob).parent('div').parent('form');
		$.ajax({
	        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/save_properties' ?>",
	        dataType: "html",
	        type: "POST",
	        data: form.serialize(),
	        success: function(result){
	        	var ar = result.split("|");
                if(ar[0]!='False'){
        			if(ar[0]!=0){
	        			$(".value"+ar[1]).append(ar[2]);
        			}
        			if(ar[0]!=0){
	        			$("#PropertiesID").append(ar[2]);
        			}
        			disablescrollsetup();
        			$(".over_lay").hide();
                }else{
                	alert("Không thực hiện được chức năng theo yêu cầu.");
                }
	        }
	    });
	}

	var PropertiesList = [];

	function select_properties(){
		var ID = $("#PropertiesID").val();
		if(ID!=''){
			if(jQuery.inArray( "data"+ID, PropertiesList )<0){
				$(".message_empty").remove();
				$.ajax({
			        url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/add_newrow_properties' ?>",
			        dataType: "html",
			        type: "POST",
			        data: 'Data='+ID,
			        success: function(result){
			        	$(".content_tab7").append(result);
			        	PropertiesList.push("data"+ID);
			        }
			    })
			}
	    }
	}

	function add_unit(){
		var unit = prompt("Vui lòng điền đơn vị muốn tạo mới");
		if(unit!=null){
			$.ajax({
	        	url: "<?php echo base_url().ADMINPATH.'/report/warehouse_products/add_unit' ?>",
		        dataType: "html",
		        type: "POST",
		        data: 'Data='+unit,
		        success: function(result){
		        	$("#Donvi").prepend("<option value='"+unit+"'>"+unit+"</option>");
		        	$("#Donvi").val(unit);
		        }
		    });
		}
	}

	function remove_rowtab(ob){
		$(ob).parent('div').parent('li').parent('div').remove();
	}

	function saveall(ob){
		$(ob).addClass("saving");
		$("#products_form").append("<input type='hidden' name='SaveAndExit' value='1' />");
		$("#products_form").submit();
	}
</script>

