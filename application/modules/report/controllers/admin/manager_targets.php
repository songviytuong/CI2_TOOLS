<?php 
class Manager_targets extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="manager_targets";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_targets")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_targets order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_targets/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_targets/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Targets | Manager Report Tools');
		$this->template->write_view('content','admin/manager_targets_home',$data);
		$this->template->render();
	}

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Targets add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_targets/'
        );
        $this->template->write_view('content','admin/manager_targets_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Month = isset($_POST['Month']) ? $_POST['Month'] : '' ;
        $Year = isset($_POST['Year']) ? $_POST['Year'] : '' ;
        if(is_numeric($Month) && is_numeric($Year)){
            if($Month>12 || $Month<1) return;
            $Month = $Month<10 ? '0'.$Month : $Month ;
            $data = array(
                'Month'     => $Month.'/'.$Year
            );
            $json = array();
            foreach($_POST as $key=>$value){
                if(!array_key_exists($key,$data)){
                    $json[$key] = $value;
                }
            }
            $json = json_encode($json);
            $data['Data'] = $json;
            $have_month = $this->db->query("select ID from ttp_report_targets where Month='$Month/$Year'")->row();
            if($have_month){
                $this->db->where("ID",$have_month->ID);
                $this->db->update("ttp_report_targets",$data);
            }else{
                $this->db->insert("ttp_report_targets",$data);
            }
        }
        redirect(ADMINPATH.'/report/manager_targets/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_targets where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_targets where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Targets | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_targets/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_targets_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Month = isset($_POST['Month']) ? $_POST['Month'] : '' ;
        $Year = isset($_POST['Year']) ? $_POST['Year'] : '' ;
        if(is_numeric($Month) && is_numeric($Year)){
            if($Month>12 || $Month<1) return;
            $Month = $Month<10 ? '0'.$Month : $Month ;
            $data = array(
                'Month'     => $Month.'/'.$Year
            );
            $json = array();
            foreach($_POST as $key=>$value){
                if(!array_key_exists($key,$data)){
                    $json[$key] = $value;
                }
            }
            $json = json_encode($json);
            $data['Data'] = $json;
            $have_month = $this->db->query("select ID from ttp_report_targets where Month='$Month/$Year' and ID!=$ID")->row();
            if($have_month){
                $this->db->query("delete from ttp_report_targets where ID=$have_month->ID");
            }
            $this->db->where("ID",$have_month->ID);
            $this->db->update("ttp_report_targets",$data);
        }
        redirect(ADMINPATH.'/report/manager_targets/');
    }
}
?>
