<div class="col-xs-12">
	<div class="alert alert-info col-xs-12">
		<p><b>Ms Quỳnh</b> |  <b>0985064502 - 02862958887</b> <br>Xử lý đơn hàng bưu cục Viettel Tân Sơn Nhì</p>
	</div>
	<div class="row" style="padding:10px 0px;margin:0px;border-bottom: 1px solid #e1e1e1;">
		<div class="col-xs-12 col-md-3"><b>Vận đơn</b></div>
		<div class="col-xs-12 col-md-3"><b>Địa chỉ nhận hàng</b></div>
		<div class="col-xs-12 col-md-2"><b>Đối tác</b></div>
		<div class="col-xs-12 col-md-2"><b>Hệ thống</b></div>
		<div class="col-xs-12 col-md-2"><b>Đồng bộ</b></div>
	</div>
	<?php 
	$order_arr = array();
	if(count($order)>0){
		foreach($order as $row){
			$order_arr[$row->TransportRef] = $row->TransportStatus;
		}
	}
	$pagetotal = 0;
	if(isset($data['data']) && count($data['data'])>0){
		$listpage = 0;
		foreach($data['data'] as $key=>$row){
			$TransportRef = isset($row['order_number']) ? $row['order_number'] : '###' ;
			$Customer = isset($row['receiver_fullname']) ? $row['receiver_fullname'] : '--' ;
			$Phone = isset($row['receiver_phone']) ? $row['receiver_phone'] : '--' ;
			$Address = isset($row['address_delivery']) ? $row['address_delivery'] : '--' ;
			$Status = isset($row['status']) && isset($row['status']['name']) ? $row['status']['name'] : '--' ;
			$Color = isset($row['status']) && isset($row['status']['color']) ? strtolower($row['status']['color']) : '--' ;
			$Code = isset($row['status']) && isset($row['status']['code']) ? strtolower($row['status']['code']) : 0 ;
			if($Code!=600){
				$fee = isset($row['pvc']) ? $row['pvc'] : 0 ;
				$SystemStatus = isset($order_arr[$TransportRef]) ? $order_arr[$TransportRef] : '' ;
				$Code = $SystemStatus!=$Status ? "<a onclick='update_transport_status(this,\"$TransportRef\",\"$Status\",$Code,$fee)' class='btn-sm btn-success'>Xác nhận</a>" : '';
				$page = $listpage%6;
				if($page==0){
					$pagetotal++;
				}
				$listpage++;
				echo "<div class='row page-nav page-$pagetotal'>";
				echo "<div class='col-xs-12 col-md-3'><b>$TransportRef</b><br>$Customer<br>Số điện thoại : $Phone</div>";
				echo "<div class='col-xs-12 col-md-3'>$Address</div>";
				echo "<div class='col-xs-12 col-md-2'><label class='label label-$Color'>$Status</label></div>";
				$system = isset($order_arr[$TransportRef]) ? "<label class='label label-primary'>".$order_arr[$TransportRef]."</label>" : '' ;
				echo "<div class='col-xs-12 col-md-2'>$system</div>";
				echo "<div class='col-xs-12 col-md-2'>$Code</div>";
				echo "</div>";
			}
		}
	}
	?>
	<nav aria-label="Page navigation">
	  	<ul class="pagination">
	    	<?php 
	    	for($i=0;$i<$pagetotal;$i++){
	    		$page_li = $i+1;
	    		$active = $page_li==1 ? 'active' : '' ;
	    		echo '<li class="'.$active.'"><a onclick="load_page(this,'.$page_li.')">'.$page_li.'</a></li>';
	    	}
	    	?>
	  	</ul>
	</nav>
</div>
<style>
	.page-nav{display: none;padding:10px 0px;margin:0px;}
	.page-nav:nth-child(2n+1){background: #eee;}
	.page-1{display: block;}
</style>
<script type="text/javascript">
	function load_page(ob,page){
		$(".pagination li").removeClass("active");
		$(ob).parent('li').addClass("active");
		$(".page-nav").hide();
		$(".page-nav.page-"+page).show();
	}

	function update_transport_status(ob,TransportRef,TransportStatus,Code,Fee){
		$(ob).html('Loading...');
		$.post("<?php echo base_url().ADMINPATH.'/report/import_order/update_transport_status' ?>",{TransportRef:TransportRef,TransportStatus:TransportStatus,Code:Code,Fee:Fee},function(result){
			$(ob).html(result);
		},'html');
	}
</script>