<?php 
$active_vs = 1;
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

?>
<style>
    .dropdown-menu{max-height:500px;overflow-y:scroll;}
</style>
<div class="containner">
    
    <div class="select_progress">
	    <div class="block1">
	    	<h3 style="margin: 10px 0px;"><?php echo $data->name ?> <a title="<?php echo $data->description ?>"><i class="fa fa-question-circle" aria-hidden="true"></i></a></h3>
	    </div>
	    <div class="block2">
		    <div class="dropdown">
                <a class='value_excute'><span><?php echo date('d/m/Y',strtotime($startday))." - ".date('d/m/Y',strtotime($stopday)) ?></span> <b class="caret"></b></a>
                <p class="value_excute_vs" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>So sánh với : <span><?php echo date('d/m/Y',strtotime($startday_vs))." - ".date('d/m/Y',strtotime($stopday_vs)) ?></span></p>
                <div class="box_dropdown">
                    <div id="reportrange" class="list_div">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                    <div class="center_div list_div">
                        <span><input type='checkbox' id='active_vs' <?php echo $active_vs==1 ? "checked='checked'" : "" ; ?> />So sánh với</span>
                        <select id="vs_selectbox" <?php echo $active_vs==1 ? "style='opacity:1'" : "style='opacity:0.6'" ; ?>>
                            <option value="tuychinh">Tùy chỉnh</option>
                            <option value="lastmonth">Tháng trước</option>
                            <option value="last7day">7 ngày trước</option>
                            <option value="last30day">30 ngày trước</option>
                        </select>
                    </div>
                    <div id="reportrange1" class="list_div" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                    <div class='compare list_div'>
                        <a id="showcompare" class="btn btn-danger"><i class="fa fa-exchange"></i> Áp dụng</a>
                        <a id="cancel_showcompare" class="btn btn-default">Hủy</a>
                    </div>  
                </div>
            </div>
	    </div>
    </div>
    <div class="row" style="margin:0px;">
        <div class="btn-group">
            <?php 
            $arr_menu = array();
            $currenttype = "Chọn ngành hàng";
            $str = "";
            if(count($categories_all)>0){
                foreach($categories_all as $row){
                    $selected = $row->ID==$type ? "style='color:#090'" : "style='color:#ccc'";
                    $currenttype = $row->ID==$type ? $row->Title : $currenttype ;
                    $str.="<li><a href='$base_link/?type=$row->ID'><i class='fa fa-check-square' aria-hidden='true' $selected></i> $row->Title</a></li>";
                }
            }
            ?>
            <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> <?php echo $currenttype ?></a>
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <?php 
                echo $str;
                ?>
            </ul>
        </div>
    </div>
    <div class="row" style="margin:0px">
        <div class="form-group">
            <table class="table table-bordered">
                <tr style="background:#EEE">
                    <th class='text-left'>STT</th>
                    <th>Loại nội dung</th>
                    <th class='text-right'>SP tạo trong kỳ</th>
                    <th class='text-right'>SP sửa trong kỳ</th>
                    <th class='text-right'>Tổng số SP</th>
                    <th class='text-right'>SL lịch sử</th>
                    <th class='text-right'>Chênh lệch</th>
                </tr>
                <?php 
                $arr_categories = array();
                $arr_categories_vs = array();
                if(count($categories)>0){
                    foreach($categories as $row){
                        $arr_categories[$row->ID] = array(
                            'Created' => array(
                                'Primary' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Chimmoi' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Total' => array()
                            ),
                            'LastEdited' => array(
                                'Primary' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Chimmoi' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Total' => array()
                            ),
                            'Title'=>$row->Title,
                            'Total'=>array()
                        );

                        $arr_categories_vs[$row->ID] = array(
                            'Created' => array(
                                'Primary' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Chimmoi' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Total' => array()
                            ),
                            'LastEdited' => array(
                                'Primary' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Chimmoi' => array(
                                    0=>array(),
                                    1=>array(),
                                    2=>array(),
                                    'Total'=>array()
                                ),
                                'Total' => array()
                            ),
                            'Title'=>$row->Title,
                            'Total'=>array()
                        );
                    }
                }
                if(count($result_created)>0){
                    foreach($result_created as $row){
                        if(isset($arr_categories[$row->CategoriesID])){
                            if($row->Chimmoi==1){
                                if(isset($arr_categories[$row->CategoriesID]['Created']['Chimmoi'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Created']['Chimmoi'][$row->VariantType])){
                                        $arr_categories[$row->CategoriesID]['Created']['Chimmoi'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Total'])){
                                        $arr_categories[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Created']['Chimmoi']['Total'])){
                                        $arr_categories[$row->CategoriesID]['Created']['Chimmoi']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Created']['Total'])){
                                        $arr_categories[$row->CategoriesID]['Created']['Total'][] = $row->ID;
                                    }
                                }
                            }else{
                                if(isset($arr_categories[$row->CategoriesID]['Created']['Primary'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Created']['Primary'][$row->VariantType])){
                                        $arr_categories[$row->CategoriesID]['Created']['Primary'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Total'])){
                                        $arr_categories[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Created']['Primary']['Total'])){
                                        $arr_categories[$row->CategoriesID]['Created']['Primary']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Created']['Total'])){
                                        $arr_categories[$row->CategoriesID]['Created']['Total'][] = $row->ID;
                                    }
                                }
                            }
                        }
                    }
                }

                if(count($result_lastedited)>0){
                    foreach($result_lastedited as $row){
                        if(isset($arr_categories[$row->CategoriesID])){
                            if($row->Chimmoi==1){
                                if(isset($arr_categories[$row->CategoriesID]['LastEdited']['Chimmoi'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['LastEdited']['Chimmoi'][$row->VariantType])){
                                        $arr_categories[$row->CategoriesID]['LastEdited']['Chimmoi'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Total'])){
                                        $arr_categories[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['LastEdited']['Chimmoi']['Total'])){
                                        $arr_categories[$row->CategoriesID]['LastEdited']['Chimmoi']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['LastEdited']['Total'])){
                                        $arr_categories[$row->CategoriesID]['LastEdited']['Total'][] = $row->ID;
                                    }
                                }
                            }else{
                                if(isset($arr_categories[$row->CategoriesID]['LastEdited']['Primary'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['LastEdited']['Primary'][$row->VariantType])){
                                        $arr_categories[$row->CategoriesID]['LastEdited']['Primary'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['Total'])){
                                        $arr_categories[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['LastEdited']['Primary']['Total'])){
                                        $arr_categories[$row->CategoriesID]['LastEdited']['Primary']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories[$row->CategoriesID]['LastEdited']['Total'])){
                                        $arr_categories[$row->CategoriesID]['LastEdited']['Total'][] = $row->ID;
                                    }
                                }
                            }
                        }
                    }
                }

                if(count($result_created_vs)>0){
                    foreach($result_created_vs as $row){
                        if(isset($arr_categories_vs[$row->CategoriesID])){
                            if($row->Chimmoi==1){
                                if(isset($arr_categories_vs[$row->CategoriesID]['Created']['Chimmoi'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Created']['Chimmoi'][$row->VariantType])){
                                        $arr_categories_vs[$row->CategoriesID]['Created']['Chimmoi'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Created']['Chimmoi']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Created']['Chimmoi']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Created']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Created']['Total'][] = $row->ID;
                                    }
                                }
                            }else{
                                if(isset($arr_categories_vs[$row->CategoriesID]['Created']['Primary'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Created']['Primary'][$row->VariantType])){
                                        $arr_categories_vs[$row->CategoriesID]['Created']['Primary'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Created']['Primary']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Created']['Primary']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Created']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Created']['Total'][] = $row->ID;
                                    }
                                }
                            }
                        }
                    }
                }

                if(count($result_lastedited_vs)>0){
                    foreach($result_lastedited_vs as $row){
                        if(isset($arr_categories_vs[$row->CategoriesID])){
                            if($row->Chimmoi==1){
                                if(isset($arr_categories_vs[$row->CategoriesID]['LastEdited']['Chimmoi'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['LastEdited']['Chimmoi'][$row->VariantType])){
                                        $arr_categories_vs[$row->CategoriesID]['LastEdited']['Chimmoi'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['LastEdited']['Chimmoi']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['LastEdited']['Chimmoi']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['LastEdited']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['LastEdited']['Total'][] = $row->ID;
                                    }
                                }
                            }else{
                                if(isset($arr_categories_vs[$row->CategoriesID]['LastEdited']['Primary'][$row->VariantType])){
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['LastEdited']['Primary'][$row->VariantType])){
                                        $arr_categories_vs[$row->CategoriesID]['LastEdited']['Primary'][$row->VariantType][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['LastEdited']['Primary']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['LastEdited']['Primary']['Total'][] = $row->ID;
                                    }
                                    if(!in_array($row->ID,$arr_categories_vs[$row->CategoriesID]['LastEdited']['Total'])){
                                        $arr_categories_vs[$row->CategoriesID]['LastEdited']['Total'][] = $row->ID;
                                    }
                                }
                            }
                        }
                    }
                }

                $arr_chimmoi = array(0=>"Sản phẩm chính thức",1=>"Sản phẩm chim mồi");
                $arr_typeproducts = array(0=>"Sản phẩm đơn",1=>"Sản phẩm biến thể",2=>"Sản phẩm bundle");
                $arr_convert = array(0=>"Primary",1=>"Chimmoi");
                $span = "<span style='padding:0px 0px'></span>";
                if(count($arr_categories)>0){
                    $i=1;
                    foreach($arr_categories as $key=>$row){
                        echo "<tr style='background:#dff0d8'>";
                        echo "<th>$i</th>";
                        echo "<th>".$span.$row['Title']."</th>";
                        echo "<th class='text-right'>".number_format(count($row['Created']['Total']))."</th>";
                        echo "<th class='text-right'>".number_format(count($row['LastEdited']['Total']))."</th>";
                        echo "<th class='text-right'>".number_format(count($row['Total']))."</th>";
                        echo "<th class='text-right'>".number_format(count($arr_categories_vs[$key]['Total']))."</th>";
                        $end = abs(count($row['Total'])-count($arr_categories_vs[$key]['Total']));
                        echo "<th class='text-right'>".number_format($end)."</th>";
                        echo "</tr>";
                        for($k=0;$k<2;$k++){
                            $point = $k+1;
                            $px = "10px"; 
                            echo "<tr>";
                            echo "<td>$i.$point</td>";
                            echo "<td>".str_replace('0px',$px,$span).$arr_chimmoi[$k]."</td>";
                            echo "<td class='text-right'>".number_format(count($row['Created'][$arr_convert[$k]]['Total']))."</td>";
                            echo "<td class='text-right'>".number_format(count($row['LastEdited'][$arr_convert[$k]]['Total']))."</td>";
                            $compare = array_merge($row['LastEdited'][$arr_convert[$k]]['Total'], $row['Created'][$arr_convert[$k]]['Total']);
                            $compare = array_unique($compare);
                            echo "<td class='text-right'>".number_format(count($compare))."</td>";
                            $compare_vs = array_merge($arr_categories_vs[$key]['LastEdited'][$arr_convert[$k]]['Total'], $arr_categories_vs[$key]['Created'][$arr_convert[$k]]['Total']);
                            $compare_vs = array_unique($compare_vs);
                            echo "<td class='text-right'>".number_format(count($compare_vs))."</td>";
                            $end = abs(count($compare_vs)-count($compare));
                            echo "<td class='text-right'>".number_format($end)."</td>";
                            echo "</tr>";
                            for($j=0;$j<3;$j++){
                                $point1 = $j+1;
                                $px = "20px";
                                echo "<tr>";
                                echo "<td>$i.$point.$point1</td>";
                                echo "<td>".str_replace('0px',$px,$span).$arr_typeproducts[$j]."</td>";
                                echo "<td class='text-right'>".number_format(count($row['Created'][$arr_convert[$k]][$j]))."</td>";
                                echo "<td class='text-right'>".number_format(count($row['LastEdited'][$arr_convert[$k]][$j]))."</td>";
                                $compare = array_merge($row['Created'][$arr_convert[$k]][$j], $row['LastEdited'][$arr_convert[$k]][$j]);
                                $compare = array_unique($compare);
                                echo "<td class='text-right'>".number_format(count($compare))."</td>";
                                $compare_vs = array_merge($arr_categories_vs[$key]['Created'][$arr_convert[$k]][$j], $arr_categories_vs[$key]['LastEdited'][$arr_convert[$k]][$j]);
                                $compare_vs = array_unique($compare_vs);
                                echo "<td class='text-right'>".number_format(count($compare_vs))."</td>";
                                $end = abs(count($compare_vs)-count($compare));
                                echo "<td class='text-right'>".number_format($end)."</td>";
                                echo "</tr>";
                            }
                        }
                        $i++;
                    }
                }
                ?>
            </table>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<?php 
    $numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
    $numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

    $numday_start_group2 = (strtotime(date('Y-m-d',time())) - strtotime($startday_vs))/(3600*24);
    $numday_stop_group2 = (strtotime(date('Y-m-d',time())) - strtotime($stopday_vs))/(3600*24);
    
?>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /****  reportrange 2  ****/
        var cb1 = function (start, end, label) {
            $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        var optionSet2 = {
            startDate: <?php echo $numday_start_group2==0 ? "moment()" : "moment().subtract($numday_start_group2, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group2==0 ? "moment()" : "moment().subtract($numday_stop_group2, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 120
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange1 span').html(<?php echo $numday_start_group2==0 ? "moment()" : "moment().subtract($numday_start_group2, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group2==0 ? "moment()" : "moment().subtract($numday_stop_group2, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange1').daterangepicker(optionSet2, cb1);

        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".value_excute span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
        });

        $('#reportrange1').on('apply.daterangepicker', function(ev, picker) {
            $(".value_excute_vs span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
            $(".value_excute_vs").show();
        });
    });   
</script>