<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update_mt' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN ĐỐI TÁC KÊNH MT</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save</button>
				</div>
			</div>
			<div class="row" style="margin:0px auto">
				<table class="customer_table">
					<tr>
						<td>Kênh khách hàng</td>
						<td colspan="8">
							<input type="radio" value="1" name="Type" disabled="true" style="margin-left:20px" /> GT 
							<input type="radio" value="2" name="Type" checked="checked" style="margin-left:20px" /> MT 
						</td>
					</tr>
					<tr>
						<td>Mã siêu thị</td>
						<td><input type='text' name='Code' class='required' value='<?php echo $data->Code ?>' required /></td>
						<td style="text-align:right;padding-right:10px">Cửa hàng</td>
						<td colspan="6"><input type='text' name='Name' class='required' value='<?php echo $data->Name ?>' required /></td>
					</tr>
					<tr>
						<td>Khu vực</td>
						<td>
							<select name="AreaID" id="Khuvuc">
								<option value='0'>-- Chọn khu vực --</option>
								<?php 
								$area = $this->db->query("select * from ttp_report_area")->result();
								if(count($area)>0){
									foreach($area as $row){
										$selected = $row->ID==$data->AreaID ? "selected='selected'" : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</td>
						<td style='text-align:right;padding-right:10px'>Hệ thống</td>
						<td>
							<select name="SystemID" id="SystemID">
								<?php 
								$system = $this->db->query("select * from ttp_report_system")->result();
								if(count($system)>0){
									foreach($system as $row){
										$selected = $row->ID==$data->SystemID ? "selected='selected'" : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</td>
						<td><a onclick="add_system()" class="btn btn-default" style="height:30px;line-height: 16px;margin-left:10px"><i class="fa fa-plus-square" aria-hidden="true"></i> Tạo mới</a></td>
					</tr>
					<tr>
						<td>Tỉnh thành</td>
						<td>
							<select name="CityID" id="Tinhthanh">
								<?php 
								$city = $this->db->query("select ID,Title from ttp_report_city where ID=$data->CityID")->row();
								echo $city ? "<option value='$data->CityID'>$city->Title</option>" : "<option value='0'>-- Chọn tỉnh thành --</option>" ;
								?>
							</select>
						</td>
						<td style="text-align:right;padding-right:10px">Quận huyện</td>
						<td>
							<select name="DistrictID" id="Quanhuyen">
								<?php 
								$district = $this->db->query("select ID,Title from ttp_report_district where ID=$data->DistrictID")->row();
								echo $city ? "<option value='$data->DistrictID'>$district->Title</option>" : "<option value='0'>-- Chọn quận huyện --</option>" ;
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Tên công ty</td>
						<td colspan="8">
							<input type='text' name='Company' class="required" value='<?php echo $data->Company ?>' required />
						</td>
					</tr>
					<tr>
						<td>Địa chỉ đăng ký</td>
						<td colspan="8">
							<input type='text' name='Address' class="required" value='<?php echo $data->Address ?>' required />
						</td>
					</tr>
					<tr>
						<td>Địa chỉ giao hàng</td>
						<td colspan="8">
							<input type='text' name='AddressOrder' class="required" value='<?php echo $data->AddressOrder ?>' required />
						</td>
					</tr>
					<tr>
						<td>Người đại diện</td>
						<td><input type='text' name="Surrogate" value='<?php echo $data->Surrogate ?>' /></td>
						<td style="text-align:right;padding-right:10px">Số điện thoại</td>
						<td><input type='text' name="Phone1" value='<?php echo $data->Phone1 ?>' /></td>
						<td style="text-align:right;padding-right:10px">Email</td>
						<td><input type='text' name="Email" placeholder="example@example.com.vn" value='<?php echo $data->Email ?>' /></td>
					</tr>
					<tr>
						<td>Công nợ</td>
						<td><input type='number' name="Dept" value='<?php echo $data->Dept ?>' /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 780px !important;}
	.body_content .customer_table{width:100%;border-collapse: collapse;background:none !important;margin-top:10px;}
	.body_content .customer_table tr td{padding:7px 0px;}
	.body_content .customer_table tr td:first-child{width:120px;}
	.body_content .customer_table tr td:nth-child(2){width:200px;}
	.body_content .customer_table tr td:nth-child(3){width:100px;}
	.body_content .customer_table tr td input[type="text"]{width:100%; border:1px solid #c1c1c1;padding:4px 5px;height:32px;}
	.body_content .customer_table tr td input[type="number"]{text-align:right;width:100%; border:1px solid #c1c1c1;padding:4px 5px;height:32px;}
	.body_content .customer_table tr td select{height:30px;width:100%; border:1px solid #c1c1c1;padding:4px 5px;}
	.body_content .showbox_history table{width:100%;border-collapse: collapse;border:1px solid #E1e1e1;margin-top:10px;}
	.body_content .showbox_history table tr th{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.body_content .showbox_history table tr td{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.view_history{margin-top:10px;}
	.view_history a{color:#1A82C3;text-decoration: underline;border:none !important;padding:0px !important;margin-right:20px;}
	.view_history a i{font-size: 12px;margin-left:6px;}
	.add_advisory{clear:both;margin-top:10px;}
	.add_advisory h1{font-size: 22px;padding: 10px 0px;text-transform: uppercase;font-weight: bold;margin-bottom: 10px}
	.add_advisory textarea{width:100%;border:1px solid #c1c1c1;height:80px;padding:5px;margin-top:5px;}
	.daterangepicker{width: auto;}
</style>
<script>
	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_city_by_area' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_district_by_city' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            }
	        });
		}
	});

	function changechanel(links){
		if(links==1){
			var destination="add_sales";
		}else{
			var destination="add_sales_mt";
		}
		window.location="<?php echo base_url().ADMINPATH.'/report/import_customer/' ?>"+destination;
	}

	function add_system(){
        var getValue = prompt("Điền tên hệ thống mới : ");
        if(getValue!=null){
            var url = "<?php echo base_url().ADMINPATH.'/report/import_customer/add_system_mt' ?>";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue,
                success: function(result){
                	if(result!='false'){
	                    $("#SystemID").prepend("<option value='"+result+"'>"+getValue+"</option>");
	                    $("#SystemID").val(result);
                    }else{
                    	alert("Hệ thống đã tồn tại !. Vui lòng tạo hệ thống khác .");
                    }
                }
            });
        }
    }
</script>