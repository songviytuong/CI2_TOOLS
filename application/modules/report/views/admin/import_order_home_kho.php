<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$total_tienhang = 0;
$total_chiphi 	= 0;
$total_giatri 	= 0;
$total_huy 		= 0;
$month 			= array();
$dayofmonth		= array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href='<?php echo base_url().ADMINPATH.'/report/import_order' ?>'>DANH SÁCH ĐƠN HÀNG</a><?php echo $this->user->UserType==8 ? " | <a href='".base_url().ADMINPATH."/report/import_order/quick_view'>THỐNG KÊ NHANH</a>" : '' ; ?></h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="import_orderlist">
    	<div class="block2 row">
            <div class="form-group">
        		<div class="block_2_1 col-xs-8">
                    <a class="btn btn-default" href="<?php echo base_url().ADMINPATH."/report/import_order/group_supplier" ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Yêu cầu lấy hàng</a>
                    <a class="btn btn-default" href="<?php echo base_url().ADMINPATH."/report/import_order/picking_supplier" ?>"><i class="fa fa-book" aria-hidden="true"></i> Trạng thái soạn hàng</a>
                </div>
        		<div class="block_2_2 col-xs-4">
        			<button class="btn btn-default pull-right" onclick="showtools()" style="margin-left:5px;">Advanced Filter <b class="caret"></b></button>
                    <a class="btn btn-primary" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export</a>
        		</div>
            </div>
    	</div>
        <div class="clear"></div>
        <div class="export_tools_kt">
            <form action="<?php echo base_url().ADMINPATH."/report/import/export_ketoan" ?>" method="post" id="falseclass">
                <span>Chọn ngày xuất: </span>
                <input type='text' name="Export_date" class="form-control date-picker" id="Export_date" value="<?php echo date('Y-m-d',time()-(3600*8)); ?>" required />
                <span>Chọn loại báo cáo: </span>
                <select name="TypeExport">
                    <option value="7">Các ĐH đã duyệt</option>
                </select>
                <button type="submit" class="btn btn-default">Xuất báo cáo</button>
            </form>
        </div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"c.Name",1=>"a.CustomerType",2=>"e.ID",3=>"d.ID",4=>"b.ID",5=>"a.SoluongSP",6=>"a.Total",7=>"a.Chiphi",8=>"a.Status",14=>"a.KhoID");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Tên khách hàng",1=>"Loại khách hàng",2=>"Khu vực",3=>"Tỉnh thành",4=>"Quận huyện",5=>"Số lượng sản phẩm",6=>"Tổng tiền hàng",7=>"Chi phí vận chuyển",8=>"Trạng thái đơn hàng",14=>"Kho xuất hàng");
                    $fill_data_arr = explode(" and ",$fill_data);
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                    <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                    <div class="list_toolls col-xs-2">
                        <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                        <ul class="dropdownbox">
                            <li class="form-control"><i class="fa fa-search"></i> <input type="text" /></li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                    <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                <ul>
                                    <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                    <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                    <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                    <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                    <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                    <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                    <li><a onclick="setfield(this,14,'khoxuathang')">Kho xuất hàng</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-3">
                        <?php 
                        if($value_field==2 || $value_field==3 || $value_field==4){
                            if($value_field==2){
                                $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();    
                            }
                            if($value_field==3){
                                $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();    
                            }
                            if($value_field==4){
                                $result = $this->db->query("select * from ttp_report_district order by Title ASC")->result();    
                            }
                            if(count($result)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($result as $row){
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ; 
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }elseif($value_field==14){
                            $result = $this->db->query("select MaKho,ID from ttp_report_warehouse order by Title ASC")->result();    
                            echo "<select name='FieldText[]' class='form-control'>";
                                foreach($result as $row){
                                    $fill_temp_value = $param_value==$row->ID ? $row->MaKho : $fill_temp_value ;
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ; 
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                        }elseif ($value_field==8) {
                            echo "<select name='FieldText[]' class='form-control'>";
                            echo $param_value==9 ? "<option value='9' selected='selected'>Đơn hàng chuyển sang nv điều phối</option>" : "<option value='9'>Đơn hàng chuyển sang nv điều phối</option>" ;
                            echo $param_value==8 ? "<option value='8' selected='selected'>Đơn hàng bị trả về</option>" : "<option value='8'>Đơn hàng bị trả về</option>" ;
                            echo $param_value==7 ? "<option value='7' selected='selected'>Chuyển sang bộ phận giao hàng</option>" : "<option value='7'>Chuyển sang bộ phận giao hàng</option>" ;
                            echo $param_value==6 ? "<option value='6' selected='selected'>Đơn hàng bị trả về từ kế toán</option>" : "<option value='6'>Đơn hàng bị trả về từ kế toán</option>" ;
                            echo $param_value==5 ? "<option value='5' selected='selected'>Đơn hàng chờ kế toán duyệt</option>" : "<option value='5'>Đơn hàng chờ kế toán duyệt</option>" ;
                            echo $param_value==4 ? "<option value='4' selected='selected'>Đơn hàng bị trả về từ kho</option>" : "<option value='4'>Đơn hàng bị trả về từ kho</option>" ;
                            echo $param_value==3 ? "<option value='3' selected='selected'>Đơn hàng mới chờ kho duyệt</option>" : "<option value='3'>Đơn hàng mới chờ kho duyệt</option>" ;
                            echo $param_value==2 ? "<option value='2' selected='selected'>Đơn hàng nháp</option>" : "<option value='2'>Đơn hàng nháp</option>" ;
                            echo $param_value==0 ? "<option value='0' selected='selected'>Đơn hàng thành công</option>" : "<option value='0'>Đơn hàng thành công</option>" ;
                            echo $param_value==1 ? "<option value='1' selected='selected'>Đơn hàng hủy</option>" : "<option value='1'>Đơn hàng hủy</option>" ;
                            echo "</select>";
                            $fill_temp_value = $param_value==9 ? "Đơn hàng chuyển sang nv điều phối" : $fill_temp_value ;
                            $fill_temp_value = $param_value==8 ? "Đơn hàng bị trả về" : $fill_temp_value ;
                            $fill_temp_value = $param_value==7 ? "Chuyển sang bộ phận giao hàng" : $fill_temp_value ;
                            $fill_temp_value = $param_value==6 ? "Đơn hàng bị trả về từ kế toán" : $fill_temp_value ;
                            $fill_temp_value = $param_value==5 ? "Đơn hàng chờ kế toán duyệt" : $fill_temp_value ;
                            $fill_temp_value = $param_value==4 ? "Đơn hàng bị trả về từ kho" : $fill_temp_value ;
                            $fill_temp_value = $param_value==3 ? "Đơn hàng mới chờ kho duyệt" : $fill_temp_value ;
                            $fill_temp_value = $param_value==2 ? "Đơn hàng nháp" : $fill_temp_value ;
                            $fill_temp_value = $param_value==1 ? "Đơn hàng hủy" : $fill_temp_value ;
                            $fill_temp_value = $param_value==0 ? "Đơn hàng thành công" : $fill_temp_value ;
                        }elseif ($value_field==1) {
                            echo "<select name='FieldText[]' class='form-control'>
                                    <option value='0'>Khách hàng mới</option>
                                    <option value='1'>Khách hàng cũ</option>
                                </select>";
                            $fill_temp_value = $param_value==1 ? "Khách hàng cũ" : $fill_temp_value ;
                            $fill_temp_value = $param_value==0 ? "Khách hàng mới" : $fill_temp_value ;
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                                <div class="list_toolls col-xs-2">
                                    <input type="hidden" class="FieldName" name="FieldName[]" />
                                    <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                                    <ul class="dropdownbox">
                                        <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                            <ul>
                                                <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                                <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                            </ul>
                                        </li>
                                        <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                            <ul>
                                                <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                                <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                                <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                            </ul>
                                        </li>
                                        <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                            <ul>
                                                <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                                <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                                <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                                <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                                <li><a onclick="setfield(this,14,'khoxuathang')">Kho xuất hàng</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="list_toolls reciveroparation col-xs-2">
                                    <select class="oparation form-control" name="FieldOparation[]">
                                        <option value="1">Bằng</option>
                                        <option value="0">Có chứa</option>
                                        <option value="2">Khác</option>
                                        <option value="3">Lớn hơn</option>
                                        <option value="4">Nhỏ hơn</option>
                                        <option value="5">Lớn hơn hoặc bằng</option>
                                        <option value="6">Nhỏ hơn hoặc bằng</option>
                                    </select>
                                </div>
                                <div class="list_toolls reciverfillter col-xs-3">
                                    <input type="text" name="FieldText[]" id="textsearch" class="form-control" />
                                </div>
                                <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    	<div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
    		<table id="table_data">
    			<tr>
    				<th>Loại ĐH</th>
    				<th style='width: 90px;'>Ngày</th>
    				<th>Tên khách hàng</th>
    				<th>Thành phố</th>
    				<th>SL sản phẩm</th>
    				<th>Tổng giá trị</th>
    				<th>Kho xuất</th>
    				<th>Diễn giải</th>
    			</tr>
    			<?php 
    			$total_adiva = 0;
                $type_status = $this->define_model->get_order_status('type','order');
                $arr_type = array();
                foreach($type_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $arr_type[$code] = $ite->name;
                }
    			if(count($data)>0){
                    foreach($data as $row){
    					$tienhang = $row->Total-$row->Chietkhau-$row->Reduce;
    					$total_huy += $row->Status==1 ? $row->Total : 0 ;
    					echo "<tr>
			    				<td style='text-align:center;width:80px'><a href='{$base_link}preview/$row->ID'>".$arr_type[$row->OrderType]."</a></td>
			    				<td style='width: 90px;'><a href='{$base_link}preview/$row->ID'>".date('d/m/Y',strtotime($row->Ngaydathang))."</a></td>
			    				<td><a href='{$base_link}preview/$row->ID'>$row->Name</a></td>
			    				<td><a href='{$base_link}preview/$row->ID'>$row->Thanhpho</a></td>
			    				<td style='text-align:right;width:100px '><a href='{$base_link}preview/$row->ID'>$row->SoluongSP</a></td>
                                <td style='text-align:right;width:100px'><a href='{$base_link}preview/$row->ID'>".number_format($tienhang)."</a></td>
			    				<td style='text-align:left'><a href='{$base_link}preview/$row->ID'>".$row->MaKho."</a></td>
			    				<td style='width:100px'><a href='{$base_link}preview/$row->ID'>$row->Ghichu</a></td>
			    			</tr>";
			    		$temp1 = date('mY',strtotime($row->Ngaydathang));
			    		if(!isset($dayofmonth_real[$temp1])){
			    			$dayofmonth_real[$temp1] = 1;
			    		}else{
			    			$dayofmonth_real[$temp1] = $dayofmonth_real[$temp1] +1;
			    		}
    				}
                    $total_tienhang = $total-$chietkhau;
                    $total_giatri = $total + $chiphi-$chietkhau;
    				echo "<tr>
    						<td colspan='4' style='text-align:center;'><p><span style='font-size:17px;font-weight:bold;'>Tổng cộng</span> <br>Tìm thấy <b>".number_format($find)."</b> đơn hàng theo yêu cầu .</p></td>
    						<td colspan='2' style='text-align:right'>".number_format($total_tienhang)."</td>
    						<td style='text-align:right'></td>
    						<td style='text-align:right'></td>
    					</tr>";
    			}else{
    				echo "<tr><td colspan='10'>Không tìm thấy đơn hàng.</td></tr>";
    			}
    			?>
    		</table>
            <?php 
                echo $nav;
            ?>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<?php 
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<div class="notify_current" style="display:none">
    <i class="fa fa-archive"></i><a class="numbernoti">1</a>
    <ul></ul>
</div>
<?php
    $bonus = "" ;
    $curdate = date('Y-m-d',time());
    $max = $this->db->query("select count(1) as SL from ttp_report_order where Status=3 and date(Ngaydathang)='$curdate' $bonus")->row();
    $slmax = $max ? $max->SL : 0 ;
?>
<script type="text/javascript">
    var baselink = $("#baselink_report").val();
    var resultmax = <?php echo $slmax ?>;
    var getcurrentorder = function(){
        $.ajax({
            url: baselink+"import/get_current_order_department",
            dataType: "html",
            type: "POST",
            data: "department=2&max="+resultmax,
            success: function(result){
                if(result!="false"){
                    var ob = jQuery.parseJSON(result);
                    content = ob.content;
                    for (var key in content) {
                        if (content.hasOwnProperty(key)) {
                            notifyNewOrder(content[key].user,content[key].image,content[key].title);
                        }
                    }
                    location.reload();
                }
            }
        });
    }
    setInterval(getcurrentorder,35000);
</script>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });
    
    $(".numbernoti").click(function(){
        $(this).parent().find('ul').toggle();
    });

	$("#show_thaotac").click(function(event){
        event.stopPropagation();
		$(this).parent('li').find('ul').toggle();
	});

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });

    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });
    
    var baselink = $("#baselink_report").val();
    var getnewsorder = function(){
        var data = $("#newsdescription").html();
        $.ajax({
            url: baselink+"import/get_news_order",
            dataType: "html",
            type: "POST",
            data: "",
            success: function(result){
                if(data!=result){
                    $("#newsdescription").html(result);
                    $("#newsdescription").slideDown();
                }
            }
        });

    }

    setInterval(getnewsorder,30000);
	

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"import_order/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="vung" || fieldname=="tinhthanh" || fieldname=="quanhuyen" || fieldname=='loaikhachhang' || fieldname=='trangthaidonhang' || fieldname=='doitacvanchuyen' || fieldname=='user' || fieldname=='khoxuathang'){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="tenkhachhang"){
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });

    function notifyNewOrder(title,image,message){
        notify(title, {
            body: message,
            icon: image,
            onclick: function(e) {},
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }
</script>
<style>
    .body_content .containner .import_orderlist .export_tools_kt:before{right:177px;}
</style>