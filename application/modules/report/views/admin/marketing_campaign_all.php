<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month          = array();
$dayofmonth     = array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";

$hidden = "";
if($this->user->IsAdmin == 1){
    $hidden = "hidden";
}
?>
<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
                <div class="btn_group">
                <button class="btn btn-default create <?=$hidden;?>"><i class="fa fa-calendar-o" aria-hidden="true"></i> Tạo mới</button>
                </div>
	    </div>
            <div class="block2">
                <div class="btn-group">
                    <select class="form-control" id="optSearch">
                        <option value="1" <?=($keyword_key == 1) ? 'selected' : '';?>>Theo tên chiến dịch</option>
                        <option value="2" <?=($keyword_key == 2) ? 'selected' : '';?>>Theo người phục trách</option>
                    </select>
                </div>
                <div class="btn-group">
                    <div class="input-group">
                        <input type="text" class="form-control search" autofocus="true" placeholder="Tìm kiếm..." value="<?php echo $keyword; ?>"/>
                        <span class="input-group-btn">
                            <button class="btn btn-default btnSearch" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                
                <div class="btn-group" style="padding-right:5px;">
                    
                    <a class="btn btn-success fillter">
                        <?php
                            if($fillter_by == 0){
                                echo "<i class='fa fa-check checked' rel='0'></i> Theo kế hoạch</a>";
                            }elseif($fillter_by == 1){
                                echo "<i class='fa fa-check checked' rel='1'></i> Theo thực tế</a>";
                            }else{
                                echo "<i class='fa fa-info-circle fa-check'></i> Lọc</a>";
                            }
                        ?>
                    <a class="btn btn-success dropdown-toggle fillter-down" data-toggle="dropdown">
                      <span class="fa fa-caret-down" title="Toggle dropdown menu"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="fill1" rel="0">Theo kế hoạch</a></li>
                        <li><a class="fill2" rel="1">Theo thực tế</a></li>
                    </ul>
              </div>
                <div class="block2">
                    <div id="reportrange" class="list_div">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                </div>
            </div>
        </div>
        <div class="table_data">
            <table id="table_data">
                <tr>
                    <th class="text-center ">STT</th>
                    <th class="col-xs-4">Tên chiến dịch</th>
                    <th class="text-center col-xs-2 hidden">Phân loại</th>
                    <th class="text-center col-xs-1">Bắt đầu kế hoạch</th>
                    <th class="text-center col-xs-1">Kết thúc kế hoạch</th>
                    <th class="text-center col-xs-1">Bắt đầu thực tế</th>
                    <th class="text-center col-xs-1">Kết thúc thực tế</th>
                    <th class="text-center col-xs-2 hidden">Website mục tiêu</th>
                    <th class="text-center">Ngân sách</th>
                    <th class="text-center">Thực chi</th>
                    <th class="text-center col-xs-1">% Ngân sách</th>
                    <th class="text-center col-xs-2" colspan="2">Tình trạng</th>
                    <th class="text-center">Xóa</th>
                </tr>
                <?php
                    $market_type = $this->define_model->get_order_status('type','campaign','position','asc');
                    $array_type = array();
                    foreach($market_type as $key=>$ite){
                        $code = (int)$ite->code;
                        $array_type[$code] = $ite->name;
                    }
                    ?>
                <?php
                    $i = 1;
                    foreach($data as $key1=>$row){
                        $market_id = $row->market_id;
                        $startParent = $this->db->query("select start as startParent,id as parentid from ttp_marketing_campaign_details where id = (select min(id) as min from ttp_marketing_campaign_details Where market_id = $market_id and parent=0)")->row();
                        $parentid = ($startParent) ? $startParent->parentid : 0;
                        $startAction = $this->db->query("select action_start as startAction from ttp_marketing_campaign_details where id = (select min(id) as min from ttp_marketing_campaign_details Where market_id = $market_id and parent= ".$parentid.")")->row();
                        if($startAction){ $startParent = $startParent->startParent; }else{ $startParent = date('Y-m-d H:i:s'); }
                        if($startAction){ $startAction = $startAction->startAction; }else{ $startAction = date('Y-m-d H:i:s'); }
                        $endParent = $this->db->query("select end as endParent,id as parentid from ttp_marketing_campaign_details where id = (select max(id) as max from ttp_marketing_campaign_details Where market_id = $market_id and parent=0)")->row();
                        $endparentid = ($endParent) ? $endParent->parentid : 0;
                        $endAction = $this->db->query("select action_end as endAction from ttp_marketing_campaign_details where id = (select max(id) as max from ttp_marketing_campaign_details Where market_id = $market_id and parent=$endparentid)")->row();
                        if($endParent){ $endParent = $endParent->endParent; }else{ $endParent = date('Y-m-d H:i:s'); }
                        if($endAction){ $endAction = $endAction->endAction; }else{ $endAction = date('Y-m-d H:i:s'); }
                        
                        $ns = $this->db->query("select money_need,id from ttp_marketing_campaign_details where market_id=$market_id and parent=0")->row();
                        $ns = ($ns) ? $ns->money_need : 0;
                        $tt = $this->db->query("select SUM(money_action) as tt from ttp_marketing_campaign_details where market_id=$market_id")->row()->tt;
                        
                        //GetName
                        $id = json_decode($row->owner);
                        $us = $this->db->query("SELECT FirstName,LastName FROM ttp_user WHERE RoleID = 3 and id = $id[0]")->row();
                ?>
                <tr>
                    <td class="text-center"><?=$i;?></td>
                    <td class="text-left editLink"><a class="onEdit" href="<?=$base_link;?>edit/<?=$row->market_id?>?_token=<?=substr(md5('EDIT'.$row->market_id),0,8);?>"><?=$row->market_name?></a><br/><small>+ Phụ trách: <?=$us->FirstName." ".$us->LastName?></small>
                    <td class="text-center"><?=date('d-M-y',strtotime($startParent));?></td>
                    <td class="text-center"><?=date('d-M-y',strtotime($endParent));?></td>
                    <td class="text-center"><?=($startAction) ? date('d-M-y',strtotime($startAction)) : date('d-M-y');?></td>
                    <td class="text-center"><?=($endAction) ? date('d-M-y',strtotime($endAction)): date('d-M-y');?></td>
                    <?php
                    
                        if(isset($tt) && isset($ns) && $ns != 0)
                        {
                            $percent = round($tt/($ns/100),1);
                            $percent = ($percent)? $percent." %" : 0;
                        }else{
                            $percent = "0";
                        }
                    ?>
                    <td class="text-right"><?=number_format($ns)?></td>
                    <td class="text-center"><?=number_format($tt);?></td>
                    <td class="text-center"><?=$percent;?></td>
                    <td class="text-center">
                        <?php
                        $market_status = $this->define_model->get_order_status('status','campaign','position','asc');
                        foreach($market_status as $key=>$status){
                            if($row->market_status == $status->code){
                                $_active =  $status->name;
                                echo $_active;
                            }
                        }
                        ?>
                    </td>
                    <td class="text-right"><?=$row->market_success;?> %</td>
                    <td class="text-center">
                        <?php 
                            if(in_array("0",array($row->market_status))){
                        ?>
                        <a class="btn deleteMarket" rel="<?=$row->market_id;?>" data-token="<?=substr(md5('REMOVE-MARKET'.$row->market_id),0,8);?>"><i class="fa fa-trash-o"></i></a>
                        <?php } else { ?>
                        <a class="btn disabled">--</a>
                        <?php } ?>
                    </td>
                </tr>
                <tr class="showInfo_<?=$row->market_id;?>">
                    
                </tr>
                <?php $i++;} ?>
                
            </table>
        </div>
        <div class="over_lay black">
            <div class="box_inner">
                <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
                <div class="block2_inner"></div>
            </div>
        </div>
        <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
  </div>
<style>
    .editLink {
        cursor: pointer;
    }
</style>
<script type="text/javascript">
    var baselink = $("#baselink_report").val();
    
    $('.editLink').click(function(){
        window.location = $(this).children("a").attr("href");
    });
    
    $('.search').keypress(function(e){
    if(e.which == 13) {
            $('.btnSearch').click();
            return false;
        }
    });
    
    $('.btnSearch').click(function(){
        var id = $('#optSearch').val();
        var keyword = $('.search').val();
        window.location = baselink + "marketing_campaign/all/?_search="+id+"|"+keyword;
    });
$(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            
            $.ajax({
                url: baselink+"marketing_campaign/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    console.log(result);
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
        
        $('.fill1').click(function(){
            var baselink = $("#baselink_report").val();
            var txt = $(this).text();
            var rel = $(this).attr("rel");
            $('.fillter').html("<i class='fa fa-check checked' rel=" + rel + "></i> " + txt);
            $.ajax({
                url: baselink+"marketing_campaign/set_fillterby",
                dataType: "html",
                type: "POST",
                data: "fillterRef="+rel,
                success: function(){
                    location.reload(true);
                }
            }); 
        });
        
        $('.fill2').click(function(){
            var baselink = $("#baselink_report").val();
            var txt = $(this).text();
            var rel = $(this).attr("rel");
            $('.fillter').html("<i class='fa fa-check checked' rel=" + rel + "></i> " + txt);
            $.ajax({
                url: baselink+"marketing_campaign/set_fillterby",
                dataType: "html",
                type: "POST",
                data: "fillterRef="+rel,
                success: function(){
                    location.reload(true);
                }
            });
        });
        
        $("#close_overlay").click(function(){
            $(".over_lay").removeClass('in');
            $(".over_lay").addClass('out');
            setTimeout(function(){
                $(".over_lay").hide();
                disablescrollsetup();
            },200);
	});
        
        $('.create').click(function(){
            var baselink = $("#baselink_report").val();
            window.location = baselink + 'marketing_campaign/addCampaign';
        });
        
        $('.deleteMarket').click(function(){
            var baselink = $("#baselink_report").val();
            var id = $(this).attr("rel");
            var token = $(this).attr("data-token");
            if (!confirm('Bạn muốn xóa chiến dịch?')) {
                return false;
            }else{
                $.ajax({
                    url: baselink+"marketing_campaign/deleteMarket",
                    dataType: "html",
                    type: "POST",
                    data: "id="+id+"&token="+token,
                    success: function(result){
                        if(result == "OK"){
                            location.reload(true);
                        }
                    }
                });
            }
        });
        
        function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}
    });
</script>
 