<div class="containner">
	<div class="row" style="background:#FFF;padding:10px;border:1px solid #eee;margin-bottom:10px;">
		<div class="col-xs-8"><h4>Tổng số đơn hàng</h4></div>
		<div class="col-xs-4 text-right"><h4><span class="label label-success"><?php echo number_format($total) ?></span></h4></div>
		<div class="col-xs-12"><hr style="margin:0px;padding:0px;"></div>
		<div class="col-xs-8"><h5>Đơn hàng thành công</h5></div>
		<div class="col-xs-4 text-right"><h5><span class="badge"><?php echo number_format($success) ?></span></h5></div>
		<div class="col-xs-8"><h5>Đơn hàng hủy</h5></div>
		<div class="col-xs-4 text-right"><h5><span class="badge"><?php echo number_format($false) ?></span></h5></div>
		<div class="col-xs-8"><h5>Đơn hàng đang xử lý</h5></div>
		<div class="col-xs-4 text-right"><h5><span class="badge"><?php echo number_format($total-$success-$false) ?></h5></span></div>
	</div>
	<div class="table-responsive">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Đơn hàng</th>
				<th>Khách hàng</th>
				<th class='lg-width'>Địa chỉ</th>
				<th>Giá trị</th>
				<th>Nộp tiền</th>
			</tr>
		<?php 
		$arr_money = array(
            1=>'<i class="fa fa-circle text-success" aria-hidden="true"></i> Đã nộp',
            0=>'<i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa nộp'
        );
		if(count($data)>0){
			$i = $start+1;
			foreach ($data as $row) {
				echo "<tr>";
				echo "<td>$i</td>";
				echo "<td>$row->MaDH</td>";
				echo "<td>$row->Name</td>";
				echo "<td>$row->AddressOrder</td>";
				echo "<td>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</td>";
				echo "<td><a class='status-button'>".$arr_money[$row->FinanceMoney]."</a></td>";
				echo "</tr>";
				$i++;
			}
		}
		?>
		</table>
	</div>
	<div><?php echo $nav ?></div>
</div>