<div class="sub_menu">
	<div class="find_record">
		<a style="padding:0px 5px" href="<?php echo ADMINPATH.'/pos/pos_sales/bill' ?>"><i class="fa fa-arrow-left" style="margin-right:5px"></i> Quay lại</a>
	</div>
</div>
<div class="containner">
	<div class="order_info">
		<div class="row">
			<div class="col-xs-8">
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Mã phiếu nhập</span>: <?php echo $data->POCode ?></div>
				<?php 
				$arr = array(
					5=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Thành công</a>',
					2=>'<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Nháp</a>'
				);
				?>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Trạng thái</span>: <?php echo $arr[$data->Status]; ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Thời gian</span>: <?php echo date('d/m/Y H:i',strtotime($data->Created)) ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Chi nhánh nhập</span>: <?php echo $data->WarehouseTitle ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Nhà cung cấp</span>: <?php echo $data->ProductionTitle ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Người tạo phiếu</span>: <?php echo $data->UserName ?></div>
			</div>
			<div class="col-xs-4">
				<textarea placeholder="Ghi chú ..."><?php echo $data->Note ?></textarea>
			</div>
		</div>
	</div>
	<div class="table_div" style="margin-top:20px">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã SKU</th>
				<th>Tên sản phẩm</th>
				<th>Đơn vị</th>
				<th>Số lượng</th>
				<th>Giá nhập</th>
				<th>Thành tiền</th>
			</tr>
			<tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_perchaseorder_details a,ttp_report_products b where a.ProductsID=b.ID and a.POID=$data->ID")->result();
				$total = 0;
				if(count($details)>0){
					$i=1;
					foreach ($details as $row) {
						$total = $total + ($row->PriceCurrency*$row->Amount);
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td>'.$row->MaSP.'</td>
						<td>'.$row->Title.'</td>
						<td>'.$row->Donvi.'</td>
						<td>'.$row->Amount.'</td>
						<td>'.number_format($row->PriceCurrency).'</td>
						<td>'.number_format($row->PriceCurrency*$row->Amount).'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
			<tr>
				<td colspan="6" class="text-right"><b>TỔNG CỘNG : </b></td>
				<td><b><?php echo number_format($total) ?></b></td>
			</tr>
			<tr>
				<td colspan="6" class="text-right"><b>CHIẾT KHẤU : </b></td>
				<td><b><?php echo number_format($data->Reduce) ?></b></td>
			</tr>
			<tr>
				<td colspan="6" class="text-right"><b>THÀNH TIỀN PHẢI TRẢ : </b></td>
				<td><b><?php echo number_format($data->TotalPrice - $data->Reduce) ?></b></td>
			</tr>
		</table>
	</div>
</div>
