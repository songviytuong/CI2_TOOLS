<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<form action="<?php echo $base_link ?>setsessionsearch" method="post">
					<a class="pull-left btn"><i class="fa fa-search" style="font-size:16px"></i></a><input type="text" value='<?php echo $keywords ?>' class="form-control" name="keywords" placeholder="Tìm nhà cung cấp / tiêu đề bảng cập nhật" style="width:300px;border:none;border-bottom:1px solid #e1e1e1;box-shadow:none;padding-left:0px;padding-right:0px;" />
				</form>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'products'; ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Gửi bảng cập nhật</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Ngày gửi</th>
					<th>Tiêu đề bảng cập nhật</th>
					<th>Mô tả bảng cập nhật</th>
					<th>Nhà cung cấp</th>
					<th>Thao tác</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>".date('d/m/Y',strtotime($row->Created))."</td>";
						echo "<td style='width:250px'>$row->Title</td>";
						echo "<td>$row->Note</td>";
						echo "<td style='width:150px'>$row->SupplierTitle</td>";
						echo "<td style='width:150px'>
								<a href='{$base_link}view_data_update_supplier/$row->ID'><i class='fa fa-pencil-square-o'></i> View </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete_data_update_supplier/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}else{
					echo "<tr><td colspan='6'>Không tìm thấy dữ liệu .</td></tr>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>