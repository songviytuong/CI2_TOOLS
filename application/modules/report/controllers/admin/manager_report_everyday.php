<?php 
class Manager_report_everyday extends Admin_Controller { 
 
 	public $user;
 	public $classname="manager_report_everyday";

    public function __construct() { 
        parent::__construct();
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;
	
	public function index(){
        $this->template->add_title('Report Every Day Tools');
		$this->template->write_view('content','admin/manager_report_everyday_home');
		$this->template->render();
	}

    public function set_month(){
        if(isset($_POST['month'])){
            $month = isset($_POST['month']) ? $_POST['month'] : '' ;
            $this->session->set_userdata("month_syn",$month);
        }
        echo "OK";
    }

    public function sync(){
        $day = isset($_POST['day']) ? $_POST['day'] : '' ;
        if($day!=''){
            $total_Order        = array();
            $total_success_Order= 0;
            $total_false_Order  = 0;
            $total_Customer     = array();
            $total_Sales        = 0;
            $total_Products     = 0;
            $total_new_order    = 0;
            $percent_new_order  = 0;
            $percent_prod_order = 0;
            $percent_success    = 0;
            $percent_false      = 0;
            $abs_order          = 0;
            $city_order         = array();
            $categories_order   = array();
            $area_order         = array();
            $products_order     = array();

            $total_Order_ss        = array();
            $total_success_Order_ss= 0;
            $total_false_Order_ss  = 0;
            $total_Customer_ss     = array();
            $total_Sales_ss        = 0;
            $total_Products_ss     = 0;
            $total_new_order_ss    = 0;
            $percent_new_order_ss  = 0;
            $percent_prod_order_ss = 0;
            $percent_success_ss    = 0;
            $percent_false_ss      = 0;
            $abs_order_ss          = 0;
            $city_order_ss         = array();
            $categories_order_ss   = array();
            $area_order_ss         = array();
            $products_order_ss     = array();
            
            $point_per_day      = 0;
            $point_per_order    = 0;
            $point_per_customer = 0;
            $point_per_new      = 0;
            $point_per_products = 0;

            $point_per_day_ss      = 0;
            $point_per_order_ss    = 0;
            $point_per_customer_ss = 0;
            $point_per_new_ss      = 0;
            $point_per_products_ss = 0;

            $result = $this->db->query("select a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.CityID,b.ProductsID,a.Ngaydathang,c.CategoriesID from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and c.ID=b.ProductsID and date(Ngaydathang)='$day' group by a.ID,b.ProductsID")->result();
            if(count($result)>0){
                foreach($result as $row){
                        if(!array_key_exists($row->ID,$total_Order)){
                            $total_Order[$row->ID] = "";
                            /* Total success and false order */
                            if($row->Status==0){
                                $total_success_Order++;
                            }
                            if($row->Status==1){
                                $total_false_Order++;
                            }

                            /* Total order group by city */
                            if(isset($city_order[$row->CityID])){
                                $city_order[$row->CityID]['sl'] += $row->Status==0 ? 1 : 0 ;
                                $city_order[$row->CityID]['total'] += $row->Status==0 ? $row->Total : 0 ;
                            }else{
                                $city_order[$row->CityID]['sl'] = $row->Status==0 ? 1 : 0 ;
                                $city_order[$row->CityID]['total'] = $row->Status==0 ? $row->Total : 0;
                            }

                            /* Total order group by categories */
                            if(isset($categories_order[$row->CategoriesID])){
                                $categories_order[$row->CategoriesID]['sl'] += $row->Status==0 ? 1 : 0 ;
                                $categories_order[$row->CategoriesID]['total'] += $row->Status==0 ? $row->Total : 0 ;
                            }else{
                                $categories_order[$row->CategoriesID]['sl'] = $row->Status==0 ? 1 : 0 ;
                                $categories_order[$row->CategoriesID]['total'] = $row->Status==0 ? $row->Total : 0;
                            }

                            /* Total by product */
                            if(array_key_exists($row->ProductsID,$products_order)){
                                $products_order[$row->ProductsID]['sl'] += $row->Status==0 ? 1 : 0 ;
                                $products_order[$row->ProductsID]['total'] += $row->Status==0 ? $row->Total : 0 ;
                            }else{
                                $products_order[$row->ProductsID]['sl'] = $row->Status==0 ? 1 : 0 ;
                                $products_order[$row->ProductsID]['total'] = $row->Status==0 ? $row->Total : 0;
                            }

                            /* Total customer */
                            if(!in_array($row->CustomerID,$total_Customer)){
                                $total_Customer[] = $row->CustomerID;
                            }

                            /* Total new order */
                            if($row->CustomerType==0){
                                $total_new_order++;
                            }

                            $point_per_products += $row->Status==0 ? 1 : 0 ;
                            $point_per_new += $row->Status==0 ? 1 : 0 ;
                            $point_per_customer += $row->Status==0 ? 1 : 0 ;
                            $point_per_order    += $row->Status==0 ? 1 : 0 ;
                            $point_per_day += $row->Status==0 ? $row->Total : 0 ;
                        }

                        if(!array_key_exists($row->ID,$total_Order_ss)){
                            $total_Order_ss[$row->ID] = "";
                        
                            /* Total success and false order */
                            if($row->Status==0){
                                $total_success_Order_ss++;
                            }
                            if($row->Status==1){
                                $total_false_Order_ss++;
                            }
                            
                            /* Total order group by city */
                            if(isset($city_order_ss[$row->CityID])){
                                $city_order_ss[$row->CityID]['sl'] += 1;
                                $city_order_ss[$row->CityID]['total'] += $row->Total;
                            }else{
                                $city_order_ss[$row->CityID]['sl'] = 1 ;
                                $city_order_ss[$row->CityID]['total'] = $row->Total;
                            }

                            /* Total order group by categories */
                            if(isset($categories_order_ss[$row->CategoriesID])){
                                $categories_order_ss[$row->CategoriesID]['sl'] += 1;
                                $categories_order_ss[$row->CategoriesID]['total'] += $row->Total;
                            }else{
                                $categories_order_ss[$row->CategoriesID]['sl'] = 1;
                                $categories_order_ss[$row->CategoriesID]['total'] = $row->Total;
                            }

                            /* Total by product */
                            if(array_key_exists($row->ProductsID,$products_order_ss)){
                                $products_order_ss[$row->ProductsID]['sl'] += 1 ;
                                $products_order_ss[$row->ProductsID]['total'] += $row->Total;
                            }else{
                                $products_order_ss[$row->ProductsID]['sl'] = 1 ;
                                $products_order_ss[$row->ProductsID]['total'] = $row->Total;
                            }

                            /* Total customer */
                            if(!in_array($row->CustomerID,$total_Customer_ss)){
                                $total_Customer_ss[] = $row->CustomerID;
                            }

                            /* Total new order */
                            if($row->CustomerType==0){
                                $total_new_order_ss++;
                            }

                            $point_per_products_ss += 1 ;
                            $point_per_new_ss += 1 ;
                            $point_per_customer_ss += 1 ;
                            $point_per_order_ss += 1 ;
                            $point_per_day_ss += $row->Total;
                        }

                    if($row->Status==0){
                        /* Total products */
                        $total_Products+= $row->SoluongSP;
                    }else{
                        /* Total products */
                        $total_Products_ss+= $row->SoluongSP;
                    }
                }
                $total_Order = count($total_Order);
                $percent_prod_order = round($total_Products/$total_Order,1);
                $total_Order_ss = count($total_Order_ss);
                $percent_prod_order_ss = round($total_Products_ss/$total_Order_ss,1);
                $abs_order = round($total_Sales/$total_Order);
                $percent_success = round($total_success_Order/($total_Order/100));
                $percent_false = 100-$percent_success;
                $percent_new_order = round($total_new_order/(count($total_Customer)/100),2);
            }
            $json = array(
                'Total'             =>$point_per_day_ss,
                'Total_ss'          =>$point_per_day,
                'Products'          =>$point_per_products_ss,
                'Products_ss'       =>$point_per_products,
                'Order'             =>$total_Order_ss,
                'Order_ss'          =>$total_Order,
                'Customer'          =>count($total_Customer_ss),
                'Customer_ss'       =>count($total_Customer),
                'NewCustomer'       =>$total_new_order_ss,
                'NewCustomer_ss'    =>$total_new_order,
                'OrderSuccess'      =>$total_success_Order_ss,
                'OrderSuccess_ss'   =>$total_success_Order,
                'OrderFalse'      =>$total_false_Order_ss,
                'OrderFalse_ss'   =>$total_false_Order,
                'CityOrder'         =>$city_order_ss,
                'CityOrder_ss'      =>$city_order,
                'CategoriesOrder'   =>$categories_order_ss,
                'CategoriesOrder_ss'=>$categories_order,
                'ProductsOrder'     =>$products_order_ss,
                'ProductsOrder_ss'  =>$products_order
            );

            $data = array(
                'Data'      =>json_encode($json),
                'ReportDay' =>$day,
                'LastEdited'=>date('Y-m-d H:i:s')
            );
            $check = $this->db->query("select ID from ttp_report_everday where ReportDay='$day'")->row();
            if($check){
                $this->db->where("ID",$check->ID);
                $this->db->update("ttp_report_everday",$data);
            }else{
                $data['ReportDay'] = $day;
                $data['Created']=date('Y-m-d H:i:s');
                $this->db->insert("ttp_report_everday",$data);
            }
        }
    }
}