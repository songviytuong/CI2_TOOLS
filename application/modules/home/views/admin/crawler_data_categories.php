<div class="">
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" id="formdata" action="<?php echo $base_link ."selectdata" ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="tab1 tabcontent">
                    <div class="x_title">
                        <h2>CHOOSE CATEGORIES</h2>
                        <button type="submit" class="btn btn-success" style="float:right">Next step</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                            <div class="x_content">
                                <table>
                                    <thead>
                                        <tr class="headings">
                                            <th></th>
                                            <th>STT</th>
                                            <th>Categories from site <?php echo $site ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        for ($i=1; $i < 11; $i++) { 
                                            echo '<tr>
                                                    <td><input type="checkbox" name="'.$i.'" /></td>
                                                    <td>'.$i.'</td>
                                                    <td>Name Categories '.$i.'</td>
                                                </tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
<style>
    table{width:100%;}
    table tr th{padding:5px 10px;background: #EEE;border:1px solid #E1e1e1;}
    table tr td{padding:5px 10px;border:1px solid #E1e1e1;background: #FFF}
    table tr td:nth-child(1){width:30px;}
    table tr td:nth-child(2){width:45px;text-align:center;}
</style>