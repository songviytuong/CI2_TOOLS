<?php
class Partner_supplier extends Admin_Controller {

    public $user;
    public $classname="partner_supplier";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/supplier_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function index(){
        $this->request_available();
    }

    public function supplier_report_products(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and a.SupplierID in(".implode(',',$arr_supp).")";
        $bonus = $supplier;
        $branch = isset($_GET['branch']) ? (int)$_GET['branch'] : 0 ;
        $bonus.= $branch>0 ? " and c.BranchID=$branch" : "" ;
        $result = $this->db->query("select b.ID,a.Title,c.Price,c.TimeReciver,c.ProductsID,c.Amount,b.MaDH,c.NCC,b.CityID,d.AreaID,c.BranchID,a.CategoriesID,a.TrademarkID,e.Title as Brand from ttp_report_products a,ttp_report_order b,ttp_report_order_send_supplier c,ttp_report_city d,ttp_report_trademark e where a.TrademarkID=e.ID and b.CityID=d.ID and a.ID=c.ProductsID and b.ID=c.OrderID and date(c.TimeReciver)>='$startday' and date(c.TimeReciver)<='$stopday' $bonus")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'supplier_list'=>$arr_supp,
            'branchid'    => $branch,
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $this->template->add_title('Overview');
        $this->template->write_view('content','admin/import_supplier_report_products',$data);
        $this->template->render();
    }

    public function supplier_report_overview(){
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and a.SupplierID in(".implode(',',$arr_supp).")";
        $bonus = $supplier;
        $payment = isset($_GET['payment']) ? (int)$_GET['payment'] : -1 ;
        $bonus.= $payment>-1 ? " and c.NCC=$payment" : "" ;
        $result = $this->db->query("select b.ID,a.Title,c.Price,c.TimeReciver,c.ProductsID,c.Amount,b.MaDH,c.NCC,b.CityID,d.AreaID,c.BranchID from ttp_report_products a,ttp_report_order b,ttp_report_order_send_supplier c,ttp_report_city d where b.CityID=d.ID and a.ID=c.ProductsID and b.ID=c.OrderID and date(c.TimeReciver)>='$startday' and date(c.TimeReciver)<='$stopday' $bonus")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'supplier_list'=>$arr_supp,
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $this->template->add_title('Overview');
        $this->template->write_view('content','admin/import_supplier_report_overview',$data);
        $this->template->render();
    }

    public function request_available(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Supplier | Request available');

        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and c.SupplierID in(".implode(',',$arr_supp).")";
        $result = $this->db->query("select a.ID,a.MaDH,b.Amount,b.ProductsID,b.ID as DetailsID,c.Donvi,c.Title,b.TimeRequest from ttp_report_order a,ttp_report_order_request_available b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and b.Status=0 and date(b.TimeRequest)>='$startday' and date(b.TimeRequest)<='$stopday' $supplier order by a.ID ASC")->result();

        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $view = "admin/import_supplier_home";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function payment_history(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and a.SupplierID in(".implode(',',$arr_supp).")";
        $bonus = $supplier;

        $result = $this->db->query("select a.ID,a.BarcodeClient,a.Title,a.Donvi,c.Price,c.UCCPaid,c.Amount,b.MaDH,c.UCC,c.NCC from ttp_report_products a,ttp_report_order b,ttp_report_order_send_supplier c where a.ID=c.ProductsID and b.ID=c.OrderID and date(c.UCCPaid)>='$startday' and date(c.UCCPaid)<='$stopday' and c.UCC=2 $bonus")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $this->template->add_title('Payment');
        $this->template->write_view('content','admin/import_supplier_payment_history',$data);
        $this->template->render();
    }

    public function download_payment_history(){
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and a.SupplierID in(".implode(',',$arr_supp).")";
        $bonus = $supplier;

        $result = $this->db->query("select a.ID,a.BarcodeClient,a.Title,a.Donvi,c.Price,c.UCCPaid,c.Amount,b.MaDH,c.UCC,c.NCC from ttp_report_products a,ttp_report_order b,ttp_report_order_send_supplier c where a.ID=c.ProductsID and b.ID=c.OrderID and date(c.UCCPaid)>='$startday' and date(c.UCCPaid)<='$stopday' and c.UCC=2 $bonus")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'ID')
                        ->setCellValue('B1', 'Mã sản phẩm NCC')
                        ->setCellValue('C1', 'Tên sản phẩm')
                        ->setCellValue('D1', 'DVT')
                        ->setCellValue('E1', 'Mã đơn hàng')
                        ->setCellValue('F1', 'Ngày Ucancook thanh toán')
                        ->setCellValue('G1', 'Số lượng')
                        ->setCellValue('H1', 'Đơn giá')
                        ->setCellValue('I1', 'Thành tiền')
                        ->setCellValue('J1', 'NCC kiểm tra');

            $i=2;
            $ncc_array = $this->lib->get_config_define("payment","ncc",0,"code");
            foreach($result as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->ID)
                            ->setCellValue('B'.$i, $row->BarcodeClient)
                            ->setCellValue('C'.$i, $row->Title)
                            ->setCellValue('D'.$i, $row->Donvi)
                            ->setCellValue('E'.$i, $row->MaDH)
                            ->setCellValue('F'.$i, date('d/m/Y',strtotime($row->UCCPaid)))
                            ->setCellValue('G'.$i, $row->Amount)
                            ->setCellValue('H'.$i, $row->Price)
                            ->setCellValue('I'.$i, $row->Amount*$row->Price)
                            ->setCellValue('J'.$i, $ncc_array[$row->NCC]);
                    $i++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->setTitle("payment_history");

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="payment_history.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data to export .";
        }
    }

    public function payment_home(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and a.SupplierID in(".implode(',',$arr_supp).")";

        $bonus = $supplier;
        $NCC = isset($_GET['NCC']) ? $_GET['NCC'] : -1 ;
        $bonus .= $NCC>-1 ? " and c.NCC=$NCC" : "" ;
        $BranchID = isset($_GET['BranchID']) ? $_GET['BranchID'] : 0 ;
        $bonus .= $BranchID>0 ? " and c.BranchID=$BranchID" : "" ;
        $CategoriesID = isset($_GET['CategoriesID']) ? $_GET['CategoriesID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? $_GET['TrademarkID'] : 0 ;
        $bonus .= $TrademarkID>0 ? " and a.TrademarkID=$TrademarkID" : "" ;

        $result = $this->db->query("select a.CategoriesID,a.ID,a.BarcodeClient,a.Title,a.Donvi,c.Price,c.TimeReciver,c.Amount,b.MaDH,c.UCC,c.NCC,c.ID as DetailsID,c.DeliveryCode,c.Reduce from ttp_report_products a,ttp_report_order b,ttp_report_order_send_supplier c where a.ID=c.ProductsID and b.ID=c.OrderID and date(c.TimeReciver)>='$startday' and date(c.TimeReciver)<='$stopday' and c.WarehouseID=0 $bonus")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $this->template->add_title('Payment');
        $this->template->write_view('content','admin/import_supplier_payment_home',$data);
        $this->template->render();
    }

    public function change_deliverycode(){
      $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
      $Code = isset($_POST['Code']) ? strip_tags($_POST['Code']) : '' ;
      $this->db->query("update ttp_report_order_send_supplier set DeliveryCode='$Code' where ID=$ID");
      echo "true";
    }

    public function ncc_payment_active(){
        if($this->user->IsAdmin==1){
            $data = isset($_POST['data']) ? json_decode($_POST['data']) : array();
            if(count($data)>0){
                $ncc_array = $this->lib->get_config_define("payment","ncc",0,"code");
                foreach($data as $row){
                    $id = isset($row->ID) ? $row->ID : 0 ;
                    $NCC = isset($row->NCC) ? $row->NCC : 0 ;
                    $NCC = array_search($NCC, $ncc_array);
                    if($id>0 && $NCC>0){
                        $Bonus = "";
                        if($NCC==1){
                            $Status=0;
                        }
                        if($NCC==2){
                            $Status=1;
                        }
                        if($NCC==3){
                            $Status=2;
                            $Bonus = ",NCCRecived='".date('Y-m-d H:i:s')."'";
                        }
                        $this->db->query("update ttp_report_order_send_supplier set NCC=$NCC,NCCLastEdited='".date('Y-m-d H:i:s')."'$Bonus where NCC=$Status and ID=$id");
                    }
                }
            }
        }
        echo "true";
    }

    public function ucc_payment_active(){
        if($this->user->IsAdmin==1){
            $data = isset($_POST['data']) ? json_decode($_POST['data']) : array();
            if(count($data)>0){
                $ncc_array = $this->lib->get_config_define("payment","ucc",0,"code");
                foreach($data as $row){
                    $id = isset($row->ID) ? $row->ID : 0 ;
                    $NCC = isset($row->NCC) ? $row->NCC : 0 ;
                    $NCC = array_search($NCC, $ncc_array);
                    if($id>0 && $NCC>0){
                        $Bonus = "";
                        if($NCC==1){
                            $Status=0;
                        }
                        if($NCC==2){
                            $Status=1;
                            $Bonus = ",NCC=2,UCCPaid='".date('Y-m-d H:i:s')."'";
                        }
                        $this->db->query("update ttp_report_order_send_supplier set UCC=$NCC,UCCLastEdited='".date('Y-m-d H:i:s')."'$Bonus where UCC=$Status and ID=$id");
                    }
                }
            }
        }
        echo "true";
    }

    public function download_payment_home(){
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and a.SupplierID in(".implode(',',$arr_supp).")";

        $bonus = $supplier;
        $NCC = isset($_GET['NCC']) ? $_GET['NCC'] : -1 ;
        $bonus .= $NCC>-1 ? " and c.NCC=$NCC" : "" ;
        $BranchID = isset($_GET['BranchID']) ? $_GET['BranchID'] : 0 ;
        $bonus .= $BranchID>0 ? " and c.BranchID=$BranchID" : "" ;
        $CategoriesID = isset($_GET['CategoriesID']) ? $_GET['CategoriesID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? $_GET['TrademarkID'] : 0 ;
        $bonus .= $TrademarkID>0 ? " and a.TrademarkID=$TrademarkID" : "" ;

        $result = $this->db->query("select a.CategoriesID,c.ID,a.BarcodeClient,a.Title,a.Donvi,c.Price,c.TimeReciver,c.Amount,b.MaDH,c.UCC,c.NCC,c.ID as DetailsID,c.DeliveryCode from ttp_report_products a,ttp_report_order b,ttp_report_order_send_supplier c where a.ID=c.ProductsID and b.ID=c.OrderID and date(c.TimeReciver)>='$startday' and date(c.TimeReciver)<='$stopday' $bonus")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'ID')
                        ->setCellValue('B1', 'Mã sản phẩm NCC')
                        ->setCellValue('C1', 'Tên sản phẩm')
                        ->setCellValue('D1', 'DVT')
                        ->setCellValue('E1', 'Mã đơn hàng')
                        ->setCellValue('F1', 'Ngày lấy hàng')
                        ->setCellValue('G1', 'Số lượng')
                        ->setCellValue('H1', 'Đơn giá')
                        ->setCellValue('I1', 'Thành tiền')
                        ->setCellValue('J1', 'UCC kiểm tra')
                        ->setCellValue('K1', 'NCC kiểm tra')
                        ->setCellValue('L1', 'Mã phiếu giao hàng NCC');

            $i=2;
            $ucc_array = $this->lib->get_config_define("payment","ucc");
            $ncc_array = $this->lib->get_config_define("payment","ncc",0,"code");
            foreach($result as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->ID)
                            ->setCellValue('B'.$i, $row->BarcodeClient)
                            ->setCellValue('C'.$i, $row->Title)
                            ->setCellValue('D'.$i, $row->Donvi)
                            ->setCellValue('E'.$i, $row->MaDH)
                            ->setCellValue('F'.$i, date('d/m/Y',strtotime($row->TimeReciver)))
                            ->setCellValue('G'.$i, $row->Amount)
                            ->setCellValue('H'.$i, $row->Price)
                            ->setCellValue('I'.$i, $row->Amount*$row->Price)
                            ->setCellValue('J'.$i, $ucc_array[$row->UCC])
                            ->setCellValue('K'.$i, $ncc_array[$row->NCC])
                            ->setCellValue('L'.$i, $row->DeliveryCode);
                    $i++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->setTitle("payment_supplier");

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="payment_supplier.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data to export .";
        }
    }

    public function load_ncc_payment($ob=0){
        $data = array('ob'=>$ob);
        $this->load->view("admin/import_supplier_load_ncc_payment",$data);
    }

    public function set_day(){
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("supplier_startday",$startday);
                    $this->session->set_userdata("supplier_stopday",$stopday);
                }
            }
        }
        echo 'OK';
    }

    public function products_home(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $keywords = $this->session->userdata("sessionsearch_update");
        if($this->user->IsAdmin==1){
            $nav = $this->db->query("select count(1) as nav from ttp_report_products a,ttp_report_production b where a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%')")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.BarcodeClient,a.Title,a.Donvi,a.RootPrice,a.PriceSuggest,b.Title as SupplierTitle from ttp_report_products a,ttp_report_production b where a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%') order by a.ID DESC $limit_str")->result();
        }else{
            $nav = $this->db->query("select count(1) as nav from ttp_report_products a,ttp_report_production b,ttp_user_supplier c where b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%')")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.BarcodeClient,a.Title,a.Donvi,a.RootPrice,a.PriceSuggest,b.Title as SupplierTitle from ttp_report_products a,ttp_report_production b,ttp_user_supplier c where b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%') order by a.ID DESC $limit_str")->result();
        }
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/partner_supplier/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'keywords'  =>  $keywords,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/partner_supplier/products_home',5,$nav,$this->limit)
        );
        $this->template->add_title('Supplier | Products information');
        $this->template->write_view('content','admin/import_supplier_products_home',$data);
        $this->template->render();
    }

    public function update_products_home(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $keywords = $this->session->userdata("sessionsearch_update");
        if($this->user->IsAdmin==1){
            $nav = $this->db->query("select count(1) as nav from ttp_report_products_supplier_update a,ttp_report_production b where a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%')")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.*,b.Title as SupplierTitle from ttp_report_products_supplier_update a,ttp_report_production b where a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%') order by a.ID DESC $limit_str")->result();
        }else{
            $nav = $this->db->query("select count(1) as nav from ttp_report_products_supplier_update a,ttp_report_production b,ttp_user_supplier c where b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%')")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.*,b.Title as SupplierTitle from ttp_report_products_supplier_update a,ttp_report_production b,ttp_user_supplier c where b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%') order by a.ID DESC $limit_str")->result();
        }
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/partner_supplier/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'keywords'  =>  $keywords,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/partner_supplier/update_products_home',5,$nav,$this->limit)
        );
        $this->template->add_title('Supplier | Update products information');
        $this->template->write_view('content','admin/import_supplier_update_products_home',$data);
        $this->template->render();
    }

    public function view_data_update_supplier($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Supplier | Update products');
        if($this->user->IsAdmin==1){
            $result = $this->db->query("select a.*,b.Title as SupplierTitle from ttp_report_products_supplier_update a,ttp_report_production b where a.SupplierID=b.ID and a.ID=$id")->row();
        }else{
            $result = $this->db->query("select a.*,b.Title as SupplierTitle from ttp_report_products_supplier_update a,ttp_report_production b,ttp_user_supplier c where a.SupplierID=b.ID and b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.ID=$id")->row();
        }
        if($result){
            $data = array(
                'data'      => $result,
                'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
            );
            $view = "admin/import_supplier_view_update_supplier";
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }
    }

    public function delete_data_update_supplier($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if($this->user->IsAdmin==1){
            $result = $this->db->query("select a.ID from ttp_report_products_supplier_update a,ttp_report_production b where a.SupplierID=b.ID and a.ID=$id")->row();
        }else{
            $result = $this->db->query("select a.ID from ttp_report_products_supplier_update a,ttp_report_production b,ttp_user_supplier c where a.SupplierID=b.ID and b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.ID=$id")->row();
        }
        if($result){
            $this->db->query("delete from ttp_report_products_supplier_update where ID=$id");
            $this->db->query("delete from ttp_report_products_supplier_update_details where UpdateID=$id");
        }
        redirect(ADMINPATH."/report/partner_supplier/update_products_home");
    }

    public function export_products_supplier(){
        $keywords = $this->session->userdata("sessionsearch_update");
        if($this->user->IsAdmin==1){
            $object = $this->db->query("select a.BarcodeClient,a.Title,a.Donvi,a.RootPrice,a.PriceSuggest,b.Title as SupplierTitle from ttp_report_products a,ttp_report_production b where a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%') order by a.ID DESC")->result();
        }else{
            $object = $this->db->query("select a.BarcodeClient,a.Title,a.Donvi,a.RootPrice,a.PriceSuggest,b.Title as SupplierTitle from ttp_report_products a,ttp_report_production b,ttp_user_supplier c where b.ID=c.SupplierID and c.UserID=".$this->user->ID." and a.SupplierID=b.ID and (a.Title like '%$keywords%' or b.Title like '%$keywords%') order by a.ID DESC")->result();
        }
        if(count($object)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Mã sản phẩm NCC')
                        ->setCellValue('B1', 'Tên sản phẩm')
                        ->setCellValue('C1', 'DVT')
                        ->setCellValue('D1', 'Giá bán cho UCC')
                        ->setCellValue('E1', 'Giá bán lẻ đề nghị')
                        ->setCellValue('F1', 'Nhà cung cấp');
            $i=2;
            $arr = array(0=>'Cập nhật',1=>'Tạo mới');
            foreach($object as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->BarcodeClient)
                            ->setCellValue('B'.$i, $row->Title)
                            ->setCellValue('C'.$i, $row->Donvi)
                            ->setCellValue('D'.$i, $row->RootPrice)
                            ->setCellValue('E'.$i, $row->PriceSuggest)
                            ->setCellValue('F'.$i, $row->SupplierTitle);
                    $i++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->setTitle("supplier_products");

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="supplier_products.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data to export .";
        }
    }

    public function download_update_products($id=0){
        if($this->user->IsAdmin==1){
            $result = $this->db->query("select b.* from ttp_report_products_supplier_update a,ttp_report_products_supplier_update_details b where a.ID=b.UpdateID and a.ID=$id")->result();
        }else{
            $result = $this->db->query("select b.* from ttp_report_products_supplier_update a,ttp_report_products_supplier_update_details b,ttp_user_supplier c where a.ID=b.UpdateID and a.SupplierID=c.SupplierID and c.UserID=".$this->user->ID." and a.ID=$id")->result();
        }
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Mã sản phẩm')
                        ->setCellValue('B1', 'Tên sản phẩm')
                        ->setCellValue('C1', 'DVT')
                        ->setCellValue('D1', 'Giá bán lẻ (bao gồm VAT)')
                        ->setCellValue('E1', 'Giá bán theo đơn vị chuẩn')
                        ->setCellValue('F1', 'Giá bán cho UCC')
                        ->setCellValue('G1', 'Tình trạng');
            $i=2;
            $arr = array(0=>'Cập nhật',1=>'Tạo mới');
            foreach($result as $row){
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, $row->MaSP)
                            ->setCellValue('B'.$i, $row->Title)
                            ->setCellValue('C'.$i, $row->Unit)
                            ->setCellValue('D'.$i, $row->Price)
                            ->setCellValue('E'.$i, $row->BasePrice)
                            ->setCellValue('F'.$i, $row->RootPrice)
                            ->setCellValue('G'.$i, $arr[$row->Status]);
                    $i++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->setTitle("Supplier_update_products");

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="supplier_update_products.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data to export .";
        }
    }

    public function setsessionsearch(){
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $this->session->set_userdata("sessionsearch_update",$keywords);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function save_data_update_supplier(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $agree = isset($_POST['agree']) ? $_POST['agree'] : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $TotalProducts = isset($_POST['TotalProducts']) ? $_POST['TotalProducts'] : 0 ;
        $SupplierID = isset($_POST['SupplierID']) ? $_POST['SupplierID'] : 0 ;
        $InUCC = isset($_POST['InUCC']) ? $_POST['InUCC'] : 0 ;
        $NotInUCC = isset($_POST['NotInUCC']) ? $_POST['NotInUCC'] : 0 ;
        $RemainUCC = isset($_POST['RemainUCC']) ? $_POST['RemainUCC'] : 0 ;
        $RemainStatus = isset($_POST['RemainStatus']) ? $_POST['RemainStatus'] : 1 ;
        $result = isset($_POST['data']) ? json_decode($_POST['data']) : array() ;
        $supplier_list = $this->db->query("select b.ID from ttp_user_supplier b where b.SupplierID=$SupplierID and b.UserID=".$this->user->ID)->row();
        if(!$supplier_list){
            redirect(ADMINPATH.'/report/partner_supplier/products');
        }
        if($agree=='on' && $Title!='' && $Note!='' && count($result)>0 && $SupplierID>0){
            $data = array(
                'Title'         =>$Title,
                'Note'          =>$Note,
                'TotalProducts' =>$TotalProducts,
                'InUCC'         =>$InUCC,
                'NotInUCC'      =>$NotInUCC,
                'RemainUCC'     =>$RemainUCC,
                'RemainStatus'  =>$RemainStatus,
                'SupplierID'    =>$SupplierID,
                'Created'       =>date('Y-m-d H:i:s'),
                'UserID'        =>$this->user->ID
            );
            $this->db->insert("ttp_report_products_supplier_update",$data);
            $id = $this->db->insert_id();
            $sql = array();
            foreach($result as $row){
                $sql[] = "($id,'".$row[0]."','".$row[1]."','".$row[2]."','".$row[3]."','".$row[4]."','".$row[5]."','".$row[6]."')";
            }
            $sql = "insert into ttp_report_products_supplier_update_details(UpdateID,MaSP,Title,Unit,Price,BasePrice,RootPrice,Status) values".implode(',', $sql);
            $this->db->query($sql);
            redirect(ADMINPATH.'/report/partner_supplier/update_products_home');
        }else{
            redirect(ADMINPATH.'/report/partner_supplier/products');
        }
    }

    public function products(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Supplier | Update products');
        $data = array(
            'data'      => array(),
            'base_link' =>  base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $view = "admin/import_supplier_products";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function upload_import_payment($ob=0){
        $file = isset($_FILES['file']['tmp_name']) ? $_FILES['file']['tmp_name'] : "";
        $name = isset($_FILES['file']['name']) ? $_FILES['file']['name'] : "";
        if($name!=''){
            $ext = explode('.',$name);
            $ext = $ext[count($ext)-1];
            if($ext!='xls' && $ext!='xlsx'){
                $this->load->view("admin/import_supplier_products_preview",array('error'=>'Định dạng file không được chấp nhận. Vui lòng chọn file theo đúng định dạng quy định.'));
                return;
            }
        }
        if(file_exists($file)){
            $result = array();
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow         = $worksheet->getHighestRow();
                $highestColumn      = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 1; $row <= $highestRow; $row ++) {
                    $data = array();
                    for($key=0;$key<=$highestColumnIndex;$key++){
                        $value = trim($worksheet->getCellByColumnAndRow($key, $row)->getValue());
                        $data[] = $value;
                    }
                    if(count($data)>5){
                        $result[] = $data;
                    }
                }
            }
            $this->load->view("admin/import_supplier_payment_preview",array('ob'=>$ob,'data'=>$result,'filename'=>$name));
        }
    }

    public function upload_import(){
        $file = isset($_FILES['file']['tmp_name']) ? $_FILES['file']['tmp_name'] : "";
        $name = isset($_FILES['file']['name']) ? $_FILES['file']['name'] : "";
        $supplier = isset($_POST['supplier']) ? $_POST['supplier'] : 0;
        if($name!=''){
            $ext = explode('.',$name);
            $ext = $ext[count($ext)-1];
            if($ext!='xls' && $ext!='xlsx'){
                $this->load->view("admin/import_supplier_products_preview",array('error'=>'Định dạng file không được chấp nhận. Vui lòng chọn file theo đúng định dạng quy định.'));
                return;
            }
        }
        if(file_exists($file)){
            $result = array();
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow         = $worksheet->getHighestRow();
                $highestColumn      = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 2; $row <= $highestRow; $row ++) {
                    $data = array();
                    for($key=0;$key<=$highestColumnIndex;$key++){
                        $value = trim($worksheet->getCellByColumnAndRow($key, $row)->getValue());
                        $data[] = $value;
                    }
                    if(count($data)>5){
                        $result[] = $data;
                    }
                }
            }
            $this->load->view("admin/import_supplier_products_preview",array('data'=>$result,'filename'=>$name,'supplier'=>$supplier));
        }
    }

    public function create_shipment_default($ProductsID=0){
        $data = array(
            'UserID'    => $this->user->ID,
            'ShipmentCode'=>'Default',
            'ProductsID'=>$ProductsID,
            'DateProduction'=>date('Y-m-d'),
            'DateExpiration'=>date('Y-m-d',time()+24*3600*365),
            'Created'   =>date('Y-m-d H:i:s')
        );
        $this->db->insert("ttp_report_shipment",$data);
        return $this->db->insert_id();
    }

    public function accept_available(){
        $OrderID = isset($_POST['OrderID']) ? (int)$_POST['OrderID'] : 0 ;
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $Amount = isset($_POST['Amount']) ? (float)$_POST['Amount'] : 0 ;
        $order = $this->db->query("select KhoID,UserID,MaDH from ttp_report_order where ID=$OrderID")->row();
        if($order){
            if($Amount>0){
                $shipmentdefault = $this->db->query("select ID from ttp_report_shipment where ProductsID=$ProductsID")->row();
                if(!$shipmentdefault){
                    $ShipmentID = $this->create_shipment_default($ProductsID);
                }else{
                    $ShipmentID = $shipmentdefault->ID;
                }
                $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                if($check){
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$order->KhoID and LastEdited=1");
                    if($check->DateInventory==date('Y-m-d')){
                        $this->db->query("update ttp_report_inventory set Available=Available+$Amount,LastEdited=1 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$order->KhoID");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check->OnHand,
                            'Available'     =>$check->Available+$Amount,
                            'WarehouseID'   =>$order->KhoID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"Available",
                            'Amount'        =>$Amount,
                            'Action'        =>'update',
                            'Method'        =>'accept available from supplier '.$OrderID
                        );
                        $this->write_log_inventory($data_log);
                    }else{
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$check->OnHand,$check->Available+$Amount,$order->KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check->OnHand,
                            'Available'     =>$check->Available+$Amount,
                            'WarehouseID'   =>$order->KhoID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"Available",
                            'Amount'        =>$Amount,
                            'Action'        =>'update',
                            'Method'        =>'accept available from supplier '.$OrderID
                        );
                        $this->write_log_inventory($data_log);
                    }
                }else{
                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,0,$Amount,$order->KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                    $data_log = array(
                        'ProductsID'    =>$ProductsID,
                        'OnHand'        =>0,
                        'Available'     =>$Amount,
                        'WarehouseID'   =>$order->KhoID,
                        'ShipmentID'    =>$ShipmentID,
                        'DateInventory' =>date('Y-m-d'),
                        'Type'          =>"Available",
                        'Amount'        =>$Amount,
                        'Action'        =>'update',
                        'Method'        =>'accept available from supplier '.$OrderID
                    );
                    $this->write_log_inventory($data_log);
                }
                $this->db->query("update ttp_report_order_request_available set Status=1,Response=$Amount,UserAccept=".$this->user->ID.",TimeAccept='".date('Y-m-d H:i:s')."' where ID=$ID");
                $products = $this->db->query("select Title,Donvi from ttp_report_products where ID=$ProductsID")->row();
                if($products){
                    $data_request = file_get_contents("log/supplier/user_request_available.txt");
                    $data_request = $data_request!='' ? json_decode($data_request,true) : array();
                    if(isset($data_request[$order->UserID])){
                        $data_request[$order->UserID] .= "$products->Title đã có đủ $Amount $products->Donvi .";
                    }else{
                        $data_request[$order->UserID] = "$products->Title đã có đủ $Amount $products->Donvi .";
                    }
                    file_put_contents("log/supplier/user_request_available.txt",json_encode($data_request));
                }
                echo "true";
                return;
            }
        }
        echo "false";
    }

    public function skip_available(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $OrderID = isset($_POST['OrderID']) ? (int)$_POST['OrderID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $Amount = isset($_POST['Amount']) ? (float)$_POST['Amount'] : 0 ;
        $AmountRes = isset($_POST['AmountRes']) ? (float)$_POST['AmountRes'] : 0 ;
        $order = $this->db->query("select KhoID from ttp_report_order where ID=$OrderID")->row();
        if($order){
            if($AmountRes>0){
                $shipmentdefault = $this->db->query("select ID from ttp_report_shipment where ProductsID=$ProductsID")->row();
                if(!$shipmentdefault){
                    $ShipmentID = $this->create_shipment_default($ProductsID);
                }else{
                    $ShipmentID = $shipmentdefault->ID;
                }
                $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                if($check){
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$order->KhoID and LastEdited=1");
                    if($check->DateInventory==date('Y-m-d')){
                        $this->db->query("update ttp_report_inventory set Available=Available+$AmountRes,LastEdited=1 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$order->KhoID");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check->OnHand,
                            'Available'     =>$check->Available+$AmountRes,
                            'WarehouseID'   =>$order->KhoID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"Available",
                            'Amount'        =>$AmountRes,
                            'Action'        =>'update',
                            'Method'        =>'accept available from supplier '.$OrderID
                        );
                        $this->write_log_inventory($data_log);
                    }else{
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$check->OnHand,$check->Available+$AmountRes,$order->KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check->OnHand,
                            'Available'     =>$check->Available+$AmountRes,
                            'WarehouseID'   =>$order->KhoID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"Available",
                            'Amount'        =>$AmountRes,
                            'Action'        =>'update',
                            'Method'        =>'accept available from supplier '.$OrderID
                        );
                        $this->write_log_inventory($data_log);
                    }
                }else{
                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,0,$AmountRes,$order->KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                    $data_log = array(
                        'ProductsID'    =>$ProductsID,
                        'OnHand'        =>0,
                        'Available'     =>$AmountRes,
                        'WarehouseID'   =>$order->KhoID,
                        'ShipmentID'    =>$ShipmentID,
                        'DateInventory' =>date('Y-m-d'),
                        'Type'          =>"Available",
                        'Amount'        =>$AmountRes,
                        'Action'        =>'update',
                        'Method'        =>'accept available from supplier '.$OrderID
                    );
                    $this->write_log_inventory($data_log);
                }
                $this->db->query("update ttp_report_order_request_available set Status=2,Response=$AmountRes,UserAccept=".$this->user->ID.",TimeAccept='".date('Y-m-d H:i:s')."' where ID=$ID");
                echo "true";
                return;
            }
        }
        echo "false";
    }

    public function deny_available(){
        $Note = isset($_POST['Note']) ? json_decode($_POST['Note']) : '' ;
        $Note = $Note!='' ? mysql_real_escape_string($Note->Note) : '' ;
        $Note  = strip_tags($Note);
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $OrderID = isset($_POST['OrderID']) ? (int)$_POST['OrderID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $Amount = isset($_POST['Amount']) ? (float)$_POST['Amount'] : 0 ;
        $order = $this->db->query("select KhoID,UserID from ttp_report_order where ID=$OrderID")->row();
        if($order){
            $Products = $this->db->query("select Title from ttp_report_products where ID=$ProductsID")->row();
            if($Products){
                $data_request = file_get_contents("log/supplier/user_request_available.txt");
                $data_request = $data_request!='' ? json_decode($data_request,true) : array();
                if(isset($data_request[$order->UserID])){
                    $data_request[$order->UserID] .= "$Products->Title hết hàng [$Note].";
                }else{
                    $data_request[$order->UserID] = "$Products->Title hết hàng [$Note].";
                }
                file_put_contents("log/supplier/user_request_available.txt",json_encode($data_request));
                $this->db->query("update ttp_report_order_request_available set Status=3,UserAccept=".$this->user->ID.",TimeAccept='".date('Y-m-d H:i:s')."',Note='$Note' where ID=$ID");
                echo "true";
                return;
            }
        }
        echo "false";
    }

    public function request_picking(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report Tools');

        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and c.SupplierID in(".implode(',',$arr_supp).")";
        $SupplierID = isset($_GET['supplier']) ? (int)$_GET['supplier'] : 0 ;
        $supplier = $SupplierID>0 ? $supplier." and c.SupplierID=$SupplierID" : $supplier ;
        $BranchID = isset($_GET['branch']) ? (int)$_GET['branch'] : 0 ;
        $supplier = $BranchID>0 ? $supplier." and b.BranchID=$BranchID" : $supplier ;
        $result = $this->db->query("select a.ID,a.MaDH,b.Amount,b.ProductsID,b.Price,b.Status,c.Donvi,c.Title,b.TimeRequest,b.BranchID,b.ID as IDDetails,d.Title as BranchTitle from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_branch d where b.BranchID=d.ID and a.ID=b.OrderID and b.ProductsID=c.ID and b.Status=0 and a.Status=3 and b.WarehouseID=0 and date(b.TimeRequest)>='$startday' and date(b.TimeRequest)<='$stopday' $supplier order by a.ID ASC")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'SupplierID'=> $SupplierID,
            'BranchID'  => $BranchID,
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $view = "admin/import_supplier_picking";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function readytopick(){
        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $check = $this->db->query("select a.ID from ttp_report_order_send_supplier a,ttp_report_branch b where a.BranchID=b.ID and a.ID=$ID and b.SupplierID in(".implode(',',$arr_supp).") and Status=0")->row();
        if($check){
            $data = array(
                'TimeAccept' => date('Y-m-d H:i:s'),
                'UserAccept' => $this->user->ID,
                'Status'     => 1
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_order_send_supplier",$data);
            echo "true";
        }else{
            echo "false";
        }
    }

    public function waiting_pick(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";

        $this->template->add_title('Supplier | Wait picking');
        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and c.SupplierID in(".implode(',',$arr_supp).")";
        $SupplierID = isset($_GET['supplier']) ? (int)$_GET['supplier'] : 0 ;
        $supplier = $SupplierID>0 ? $supplier." and c.SupplierID=$SupplierID" : $supplier ;
        $BranchID = isset($_GET['branch']) ? (int)$_GET['branch'] : 0 ;
        $supplier = $BranchID>0 ? $supplier." and b.BranchID=$BranchID" : $supplier ;
        $result = $this->db->query("select a.ID,a.MaDH,b.Amount,c.BarcodeClient,b.Price,c.Donvi,c.Title,b.UserReciver,d.Title as BranchTitle from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_branch d where b.BranchID=d.ID and a.ID=b.OrderID and b.ProductsID=c.ID and b.Status=1 and b.WarehouseID=0 and date(b.TimeAccept)>='$startday' and date(b.TimeAccept)<='$stopday' $supplier order by a.ID ASC $limit_str")->result();

        $nav = $this->db->query("select count(1) as nav from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_branch d where b.BranchID=d.ID and a.ID=b.OrderID and b.ProductsID=c.ID and b.Status=1 and b.WarehouseID=0 and date(b.TimeAccept)>='$startday' and date(b.TimeAccept)<='$stopday' $supplier")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'SupplierID'=> $SupplierID,
            'BranchID'  => $BranchID,
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/',
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/partner_supplier/waiting_pick',5,$nav,$this->limit)
        );
        $view = "admin/import_supplier_wait_picking";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function picked(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("supplier_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("supplier_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";

        $this->template->add_title('Supplier | Has Picked');
        $supplier_list = $this->db->query("select * from ttp_user_supplier where UserID=".$this->user->ID)->result();
        $arr_supp = array();
        $arr_supp[] = 0;
        if(count($supplier_list)>0){
            foreach($supplier_list as $row){
                $arr_supp[] = $row->SupplierID;
            }
        }
        $supplier = $this->user->IsAdmin==1 ? "" : " and c.SupplierID in(".implode(',',$arr_supp).")";
        $SupplierID = isset($_GET['supplier']) ? (int)$_GET['supplier'] : 0 ;
        $supplier = $SupplierID>0 ? $supplier." and c.SupplierID=$SupplierID" : $supplier ;
        $BranchID = isset($_GET['branch']) ? (int)$_GET['branch'] : 0 ;
        $supplier = $BranchID>0 ? $supplier." and b.BranchID=$BranchID" : $supplier ;
        $result = $this->db->query("select a.ID,a.MaDH,b.Amount,c.BarcodeClient,b.Price,c.Donvi,c.Title,b.UserReciver,d.Title as BranchTitle,b.TimeReciver from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_branch d where b.BranchID=d.ID and a.ID=b.OrderID and b.ProductsID=c.ID and b.Status>=2 and b.WarehouseID=0 and date(b.TimeReciver)>='$startday' and date(b.TimeReciver)<='$stopday' $supplier order by a.ID ASC $limit_str ")->result();
        $nav = $this->db->query("select count(1) as nav from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_branch d where b.BranchID=d.ID and a.ID=b.OrderID and b.ProductsID=c.ID and b.Status>=2 and b.WarehouseID=0 and date(b.TimeReciver)>='$startday' and date(b.TimeReciver)<='$stopday' $supplier")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'SupplierID'=> $SupplierID,
            'BranchID'  => $BranchID,
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/partner_supplier/picked',5,$nav,$this->limit),
            'base_link' => base_url().ADMINPATH.'/report/partner_supplier/'
        );
        $view = "admin/import_supplier_picked";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function viewtransport(){
        $id = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $info = $this->db->query("select * from ttp_user where ID=$id")->row();
        if($info){
            $this->load->view("import_supplier_view_transporter",array('info'=>$info,'id'=>$id));
        }
    }

    public function get_notify(){
        $data = array(
            'status'=>false,
            'title'=>"Thông báo từ hệ thống ucancook",
            'image'=>"public/admin/images/logo_in_bill.png",
            'message'=>""
        );
        $supplier_list = $this->session->userdata("supplier_list");
        if(!is_object($supplier_list)){
            $supplier_list = $this->db->query("select DISTINCT b.ID,b.SupplierID from ttp_user_supplier a,ttp_report_branch b where a.SupplierID=b.SupplierID and a.UserID=".$this->user->ID)->result();
            $this->session->set_userdata("supplier_list",$supplier_list);
        }
        $data_request = file_get_contents("log/supplier/request_available.txt");
        $data_request = $data_request!='' ? json_decode($data_request,true) : array();
        $str = "";
        if(count($supplier_list)>0){
            $total = 0;
            foreach($supplier_list as $row){
                if(isset($data_request[$row->SupplierID]) && $data_request[$row->SupplierID]>0){
                    $data['status'] = true;
                    $total+=$data_request[$row->SupplierID];
                    $data_request[$row->SupplierID] = 0;
                }
            }
            if($total>0){
                $str.=number_format($total)." yêu cầu xác nhận có hàng .";
                file_put_contents("log/supplier/request_available.txt",json_encode($data_request));
            }
        }
        $data_request = file_get_contents("log/supplier/request_picking.txt");
        $data_request = $data_request!='' ? json_decode($data_request,true) : array();
        if(count($supplier_list)>0){
            $total = 0;
            foreach($supplier_list as $row){
                if(isset($data_request[$row->ID]) && $data_request[$row->ID]>0){
                    $data['status'] = true;
                    $total+=$data_request[$row->ID];
                    $data_request[$row->ID] = 0;
                }
            }
            if($total>0){
                $str.=number_format($total)." yêu cầu soạn hàng.";
                file_put_contents("log/supplier/request_picking.txt",json_encode($data_request));
            }
        }
        $data['message'] = $str;
        echo json_encode($data);
    }

}
?>
