<?php

class Lib {

    public $CI;

    public function __construct() {
        $this->CI = & get_instance();
    }

    public function get_times_remains($times) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $remain = strtotime(date('Y-m-d H:i:m', time())) - strtotime($times);
        $remain_change = $remain / (60 * 60 * 24);
        if ($remain_change >= 1) {
            if ($remain_change >= 365) {
                $remain_change = $remain / (60 * 60 * 24 * 365);
                $temp = round($remain_change, 0);
                $remain_change = $temp . ' năm trước';
            } else {
                $temp = round($remain_change, 0);
                $remain_change = $temp . ' ngày trước';
            }
        } else {
            $remain_change = $remain / (60 * 60);
            if ($remain_change >= 1) {
                $remain_change = round($remain_change) . ' giờ trước';
            } else {
                $remain_change = $remain / 60;
                if (round($remain_change, 0) > 0) {
                    $remain_change = round($remain_change, 0) . ' phút trước';
                } else {
                    $remain_change = '1 phút trước';
                }
            }
        }
        return $remain_change;
    }

    public function get_nume_day($date1 = '', $date2 = '') {
        if ($date1 != '' && $date2 != '') {
            $datediff = floor(strtotime($date1) / (60 * 60 * 24)) - floor(strtotime($date2) / (60 * 60 * 24));
            $datediff = abs($datediff);
            return $datediff;
        }
        return 0;
    }

    public function set_getvalue($key = '', $value = '') {
        if ($key != '') {
            $_GET[$key] = $value;
        }
        $str = array();
        if (count($_GET) > 0) {
            foreach ($_GET as $itemkey => $itemvalue) {
                $str[] = $itemkey . "=" . $itemvalue;
            }
        }
        return implode("&", $str);
    }

    public function getURLSegmentByClassName($ClassName = '') {
        if ($ClassName != '') {
            $result = $this->CI->db->query("select Alias from ttp_sitetree where Classname='$ClassName'")->row();
            if ($result) {
                return base_url() . $result->Alias;
            }
        }
        return '';
    }

    public function asciiCharacter($string = null) {
        $string = trim($string);
        $string = rawurldecode($string);
        $string = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $string);
        $string = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $string);
        $string = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $string);
        $string = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $string);
        $string = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $string);
        $string = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $string);
        $string = preg_replace("/(đ)/", 'd', $string);
        $string = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $string);
        $string = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $string);
        $string = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $string);
        $string = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $string);
        $string = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $string);
        $string = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $string);
        $string = preg_replace("/(Đ)/", 'D', $string);
        $string = str_replace("--", '-', $string);
        $string = str_replace("--", '-', $string);
        $string = str_replace("--", '-', $string);
        $string = str_replace(" ", ' ', $string);
        return $string;
    }

    private function toSlug($string, $space = "-") {
        $string = trim($string);
        $string = rawurldecode($string);
        $string = preg_replace("/[^a-zA-Z0-9 -]/", "", $string);
        $string = strtolower($string);
        $string = str_replace(" ", $space, $string);
        $string = str_replace("--", $space, $string);
        return $string;
    }

    public function alias($text) {
        $text = $this->asciiCharacter($text);
        $text = $this->toSlug($text);
        return $text;
    }

    public function splitString($string, $count, $symbol = '...') {
        if ($string) {
            preg_match_all('`.`u', $string, $arr);
            $arr = array_chunk($arr[0], count($string));
            $arr = array_map('implode', $arr);
            $kq = '';
            if (count($arr) < $count)
                return $kq .= $string;
            for ($i = 0; $i < $count; $i++) {
                $kq .= $arr[$i];
            }
            if ($kq)
                return $kq . $symbol;
        } else
            return false;
    }

    public function nav($url, $segment = 3, $total = 10, $perpage = 20, $numlink = 5) {
        $this->CI->load->library('pagination');
        $config['uri_segment'] = $segment;
        $config['num_links'] = $numlink;
        $config['base_url'] = $url;
        $config['total_rows'] = $total;
        $config['per_page'] = $perpage;
        $this->CI->pagination->initialize($config);

        return "<nav>" . $this->CI->pagination->create_links() . "</nav>";
    }

    public function get_catpchas() {
        $this->CI->load->library('session');
        $word_1 = chr(rand(97, 122));
        $word_2 = chr(rand(48, 57));
        $word_3 = chr(rand(97, 122));
        $word_4 = chr(rand(48, 57));
        $word_5 = chr(rand(97, 122));
        $word_6 = chr(rand(97, 122));
        $word_7 = chr(rand(97, 122));
        $filename = $word_1 . $word_2 . $word_3 . $word_4 . $word_5 . $word_6 . $word_7;
        $dir = 'public/font/';
        $image = imagecreatetruecolor(115, 25);
        $font = "recaptchaFont.ttf"; // font style
        $color = imagecolorallocate($image, 0, 0, 0); // color
        $white = imagecolorallocate($image, 241, 241, 241); // background color white
        imagefilledrectangle($image, 0, 0, 709, 99, $white);
        imagettftext($image, 14, 0, 10, 18, $color, $dir . $font, $filename);
        $this->CI->session->set_userdata('captchars', $filename);
        header("Content-type: image/png");
        imagepng($image);
        imagedestroy($image);
    }

    public function getIDFromURL($url) {
        $params = explode("-", $url);
        return (int) $params[count($params) - 1];
    }

    public function fill_data($str = "") {
        $str = mysql_real_escape_string($str);
        $str = $this->CI->security->xss_clean($str);
        return $str;
    }

    public function get_menu_permission($ParentID = 0, $UserID = 0, $Isadmin = 0) {
        if ($Isadmin == 1) {
            $result = $this->CI->db->query("select ID,Title,Alias,CurrentChecked from ttp_system_menu where ParentID=$ParentID and Published=1 order by Position ASC")->result();
        } else {
            $result = $this->CI->db->query("select a.ID,a.Title,a.Alias,a.CurrentChecked from ttp_system_menu a,ttp_system_menu_user b where a.ID=b.MenuID and b.UserID=$UserID and ParentID=$ParentID and a.Published=1 order by a.Position ASC")->result();
        }
        return $result;
    }

    public function get_config_define($group, $type, $gettype = 0, $orderby = 'name', $sort = 'asc') {
        $this->CI->db->select('id, code, name, position, description, del_flg, color');
        $this->CI->db->where('group', $group);
        $this->CI->db->where('type', $type);
        $this->CI->db->order_by($orderby, $sort);
        $res = $this->CI->db->get('ttp_define')->result();
        $arr = array();
        if (count($res) > 0) {
            foreach ($res as $row) {
                $arr[$row->code] = $row->name;
            }
        }
        return $gettype == 0 ? $arr : $res;
    }

    function getFieldsByValue($fields = '*', $where = array()) {
        $this->CI->db->select($fields);
        foreach ($where as $key => $item) {
            $this->CI->db->where($key, $item);
        }
        $res = $this->CI->db->get('ttp_define')->row();
        return $res;
    }

    public function generateEAN13($code) {
        $weightflag = true;
        $sum = 0;
        for ($i = strlen($code) - 1; $i >= 0; $i--) {
            $sum += (int) $code[$i] * ($weightflag ? 3 : 1);
            $weightflag = !$weightflag;
        }
        $code .= (10 - ($sum % 10)) % 10;
        return $code;
    }

    public function write_log_data($data = '', $status = '') {
        if ($data != '') {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $sql = array(
                'Note' => $data,
                'Status' => $status,
                'Created' => date('Y-m-d H:i:s')
            );
            $this->CI->db->insert('ttp_log', $sql);
        }
    }

    public function getfee($iddistrict = 0, $total = 0) {
        $check = $this->CI->db->query("select * from ttp_report_district where ID=$iddistrict")->row();
        $fee = 0;
        if ($check) {
            if ($total < 300000) {
                $fee = $check->PriceCost;
            }
            if ($total >= 300000 && $total < 2000000) {
                $fee = $check->PriceCost1;
            }
            if ($total >= 2000000) {
                $fee = $check->PriceCost2;
            }
        }
        return $fee;
    }

    /*
     *   Get profile user and check permission on ClassName Module
     *   @param id : ID user or UserName
     *   @param classname : Classname module check permission
     */

    public function get_user($id = 0, $classname = '') {
        if (is_numeric($id)) {
            $user = $this->CI->db->query("select a.* from ttp_user a,ttp_role b where a.ID=$id and a.RoleID=b.ID and b.Published=1 and a.Published=1")->row();
        } else {
            $user = $this->CI->db->query("select a.* from ttp_user a,ttp_role b where UserName='$id' and a.RoleID=b.ID and b.Published=1 and a.Published=1")->row();
        }
        if ($user) {
            if ($user->IsAdmin == 1)
                return $user;
            if ($classname == 'home')
                return $user;
            $permission = json_decode($user->DetailRole, true);
            if (array_key_exists($classname, $permission)) {
                return $user;
            }
            redirect(base_url() . ADMINPATH . "/home/permission/index/r");
        }
        $this->CI->session->unset_userdata('ttp_usercp');
        $this->CI->session->sess_destroy();
        redirect(base_url() . ADMINPATH);
    }

    /*
     *   Get profile user and check permission on ClassName Module
     *   @param data : data permission json
     *   @param classname : Classname module check permission
     *   @param permission : Permission check permission
     *   @param isadmin : Status check user is admin
     */

    public function check_permission($data = '', $classname = '', $permission = '', $isadmin = 0) {
        if ($isadmin == 1)
            return true;
        if ($classname == 'home')
            return true;
        $data = json_decode($data, true);
        if (is_array($data)) {
            if (array_key_exists($classname, $data)) {
                if (in_array($permission, $data[$classname]))
                    return true;
            }
        }
        redirect(base_url() . ADMINPATH . "/home/permission/index/" . $permission);
    }

    /*
     *   Check module is Enable or Disable
     *   @param module : classname module check status
     */

    public function published_module($module = '') {
        $status = $this->CI->db->query("select ID from ttp_sitetree where Classname='$module' and Published=1")->row();
        if ($status)
            return true;
        redirect(base_url() . ADMINPATH . "/home/permission/module/off");
    }

    /*
     *   Check target
     */

    public function gettarget($str = '') {
        if ($str != '') {
            $findhttp = strpos($str, "http://");
            if ($findhttp == "0a") {
                $findhttp = str_replace("http://", "", $str);
                $findhttp = str_replace("https://", "", $str);
                if ($findhttp != '') {
                    $findhttp = explode('/', $findhttp);
                    $findhttp = count($findhttp) > 0 ? $findhttp[0] : '';
                    if ($findhttp != '') {
                        $findhttp = "http://" . $findhttp . '/';
                        if ($findhttp != base_url())
                            return "target='_blank'";
                    }
                }
            }
        }
        return '';
    }

    /*
     *   Get thumb name of image
     */

    public function get_thumb($path = '') {
        if ($path != '') {
            $str = strlen($path);
            $name = $str - 4;
            $start = $str - 4;
            $str_name = substr($path, 0, $name);
            $str_extend = substr($path, $start, 4);
            $name_thumb = $str_name . '_thumb' . $str_extend;
            return $name_thumb;
        }
    }

    public function getfilesize($path = '', $width = 0, $height = 0) {
        if ($width > 0 && $height > 0 && $path != '') {
            $image = explode("/", $path);
            $filename = count($image) > 0 ? $image[count($image) - 1] : $image[0];
            $crop = $width . "x" . $height . "_" . $filename;
            $file = str_replace($filename, $crop, $path);
            if (file_exists($file)) {
                return $file;
            }
            return $path;
        }
        return "http://placehold.it/{$width}x{$height}";
    }

    public function cropimage($path, $width = 300, $height = 300) {
        if (file_exists($path)) {
            /*             * ***** resize ******* */
            $image = explode("/", $path);
            $filename = count($image) > 0 ? $image[count($image) - 1] : $image[0];
            $image_config["image_library"] = "gd2";
            $image_config["source_image"] = $path;
            $image_config['create_thumb'] = FALSE;
            $image_config['maintain_ratio'] = TRUE;
            $image_config['new_image'] = $width . "x" . $height . "_" . $filename;
            $image_config['width'] = $width;
            $image_config['height'] = $height;
            $image_config['master_dim'] = "width";
            $this->CI->load->library('image_lib');
            $this->CI->image_lib->initialize($image_config);
            if (!$this->CI->image_lib->resize()) {
                echo $this->CI->image_lib->display_errors();
            }
            /*             * ***** crop ******* */
            $config['image_library'] = 'gd2';
            $config['maintain_ratio'] = false;
            $config['source_image'] = str_replace($filename, $image_config['new_image'], $path);
            $config['width'] = $width;
            $config['height'] = $height;
            $config['x_axis'] = "0";
            $config['y_axis'] = "0";
            $config['new_image'] = $width . "x" . $height . "_" . $filename;
            $this->CI->image_lib->clear();
            $this->CI->image_lib->initialize($config);
            if (!$this->CI->image_lib->crop()) {
                echo $this->CI->image_lib->display_errors();
            }
        }
    }

    public function check_permission_function($user, $function = '') {
        
    }

    public function get_status_order_from_viettelpost($time = '') {
        $month_year = date('Y-m');
        $time = $time == '' ? strtotime($month_year . '-01') : $time;
        $cookie_path = dirname(__FILE__) . '/cookie.txt';
        $data = array(
            "email" => "duy.tl@hoathienphu.com",
            "password" => "bomdin2016"
        );
        $data_string = json_encode($data);
        $ch = curl_init('http://kh.vtp.vn/api/public/api/v1/auth/login');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8'
        ));
        $response = curl_exec($ch);
        $data = json_decode($response, true);
        $token = $data['data']['token']['key'];
        $ch = curl_init('http://kh.vtp.vn/api/public/api/v1/order/orders?limit=500&page=1&sort=-time_create&time_create_start=' . $time);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
        curl_setopt($ch, CURLOPT_REFERER, 'http://kh.vtp.vn/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
            'Authorization: ' . $token
        ));

        $response1 = curl_exec($ch);
        $response1 = json_decode($response1, true);
        return $response1;
    }

}