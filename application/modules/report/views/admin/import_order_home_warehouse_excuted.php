<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$arr_data 			= array();
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH SÁCH ĐƠN HÀNG ĐÃ XỬ LÝ</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="row" style="margin: 0px;">
        <div class="form-group">
            <div class="col-xs-9">
                <div class="btn-group">
                    <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Kho xuất hàng</a>
                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php 
                        $checked_sort = $warehouseid==0 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid=0&ordertype='.$ordertype.'&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Tất cả kho</a></li>';
                        $warehouse = $this->db->query("select MaKho,ID from ttp_report_warehouse")->result();
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                $checked_sort = $warehouseid==$row->ID ? "color:#090" : "color:#ccc" ;
                                echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$row->ID.'&ordertype='.$ordertype.'&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> '.$row->MaKho.'</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="btn-group">
                    <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Loại đơn hàng</a>
                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php 
                        $checked_sort = $ordertype==6 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Tất cả đơn hàng</a></li>';
                        $checked_sort = $ordertype==0 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&ordertype=0&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Đơn hàng online</a></li>';
                        $checked_sort = $ordertype==2 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&ordertype=2&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Đơn hàng MT</a></li>';
                        $checked_sort = $ordertype==1 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&ordertype=1&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Đơn hàng GT</a></li>';
                        $checked_sort = $ordertype==4 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&ordertype=4&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Đơn hàng TD</a></li>';
                        $checked_sort = $ordertype==5 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&ordertype=5&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Đơn hàng NB</a></li>';
                        $checked_sort = $ordertype==3 ? "color:#090" : "color:#ccc" ;
                        echo '<li><a href="'.$base_link.'warehouse_excuted?warehouseid='.$warehouseid.'&ordertype=3&keywords='.$keywords.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Đơn hàng gốm sứ</a></li>';

                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-xs-3">
                <form action="<?php echo $base_link.'warehouse_excuted' ?>">
                <div class="input-group">
                    <input type="text" class="form-control" name="keywords" placeholder="Nhập mã phiếu cần tìm ..." value="<?php echo str_replace('+',' ',$keywords) ?>">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                    <input type='hidden' name='warehouseid' value='<?php echo $warehouseid ?>' />
                    <input type='hidden' name='ordertype' value='<?php echo $ordertype ?>' />
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="import_orderlist">
    	<div class="block3 table_data">
            <table id="table_data">
    			<tr>
    				<th>Loại ĐH</th>
    				<th style='width: 90px;'>Ngày</th>
    				<th>Mã xuất kho</th>
    				<th>Kho xuất</th>
                    <th>SKU</th>
    				<th>Sản phẩm</th>
                    <th>Mã số lô</th>
                    <th>Số lượng</th>
                    <th>Trạng thái</th>
    			</tr>
    			<?php 
                $type_status = $this->define_model->get_order_status('type','order');
                $arr_type = array();
                foreach($type_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $arr_type[$code] = $ite->name;
                }
    			$arr_order = array();
                if(count($bundle)>0){
                    foreach($bundle as $row){
                        $row->Status = $row->Status==9 ? "Chưa xuất kho" : "--" ;
                        if(isset($arr_order[$row->ID])){
                            $arr_order[$row->ID].="<tr class='classpage'>
                                <td style='text-align:center;width:80px'><a href='{$base_link}preview/$row->ID'>".$arr_type[$row->OrderType]."</a></td>
                                <td style='width: 90px;'><a href='{$base_link}preview/$row->ID'>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->MaXK."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->MaKho."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->MaSP."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->Title."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->ShipmentCode."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->Amount."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->Status."</a></td>
                            </tr>";
                        }else{
                            $arr_order[$row->ID]="<tr class='classpage'>
                                <td style='text-align:center;width:80px'><a href='{$base_link}preview/$row->ID'>".$arr_type[$row->OrderType]."</a></td>
                                <td style='width: 90px;'><a href='{$base_link}preview/$row->ID'>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->MaXK."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->MaKho."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->MaSP."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->Title."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->ShipmentCode."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->Amount."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".$row->Status."</a></td>
                            </tr>";
                        }
                    }
                }
                if(count($data)>0){
                    $i=1;
                    $page=1;
                    foreach($data as $row){
                        if($i%30==1 && $i!=1){
                            $page++;
                        }
                        $row->Status = $row->Status==9 ? "Chưa xuất kho" : "--" ;
                        $hidden = $page>1 ? "hidden" : "" ;
                        echo "<tr class='page$page $hidden'>
                            <td style='text-align:center;width:80px'><a href='{$base_link}preview/$row->ID'>".$arr_type[$row->OrderType]."</a></td>
                            <td style='width: 90px;'><a href='{$base_link}preview/$row->ID'>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->MaXK."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->MaKho."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->MaSP."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->Title."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->ShipmentCode."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->Amount."</a></td>
                            <td><a href='{$base_link}preview/$row->ID'>".$row->Status."</a></td>
                        </tr>";
                        if(isset($arr_order[$row->ID])){
                            echo str_replace('classpage',"page$page $hidden",$arr_order[$row->ID]);
                        }
                        $i++;
                    }
    			}else{
    				echo "<tr><td colspan='9'>Không tìm thấy đơn hàng.</td></tr>";
    			}
    			?>
    		</table>
            <?php echo $nav ?>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
    var baselink = $("#baselink_report").val();
    var i=1;
    $(window).scroll(function(){
        var scrollto = $("body");
        var position = scrollto.height();
        console.log(position + " | " + $(this).scrollTop()+ " | " +screen.height + " | "+i);
        if (($(this).scrollTop()+screen.height) > position) {
            i=i+1;
            $("tr.page"+i).removeClass("hidden");
        }
    });

</script>  
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });
    
    $(".numbernoti").click(function(){
        $(this).parent().find('ul').toggle();
    });

	$("#show_thaotac").click(function(event){
        event.stopPropagation();
		$(this).parent('li').find('ul').toggle();
	});

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });

    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });
    
    var baselink = $("#baselink_report").val();
    var getnewsorder = function(){
        var data = $("#newsdescription").html();
        $.ajax({
            url: baselink+"import/get_news_order",
            dataType: "html",
            type: "POST",
            data: "",
            success: function(result){
                if(data!=result){
                    $("#newsdescription").html(result);
                    $("#newsdescription").slideDown();
                }
            }
        });

    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });

    function notifyNewOrder(title,image,message){
        notify(title, {
            body: message,
            icon: image,
            onclick: function(e) {},
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }

    function navpage(ob){
        var data_nav = $(ob).attr('data');
        window.location=data_nav+"?<?php echo "warehouseid=$warehouseid&ordertype=$ordertype&keywords=$keywords" ?>";
    }    
</script>
<style>
    .body_content .containner .import_orderlist .export_tools_kt:before{right:177px;}
</style>