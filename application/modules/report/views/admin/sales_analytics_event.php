<?php 
$arr_event = array(1=>'Con số may mắn',2=>'Quiz game');
$event = isset($_GET['event']) ? $_GET['event'] : 1 ;
$title = isset($arr_event[$event]) ? $arr_event[$event] : 'Không xác định' ;
$data = $this->db->query("select * from ttp_event_analytics where EventID=$event")->result();
?>
<div class="containner">
    <div class="row" style="margin:0px">
        <span>Sự kiện thống kê</span>
        <div class="btn-group" style="margin-left:15px">
            <a class="btn btn-default"><i class="fa fa-sort" aria-hidden="true"></i> <?php echo $title ?></a>
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo ADMINPATH ?>/report/report_sales/analytics_event?event=1">Đăng ký nhận thưởng</a></li>
            </ul>
        </div>
    </div>
    <div class="row" style="margin:0px">
        <div class="form-group">
            <div class="col-xs-6">
                <h4>Các nguồn truy cập từ chương trình marketing</h4>
                <hr>
                <table class="table table-bordered table-hovered">
                    <tr>
                        <th>STT</th>
                        <th>Nguồn truy cập</th>
                        <th>Lượt truy cập</th>
                    </tr>
            	<?php 
                $arr = array();
                $arr_device = array();
                foreach ($data as $row) {
                    $temp = str_replace('.','',$row->Referer);
                    $temp = str_replace('/','',$temp);
                    if(isset($arr[$temp])){
                        $arr[$temp]['total'] = $arr[$temp]['total']+1;
                    }else{
                        $arr[$temp]=array('total'=>1,'source'=>$row->Referer);
                    }
                    if(isset($arr_device[$row->Device])){
                        $arr_device[$row->Device]['total'] = $arr_device[$row->Device]['total']+1;
                    }else{
                        $arr_device[$row->Device]['name'] = $row->Device;
                        $arr_device[$row->Device]['total'] = 1;
                    }
                }
                if(count($arr)>0){
                    $i=1;
                    foreach($arr as $row){
                        echo "<tr>";
                        echo "<td>$i</td>";
                        echo "<td>{$row['source']}</td>";
                        echo "<td>{$row['total']}</td>";
                        echo "</tr>";
                        $i++;
                    }
                }
                ?>
                </table>
            </div>
            <div class="col-xs-6">
                <div id="piechart" style="width: 100%; height: 300px;"></div>
            </div>
        </div>
    </div>
    <div class="row" style="margin:0px">
        <div class="form-group">
            <div class="col-xs-6">
                <h4>Các thiết bị truy cập từ chương trình marketing</h4>
                <hr>
                <table class="table table-bordered table-hovered">
                    <tr>
                        <th>STT</th>
                        <th>Thiết bị truy cập</th>
                        <th>Lượt truy cập</th>
                    </tr>
                <?php 
                
                if(count($arr_device)>0){
                    $i=1;
                    foreach($arr_device as $row){
                        echo "<tr>";
                        echo "<td>$i</td>";
                        echo "<td>{$row['name']}</td>";
                        echo "<td>{$row['total']}</td>";
                        echo "</tr>";
                        $i++;
                    }
                }
                ?>
                </table>
            </div>
            <div class="col-xs-6">
                <div id="piechart1" style="width: 100%; height: 300px;"></div>
            </div>
        </div>
    </div>
	<div class="over_lay"></div>
</div>
<style>
	.col-xs-4{border-right:1px solid #eee;margin-bottom:20px;min-height: 100px;}
	.col-xs-4 h5{text-transform: uppercase;}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          <?php 
            if(count($arr_device)>0){
                echo "['Thiết bị truy cập', 'Lượt truy cập']";
                foreach($arr_device as $row){
                    echo ",['".$row['name']."',".$row['total']."]";
                }
            }
          ?>
        ]);

        var options = {
          title: 'Thống kê truy cập theo thiết bị'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart1'));

        chart.draw(data, options);

        var data1 = google.visualization.arrayToDataTable([
          <?php 
            if(count($arr)>0){
                echo "['Nguồn truy cập', 'Lượt truy cập']";
                foreach($arr as $row){
                    echo ",['".$row['source']."',".$row['total']."]";
                }
            }
          ?>
        ]);

        var options1 = {
          title: 'Thống kê truy cập theo nguồn'
        };

        var chart1 = new google.visualization.PieChart(document.getElementById('piechart'));

        chart1.draw(data1, options1);
    }
</script>