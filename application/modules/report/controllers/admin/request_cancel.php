<?php 
class Request_cancel extends Admin_Controller { 
 
    public $user;
    public $classname="request_cancel";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/import_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;
    
    public function index(){
        $startday = $this->session->userdata("import_warehouse_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_warehouse_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        switch ($this->user->UserType) {
            case 5:
                $bonus=' and c.UserType=1';
                break;
            case 7:
                $bonus=' and c.UserType=3';
                break;
            case 8:
                $bonus=' and c.UserType=2';
                break;
            default:
                $bonus='';
                break;
        }
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_request_cancelorder a,ttp_report_order b,ttp_user c where a.UserID=c.ID and a.OrderID=b.ID and date(a.Created)>='$startday' and date(a.Created)<='$stopday' $bonus")->row();
        $result = $this->db->query("select a.UserAccept,a.ID,a.Status,a.Note,a.Created,b.MaDH,c.UserName from ttp_report_request_cancelorder a,ttp_report_order b,ttp_user c where a.UserID=c.ID and a.OrderID=b.ID and date(a.Created)>='$startday' and date(a.Created)<='$stopday' $bonus order by a.ID DESC $limit_str")->result();
        $nav = $nav ? $nav->nav : 0;

        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/request_cancel/',
            'start'     =>  $start,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/request_cancel/index',5,$nav,$this->limit)
        );
        $view = 'admin/request_cancel_home';
        $this->template->add_title('Yêu cầu hủy đơn hàng | Report Tools');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function edit($id=0){
        if($id>0){
            $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,d.MaKho as KhoTitle,g.Title as Thanhpho,h.Title as Quanhuyen,i.Title as Area,l.UserName as Sender,k.Status as AcceptStatus,k.Note as Lydohuy from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d,ttp_report_city g,ttp_report_district h,ttp_report_area i,ttp_report_request_cancelorder k,ttp_user l where g.AreaID=i.ID and a.DistrictID=h.ID and a.CityID=g.ID and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID and l.ID=k.UserID and a.ID=k.OrderID and k.ID=$id")->row();
            if($result){
                $view = 'admin/request_cancel_edit';
                $this->template->add_title('Yêu cầu hủy đơn hàng | Report Tools');
                $this->template->write_view('content',$view,array('data'=>$result));
                $this->template->render();
            }
        }
    }

    public function accept_cancel($id=0){
        if($id>0){
            $order = $this->db->query("select a.ID,a.Status as AcceptStatus,a.KhoID,b.Status,c.UserType,b.Note,a.MaDH,a.AffiliateID from ttp_report_order a,ttp_report_request_cancelorder b,ttp_user c where b.UserID=c.ID and a.ID=b.OrderID and a.ID=$id")->row();
            if($order){
                if($order->Status==0){
                    if($order->UserType==1 || $this->user->UserType==5 || $order->UserType==3 || $this->user->UserType==7 || $order->UserType==2 || $this->user->UserType==8 || $this->user->IsAdmin==1){
                        if($order->AcceptStatus==10){
                            $history = $this->db->query("select Status from ttp_report_orderhistory where OrderID=$id order by ID DESC limit 1,1")->row();
                            if($history){
                                $export = $this->db->query("select ID from ttp_report_export_warehouse where OrderID=$id")->row();
                                if($export){
                                    $details = $this->db->query("select a.ID,a.Amount,a.ProductsID,a.ShipmentID from ttp_report_orderdetails a,ttp_report_products b where a.OrderID=$id and a.ProductsID=b.ID and b.VariantType in(0,1)")->result();
                                    if($history->Status==5 || $history->Status==9){
                                        $this->change_inventory($order,$details);
                                    }elseif($history->Status==7 || $history->Status==11){
                                        $arr  = array();
                                        $slsp = 0;
                                        $total= 0;
                                        if(count($details)>0){
                                            foreach($details as $row){
                                                $slsp = $slsp+$row->Amount;
                                                $arr[] = "(ImportIDDV,$row->ProductsID,$row->ShipmentID,'VND',1,0,$row->Amount)";
                                            }
                                        }
                                        $details = $this->db->query("select a.Amount,a.ProductsID,a.ShipmentID from ttp_report_orderdetails_bundle a,ttp_report_products b where a.OrderID=$id and a.ProductsID=b.ID")->result();
                                        if(count($details)>0){
                                            foreach($details as $row){
                                                $slsp = $slsp+$row->Amount;
                                                $arr[] = "(ImportIDDV,$row->ProductsID,$row->ShipmentID,'VND',1,0,$row->Amount)";
                                            }
                                        }

                                        if(count($arr)>0){
                                            $thismonth = date('m',time());
                                            $thisyear = date('Y',time());
                                            $max = $this->db->query("select count(1) as max from ttp_report_inventory_import where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and Type=1")->row();
                                            $max = $max ? $max->max + 1 : 1 ;
                                            $thisyear = date('y',time());
                                            $max = "YCNK1".'_'.$order->KhoID.'_'.$thisyear.$thismonth.'_'.str_pad($max, 7, '0', STR_PAD_LEFT);

                                            $data = array(
                                                'MaNK'      => $max,
                                                'ExportID'  => $export->ID,
                                                'KhoID'     => $order->KhoID,
                                                'UserID'    => $this->user->ID,
                                                'NgayNK'    => date('Y-m-d'),
                                                'Type'      => 1,
                                                'Note'      => $order->Note,
                                                'Status'    => 0,
                                                'TotalAmount'=> $slsp,
                                                'TotalPrice'=> $total,
                                                'Created'   => date('Y-m-d H:i:s'),
                                                'LastEdited'=> date('Y-m-d H:i:s')
                                            );
                                            $this->db->insert('ttp_report_inventory_import',$data);
                                            $ID = $this->db->insert_id();
                                            $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Amount) values".implode(',',$arr);
                                            $arr = str_replace('ImportIDDV',$ID,$arr);
                                            $this->db->query($arr);
                                            $data_his = array(
                                                'ImportID'  => $ID,
                                                'Status'    => 0,
                                                'Note'      => $order->Note,
                                                'UserID'    => $this->user->ID,
                                                'Created'   => date('Y-m-d H:i:s')
                                            );
                                            $this->db->insert('ttp_report_inventory_import_history',$data_his);
                                        }
                                    }
                                }
                                if($order->AffiliateID==27){
                                    $this->reject_affi_AT($order);
                                }
                                $this->db->query("update ttp_report_request_cancelorder set Status=1,UserAccept=".$this->user->ID." where OrderID=$id");
                                $this->db->query("update ttp_report_order set Status=1 where ID=$id");
                            }
                        }
                    }
                }
            }
        }
        redirect(ADMINPATH.'/report/request_cancel');
    }

    public function reject_affi_AT($result){
        $data = array(
            "transaction_id"=> $result->MaDH,
            "status"=> 2
        );
        $data_string = json_encode($data);
        $ch = curl_init('https://api.accesstrade.vn/v1/postbacks/conversions');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Token 0AC8BWsHsG94A_Ksr2YmGaX6WJjUk8cb'
        ));
        $response = curl_exec($ch);
        $this->write_log_affiliate_AT($result->ID,$data_string,$response);
    }

    public function write_log_affiliate_AT($orderid=0,$data='',$response=''){
        $sql = array(
            'OrderID'   => $orderid,
            'Data'      => $data,
            'Response'  => $response,
            'Created'   => date('Y-m-d H:i:s')
        );
        $this->db->insert("ttp_report_affiliate_tracking",$sql);
    }

    public function change_inventory($order,$details){
        if($order){
            if(count($details)>0){
                foreach($details as $row){
                    $bundle = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$row->ID")->result();
                    if(count($bundle)>0){
                        foreach($bundle as $item){
                            $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                            if($check_exists){
                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1");
                                if($check_exists->DateInventory==date('Y-m-d')){
                                    $this->db->query("update ttp_report_inventory set Available=Available+$item->Amount,LastEdited=1 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and DateInventory='$check_exists->DateInventory' and WarehouseID=$order->KhoID");
                                    $data_log = array(
                                        'ProductsID'    =>$item->ProductsID,
                                        'OnHand'        =>$check_exists->OnHand,
                                        'Available'     =>$check_exists->Available+$item->Amount,
                                        'WarehouseID'   =>$order->KhoID,
                                        'ShipmentID'    =>$item->ShipmentID,
                                        'DateInventory' =>$check_exists->DateInventory,
                                        'Type'          =>"Available",
                                        'Amount'        =>$item->Amount,
                                        'Action'        =>'Cancel order 1',
                                        'Method'        =>'Cancel order id '.$order->ID
                                    );
                                    $this->write_log_inventory($data_log);
                                }else{
                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$check_exists->OnHand,$check_exists->Available+$item->Amount,$order->KhoID,$item->ShipmentID,'".date('Y-m-d')."',1)");
                                    $data_log = array(
                                        'ProductsID'    =>$item->ProductsID,
                                        'OnHand'        =>$check_exists->OnHand,
                                        'Available'     =>$check_exists->Available+$item->Amount,
                                        'WarehouseID'   =>$order->KhoID,
                                        'ShipmentID'    =>$item->ShipmentID,
                                        'DateInventory' =>date('Y-m-d'),
                                        'Type'          =>"Available",
                                        'Amount'        =>$item->Amount,
                                        'Action'        =>'Cancel order 2',
                                        'Method'        =>'Cancel order id '.$order->ID
                                    );
                                    $this->write_log_inventory($data_log);
                                }
                            }
                        }
                    }else{
                        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
                        if($check_exists){
                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");
                            if($check_exists->DateInventory==date('Y-m-d')){
                                $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,LastEdited=1 where ID=$check_exists->ID");
                                $data_log = array(
                                    'ProductsID'    =>$row->ProductsID,
                                    'OnHand'        =>$check_exists->OnHand,
                                    'Available'     =>$check_exists->Available+$row->Amount,
                                    'WarehouseID'   =>$order->KhoID,
                                    'ShipmentID'    =>$row->ShipmentID,
                                    'DateInventory' =>date('Y-m-d'),
                                    'Type'          =>"Available",
                                    'Amount'        =>$row->Amount,
                                    'Action'        =>'Cancel order 3',
                                    'Method'        =>'Cancel order id '.$order->ID
                                );
                                $this->write_log_inventory($data_log);
                            }else{
                                $Available = $check_exists->Available+$row->Amount;
                                $OnHand = $check_exists->OnHand;
                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                $data_log = array(
                                    'ProductsID'    =>$row->ProductsID,
                                    'OnHand'        =>$check_exists->OnHand,
                                    'Available'     =>$check_exists->Available+$row->Amount,
                                    'WarehouseID'   =>$order->KhoID,
                                    'ShipmentID'    =>$row->ShipmentID,
                                    'DateInventory' =>date('Y-m-d'),
                                    'Type'          =>"Available",
                                    'Amount'        =>$row->Amount,
                                    'Action'        =>'Cancel order 4',
                                    'Method'        =>'Cancel order id '.$order->ID
                                );
                                $this->write_log_inventory($data_log);
                            }
                        }
                    }
                }
            }
        }
    }
}
?>