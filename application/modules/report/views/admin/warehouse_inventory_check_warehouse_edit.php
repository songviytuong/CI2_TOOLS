<div class="containner">
    <form action="<?php echo base_url().ADMINPATH.'/report/warehouse/add_details_check_warehouse' ?>" method="POST">
        <input type='hidden' name='ID' value='<?php echo $data->ID ?>' />
        <div class="row">
            <div class="form-group">
                <div class="col-xs-6">
                    <?php 
                    if($data->Status==0){
                    ?>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-check" aria-hidden="true"></i> Xác nhận kiểm kho</button>
                        <a class="btn btn-default delete" href="<?php echo base_url().ADMINPATH.'/report/warehouse/delete_check_warehouse/'.$data->ID ?>"><i class="fa fa-times" aria-hidden="true"></i> Hủy kiểm kho</a>
                    <?php 
                    }else{
                        echo "<a class='btn btn-primary' style='margin-right:10px'><i class='fa fa-download'></i> Xuất excel</a>";
                        echo "<a class='btn btn-default'><i class='fa fa-print'></i> In phiếu kiểm kho</a>";
                    }
                    ?>
                </div>
                <div class="col-xs-6">
                    <div class="breakcrumb">
                        <a>Dự thảo</a>
                        <a <?php echo $data->Status==0 ? 'class="active"' : '' ; ?>>Đang xử lý</a>
                        <a <?php echo $data->Status==1 ? 'class="active"' : '' ; ?>>Đã xác nhận</a>
                        <span>Trạng thái xử lý</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="form-group"><hr></div></div>
        <div class="row">
            <div class="form-group">
                <h3><?php echo $data->Title ?></h3>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="col-xs-2 control-label"><b>Ngày kiểm kho</b></label>
                <label class="col-xs-10 control-label"> :  <?php echo date('d/m/Y H:i:s',strtotime($data->Created)) ?></label>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="col-xs-2 control-label"><b>Địa điểm kiểm kho</b></label>
                <label class="col-xs-10 control-label"> :  <?php echo $data->MaKho.' - '.$data->TenKho ?></label>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <?php 
                $status = $this->define_model->get_order_status('products','productstype','position');
                $arr_status = array();
                foreach($status as $row){
                    $arr_status[$row->code] = $row->name;
                }
                ?>
                <label class="col-xs-2 control-label"><b>Loại sản phẩm</b></label>
                <label class="col-xs-10 control-label"> :  <?php echo $arr_status[$data->Type] ?></label>
            </div>
        </div>
        <div class="row"><div class="form-group"><hr></div></div>
        <div class="row">
            <div class="form-group">
                <div class="col-xs-6"><h3>Chi tiết kiểm kho</h3></div>
                <div class="col-xs-6">
                    <?php 
                    if($data->Status==0){ 
                    ?>
                        <a class="pull-right text-primary" onclick="resetamount()"><i class="fa fa-exchange" aria-hidden="true"></i> Thiết lập số lượng về 0</a>
                    <?php 
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <table class="table">
                    <tr>
                        <th>Sản phẩm</th>
                        <th>Đơn vị</th>
                        <th>Lô sản xuất</th>
                        <th class='text-right'>Số lượng lý thuyết</th>
                        <th class='text-right'>Số lượng thực tế </th>
                    </tr>
                    <?php 
                    if(count($inventory)>0){
                        foreach($inventory as $row){
                            echo "<tr>";
                            echo "<td>$row->Title</td>";
                            echo "<td>$row->Donvi</td>";
                            echo "<td>$row->ShipmentCode</td>";
                            if($data->Status==0){
                                echo "<td class='text-right'><input type='hidden' name='OldData[".$row->ProductsID."_".$row->ShipmentID."]' value='".$row->OnHand."' />".number_format($row->OnHand,2)."</td>";
                                echo "<td class='text-right'><input class='amount' type='text' name='NewData[".$row->ProductsID."_".$row->ShipmentID."]' value='".number_format($row->OnHand,2)."' /></td>";
                            }else{
                                echo "<td class='text-right'>".number_format($row->OnHandOld,2)."</td>";
                                echo "<td class='text-right'>".number_format($row->OnHand,2)."</td>";
                            }
                            echo "</tr>";
                            echo "<input type='hidden' name='IDData[".$row->ProductsID."_".$row->ShipmentID."]' value='$row->ID' />";
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </form>
</div>
<style>
    .row{margin:0px;}
    hr{margin:0px;}
    .row h3{margin:10px 0px;}
    table tr td input{text-align:right;border:none;outline:none;}
</style>
<script>
    function resetamount(){
        $("input.amount").each(function(){
            $(this).val("0.00");
        });
    }
</script>