<?php 
class Pos_inventory extends Admin_Controller { 
 	public $user;
 	public $classname="pos_inventory";
    public $limit=15;
    public $WarehouseID=2;
    public $PosUser = 84;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('pos');
        $this->template->add_doctype();
        $warehouse = $this->session->userdata('branch');
        $this->WarehouseID = $warehouse!='' ? $warehouse : $this->WarehouseID ;
    }

    public function index(){
        $this->lib->check_permission_function($this->user);
        $this->template->add_title('Inventory | POS Tools');
        $result = $this->db->query("select a.*,b.RootPrice,b.MaSP,b.Title,b.Donvi,b.TypeProducts from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.WarehouseID=$this->WarehouseID and a.LastEdited=1 and a.OnHand>0")->result();
        $data = array(
            'data'  => $result
        );
		$this->template->write_view('content','admin/inventory_check',$data);
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Kho hàng','function'=>'<a>Kiểm kê hàng hóa</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'inventory'));
		$this->template->render();
	}

    public function create(){
        $this->lib->check_permission_function($this->user);
        $files = scandir('log/pos/inventory');
        if(count($files)==2){
            $txt = time();
            file_put_contents('log/pos/inventory/draft_order_'.$txt.'.txt',json_encode(array('products'=>array(),'customer'=>array('Title'=>'','WarehouseID'=>'','Active'=>0,'Type'=>0))));
        }
        $this->template->add_title('Inventory | POS Tools');
        $this->template->write_view('content','admin/inventory_create');
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Kho hàng','function'=>'<a href="'.ADMINPATH.'/pos/pos_inventory">Kiểm kê hàng hóa</a>','bonus'=>'<a>Tạo phiếu kiểm kho</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'inventory'));
        $this->template->render();
    }

    public function get_products_by_barcode(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select ID from ttp_report_products where Barcode='$keywords'")->row();
        $response = array('error'=>1);
        if($result){
            $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$result->ID])){
                $data['products'][$result->ID] = $data['products'][$result->ID] + 1;
            }else{
                $data['products'][$result->ID] = 1;
            }
            file_put_contents('log/pos/inventory/draft_order_'.$order.'.txt',json_encode($data));
            $response = array('error'=>0);
        }
        echo json_encode($response);
    }

    public function get_products_list(){
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select MaSP,Title,ID from ttp_report_products where Title like '%$keywords%' or MaSP like '%$keywords%' or Barcode='$keywords' limit 0,20")->result();
        if(count($result)>0){
            foreach ($result as $key => $value) {
                echo "<p><a onclick='inventory_apply_products($value->ID)'>($value->MaSP) | $value->Title</a></p>";
            }
        }
    }

    public function load_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $this->load->view("admin/inventory_draft_order_products",array('data'=>$data));
    }

    public function change_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 1 ;
        if($amount>0 && $products>0){
            $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$products])){
                $data['products'][$products] = $amount;
            }
            file_put_contents('log/pos/inventory/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function remove_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(isset($data['products'][$products])){
            unset($data['products'][$products]);
        }
        file_put_contents('log/pos/inventory/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function remove_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : '' ;
        @unlink("log/pos/inventory/draft_order_".$order.".txt");
        $files = scandir('log/pos/inventory');
        $first = 0;
        if(count($files)>0){
            $i=1;
            foreach($files as $row){
                if($row!='.' && $row!='..'){
                    $name = explode('_',$row);
                    $name = str_replace('.txt','',$name[2]);
                    if($i==1){
                        $first = $name;
                    }
                    $i++;
                }
            }
        }
        echo $first;
    }

    public function load_customers_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        echo json_encode($data['customer']);
    }

    public function reset_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['products'] = array();
        file_put_contents('log/pos/inventory/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(!isset($data['products'])){
            $data['products'] = array();
        }
        if(!isset($data['customer'])){
            $data['customer'] = array('Title'=>'','WarehouseID'=>'','Active'=>0,'Type'=>0);
        }
        if(isset($data['products'][$products])){
            $data['products'][$products] = $data['products'][$products]+1;
        }else{
            $data['products'][$products] = 1;
        }
        file_put_contents('log/pos/inventory/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function draftorder(){
        $txt = time();
        file_put_contents('log/pos/inventory/draft_order_'.$txt.'.txt',json_encode(array('products'=>array(),'customer'=>array('Title'=>'','WarehouseID'=>'',"Active"=>"0",'Type'=>0))));
        $response['error'] = 0;
        $response['order'] = $txt;
        echo json_encode($response);
    }

    public function create_order(){
        $order = isset($_POST['order']) ? $_POST['order'] : '1483783819' ;
        $title = isset($_POST['title']) ? $_POST['title'] : 'Kiểm tồn kho cuối ngày' ;
        $warehouse = isset($_POST['warehouse']) ? $_POST['warehouse'] : 2 ;
        $typeinvent = isset($_POST['typeinvent']) ? $_POST['typeinvent'] : 0 ;
        $response = array("error"=>1,"message"=>"Áp dụng kiểm kho thành công !");
        if($title!=''){
            $data = file_get_contents('log/pos/inventory/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products']) && count($data['products'])>0){
                $sql = array(
                    'Title'         => $title,
                    'UserID'        => $this->user->ID,
                    'WarehouseID'   => $warehouse,
                    'Type'          => $typeinvent,
                    'Status'        => 1,
                    'Created'       => date('Y-m-d H:i:s'),
                    'LastEdited'    => date('Y-m-d H:i:s')
                );
                $this->db->insert("ttp_report_inventory_check",$sql);
                $orderid = $this->db->insert_id();
                $data_history = array(
                    'CheckID'   => $orderid,
                    'TimeChange'  => date('Y-m-d H:i:s'),
                    'Status'    => 1,
                    'UserID'    => $this->user->ID
                );
                $this->db->insert('ttp_report_inventory_check_history',$data_history);
                if($typeinvent==0){
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where WarehouseID=$warehouse");
                }
                $details_insert = array();
                $key = array_keys($data['products']);
                $total_products = count($key);
                $products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
                if(count($products)>0){
                    foreach ($products as $row) {
                        $amount = $data['products'][$row->ID];
                        $shipment = $this->db->query("select ID from ttp_report_shipment where ProductsID=$row->ID")->row();
                        $shipment = $shipment ? $shipment->ID : 0 ;
                        $old = $this->db->query("select sum(OnHand) as total from ttp_report_inventory where WarehouseID=$warehouse and ProductsID=$row->ID and LastEdited=1")->row();
                        $old = $old ? $old->total : 0 ;
                        $old = $old=='' ? 0 : $old ;
                        $details_insert[] = "($orderid,$row->ID,$shipment,$amount,$old)";
                        $this->change_inventory($row->ID,$shipment,$warehouse,$amount);
                    }
                    if(count($details_insert)>0){
                        $sql = "insert into ttp_report_inventory_check_details(CheckID,ProductsID,ShipmentID,OnHand,OnHandOld) values".implode(',',$details_insert);
                        $this->db->query($sql);
                    }
                }
				$warehouse = $this->db->query("select MaKho from ttp_report_warehouse where ID=$warehouse")->row();
				$warehouse = $warehouse ? $warehouse->MaKho : '##' ;
				$this->lib->write_log_data("<b>".$this->user->UserName."</b> thực hiện kiểm kho <b class='text-primary'>$warehouse</b>",0);
                $response = array("error"=>0,"message"=>"Áp dụng kiểm kho thành công !","id"=>$orderid);
                $data["customer"]["Active"] = $orderid;
                file_put_contents('log/pos/inventory/draft_order_'.$order.'.txt',json_encode($data));
            }else{
                $response = array("error"=>1,"message"=>"Áp dụng kiểm kho không thành công");
            }
        }
        echo json_encode($response);
    }

    public function change_inventory($ProductsID,$ShipmentID,$WarehouseID,$Amount){
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
        $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1");
        if($check_exists){
            if($check_exists->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=$Amount,OnHand=$Amount,LastEdited=1 where ID=$check_exists->ID");
            }else{
                $Available = $Amount;
                $OnHand = $Amount;
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
            }
        }else{
            $Available = $Amount;
            $OnHand = $Amount;
            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
        }
    }
}
?>
