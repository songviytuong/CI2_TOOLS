<?php 
class Manager_sms extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="manager_sms";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $bonus = "";
        $search = $this->session->userdata("report_filter_District_CategoriesID");
        $bonus = $search!='' ? " where Message like '%$search%' or Phone like '%$search%'" : '' ;
        $nav = $this->db->query("select count(1) as nav from ttp_report_sms $bonus")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_sms $bonus order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_sms/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_sms/index',5,$nav,$this->limit)
        );
        $this->template->add_title('SMS System | Manager Report Tools');
		$this->template->write_view('content','admin/manager_sms_home',$data);
		$this->template->render();
	}

    public function send_sms(){
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '' ;
        $message = isset($_POST['message']) ? $this->lib->asciiCharacter($_POST['message']) : '' ;
        $response['error'] = $this->sendsms($phone,$message);
        echo json_encode($response);
    }

    public function sendsms($PHONE='',$MESSAGE='',$SMSTYPE=0){
      $TYPE  = 1;
      $IDREQ  = time();
      $client  = new SoapClient("http://210.211.109.118/apibrandname/send?wsdl");
      $result  = $client->send(array("USERNAME" => SMS_USERNAME, "PASSWORD" => SMS_PASSWORD, "BRANDNAME" => SMS_BRANDNAME, "MESSAGE" => $MESSAGE, "TYPE" => $TYPE, "PHONE" => $PHONE, "IDREQ" => $IDREQ));
      $data = array(
          'Message' => $MESSAGE,
          'Phone'   => $PHONE,
          'Created' => date('Y-m-d H:i:s'),
          'Type'    => $SMSTYPE,
          'CodeStatus'=> $result->return->result
      );
      $this->db->insert('ttp_report_sms',$data);
      return $result->return->result;
    }
}
?>
