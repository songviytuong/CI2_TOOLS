<?php 
$url = base_url().ADMINPATH.'/report';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-th fa-fw"></i> Dashboards</p>
    <ul>
        <li><a href="<?php echo $url.'/manager_categories' ?>" <?php echo $segment_current=='manager_categories' ? "class='active'" : '' ; ?>>Nhóm ngành hàng</a></li>
        <li><a href="<?php echo $url.'/manager_district' ?>" <?php echo $segment_current=='manager_district' ? "class='active'" : '' ; ?>>Quận huyện</a></li>
        <li><a href="<?php echo $url.'/manager_city' ?>" <?php echo $segment_current=='manager_city' && $segment_current_bonus=='' ? "class='active'" : '' ; ?>>Tỉnh thành</a></li>
        <li><a href="<?php echo $url.'/manager_area' ?>" <?php echo $segment_current=='manager_area' ? "class='active'" : '' ; ?>>Khu vực</a></li>
        <li><a href="<?php echo $url.'/manager_department' ?>" <?php echo $segment_current=='manager_department' ? "class='active'" : '' ; ?>>Phòng ban</a></li>
        <li><a href="<?php echo $url.'/manager_team' ?>" <?php echo $segment_current=='manager_team' ? "class='active'" : '' ; ?>>Danh sách team</a></li>
        <li><a href="<?php echo $url.'/manager_transport' ?>" <?php echo $segment_current=='manager_transport' ? "class='active'" : '' ; ?>>Đơn vị vận chuyển</a></li>
        <li><a href="<?php echo $url.'/manager_sms' ?>" <?php echo $segment_current=='manager_sms' ? "class='active'" : '' ; ?>>Hệ thống SMS</a></li>
    </ul>
    <p class="title"><i class="fa fa-th fa-fw"></i> System config</p>
    <ul>
        <li><a href="<?php echo $url.'/manager_config' ?>" <?php echo $segment_current=='manager_config' ? "class='active'" : '' ; ?>>Cấu hình dữ liệu tĩnh</a></li>
        <li><a href="<?php echo $url.'/manager_system_menu' ?>" <?php echo $segment_current=='manager_system_menu' ? "class='active'" : '' ; ?>>Quản lý menu chức năng</a></li>
    </ul>
</div>