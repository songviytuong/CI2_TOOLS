<?php 
class Manager_city extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="manager_city";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function city_mapper(){
        $this->template->add_title('City Mapper | Manager Report Tools');
        $this->template->write_view('content','admin/manager_city_maper');
        $this->template->render();
    }

    public function save_city_mapper(){
        $CITY = isset($_POST['CITY']) ? $_POST['CITY'] : 0 ;
        $GHN = isset($_POST['GHN']) ? $_POST['GHN'] : 0 ;
        $GOLD = isset($_POST['GOLD']) ? $_POST['GOLD'] : 0 ;
        $this->db->query("update ttp_report_city set GHN='$GHN',GoldTimes='$GOLD' where ID=$CITY");
        echo "OK";
    }

    public function save_district_mapper(){
        $CITY = isset($_POST['CITY']) ? $_POST['CITY'] : 0 ;
        $GHN = isset($_POST['GHN']) ? $_POST['GHN'] : 0 ;
        $GOLD = isset($_POST['GOLD']) ? $_POST['GOLD'] : 0 ;
        $this->db->query("update ttp_report_district set GHN='$GHN',GoldTimes='$GOLD' where ID=$CITY");
        echo "OK";
    }

    public function district_mapper(){
        $this->template->add_title('District Mapper | Manager Report Tools');
        $this->template->write_view('content','admin/manager_district_maper');
        $this->template->render();
    }

    public function export(){
        $result = $this->db->query("select * from ttp_report_district")->result();
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        echo "<table>";
        foreach($result as $row){
            echo "<tr>
            <td>$row->Title</td>
            <td>$row->GHN</td>
            <td>$row->GoldTimes</td>
            </tr>";
        }
        echo "</table>";
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_city")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_city order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_city/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_city/index',5,$nav,$this->limit)
        );
        $this->template->add_title('City | Manager Report Tools');
		$this->template->write_view('content','admin/manager_city_home',$data);
		$this->template->render();
	}

    public function search($link='search'){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $CategoriesID = $this->session->userdata("report_filter_City_CategoriesID");
        $str_nav = "select count(1) as nav from ttp_report_city";
        $str = "select * from ttp_report_city";
        if($CategoriesID>0){
            $str.=" where AreaID=$CategoriesID";
            $str_nav.=" where AreaID=$CategoriesID";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data=array(
            'data'  => $this->db->query($str." order by ID DESC $limit_str")->result(),
            'nav'   => $this->lib->nav(base_url().ADMINPATH.'/report/manager_city/'.$link,5,$nav,$this->limit),
            'start' => $start,
            'find'      =>  $nav,
            'base_link' =>  base_url().ADMINPATH.'/report/manager_city/',
        );
        $this->template->write_view('content','admin/manager_city_home',$data);
        $this->template->render();
    }

    public function setsessionsearch(){
        if(isset($_POST['CategoriesID'])){
            $CategoriesID = mysql_real_escape_string($_POST['CategoriesID']);
            $this->session->set_userdata("report_filter_City_CategoriesID",$CategoriesID);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_City_CategoriesID");
        $this->search('setsessionsearch');
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('City add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_city/'
        );
        $this->template->write_view('content','admin/manager_city_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
        $DefaultWarehouse = isset($_POST['DefaultWarehouse']) ? $this->lib->fill_data($_POST['DefaultWarehouse']) : 0 ;
        if($Title!='' && $CategoriesID!=''){
            $data = array(
                'AreaID'        => $CategoriesID,
                'Title'         => $Title,
                'DefaultWarehouse'=>$DefaultWarehouse
            );
            $this->db->insert("ttp_report_city",$data);
        }
        redirect(ADMINPATH.'/report/manager_city/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_city where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_city where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit City | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_city/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_city_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $MT = isset($_POST['MT']) ? $_POST['MT'] : 0 ;
        $GT = isset($_POST['GT']) ? $_POST['GT'] : 0 ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
        $DefaultWarehouse = isset($_POST['DefaultWarehouse']) ? $this->lib->fill_data($_POST['DefaultWarehouse']) : 0 ;
        if($Title!='' && $CategoriesID!=''){
            $data = array(
                'AreaID'        => $CategoriesID,
                'Title'         => $Title,
                'DefaultWarehouse'=>$DefaultWarehouse,
                'MT'            =>$MT,
                'GT'            =>$GT
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_city",$data);
        }
        redirect(ADMINPATH.'/report/manager_city/');
    }
}
?>
