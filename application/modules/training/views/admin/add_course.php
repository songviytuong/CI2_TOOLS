<?php
$url = base_url().ADMINPATH.'/training';
?>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
    <!-- warning message -->
    <div class="alert alert_error hidden"><h5></h5><p></p></div>  
   <form id="fr_course" enctype="multipart/form-data" method="POST" accept-charset="utf-8">
      <div class="import_select_progress">
         <div class="block1">
            <h1>THÔNG TIN KHÓA HỌC</h1>
         </div>
         <div class="block2">
            <a onclick="saves(this);return false;" class="btn btn-primary btn_save" <?php echo isset($_GET['tab']) ? "style='display:none'" : '' ; ?>> <?php echo isset($data["title"]) ? "<i class='fa fa-floppy-o'></i> Lưu thay đổi" : "<i class='fa fa-check-circle'></i> Tạo khóa học" ; ?></a>
            <a onclick="goBack();return false;" align="right" class="btn btn-danger"><i class="fa fa-times"></i> Đóng</a>
         </div>
      </div>
      <div class="box_training_tab">
         <div class="control_tab">
            <a data="tab1" <?php echo !isset($_GET['tab']) ? "class='current'" : '' ; ?>>Thông tin chung</a>
            <?php if(isset($data["id"]) > 0) { ?>
            <a data="tab2" <?php echo isset($_GET['tab']) ? "class='current'" : '' ; ?>>Bài học</a>
            <?php } ?>
         </div>
         <!-- Tab panes -->
         <div class="tab-content">
            <div class="content_tab content_tab1" id="home" <?php echo !isset($_GET['tab']) ? "" : "style='display:none'" ; ?>>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Tên khóa học: </label>
                        <div class="col-xs-9">
                           <input class="form-control" id="title" name='title' placeholder="Khóa học mới..." type="text" value="<?php echo isset($data["title"]) ? $data["title"] : "" ; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label for="select" class="col-xs-3 control-label">Nhóm khóa học: </label>
                        <div class="col-xs-9">
                            <div class="input-group">
                                <span class="input-group-addon btn" onclick="edit_group(this)"><i class="fa fa-pencil-square-o"></i></span>
                                <select class="form-control" name="groupid" id="groupid">
                                  <option value="0">Chọn nhóm</option>
                                  <?php foreach ($group as $key => $value) {
                                  $sl= '';
                                  if($data["groupid"] ==$value["id"]){
                                  $sl = 'selected="selected"';
                                  }
                                  ?>
                                  <option <?php echo $sl; ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"] ?></option>
                                  <?php } ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" onclick="add_group(this)"><i class="fa fa-plus"></i> Thêm</button>
                                </span>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label">Link Video: </label>
                        <div class="col-xs-9">
                           <input class="form-control" id="video" name='video' placeholder="Link video nếu có ..." type="text">
                        </div>
                     </div>   
                  </div>
                  <div class="col-xs-6">
                     <div class="form-group">
                        <label for="select" class="col-xs-3 control-label">Cấp độ: </label>
                        <div class="col-xs-9">
                            <div class="input-group">
                                <span class="input-group-addon btn" onclick="edit_level(this)"><i class="fa fa-pencil-square-o"></i></span>
                                <select class="form-control" name="level" id="level">
                                  <option value="0">Chọn cấp độ</option>
                                  <?php foreach ($level as $key => $value) {
                                  $sl2 = '';
                                  if($data["level"] ==$value["id"]){
                                  $sl2 = 'selected="selected"';
                                  }
                                  ?>
                                  <option <?php echo $sl2; ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"] ?></option>
                                  <?php } ?>
                               </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" onclick="add_level(this)"><i class="fa fa-plus"></i> Thêm</button>
                                </span>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Kiểm tra nhanh: </label>
                            <div class="col-xs-9">
                                <div class="radio">
                                    <label>
                                        <input onclick="show_position_lesson()" type="radio" value="1" <?php echo isset($data["fast_test"]) && $data["fast_test"]==1 ? "checked='checked'" : '' ; ?> name="fast_test" >
                                        Có
                                    </label>
                                    <label>
                                        <input onclick="hide_position_lesson()" type="radio" <?php echo (isset($data["fast_test"]) && $data["fast_test"]==0) || !isset($data["fast_test"]) ? "checked='checked'" : '' ; ?> name="fast_test" value="0">
                                        Không
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Hình đại diện : </label>
                            <div class="col-xs-9">
                                <input type='file' id="choosefile" name='Image_upload' />
                                <div class="dvPreview">
                                  <?php 
                                  if(isset($data["img"]) && isset($data["img"])!=''){
                                    echo "<img style='width:auto;height:60px' src='".base_url().$data["img"]."' />";
                                  }
                                  ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row <?php echo isset($data["fast_test"]) && $data["fast_test"]==1 ? "" : 'hidden' ; ?>" id="position_lesson">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Bài kiểm tra nhanh: </label>
                            <div class="col-xs-9">
                               <select class="form-control" name="point_test">
                                   <option value="0">-- Chọn bài kiểm tra --</option>
                                   <?php 
                                    if(isset($data['id'])){
                                        $lesson_test = $this->db->query("select a.title,a.id,b.title as ChapterTitle from ttp_training_lesson a,ttp_training_chapter b where a.Type=3 and a.enabled=1 and a.chapterid=b.id and a.courseid=".$data['id'])->result();
                                        if(count($lesson_test)>0){
                                            foreach($lesson_test as $row){
                                                $selected= $data['point_test']==$row->id ? 'selected="selected"' : '' ;
                                                echo "<option value='$row->id' $selected>$row->ChapterTitle | $row->title</option>";
                                            }
                                        }
                                    }
                                   ?>
                               </select>
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                          <label class="control-label">Mô tả ngắn: </label>
                          <textarea name="soft" id="soft" class="form-control"><?php echo isset($data["des_soft"]) ? $data["des_soft"] : "" ; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Mô tả dài: </label>
                          <textarea id="des_full" class="form-control ckeditor" name="des_full"><?php echo isset($data["des_full"]) ? $data["des_full"] : "" ; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Bạn học được gì: </label>
                          <textarea id="study" class="form-control ckeditor" name="study"><?php echo isset($data["study"]) ? $data["study"] : "" ; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Yêu cầu khi học: </label>
                          <textarea class="form-control ckeditor" id="request" name="request"><?php echo isset($data["request"]) ? $data["request"] : "" ; ?></textarea>
                        </div>
                    </div>   
                </div>

                <div>
                  <h5>Phụ lục khóa học : 
                    <div class="pull-right">
                      <a class="btn btn-default" onclick="add_part()"><i class="fa fa-plus"></i> Tạo phần</a>
                      <a class="btn btn-default" onclick="add_chapter()"><i class="fa fa-plus"></i> Tạo chương</a>
                    </div>
                  </h5>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="table_data">
                            <table id="table_data_part">
                                <tr>
                                    <th class="th_stt">STT</th>
                                    <th>Tên phần</th>
                                    <th class="th_action">Thao tác</th>
                                </tr>
                                <?php 
                                if(isset($data['id'])){
                                    $part = $this->db->query("select * from ttp_training_part where scienceid=".$data['id'])->result();
                                    if(count($part)>0){
                                        $i=1;
                                        foreach($part as $row){
                                            echo "<tr>
                                                <td>$i</td>
                                                <td><span class='data_change'>$row->title</span></td>
                                                <td><a class='td_link_edit' title='Chỉnh sửa' onclick='edit_part(this,$row->id)'><i class='fa fa-pencil-square-o'></i> Chỉnh sửa</a><a onclick='del_part(this,$row->id)'><i class='fa fa-trash-o'></i> Xóa</a></td>
                                            </tr>";
                                            $i++;
                                        }
                                    }
                                }else{
                                  echo "<tr class='empty'><td colspan='3'>Không có phần nào trong khóa học này !</td></tr>";
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="table_data">
                            <table id="table_data_chapter">
                                <tr>
                                    <th class="th_stt">STT</th>
                                    <th>Tên chương</th>
                                    <th class="th_action">Thao tác</th>
                                </tr>
                                <?php 
                                if(isset($data['id'])){
                                    $part = $this->db->query("select * from ttp_training_chapter where courseid=".$data['id'])->result();
                                    if(count($part)>0){
                                        $i=1;
                                        foreach($part as $row){
                                            echo "<tr>
                                                <td>$i</td>
                                                <td><span class='data_change data_change_$row->id'>$row->title</span></td>
                                                <td><a class='td_link_edit' title='Chỉnh sửa' onclick='edit_chapter(this,$row->id)'><i class='fa fa-pencil-square-o'></i> Chỉnh sửa</a> <a onclick='del_chapter(this,$row->id)'><i class='fa fa-trash-o'></i> Xóa</a></td>
                                            </tr>";
                                            $i++;
                                        }
                                    }
                                }else{
                                  echo "<tr class='empty'><td colspan='3'>Không có chương nào trong khóa học này !</td></tr>";
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>

               <input type="hidden" id="full" name="full"></input>
               <input type="hidden" id="sstudy" name="sstudy"></input>
               <input type="hidden" id="srequest" name="srequest"></input>
               <input type="hidden" name="id" id="id" value="<?php echo isset($data["id"]) ? $data["id"] : 0; ?>"></input>

            </div>
            <!-- end thông tin chung -->
            <div class="content_tab content_tab2" id="lesson" <?php echo isset($_GET['tab']) ? "style='display:block'" : '' ; ?>>
               <div class="title_tab">
                  <div class="title_tab_button">
                     <a onclick="save_order(this);return false;" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Lưu thứ tự</a>
                     <a href="<?php echo $url;?>/training/add_lesson/0/<?php echo isset($data["id"]) ? $data["id"] : 0; ?>" class="btn btn-danger pull-left"><i class="fa fa-plus"></i> Thêm bài học</a>
                  </div>
               </div>
               <div class="table_data">
                   <table>
                      <tr>
                         <th>STT</th>
                         <th>Tên bài học</th>
                         <th>Chương</th>
                         <th>Loại bài học</th>
                         <th class="th_action_min">Sắp xếp</th>
                         <th class="th_action">Thao tác</th>
                      </tr>
                      <?php
                      if(isset($list_lesson) && count($list_lesson)>0){
                      $stt =0;
                      foreach ($list_lesson as $key => $value) {
                      ?>
                      <tr>
                         <td> <?php echo ++$stt; ?></td>
                         <td> <a target="_blank" href="<?php echo $url;?>/training/add_lesson/<?php echo $value["id"];?>"><?php echo $value["title"]; ?></a></td>
                         <td> <?php echo $chapter_name[$value["chapterid"]]; ?></td>
                         <td> <?php echo $type[$value["type"]]; ?></td>
                         <td><input class="form-control input_stt" type="text" name="orderby[]" id="orderby" value="<?php echo isset($value["orderby"]) && $value["orderby"]>0 ? $value["orderby"] : $stt; ?>"></input></td>
                         <td> <a class="td_link_edit" href="<?php echo $url;?>/training/add_lesson/<?php echo $value["id"];?>"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a> <a onclick="del_lesson(this,<?php echo $value["id"];?>);return false;"><i class="fa fa-trash-o"> </i> Xóa</a></td>
                      </tr>
                      <?php } ?>
                      <?php }else{
                        echo "<tr><td colspan='6'>Khóa học hiện tại chưa có bài học .</td></tr>";
                        } ?>
                   </table>
               </div>
            </div>
         </div>
         <input id="tid" type="hidden" name="tid" value="0"></input>
         <input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
         <input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
      </div>
   </form>
   <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".control_tab a").click(function(){
        $(".control_tab a").removeClass("current");
        var data = $(this).attr('data');
        $(this).addClass("current");
        $(".content_tab").hide();
        $(".content_"+data).show();
        $("#currenttab").val(data);
        if(data=='tab1'){
            $(".btn_save").show();
        }else{
            $(".btn_save").hide();
        }
    });

    function add_chapter(){
        var courseid = $('#id').val();
        if(courseid==0){
            var des_full = CKEDITOR.instances.des_full.getData();
            var study = CKEDITOR.instances.study.getData();
            var request = CKEDITOR.instances.request.getData();
            $('#full').val(des_full);
            $('#sstudy').val(study);
            $('#srequest').val(request);
            var link = $('#link').val();
            $.post(link+"/training/save_course/", $("form").serialize(),function(resp){
                $.each(resp, function (i, obj) {
                    var id = obj.id;
                    var msg = obj.msg;
                    if(msg != "ok"){
                        $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                        $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ");
                        $(".alert_error p").html(msg);
                        $('#' + id).focus();
                        $(ob).removeClass('saving');
                        $(ob).find('i').show();
                    }else{
                        window.location=link+"/training/add_course/"+id+"?showpopup=chapter";
                    }
                });
            }, 'json');
        }else{
            var link = $('#link').val();
            enablescrollsetup();
            $(".over_lay").show();
            $(".over_lay .block1_inner h1").html("Tạo chương mới");
            $(".over_lay .block2_inner").load(link+'/training/add_chapter_view/'+courseid);
        }
    }

    <?php 
    $showpopup = isset($_GET['showpopup']) ? $_GET['showpopup'] : '' ;
    if($showpopup=='chapter'){
        echo "add_chapter();";
    }
    ?>

    function edit_chapter(ob,ChapterID){
        var link = $('#link').val();
        var id = $("#id").val();
        enablescrollsetup();
        $(".over_lay").show();
        $(".over_lay .block1_inner h1").html("Chỉnh sửa thông tin chương");
        $(".over_lay .block2_inner").load(link+'/training/add_chapter_view/'+id+'/'+ChapterID);
    }

    function del_chapter(ob,ID){
        var result = confirm('Bạn có chắc chắn muốn xóa không');
        if(result){
            var link = $('#link').val();
            $.post(link+"/training/del_chapter/"+ID, $("form").serialize(),function(resp){
                $(ob).parent('td').parent('tr').fadeOut();
            });
        }
    }

    $("#close_overlay").click(function(){
        $(".over_lay").hide();
        disablescrollsetup();
        $(".over_lay .block2_inner").html('');
        $(window).scrollTop($(document).height());
    });

    function enablescrollsetup(){
        $("body").css({'height':'100%','overflow-y':'hidden'});
        h = window.innerHeight;
        h = h-200;
        $(window).scrollTop(70);
        $(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
    }

    function disablescrollsetup(){
        $("body").css({'height':'auto','overflow-y':'scroll'});
    }
    
    function goBack() {
        var link = $('#link').val();
        window.location = link+'/course/slist';
    }

    function del_lesson(ob,id){
        var result = confirm('Bạn có chắc chắn muốn xóa không');
        if(result){
            var link = $('#link').val();
            $.post(link+"/course/delete_lesson/"+id, $("form").serialize(),function(resp){
                $(ob).parent('td').parent('tr').fadeOut();
            });
        }
    }

    function del_answer(id){
        var result = confirm('Bạn có chắc chắn muốn xóa không');
        if(result){
            var link = $('#link').val();
            $.post(link+"/course/delete_question/"+id, $("form").serialize(),function(resp){
                location.reload();
            });
        }
    }

    function add_group(ob){
        var getValue = prompt("Điền tên nhóm mới : ", "");
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/add_new_group";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue,
                success: function(result){
                    if(result!='False'){
                        $('#groupid').append("<option value='"+result+"'>"+getValue+"</option>");
                        $('#groupid').val(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function edit_group(){
        var ID = $('#groupid').val();
        var text = $('#groupid :selected').text();
        var getValue = prompt("Điền tên nhóm mới thay cho tên nhóm cũ : ", text);
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/update_group";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&ID="+ID,
                success: function(result){
                    if(result!='False'){
                        $('#groupid :selected').text(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function add_level(ob){
        var getValue = prompt("Điền loại cấp độ mới : ", "");
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/add_new_level";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue,
                success: function(result){
                    if(result!='False'){
                        $('#level').append("<option value='"+result+"'>"+getValue+"</option>");
                        $('#level').val(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function edit_level(){
        var ID = $('#level').val();
        var text = $('#level :selected').text();
        var getValue = prompt("Điền tên nhóm mới thay cho tên nhóm cũ : ", text);
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/update_level";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&ID="+ID,
                success: function(result){
                    if(result!='False'){
                        $('#level :selected').text(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function saves(ob){
        $(ob).addClass('saving');
        $(ob).find('i').hide();
        var des_full = CKEDITOR.instances.des_full.getData();
        var study = CKEDITOR.instances.study.getData();
        var request = CKEDITOR.instances.request.getData();
        $('#full').val(des_full);
        $('#sstudy').val(study);
        $('#srequest').val(request);
        var link = $('#link').val();
        var formData = new FormData($("form")[0]);
        $.ajax({
            url: link+"/training/save_course/",
            type: "POST",
            data: formData,
            async: false,
            dataType:"json",
            success: function (result) {
              console.log(result);
                var id = result.id;
                var msg = result.msg;
                if(msg != "ok"){
                    $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                    $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ");
                    $(".alert_error p").html(msg);
                    $('#' + id).focus();
                    $(ob).removeClass('saving');
                    $(ob).find('i').show();
                }else{
                    var courseid = $("#id").val();
                    if(courseid!=0){
                        $("#id").val(id);
                        $(".alert_error").removeClass('alert-danger').addClass("alert-success").removeClass('hidden');
                        $(".alert_error h5").hide();
                        $(".alert_error p").html("<i class='fa fa-check-circle'></i> Lưu dữ liệu thành công !");
                        $(ob).removeClass('saving');
                        $(ob).find('i').show();
                    }else{
                        window.location=link+"/training/add_course/"+id+"?tab";
                    }
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

    function save_order(ob){
       $(ob).addClass("saving");
       $(ob).find('i').hide();
       var link = $('#link').val();
       var courseid = $('#id').val();
       $.post(link+"/training/save_order_lesson/", $("form").serialize(),function(resp){
            $(ob).removeClass("saving");
            $(ob).find('i').show();
            $(".alert_error").removeClass('alert-danger').addClass("alert-success").removeClass('hidden');
            $(".alert_error h5").hide();
            $(".alert_error p").html("<i class='fa fa-check-circle'></i> Lưu dữ liệu thành công !");
       }, 'json');
    }

    function add_part(){
        var courseid = $('#id').val();
        if(courseid==0){
            var des_full = CKEDITOR.instances.des_full.getData();
            var study = CKEDITOR.instances.study.getData();
            var request = CKEDITOR.instances.request.getData();
            $('#full').val(des_full);
            $('#sstudy').val(study);
            $('#srequest').val(request);
            var link = $('#link').val();
            $.post(link+"/training/save_course/", $("form").serialize(),function(resp){
                $.each(resp, function (i, obj) {
                    var id = obj.id;
                    var msg = obj.msg;
                    if(msg != "ok"){
                        $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                        $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ");
                        $(".alert_error p").html(msg);
                        $('#' + id).focus();
                        $(ob).removeClass('saving');
                        $(ob).find('i').show();
                    }else{
                        window.location=link+"/training/add_course/"+id+"?showpopup=part";
                    }
                });
            }, 'json');
        }else{
          var getValue = prompt("Điền tên phần mới : ", "");
          if(getValue!=null){
              var link = $('#link').val();
              var url = link+"/training/add_new_part";
              $.ajax({
                  dataType: "html",
                  type: "POST",
                  url: url,
                  data: "Title="+getValue+"&CoursesID="+courseid,
                  success: function(result){
                      if(result!='False'){
                          var rowCount = $('table#table_data_part tr:last').index()+1;
                          var stt = $('table#table_data_part tr:last').index()+1;
                          var table = document.getElementById("table_data_part");
                          var row = table.insertRow(rowCount);
                          row.insertCell(0).innerHTML = stt;
                          row.insertCell(1).innerHTML = "<span class='data_change'>"+getValue+"</span>";
                          row.insertCell(2).innerHTML = "<a class='td_link_edit' onclick='edit_part(this,"+result+")' title='Chỉnh sửa' onclick='edit_part()'><i class='fa fa-pencil-square-o'></i> Chỉnh sửa</a><a onclick='del_part(this,"+result+")'><i class='fa fa-trash-o fa-lg'></i> Xóa</a>";
                          <?php 
                          $showpopup = isset($_GET['showpopup']) ? $_GET['showpopup'] : '' ;
                          if($showpopup=='part'){
                              echo "window.location=link+'/training/add_course/'+courseid;";
                          }
                          ?>
                      }else{
                          alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                      }
                  }
              });
          }
        }
    }

    <?php 
    $showpopup = isset($_GET['showpopup']) ? $_GET['showpopup'] : '' ;
    if($showpopup=='part'){
        echo "add_part();";
    }
    ?>

    function edit_part(ob,ID){
        var text = $(ob).parent('td').parent('tr').find('.data_change').html();
        var getValue = prompt("Điền tên phần mới thay cho tên phần cũ : ", text);
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/update_part";
            var courseid = $('#id').val();
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&ID="+ID+"&CoursesID="+courseid,
                success: function(result){
                    if(result!='False'){
                        $(ob).parent('td').parent('tr').find('.data_change').html(getValue);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function del_part(ob,ID){
        if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
            return false;
        }else{
            var link = $('#link').val();
            var url = link+"/training/del_part";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "ID="+ID,
                success: function(result){
                    if(result!='False'){
                        $(ob).parent('td').parent('tr').fadeOut();
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    $(document).ready(function(){
        $("#choosefile").change(function(){
        var Fileinput = document.getElementById("choosefile");
        var file = Fileinput.files[0];
        var imageType = /image.*/
        var dvPreview = $(".dvPreview");
        if(file.type.match(imageType)){
        var reader = new FileReader();
        reader.onload = function (e) {
        var img = $("<img />");
        img.attr("style", "max-height:60px;max-width: 60px");
        img.attr("src", e.target.result);
        dvPreview.html(img);
        }
        reader.readAsDataURL(file);
        }else{
        console.log("Not an Image");
        }
        });
    });

    function show_position_lesson(){
        $("#position_lesson").removeClass('hidden');
    }

    function hide_position_lesson(){
        $("#position_lesson").addClass('hidden');
    }

</script>