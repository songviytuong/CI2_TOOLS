<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách đối tác bán hàng</h1>
			</div>
			<div class="block2">
				<form action="<?php echo base_url().ADMINPATH."/report/partner_affiliate/information" ?>" method="get">
					<a class="pull-left btn"><i class="fa fa-search" style="font-size:16px"></i></a><input type="text" class="form-control" name="keywords" value="<?php echo isset($_GET['keywords']) ? strip_tags($_GET['keywords']) : '' ; ?>" placeholder="Tìm tài khoản" style="width:250px;border:none;box-shadow:none;padding-left:0px;padding-right:0px;">
				</form>
			</div>
		</div>
		<table class="table table-bordered table-hover">
			<tr>
				<th>STT</th>
				<th>Tài khoản</th>
				<th>Họ tên</th>
				<th>Số tài khoản</th>
				<th>Tên ngân hàng</th>
				<th>Cấp độ</th>
				<th>% chiết khấu</th>
				<th>Ngày đăng ký</th>
			</tr>
			<?php 
			if(count($data)>0){
				$i = $start+1;
				$arr = $this->lib->get_config_define("affiliate","level");
				$arr1 = $this->lib->get_config_define("affiliate","discount");
				foreach($data as $row){
					$info = json_decode($row->Data);
					$Fullname = isset($info->Fullname) ? $info->Fullname : '--' ;
					$Bankcode = isset($info->Bankcode) ? $info->Bankcode : '--' ;
					$Banktitle = isset($info->Banktitle) ? $info->Banktitle : '--' ;
 					echo "<tr>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>$i</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>$row->UserName</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>$Fullname</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>$Bankcode</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>$Banktitle</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>".$arr[$row->Level]."</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>".$arr1[$row->Level]."</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_affiliate\")'>$row->Created</a></td>";
					echo "</tr>";
					$i++;
				}
			}else{
				echo "<tr><td colspan='8'>Không tìm thấy dữ liệu</td></tr>";
			}
			?>
		</table>
		<?php echo $nav ?>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script>
	var baselink = $("#baselink_report").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function load_edit(id,action){
		enablescrollsetup();
		$(".over_lay .box_inner").css({'margin-top':'50px'});
		$(".over_lay .box_inner .block1_inner h1").html("Thông tin đối tác bán hàng");
		$(".over_lay .box_inner .block2_inner").load(baselink+"partner_affiliate/"+action+"/"+id);
		$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}
</script>