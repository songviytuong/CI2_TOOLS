<?php 
$url = base_url().ADMINPATH.'/training';
?>
<form action="" id="form_search" method='post'>
<div class="containner">
	<div class="manager">
		<div class="import_select_progress">
	        <div class="block1">
	            <h1>DANH SÁCH KHÓA HỌC</h1>
	        </div>
	        <div class="block2">
	        	<div class="input-group">
		            <input type="text" class="form-control" placeholder="Tìm khóa học..." class="form-control" value="<?php echo  isset($kq) ? $kq : '' ?>" name="kq" id="kq" ></input>
		            <span class="input-group-btn"><a class="btn btn-primary" onclick="searchs();return false;"><i class="fa fa-search"></i></a></span>
	            </div>
	        </div>
	    </div>
		<div class="table_data">
		    <table>
		    	<tr>
		    		<th>STT</th>
		    		<th>Tên khóa học</th>
		    		<th>Nhóm khóa học</th>
		    		<th>Thời lượng</th>
		    		<th>Cấp độ</th>
		    		<th>Ngày yêu cầu</th>
		    		<th>Ngày hoàn tất</th>
		    		<th class="th_state">Tình trạng</th>
		    	</tr>
		    	<?php
		    	$stt =0;
		    	$arr = array();
		    	if(count($list)>0){
			    	foreach ($list as $row) {
			    	 	if(!in_array($row->courseid,$arr)){
			    	 		$arr[]=$row->courseid;
			    	 	}
			    	 	$status = '';
			    	 	if($row->status == 0){
			    	 		$status = '<td class="text-warning"><i class="fa fa-minus-circle"></i> Chưa bắt đầu</td>';
			    	 	}
			    	 	if($row->status == 1){
			    	 		$status = '<td class="text-success"><i class="fa fa-check-circle"></i> Hoàn thành</td>';
			    	 	}
			    	 	if($row->status == 2){
			    	 		$status = '<td class="text-danger"><i class="fa fa-info-circle"></i> Chưa hoàn thành</td>';
			    	 	}

			    		?>
			    		<tr>
			    		<td> <?php echo ++$stt; ?></td>
			    		<td> <a href="<?php echo $url;?>/learn/detail/<?php echo $row->courseid;?>"><?php echo $row->title; ?></a></td>
			    		<td> <?php echo $row->group_title; ?></td>
			    		<td><span class='hour_course_<?php echo $row->courseid; ?>'></span></td>
			    		<td> <?php echo $row->level_title; ?></td>
			    		<td> <?php echo date('d/m/Y',strtotime($row->deathline)); ?></td>
			    		<td> <?php echo isset($row->enddate) && $row->enddate!='0000-00-00 00:00:00' ? date('d/m/Y',strtotime($row->enddate)) : ''; ?></td>
			    		<?php echo $status ?>
			    		</tr>
			    	<?php 
			    	}
			    	$list_array = implode(',',$arr);
			    	if($list_array!=''){
			    		$data = $this->db->query("select courseid,sum(hour) as total from ttp_training_lesson where courseid in($list_array) group by courseid")->result();
			    		if(count($data)>0){
			    			$str = '';
			    			foreach($data as $row){
			    				$temp_hour = $row->total==0 ? 0 : round($row->total/60);
			    				$temp_hour = $temp_hour>0 ? $temp_hour.' giờ ' : '' ;
			    				$temp_minus = $row->total==0 ? 0 : $row->total%60;
			    				$str.="$('.hour_course_".$row->courseid."').html('".$temp_hour."$temp_minus phút');";
			    			}
			    		}
			    	}
		    	}
		    	?>
		    </table>
		</div>
	</div>
</div>
</form>
<script>
	<?php 
	if(isset($str) && $str!=''){
		echo $str;
	}
	?>
	function searchs(){
		$('#form_search').submit();
	}
</script>