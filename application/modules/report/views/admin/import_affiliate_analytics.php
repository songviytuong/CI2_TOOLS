<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$numday = $this->lib->get_nume_day($startday,$stopday);
$arr_day = array();
$arr_refer = array();
$arr_products = array();
$total = 0;
if(count($data)>0){
    foreach($data as $row){
        $keytime = date('Y-m-d',strtotime($row->AnalyticsDay));
        if(isset($arr_day[$keytime]['Mobile'])){
            $arr_day[$keytime]['Mobile'] = $arr_day[$keytime]['Mobile']+$row->Mobile;
        }else{
            $arr_day[$keytime]['Mobile'] = $row->Mobile;
        }

        if(isset($arr_day[$keytime]['Browser'])){
            $arr_day[$keytime]['Browser'] = $arr_day[$keytime]['Browser']+$row->Browser;
        }else{
            $arr_day[$keytime]['Browser'] = $row->Browser;
        }
        if(isset($arr_refer["$row->Referer"])){
            $arr_refer["$row->Referer"]['Total'] = $arr_refer["$row->Referer"]['Total']+$row->Quantity;
            $arr_refer["$row->Referer"]['Mobile'] = $arr_refer["$row->Referer"]['Mobile']+$row->Mobile;
            $arr_refer["$row->Referer"]['Browser'] = $arr_refer["$row->Referer"]['Browser']+$row->Browser;
        }else{
            $arr_refer["$row->Referer"]['Referer'] = $row->Referer;
            $arr_refer["$row->Referer"]['Total'] = $row->Quantity;
            $arr_refer["$row->Referer"]['Mobile'] = $row->Mobile;
            $arr_refer["$row->Referer"]['Browser'] = $row->Browser;
        }
        if(isset($arr_products[$row->ProductsID])){
            $arr_products[$row->ProductsID]['Total'] = $arr_products[$row->ProductsID]['Total']+$row->Quantity;
            $arr_products[$row->ProductsID]['Mobile'] = $arr_products[$row->ProductsID]['Mobile']+$row->Mobile;
            $arr_products[$row->ProductsID]['Browser'] = $arr_products[$row->ProductsID]['Browser']+$row->Browser;
        }else{
            $arr_products[$row->ProductsID]['Title'] = $row->Title;
            $arr_products[$row->ProductsID]['SKU'] = $row->MaSP;
            $arr_products[$row->ProductsID]['Total'] = $row->Quantity;
            $arr_products[$row->ProductsID]['Mobile'] = $row->Mobile;
            $arr_products[$row->ProductsID]['Browser'] = $row->Browser;
        }
        $total = $total+$row->Quantity;
    }
}
$percent = $total==0 ? 0 : ($ordersuccess/$total)*100;
?>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<h1 style="font-size: 20px;font-weight: bold;">BÁO CÁO TỔNG QUAN LƯỢT TRUY CẬP</h1>
	    </div>
	    <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
	    </div>
    </div>
    <div class="row">
    	<div id="chart_div" style="width:100%;height:400px;"></div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-4">
            <h5>Tổng lượt truy cập</h5>
            <h2><?php echo number_format($total) ?></h2>
        </div>
        <div class="col-xs-4">
            <h5>Số sản phẩm có lượt truy cập</h5>
            <h2><?php echo number_format(count($arr_products)) ?></h2>
        </div>
        <div class="col-xs-4">
            <h5>Tỉ lệ chuyển đổi thành đơn hàng</h5>
            <h2><?php echo number_format($percent,2) ?>%</h2>
        </div>
    </div>
    <hr>
    <div class="row" style="margin:0px">
        <div class="form-group">
            <div class="col-xs-6">
            <h4>Lượt truy cập đến sản phẩm đích</h4>
            <hr>
            <table class="table table-bordered table-hovered">
                <tr>
                    <th>STT</th>
                    <th>SKU</th>
                    <th>Tên sản phẩm</th>
                    <th>Điện thoại</th>
                    <th>Máy tính</th>
                </tr>
            <?php 
            if(count($arr_products)>0){
                $i=1;
                $total_mobile = 0;
                $total_browser = 0;
                foreach($arr_products as $row){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td>".$row['SKU']."</td>";
                    echo "<td>".$row['Title']."</td>";
                    echo "<td class='text-right'>".number_format($row['Mobile'])."</td>";
                    echo "<td class='text-right'>".number_format($row['Browser'])."</td>";
                    echo "</tr>";
                    $total_mobile = $total_mobile+$row['Mobile'];
                    $total_browser = $total_browser+$row['Browser'];
                    $i++;
                }
                echo "<tr><th colspan='3' class='text-center'>TỔNG CỘNG</th><th class='text-right'>".number_format($total_mobile)."</th><th class='text-right'>".number_format($total_browser)."</th></tr>";
            }else{
                echo "<tr><th colspan='5'>Không tìm thấy dữ liệu</th></tr>";
            }
            ?>
            </table>
            </div>
            <div class="col-xs-6">
                <div id="piechart1" style="width: 100%; height: 300px;"></div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row" style="margin:0px">
        <div class="form-group">
            <div class="col-xs-6">
            <h4>Các nguồn truy cập từ link của bạn</h4>
            <hr>
            <table class="table table-bordered table-hovered">
                <tr>
                    <th>STT</th>
                    <th>Nguồn truy cập</th>
                    <th>Lượt truy cập</th>
                    <th>Điện thoại</th>
                    <th>Máy tính</th>
                </tr>
        	<?php 
            if(count($arr_refer)>0){
                $i=1;
                $total_mobile = 0;
                $total_browser = 0;
                $total_all = 0;
                foreach($arr_refer as $row){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td>".$row['Referer']."</td>";
                    echo "<td class='text-right'>".number_format($row['Total'])."</td>";
                    echo "<td class='text-right'>".number_format($row['Mobile'])."</td>";
                    echo "<td class='text-right'>".number_format($row['Browser'])."</td>";
                    echo "</tr>";
                    $i++;
                    $total_mobile = $total_mobile+$row['Mobile'];
                    $total_browser = $total_browser+$row['Browser'];
                    $total_all = $total_all+$row['Total'];
                }
                echo "<tr><th colspan='2' class='text-center'>TỔNG CỘNG</th><th class='text-right'>".number_format($total_all)."</th><th class='text-right'>".number_format($total_mobile)."</th><th class='text-right'>".number_format($total_browser)."</th></tr>";
            }else{
                echo "<tr><td colspan='5'>Không tìm thấy dữ liệu</td></tr>";
            }
            ?>
            </table>
            </div>
            <div class="col-xs-6">
                <div id="piechart" style="width: 100%; height: 300px;"></div>
            </div>
        </div>
    </div>
	<div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<style>
	.col-xs-4{border-right:1px solid #eee;margin-bottom:20px;min-height: 100px;}
	.col-xs-4 h5{text-transform: uppercase;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    location.reload();
                }
            }); 
        });
    });
</script>
<!-- /datepicker -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Ngày thống kê', 'Di động', 'Desktop'],
      <?php 
        $day = array();
        for ($i=0; $i < $numday+1; $i++) { 
            $time = date('d/m',strtotime($startday)+$i*3600*24);
            $keytime = date('Y-m-d',strtotime($startday)+$i*3600*24);
            $Mobile = isset($arr_day[$keytime]['Mobile']) ? $arr_day[$keytime]['Mobile'] : 0 ;
            $Browser = isset($arr_day[$keytime]['Browser']) ? $arr_day[$keytime]['Browser'] : 0 ;
            $day[] = "['$time',$Mobile,$Browser]";
        }
        echo implode(',', $day);
        ?>]);

    var options = {
        title: 'Biểu đồ lượt truy cập theo ngày từ link chia sẻ',
        hAxis: {title: 'Ngày thống kê',  titleTextStyle: {color: '#333'}},
        vAxis: {minValue: 0}
    };

    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  
    var data1 = google.visualization.arrayToDataTable([
      <?php 
        if(count($arr_refer)>0){
            echo "['Nguồn truy cập', 'Lượt truy cập']";
            foreach($arr_refer as $row){
                echo ",['".$row['Referer']."',".$row['Total']."]";
            }
        }
      ?>
    ]);

    var options1 = {
      title: 'Thống kê truy cập theo nguồn'
    };

    var chart1 = new google.visualization.PieChart(document.getElementById('piechart'));

    chart1.draw(data1, options1);

    var data2 = google.visualization.arrayToDataTable([
      <?php 
        if(count($arr_products)>0){
            echo "['Sản phẩm', 'Lượt truy cập']";
            foreach($arr_products as $row){
                echo ",['".$row['SKU']."',".$row['Total']."]";
            }
        }
      ?>
    ]);

    var options2 = {
      title: 'Thống kê truy cập đến sản phẩm'
    };

    var chart2 = new google.visualization.PieChart(document.getElementById('piechart1'));

    chart2.draw(data2, options2);
  }
</script>