<?php 
$url = base_url().ADMINPATH.'/training/group';
?>
<form action="" id="form_search" method='post'>
<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách nhóm khóa học</h1>
	        </div>
			<div class="block2">
				<a onclick="add(this)" class="btn btn-danger"><i class="fa fa-plus"></i> Tạo nhóm</a>
			</div>
		</div>
		<div class="table_data">	
		    <table id="table_data">
	            <thead>
		    	<tr>
		    		<th>STT</th>
		    		<th>Tên nhóm</th>
		    		<th class="th_action">Thao tác</th>
		    	</tr>
		    	</thead>
		    	<tbody>
		    	<?php
		    	if(count($data)>0){
		    		$stt =0;
		    	 	foreach ($data as $row) {
			    	 	$stt++;
			    	 	echo "<tr>";
			    	 	echo "<td>$stt</td>";
			    	 	echo "<td><span class='title_group'>$row->title</span></td>";
			    	 	echo "<td><a onclick='edit(this,$row->id)'><i class='fa fa-pencil-square-o'></i> Chỉnh sửa</a> &nbsp; <a onclick='del(this,$row->id)'><i class='fa fa-trash-o'> </i> Xóa</a></td>";
			    	 	echo "</tr>";
			    	}
		    	}else{
		    		echo "<tr><td colspan='3'>Không tìm thấy nhóm khóa học nào .</td></tr>";
		    	}
		    	?>
		    	</tbody>
		    </table>
		</div>
</div>
<input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training/group" ?>" />
</form>
<script>
	function del(ob,id){
		var result = confirm('Bạn có chắc chắn muốn xóa không');
		if(result){
			var link = $('#link').val();
	        $.post(link+"/remove/"+id, $("form").serialize(),function(resp){
	        	location.reload();
	        });
		}
	}

	function edit(ob,id){
        var text = $(ob).parent('td').parent('tr').find('.title_group').html();
        var getValue = prompt("Điền tên nhóm mới thay cho tên nhóm cũ : ", text);
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/update";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&ID="+id,
                success: function(result){
                    if(result!='False'){
                        $(ob).parent('td').parent('tr').find('.title_group').html(getValue);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function add(ob){
        var getValue = prompt("Điền tên nhóm mới : ");
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/add";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue,
                success: function(result){
                    if(result!='False'){
                        location.reload();
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }
	   
</script>
