<div class="containner">
	<div class="waiting"><h1>Loading...</h1></div>
	<div class="branchtitle">
		<select class="type" onchange="changefillter()">
			<option value='0' <?php echo $type==0 ? "selected='selected'" : '' ; ?>>Nhà cung cấp</option>
			<option value='1' <?php echo $type==1 ? "selected='selected'" : '' ; ?>>Kho nội bộ</option>
		</select>
		<select class="place" onchange="changefillter()">
			<option value='0'>Tất cả địa điểm lấy hàng</option>
			<?php 
			$branch = $this->db->query("select * from ttp_report_branch")->result();
			$arr_branch = array();
			if(count($branch)>0){
				foreach($branch as $row){
					$arr_branch[$row->ID] = $row->Title;
					$selected = $place==$row->ID ? "selected='selected'" : "" ;
					echo "<option value='$row->ID' $selected>$row->Title</option>";
				}
			}

			$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
			$arr_warehouse = array();
			if(count($warehouse)>0){
				foreach($warehouse as $row){
					$arr_warehouse[$row->ID] = $row->MaKho;
				}
			}
			?>
		</select>
	</div>
	<?php 
	if(count($data)>0){
		$temp='';
		foreach($data as $row){
			$next = $row->MaDH!=$temp ? "" : "" ;
			$temp = $row->MaDH;
			$title = $row->BranchID>0 ? $arr_branch[$row->BranchID] : '' ;
			$title = $row->WarehouseID>0 ? $arr_warehouse[$row->WarehouseID] : $title ;
			echo "<div class='list'>";
			echo "<p class='ordertitle'><b>$row->MaDH</b><small>$title</small></p>";
			echo "<p class='productstitle'>$row->Title</p>";
			echo "<p class='productsamount'>x ".number_format($row->Amount,3)." / $row->Donvi <label for='checkbox$row->IDDetails' data-state='0' class='getproducts' onclick='getproducts(this)'><i class='fa fa-check' aria-hidden='true'></i></label>
				<label class='cancel_products' onclick='cancel_picking_products(this,$row->IDDetails)'><i class='fa fa-trash'></i></label>
			</p>";
			echo "<input class='hidden checkboxdetails' type='checkbox' id='checkbox$row->IDDetails' data-details='$row->IDDetails' />";
			echo "</div>";
		}
		echo "<a class='apply' onclick='applydetails(this)'>Xác nhận</a>";
	}else{
		echo '<p class="text-center" style="margin:100px 0px 20px 0px"><i class="fa fa-smile-o" aria-hidden="true" style="font-size:80px;color:#ccc;"></i></p><p class="text-center" style="font-size: 20px;color: #ccc;text-shadow:0px 1px 0px #FFF;">Bạn đã xử lý hết yêu cầu .</p>';
	}
	?>
</div>
<style>
	.waiting{display: none;}
</style>
<script>
	var link = "<?php echo $base_link ?>";

	function changefillter(){
		var type = $(".type").val();
		var place = $(".place").val();
		window.location = link+"?type="+type+"&place="+place;
	}

	function getproducts(ob){
		var datastate = $(ob).attr('data-state');
		if(datastate==0){
			$(ob).addClass("active");
			$(ob).attr('data-state',1);
		}else{
			$(ob).removeClass("active");
			$(ob).attr('data-state',0);
		}
	}

	function applydetails(ob){
		var state = 0;
		var dataid = [];
		$(".checkboxdetails").each(function(){
			if(this.checked==true){
				var ID = $(this).attr('data-details');
				dataid.push(ID);
				state=1;
				$(this).parent('.list').slideUp('fast');
				$(this).parent('.list').remove();
			}
		});
		if(state==1){
			$(".waiting").show();
			$.ajax({
		        type:"POST",
		        cache:false,
		        url:link+"apply_get_products_from_branch",
		        data:"data="+JSON.stringify(dataid),
		        success: function (html) {
		        	$(".waiting").hide();
		        	if(html!='true'){
		        		console.log(html);
		        	}
		        }
		    });
		}
	}

	function cancel_picking_products(ob,ID){
		var r = confirm("Bạn có chắc hủy yêu cầu lấy sản phẩm này !");
		if (r == true) {
			window.location=link+'cancel_picking_products/'+ID;
		}
	}
</script>
