<?php 
class Marketing_campaign extends Admin_Controller { 
 
    public $user;
    public $classname="marketing_campaign";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }
	
    public function index(){
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        ); 
        $view = "admin/marketing_campaign";
        $this->template->add_title('Marketing Comapaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    public function set_day(){
        $active = isset($_POST['active']) ? $_POST['active'] : 0 ;
        if($active==1){
            $this->session->set_userdata("active_vs",1);
        }else{
            $this->session->set_userdata("active_vs",0);
        }
        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;
        if($group1!=''){
            $group1 = explode("-",$group1);
            $startday = isset($group1[0]) ? trim($group1[0]) : '';
            $stopday = isset($group1[1]) ? trim($group1[1]) : '';
            if($startday!='' && $stopday!=''){
                $startday = explode('/',$startday);
                $stopday = explode('/',$stopday);
                if(count($startday)==3 && count($stopday)==3){
                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
                    $timestart = strtotime($startday);
                    $timestop = strtotime($stopday);
                    if($timestart>$timestop){
                        echo "fasle";
                        return;
                    }
                    $this->session->set_userdata("marketing_campaign_startday",$startday);
                    $this->session->set_userdata("marketing_campaign_stopday",$stopday);
                }
            }
        }
        echo "OK";
    }
    
    public function set_fillterby(){
        $fil = $_POST["fillterRef"];
        $this->session->set_userdata("fillter_by",$fil);
        echo "OK";
    }
    
    public function all(){
        $startday = $this->session->userdata("marketing_campaign_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("marketing_campaign_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        
        $fillter_by = $this->session->userdata('fillter_by');
        $fillter_by = $fillter_by == '' ? "" : $fillter_by;

        $where = "WHERE ";
        
        if($fillter_by == 0){
            /*Theo kế hoạch*/
            // $where .= "($startday <= start and end >= $stopday)";
        }
        $keyword = isset($_GET["_search"]) ? $_GET["_search"] : '';
        $keyword_key = "";
        $keyword = explode('|', $keyword);
        $keyword_key = isset($keyword[0]) ? $keyword[0] : '';
        $keyword = isset($keyword[1]) ? $keyword[1] : '' ;
        if($keyword && $this->user->IsAdmin==1){
            if($keyword_key == 1){
                $where .= "market_name LIKE '%$keyword%'";
            }else{
                $userData = $this->db->query("SELECT ID FROM ttp_user WHERE LastName LIKE '%$keyword%'")->row();
                $userID = $userData->ID;
                $where .= "owner LIKE '%\"$userID\"%'";
            }
            
        }else{
            $userID = $this->user->ID;
            $where .= "owner LIKE '%\"$userID\"%'";
        }
        if($this->user->IsAdmin==1){
            $where .= " AND market_status in(1,2,3,4,5,6,7)"; //Chờ duyệt
        }
        $result = $this->db->query("SELECT * FROM ttp_marketing_campaign $where")->result();
        $data = array(
            'data' => $result,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'fillter_by'   => $fillter_by,
            'keyword_key'   => $keyword_key,
            'keyword'   => $keyword,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );
        $view = "admin/marketing_campaign_all";
        $this->template->add_title('All | Promotion Comapaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    public function updateStatus(){
        $notes = isset($_POST["notes"]) ? $_POST["notes"] : '';
        $data = array(
            'market_status' => $_POST["status"],
            'market_notes'  => $notes
        );
        
        /*History*/
        $insertHistory = array();
        $insertHistory["Notes"] = $notes;
        $insertHistory["TranID"] = $_POST["market_id"];
        $insertHistory["Status"] = $_POST["status"];
        $insertHistory["UserID"] = $this->user->ID;
        $insertHistory["Created"] = date('Y-m-d h:i:s',time());
        $this->db->insert('ttp_marketing_campaign_history',$insertHistory);
        
        $this->db->where("market_id",$_POST["market_id"]);
        if($this->db->update('ttp_marketing_campaign',$data)){
            echo "OK";
        }else{
            echo "Error";
        }
    }
    public function marketing_campaign_attach(){
        $market_id = $_POST["market_id"];
        $resultParent = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $market_id")->row();
        
        $data = array(
            'market_status_active' => $resultParent->market_status,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );  
        $view = "admin/marketing_campaign_attach";
        $this->load->view($view,$data);
    }
    public function marketing_campaign_planer()
    {
        $rel = $_POST["rel"];
        $market_id = $_POST["market_id"];
        $parent = $_POST["parent"];
        $id = $_POST["id"];
        
        $resultParent = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $market_id")->row();
        $resultPar = $this->db->query("SELECT money_need,value,symbol,action_title FROM ttp_marketing_campaign_details WHERE market_id='$market_id' and parent=0 and id=$id")->row();
        $result = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE market_id='$market_id' and parent='$parent'")->result();
        $result_symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');

        $data = array(
            'data'  => $result,
            'dataPar'  => $resultPar,
            'market_id'  => $market_id,
            'parent'  => $parent,
            'rel'  => $rel,
            'result_symbol'  => $result_symbol,
            'market_status_active' => $resultParent->market_status,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );  
        $view = "admin/marketing_campaign_planer_add";
        $this->load->view($view,$data);
    }
    
    
    
    public function removePlaner(){
        if(in_array(array(2,5,6,7),$_POST["status"])){
            $id = $_POST["id"];
            $this->db->where('id', $id);
            if($this->db->delete('ttp_marketing_campaign_details')){
                echo "OK";
            }
        }
        
    }
    
    public function loadPlaner(){
        $market_id = $_POST["market_id"];
        $parent = $_POST["parent"];
        $resultParent = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $market_id")->row();
        $result = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE market_id = $market_id and parent = $parent")->result();
        $i = 1;
        foreach($result as $row){
            $delete = "";
            if($this->user->IsAdmin!=1){
                $delete = "onclick='removePlaner($row->id);'";
            }
        echo "<tr>
                <td class='hidden'></td>
                <td class='text-center'>$i</td>
                <td style='padding-left:10px;'>$row->name</td>
                <td class='text-center' style='cursor:pointer' title='Xóa'><i class='fa fa-trash-o' ".$delete."></i></td>
            </tr>";
            $i++;
        }
    }
    
    public function loadTr(){
        $result_symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
        
        $html= "<tr>";
        $html.= "<th class='text-center hidden'></th>";
        $html.= "<th class='text-right col-xs-3'><a class='btn text-danger' onclick='removeTr(this)'><i class='fa fa-minus-circle'></i></a></th>";
        $html.= "<th class='text-center'><input type='text' name='action_title[]' value='' class='form-control' placeholder='Tên mục tiêu'/></th>";
        $html.= "<th class='text-center'>
                <input type='text' id='kpi_focus' onkeyup='keyUp(this);' name='kpi_focus[]' value='' placeholder='0' class='form-control' autocomplete='off'/>
            </th>";
        $html.= "<th class='text-center'>
                <select class='form-control' name='symbol[]'>";
        foreach($result_symbol as $row){
        $html.= "<option value=$row->code>$row->name</option>";
        }
        $html.= "</select>";
        $html.= "</th>";
        $html.= "</tr>";
        echo $html;
    }
    
    public function marketing_campaign_edit_add()
    {
        $market_id = $_POST["market_id"];
        $resultParent = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $market_id")->row();
        $result_symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
        $data = array(
            'data'  => $market_id,
            'edit'  => false,
            'market_id'  => $market_id,
            'result_symbol'  => $result_symbol,
            'market_status_active' => $resultParent->market_status,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );  
        $view = "admin/marketing_campaign_edit_add";
        $this->load->view($view,$data);
    }
    public function addCampaign(){
        $domain = $this->db->query("SELECT * FROM ttp_marketing_domain")->result();
        $owner = $this->db->query("SELECT * FROM ttp_user WHERE RoleID = 3")->result();
        $data = array(
            'action' => 'add',
            'domain' => $domain,
            'owner' => $owner,
        );
        $view = "admin/marketing_campaign_add";
        $this->template->add_title('Add Campaign | Marketing Campaign');
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
    
    public function addCampaignAction(){
        $data = array(
            'market_name' => $_REQUEST["market_name"],
            'market_description' => $_REQUEST["market_desc"],
            'market_type' => isset($_REQUEST["market_type"]) ? json_encode($_REQUEST["market_type"]) : "[]",
            'market_domain' => isset($_REQUEST["market_domain"]) ? json_encode($_REQUEST["market_domain"]) : "[]",
            'owner' => isset($_REQUEST["owner"]) ? json_encode($_REQUEST["owner"]) : "[]",
            'owner_action' => isset($_REQUEST["owner_action"]) ? json_encode($_REQUEST["owner_action"]) : "[]",
        );
        if($this->db->insert('ttp_marketing_campaign',$data)){
            $res = array();
            $res["status"] = "OK";
            $token = md5('EDIT'.$this->db->insert_id());
            $token = substr($token, 0,8);
            $res["lastid"] = $this->db->insert_id();
            $res["token"] = $token;
            echo json_encode($res);
        }
        
    }
    
    public function loadFileAttach(){
        $PlanID = isset($_POST['PlanID']) ? $_POST['PlanID'] : 0 ;
        $dataRow = $this->db->query("SELECT file FROM ttp_marketing_campaign_details WHERE id=$PlanID")->row();
        $dataRowData = ($dataRow) ? json_decode($dataRow->file) : null;
        $html = "";
        if($dataRowData != null){
            foreach($dataRowData as $key=>$row){
                $FileName = explode("/", $row);
                $FileName = $FileName[count($FileName)-1];
                $FileType = explode(".", $FileName);
                $FileName = $FileType[0];
                $FileType = $FileType[1];
                $FileSize = filesize($row);
                $html .= "<tr>
                    <td class='hidden'></td>
                    <td class='text-center'><img src='public/admin/images/icon/_".$FileType.".png' height='16px'/></td>
                    <td style='padding-left:10px;'><a href='$row' target='_blank'>".$FileName."</a></td>
<td class='text-right' style='padding-right:10px;'>".$this->FileSizeConvert($FileSize)."</td>                    
<td class='text-center'><i class='fa fa-trash-o' onClick='delFileAttach($key,$PlanID)' style='cursor:pointer'></i></td>
                </tr>";
            }
        }
        echo $html;
    }
    
    function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
                ),
            );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
    
    public function delFileAttach(){
        $Key = isset($_POST['key']) ? $_POST['key'] : 0 ;
        $PlanID = isset($_POST['PlanID']) ? $_POST['PlanID'] : 0 ;
        $dataRow = $this->db->query("SELECT file FROM ttp_marketing_campaign_details WHERE id=$PlanID")->row()->file;
        $myArray = json_decode($dataRow);
        unlink($myArray[$Key]);
        unset($myArray[$Key]);
        $arr = array();
        foreach($myArray as $value){
            $arr[] = $value;
        }
        
        $arr = json_encode($arr); 
        $this->db->query("update ttp_marketing_campaign_details set file='$arr' where id = $PlanID");
    }
    
    public function uploadFile(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $object = $this->db->query("select file from ttp_marketing_campaign_details where id=$ID")->row();
        if(!$object) exit();
        if(isset($_FILES['file'])){
            if($_FILES['file']['error']==0){
                $data = json_decode($object->file,true);
                $str = $this->upload_attach_file("file","image");
                if($str!='' && !in_array($str,$data)){
                    $data[] = $str;
                }
                $data = json_encode($data);
                $this->db->query("update ttp_marketing_campaign_details set file='$data' where id = $ID");
                echo "OK";
            }else{
                echo "False";
            }
        }else{
            echo "False";
        }
    }
    
    public function edit(){
        $token = isset($_GET["_token"]) ? $_GET["_token"] : null;
        $id = $this->uri->segment(5);
        $mytoken = md5('EDIT'.$id);
        $mytoken = substr($mytoken, 0,8);
        
        if($mytoken != $token){
            redirect(base_url().ADMINPATH.'/report/marketing_campaign/all');
        }else{
            $resultParent = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $id")->row();
            $result = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE market_id = $id and parent=0")->result();
            $domain = $this->db->query("SELECT * FROM ttp_marketing_domain")->result();
            $owner = $this->db->query("SELECT * FROM ttp_user WHERE RoleID = 3")->result();
            $type_active = json_decode($resultParent->market_type);
            $domain_active = ($resultParent->market_domain) ? json_decode($resultParent->market_domain) : array();
            $owner_active = ($resultParent->owner) ? json_decode($resultParent->owner) : array();
            $owner_action_active = ($resultParent->owner_action) ? json_decode($resultParent->owner_action) : array();

            $status = $resultParent->market_status;
            $data = array(
                'action' => 'edit',
                'data' => $result,
                'market_id' => $resultParent->market_id,
                'market_status_active' => $resultParent->market_status,
                'market_priority_active' => $resultParent->market_priority,
                'market_success_active' => $resultParent->market_success,
                'domain' => $domain,
                'owner' => $owner,
                'type_active' => $type_active,
                'domain_active' => $domain_active,
                'owner_active' => $owner_active,
                'owner_action_active' => $owner_action_active,
                'dataParent' => $resultParent,
                'market_notes' => $resultParent->market_notes,
                'status' => $status,
                'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
            );
            $view = "admin/marketing_campaign_edit";
            $this->template->add_title('Edit | Marketing Campaign');
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }
    }
    public function editMarket(){
        $id = $_POST["id"];
        $result_symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
        $result = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE id = $id")->row();
        $resultParent = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $result->market_id")->row();
        $data = array(
            'edit' => true,
            'data' => $result,
            'result_symbol' => $result_symbol,
            'market_status_active' => $resultParent->market_status,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );
        $view = "admin/marketing_campaign_edit_add";
        $this->load->view($view,$data);
    }
    
    public function updateKPI(){
        $focus = str_replace((","), "", $_REQUEST["kpi_focus"]);
        foreach($focus as $key=>$row){
            if($row == ""){
                unset($focus[$key]);
            }
        }
        $symbol = $_REQUEST["symbol"];
        $action_title = $_REQUEST["action_title"];
//        var_dump($_REQUEST); exit();
        $data = array(
            'name' => $_REQUEST["kpi_name"],
            'description' => $_REQUEST["kpi_desc"],
            'start' => ($_REQUEST["kpi_start"]) ? $_REQUEST["kpi_start"] : date('Y-m-d',time()),
            'end' => ($_REQUEST["kpi_end"]) ? $_REQUEST["kpi_end"] : date('Y-m-d',time()),
            'value' => json_encode($focus),
            'symbol' => json_encode($symbol),
            'action_title' => json_encode($action_title),
            'money_need' => $_REQUEST["money_need"],
        );
        $this->db->where("id",$_REQUEST["id"]);
        if($this->db->update('ttp_marketing_campaign_details',$data)){
            echo "OK";
        }
    }
    public function note(){
        $json = '["34234","1500000"]';
        $cntjson = count(json_decode($json));
        $array = array(
            'zero' => '0'
        );
        $cntarr = count($array);
        $hieu = $cntjson - $cntarr;
        if ($hieu > 0) {
            for ($i = 0; $i < $hieu; $i++) {
                $stack[] = "";
            }
            $res = array_slice($array, 0, count($array), true) + $stack;
        }
        echo "<pre>";
        print_r($res);
    }
    public function createKPI(){
        $focus = str_replace((","), "", $_REQUEST["kpi_focus"]);
        foreach($focus as $key=>$row){
            if($row == ""){
                unset($focus[$key]);
            }
        }
        $symbol = $_REQUEST["symbol"];
        $action_title = $_REQUEST["action_title"];
//        var_dump($_REQUEST); exit();
        $data = array(
            'name' => $_REQUEST["kpi_name"],
            'description' => $_REQUEST["kpi_desc"],
            'market_id' => $_REQUEST["market_id"],
            'start' => ($_REQUEST["kpi_start"]) ? $_REQUEST["kpi_start"] : date('Y-m-d',time()),
            'end' => ($_REQUEST["kpi_end"]) ? $_REQUEST["kpi_end"] : date('Y-m-d',time()),
            'value' => json_encode($focus),
            'symbol' => json_encode($symbol),
            'action_title' => json_encode($action_title),
            'money_need' => $_REQUEST["money_need"],
            'parent' => 0,
        );
        if($this->db->insert('ttp_marketing_campaign_details',$data)){
            echo "OK";
        }
    }
    
    
    
    public function resetKPI(){
        /*History*/
        $insertHistory = array();
        $insertHistory["Notes"] = "Reset khi không được duyệt";
        $insertHistory["TranID"] = $_POST["market_id"];
        $insertHistory["Status"] = 0;
        $insertHistory["UserID"] = $this->user->ID;
        $insertHistory["Created"] = date('Y-m-d h:i:s',time());
        $this->db->insert('ttp_marketing_campaign_history',$insertHistory);
        
        $data = array(
            'market_status' => 0,
        );
        $this->db->where('market_id',$_POST["market_id"]);
        if($this->db->update('ttp_marketing_campaign',$data)){
            echo "OK";
        }
    }
    
    public function updateCompaign(){
        /*History*/
        $note = "--";
        if(isset($_POST["market_status"]) != 0){
            if($_POST["market_status"] == 2){
                $note = "Hoàn tất";
            }
            $insertHistory = array();
            $insertHistory["Notes"] = $note;
            $insertHistory["TranID"] = $_POST["market_id"];
            $insertHistory["Status"] = $_POST["market_status"];
            $insertHistory["UserID"] = $this->user->ID;
            $insertHistory["Created"] = date('Y-m-d h:i:s',time());
            
            $TranID = $_POST["market_id"];
            $Status = $_POST["market_status"];
            
            $isExist = $this->db->query("select Status from ttp_marketing_campaign_history Where id = (Select max(id) from ttp_marketing_campaign_history)")->row()->Status;
            if($isExist != $Status){
                $this->db->insert('ttp_marketing_campaign_history',$insertHistory);
            }
        }
//        var_dump($_REQUEST); exit();
        switch (isset($_REQUEST["market_status"])){
            case 0:
                $data = array(
                    'market_name' => $_REQUEST["market_name"],
                    'market_description' => $_REQUEST["market_desc"],
                    'market_type' => json_encode($_REQUEST["market_type"]),
                    'market_domain' => json_encode($_REQUEST["market_domain"]),
                    'owner' => isset($_REQUEST["owner"]) ? json_encode($_REQUEST["owner"]) : "[]",
                    'owner_action' => isset($_REQUEST["owner_action"]) ? json_encode($_REQUEST["owner_action"]) : "[]",
                    'market_status' => isset($_REQUEST["market_status"]) ? $_REQUEST["market_status"] : 0,
                    'market_priority' => $_REQUEST["market_priority"],
                    'market_success' => isset($_REQUEST["market_success"]) ? $_REQUEST["market_success"] : 0
                );
            break;
            default:
                $data = array(
                    'market_status' => $_REQUEST["market_status"],
                    'market_priority' => $_REQUEST["market_priority"],
                    'market_success' => isset($_REQUEST["market_success"]) ? $_REQUEST["market_success"] : 0
                );
            break;
        }

        $this->db->where('market_id',$_REQUEST["market_id"]);
        if($this->db->update('ttp_marketing_campaign',$data)){
            echo "OK";
        }
    }
    public function addPlaner(){
        $focus = str_replace((","), "", $_REQUEST["planer_focus"]);
        $symbol = $_REQUEST["symbol"];
//        $action_title = $_REQUEST["action_title"];
//        var_dump($_REQUEST); exit();
        $planer_name = $_REQUEST["planer_name"];
        $market_id = $_REQUEST["market_id"];
        $parent = $_REQUEST["parent"];
        $data = array(
            'name'  => $planer_name,
            'market_id'  => $market_id,
            'parent'  => $parent,
            'start' => ($_REQUEST["planer_start"]) ? $_REQUEST["planer_start"] : date('Y-m-d',time()),
            'end' => ($_REQUEST["planer_end"]) ? $_REQUEST["planer_end"] : date('Y-m-d',time()),
            'value' => json_encode($focus),
            'symbol' => json_encode($symbol),
//            'action_title' => json_encode($action_title),
            'action'  => '[]',
            'money' => $_REQUEST["money"],
            'file'  => '[]',
        );
        if($this->db->insert('ttp_marketing_campaign_details',$data)){
            echo "OK";
        }
        
    }
    
    public function editPlaner(){
        $id = $_POST["id"]; //id detail
        $result_symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
        $result = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE id = $id")->row();
        $market_id = $result->market_id;
        $getStatus = $this->db->query("SELECT * FROM ttp_marketing_campaign WHERE market_id = $market_id")->row();
        $resultPar = $this->db->query("SELECT money_need,value,symbol,action_title FROM ttp_marketing_campaign_details WHERE id=$result->parent")->row();
        $data = array(
            'data' => $result,
            'dataPar' => $resultPar,
            'result_symbol' => $result_symbol,
            'market_status_active' => $getStatus->market_status,
            'money' => $result->money,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );
        
        $view = "admin/marketing_campaign_planer_edit";
        $this->load->view($view,$data);
    }
    
    public function updatePlaner(){
//        var_dump($_REQUEST); exit();
        $focus = str_replace((","), "", $_REQUEST["planer_focus"]);
        $action = str_replace((","), "", $_REQUEST["planer_action"]);
        $money_action = str_replace((","), "", $_REQUEST["money_action"]);
        
        $data = array(
            'name' => $_REQUEST["planer_name"],
            'start' => $_REQUEST["planer_start"],
            'end' => $_REQUEST["planer_end"],
            'updated' => date('Y-m-d h:i:s',time()),
            'value' => json_encode($focus),
            'action' => json_encode($action),
            'action_start' => $_REQUEST["action_start"],
            'action_end' => $_REQUEST["action_end"],
            'money' => $_REQUEST["money"],
            'money_action' => $money_action,
        );
        $res = array();
        $this->db->where('id',$_REQUEST["planer_id"]);
        if($this->db->update('ttp_marketing_campaign_details',$data)){
            $res["status"] = "OK";
            $res["now"] = date('Y-m-d h:i:s',time());
            echo json_encode($res);
        }
    }
    public function deleteMarket(){
        $id = $_POST["id"];
        $token = $_POST["token"];
        $mytoken = md5('REMOVE-MARKET'.$id);
        $mytoken = substr($mytoken, 0,8);
        if($mytoken == $token){
            $this->db->where('market_id', $id);
            if($this->db->delete('ttp_marketing_campaign')){
                /*Remove Details*/
                $this->db->where('market_id', $id);
                $this->db->delete('ttp_marketing_campaign_details');
                /*Remove History*/
                $this->db->where('TranID', $id);
                $this->db->delete('ttp_marketing_campaign_history');
            }
            echo "OK";
        }
    }
    public function deleteKPI(){
        $id = $_POST["id"];
        $token = $_POST["token"];
        $mytoken = md5('REMOVE'.$id);
        $mytoken = substr($mytoken, 0,8);
        if($mytoken == $token){
            $this->db->where('id', $id);
            if($this->db->delete('ttp_marketing_campaign_details')){
                $this->db->where('parent', $id);
                if($this->db->delete('ttp_marketing_campaign_details')){
                    echo "OK";
                }
            }
        }else{
            echo "Error".$id.$token;
        }
    }
    
    public function listDVT(){
        $symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
        $data = array(
            'data' => $symbol,
            'base_link' =>  base_url().ADMINPATH.'/report/marketing_campaign/'
        );
        $view = "admin/marketing_campaign_list_dvt";
        $this->load->view($view,$data);
    }
    public function loadDVT(){
        $symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
        $res = "";
        foreach($symbol as $sym){
            $res .= "<tr><td class='hidden'></td><th class='th-$sym->id'>".$sym->name."</th><td class='text-center'>";
                        if($sym->del_flg == 1){
                    $res .= "<i class='fa fa-edit' onclick='editDVT($sym->id)' style='cursor: pointer;'></i>";
                        } else {
                    $res .= "<i class='fa fa-trash-o' onclick='deleteDVT($sym->id)' style='cursor: pointer;'></i>";
                    }
                $res .= "</td></tr>";
        }
        echo $res;
    }
    
    public function addDVT(){
        /*getMax code*/
        $getMaxCode = $this->db->query("SELECT code,position FROM ttp_define WHERE id = (SELECT MAX(id) FROM ttp_define WHERE `group`='symbol' and `type` = 'campaign')")->row();
        
        $insertData = array();
        $insertData["name"] = $_POST["dvt"];
        $insertData["code"] = $getMaxCode->code+1;
        $insertData["group"] = "symbol";
        $insertData["type"] = "campaign";
        $insertData["position"] = $getMaxCode->position+1;
        if($this->db->insert('ttp_define',$insertData)){
            echo "OK";
        }
    }
    
    public function updateDVT(){
        $data = array(
            'name' => $_POST["name"]
        );
        $this->db->where('id', $_POST["id"]);
        if($this->db->update('ttp_define',$data)){
            echo "OK";
        }
    }
    
    public function deleteDVT(){
        $this->db->where('id', $_POST["id"]);
        if($this->db->delete('ttp_define')){
            echo "OK";
        }
    }
}
?>