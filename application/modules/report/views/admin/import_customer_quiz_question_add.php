<div class="alert alert-sm hidden" id="alert-message">Lưu thông tin thành công!</div>
<input type="hidden" id="ParentID" value="<?php echo $parent ?>" />
<div class="row">
	<div class="col-xs-12">
		<p><b>Nội dung <?php echo $parent==0 ? "câu hỏi" : "đáp án" ; ?></b></p>
		<div class="form-group">
			<input type="text" id="Title" class="form-control" placeholder="Nhập nội dung <?php echo $parent==0 ? "câu hỏi" : "đáp án" ; ?>" />
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-3">
		<p><b>Cấp độ <?php echo $parent==0 ? "câu hỏi" : "đáp án" ; ?></b></p>
		<div class="form-group">
			<select class="form-control" id="Level">
				<option value="1">Cấp độ 1</option>
				<option value="2">Cấp độ 2</option>
				<option value="3">Cấp độ 3</option>
			</select>
		</div>
	</div>
	<div class="col-xs-3">
		<p><b>Điểm số cho <?php echo $parent==0 ? "câu hỏi" : "đáp án" ; ?></b></p>
		<div class="form-group">
			<input type="number" class="form-control" placeholder="Nhập số điểm cho <?php echo $parent==0 ? "câu hỏi" : "đáp án" ; ?>" id="Point" />
		</div>
	</div>
	<?php 
	if($parent>0){
	?>
	<div class="col-xs-3">
		<p><b>Là đáp án đúng</b></p>
		<div class="form-group">
			<select class="form-control" id="IsTrue">
				<option value="0">False</option>
				<option value="1">True</option>
			</select>
		</div>
	</div>
	<?php
	}
	?>
</div>
<hr>
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-danger" onclick="add(this)"><i class="fa fa-pencil"></i> Lưu thông tin</a> 
		<?php 
		if($parent!=0){
			echo '<a class="btn btn-default" onclick="loadeditform('.$parent.')"><i class="fa fa-long-arrow-left"></i> Quay lại câu hỏi</a>';
		}
		?>
	</div>
</div>
<script>
	function add(ob){
		var data = {
			Title: $("#Title").val(),
			Point: $("#Point").val(),
			Level: $("#Level").val(),
			ParentID: $("#ParentID").val(),
			IsTrue: $("#IsTrue").val()
		};
		if($("#Title").val()==''){
			$("#Title").focus();
			return;
		}
		$(ob).addClass("saving");
		$.ajax({
			url: "<?php echo base_url().ADMINPATH.'/report/import_customer/' ?>add_question",
            type: "POST",
            data: "Data="+JSON.stringify(data),
            dataType: "json",
            success: function(result){
            	$(ob).removeClass("saving");
                if(result.error==0){
                	loadeditform(result.id);
                }else{
                	$("#alert-message").html("Lưu không thành công!").removeClass('hidden').removeClass('alert-success').addClass('alert-danger');
                }
            },error: function(result){
            	console.log(result.responseText);
            }
		});
	}
</script>