<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$total_tienhang = 0;
$total_chiphi   = 0;
$total_giatri   = 0;
$total_huy      = 0;
$month          = array();
$dayofmonth     = array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";
$channel = $this->user->UserType==1 ? "?order_type=".$this->user->Channel : "?order_type=1";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1><a href='<?php echo base_url().ADMINPATH.'/report/import_order' ?>'>DANH SÁCH ĐƠN HÀNG</a><?php echo " | <a href='".base_url().ADMINPATH."/report/import_order/quick_view".$channel."'>THỐNG KÊ NHANH</a>"; ?></h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="import_orderlist">
        <div class="block2">
            <div class="block_2_1 pull-left" style="margin-right:10px;">
                <div class="dropdown">
                  <button class="btn btn-danger dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    Tạo đơn hàng
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="<?php echo $base_link.'add' ?>"><i style="margin-right:5px;" class="fa fa-random" aria-hidden="true"></i> Theo quy trình</a></li>
                    <li><a href="<?php echo base_url().ADMINPATH.'/pos/pos_sales' ?>"><i style="margin-right:5px;" class="fa fa-shopping-cart" aria-hidden="true"></i> Bán lẻ tại shop</a></li>
                  </ul>
                </div>
            </div>
            <div class="block_2_2">
                <button class="btn btn-default" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
                <?php if($this->user->UserType==5){ ?>
                <a class="btn btn-primary" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export</a>
                <?php } ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="export_tools_kt">
            <form action="<?php echo base_url().ADMINPATH."/report/import/export_ketoan" ?>" method="post" id="falseclass">
                <span>Chọn ngày xuất: </span>
                <input type='text' name="Export_date" class="form-control date-picker" id="Export_date" value="<?php echo date('Y-m-d',time()); ?>" required />
                <span>Chọn loại báo cáo: </span>
                <select name="TypeExport">
                    <option value="4">BC Telesales</option>
                    <option value="8">Dữ liệu khách hàng/tháng</option>
                    <option value="12">Báo cáo doanh số</option>
                    <option value="6">BC PXK đã xuất</option>
                </select>
                <button type="submit" class="btn btn-default">Xuất báo cáo</button>
            </form>
        </div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"c.Name",1=>"a.CustomerType",2=>"e.ID",3=>"d.ID",4=>"b.ID",5=>"a.SoluongSP",6=>"a.Total",7=>"a.Chiphi",8=>"a.Status",9=>"a.UserID",10=>"a.TransportID",11=>"c.Phone1");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Tên khách hàng",1=>"Loại khách hàng",2=>"Khu vực",3=>"Tỉnh thành",4=>"Quận huyện",5=>"Số lượng sản phẩm",6=>"Tổng tiền hàng",7=>"Chi phí vận chuyển",8=>"Trạng thái đơn hàng",9=>"Nhân viên khởi tạo",10=>"Đối tác vận chuyển",11=>"Số điện thoại");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                    <div class="list_toolls col-xs-1 hidden-xs hidden-sm"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                    <div class="list_toolls col-xs-12 col-sm-3 col-md-2">
                        <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                        <ul class="dropdownbox">
                            <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                    <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                    <li><a onclick="setfield(this,11,'sodienthoai')">Số điện thoại</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                <ul>
                                    <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                    <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                    <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,9,'user')">Nhân viên khởi tạo</a></li>
                                    <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                    <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                    <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                    <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                    <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-4 col-sm-3 col-md-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-6 col-sm-3 col-md-3">
                        <?php 
                        if($value_field==2 || $value_field==3 || $value_field==4){
                            if($value_field==2){
                                $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();    
                            }
                            if($value_field==3){
                                $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();    
                            }
                            if($value_field==4){
                                $result = $this->db->query("select * from ttp_report_district order by Title ASC")->result();    
                            }
                            if(count($result)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($result as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ; 
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }elseif ($value_field==8) {
                            echo "<select name='FieldText[]' class='form-control'>";
                            echo $param_value==9 ? "<option value='9' selected='selected'>Đơn hàng chuyển sang nv điều phối</option>" : "<option value='9'>Đơn hàng chuyển sang nv điều phối</option>" ;
                            echo $param_value==8 ? "<option value='8' selected='selected'>Đơn hàng bị trả về</option>" : "<option value='8'>Đơn hàng bị trả về</option>" ;
                            echo $param_value==7 ? "<option value='7' selected='selected'>Chuyển sang bộ phận giao hàng</option>" : "<option value='7'>Chuyển sang bộ phận giao hàng</option>" ;
                            echo $param_value==6 ? "<option value='6' selected='selected'>Đơn hàng bị trả về từ kế toán</option>" : "<option value='6'>Đơn hàng bị trả về từ kế toán</option>" ;
                            echo $param_value==5 ? "<option value='5' selected='selected'>Đơn hàng chờ kế toán duyệt</option>" : "<option value='5'>Đơn hàng chờ kế toán duyệt</option>" ;
                            echo $param_value==4 ? "<option value='4' selected='selected'>Đơn hàng bị trả về từ kho</option>" : "<option value='4'>Đơn hàng bị trả về từ kho</option>" ;
                            echo $param_value==3 ? "<option value='3' selected='selected'>Đơn hàng mới chờ kho duyệt</option>" : "<option value='3'>Đơn hàng mới chờ kho duyệt</option>" ;
                            echo $param_value==2 ? "<option value='2' selected='selected'>Đơn hàng nháp</option>" : "<option value='2'>Đơn hàng nháp</option>" ;
                            echo $param_value==0 ? "<option value='0' selected='selected'>Đơn hàng thành công</option>" : "<option value='0'>Đơn hàng thành công</option>" ;
                            echo $param_value==1 ? "<option value='1' selected='selected'>Đơn hàng hủy</option>" : "<option value='1'>Đơn hàng hủy</option>" ;
                            $fill_temp_value = $param_value==9 ? "Đơn hàng chuyển sang nv điều phối" : $fill_temp_value ;
                            $fill_temp_value = $param_value==8 ? "Đơn hàng bị trả về" : $fill_temp_value ;
                            $fill_temp_value = $param_value==7 ? "Chuyển sang bộ phận giao hàng" : $fill_temp_value ;
                            $fill_temp_value = $param_value==6 ? "Đơn hàng bị trả về từ kế toán" : $fill_temp_value ;
                            $fill_temp_value = $param_value==5 ? "Đơn hàng chờ kế toán duyệt" : $fill_temp_value ;
                            $fill_temp_value = $param_value==4 ? "Đơn hàng bị trả về từ kho" : $fill_temp_value ;
                            $fill_temp_value = $param_value==3 ? "Đơn hàng mới chờ kho duyệt" : $fill_temp_value ;
                            $fill_temp_value = $param_value==2 ? "Đơn hàng nháp" : $fill_temp_value ;
                            $fill_temp_value = $param_value==1 ? "Đơn hàng hủy" : $fill_temp_value ;
                            $fill_temp_value = $param_value==0 ? "Đơn hàng thành công" : $fill_temp_value ;
                            echo "</select>";
                        }elseif ($value_field==9) {
                            $userlist = $this->db->query("select ID,UserName from ttp_user where UserType=1")->result();
                            if(count($userlist)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($userlist as $row){
                                    $fill_temp_value = $param_value==$row->ID ? $row->UserName : $fill_temp_value ;
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ; 
                                    echo "<option value='$row->ID' $selected>$row->UserName</option>";
                                }
                                echo "</select>";
                            }
                        }elseif ($value_field==1) {
                            echo "<select name='FieldText[]' class='form-control'>
                                    <option value='0'>Khách hàng mới</option>
                                    <option value='1'>Khách hàng cũ</option>
                                </select>";
                            $fill_temp_value = $param_value==1 ? "Khách hàng cũ" : $fill_temp_value ;
                            $fill_temp_value = $param_value==0 ? "Khách hàng mới" : $fill_temp_value ;
                        }elseif ($value_field==10) {
                            echo "<select name='FieldText[]' class='form-control'>";
                            $transport = $this->db->query("select * from ttp_report_transport")->result();
                            foreach($transport as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ; 
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <div class="list_toolls col-xs-1 hidden-xs hidden-sm"><label class="title first-title">Chọn</label></div>
                                <div class="list_toolls col-xs-12 col-sm-3 col-md-2">
                                    <input type="hidden" class="FieldName" name="FieldName[]" />
                                    <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                                    <ul class="dropdownbox">
                                        <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                            <ul>
                                                <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                                <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                                <li><a onclick="setfield(this,11,'sodienthoai')">Số điện thoại</a></li>
                                            </ul>
                                        </li>
                                        <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                            <ul>
                                                <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                                <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                                <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                            </ul>
                                        </li>
                                        <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                            <ul>
                                                <li><a onclick="setfield(this,9,'user')">Nhân viên khởi tạo</a></li>
                                                <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                                <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                                <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                                <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                                <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="list_toolls reciveroparation col-xs-4 col-sm-3 col-md-2">
                                    <select class="oparation form-control" name="FieldOparation[]">
                                        <option value="1">Bằng</option>
                                        <option value="0">Có chứa</option>
                                        <option value="2">Khác</option>
                                        <option value="3">Lớn hơn</option>
                                        <option value="4">Nhỏ hơn</option>
                                        <option value="5">Lớn hơn hoặc bằng</option>
                                        <option value="6">Nhỏ hơn hoặc bằng</option>
                                    </select>
                                </div>
                                <div class="list_toolls reciverfillter col-xs-6 col-sm-3 col-md-3">
                                    <input type="text" name="FieldText[]" id="textsearch" class="form-control" />
                                </div>
                                <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1 hidden-xs hidden-sm"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
            <table id="table_data">
                <tr>
                    <th style='width: 90px;'>Ngày tạo</th>
                    <th>Mã ĐH</th>
                    <th style='min-width: 110px;'>Tên khách hàng</th>
                    <?php 
                    echo $this->user->UserType!=10 ? "" : "<th>Công ty</th>";
                    echo $this->user->UserType!=10 ? "<th>Số ĐT</th>" : "" ;
                    ?>
                    <?php 
                    echo $this->user->UserType!=10 ? "<th>Thành phố</th>" : "";
                    echo $this->user->UserType!=10 ? "<th>Quận huyện</th>" : "";
                    ?>
                    <th>Giá trị</th>
                    <th>Tình trạng ĐH</th>
                    <?php 
                    echo $status_leader==1 || $this->user->UserType==5 || $this->user->UserType==13 ? "<th>User</th>" : "" ;
                    ?>
                </tr>
                <?php 
                $arr_user_list = array();
                $userlist = $this->db->query("select UserName,ID from ttp_user")->result();
                if(count($userlist)>0){
                    foreach($userlist as $row){
                        $arr_user_list[$row->ID] = $row->UserName;
                    }
                }
                $this->load->model('define_model');
                $arr_status = $this->define_model->get_order_status('status','order');
                $array_status = array();
                foreach($arr_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $array_status[$code] = $ite->name;
                }
                
                $type_status = $this->define_model->get_order_status('type','order');
                $arr_type = array();
                foreach($type_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $arr_type[$code] = $ite->name;
                }
                
                if(count($data)>0){
                    foreach($data as $row){
                        $tienhang = $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                        $status = isset($array_status[$row->Status]) ? $array_status[$row->Status] : "--" ;
                        /*if($row->Status==2){
                            $request = $this->db->query("select * from ttp_report_order_request_available where OrderID=$row->ID and UserAccept!=0")->row();
                            if($request){
                                $status = $status."<br>[Đã xác nhận]";
                            }else{
                                $status = $status."<br>[Chưa xác nhận]";
                            }
                        }*/
                        $status = $row->TransportStatus!='' && $row->Status==7 ? $status."<br><label class='label label-primary'>$row->TransportStatus</label><br><br>$row->TransportRef" : $status ;
                        $total_huy += $row->Status==1 ? $row->Total : 0 ;
                        $Thanhpho = $this->user->UserType!=10 ? "<td style='width: 100px;'><a href='{$base_link}edit/$row->ID'>$row->Thanhpho</a></td>" : '' ;
                        $Quanhuyen = $this->user->UserType!=10 ? "<td style='width: 100px;'><a href='{$base_link}edit/$row->ID'>$row->Quanhuyen</a></td>" : '' ;
                        $Congty = $this->user->UserType!=10 ? "" : "<td style='width: 150px;'><a href='{$base_link}edit/$row->ID'>$row->Company</a></td>" ;
                        $Phone  = $this->user->UserType!=10 ? "<td style='width:100px'><a href='{$base_link}edit/$row->ID'>$row->Phone1</a></td>" : "" ;
                        echo "<tr>
                                <td style='width:120px;background:none'><a href='{$base_link}edit/$row->ID'>".date('d/m/Y',strtotime($row->Ngaydathang))."</a></td>
                                <td style='width:80px'><a href='{$base_link}edit/$row->ID'>$row->MaDH</a></td>
                                <td><a href='{$base_link}edit/$row->ID'>$row->Name</a></td>
                                $Congty
                                $Phone
                                $Thanhpho
                                $Quanhuyen
                                <td style='text-align:right;width:100px'><a href='{$base_link}edit/$row->ID'>".number_format($tienhang)."</a></td>
                                <td style='text-align:center;width:120px'><a href='{$base_link}edit/$row->ID'>$status</a></td>";
                        if($status_leader==1 || $this->user->UserType==5 || $this->user->UserType==13){
                            echo isset($arr_user_list[$row->UserID]) ? "<td><a href='{$base_link}edit/$row->ID'>".$arr_user_list[$row->UserID]."</a></td>" : "" ;
                        }
                        echo "</tr>";
                        $temp1 = date('mY',strtotime($row->Ngaydathang));
                        if(!isset($dayofmonth_real[$temp1])){
                            $dayofmonth_real[$temp1] = 1;
                        }else{
                            $dayofmonth_real[$temp1] = $dayofmonth_real[$temp1] +1;
                        }
                    }
                    $total_tienhang = $total-$chietkhau;
                    $total_giatri = $total + $chiphi-$chietkhau;
                    $colspan = $status_leader==1 || $this->user->UserType==5 ? 3 : 2 ;
                    echo "<tr>
                            <td colspan='6' style='text-align:center;'><p><span style='font-size:17px;font-weight:bold;'>Tổng cộng</span> <br>Tìm thấy <b>".number_format($find)."</b> đơn hàng theo yêu cầu .</p></td>
                            <td style='text-align:right'>".number_format($total_tienhang)."</td>
                            <td colspan='$colspan'></td>
                        </tr>";
                }else{
                    $colspan = $status_leader==1 || $this->user->UserType==5 || $this->user->UserType==13 ? 10 : 9 ;
                    echo "<tr><td colspan='$colspan'>Không tìm thấy đơn hàng.</td></tr>";
                }
                ?>
            </table>
            <?php 
                echo $nav;
            ?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<div class="notify_current" style="display:none">
    <a class="numbernoti">1</a>
    <ul></ul>
</div>

<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });

    var baselink = $("#baselink_report").val();
    
    var getcurrentorder = function(){
        $.ajax({
            url: baselink+"import/get_notify_from_supplier_available",
            dataType: "json",
            type: "POST",
            data: "",
            success: function(result){
                if(result.status!=false){
                    notifyNewOrder(result.title,result.image,result.message);
                }
            }
        });
    }
    setInterval(getcurrentorder,25000);

    function notifyNewOrder(title,image,message){
        notify(title, {
            body: message,
            icon: image,
            onclick: function(e) {},
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }
</script>  

<script type="text/javascript">
    $(".numbernoti").click(function(){
        $(this).parent().find('ul').toggle();
    });

    $("#show_thaotac").click(function(event){
        event.stopPropagation();
        $(this).parent('li').find('ul').toggle();
    });

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });

    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });
    
    function checkfull(ob){
        if(ob.checked===true){
            $("#table_data .selected_order").prop('checked', true);
        }else{
            $("#table_data .selected_order").prop('checked', false);
        }
    }
    
    function edit_row(ob){
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                window.location="<?php echo $base_link.'edit/' ?>"+data_id;
                return false;
            }
        });
        $(this).parent('li').parent('ul').toggle();
    }

    function success_row(ob){
        $(ob).parent('li').parent('ul').toggle();
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            senddata("set_success",datastr);
        }
    }

    function cancel_row(ob){
        $(ob).parent('li').parent('ul').toggle();
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            senddata("set_cancel",datastr);
        }
    }

    function waiting_row(ob){
        $(ob).parent('li').parent('ul').toggle();
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            senddata("set_waiting",datastr);
        }
    }

    function delete_row(ob){
        $(ob).parent('li').parent('ul').toggle();
        if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
            return false;
        }
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            senddata("droporder",datastr);
        }
    }

    function senddata(url,data){
        if(url!='' && data!=''){
            $(".over_lay").fadeOut();
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import_order/"+url,
                dataType: "html",
                type: "POST",
                data: "data="+data,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown();
                    }
                }
            }); 
        }else{
            $(".warning_message").slideDown();
        }
    }

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"import_order/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="vung" || fieldname=="tinhthanh" || fieldname=="quanhuyen" || fieldname=='loaikhachhang' || fieldname=='trangthaidonhang' || fieldname=='doitacvanchuyen' || fieldname=='user'){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="tenkhachhang"){
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });

</script>
<style>
    .body_content .containner .import_orderlist .export_tools_kt:before{right:177px;}
</style>
