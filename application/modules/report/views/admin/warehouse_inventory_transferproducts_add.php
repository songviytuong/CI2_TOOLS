<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>PHIẾU YÊU CẦU XUẤT KHO LƯU CHUYỂN NỘI BỘ</h1>
				</div>
				<div class="block2">
					<button class='btn btn-primary btn_default' type="submit"><i class="fa fa-paper-plane"></i> Yêu cầu chuyển</button>
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="block1">
					<div class="row">
		    			<div class="col-xs-4">
		    				<div class="form-group">
	    						<label class="col-xs-5 control-label">Ngày chứng từ: </label>
	    						<div class="col-xs-7">
			    					<input type='text' name='NgayXK' class='form-control' id='NgayNK' value='<?php echo date('Y-m-d') ?>' readonly="true" />
			    				</div>
		    				</div>
		    			</div>
						<div class="col-xs-4">
		    				<div class="form-group">
	    						<label class="col-xs-5 control-label">Kho xuất hàng: </label>
	    						<div class="col-xs-7">
			    					<select name='KhoSender' class="form-control" id="KhoSender" onchange="set_empty_table()">
										<?php 
										if($this->user->UserType==2 || $this->user->UserType==8){
											$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%' order by MaKho ASC")->result();
										}else{
											$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
										}
										if(count($warehouse)>0){
											foreach($warehouse as $row){
												echo "<option value='$row->ID'>$row->MaKho</option>";
											}
										}
										?>
									</select>
			    				</div>
		    				</div>
		    			</div>
		    			<div class="col-xs-4">
		    				<div class="form-group">
		    					<label class="col-xs-5 control-label">Kho nhận hàng: </label>
		    					<div class="col-xs-7">
			    					<select name='KhoReciver' id="KhoReciver" class="form-control">
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											echo "<option value='$row->ID'>$row->MaKho</option>";
										}
									}
									?>
								</select>
			    				</div>
		    				</div>
		    			</div>
		    		</div>
		    		<div class="row" style="margin:5px 0px">
		    			<div class="form-group">
							<div class="col-xs-12">
								<textarea class="required form-control" placeholder="Nhập lý do xuất chuyển kho ..." name='Note' required ></textarea>
			    			</div>
		    			</div>
		    		</div>
				</div>
				<!-- end block1 -->
		    	<div class="block2 row">
		    		<div class="col-xs-4">
		    			<div class="form-group">
			    			<div class="col-xs-3">
			    				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
			    			</div>
			    			<div class="col-xs-2">
			    			<ul>
								<li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
				    				<ul>
					    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
					    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
				    				</ul>
								</li>
							</ul>
							</div>
			    		</div>
			    	</div>
		    	</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th style='width:155px'>Lô xuất hàng</th>
		    				<th style='width:128px'>Số lượng còn</th>
		    				<th style="width: 130px;">SL yêu cầu xuất</th>
		    				<th>Ghi chú</th>
		    			</tr>
		    			<tr>
		    				<td colspan="6">TỔNG CỘNG</td>
		    				<td><span class='tongcong'>0</span></td>
		    				<td></td>
		    			</tr>
		    		</table>
		    		<input type='hidden' name='Status' value='0' />
		    		<input type='hidden' name='Ghichu' value='' />
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text" || node.type=="number") && node.id!="input_search_products")  {return false;} 
	} 

	document.onkeypress = stopRKey; 

	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	$("#add_products_to_order").click(function(){
		var KhoSender = $("#KhoSender").val();
		if(KhoSender>0){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"850px"});
			$(".over_lay .box_inner .block2_inner").html("");
			$.ajax({
	        	url: link+"get_products",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "KhoID="+KhoSender+"&getall=1",
	            success: function(result){
	                if(result!='false'){
	        			$(".over_lay .box_inner").css({'margin-top':'50px'});
				    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
				    	$(".over_lay .box_inner .block2_inner").html(result);
				    	$(".over_lay").removeClass('in');
				    	$(".over_lay").fadeIn('fast');
				    	$(".over_lay").addClass('in');
	                }else{
	                	alert("Kho hiện tại không có hàng để xuất .");
	                }
	                $(this).removeClass('saving');
	            }
	        });
        }else{
        	alert("Vui lòng chọn kho xuất hàng");
        }
	});

	function input_search_products(ob){
		var KhoSender = $("#KhoSender").val();
		var data = $(ob).val();
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data+"&KhoID="+KhoSender+"&getall=1",
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Kho hiện tại không có hàng để xuất .");
                }
            }
        });        
	}
	
	var sttrow = 1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				var data_donvi = $(this).attr('data-donvi');
				var table = document.getElementById("table_data");
				var row = table.insertRow(sttrow);
				row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
				row.insertCell(1).innerHTML=data_code;
				row.insertCell(2).innerHTML=data_name;
				row.insertCell(3).innerHTML=data_donvi;
				row.insertCell(4).innerHTML="<select class='ShipmentDefault' name='ShipmentID[]' data-id='"+data_id+"' onchange='get_available_by_shipment("+data_id+",this)'></select>";
				row.insertCell(5).innerHTML="<span class='Availabletd'>0</span>";
				row.insertCell(6).innerHTML="<input type='text' name='Amount[]' class='Amount_input' value='1' onchange='recal()' required />";
				row.insertCell(7).innerHTML="<input type='text' name='ProductsNote[]' />";
				sttrow=sttrow+1;
			}
		});
		loadshipmentdefault();
		recal();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoSender').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			$(this).load(link+"get_shipment_by_productsID/"+data+"/"+warehouse);
		});
	}

	function get_available_by_shipment(id,ob){
		var KhoSender = $("#KhoSender").val();
		var Shipment = $(ob).val();
		$(ob).parent('td').parent('tr').find('.Availabletd').load(link+"get_available_by_shipment/"+id+"/"+KhoSender+"/"+Shipment);
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				sttrow = sttrow-1;
			}
		});
		recal();
	});

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseFloat(parent.find('input.Amount_input').val());
			max = parseFloat(parent.find('.Availabletd').html().replace(/,/g, ''));
			if(amount>max){
				parent.find('input.Amount_input').val(max);
				amount = max;
			}
			tongcong = tongcong+amount;
		});
		$(".tongcong").html(tongcong.toFixed(2));
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function set_empty_table(){
		$("#table_data .selected_products").each(function(){
			$(this).parent('td').parent('tr').remove();
			sttrow=1;
		});
	}
</script>
<style>
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(5){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(6){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(7){text-align:right;}
</style>