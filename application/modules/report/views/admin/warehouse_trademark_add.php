<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST" enctype="multipart/form-data">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Tạo mới thương hiệu</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-8'>
								<input type='radio' name="Published" value="1" checked="true" /> Enable 
								<input type='radio' name="Published" value="0" /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Tên thương hiệu</label>
							<div class='col-xs-8'><input type='text' class="form-control" name="Title" required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Hình đại diện (200x200)</label>
							<div class="col-xs-8">
								<input type="file" name="Image_upload" id="choosefile" onchange="viewimage()" /> 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">
							<div class="col-xs-4"></div><div class="col-xs-3 dvPreview"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Mô tả ngắn</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<textarea class="form-control" name="Description"><?php echo isset($data->Description) ? $data->Description : '' ; ?></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label class="col-xs-2 control-label">Nội dung mô tả thương hiệu</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<textarea class="resizable_textarea form-control ckeditor" name="Content"></textarea>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	function viewimage(){
        var Fileinput = document.getElementById("choosefile");
        var file = Fileinput.files[0];
        var imageType = /image.*/
        var dvPreview = $(".dvPreview");
        dvPreview.html("");
        if(file.type.match(imageType)){
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $("<img />");
                img.attr("class", "img-responsive");
                img.attr("src", e.target.result);
                dvPreview.html(img);
            }
            reader.readAsDataURL(file);
        }else{
            console.log("Not an Image");
        }
    }
</script>