<?php 
if($this->user->UserType==6 && ($data->Status!=1 && $data->Status!=3)){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}elseif($this->user->UserType==2 && $data->Status!=0){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}elseif($this->user->UserType==3 && $data->Status!=2){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}elseif($data->Status==4||$data->Status==5){
	echo '<div class="warning_message" style="display:block"><i class="fa fa-exclamation-triangle"></i> <span>Phiếu nhập kho này đã bị khóa , mọi thay đổi của bạn sẽ không được áp dụng .</span><a id="close_message_warning"><i class="fa fa-times"></i></a></div>' ;
}
?>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update_from_cm' ?>" method="POST">
			<input type='hidden' name='ID' value='<?php echo $data->ID ?>' />
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN PHIẾU YÊU CẦU NHẬP KHO <b style="font-size:inherit;color:#D94A38"><?php echo $data->MaNK ?></b> </h1>
				</div>
				<div class="block2">
					
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="row">
					<div class="form-group">
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Nhập tại kho</label>
							<div class="col-xs-7">
								<select name='KhoID' class="form-control">
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											if($this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ){
												if($data->KhoID==$row->ID){
													echo "<option value='$row->ID' selected='selected'>$row->MaKho</option>";
												}
											}else{
												$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
												echo "<option value='$row->ID' $selected>$row->MaKho</option>";	
											}
											
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Ngày chứng từ</label>
							<div class="col-xs-7">
								<input type='text' class="form-control" name='NgayNK' <?php echo $this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ? "" : "id='NgayNK'" ; ?> value='<?php echo date('Y-m-d') ?>' value='<?php echo date('Y-m-d',strtotime($data->NgayNK)) ?>' readonly="true" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12">
							<label class="control-label col-xs-2">Hình thức nhập</label>
							<div class="col-xs-7" style="margin-left: -60px;line-height: 22px;">
								<input type='radio' name="Type" value="0" checked="checked" class='pull-left' style='margin-left:30px;margin-right:8px' />
								<span class='pull-left'>Mua hàng</span>
								<input type='radio' name="Type" value="1" disabled="true" class='pull-left' style='margin-left:30px;margin-right:8px;margin-left:20px' />
								<span class='pull-left'>Hàng trả về</span>
								<input type='radio' name="Type" value="2" disabled="true" class='pull-left' style='margin-left:30px;margin-right:8px;margin-left:20px' />
								<span class='pull-left'>Thành phẩm / Trả kho nội bộ</span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Theo PO số</label>
							<div class="col-xs-7">
								<?php 
								$PO = $this->db->query("select POCode from ttp_report_perchaseorder where ID=$data->POID")->row();
								echo $PO ? "<input type='text' class='form-control' name='POID' value='".$PO->POCode."' />" : "" ;
								?>
							</div>
						</div>
						<div class="col-xs-4">
							<label class="control-label col-xs-5">Tên nhà cung cấp</label>
							<div class="col-xs-7 input-group">
								<select name="ProductionID" id="ProductionID" class="form-control">
									<?php 
									$production = $this->db->query("select ID,Title from ttp_report_production where Published=1")->result();
									if(count($production)>0){
										foreach($production as $row){
											if($this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ){
												if($data->ProductionID==$row->ID){
													echo "<option value='$row->ID' selected='selected'>$row->Title</option>";
												}
											}else{
												$selected = $data->ProductionID==$row->ID ? "selected='selected'" : '' ;
												echo "<option value='$row->ID' $selected>$row->Title</option>";
											}
										}
									}
									?>
								</select>
								<?php 
								if($this->user->UserType!=2 && $this->user->UserType!=8 && $this->user->UserType!=3 && $this->user->UserType!=7 ){
								?>
								<span class="input-group-btn"><a class='btn btn_default' title="Thêm nhà cung cấp mới" onclick="add_production(this)"><i class="fa fa-plus"></i></a></span>
								<?php 
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12">
							<label class="control-label col-xs-2">Diễn giải</label>
							<div class="col-xs-10">
								<input type='text' class="form-control" name="Note" value='<?php echo $data->Note ?>' style="margin-left:-30px" required <?php echo $this->user->UserType==2 || $this->user->UserType==8  || $this->user->UserType==3  || $this->user->UserType==7 ? "readonly='true'" : "" ; ?> />
							</div>
						</div>
					</div>
				</div>
				<!-- end block1 -->
		    	<div class="block2">
		    		<a class="btn btn-primary" id="add_products_to_order"><i class="fa fa-plus"></i> Sản Phẩm</a>
					<ul>
						<li><a id='show_thaotac'>Thao tác<b class="caret"></b></a>
		    				<ul>
			    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
		    				</ul>
						</li>
					</ul>
				</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang table_data">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th style="width: 150px;">Số lượng <br>của PO</th>
		    				<th style="width: 150px;">Số lượng thực tế <br>đã nhập</th>
		    				<th style="width: 130px;">SL yêu cầu nhập</th>
		    			</tr>
		    			<?php 
		    			$Quantity_po = array();
				        if($data->POID>0){
				            $detailspo = $this->db->query("select ProductsID,Amount from ttp_report_perchaseorder_details where POID=$data->POID")->result();
				            if(count($detailspo)>0){
				                foreach($detailspo as $row){
				                    $bonus[] = $row->ProductsID;
				                    $Quantity_po[$row->ProductsID] = $row->Amount;
				                }
				            }
				        }

				        $Quantity_products = array();
				        if($bonus!='' && $data->POID>0){
				            $bonus_quantity = count($bonus)>0 ? ' and a.ProductsID in('.implode(',',$bonus).')' : '' ;
				            $products = $this->db->query("select a.ProductsID,sum(a.Amount) as Amount from ttp_report_inventory_import_details a,ttp_report_inventory_import b where b.ID=a.ImportID and b.POID=$data->POID $bonus_quantity group by a.ProductsID")->result();
				            if(count($products)>0){
				                foreach($products as $row){
				                    $Quantity_products[$row->ProductsID] = $row->Amount;
				                }
				            }
				        }

		    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_inventory_import_details a,ttp_report_products b where a.ImportID=$data->ID and a.ProductsID=b.ID")->result();
		    			$arrproducts = array();
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					echo '<tr>
		    							<td><input type="checkbox" class="selected_products" data-id="'.$row->ProductsID.'"><input type="hidden" name="ProductsID[]" value="'.$row->ProductsID.'"></td>
		    							<td>'.$row->MaSP.'</td>
		    							<td>'.$row->Title.'</td>
		    							<td>'.$row->Donvi.'</td>';
	    						$temp_po = isset($Quantity_po[$row->ProductsID]) ? $Quantity_po[$row->ProductsID] : 0 ;
               	 				$temp_products = isset($Quantity_products[$row->ProductsID]) ? $Quantity_products[$row->ProductsID] : 0 ;
               	 				$max = $temp_po-$temp_products;
	    						echo '<td style="text-align:right"><span class="quantitypo">'.number_format($temp_po).'</span></td>';
	    						echo '<td style="text-align:right">'.number_format($temp_products).'</td>';
	    						echo '<td><input type="number" name="Amount[]" class="Amount_input" value="'.$row->Request.'" onchange="changerow(this)" data-max="'.$max.'" required ></td></tr>';
		    					$arrproducts[] = '"data'.$row->ProductsID.'"';
		    				}
		    			}
		    			?>
		    			<tr>
		    				<td colspan="6">TỔNG CỘNG</td>
		    				<td><span class='tongcong'><?php echo number_format($data->TotalAmount) ?></span></td>
		    			</tr>
		    		</table>
		    		<div class="history_status">
			    		<h3 style="margin-bottom:10px">Lịch sử trạng thái yêu cầu nhập kho</h3>
			    		<?php 
			    		$history = $this->db->query("select a.*,b.UserName from ttp_report_inventory_import_history a,ttp_user b where a.UserID=b.ID and a.ImportID=$data->ID")->result();
			    		if(count($history)>0){
			    			$arr_status = array(
			                    0=>'Yêu cầu nhập kho',
			                    1=>'Yêu cầu bị trả về từ kho',
			                    2=>'Yêu cầu chờ kế toán xử lý',
			                    3=>'Yêu cầu bị trả về từ kế toán',
			                    4=>'Hàng đã nhập kho thành công',
			                    5=>'Yêu cầu bị hủy'
			                );
			    			echo "<table><tr><th style='width:250px'>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
			    			foreach($history as $row){
			    				echo "<tr>";
			    				echo isset($arr_status[$row->Status]) ? "<td>".$arr_status[$row->Status]."</td>" : "<td>--</td>" ;
			    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Created))."</td>";
			    				echo isset($row->Note) ? "<td>".$row->Note."</td>" : "<td>--</td>";
			    				echo "<td>$row->UserName</td>";
			    				echo "</tr>";
			    			}
			    			echo "</table>";
			    		}
			    		?>
			    	</div>
		    		<div class='row' style='margin-top:20px'>
		    			<div class="form-group">
			    			<div class="col-xs-12">
			    				<label class='control-label col-xs-2'>Tình trạng chứng từ:</label>
			    				<div class="col-xs-3">
			    					<select name="Status" class="form-control">
			    						<option value='0' selected='selected'>Yêu cầu nhập kho</option>
			    					</select>
			    				</div>
			    			</div>
		    			</div>
		    			<div class="form-group">
			    			<div  class="col-xs-12">
			    				<label class='control-label col-xs-12'>Ghi chú tình trạng</label>
			    				<div class="col-xs-12"><textarea name="Ghichu" class="form-control" style='width: 97%;margin-left: 15px;'></textarea></div>
			    			</div>
		    			</div>
		    			<button class='btn btn-danger' style='margin-left:15px;margin-top:15px'><i class='fa fa-refresh'></i> Cập nhật</button>
		    		</div>
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
	} 

	document.onkeypress = stopRKey;

	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function changestatus(){
		$("#IsChangeOrder").val("1");
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function add_production(ob){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Thêm nhà cung cấp mới");
		$(".over_lay .box_inner .block2_inner").html("<div class='row'><p style='margin-bottom:5px'>Tên nhà cung cấp</p><div><input type='text' class='title_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Mã nhà cung cấp</p><div><input type='text' class='code_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Địa chỉ trụ sở</p><div><input type='text' class='address_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Số điện thoại</p><div><input type='text' class='phone_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Fax</p><div><input type='text' class='fax_production' style='margin-bottom:5px' /></div></div><div class='row'><button class='btn btn-primary' style='float:left;background:#1A82C3;color:#FFF;border: 1px solid #2477CA;' onclick='save_production(this)'>Lưu dữ liệu</button></div>");
		$(".over_lay").show();
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function save_production(ob){
		$(ob).addClass("saving");
		var title = $(".title_production").val();
		var code = $(".code_production").val();
		var address = $(".address_production").val();
		var phone = $(".phone_production").val();
		var fax = $(".fax_production").val();
		if(title!='' && code!=''){
			$.ajax({
            	url: link+"add_production",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "Title="+title+"&code="+code+"&address="+address+"&phone="+phone+"&fax="+fax,
	            success: function(result){
	            	$("#ProductionID").prepend(result);
	            	$(".over_lay").hide();
					disablescrollsetup();
					$(ob).removeClass("saving");
	            }
	        });
		}else{
			alert("Vui lòng điền đầy đủ thông tin !");
		}
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: link+"save_shipment",
            dataType: "html",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	load_shipment(id);
            	$(".over_lay").hide();
				disablescrollsetup();
				$(ob).removeClass("saving");
            }
        });
	}

	$("#add_products_to_order").click(function(){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"850px"});
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "",
            success: function(result){
                if(result!='false'){
        			$(".over_lay").show();
					$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	var celldata = [<?php echo implode(',',$arrproducts) ?>];
	var sttrow = <?php echo count($arrproducts)+1 ?>;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				var data_donvi = $(this).attr('data-donvi');
				var data_po = parseInt($(this).attr('data-po'));
				var data_products = parseInt($(this).attr('data-products'));
				max = data_po-data_products;
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML=data_donvi;
					row.insertCell(4).innerHTML=data_po.format('a',3);
					row.insertCell(5).innerHTML=data_products.format('a',3);
					row.insertCell(6).innerHTML="<input type='number' name='Amount[]' class='Amount_input' value='1' max='"+max+"' onchange='recal()' required />";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
			}
		});
		recal();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	
	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				var index = celldata.indexOf("data"+data_id);
				celldata.splice(index, 1);
				sttrow = sttrow-1;
			}
		});
		recal();
	});

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			tongcong = tongcong+amount;
		});
		$(".tongcong").html(tongcong.format('a',3));
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		Max = $(ob).attr('data-max');
		Amount = $(ob).val();
		if(Max<Amount){
			alert("Cảnh báo : Số bạn vừa nhập đã vượt quá số lượng PO !.");
			$(ob).val(Max);
		}
		recal();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	$('form input').on('keypress', function(e) {
	    return e.which !== 13;
	});
</script>
<style>
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(5){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(6){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(7){text-align:right;}
</style>