<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <?php echo $_title; ?>
    <base href="<?php echo base_url() ?>">
    <script src="public/pos/js/jquery.min.js"></script>
	<link type="image/x-icon" href="public/admin/images/favicon.png" rel="shortcut icon">
    <link rel="stylesheet" href="public/admin/fonts/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="public/admin/css/bootstrap.min.css" >

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="public/admin/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="public/pos/css/style.css">
</head>
<body>
	<?php echo $header; ?>
	<?php echo $sitebar; ?>
	<?php echo $content; ?>
	<div class="fade_excuting hidden"></div>
	<script>
		var baseurl = "<?php echo base_url().ADMINPATH.'/pos/' ?>";
	</script>
	
	<script src="public/pos/js/scripts.js"></script>
</body>
</html>