<div class="containner">
		<div class="import_orderlist">
			<div class="block2 row">
	    		<div class="block_2_1 col-xs-8">
	    			<h3 style="margin:0px;">Danh sách khách hàng trúng thưởng</h3>
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>Tên khách hàng</th>
						<th>Số điện thoại</th>
						<th>Phần quà</th>
						<th>Thời gian</th>
						<th>Tình trạng</th>
					</tr>
					<?php
          	$prize = $this->db->query("select * from ttp_define where `group`='prize' and `type`='lucky_draw'")->result();
          	$arr_prize = array();
          	if(count($prize)>0){
            	foreach($prize as $row){
                	$arr_prize[$row->code] = $row->name;
            	}
          	}
          	$arr = array(0=>'off',1=>"on");
          	$arr_state = array(0=>'Khách chưa nhận qùa',1=>"Khách đã nhận quà");
			if(count($data)>0){
				$i=$start;
				foreach($data as $row){
					$i++;
					echo "<tr>";
					echo "<td style='width:30px;text-align:center;background:#F7F7F7'>$i</td>";
					echo "<td>$row->Name</td>";
					echo "<td>$row->Phone1</td>";
					echo "<td>".date('d/m/Y H:i',strtotime($row->Created))."</td>";
	  				echo "<td>".$arr_prize[$row->ResultCode]."</td>";
					echo "<td><button title='".$arr_state[$row->Recive]."' class='on-off ".$arr[$row->Recive]."' onclick='change_on_off(this,$row->ID,".$row->Recive.")'></button></td>";
					echo "</tr>";
				}
			}else{
				$keywords = $keywords!='' ? '"<b>'.$keywords.'</b>"' : $keywords ;
				echo "<tr><td colspan='8'>Không tìm thấy dữ liệu $keywords.</td></tr>";
			}
			?>
		</table>
		<?php if(count($data)>0) echo $nav; ?>
		</div>
		</div>
</div>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;}
	.body_content .containner table tr td:nth-child(5){max-width: 300px}
</style>
<script type="text/javascript">
	function change_on_off(ob,id,state){
		$.post("<?php echo base_url().ADMINPATH ?>"+"/report/import_customer/change_state_prize",{id:id,state:state},function(result){
			if(result.error==false){
				if(state==0){
					$(ob).removeClass('off').addClass('on');
				}else{
					$(ob).removeClass('on').addClass('off');
				}
			}else{
				alert(result.message);
			}
		},'json');
	}
</script>
