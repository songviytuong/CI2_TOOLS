<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
    .body_content .containner .black .box_inner .block1_inner h1{
        font-size:13px;
    }
</style>
<?php
    $disabled = "";
    if($market_status_active == 0){
        
    }else if($market_status_active == 1){
        
    }else if($market_status_active == 2){
        $disabled = "disabled";
    }else if($market_status_active == 3){
    }else if($market_status_active == 4){
    }else if($market_status_active == 5){
        $disabled = "disabled";
    }else if($market_status_active == 6){
    }else if($market_status_active == 7){
        $disabled = "disabled";
    }
?>
<div class="table_data">
    <table id="table_data" class="listPlaner">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center col-xs-1">Loại</th>
            <th class="text-left">Tên tài liệu</th>
            <th class="text-center col-xs-2">Size</th>
            <th class="text-center col-xs-1"></th>
        </tr>
        <tbody class="showContent">
            
        </tbody>
    </table>
    
    <input type='hidden' id="IDEdit" name="IDEdit" value='<?=$_REQUEST["FileID"]?>' />
    <input type='hidden' id="baselink_url" value="<?php echo base_url().ADMINPATH.'/report/marketing_campaign/' ; ?>" />
    <div id="dvPreview"></div>
    <div class="row">
            <div class="col-xs-4"></div>
            <div class="col-xs-8 text-right">
                <a class="btn btn-primary btn-sm activeupload" onclick="activeupload(this,'filerow')"><i class="fa fa-save"></i> Đăng tải & Lưu</a>
                <!--<a class="btn btn-danger btn-sm"><i class="fa fa-search"></i> Chọn File...</a>-->
                <a class="btn btn-primary btn-sm btn-file choosefile" <?=$disabled?>>
                    <i class="fa fa-folder-open" aria-hidden="true"></i> Browse...
                    <input type="file" name="Images_upload[]" id="choosefile" multiple/>
                </a>
            </div>
    </div>
</div>
<script type="text/javascript">
var PlanID = $('#IDEdit').val();
$(document).ready(function () {
    setTimeout(function(){
        $(".activeupload").hide();
        loadFileAttach(PlanID);
    },100);
});

function loadFileAttach(PlanID){
    $.ajax({
        url: baselink+"marketing_campaign/loadFileAttach",
        dataType: "html",
        type: "POST",
        data: "PlanID="+PlanID,
        success: function(result){
            $('.showContent').html(result);
        }
    });
}

function delFileAttach(key,planid){
    $.ajax({
        url: baselink+"marketing_campaign/delFileAttach",
        dataType: "html",
        type: "POST",
        data: "PlanID="+planid+"&key="+key,
        success: function(result){
            loadFileAttach(planid);
        }
    });
}

var imageeach = [];
function activeupload(ob,temp,IDEdit){
    var i=0;
    $("."+temp).each(function(){
        imageeach[i] = $(this).attr('data');
        i++;
    });
    
    if(i>0){
        $(ob).find('i').hide();
        $(ob).addClass("saving");
        if(temp=='filerow'){
            eachupload(0,imageeach.length);
            $(ob).removeClass("saving");
        }
    }
    
}

function eachupload(ob,count){
        var IDEdit = $("#IDEdit").val();
        temp = ob+1;
        if(ob==count){
            loadFileAttach(IDEdit);
            return false;
        }
        ob = imageeach[ob];
        var data = $(".filerow"+ob).attr('data');
        var Fileinput = document.getElementById("choosefile");
        var file = Fileinput.files[data];
        var fd = new FormData();
        
        fd.append('file', file);
        fd.append('ID',IDEdit);
        var xhr = new XMLHttpRequest();
        var baselink = $("#baselink_url").val();
        xhr.open('POST', baselink + "uploadFile", true);
        xhr.onload = function() {
            if (this.status == 200) {
                if(xhr.responseText!="False"){
                        $(".filerow"+ob).remove();
                }else{
                        $(this).css({'border':'1px solid #F00'});
                }
                eachupload(temp,imageeach.length);
            }else{
                eachupload(temp,imageeach.length);
            }
        };
        xhr.send(fd);
        
}

$("#choosefile").change(function(){
    var Fileinput = document.getElementById("choosefile");
    var numfile = Fileinput.files.length;
    var dvPreview = $("#dvPreview");
    if(numfile>0){
        dvPreview.html("");
        var j=0;
        for (var i = 0; i < numfile; i++) {
            var reader = new FileReader();
            var file = Fileinput.files[i];
            var imageType = /.*/;
            var head = "";
            if(file.type.match(imageType)){
                reader.onloadend = function (e) {
                    var file = Fileinput.files[j];
                    dvPreview.append("<div class='table_data filerow filerow"+j+"' data='"+j+"'>\n\
            <div class='col-sm-1 text-center'><img src='"+e.target.result+"' class='img-responsive' /></div>\n\
            <div class='col-sm-6'>"+file.name+"</div>\n\
            <div class='col-sm-2'>"+file.type+"</div>\n\
            <div class='col-sm-2 text-center'>"+file.size+"</div>\n\
            <div class='col-sm-1 text-center'><a onclick='remove_imagerow(this)' title='Xóa'><i class='fa fa-trash-o'></i></a></div>\n\
            </div>");
                    $(".activeupload").show();
                    j++;
                }
            }else{
                console.log("Not an Image");
            }
            reader.readAsDataURL(file);
        }
        $(".dvPreview").slideDown('slow');
    }
});

function remove_imagerow(ob){
    $(ob).parent('div').parent('div').remove();
    var temp = 0;
    $(".filerow").each(function(){
            temp++;
    });
    if(temp==0){
        $(".activeupload").hide();
        $("#dvPreview").html("");
    }
}
</script>