<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>System Authentication | Login</title> 
        <link href="<?php echo base_url() ?>public/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>public/admin/css/animate.min.css" rel="stylesheet">
        <script src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>
        <style>
            body{background: #158796;}
            .login_content{width:60%;margin:60px auto; font-size: 14px;}
            .login_content img{display: block;margin:10px auto;}
            .login_content .row{}
            .login_content .row p{margin-bottom:10px;color:#FFF;display: block;font-size: 14px;}
            .login_content .row input{margin-bottom:20px;border-radius: 0px;}
            .login_content .row input[type="submit"]{width:100%;padding:10px 0px; margin-top:10px;}
            .separator{margin-top:50px;text-align: center;}
            .separator p{color:#ccc;}
        </style>
        <script src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>
        <script src="<?php echo base_url() ?>public/admin/js/script_admin.js?v=1"></script>
    </head>
    <body>
        <div id="login" class="animate form">
            <section class="login_content">
                <form method="post" action="<?php echo base_url() . ADMINPATH . "/check-login" ?>">
                    <div><img src='<?php echo base_url() ?>public/admin/images/logo_mobile.png' /></div>
                    <div class='row'><p style="text-align:center; font-size:14px;">Auchan Retail Vietnam</p></div>
                    <div class='row'><p></p></div>
                    <div class='row'><p>Username:</p></div>
                    <div class='row'>
                        <input type="text" class="form-control" placeholder="Username" name="username" required id="username" autocomplete="off"/>
                    </div>
                    <div class='row'><p>Password:</p></div>
                    <div class='row'>
                        <input type="password" class="form-control" placeholder="Password" required name="password" id="password" autocomplete="off" />
                    </div>
                    <div class='row'>
                        <input type="submit" name="commit" value="Login" class="btn btn-danger submit" />
                    </div>
                    <div class="right_box text-center" style="color: #fff;">
                        <h5>Your IP: <?php echo $_ci_vars['yourip'] ?> </h5>
                        <h5>Referer : <?php echo $_ci_vars['referer'] ?></h5>
                        <div>
                            <?php if ($_ci_vars['type'] == 1) : ?>
                                <span class="btn btn-success">Wait to be accepted before you start</span>
                            <?php endif; ?>
                            <?php if ($_ci_vars['type'] == 2): ?>
                                Click <a class="accept btn btn-success btn-sm">here</a> to Accept Login
                            <?php endif; ?>
                        </div>
                    </div>
                </form>
            </section>

        </div>

    </body>
</html>