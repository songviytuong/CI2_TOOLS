<?php 
$current = $this->uri->segment(3);
$current = explode('_',$current);
$current = isset($current[0]) ? $current[0] : '' ;
$current_training = $this->uri->segment(2);
?>
<div class="header">
    <div class="block1">
        <a href="<?php echo base_url().ADMINPATH."/report" ?>">
        <img src='public/admin/images/logo.png' />
        <p class='p1'>DOHATO</p>
        <p class='p2'>Do happy things together</p>
        </a>
    </div>
    <label for="openmenu" class="btn-openmenu-mobile"></label>
    <input type="checkbox" id="openmenu" />
    <div class="block2">
        <ul>
            <?php 
            $menu = $this->lib->get_menu_permission(0,$this->user->ID,$this->user->IsAdmin);
            if(count($menu)>0){
                foreach($menu as $row){
                    $current_checked = json_decode($row->CurrentChecked,true);
                    $selected = in_array($current_training,$current_checked) ? "class='current_group'" : " href='".base_url().ADMINPATH."/$row->Alias'" ;
                    $label = in_array($current_training,$current_checked) ? "<label onclick='showsitebar(this)'>$row->Title</label>" : $row->Title ;
                    echo "<li><a $selected>$label</a></li>";
                }
            }
            ?>
        </ul>
    </div>
    <div class="info_user">
        <ul>
        <?php 
        $checkleader = $this->db->query("select ID from ttp_report_team where UserID=".$this->user->ID)->row();
        $checkleader = $checkleader ? "TEAM LEADER" : "" ;
        $checkleader = $this->user->IsAdmin==1 ? "Developer" : $checkleader ;
        $checkleader = $this->user->UserType==1 ? "NV Telesales" : $checkleader ;
        $checkleader = $this->user->UserType==2 ? "NV KHO" : $checkleader ;
        $checkleader = $this->user->UserType==3 ? "NV KẾ TOÁN" : $checkleader ;
        $checkleader = $this->user->UserType==4 ? "NV ĐIỀU PHỐI" : $checkleader ;
        $checkleader = $this->user->UserType==5 ? "CRM MANAGER" : $checkleader ;
        $checkleader = $this->user->UserType==6 ? "NV CM" : $checkleader ;
        $checkleader = $this->user->UserType==7 ? "KẾ TOÁN MANAGER" : $checkleader ;
        $checkleader = $this->user->UserType==8 ? "KHO MANAGER" : $checkleader ;
        $checkleader = $this->user->UserType==9 ? "CEO TTP GROUP" : $checkleader ;
        echo file_exists($this->user->Thumb) ? "<a href='".base_url().ADMINPATH."/home/profile"."'><li class='li1'><img src='".$this->user->Thumb."' /></li></a>" : "<a href='".base_url().ADMINPATH."/home/profile"."'><li class='li1'><img src='public/admin/images/user.png' /></li></a>";
        echo "<li class='li2'><a href='".base_url().ADMINPATH."/home/profile"."'>".$this->user->UserName." <br><span>$checkleader</span></a></li>";
        echo "<li class='li3'><a href='".base_url().ADMINPATH."/logout"."'>Logout <i class='fa fa-sign-out pull-right'></i></a></li>";
        ?>
        </ul>
    </div>
</div>
