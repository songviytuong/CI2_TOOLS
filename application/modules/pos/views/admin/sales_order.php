<div class="sub_menu">
	<div class="find_record">
		<a style="padding:0px 5px" href="<?php echo ADMINPATH.'/pos/pos_sales/bill' ?>"><i class="fa fa-arrow-left" style="margin-right:5px"></i> Quay lại</a>
	</div>
</div>
<div class="containner">
	<div class="order_info">
		<div class="row">
			<div class="col-xs-8">
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Mã hóa đơn</span>: <?php echo $data->MaDH ?></div>
				<?php 
				$arr = array(
					0=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Thành công</a>',
					1=>'<a class="status-button"><i class="fa fa-circle text-danger" aria-hidden="true"></i> Đã hủy</a>',
					7=>'<a class="status-button"><i class="fa fa-circle text-warning" aria-hidden="true"></i> Đang giao hàng</a>'
				);
				?>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Trạng thái</span>: <?php echo $arr[$data->Status]; ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Thời gian</span>: <?php echo date('d/m/Y H:i',strtotime($data->Ngaydathang)) ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Chi nhánh bán</span>: <?php echo $data->WarehouseTitle ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Khách hàng</span>: <?php echo $data->Name ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Người bán hàng</span>: <?php echo $data->UserName ?></div>
			</div>
			<div class="col-xs-4">
				<textarea placeholder="Ghi chú ..."><?php echo $data->Note ?></textarea>
			</div>
		</div>
	</div>
	<div class="table_div" style="margin-top:20px">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã SKU</th>
				<th>Tên sản phẩm</th>
				<th>Đơn vị</th>
				<th>Số lượng</th>
				<th>Giá bán</th>
				<th>Đã giảm</th>
				<th>Thành tiền</th>
			</tr>
			<tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
				$total = 0;
				if(count($details)>0){
					$i=1;
					foreach ($details as $row) {
						$total = $total + ($row->Price*$row->Amount);
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td>'.$row->MaSP.'</td>
						<td>'.$row->Title.'</td>
						<td>'.$row->Donvi.'</td>
						<td>'.$row->Amount.'</td>
						<td>'.number_format($row->Price).'</td>
						<td>'.number_format($row->PriceDown).'</td>
						<td>'.number_format($row->Price*$row->Amount).'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>TỔNG CỘNG : </b></td>
				<td><b><?php echo number_format($total) ?></b></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>GIẢM GIÁ : </b></td>
				<td><b><?php echo number_format($data->Chietkhau) ?></b></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>CHI PHÍ KHÁC : </b></td>
				<td><b><?php echo number_format($data->Chiphi) ?></b></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>THÀNH TIỀN PHẢI TRẢ : </b></td>
				<td><b><?php echo number_format($data->Total - $data->Chietkhau + $data->Chiphi) ?></b></td>
			</tr>
		</table>
	</div>
	<hr>
	<div class="row text-right">
		<div class="col-xs-12">
		<a class="btn btn-default" href="<?php echo ADMINPATH.'/pos/pos_sales/edit_order/'.$data->ID ?>"><i class="fa fa-pencil"></i> Điều chỉnh hóa đơn</a>
		<?php 
			if($data->Status==0){
				?>
				<a class="btn btn-danger" href="<?php echo ADMINPATH.'/pos/pos_sales/drop_order/'.$data->ID ?>"><i class="fa fa-times"></i> Hủy đơn hàng</a>
				<?php
			}
		?>
		<a class="btn btn-primary" href="<?php echo ADMINPATH.'/pos/pos_sales/print_order/'.$data->ID ?>"><i class="fa fa-print"></i> In hóa đơn</a>
		<a class="btn btn-success" target="_blank" href="<?php echo ADMINPATH.'/pos/pos_sales/export_warehouse/'.$data->ID ?>"><i class="fa fa-print"></i> In phiếu xuất kho</a>
		</div>
	</div>
</div>
