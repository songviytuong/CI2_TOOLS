<div class="containner">
    <form action="<?php echo $base_link.'save_data_update_supplier' ?>" method="post">
    	<div class="import_select_progress">
    	    <div class="block1">
    	    	<h1>Bảng cập nhật danh sách sản phẩm</h1>
    	    </div>
        </div>
        <div class="row">
        	<label class="col-xs-2 control-label"><b>Nhà cung cấp</b></label>
        	<label class="col-xs-10 control-label">
        		: <?php echo $data->SupplierTitle ?>
        	</label>
        </div>
        <div class="row">
            <label class="col-xs-2 control-label"><b>Tiêu đề bảng cập nhật</b></label>
            <label class="col-xs-10 control-label">
                : <?php echo $data->Title ?>
            </label>
        </div>
        <div class="row">
            <label class="col-xs-2 control-label"><b>Mô tả cho bảng cập nhật</b></label>
            <label class="col-xs-10 control-label">
                : <?php echo $data->Note ?>
            </label>
        </div>
        <hr>
        
        <div id="content_import">
            <div class="row" style="margin:0px">
                <div class="col-xs-12"><h4 style="margin-left:-15px">Kết quả bảng cập nhật</h4></div>
                <div class="col-xs-4 specialcol4">
                    <p>Bạn đã cập nhật <?php echo $data->TotalProducts ?> sản phẩm</p>
                    <p>- có <b><?php echo $data->InUCC ?></b> sản phẩm hiện có của UCC</p>
                    <p>- và <b><?php echo $data->NotInUCC ?></b> sản phẩm có mã mới </p>
                </div>
                <div class="col-xs-4 specialcol4">
                    <p><b><?php echo $data->RemainUCC ?></b> sản phẩm hiện có trong UCC nhưng không có trong DS bạn vừa cập nhật. Sẽ được hiểu là không có hàng hay không thay đổi về giá bán?</p>
                    <a onclick="activeme(this,0)"><i class="fa fa-check-square <?php echo $data->RemainStatus==0 ? "active" : '' ; ?>" aria-hidden="true"></i> Không có hàng</a>
                    <a onclick="activeme(this,1)"><i class="fa fa-check-square <?php echo $data->RemainStatus==1 ? "active" : '' ; ?>" aria-hidden="true"></i> Không thay đổi giá bán</a>
                </div>
            </div>
            <hr>
            <?php 
            $details = $this->db->query("select * from ttp_report_products_supplier_update_details where UpdateID=$data->ID")->result();
            ?>
            <h4>Danh sách sản phẩm đã cập nhật <a href="<?php echo count($details>0) ? $base_link.'download_update_products/'.$data->ID : $base_link.'#' ?>" class="btn-sm btn-primary pull-right" style="margin-bottom:10px"><i class="fa fa-download"></i> Download</a></h4>
            <table class='table table-bordered'>
            <tr>
                <th>STT</th>
                <th>Mã SP</th>
                <th>Tên sản phẩm NCC</th>
                <th style='width: 80px;'>ĐVT</th>
                <th>Giá bán lẻ (bao gồm VAT)</th>
                <th>Giá bán theo đơn vị chuẩn</th>
                <th>Giá bán cho UCC</th>
                <th style='width: 100px;'>Tình trạng</th>
            </tr>
            <?php 
            
            if(count($details)>0){
                $i=1;
                $arr = array(0=>'<a class="text-primary"><i class="fa fa-check-circle"></i> Cập nhật</a>',1=>'<a class="text-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tạo mới</a>');
                foreach($details as $row){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td>$row->MaSP</td>";
                    echo "<td>$row->Title</td>";
                    echo "<td>$row->Unit</td>";
                    echo "<td>".number_format($row->Price)."</td>";
                    echo "<td>$row->BasePrice</td>";
                    echo "<td>".number_format($row->RootPrice)."</td>";
                    echo "<td>".$arr[$row->Status]."</td>";
                    echo "</tr>";
                    $i++;
                }
            }
            ?>
        </div>
    </form>
</div>
<style>
	.icon_list{width: 30px;height:30px;text-align:center;line-height: 30px;background: #eee;border-radius: 50%;float:right;}
	.col-xs-4.specialcol4{    background: #fff9dc;padding: 10px 15px;min-height: 105px;margin-right:15px;border: 1px solid #f9eebb;}
    .col-xs-4.specialcol4 a{margin-right:15px;}
    .col-xs-4.specialcol4 i{margin-right: 5px;font-size: 15px;color:#ccc;}
    .col-xs-4.specialcol4 i.active{color:#090;}
    .col-xs-4.specialcol4 b{color:#27c;}
</style>