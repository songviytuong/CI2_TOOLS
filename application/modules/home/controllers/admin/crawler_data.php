<?php 
class Crawler_data extends Admin_Controller { 
	public $user;
	public $classname="crawler_data";
	public $limit = 15;
	public $mediasite = "http://ucancook.vn/assets/";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Data list | Crawler Config');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_crawler_data")->row();
        $nav = $nav ? $nav->nav : 0;
		$object = $this->db->query("select * from ttp_crawler_data order by Created DESC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_data/',
			'data'		=>	$object,
			'start'		=>	$start,
			'find'		=>  $nav,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/crawler_data/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/crawler_data_home',$data);
		$this->template->render();
	}

	public function search($link='search'){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
		$keyword = $this->session->userdata("crawler_filter_Keywords");
		$Published = $this->session->userdata("crawler_filter_Published");
		$Site = $this->session->userdata("crawler_filter_Site");
		$str_nav = "select count(1) as nav from ttp_crawler_data";
		$str = "select * from ttp_crawler_data";
		$str = $keyword != '' ? $str." where Destination like '%".urldecode($keyword)."%'" : $str ;
		$str_nav = $keyword != '' ? $str_nav." where Destination like '%".urldecode($keyword)."%'" : $str_nav ;
		if($Published!=''){
			$str.= $keyword != '' ? " and Published=$Published" : " where Published=$Published" ;
			$str_nav.=$keyword != '' ? " and Published=$Published" : " where Published=$Published" ;
		}
		if($Site!=''){
			$str.= $keyword != '' || $Published != '' ? " and Destination like '%$Site%'" : " where Destination like '%$Site%'" ;
			$str_nav.=$keyword != '' || $Published != '' ? " and Destination like '%$Site%'" : " where Destination like '%$Site%'" ;
		}
		$nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
		$this->template->add_title('Tìm kiếm dữ liệu');
		$data=array(
			'data'	=> $this->db->query($str." order by ID DESC $limit_str")->result(),
			'nav'	=> $this->lib->nav(base_url().ADMINPATH.'/home/crawler_data/'.$link,5,$nav,$this->limit),
			'start'	=> $start,
			'find'		=>  $nav,
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_data/',
		);
		$this->template->write_view('content','admin/crawler_data_home',$data);
		$this->template->render();
	}

	public function setsessionsearch(){
		if(isset($_POST['Keywords'])){
            $Keywords = mysql_real_escape_string($_POST['Keywords']);
            $this->session->set_userdata("crawler_filter_Keywords",$Keywords);
        }
		if(isset($_POST['Published'])){
            $Published = mysql_real_escape_string($_POST['Published']);
            $this->session->set_userdata("crawler_filter_Published",$Published);
        }
        if(isset($_POST['Site'])){
            $Site = mysql_real_escape_string($_POST['Site']);
            $this->session->set_userdata("crawler_filter_Site",$Site);
        }
		$this->search('setsessionsearch');
	}

	public function clearfilter(){
		$this->session->unset_userdata("crawler_filter_Keywords");
        $this->session->unset_userdata("crawler_filter_Published");
        $this->session->unset_userdata("crawler_filter_Site");
        $this->search('setsessionsearch');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$this->db->query("delete from ttp_crawler_data where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	
	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add data | Crawler Config');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_data/'
		);
		$this->template->write_view('content','admin/crawler_data_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Destination = isset($_POST['Destination']) ? $_POST['Destination'] : '' ;
		if($Destination==""){
			redirect(ADMINPATH.'/home/crawler_data/add');
		}
		$this->template->add_title('Next step | Crawler Config');
		$data = array(
			'base_link' => base_url().ADMINPATH.'/home/crawler_data/',
			'data'		=> explode("\n", $Destination)
		);
		$this->template->write_view('content','admin/crawler_data_nextstep',$data);
		$this->template->render();
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_crawler_data where ID=$id")->row();
			if(!$result) return;
			$this->template->add_title('Edit data | Crawler Config');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/crawler_data/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/crawler_data_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$id = isset($_POST['ID']) ? $_POST['ID'] : "" ;
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_crawler_data where ID=$id")->row();
			if($result){
				$data = json_decode($result->Data,true);
				$data['Title'] = isset($_POST['Title']) ? $_POST['Title'] : $data['Title'] ;
				$data['Introtext'] = isset($_POST['Introtext']) ? $_POST['Introtext'] : $data['Introtext'] ;
				$data['Description'] = isset($_POST['Description']) ? $_POST['Description'] : $data['Description'] ;
				$data = json_encode($data);
				$this->db->where("ID",$id);
				$this->db->update("ttp_crawler_data",array('Data'=>$data));
			}
		}
		redirect(ADMINPATH.'/home/crawler_data/');
	}

	public function crawldata(){
		$crawl = isset($_POST['type']) ? $_POST['type'] : '' ;
		$link = isset($_POST['link']) ? $_POST['link'] : '' ;
		if($crawl!='' && $link!=''){
			$crawl_regex = $this->db->query("select * from ttp_crawler_site where Site='$crawl' and Published=1")->row();
			if($crawl_regex){
				$function = $this->lib->alias($crawl);
				$this->$function($link,json_decode($crawl_regex->Data));
				return;
			}
		}
		echo "false";
	}

	/*
	****************************************************
	*	Bots get data from site afamily.vn			   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function afamilyvn($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $json['Thumb'] = $html->find("$Thumb",0) ? $html->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $content = $html->find("$Introtext",0);
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	        	$temp = $this->getfull_src("http://afamily.vn",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."afamilyvn/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'afamilyvn');
        	}
        }
	}

	/*
	****************************************************
	*	Bots get data from site phununews.vn		   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function phununewsvn($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $json['Thumb'] = $html->find("$Thumb",0) ? $html->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $content = $html->find("$Introtext",0);
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	            $temp = $this->getfull_src("http://phununews.vn",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."phununewsvn/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'phununewsvn');
        	}
        }
	}	

	/*
	****************************************************
	*	Bots get data from site phunutoday.vn		   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function phunutodayvn($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $json['Thumb'] = $html->find("$Thumb",0) ? $html->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $content = $html->find("$Introtext",0);
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	        	$temp = $this->getfull_src("http://phunutoday.vn",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."phunutodayvn/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'phunutodayvn');
        	}
        }
	}	

	/*
	****************************************************
	*	Bots get data from site eva.vn				   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function evavn($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $json['Thumb'] = $html->find("$Thumb",0) ? $html->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $content = $html->find("$Introtext",0);
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	        	$temp = $this->getfull_src("http://eva.vn",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."evavn/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'evavn');
        	}
        }
	}	

	
	/*
	****************************************************
	*	Bots get data from site ngoisao.net			   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function ngoisaonet($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $json['Thumb'] = $html->find("$Thumb",0) ? $html->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $content = $html->find("$Introtext",0);
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	            $temp = $this->getfull_src("http://ngoisao.net",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."ngoisaonet/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'ngoisaonet');
        	}
        }
	}	

	/*
	****************************************************
	*	Bots get data from site dep.com.vn			   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function depcomvn($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $json['Thumb'] = $html->find("$Thumb",0) ? $html->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $content = $html->find("$Introtext",0);
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	            $temp = $this->getfull_src("http://dep.com.vn",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."depcomvn/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'depcomvn');
        	}
        }
	}	

	/*
	****************************************************
	*	Bots get data from site phongcachsong.net	   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function phongcachsongnet($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $content = $html->find("$Introtext",0);
        $json['Thumb'] = $content->find("$Thumb",0) ? $content->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	            $temp = $this->getfull_src("http://phongcachsong.net",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."phongcachsongnet/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'phongcachsongnet');
        	}
        }
	}	

	/*
	****************************************************
	*	Bots get data from site monan9.com 		 	   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function cachlam9com($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $content = $html->find("$Introtext",0);
        $json['Thumb'] = $content->find("$Thumb",0) ? $content->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        @$content->find("center",0)->innertext = "";
        @$content->find("center",0)->innertext = "";
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	            $temp = $this->getfull_src("http://cachlam9.com",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."cachlam9com/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'cachlam9com');
        	}
        }
	}	


	/*
	****************************************************
	*	Setting Bots before get data from link 		   *
	*	$link : Destination link to get data 		   *
	*	$https : security mode of destanation resource *
	*												   *
	****************************************************
	*/

	public function crawl_apply($link="",$https=false){
		$setting = $this->db->query("select Data from ttp_crawler_setting limit 0,1")->row();
		if($setting){
			$data = json_decode($setting->Data,true);
			$ckfile = tempnam ("/tmp", "CURLCOOKIE");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_USERAGENT, $data['USERAGENT']);
			curl_setopt($ch, CURLOPT_REFERER, $data['REFERER']);
			curl_setopt($ch, CURLOPT_TIMEOUT, $data['TIMEOUT']);
			$UseProxy = isset($data['UseProxy']) ? $data['UseProxy'] : 0 ;
			if($UseProxy==1){
				curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
				curl_setopt($ch, CURLOPT_PROXY, $data['PROXYIP'].":".$data['PROXYPORT']);
				curl_setopt($ch, CURLOPT_PROXY, $data['PROXYIP'].":".$data['PROXYPORT']);
				curl_setopt($ch, CURLOPT_PROXYUSERPWD, $data['PROXYUSER'].":".$data['PROXYPASSWORD']);
			}
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $https);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			return $output;
		}
		return "";
	}

	/*
	****************************************************
	*	Bots get data from site feedy.vn 		 	   *
	*	$link : Destination link to get data 		   *
	*	$data : Code setting to get data from this site*
	*												   *
	****************************************************
	*/

	public function feedyvn($link,$data){
		include_once('application/libraries/simple_html_dom.php');
        $output = $this->crawl_apply($link);
        $html = str_get_html($output);
        $json = array();
        $Thumb = isset($data->Thumb) ? "$data->Thumb" : "" ;
        $Title = isset($data->Title) ? "$data->Title" : "" ;
        $Description = isset($data->Description) ? "$data->Description" : "" ;
        $Categories = isset($data->Categories) ? "$data->Categories" : "" ;
        $Introtext = isset($data->Content) ? "$data->Content" : "" ;
        $content = $html->find("$Introtext",0);
        $json['Thumb'] = $content->find("$Thumb",0) ? $content->find("$Thumb",0)->content : "";
        $json['Title'] = $html->find("$Title",0) ? trim($html->find("$Title",0)->innertext) : "";
        $json['Description'] = $html->find("$Description",0) ? trim($html->find("$Description",0)->plaintext) : "";
        $json['Categories'] = $html->find("$Categories",0) ? $html->find("$Categories",0)->plaintext : "";
        @$content->find("center",0)->innertext = "";
        @$content->find("center",0)->innertext = "";
        $json['Introtext'] = $content ? $content->innertext : "" ;
        $json['Images'] = array();
        if($content->find('img',0)){
	        $list_image = array();
	        foreach($content->find('img') as $row){
	            $temp = $this->getfull_src("http://feedy.vn",$row->src);
	            $ext = explode("/",$temp);
	            $ext = count($ext)>0 ? $ext[count($ext)-1] : "" ;
	            if($ext!=''){
	            	$file = explode(".",$ext);
	            	if(count($file)>0){
	            		$duoi = $file[count($file)-1];
	            		$name = sha1($temp);
	            		$json['Introtext'] = str_replace($row->src,$this->mediasite."feedyvn/$name.$duoi",$json['Introtext']);
	            		$json['Introtext'] = preg_replace('/(<[^>]+) href=".*?"/i', '$1', $json['Introtext']);
	            		$json['Images'][] = $temp;
	            	}
	            }
	        }
        }
        $data = array(
        	'Destination'	=> $link,
        	'Data'			=> json_encode($json),
        	'Created'		=> date('Y-m-d H:i:s',time()),
        	'Published'		=> 0
        );
        if($json['Title']!='' && $json['Introtext']!=''){
        	$this->db->insert('ttp_crawler_data',$data);
        	if(count($json['Images'])>0){
	        	$imagelist = json_encode($json['Images']);
	        	$this->transfer_assets_site($imagelist,'feedyvn');
        	}
        }
	}

	

	/*
	****************************************************
	*	Buffer images to static system                 *
	*	Use Public $key to connect assets system       *
	*	Public $mediasite is address assets system     *
	*												   *
	****************************************************
	*/
	public $key = "@ucancook.vn";
	
	public function transfer_assets_site($json="",$site=""){
		if($json!=""){
			$data = array(
				'USERAGENT'=>'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
				'REFERER'=>'http://tools.ucancook.vn/',
				'TIMEOUT'=>100
			);
			$field = array(
				'key'=>sha1($this->key),
				'json'=>$json,
				'site'=>$site
			);
			$ckfile = tempnam ("/tmp", "CURLCOOKIE");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->mediasite.'?trans=true');
			curl_setopt($ch, CURLOPT_USERAGENT, $data['USERAGENT']);
			curl_setopt($ch, CURLOPT_REFERER, $data['REFERER']);
			curl_setopt($ch, CURLOPT_TIMEOUT, $data['TIMEOUT']);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($field));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$output = curl_exec($ch);
			if($output=="true"){
				echo "OK";
			}else{
				echo "False";
			}
		}
	}

	/*
	****************************************************
	*	Buffer data to post site system                *
	*	Use Public $key to connect post site system    *
	*	Public $postsite is address post site system   *
	*												   *
	****************************************************
	*/

	public function transfer_post_site($json="",$postsite=""){
		if($json!=""){
			$data = array(
				'USERAGENT'=>'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
				'REFERER'=>'http://tools.ucancook.vn/',
				'TIMEOUT'=>100
			);
			$json['key']= sha1($this->key);
			$ckfile = tempnam ("/tmp", "CURLCOOKIE");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $postsite.'?trans=true');
			curl_setopt($ch, CURLOPT_USERAGENT, $data['USERAGENT']);
			curl_setopt($ch, CURLOPT_REFERER, $data['REFERER']);
			curl_setopt($ch, CURLOPT_TIMEOUT, $data['TIMEOUT']);
			curl_setopt($ch,CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,http_build_query($json));
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			$output = curl_exec($ch);
			if($output=="true"){
				return "OK";
			}else{
				return "False";
			}
		}
		return "False";
	}

	public function getfull_src($domain="",$src=""){
		if($domain!="" && $src!=""){
			$temp = explode("/",$src);
			if($temp>0){
				$http = isset($temp[0]) ? $temp[0] : "" ;
				if($http=='http:' || $http=='https:'){
					return $src;
				}else{
					if($temp[0]==''){
						unset($temp[0]);
					}
					$temp = implode("/",$temp);
					return $domain."/".$temp;
				}
			}
		}
		return "";
	}

	/*
	*********************************************
	*	Load view to action public data 		*
	*									  		*
	*********************************************
	*/

	public function public_data(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Select site | Crawler Config');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_data/',
			'data'		=> $this->db->query("select * from ttp_crawler_data where Published=0 order by ID ASC limit 0,20")->result()
		);
		$this->template->write_view('content','admin/crawler_data_public',$data);
		$this->template->render();
	}

	/*
	*********************************************
	*	Get data crawl to preview before send	*
	*	$id : ID post to preview 			    *
	*									  		*
	*********************************************
	*/
	
	public function get_crawl_post_by_id(){
		$id = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
		if(is_numeric($id)){
			$data = $this->db->query("select * from ttp_crawler_data where ID=$id")->row();
			if($data){
				$json = json_decode($data->Data);
				echo $json->Title ? "<h1>$json->Title</h1>" : "<h1>No Title</h1>";
				$site = explode('/',$data->Destination);
				$site = isset($site[2]) ? $site[2] : 'Unknown site' ;
				echo $json->Categories ? "<p style='border-bottom: 1px solid #E1E1E1;padding-bottom: 5px;'><small>$site » ".str_replace(' / ',' » ',$json->Categories)."</small></p>" : "<p style='border-bottom: 1px solid #E1E1E1;padding-bottom: 5px;'><small>$site » No Categories</small></p>";
				echo $json->Introtext ? "<div class='content_crawl_reciver'>".$json->Introtext."</div>" : "<div class='content_crawl_reciver'>No Content</div>" ;
				return;
			}
		}
		echo "false";
	}

	/*
	*********************************************
	*	Sync categories from destination site	*
	*	$site : destination to get categories   *
	*									  		*
	*********************************************
	*/

	public function get_categories_from_site(){
		$site = isset($_POST['Site']) ? $_POST['Site'] : '' ;
		if($site!=''){
			$data = $this->crawl_apply($site.'?getcat=true');
			echo $data;
			return;
		}
		echo "false";
	}

	/*
	*********************************************
	*	Send data crawl to my site	 	  		*
	*	$site : destination site send data 		*
	*	$post : Post ID from my database		*
	*	$cat : list categories from destination *
	*									  		*
	*********************************************
	*/

	public function send_post(){
		$site = isset($_POST['Site']) ? $_POST['Site'] : '' ;
		$post = isset($_POST['post']) ? $_POST['post'] : 0 ;
		$cat = isset($_POST['cat']) ? $_POST['cat'] : '' ;
		if($site!='' && $post!='' && $cat!=''){
			$result = $this->db->query("select * from ttp_crawler_data where ID=$post")->row();
			if($result){
				$json = json_decode($result->Data);
				$data = array();
				$data['post_title'] = isset($json->Title) ? $json->Title : '' ;
				$data['post_link'] = isset($json->Title) ? $this->lib->alias($json->Title) : '' ;
				$data['post_content'] = isset($json->Introtext) ? $json->Introtext : '' ;
				$data['post_description'] = isset($json->Description) ? $json->Description : '' ;
				$data['post_categories'] = $cat ;
				if($data['post_title']!='' && $data['post_content']!=''){
					$status = $this->transfer_post_site($data,$site);
					if($status=="OK"){
						$this->db->query("update ttp_crawler_data set Published=1 where ID=$post");
					}
				}
			}
		}
	}

	/*
	***************************************
	*	Crawl all links on from site	  *
	*	$str : link to crawl 			  *
	*									  *
	***************************************
	*/

	public function crawl_links_from_string(){
		$str = isset($_POST['str']) ? $_POST['str'] : '' ;
		if($str!=''){
			$data = $this->crawl_apply($str);
			include_once('application/libraries/simple_html_dom.php');
			$html = str_get_html($data);
			$arr = array();
			foreach($html->find('a') as $row){
				$explode = explode("://",$row->href);
				if(count($explode)==2){
					if($explode[0]=="http" || $explode[0]=="https"){
						$link = implode("://",$explode);
						if(!in_array("$link",$arr)){
							echo '<div class="row_links"><a onclick="uselinks(this)" title="Get this link">'.$link.'</a></div>';
							$arr[] = $link;
						}
					}
				}else{
					$link = $str.$row->href;
					if(!in_array("$link",$arr)){
						echo '<div class="row_links"><a onclick="uselinks(this)" title="Get this link">'.$link.'</a></div>';
						$arr[] = $link;
					}
				}
			}
		}
	}

	/*
	***************************************
	*	Load post from site (fillter)	  *
	*	$site : load post from this site  *
	*									  *
	***************************************
	*/

	public function load_post_available_from_site(){
		$site = isset($_POST['Site']) ? $_POST['Site'] : '' ;
		$result = $this->db->query("select * from ttp_crawler_data where Destination like '%$site%' and Published=0 limit 0,20")->result();
		if(count($result)>0){
			$i=1;
			foreach($result as $row){
				$json = json_decode($row->Data,true);
				$site = explode("/",$row->Destination);
                $type = isset($site[2]) ? $site[2] : '' ;
                $categories = isset($json['Categories']) ? "<small>$type » ".str_replace(" / "," » ",$json['Categories'])."</small>" : "<small>$type</small>";
				echo '<tr class="even pointer">
                    <td class="a-center">'.$i.'</td>
                    <td>'.$json['Title'].'<br>'.$categories.'</td>
                    <td class="a-right a-right status'.$row->ID.'" style="text-align:center"><a class="crawl_preview" data="'.$row->ID.'"><i class="fa fa-search"></i></a></td>
                    <td class="last" style="text-align:center"><input class="rowcheckbox row'.$row->ID.'" type="checkbox" value="'.$row->ID.'" /></td>
                </tr>';
				$i++;
			}
			$j = $i-1;
			if(count($result)==20)
			echo '<tr class="even pointer"><td colspan="4" style="text-align:center"><a onclick="loadmore(this)" data="'.$j.'">Load more post</a></td></tr>';
		}else{
			echo '<tr class="even pointer"><td colspan="4" style="text-align:center">Not found data .</td></tr>';
		}
	}

	/*
	***************************************
	*	Load more post from site 		  *
	*	$site : load post from this site  *
	*	$stt : start limit in result      *
	***************************************
	*/
	public function load_more_post(){
		$site = isset($_POST['Site']) ? $_POST['Site'] : '' ;
		$stt = isset($_POST['stt']) ? $_POST['stt'] : 0 ;
		$stt = $stt+1;
		$result = $this->db->query("select * from ttp_crawler_data where Destination like '%$site%' and Published=0 limit $stt,20")->result();
		if(count($result)>0){
			$i=$stt;
			foreach($result as $row){
				$json = json_decode($row->Data,true);
				$site = explode("/",$row->Destination);
                $type = isset($site[2]) ? $site[2] : '' ;
                $categories = isset($json['Categories']) ? "<small>$type » ".str_replace(" / "," » ",$json['Categories'])."</small>" : "<small>$type</small>";
				echo '<tr class="even pointer">
                    <td class="a-center">'.$i.'</td>
                    <td>'.$json['Title'].'<br>'.$categories.'</td>
                    <td class="a-right a-right status'.$row->ID.'" style="text-align:center"><a class="crawl_preview" data="'.$row->ID.'"><i class="fa fa-search"></i></a></td>
                    <td class="last" style="text-align:center"><input class="rowcheckbox row'.$row->ID.'" type="checkbox" value="'.$row->ID.'" /></td>
                </tr>';
				$i++;
			}
			if(count($result)==20)
			echo '<tr class="even pointer"><td colspan="4" style="text-align:center"><a onclick="loadmore(this)" data="'.$i.'">Load more post</a></td></tr>';
		}else{
			echo '<tr class="even pointer"><td colspan="4" style="text-align:center">Not found data .</td></tr>';
		}
	}	

}
?>