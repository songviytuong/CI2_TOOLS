$(document).ready(function() {
    /*
     *	Warning delete
     */
    $(".delete").click(function() {
        if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
            return false;
        }
    });

    $(".controltab a").click(function() {
        $(".controltab a").removeClass("current");
        $(this).addClass("current");
        var data = $(this).attr("data");
        $(".tab").hide();
        $("." + data).show();
    });

    $('.accept').click(function() {
        $.ajax({
            url: "/administrator/send",
            dataType: "html",
            type: "POST",
            data: { position: 'verify' },
            context: $(this),
            success: function(result) {
                setTimeout(function() {
                    $('.accept_label').html(result);
                }, 1000);
            }
        });
    });
});