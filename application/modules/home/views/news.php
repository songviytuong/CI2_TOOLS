<script>
    var d = document.getElementById("menu_news");
    d.className = d.className + "current-menu-item";
</script>
<?php 
  if($lang=="en"){
      $pagelink = $this->uri->segment(2);
  }else{
      $pagelink = $this->uri->segment(1);
  }
?>
<div class="content blog-page communication-page">
      <div class="top-slide hidden-xs">
          <?php 
          $headerimage = $this->db->query("select * from ttp_banner where PositionID=7 order by ID DESC limit 0,1")->row();
          if($headerimage){
            echo file_exists($headerimage->Thumb) ? "<img src='$headerimage->Thumb' alt='' class='img-responsive'>" : "";
          }
          ?>
      </div>
      <!-- top-slide-->
      <?php 
      if($lang=="en"){
          $news = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Alias_en as Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%'")->row();
      }else{
          $news = $this->db->query("select a.ID,a.Title,a.Description,a.Alias from ttp_categories a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"block\":\"block1\"%'")->row();
      }
      if($news){
          echo '<div class="content-box communication-box"><div class="container">
          <h3 class="box-title"><span>'.$news->Title.'</span><i class="hidden-xs">'.$news->Description.'</i></h3><div class="row">';
          if($lang=="en"){
              $result = $this->db->query("select Title_en as Title,Data,Description_en as Description,Alias_en as Alias,Thumb from ttp_post where CategoriesID=$news->ID and Published=1 order by ID DESC limit 0,12")->result();
          }else{
              $result = $this->db->query("select Title,Data,Description,Alias,Thumb from ttp_post where CategoriesID=$news->ID and Published=1 order by ID DESC limit 0,12")->result();
          }
          if(count($result)>0){
              $i=1;
              foreach($result as $row){
                  $json = json_decode($row->Data);
                  $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                  $link = $lang=="en" ? "en/".$link : $link ;
                  $image = $this->lib->getfilesize($row->Thumb,270,151);
                  echo '<div class="col-xs-6 col-sm-4 col-md-3 post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                          <h4><a href="'.$link.'">'.$this->lib->splitString($row->Title,50).'</a></h4>
                          <p class="hidden-xs">'.$this->lib->splitString($row->Description,100).'</p>
                        </div>';
                  if(($i%3)==1){
                    echo '<div class="clearfix visible-sm"></div>';
                  }
                  $i++;
              }
          }
          $linkcat = $lang=="en" ? "en/".$news->Alias : $news->Alias ;
          $viewmore = $lang=="en" ? VIEWALL_EN : VIEWALL_VI ;
          echo '<div class="clearfix"></div><a href="'.$linkcat.'" class="btn btn-primary btn-lg center-block read-all">'.$viewmore.'</a></div></div></div>';
      }
      ?>
      </div>
    </div>