<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Danh sách sản phẩm cần xác nhận có hàng</h1>
	    </div>
	    <div class="block2">
			<div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
	    </div>
    </div>
    <?php 
    if(count($data)>0){
    ?>
    <table class="table table-bordered table-hover">
    	<tr style="background:#f5f5f5">
    		<th>STT</th>
    		<th>MÃ ĐƠN HÀNG</th>
    		<th>SẢN PHẨM</th>
    		<th>ĐƠN VỊ</th>
    		<th>SỐ LƯỢNG</th>
    		<th class="text-center">THAO TÁC</th>
    	</tr>
    	<?php 
    		$temp = 0;
    		$i=1;
    		foreach($data as $row){
    			$group = $temp!=$row->ID || $temp==0 ? "<td>$row->MaDH  <br> <small>".$this->lib->get_times_remains($row->TimeRequest)."</small></td>" : "<td>$row->MaDH  <br> <small>".$this->lib->get_times_remains($row->TimeRequest)."</small></td>" ;
    			$temp = $row->ID;
    			echo "<tr>
    				<td class='text-center'>$i</td>
    				$group
    				<td>$row->Title</td>
    				<td style='width:80px'>$row->Donvi</td>
    				<td class='text-right' style='width: 100px;'>".number_format($row->Amount,2)."</td>
    				<td class='text-center' style='width: 300px;vertical-align: middle;'><a onclick='accept_available(this,$row->ProductsID,$row->Amount,$row->ID,$row->DetailsID)' class='btn-sm btn-success' title='Click vào đây có nghĩa là bạn đã xác nhận có đủ số lượng sản phẩm này để cung cấp cho chúng tôi .'><i class='fa fa-check' aria-hidden='true' style='margin-right:3px;'></i> Có đủ</a>
    					<a onclick='skip_available(this,$row->ProductsID,$row->Amount,$row->ID,$row->DetailsID)' class='btn-sm btn-warning' title='Click vào đây để xác nhận số lượng có thể cung cấp.'><i class='fa fa-minus-circle' aria-hidden='true' style='margin-right:3px;'></i> Không đủ</a>
    					<a onclick='close_available(this,$row->ProductsID,$row->Amount,$row->ID,$row->DetailsID)' class='btn-sm btn-danger' title='Click vào đây có nghĩa là bạn đã xác nhận không đủ số lượng sản phẩm để cung cấp.'><i class='fa fa-minus-circle' aria-hidden='true' style='margin-right:3px;'></i> Hết hàng</a>
    				</td>
    			</tr>";
    			$i++;
    		}
    	?>
    </table>
    <?php 
    }else{
    	echo '<p class="text-center" style="margin:80px 0px 30px 0px;color:#999"><i class="fa fa-stack-overflow" aria-hidden="true" style="font-size:130px"></i></p>
    	<p class="text-center" style="font-size:19px;color:#999">You have no order.</p>
    	<p class="text-center" style="font-size:19px;color:#999">Please enjoy your day!</p>';

    }
    ?>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

	var link = "<?php echo $base_link ?>";

	function accept_available(ob,ProductsID,Amount,OrderID,ID){
		$(ob).addClass("saving");
		$.ajax({
            	url: link+"accept_available",
	            dataType: "html",
	            type: "POST",
	            data: "ProductsID="+ProductsID+'&Amount='+Amount+"&OrderID="+OrderID+"&ID="+ID,
	            success: function(result){
	            	$(ob).removeClass('saving');
	            	if(result=='true'){
	            		$(ob).parent('td').parent('tr').fadeOut();
	            	}else{
	            		alert("Yêu cầu không được thực thi !");
	            	}
	            }
	        });
	}

	function skip_available(ob,ProductsID,Amount,OrderID,ID){
		var AmountRes = prompt("Vui lòng điền số lượng sản phẩm bạn có thể cung cấp cho chúng tôi", Amount);
        if(AmountRes>=Amount){
            alert("Số lượng sản phẩm bạn nhập vào không thể bằng hoặc vượt quá số lượng yêu cầu .");
            return;
        }
    	if (AmountRes != null && AmountRes>0) {
			$(ob).addClass("saving");
			$.ajax({
	        	url: link+"skip_available",
	            dataType: "html",
	            type: "POST",
	            data: "ProductsID="+ProductsID+'&Amount='+Amount+"&OrderID="+OrderID+"&AmountRes="+AmountRes+"&ID="+ID,
	            success: function(result){
	            	$(ob).removeClass('saving');
	            	if(result=='true'){
	            		$(ob).parent('td').parent('tr').fadeOut();
	            	}else{
	            		alert("Yêu cầu không được thực thi !");
	            	}
	            }
	        });
		}else{
			alert("Dữ liệu không được chấp nhận !");
		}
	}

	function close_available(ob,ProductsID,Amount,OrderID,ID){
        var AmountRes = prompt("Vui lòng điền ghi chú (ví dụ : thời gian bạn có thể cung cấp sản phẩm này lại, ngưng cung cấp, ...)");
        if(AmountRes == null){
            alert("Bạn bắt buộc phải nhập ghi chú trong trường hợp này !");
            return;
        }
        var jsonArg1 = new Object();
            jsonArg1.Note = AmountRes;
        var jsonArray = JSON.stringify(jsonArg1);
		$(ob).addClass("saving");
		$.ajax({
        	url: link+"deny_available",
            dataType: "html",
            type: "POST",
            data: "ProductsID="+ProductsID+'&Amount='+Amount+"&OrderID="+OrderID+"&ID="+ID+"&Note="+jsonArray,
            success: function(result){
                console.log(result);
                $(ob).removeClass('saving');
            	if(result=='true'){
            		$(ob).parent('td').parent('tr').fadeOut();
            	}else{
            		alert("Yêu cầu không được thực thi !");
            	}
            }
        });
	}
</script>