<div class="containner">
	<div class="row">
		<div class="col-xs-7">
			<h3>Lịch sử tin nhắn hệ thống SMS</h3>
		</div>
		<div class="col-xs-5 text-right">
			<a class="btn btn-danger" onclick="open_sms_box()"><i class="fa fa-send"></i> Gửi SMS</a>
		</div>
		<div class="col-xs-12">
			<table class="table table-bordered">
				<tr style="background:#EEE">
					<th>STT</th>
					<th>Nội dung tin nhắn</th>
					<th style="width:120px">Số điện thoại</th>
					<th style="width:120px">Thời gian gửi</th>
					<th style="width:100px">Loại SMS</th>
					<th style="width:100px">Trạng thái</th>
				</tr>
				<?php 
				$arr = array(0=>"Thành công",-1=>"Lỗi");

				if(count($data)>0){
					$i=$start+1;
					foreach($data as $row){
						$row->CodeStatus = isset($arr[$row->CodeStatus]) ? $arr[$row->CodeStatus] : "Lỗi" ;
						echo "<tr>
							<td>$i</td>
							<td>$row->Message</td>
							<td>$row->Phone</td>
							<td>$row->Created</td>
							<td>$row->Type</td>
							<td>$row->CodeStatus</td>
						</tr>";
						$i++;
					}
				}
				?>
			</table>
		</div>
		<div class="col-xs-12">
			<?php echo $nav ?>
		</div>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner">
    			<div class="row">
    				<div class="col-xs-12">
    					<p><b>Số điện thoại nhận tin</b> <small>(Gửi đến nhiều số bằng cách tách các số bằng dấu ,)</small></p>
    				</div>
    				<div class="col-xs-12">
    					<input class="form-control" name="PhoneSMS" id="PhoneSMS" />
    				</div>
    			</div>
    			<div class="row"></div>
    			<div class="row">
    				<div class="col-xs-9">
    					<p><b>Nội dung tin nhắn</b> <small>(Nội dung tin nhắn bắt buộc không dấu)</small></p>
    				</div>
    				<div class="col-xs-3">
    					<select name="" id="" class="form-control" style="border:none;box-shadow:none;font-size:12px;padding: 0px;height: 20px;">
    						<option value="0">-- Chọn chiến dịch SMS --</option>
    					</select>
    				</div>
    				<div class="col-xs-12">
    					<textarea class="form-control" name="MessageSMS" id="MessageSMS" placeholder="Nhập nội dung tin nhắn vào đây" onkeydown="sms_character(this)" rows="3"></textarea>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-xs-12">
    					<div class="alert hidden" id="alert"></div>
    				</div>
    			</div>
    			<div class="row">
    				<div class="col-xs-12">
    					<a class="btn btn-primary" onclick="send_sms(this)"><i class="fa fa-send"></i> Xác nhận gửi SMS</a>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<Script>
	var baseurl = $("#baselink_report").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}	

	function open_sms_box(){
		enablescrollsetup();
    	$(".over_lay .box_inner").css({'margin-top':'50px'});
    	$(".over_lay .box_inner .block1_inner h1").html("Nhắn tin từ hệ thống SMS");
    	$(".over_lay").removeClass('in');
    	$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function sms_character(ob){
		var data = $(ob).val();
		if(data.length>266){
			$("#alert").addClass("alert-danger").removeClass("hidden").html("Nội dung tin nhắn quá dài ! Số lượng ký tự không được quá 266 ký tự .");
		}else{
			var remain = 266-data.length;
			$("#alert").removeClass("hidden").removeClass("alert-danger").addClass("alert-success").html("Còn lại "+remain+" ký tự được sử dụng cho tin nhắn này.");
		}
	}

	function send_sms(ob){
		var phone = $("#PhoneSMS").val();
		if(phone.length<10){
			alert("Số điện thoại nhận tin không hợp lệ !");
			return false;
		}
		var message = $("#MessageSMS").val();
		if(message.length<5){
			alert("Nội dung tin nhắn quá ngắn");
			return false;
		}
		if(message.length>266){
			alert("Nội dung tin nhắn quá dài");
			return false;
		}
		$(ob).addClass("saving");
		$.post(baseurl+"manager_sms/send_sms",{phone:phone,message:message},function(data){
			if(data.error==0){
				alert("Đã gửi SMS thành công !");
				$(".over_lay").hide();
				disablescrollsetup();
				$("#PhoneSMS").val('');
			}else{
				alert("Có lỗi xảy ra trong quá trình gửi SMS .");
			}
			$(ob).removeClass("saving");
	    },"json");
	}
</Script>