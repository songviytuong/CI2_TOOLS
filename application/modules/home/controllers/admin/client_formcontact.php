<?php 
class Client_formcontact extends Admin_Controller { 
	public $user;
	public $classname="client_formcontact";
	public $limit = 15;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Contact form list | Client Interaction');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_contact")->row();
        $nav = $nav ? $nav->nav : 0;
		$object = $this->db->query("select * from ttp_contact order by Created DESC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/client_formcontact/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/client_formcontact/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/client_formcontact_home',$data);
		$this->template->render();
	}

	public function search(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		if(isset($_GET['keyword'])){
			$keyword = $this->lib->fill_data($_GET['keyword']);
			$str = "select * from ttp_contact";
			$str = $keyword != '' ? $str." where (Name like '%".urldecode($keyword)."%' or Email like '%".urldecode($keyword)."%')" : $str ;
			$this->template->add_title('Tìm kiếm dữ liệu');
			$data=array(
				'data'	=> $this->db->query($str)->result(),
				'nav'	=> '',
				'start'	=> 0,
				'base_link' =>	base_url().ADMINPATH.'/home/client_formcontact/',
			);
			$this->template->write_view('content','admin/client_formcontact_home',$data);
			$this->template->render();
			return;
		}
		redirect(ADMINPATH.'/home/client_formcontact/');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$this->db->query("delete from ttp_contact where ID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add new | Client Interaction');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/client_formcontact/'
		);
		$this->template->write_view('content','admin/client_formcontact_add',$data);
		$this->template->render();
	}

	public function addnew(){
		redirect(ADMINPATH.'/home/client_formcontact/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_contact where ID=$id")->row();
			if(!$result) return;
			$this->template->add_title('Edit Contact Message | Client Interaction');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/client_formcontact/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/client_formcontact_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		redirect(ADMINPATH.'/home/client_formcontact/');
	}

}
?>