<?php 
class Api_client extends Admin_Controller { 
 
 	public $user;
 	public $classname="api_client";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/api_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_system_client a,ttp_system_website b where a.WebsiteID=b.ID")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select a.*,b.Domain from ttp_system_client a,ttp_system_website b where a.WebsiteID=b.ID $limit_str")->result();
        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/api_client/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/api_client/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Website system | Report Tools');
        $this->template->write_view('content','admin/api_client_home',$data);
        $this->template->render();
    }
    
    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Add client system | Report Tools');
        $this->template->write_view('content','admin/api_client_add',array('base_link'=>base_url().ADMINPATH.'/report/api_client/'));
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Edit client system | Report Tools');
        $result = $this->db->query("select * from ttp_system_client where ID=$id")->row();
        if($result){
            $this->template->write_view('content','admin/api_client_edit',
                array(
                    'base_link' =>base_url().ADMINPATH.'/report/api_client/',
                    'data'      =>$result
                )
            );
            $this->template->render();
        }
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_system_client where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Published = isset($_POST['Published']) ? (int)$_POST['Published'] : 0 ;
        $WebsiteID = isset($_POST['WebsiteID']) ? (int)$_POST['WebsiteID'] : 0 ;
        $ClientID = isset($_POST['ClientID']) ? trim(mysql_real_escape_string($_POST['ClientID'])) : '' ;
        $ClientKey = isset($_POST['ClientKey']) ? trim(mysql_real_escape_string($_POST['ClientKey'])) : '' ;
        $ClientPassword = isset($_POST['ClientPassword']) ? trim(mysql_real_escape_string($_POST['ClientPassword'])) : '' ;
        if($WebsiteID!='' && $ClientID!='' && $ClientKey!='' && $ClientPassword!=''){
            $data = array(
                'WebsiteID'=>$WebsiteID,
                'Published'=>$Published,
                'ClientID'    =>$ClientID,
                'ClientKey'=>$ClientKey,
                'ClientPassword'=>$ClientPassword,
                'Created'=>date('Y-m-d H:i:s'),
                'LastEdited'=>date('Y-m-d H:i:s'),
                'Token' => sha1(time().$ClientPassword)
            );
            $this->db->insert('ttp_system_client',$data);
            $id = $this->db->insert_id();
        }
        redirect(base_url().ADMINPATH.'/report/api_client/');
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? trim(mysql_real_escape_string($_POST['ID'])) : 0 ;
        $Published = isset($_POST['Published']) ? (int)$_POST['Published'] : 0 ;
        $WebsiteID = isset($_POST['WebsiteID']) ? (int)$_POST['WebsiteID'] : 0 ;
        $ClientID = isset($_POST['ClientID']) ? trim(mysql_real_escape_string($_POST['ClientID'])) : '' ;
        $ClientKey = isset($_POST['ClientKey']) ? trim(mysql_real_escape_string($_POST['ClientKey'])) : '' ;
        $ClientPassword = isset($_POST['ClientPassword']) ? trim(mysql_real_escape_string($_POST['ClientPassword'])) : '' ;
        if($WebsiteID!='' && $ClientID!='' && $ClientKey!='' && $ClientPassword!=''){
            $result = $this->db->query("select * from ttp_system_client where ID=$ID")->row();
            if($result){
                $data = array(
                    'WebsiteID'=>$WebsiteID,
                    'Published'=>$Published,
                    'ClientID'    =>$ClientID,
                    'ClientKey'=>$ClientKey,
                    'ClientPassword'=>$ClientPassword,
                    'LastEdited'=>date('Y-m-d H:i:s'),
                    'Token' => sha1(time().$ClientPassword)
                );
                $this->db->where("ID",$ID);
                $this->db->update('ttp_system_client',$data);
            }
        }
        redirect(base_url().ADMINPATH.'/report/api_client/');
    }
}
?>