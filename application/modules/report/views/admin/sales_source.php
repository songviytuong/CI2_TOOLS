<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>THỐNG KÊ THEO NGUỒN / KÊNH ĐẶT HÀNG</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="quick_view">
        <div class="row">
            <div class="col-xs-6"></div>
            <?php 
            $viewtype = isset($_GET['type']) ? 3 : 0 ;
            ?>
            <div class="col-xs-6 text-right" style='margin-bottom:10px'>
                Chọn dữ liệu theo kênh : 
                <select onchange="changetype(this)" style='margin-left:5px'>
                    <option value='0' <?php echo $viewtype==0 ? 'selected="selected"' : '' ; ?>>Online</option>
                    <option value='3' <?php echo $viewtype==3 ? 'selected="selected"' : '' ; ?>>Gốm sứ</option>
                </select>
            </div>
        </div>
        <div class="block1">
            <canvas id="canvas_pie"></canvas>
        </div>
        <div class="block2">
            <?php 
            $arr = array();
            $arr_total = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
            $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#2824FF","#cccccc");
            if(count($data)>0){
                foreach($data as $row){
                    $arr_total['SLDH'] = $arr_total['SLDH']+$row->SLDH;
                    $arr_total['SLSP'] = $arr_total['SLSP']+$row->SoluongSP;
                    $arr_total['Total'] = $arr_total['Total']+$row->Total;
                    $arr[$row->ID]['Title'] = $row->Title;
                    $arr[$row->ID]['SLDH'] = $row->SLDH;
                    $arr[$row->ID]['SLSP'] = $row->SoluongSP;
                    $arr[$row->ID]['Total'] = $row->Total;
                }
            }
            $arr_chart = array();
            if(count($arr)>0){
                echo "<table>
                        <tr>
                            <th>Nguồn đặt hàng</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                foreach($arr as $key=>$value){
                    $array_color[$key] = isset($array_color[$key]) ? $array_color[$key] : '#eee' ;
                    $percent = round($value['Total']/($arr_total['Total']/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$key]."'></span>".$value['Title']."</td>";
                    echo "<td>".number_format($value['SLDH'])."</td>";
                    echo "<td>".number_format($value['SLSP'])."</td>";
                    echo "<td>".number_format($value['Total'])."</td>";
                    echo "<td>".$percent."%</td>";
                    echo "</tr>";
                    $arr_chart[] = "{
                                        value: ".$value['SLDH'].",
                                        color: '".$array_color[$key]."',
                                        highlight: '".$array_color[$key]."',
                                        label: 'Số lượng ".$value['Title']."'
                                    }";
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }else{
                echo "<p>Không có dữ liệu trong khoản thời gian này .</p>";
            }
            ?>
        </div>
    </div>
    <div style="width:100%;clear:both;padding:10px 0px;"></div>
    <div class="quick_view">
        <div class="block1">
            <canvas id="canvas_pie1"></canvas>
        </div>
        <div class="block2">
            <?php 
            $arr = array();
            $arr_total = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
            $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#2824FF","#cccccc");
            if(count($channel)>0){
                foreach($channel as $row){
                    $arr_total['SLDH'] = $arr_total['SLDH']+1;
                    $arr_total['SLSP'] = $arr_total['SLSP']+$row->SoluongSP;
                    $arr_total['Total'] = $arr_total['Total']+$row->Total;
                    if(isset($arr[$row->ID])){
                        $arr[$row->ID]['SLDH'] += 1;
                        $arr[$row->ID]['SLSP'] += $row->SoluongSP;
                        $arr[$row->ID]['Total'] += $row->Total;
                        if(isset($arr[$row->ID]['Source'][$row->SourceID])){
                            $arr[$row->ID]['Source'][$row->SourceID]['SLDH'] += 1;
                            $arr[$row->ID]['Source'][$row->SourceID]['SLSP'] += $row->SoluongSP;
                            $arr[$row->ID]['Source'][$row->SourceID]['Total'] += $row->Total;
                        }else{
                            $arr[$row->ID]['Source'][$row->SourceID]['Title'] = $row->SourceTitle;
                            $arr[$row->ID]['Source'][$row->SourceID]['SLDH'] = 1;
                            $arr[$row->ID]['Source'][$row->SourceID]['SLSP'] = $row->SoluongSP;
                            $arr[$row->ID]['Source'][$row->SourceID]['Total'] = $row->Total;
                        }
                    }else{
                        $arr[$row->ID]['Title'] = $row->Title;
                        $arr[$row->ID]['SLDH'] = 1;
                        $arr[$row->ID]['SLSP'] = $row->SoluongSP;
                        $arr[$row->ID]['Total'] = $row->Total;
                        $arr[$row->ID]['Source'][$row->SourceID]['Title'] = $row->SourceTitle;
                        $arr[$row->ID]['Source'][$row->SourceID]['SLDH'] = 1;
                        $arr[$row->ID]['Source'][$row->SourceID]['SLSP'] = $row->SoluongSP;
                        $arr[$row->ID]['Source'][$row->SourceID]['Total'] = $row->Total;
                    }
                }
            }
            $arr_chart1 = array();
            if(count($arr)>0){
                echo "<table>
                        <tr>
                            <th>Kênh đặt hàng</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                foreach($arr as $key=>$value){
                    $percent = $arr_total['Total']==0 ? 0 : round($value['Total']/($arr_total['Total']/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$key]."'></span>".$value['Title']."<i onclick='showdetails(this,$key)' status='close' class='fa fa-angle-double-down' aria-hidden='true'></i></td>";
                    echo "<td>".number_format($value['SLDH'])."</td>";
                    echo "<td>".number_format($value['SLSP'])."</td>";
                    echo "<td>".number_format($value['Total'])."</td>";
                    echo "<td>".$percent."%</td>";
                    echo "</tr>";
                    if(isset($value['Source']) && count($value['Source']>0)){
                        foreach($value['Source'] as $itemkey=>$item){
                            $array_color[$itemkey] = isset($array_color[$itemkey]) ? $array_color[$itemkey] : '#eee' ;
                            $percent = $value['Total']==0 ? 0 : round($item['Total']/($value['Total']/100),1);
                            echo "<tr class='trsource trsource$key' style='display:none'>";
                            echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-left:20px;margin-right: 8px;background:".$array_color[$itemkey]."'></span>".$item['Title']."</td>";
                            echo "<td>".number_format($item['SLDH'])."</td>";
                            echo "<td>".number_format($item['SLSP'])."</td>";
                            echo "<td>".number_format($item['Total'])."</td>";
                            echo "<td>".$percent."%</td>";
                            echo "</tr>";
                        }
                    }
                    $arr_chart1[] = "{
                                        value: ".$value['SLDH'].",
                                        color: '".$array_color[$key]."',
                                        highlight: '".$array_color[$key]."',
                                        label: 'Số lượng ".$value['Title']."'
                                    }";
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }else{
                echo "<p>Không có dữ liệu trong khoản thời gian này .</p>";
            }
            ?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var sharePiePolorDoughnutData = [
        <?php 
        echo implode(',', $arr_chart);
        ?>
    ];

    var sharePiePolorDoughnutData1 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];

    $(document).ready(function () {
        window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_pie1").getContext("2d")).Pie(sharePiePolorDoughnutData1, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });

    function showdetails(ob,ID){
        var status_tr = $(ob).attr('status');
        if(status_tr=='close'){
            $(".trsource"+ID).show();
            $(ob).attr('status','open');
            $(ob).removeClass("fa-angle-double-down");
            $(ob).addClass("fa-angle-double-up");
        }else{
            $(".trsource"+ID).hide();
            $(ob).attr('status','close');
            $(ob).removeClass("fa-angle-double-up");
            $(ob).addClass("fa-angle-double-down");
        }
    }

    function changetype(ob){
        var datatype = $(ob).val();
        if(datatype==0){
            window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/report_source' ?>";
        }else{
            window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/report_source?type=' ?>"+datatype;
        }
    }
</script>
