<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Danh sách yêu cầu soạn hàng</h1>
	    </div>
	    <div class="block2">
	    	<div class="btn-group" style="margin-left:15px;">
	        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Kho soạn hàng</a>
	            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	            <ul class="dropdown-menu">
	            	<li <?php echo $WarehouseID==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,0)'>Tất cả kho hàng</a></li>
					<?php 
					if(count($WarehouseList)>0){
						foreach($WarehouseList as $row){
							$style = $row->ID==$WarehouseID ? "style='background:#c3e2ff;'" : '' ;
							echo "<li $style><a onclick='changefillter(this,$row->ID)'>$row->MaKho</a></li>";
						}
					}
					?>
		        </ul>
	        </div>
	    </div>
    </div>
	<table class="table table-bordered">
		<tr style='background:#f5f5f5;'>
			<th>STT</th>
			<th>MÃ ĐƠN HÀNG</th>
			<th>TÊN SẢN PHẨM</th>
			<th>DV</th>
			<th>SỐ LƯỢNG</th>
			<th>KHO LẤY HÀNG</th>
    		<th class='text-center' style='width: 125px;'>XÁC NHẬN</th>
		</tr>
		<?php 
		if(count($data)>0){
			$i=0;
			$temp='';
			$group_order = array();
			foreach($data as $row){
				if(isset($group_order[$row->ID])){
					$group_order[$row->ID] = $group_order[$row->ID]+1;
				}else{
					$group_order[$row->ID] = 1;
				}
			}
			foreach($data as $row){
				$rowspan = $group_order[$row->ID];
				$group = $row->MaDH!=$temp ? "<td rowspan='$rowspan' style='vertical-align:middle'>$row->MaDH</td>" : "" ;
				$i = $row->MaDH!=$temp ? $i+1 : $i ;
				$stt = $row->MaDH!=$temp ? "<td rowspan='$rowspan' style='vertical-align:middle'>$i</td>" : "" ;
				$temp = $row->MaDH;
				echo "<tr>
					$stt
					$group
					<td>$row->Title</td>
					<td>$row->Donvi</td>
					<td class='text-right'>".number_format($row->Amount,2)."</td>
					<td>$row->MaKho</td>
					<td class='text-center' style='vertical-align:middle'><a class='btn-sm btn-danger' data-state='0' onclick='readytopick(this,$row->IDDetails)'><i class='fa fa-check-square-o' aria-hidden='true'></i> Đã soạn đủ</a></td>
				</tr>";
			}
		}else{
			echo '<tr><td colspan="7">Không tìm thấy dữ liệu theo yêu cầu .</td></tr>';
		}
		?>
	</table>
</div>

<script>
	var link = "<?php echo $base_link ?>";

	function changefillter(ob,WarehouseID){
		window.location = link+"request_picking?warehouse="+WarehouseID;
	}

	function readytopick(ob,detailis){
		var data = $(ob).attr('data-state');
		if(data==0){
			$(ob).addClass('saving');
			$.ajax({
	            url: link+"readytopick",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+detailis,
	            success: function(result){
	            	console.log(result);
	                $(ob).removeClass('saving');
	                if(result=='true'){
	                	$(ob).removeClass("btn-danger");
	                    $(ob).addClass("text-success");
	                    $(ob).html("<i class='fa fa-check-square-o' aria-hidden='true'></i> Đã xác nhận");
	                }else{
	                    alert("Không thực thi được yêu cầu .");
	                }
	            },error:function(result){
	                console.log(result.responseText);
	            }
	        });
        }
	}

</script>