<?php

class Assign_model extends CI_Model {

    var $tablename = 'auc_assign';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function getOwners($params = array()) {
        $this->db->select('*');
        if (!empty($params['oid'])) {
            $this->db->where('Owner', $params['oid']);
        }
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function getStatus($pid) {
        $this->db->select('Status');
        if (!empty($pid)) {
            $this->db->where('ProductID', $pid);
        }
        $result = $this->db->get($this->tablename)->row();
        return $result;
    }

    function getMaxCreated($pid) {
        $this->db->select('max(ID) as ID');
        $this->db->where('ProductsID', $pid);
        $this->db->where('Notes', 'Wait Approval');
        $result = $this->db->get('ttp_report_products_history')->row();
        return $result;
    }

    function getAllAssign($params = array()) {
        $this->db->select('ProductID,Owner,Status');
        if (!empty($params['oid'])) {
            $this->db->where('Owner', $params['oid']);
        }
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function getAssignByProductID($params = array()) {
        $this->db->select('ProductID,Owner,Status');
        if (!empty($params['pid'])) {
            $this->db->where('ProductID', $params['pid']);
        }
        $result = $this->db->get($this->tablename)->row();
        return $result;
    }

    function getCounterAssign($uid = 5, $status = 0) {
        if ($uid) {
            $query = $this->db->query('SELECT * FROM ' . $this->tablename . ' WHERE Owner = ' . $uid . (($status != 0) ? ' and Status=' . $status : ''));
        } else {
            $query = $this->db->query('SELECT * FROM ' . $this->tablename);
        }
        $result = $query->num_rows();
        return $result;
    }

    function getCounterAssignEdited($uid = 5) {
        $query = $this->db->query('SELECT * FROM ' . $this->tablename . ' WHERE Owner = ' . $uid.' and LastEdited IS NOT NULL');
        $result = $query->num_rows();
        return $result;
    }

    function isExist($pid) {
        $query = $this->db->query('SELECT ID FROM ' . $this->tablename . ' WHERE ProductID=' . $pid);
        $result = $query->num_rows();
        return $result;
    }

    function getDataAssign() {
//        $this->db->cache_on();
        $this->db->select('ProductID');
//        $this->db->where_in('Status', array(2,3));
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function updateStatus($pid, $status) {
        $update = array(
            'Status' => $status,
            'Confirmed' => date('Y-m-d H:i:s')
        );
        $this->db->where('ProductID', $pid);
        $this->db->update($this->tablename, $update);
        return true;
    }

    function lastUpdate($pid) {
        $this->db->select('max(Confirmed),Status');
        $this->db->where('ProductID', $pid);
        $result = $this->db->get($this->tablename)->row();
        return $result;
    }

}
