<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa team</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">
							<label class="control-label col-xs-4">Trạng thái hoạt động</label>
							<div class='col-xs-8'>
								<input type='radio' name="Published" value="1" <?php echo $data->Published==1 ? 'checked="true"' : '' ; ?> /> Enable 
								<input type='radio' name="Published" value="0" <?php echo $data->Published==0 ? 'checked="true"' : '' ; ?> /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">	
							<label class="control-label col-xs-4">Tên team</label>
							<div class='col-xs-8'>
								<input type='text' class="form-control" name="Title" value="<?php echo $data->Title ?>" required />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">	
							<label class="control-label col-xs-4">Thuộc phòng ban</label>
							<div class='col-xs-8'>
								<select name="Department" class="form-control">
								<?php 
								$depart = $this->db->query("select * from ttp_report_targets_department")->result();
								if(count($depart)>0){
									foreach($depart as $row){
										$selected = $row->ID==$data->Department ? "selected='selected'" : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<?php 
				$json = json_decode($data->Data,true);
				?>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">	
							<label class="control-label col-xs-4">Team leader</label>
							<div class='col-xs-8'>
								<select name="UserID" class="form-control">
								<?php 
								$depart = $this->db->query("select ID,UserName from ttp_user where UserType=1")->result();
								if(count($depart)>0){
									foreach($depart as $row){
										$selected= $row->ID==$data->UserID ? "selected='selected'" : '' ;
										echo "<option value='$row->ID' $selected>$row->UserName</option>";
									}
								}
								?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">	
							<label class="control-label col-xs-12" style="border-bottom: 1px solid #ccc;padding: 5px 15px;">Chọn nhân viên chưa có team vào team này</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-12">
							<?php 
								if(count($depart)>0){
									foreach($depart as $row){
										$checked = in_array($row->ID,$json) ? "checked='checked'" : '' ;
										echo "<li style='float:left;width:200px;margin-left:20px;display:block'><input type='checkbox' name='userlist[]' value='$row->ID' $checked /> <span>$row->UserName</span></li>";
									}
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>