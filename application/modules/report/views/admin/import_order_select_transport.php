<div class="containner">
	<?php 
    $haft = isset($_GET['haft']) ? (int)$_GET['haft'] : 0 ;
    ?>
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Điều phối đơn hàng</h1>
	    </div>
        <?php 
        $arr = array(9=>"Chờ điều phối xử lý",7=>"Đã chọn vận chuyển",0=>"Giao hàng thành công",1=>"Giao không thành công");
        $timehaft = array(0=>"Toàn thời gian",1=>"Chỉ giao buổi sáng",2=>"Chỉ giao buổi chiều");
        ?>
	    <div class="block2">
            <div class="btn-group" style="margin-right:15px;">
	        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> <?php echo isset($arr[$type]) ? $arr[$type] : "Trạng thái đơn hàng" ; ?></a>
	            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	            <ul class="dropdown-menu">
	            	<li <?php echo $type==9 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,9,<?php echo $haft ?>)'>Chờ điều phối xử lý</a></li>
					<li <?php echo $type==7 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,7)'>Đã chọn vận chuyển</a></li>
					<li <?php echo $type==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,0,<?php echo $haft ?>)'>Đã giao thành công</a></li>
					<li <?php echo $type==1 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,1,<?php echo $haft ?>)'>Giao không thành công</a></li>
		        </ul>
	        </div>
            <div class="btn-group" style="margin-right:15px;">
	        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> <?php echo isset($timehaft[$haft]) ? $timehaft[$haft] : "Thời gian giao hàng" ; ?></a>
	            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	            <ul class="dropdown-menu">
	            	<li <?php echo $haft==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,<?php echo $type ?>,0)'>Tất cả</a></li>
					<li <?php echo $haft==1 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,<?php echo $type ?>,1)'>Chỉ buổi sáng</a></li>
					<li <?php echo $haft==2 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,<?php echo $type ?>,2)'>Chỉ buổi chiều</a></li>
		        </ul>
	        </div>
	        <a class='btn btn-danger' onclick='choosetransport(this)'><i class="fa fa-truck" aria-hidden="true"></i> Chọn đơn vị vận chuyển</a>
	    </div>
    </div>
    <table class="table table-bordered">
    	<tr style="background:#f5f5f5">
    		<th>STT</th>
    		<th>MÃ ĐƠN HÀNG</th>
    		<th class="text-center">KHU VỰC GIAO HÀNG</th>
    		<th>SẢN PHẨM</th>
    		<th>ĐV</th>
    		<th>SL</th>
            <th class="text-center">ĐỊA ĐIỂM LẤY HÀNG</th>
    		<th class='text-center'>TRẠNG THÁI</th>
    	</tr>
    <?php 
    if(count($data)>0){
    	$arr_warehouse = array();
    	$warhouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
    	if(count($warhouse)>0){
    		foreach($warhouse as $row){
    			$arr_warehouse[$row->ID] = $row->MaKho;
    		}
    	}

    	$arr_branch = array();
    	$branch = $this->db->query("select a.ID,a.Title as Branch,b.Title from ttp_report_branch a,ttp_report_production b where a.SupplierID=b.ID")->result();
    	if(count($branch)>0){
    		foreach($branch as $row){
    			$arr_branch[$row->ID] = "<small>".$row->Title."</small><br>".$row->Branch;
    		}
    	}

    	$group_order = array();
		foreach($data as $row){
			if(isset($group_order[$row->ID])){
				$group_order[$row->ID] = $group_order[$row->ID]+1;
			}else{
				$group_order[$row->ID] = 1;
			}
		}

    	$temp=1;
    	$i=0;
        $total = 0;
    	foreach($data as $row){
    		$rowspan = $group_order[$row->ID];
		    $day = date('d/m/Y',strtotime($row->DeliveryTime));
            $haftday = date('H',strtotime($row->DeliveryTime));
            $haftstate = $haftday<12 ? "<b style='background: #a2cd3a;color: #FFF;padding: 2px 5px;border-radius: 20px;'>Sáng</b>" : "<b style='background: #f96868;color: #FFF;padding: 2px 5px;border-radius: 20px;'>Chiều</b>" ;
            $truehaft = 0;
            if($haft==1){
                if($haftday<=12){
                    $truehaft=1;
                }
            }
            if($haft==2){
                if($haftday>12){
                    $truehaft=1;
                }
            }
            if($haft==0){
                $truehaft = 1;
            }
            $group = $temp==1 ? "<td rowspan='$rowspan' style='vertical-align:middle'>$row->MaDH <br> <small style='margin-top:3px;color:#888;'>".$haftstate.' '.$day."</small></td>" : "" ;
            $i = $temp==1 ? $i+1 : $i ;
            $stt = $temp==1 ? "<td rowspan='$rowspan' style='vertical-align:middle'>$i</td>" : "" ;
            $ship = $temp==1 ? "<td rowspan='$rowspan' class='text-center' style='width: 160px;vertical-align: middle;'>$row->District - $row->MisaCode</td>" : "" ;
            $checkinput = $temp==1 ? "<td rowspan='$rowspan' class='text-center' style='vertical-align: middle;'><input type='checkbox' class='checkedorder' data-order='$row->ID' /></td>" : "";
            if($temp<$rowspan){
                $temp++;
            }else{
                $temp=1;
            }
            $address = $row->BranchID>0 ? $arr_branch[$row->BranchID] : "" ;
            $address = $row->WarehouseID>0 ? $arr_warehouse[$row->WarehouseID] : $address ;
            if($truehaft==1){
		        echo "<tr>
		        		$stt
		        		$group
	    				$ship
	    				<td>$row->Title</td>
	    				<td style='width:60px'>$row->Donvi</td>
	    				<td class='text-right' style='width: 100px;'>".number_format($row->Amount,2)."</td>
	    				<td style='vertical-align: middle;'>$address</td>
	    				$checkinput
	    			</tr>";
    		}
    	}
    }else{
    	echo "<tr><td colspan='8'>Không tìm thấy dữ liệu theo yêu cầu</td></tr>";
    }
    ?>
	</table>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<script>
	var link = "<?php echo $base_link ?>";

	function changefillter(ob,type,haft){
		window.location=link+"logistic?type="+type+"&haft="+haft;
	}

	function choosetransport(ob){
        var SendInfo = [];
        var state=0;
		$(".checkedorder").each(function(){
			if(this.checked==true){
                $(ob).addClass('saving');
                var OrderID = $(this).attr('data-order');
                var row = OrderID;
                SendInfo.push(row);
                state=1;
            }
		});
        if(state==1){
            $.ajax({
                url: link+"choosetransport",
                dataType: "html",
                type: "POST",
                data: "data="+JSON.stringify(SendInfo),
                success: function(result){
                    $(ob).removeClass('saving');
                    enablescrollsetup();
                    $(".over_lay .box_inner").css({'margin-top':'50px'});
                	$(".over_lay .box_inner .block1_inner h1").html("Điều phối đơn hàng cho đơn vị vận chuyển");
                	$(".over_lay .box_inner .block2_inner").html(result);
                	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
        			$(".over_lay").addClass('in');
                },error:function(result){
                    console.log(result.responseText);
                }
            });
        }else{
            alert("Không đủ dữ liệu hợp lệ để gửi.");
        }
	}

    function checkfull(ob){
        if(ob.checked===true){
            $(".checkedorder").each(function(){
                $(this).prop("checked",true);
            });
        }else{
            $(".checkedorder").each(function(){
                $(this).prop("checked",false);
            });
        }
    }

    var sitebar_open = "true";
    $("#closesitebar").click(function(){
        sitebar_open = "false";
        $(".sitebar").addClass("closesitebar");
        $(this).addClass("closesitebar");
        $("#opensitebar").addClass("opensitebar");
        $(".containner").addClass("opensitebar");
        $(".copyright").addClass("opensitebar");
        $(".warning_message").addClass("opensitebar");
    });

    function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});
</script>

