<div class="containner">
	<div class="manager">
		<?php 
		if(isset($_GET['error'])){
			switch ($_GET['error']) {
				case 0:
					$message = "Mật khẩu nhập 2 lần không trùng khớp !";
					break;
				case 1:
					$message = "Số điện thoại đã có người sử dụng vui lòng chọn số điện thoại khác !";
					break;
				case 2:
					$message = "Tên đăng nhập đã có người sử dụng vui lòng chọn số điện thoại khác !";
					break;
				default:
					$message = "";
					break;
			}
			if($message!=''){
			?>
			<div class="alert alert-dismissible alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>Cảnh báo !</strong> <?php echo $message ?>
			</div>
			<?php 
			}
		}
		?>
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type='hidden' name="ID" value="<?php echo $data->ID ?>" />
			<input type='hidden' name="UserID" value="<?php echo $data->UserID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa thông tin tài khoản</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Lưu thông tin</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Loại khách hàng</label>
							<div class='col-xs-7'>
								<select class="form-control" name="Type">
									<option value='0' <?php echo $data->Type==0 ? 'selected="selected"' : '' ; ?>>Online</option>
									<option value='1' <?php echo $data->Type==1 ? 'selected="selected"' : '' ; ?>>MT</option>	
									<option value='2' <?php echo $data->Type==2 ? 'selected="selected"' : '' ; ?>>GT</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-7'>
								<select class="form-control" name="Published">
									<option value='1' <?php echo $data->Published==1 ? 'selected="selected"' : '' ; ?>>Enable</option>
									<option value='0' <?php echo $data->Published==0 ? 'selected="selected"' : '' ; ?>>Disable</option>	
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Hệ thống website</label>
							<div class='col-xs-7'>
								<select class="form-control" name="WebsiteID">
									<?php 
									$website = $this->db->query("select * from ttp_system_website")->result();
									if(count($website)>0){
										foreach($website as $row){
											$selected = $row->ID==$data->WebsiteID ? 'selected="selected"' : '' ;
											echo "<option value='$row->ID' $selected>$row->Domain</option>";
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Tên đăng nhập</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control" name="UserName" value="<?php echo $data->UserName ?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Mật khẩu đăng nhập</label>
							<div class='col-xs-7'>
								<input type="password" class="form-control" name="Password" id="Password" placeholder="Nhập mật khẩu mới ..." />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Nhập lại mật khẩu</label>
							<div class='col-xs-7'>
								<input type="password" class="form-control" name="RePassword" onchange="checkpassword(this)" placeholder="Nhập lại mật khẩu mới ..." id="RePassword" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h3>Thông tin liên hệ & giao hàng</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Họ tên</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control required" name="Name" value="<?php echo $data->Name ?>" required />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Số điện thoại 1</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control required" name="Phone1" value="<?php echo $data->Phone1 ?>" required />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Số điện thoại 2</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control" name="Phone2" value="<?php echo $data->Phone2 ?>" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Email</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control" name="Email" value="<?php echo $data->Email ?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Ngày sinh</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control" name="Birthday" id="Birthday" value="<?php echo $data->Birthday ?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Tuổi</label>
							<div class='col-xs-7'>
								<input type="number" class="form-control" name="Age" value="<?php echo $data->Age ?>" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Giới tính</label>
							<div class='col-xs-7'>
								<select name="Sex" class="form-control">
									<option value="0" <?php echo $data->Sex==0 ? 'selected="selected"' : '' ; ?>>Nữ</option>
									<option value="1" <?php echo $data->Sex==1 ? 'selected="selected"' : '' ; ?>>Nam</option>
									<option value="2" <?php echo $data->Sex==2 ? 'selected="selected"' : '' ; ?>>Khác</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Nghề nghiệp hiện tại</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control" name="Job" value="<?php echo $data->Job ?>" />
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Công ty</label>
							<div class='col-xs-7'>
								<input type="text" class="form-control" name="Company" value="<?php echo $data->Company ?>" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Khu vực</label>
							<div class='col-xs-7'>
								<select name="AreaID" class="form-control" id='Khuvuc'>
									<option value='0'>-- Chọn khu vực --</option>
								<?php 
								$area = $this->db->query("select * from ttp_report_area")->result();
								if(count($area)>0){
									foreach($area as $row){
										$selected = $row->ID==$data->AreaID ? 'selected="selected"' : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Tỉnh thành</label>
							<div class='col-xs-7'>
								<select name="CityID" class="form-control" id="Tinhthanh">
									<?php 
									$city = $this->db->query("select * from ttp_report_city where ID=$data->CityID")->row();
									echo $city ? "<option value='$city->ID'>$city->Title</option>" : "<option value='0'>-- Chọn tỉnh thành --</option>";
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Quận huyện</label>
							<div class='col-xs-7'>
								<select name="DistrictID" class="form-control" id="Quanhuyen">
									<?php 
									$district = $this->db->query("select * from ttp_report_district where ID=$data->DistrictID")->row();
									echo $city ? "<option value='$district->ID'>$district->Title</option>" : "<option value='0'>-- Chọn tỉnh thành --</option>";
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Địa chỉ khách hàng</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<div class='col-xs-12'>
								<input type="text" name="Address" class="form-control" value="<?php echo $data->Address ?>" />
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="" class="col-xs-5 control-label">Địa chỉ giao hàng</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<div class='col-xs-12'>
								<input type="text" name="AddressOrder" class="form-control" value="<?php echo $data->AddressOrder ?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var link = "<?php echo base_url().ADMINPATH.'/report/' ?>";
	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_city_by_area",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_district_by_city",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            }
	        });
		}
	});

	function checkpassword(ob){
		var repassword = $(ob).val();
		var password = $("#Password").val();
		if(password!=repassword){
			alert("Mật khẩu 2 lần nhập không trùng khớp !");
			$(ob).css({"border":"1px solid #F96868"});
			$(ob).val('');
		}else{
			$(ob).css({"border":"1px solid #ccc"});
		}
	}

    $(document).ready(function () {
        $('#Birthday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD'
        });
    });
</script>

<style>
	.daterangepicker{width: auto;}
</style>