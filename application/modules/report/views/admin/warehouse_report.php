<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$numday = $this->lib->get_nume_day($startday,$stopday);

$month 			= array();
$dayofmonth		= array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";

$arr_production = array();
$arr_import = array();
$arr_export = array();

$products = array();
if(count($import)>0){
	foreach($import as $row){
        $date_finance = date('dmY',strtotime($row->NgayNK));
        if(isset($arr_import[$date_finance]['Amount'])){
            $arr_import[$date_finance]['Amount'] += $row->Amount;
        }else{
            $arr_import[$date_finance]['Amount'] = $row->Amount;
        }
		if(isset($products[$row->ProductsID])){
            $products[$row->ProductsID]['TotalImport'] += $row->Amount;
			if(isset($products[$row->ProductsID]['Production'][$row->ProductionID])){
				$products[$row->ProductsID]['Production'][$row->ProductionID]['Amount'] += $row->Amount;
			}else{
				$products[$row->ProductsID]['Production'][$row->ProductionID] = array(
					'Name'		=> $row->ProductionName,
					'Amount'	=> $row->Amount,
                    'Price'     => 0
				);			
			}
		}else{
			$products[$row->ProductsID] = array(
				'Name'		=> $row->Title,
				'Production'=> array(
					$row->ProductionID => array(
						'Name'		=> $row->ProductionName,
						'Amount'	=> $row->Amount,
                        'Price'     => 0
					)
				),
                'TotalImport' => $row->Amount,
                'TotalExport' => 0,
                'Inventory'   => 0
			);
		}
	}
}

if(count($export)>0){
    foreach($export as $row){
        $date_finance = date('dmY',strtotime($row->Ngaydathang));
        if(isset($arr_export[$date_finance]['Amount'])){
            $arr_export[$date_finance]['Amount'] += $row->Amount;
        }else{
            $arr_export[$date_finance]['Amount'] = $row->Amount;
        }
        if(isset($products[$row->ProductsID])){
            if(isset($products[$row->ProductsID]['TotalExport'])){
                $products[$row->ProductsID]['TotalExport'] += $row->Amount;
            }else{
                $products[$row->ProductsID]['TotalExport'] = $row->Amount;
            }
        }else{
            $products[$row->ProductsID] = array(
                'Name'          => $row->Title,
                'Production'    => array(),
                'TotalImport'   => 0,
                'TotalExport'   => $row->Amount,
                'Inventory'     => 0
            );
        }
    }
}

if(count($inventory)>0){
    foreach ($inventory as $row) {
        if(isset($products[$row->ProductsID])){
            if(isset($products[$row->ProductsID]['Inventory'])){
                $products[$row->ProductsID]['Inventory'] += $row->OnHand;
            }else{
                $products[$row->ProductsID]['Inventory'] = $row->OnHand;
            }
        }
    }
}

if(count($purchase)>0){
    foreach ($purchase as $row) {
        if(isset($products[$row->ProductsID]['Production'][$row->ProductionID]['Price'])){
            $products[$row->ProductsID]['Production'][$row->ProductionID]['Price'] += $row->TotalVND;
        }
        $date_finance = date('dmY',strtotime($row->DatePO));
        if(isset($arr_import[$date_finance]['Price'])){
            $arr_import[$date_finance]['Price'] += $row->TotalVND;
        }else{
            $arr_import[$date_finance]['Price'] = $row->TotalVND;
        }
    }    
}

?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>Báo cáo xuất nhập tồn</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
	</div>
	<div class="import_orderlist">
        <div class="row">
        	<div class="col-xs-12">
        		<div class="row">
                    <div class="col-xs-7">
                        <div id="chart_div"></div>
                    </div>
                    <div class="col-xs-5">
                        <div id="chart_div1"></div>
                    </div>
                </div>
        		<div>
				  	<table class="table table-bordered" style="margin-top:15px;">
		    			<tr>
		    				<th style="border-top:none;">STT</th>
		    				<th style="border-top:none;width:300px;">Tên sản phẩm</th>
		    				<th style="border-top:none;">Nhà cung cấp</th>
		    				<th style="border-top:none;">Số lượng</th>
		    				<th style="border-top:none;">Giá trị nhập</th>
		    				<th style="border-top:none;">Tổng nhập</th>
		    				<th style="border-top:none;">Tổng xuất</th>
		    				<th style="border-top:none;">Còn tồn</th>
		    			</tr>
		    			<?php 
		    			if(count($products)>0){
		    				$i=1;
		    				foreach ($products as $key=>$value) {
		    					$rowspan = count($value['Production']);
		    					echo "<tr>";
		    					echo "<td rowspan=".$rowspan.">$i</td>";
		    					echo "<td rowspan=".$rowspan.">".$value['Name']." - $key</td>";
                                if(count($value['Production'])>0){
				    				$j=1;
                                    $item_rowspan = count($value['Production']);
                                    foreach($value['Production'] as $keyitem => $valueitem){
                                        if(!isset($arr_production[$keyitem])){
                                            $arr_production[$keyitem] = array('Name'=>$valueitem['Name'],'Price'=>$valueitem['Price']);
                                        }else{
                                            $arr_production[$keyitem]['Price'] += $valueitem['Price'];
                                        }
                                        if($j==1){
                                            echo "
				    							<td>".$valueitem['Name']."</td>
				    							<td class='text-right'>".$valueitem['Amount']."</td>
				    							<td class='text-right'>".number_format($valueitem['Price'])."</td>
				    							<td class='text-right' rowspan='".$item_rowspan."'>".number_format($value['TotalImport'],3)."</td>
				    							<td class='text-right' rowspan='".$item_rowspan."'>".number_format($value['TotalExport'],3)."</td>
				    							<td class='text-right' rowspan='".$item_rowspan."'>".number_format($value['Inventory'],3)."</td>";
                                        }else{
                                            echo "<tr>
                                                <td>".$valueitem['Name']."</td>
                                                <td class='text-right'>".$valueitem['Amount']."</td>
                                                <td class='text-right'>".number_format($valueitem['Price'])."</td>
                                            </tr>";
                                        }
                                        $j++;
				    				}
				    			}else{
                                    echo "<td>--</td>";
                                    echo "<td>--</td>";
                                    echo "<td>--</td>";
                                    echo "<td class='text-right'>".number_format($value['TotalImport'],3)."</td>
                                                <td class='text-right'>".number_format($value['TotalExport'],3)."</td>
                                                <td class='text-right'>".number_format($value['Inventory'],3)."</td>";
                                }
                                echo "</tr>";
		    					$i++;
		    				}
		    			}
		    			?>
		    		</table>
				</div>
        	</div>
        </div>
    </div>
    <div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<?php 
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<script>
	$(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day_warehouse",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"/warehouse_inventory_perchaseorder/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="mapo" || fieldname=="nhacungcap" || fieldname=="nguoitao"){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
        }
    }

    google.charts.load('current', {packages: ['corechart', 'line', 'bar']});
    google.charts.setOnLoadCallback(drawLineColors);

    function drawLineColors() {
      var data = new google.visualization.DataTable();
      data.addColumn('date', 'X');
      data.addColumn('number', 'Nhập');
      data.addColumn('number', 'Xuất');
       
      data.addRows([
        [new Date("<?php echo date('Y-m-d',strtotime($startday)) ?>"),0,0]
        <?php 
        $total_import = 0;
        $total_export = 0;
        for($i=strtotime($startday); $i < strtotime($stopday)+1; $i=$i+3600*24) { 
            $str_time = date('Y-m-d',$i);
            $strday = date('dmY',$i);
            $point_import = isset($arr_import[$strday]['Amount']) ? $arr_import[$strday]['Amount'] : 0 ;
            $point_export = isset($arr_export[$strday]['Amount']) ? $arr_export[$strday]['Amount'] : 0 ;
            $total_import += $point_import;
            $total_export += $point_export;
            echo ",[new Date(\"$str_time\"),$total_import,$total_export]";
        }
        ?>
      ]);

      var options = {
        hAxis: {
          title: 'Ngày tài chính'
        },
        vAxis: {
          title: 'Giá trị nhập / xuất'
        },
        colors: ['#a52714', '#097138']
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day']
          <?php 
          if(count($arr_production)>0){
            foreach($arr_production as $row){
                echo ",['".$row['Name']."',".$row['Price']."]";
            }
          }
          ?>
        ]);

        var options = {
          title: 'Nhà cung cấp nhập hàng <?php echo $startday.' đến '.$stopday ?>'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div1'));
        chart.draw(data, options);
    }
</script>