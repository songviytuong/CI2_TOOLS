<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <!--<a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>-->
    </div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel tab1 tabcontent">
                    <div class="x_title">
                        <h2>Recruitment <small> / edit</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="ID" value="<?php echo isset($data) ? $data->ID : 0; ?>" />
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="Name" type="text" class="form-control" name="Name" value="<?= isset($data) ? $data->Name : '' ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input id="Description" type="text" class="form-control" name="Description" value="<?= isset($data) ? $data->Description : ''?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start/End</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input id="Start" type="text" class="form-control" name="Start" value="<?= isset($data) ? $data->Start : '' ?>"/>

                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input id="End" type="text" class="form-control"  name="End" value="<?= isset($data) ? $data->End : ''?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <select name="city[]" multiple class="form-control">
                                        <?php 
                                            foreach($warehouse as $row){
                                        ?>
                                        <option value="<?=$row['city_id']?>" <?=(in_array($row['city_id'], json_decode($data->City))) ? "selected='selected'":"";?>><?=$row['city_name']?></option>
                                            <?php } ?>
                                    </select>
                                </div></div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <select name="Published" class="form-control">
                                        <option value="0" <?=($data->Published == 0) ? "selected='selected'":"";?>>No</option>
                                        <option value="1" <?=($data->Published == 1) ? "selected='selected'":"";?>>Yes</option>
                                    </select>
                                </div></div>
                        </div>
                        
                        <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">

                                    <textarea class="resizable_textarea form-control ckeditor" name="Content" id="Content"><?php echo $data->Content ?></textarea>

                                </div>

                            </div>
                        
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#Start,#End').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD 00:00:00',

        });
        
//        setInterval(autosave,20000);
    });
    
    function autosave(){
        var content = CKEDITOR.instances.Content.getData();
        var name = $("input[name='Name']").val();
        if(name!='' && content!=''){
            var formData = {
                'Name'       : name,
                'Content' : content,
                'ID'          : <?php echo $data->ID ?>
            };
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/site_recruitment/autosave/' ?>",
                dataType: "html",
                type: "POST",
                data: formData,
                success: function(result){},
                error: function(){
                    alert("Hệ thống tự động lưu dữ liệu không hoạt động do đường truyền có vấn đề . Vui lòng lưu lại bài viết để tránh mất dữ liệu !");
                }
            });
        }
    }
    
</script>