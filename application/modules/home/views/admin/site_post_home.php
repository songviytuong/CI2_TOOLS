
<div class="page-title">

    <div class="title_left">

        <h3>SITE GROUP

            <small></small>

        </h3>

    </div>

    <div class="title_right">

        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

            <form action="<?php echo $base_link . 'setsessionsearch'; ?>" method="post">

                <div class="input-group">

                    <input type="text" class="form-control" name="Keywords" placeholder="Search for..." value="<?php echo $this->session->userdata("post_filter_Keywords"); ?>" required>

                    <span class="input-group-btn">

                        <button class="btn btn-default" type="submit">Go!</button>

                    </span>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="clearfix"></div>

<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">

            <div class="x_title">

                <h2>POST <small> / view all post - <?php echo "total <b>(" . $find . ")</b> records" ?></small></h2>

                <ul class="nav navbar-right panel_toolbox">

                    <li><a href="<?php echo $base_link . "add_and_edit" ?>" class="btn btn-success" role="button">Add New</a></li>

                </ul>

                <div class="col-md-12 col-sm-12 col-xs-12 filters">
                    <form action="<?php echo $base_link . "setsessionsearch" ?>" method="POST">
                        <ul>
                            <li><span>Filter records</span></li>
                            <li> 
                                <select class="form-control" name="Published">
                                    <?php
                                    $currentpage = $this->uri->segment(4);
                                    $Published = $this->session->userdata("post_filter_Published");
                                    ?>
                                    <option value="" <?php echo $Published == '' ? "selected='selected'" : ''; ?>>-- All Published --</option>
                                    <option value="1" <?php echo $Published == 1 && is_numeric($Published) ? "selected='selected'" : ''; ?>> Enable </option>
                                    <option value="2" <?php echo $Published == 2 && is_numeric($Published) ? "selected='selected'" : ''; ?>> Pending </option>
                                    <option value="0" <?php echo $Published == 0 && is_numeric($Published) ? "selected='selected'" : ''; ?>> Disable </option>
                                </select>
                            </li>

                            <li>

                                <select class="form-control" name="CategoriesID">
                                    <option value="">-- All Categories --</option>
                                    <?php
                                    $CategoriesID = $this->session->userdata("post_filter_CategoriesID");
                                    $CategoriesRole = $this->user->CategoriesRole;
                                    $CategoriesRole = $CategoriesRole != '' ? json_decode($CategoriesRole, true) : array();
                                    $CategoriesRole = count($CategoriesRole) > 0 ? implode(",", $CategoriesRole) : "0";
                                    if ($this->user->IsAdmin == 1) {
                                        $categories = $this->db->query("select ID,Title from ttp_categories")->result();
                                    } else {
                                        $categories = $this->db->query("select ID,Title from ttp_categories")->result();
                                        //$categories = $this->db->query("select ID,Title from ttp_categories where ID in($CategoriesRole)")->result();
                                    }
                                    if (count($categories) > 0) {
                                        foreach ($categories as $row) {
                                            if ($currentpage == 'setsessionsearch') {
                                                $selected = $row->ID == $CategoriesID ? "selected='selected'" : '';
                                            } else {
                                                $selected = '';
                                            }
                                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </li>
                            <li><input type="submit" class="btn btn-default" value="Filter" /></li>
                            <li><a class="btn btn-default" href="<?php echo $base_link . 'clearfilter' ?>">Clear Filter</a>
                        </ul>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>STT</th>
                            <th>Title </th>
                            <th>Categories </th>
                            <th>Created </th>
                            <th>Published </th>
                            <th>Owner</th>
                            <th class="text-right last"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($data) > 0) {
                            $i = $start;
                            foreach ($data as $row) {
                                $this->load->model('users_model', 'users');
                                $Users = $this->users->defineUsers($row->Owner);
                                $i++;
                                ?>
                                <tr class="even pointer">
                                    <td class="a-center ">
                                        <?php echo $i; ?>
                                    </td>
                                    <td><?php echo "<a href='{$base_link}edit/$row->ID'>" . $row->Title . '</a>'; ?></td>
                                    <td><?php echo $row->PageTitle; ?></td>
                                    <td><?php echo $row->Created; ?></td>
                                    <?php
                                    switch ($row->Published) {
                                        case 0:
                                            $published = "Disable";
                                            break;
                                        case 1:
                                            $published = "Enable";
                                            break;
                                        case 2:
                                            $published = "Pending";
                                            break;
                                        default:
                                            $published = "Disable";
                                            break;
                                    }
                                    ?>
                                    <td class="a-right a-right "><?php echo $published; ?></td>
                                    <td class="a-right a-right "><?php echo $Users['FirstName']; ?></td>
                                    <td class="last text-right">
                                        <a href='<?php echo $base_link . "edit/$row->ID" ?>'><i class="fa fa-pencil-square-o"></i> Edit </a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href='<?php echo $base_link . "delete/$row->ID" ?>' class='delete_link'><i class='fa fa-trash-o'></i> Delete</a>
                                    </td>

                                </tr>

                                <?php
                            }
                        } else {
                            ?>

                            <tr class="even pointer">

                                <td class="a-center" colspan="7" style="text-align:center">Data is empty .</td>

                            </tr>

                            <?php
                        }
                        ?>

                    </tbody>

                </table>

                <?php if (count($data) > 0) echo $nav; ?>

            </div>

            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>Post / month</th>
                            <?php
                            for ($i = -1; $i < 12; $i++) {
                                echo "<th>" . date('M Y', strtotime('+' . $i . " month")) . "</th>";
                            }
                            ?>

                        </tr>
                    </thead>
                    <tbody>
                        <tr class="even pointer">
                            <?php
                            #SELECT OWNER
                            $this->load->model('users_model', 'users');
                            $data_users = $this->users->getUsersByGroupID(2);
                            foreach ($data_users as $user) {
                                ?>
                                <td><?= $user->FirstName; ?></td>
                                <?php
                                for ($i = -1; $i < 12; $i++) {
                                    $this_year = date('Y');
                                    $this_month = date('m', strtotime('+' . $i . " month"));
                                    $this->load->model('post_model', 'post');
                                    $count = $this->post->getPostCounterByDate(array('Owner' => $user->ID, 'month' => $this_month, 'year' => $this_year));
                                    ?>
                                    <td class="a-center"><?= $count; ?></td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>



