<?php

class Warehouse_products extends Admin_Controller {

    static $limit = 10;
    public $user;
    public $classname = "warehouse_products";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        $this->load->library('template');
        $this->load->model('products_model', 'products');
        $this->template->set_template('report');

        if (!in_array($this->user->ID, isOpenSideBar(2))) {
            $this->template->write_view('sitebar', 'admin/warehouse_sitebar', array('user' => $this->user));
        }
        $this->template->write_view('header', 'admin/header', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public static function setLimit($limit) {
        self::$limit = $limit;
    }

    public static function getLimit() {
        return self::$limit;
    }

    public function importTestStep1() {
        $this->db->select('*');
        $a = $this->db->get('auc_skuean')->result();
        foreach ($a as $key => $item) {
            $query1 = $this->db->query("SELECT ID,SKU,EAN FROM auc_skuean_trunc
                           WHERE `SKU` = '" . $item->SKU . "' limit 1");
            if ($query1->num_rows() == 0) {
                $data = array(
                    'SKU' => $item->SKU,
                    'EAN' => $item->EAN,
                    'Name' => trim($item->Name)
                );
                $this->db->insert('auc_skuean_trunc', $data);
            } else {
                $query = $this->db->query("SELECT ID,SKU,EAN FROM auc_skuean_trunc
                           WHERE `EAN` LIKE '%" . $item->EAN . "%' limit 1");
                if ($query->num_rows() == 0) {
                    $update = array(
                        'EAN' => $query1->row()->EAN . "," . $item->EAN
                    );
                    $this->db->where('SKU', $item->SKU);
                    $this->db->update('auc_skuean_trunc', $update);
                }
            }
        }
    }

    public function importTestStep() {
        $this->db->select('*');
        $a = $this->db->get('auc_skuean')->result();
        foreach ($a as $key => $item) {
            $query1 = $this->db->query("SELECT ID,MaSP,Barcode FROM ttp_report_products
                           WHERE `MaSP` = '" . trim($item->SKU) . "' limit 1");
            if ($query1->num_rows() == 0) {
                $data = array(
                    'MaSP' => trim($item->SKU),
                    'Barcode' => trim($item->EAN),
                    'ShortName' => trim($item->Name),
                    'Imported' => date('Y-m-d H:i:s'),
                    'TrademarkID' => 7
                );
                $this->db->insert('ttp_report_products', $data);
            } else {
                $query = $this->db->query("SELECT ID,MaSP,Barcode FROM ttp_report_products
                           WHERE `Barcode` LIKE '%" . trim($item->EAN) . "%' limit 1");
                if ($query->num_rows() == 0) {
                    $query2 = $this->db->query("SELECT Barcode FROM ttp_report_products
                           WHERE `MaSP` = '" . $item->SKU . "' limit 1");
                    if ($query2->row()->Barcode == "" || $query2->row()->Barcode == NULL || empty($query2->row()->Barcode)) {
                        $new_barcode = trim($item->EAN);
                    } else {
                        $new_barcode = trim($query1->row()->Barcode) . "," . trim($item->EAN);
                    }
                    $update = array(
                        'Barcode' => $new_barcode,
                        'Imported' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('MaSP', trim($item->SKU));
                    $this->db->update('ttp_report_products', $update);
                }
            }
        }
    }

    public function importTestStep2() {
        $key_data = isset($_GET['item']) ? $_GET['item'] : -1;
        if ($key_data == -1) {
            echo "not found item";
            return;
        }
//        $q = $this->db->query("SELECT * FROM `ttp_report_products` WHERE `Barcode` = ''")->result();
//        foreach ($q as $it) {
//            $ids[] = $it->MaSP;
//        }
//        $l = implode(",", $ids);

        $this->db->select('*');
//        $abc = "SKU IN ($l)";
//        $this->db->where($abc);
        $this->db->order_by('SKU');
        $a = $this->db->get('auc_skuean')->result_array();
        $item = isset($a[$key_data]) ? $a[$key_data] : '';
        $key = $key_data;
        if ($item != '' && $item["SKU"] != "") {
            $query1 = $this->db->query("SELECT ID,MaSP,Barcode FROM ttp_report_products
                           WHERE `MaSP` = '" . trim($item['SKU']) . "' limit 1");
            if ($query1->num_rows() == 0) {
                $data = array(
                    'MaSP' => trim($item['SKU']),
                    'Barcode' => trim($item['EAN']),
                    'ShortName' => trim($item['Name']),
                    'Imported' => date('Y-m-d H:i:s'),
                    'TrademarkID' => 7
                );
                $this->db->insert('ttp_report_products', $data);
            } else {
                $query = $this->db->query("SELECT ID,MaSP,Barcode FROM ttp_report_products
                           WHERE `Barcode` LIKE '%" . trim($item['EAN']) . "%' limit 1");

                if ($query->num_rows() == 0) {
                    $query2 = $this->db->query("SELECT Barcode FROM ttp_report_products
                           WHERE `MaSP` = '" . $item['SKU'] . "' limit 1");
                    if ($query2->row()->Barcode == "" || $query2->row()->Barcode == NULL || empty($query2->row()->Barcode)) {
                        $new_barcode = trim($item['EAN']);
                    } else {
                        $new_barcode = trim($query1->row()->Barcode) . "," . trim($item['EAN']);
                    }
                    $update = array(
                        'Barcode' => $new_barcode,
                        'Imported' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('MaSP', trim($item['SKU']));
                    $this->db->update('ttp_report_products', $update);
                }
            }
            echo "<small>SKU: " . $item["SKU"] . "</small>";
            $key++;
            echo "<meta http-equiv='refresh' content='0;URL=\"" . base_url() . ADMINPATH . "/report/warehouse_products/importTestStep2/?item=" . $key . "\"' />";
        } else {
            echo "Done.";
        }
    }

    public function importTestStep3() {
        $this->db->select('ID,Barcode');
        $this->db->like('Barcode', ',');
        $a = $this->db->get('ttp_report_products')->result();
        foreach ($a as $key => $item) {
            if ($item->Barcode[0] == ',') {
                $update = array(
                    'Barcode' => substr($item->Barcode, 1)
                );
                $this->db->where('ID', $item->ID);
                $this->db->update('ttp_report_products', $update);
            }
        }
    }

    public function add_unit() {
        $unit = isset($_POST['Data']) ? $_POST['Data'] : '';
        if ($unit != '') {
            $data = array('Title' => $unit);
            $this->db->insert("ttp_report_unit", $data);
        }
    }

    public function add_trade() {
        $trade = isset($_POST['Data']) ? $_POST['Data'] : '';
        $query = $this->db->query("SELECT ID FROM ttp_report_trademark
                           WHERE `Title` = '" . $trade . "' limit 1");
        if ($trade != '' && $query->num_rows() == 0) {
            $data = array(
                'Title' => $trade,
                'Alias' => $this->lib->alias($trade),
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s'),
                'Owner' => $this->user->ID,
                'Published' => 1,
            );
            $this->db->insert("ttp_report_trademark", $data);
            echo $this->db->insert_id();
        } else {
            echo "Error";
        }
    }

    public function add_capacity_unit() {
        $unit = isset($_POST['Data']) ? $_POST['Data'] : '';
        if ($unit != '') {
            $data = array('Title' => $unit);
            $this->db->insert("ttp_report_unit_capacity", $data);
        }
    }

    public function remove_ingredients() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $this->db->query("delete from ttp_report_products_ingredients where ID=$ID");
    }

    public function load_ingredient() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $Ingredients = $this->db->query("select * from ttp_report_products_ingredients where ProductsID=$ID")->result();
        if (count($Ingredients) > 0) {
            foreach ($Ingredients as $row) {
                echo "<div style='float:left;margin-right:30px;margin-bottom:10px'><a onclick='remove_ingredients(this,$row->ID)'><i class='fa fa-times-circle' aria-hidden='true' style='color:#c1c1c1;font-size:15px;margin-right:5px;'></i></a><a onclick='changeIngredients(this,$row->ID)'>$row->Title</a></div>";
            }
        }
    }

    public function add_ingredients() {
        $data = isset($_POST['data']) ? json_decode($_POST['data']) : '';
        $id = isset($data->id) ? $data->id : 0;
        $list = isset($data->data) ? $data->data : '';
        if ($id > 0 && $list != '') {
            $list = explode(',', $list);
            if (count($list) > 0) {
                foreach ($list as $row) {
                    $row = trim($row);
                    $arr = array('ProductsID' => $id, 'Title' => $row);
                    $this->db->insert('ttp_report_products_ingredients', $arr);
                }
            }
        }
    }

    public function ShortNameIsNull() {
        die();
        $this->db->select("ID,Title,ShortName");
        $this->db->where('Created', '0000-00-00 00:00:00');
        $this->db->where('ShortName', '');
        $result = $this->db->get('ttp_report_products')->result();
        foreach ($result as $row) {
            $update = array(
                'ShortName' => trim($row->Title),
                'Title' => '',
            );
            $this->db->where('ID', $row->ID);
            $this->db->update('ttp_report_products', $update);
        }
        echo "<pre>";
        var_dump($result);
    }

    public function UpdateOwnerFromAssign() {
        $this->db->select("ProductID,Owner");
        $result = $this->db->get('auc_assign')->result();
        foreach ($result as $row) {
            $update = array(
                'Owner' => $row->Owner
            );
            $this->db->where('ID', $row->ProductID);
            $this->db->update('ttp_report_products', $update);
        }
//        echo "<pre>";
//        var_dump($result);
    }

    public function index2() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $fillter = $this->session->userdata('fillter_products');
        $fillter = $fillter == '' ? "" : " and " . $fillter;
        $limit_str = "limit $start," . $this->getLimit();
        $nav = $this->db->query("select count(1) as nav from ttp_report_products a,ttp_report_trademark b where a.TrademarkID=b.ID and a.ParentID=0 $fillter")->row();
        $nav = $nav ? $nav->nav : 0;

        $object = $this->cache->get('DataProducts');
        for ($i = 0; $i < $this->getLimit(); $i++) {
            $arr[] = $object[$i];
        }
        if (!$object) {
            $object = $this->db->query("select a.Path as Path,"
                            . "a.ID as ID,"
                            . "a.Title as Title,"
                            . "a.ShortName as ShortName,"
                            . "a.CategoriesID as CategoriesID,"
                            . "a.Alias as Alias,"
                            . "a.MetaTitle as MetaTitle,"
                            . "a.MetaDescription as MetaDescription,"
                            . "a.MetaKeywords as MetaKeywords,"
                            . "a.MaSP as MaSP,"
                            . "a.Price as Price,"
                            . "a.Published as Published,"
                            . "b.Title as Trademark from ttp_report_products a,ttp_report_trademark b where a.TrademarkID=b.ID and a.ParentID=0 order by a.ID DESC")->result();
            $this->cache->write($object, 'DataProducts');
        }
        $data = array(
            'allowassign' => (!in_array($this->user->ID, isCollaborators()) ? TRUE : FALSE),
            'fill_data' => $this->session->userdata('fillter_products'),
            'fillter' => $fillter,
            'base_link' => base_url() . ADMINPATH . '/report/warehouse_products/',
            'data' => $arr,
            'start' => $start,
            'find' => $nav,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/report/warehouse_products/index', 5, $nav, $this->getLimit())
        );
        $this->template->add_title('Products | Warehouse Report Tools');
        $this->template->write_view('content', 'admin/warehouse_products_home', $data);
        $this->template->render();
    }

    public function index() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        if (!$this->session->userdata('limit')) {
            $this->session->set_userdata('limit', $this->getLimit());
        }
        $this->setLimit($this->session->userdata('limit'));

        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;

        $limit = $this->getLimit();
        $limit_str = "limit $start," . $limit;

        #GET NOT IN ASSIGN
        $hasAssign = $this->session->userdata('Assign');
        $this->load->model('assign_model', 'assign');
        $counterAssign = $this->assign->getCounterAssign($this->user->ID);
        $ignoreAssign = array();
        if (!$hasAssign && $counterAssign > 0) {
            $data_assign = $this->assign->getAllAssign();
            if ($data_assign) {
                foreach ($data_assign as $key => $itemAssign) {
                    $ignoreAssign[] = $itemAssign->ProductID;
                }
                $ignoreIDs = implode(",", $ignoreAssign);
            }
        }

        $Collaborators = $this->session->userdata('Collaborators');

        $fillter = $this->session->userdata('fillter_products');
        $fillter = ($fillter == '') ? "" : $fillter;

        #GET STATUS PHOTO READY
        $skus = array();
//        $skus = $this->cache->get('getDataImport!' . substr(md5($this->user->ID . 'getDataImport'), 27, 06));
//        if (!$skus) {
        $this->load->model('Imported_model', 'Imported');
        $Imported_Data = $this->Imported->getDataImport();
        foreach ($Imported_Data as $row) {
            $skus[] = $row->SKU;
        }
//            $this->cache->write($skus, 'getDataImport!' . substr(md5($this->user->ID . 'getDataImport'), 27, 06), 300);
//        }

        $ids_assigned = $assign_data = array();
//        $ids_assigned = $this->cache->get('AssignData!' . substr(md5($this->user->ID . 'AssignData'), 27, 06));
//        if (!$ids_assigned) {
//            $assign_data = $this->cache->get('getDataAssign!' . substr(md5($this->user->ID . 'getDataAssign'), 27, 06));
//            if (!$assign_data) {
        $assign_data = $this->assign->getDataAssign();
//                $this->cache->write($assign_data, 'getDataAssign!' . substr(md5($this->user->ID . 'getDataAssign'), 27, 06), 300);
//            }
        foreach ($assign_data as $assigned) {
            $ids_assigned[] = $assigned->ProductID;
        }
//            $this->cache->write($ids_assigned, 'AssignData!' . substr(md5($this->user->ID . 'AssignData'), 27, 06), 300);
//        }
        if (in_array($this->user->ID, isCollaborators())) {
            $data_assign = $this->assign->getAllAssign(array('oid' => $this->user->ID));
            foreach ($data_assign as $key => $itemAssign) {
                $allow[] = $itemAssign->ProductID;
            }
            $allow_ids = implode(",", $allow);
            $fillter = " and a.ID IN($allow_ids)";
        } else {
            $fillter = "";
        }

        $arr = $temp1 = $rest = $rest1 = $rest2 = $rest3 = array();
        $nav = 0;
        $sKUs = implode(",", $skus);
        if ($Collaborators == "Photo Ready") {
            $iID_assigned = implode(",", $ids_assigned);
            $fillter = " and a.MaSP IN($sKUs) and a.ID NOT IN($iID_assigned) and a.Title = ''";
        } else if ($Collaborators == "Wait Approval") {
            $fillter = " and a.MaSP IN($sKUs) and c.Status = 3";
        } else if ($Collaborators == "Rejected") {
            $fillter = " and a.MaSP IN($sKUs) and c.Status = 4";
        } else if ($Collaborators == "Approved") {
            $fillter = " and a.MaSP IN($sKUs) and c.Status = 5";
        } else if ($Collaborators == "Draft") {
            $fillter = " and a.Published = 0 and a.Status = 0";
        } else if ($Collaborators == "Published") {
            $fillter = " and a.Published = 1 and a.Status = 1";
        } else if ($Collaborators == "Assigned") {
            $fillter = " and a.MaSP IN($sKUs) and c.Status = 2";
        }

        $joins = "";
        if ($Collaborators && ($Collaborators == "Photo Ready")) {
            
        } else {
            $joins = $joins . ",auc_assign c ";
            $fillter .= " and a.ID = c.ProductID and a.ParentID=0 ";
        }

        $query = "select a.Path as Path,"
                . "a.ID as ID,"
                . "a.Title as Title,"
                . "a.ShortName as ShortName,"
                . "a.CategoriesID as CategoriesID,"
                . "a.Alias as Alias,"
                . "a.MetaTitle as MetaTitle,"
                . "a.MetaDescription as MetaDescription,"
                . "a.MetaKeywords as MetaKeywords,"
                . "a.MaSP as MaSP,"
                . "a.LastEdited as LastEdited,"
                . "a.ParentID as ParentID,"
                . "a.Price as Price,"
                . "a.Published as Published,"
                . "b.Title as Trademark "
                . "from ttp_report_products a,"
                . "ttp_report_trademark b"
                . "$joins "
                . "where a.TrademarkID=b.ID "
                . "$fillter "
                . "order by a.LastEdited DESC,a.Imported ASC $limit_str";
//        var_dump($query); exit();
        $object = $this->db->query($query)->result();


#IF User is Collaborators
        if (in_array($this->user->ID, isCollaborators())) {
            if ($counterAssign > 0) {
                $ignoreA = array();
                $data_assign = $this->assign->getAllAssign(array('oid' => $this->user->ID));

                foreach ($data_assign as $key => $itemAssign) {
                    $ignoreA[] = $itemAssign->ProductID;
                }
                $ignoreIDs = implode(",", $ignoreA);
                $fillter = " and a.ID IN ($ignoreIDs)";

#START
                foreach ($object as $row) {
                    if (in_array($row->ID, $ignoreA)) {
                        $temp[] = $row->MaSP;
                    }
                }
                $nav = $temp ? count($temp) : 0;
                foreach ($object as $item) {
                    if (in_array($item->MaSP, $temp)) {
                        $temp1[] = $item;
                    }
                }
#END
            }
        }

        if (!in_array($this->user->ID, isCollaborators())) {
            foreach ($object as $item) {
                $arr[] = $item;
            }
        } else {
            if ($temp1) {
                foreach ($temp1 as $item) {
                    $arr[] = $item;
                }
            }
        }

#GET TOTAL RECORD
        if ($Collaborators && ($Collaborators == "Photo Ready")) {
            $query_counter = "select count(1) as nav from ttp_report_products a, ttp_report_trademark b where a.TrademarkID = b.ID $fillter";
        } else {
            $fillter .= " and a.ID = c.ProductID and a.ParentID=0 ";
            $query_counter = "select count(1) as nav from ttp_report_products a, ttp_report_trademark b $joins where a.TrademarkID = b.ID $fillter";
        }
        $counter = $this->db->query($query_counter)->row();
        $nav = ($counter) ? $counter->nav : 0;

        $data = array(
            'allowassign' => (!in_array($this->user->ID, isCollaborators()) ? TRUE : FALSE),
            'fill_data' => $this->session->userdata('fillter_products'),
            'fillter' => $fillter,
            'base_link' => base_url() . ADMINPATH . '/report/warehouse_products/',
            'data' => $arr,
            'start' => $start,
            'find' => $nav,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/report/warehouse_products/index', 5, $nav, $limit)
        );
        $this->template->add_title('Products | Warehouse Report Tools');
        $this->template->write_view('content', 'admin/warehouse_products_home', $data);
        $this->template->render();
    }

    public function crawler() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $this->template->add_title('Crawler data | Warehouse Report Tools');
        $this->template->write_view('content', 'admin/warehouse_products_crawler');
        $this->template->render();
    }

    public function setActive() {

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $res = array();
        if ($ID) {
            $params = array(
                'ID' => $ID
            );
            $products = $this->products->getProducts('TrademarkID,Published,Status', $params);
            if ($products->TrademarkID != 0 && $products->Published == 0) {
                $update_data = array(
                    'Published' => 1,
                    'Status' => 1,
                );
                $res['img'] = "<i class='fa fa-check-square' style='color:#090'></i>";
            } else {
                $update_data = array(
                    'Published' => 0,
                    'Status' => 0,
                );
                $res['img'] = "<i class='fa fa-check-square' style='color:#ddd'></i>";
            }

            $this->db->where('ID', $ID);
            if ($this->db->update('ttp_report_products', $update_data)) {
                $history = array(
                    'Created' => date('Y-m-d H:i:s'),
                    'ProductsID' => $ID,
                    'Type' => 'Product Status',
                    'Notes' => 'Modified',
                    'Owner' => $this->user->ID
                );
                $this->db->insert('ttp_report_products_history', $history);
            }
        }
        echo json_encode($res);
    }

    public function get_data_fillter() {
        $type = isset($_POST['Type']) ? $_POST['Type'] : 0;
        if ($type == 1) {
            echo "<a onclick='set_fill_tr(this,2)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select b.Title,b.ID from ttp_report_trademark b order by b.Title ASC")->result();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    echo "<a onclick='set_fill_tr(this,2)' value='$row->ID'>$row->Title</a>";
                }
            }
        } elseif ($type == 2) {
            echo "<a onclick='set_fill_tr(this,0)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select a.ID,a.MaSP from ttp_report_products a where a.ParentID=0 order by a.MaSP ASC")->result();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    $row->MaSP = str_replace("'", '', $row->MaSP);
                    echo "<a onclick='set_fill_tr(this,0)' value='$row->MaSP' title='$row->MaSP'>$row->MaSP</a>";
                }
            }
        } elseif ($type == 3) {
            echo "<a onclick='set_fill_tr(this,1)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select a.ID,a.Title from ttp_report_products a where a.ParentID=0 order by a.Title ASC")->result();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    $row->Title = str_replace("'", '', $row->Title);
                    echo "<a onclick='set_fill_tr(this,1)' value='$row->Title' title='$row->Title'>$row->Title</a>";
                }
            }
        } elseif ($type == 4) {
            echo "<a onclick='set_fill_tr(this,6)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.CategoriesID from ttp_report_products a where a.ParentID=0 and CategoriesID!='[]'")->result();
            $arr = array();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    $temp = json_decode($row->CategoriesID, true);
                    if (is_array($temp) && count($temp) > 0) {
                        $arr = array_merge($arr, $temp);
                    }
                }
            }
            $arr = array_unique($arr);
            if (count($arr) > 0) {
                $arr = implode(',', $arr);
                $categories = $this->db->query("select Path from ttp_report_categories where ID in($arr)")->result();
                $arr_parent = array();
                if (count($categories) > 0) {
                    foreach ($categories as $row) {
                        $temp_path = explode('/', $row->Path);
                        $temp_path = isset($temp_path[0]) ? $temp_path[0] : 0;
                        if ($temp_path > 0) {
                            $arr_parent[] = $temp_path;
                        }
                    }
                }
                if (count($arr_parent) > 0) {
                    $arr_parent = implode(',', $arr_parent);
                    $categories = $this->db->query("select Title,ID from ttp_report_categories where ID in($arr_parent)")->result();
                    if (count($categories) > 0) {
                        foreach ($categories as $row) {
                            echo "<a onclick='set_fill_tr(this,6)' value='$row->ID' title='$row->Title'>$row->Title</a>";
                        }
                    }
                }
            }
        } elseif ($type == 5) {
            echo "<a onclick='set_fill_tr(this,5)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.CategoriesID from ttp_report_products a where a.ParentID=0 and CategoriesID!='[]'")->result();
            $arr = array();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    $temp = json_decode($row->CategoriesID, true);
                    if (is_array($temp) && count($temp) > 0) {
                        $arr = array_merge($arr, $temp);
                    }
                }
            }
            $arr = array_unique($arr);
            if (count($arr) > 0) {
                $arr = implode(',', $arr);
                $categories = $this->db->query("select ID,Title from ttp_report_categories where ID in($arr)")->result();
                if (count($categories) > 0) {
                    foreach ($categories as $row) {
                        echo "<a onclick='set_fill_tr(this,5)' value='$row->ID' title='$row->Title'>$row->Title</a>";
                    }
                }
            }
        } elseif ($type == 6) {
            echo "<a onclick='set_fill_tr(this,3)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.Price from ttp_report_products a where a.ParentID=0 order by a.Price ASC")->result();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    echo "<a onclick='set_fill_tr(this,3)' value='$row->Price' title='$row->Price'>" . number_format($row->Price) . "</a>";
                }
            }
        } elseif ($type == 7) {
            echo "<a onclick='set_fill_tr(this,4)' value=''>-- Tất Cả --</a>";
            $object = $this->db->query("select DISTINCT a.CurrentAmount from ttp_report_products a where a.ParentID=0 order by a.CurrentAmount ASC")->result();
            if (count($object) > 0) {
                foreach ($object as $row) {
                    echo "<a onclick='set_fill_tr(this,4)' value='$row->CurrentAmount' title='$row->CurrentAmount'>" . number_format($row->CurrentAmount) . "</a>";
                }
            }
        }
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_report_products where ID=$id")->row();

        $this->load->model('assign_model', 'assign');
        $lastUpdate = $this->assign->lastUpdate($id);

        if ($result) {
            $this->template->add_title('Products Edit | Warehouse Report Tools');
            $this->template->write_view('content', 'admin/warehouse_products_edit', array('data' => $result, 'lastUpdate' => $lastUpdate));
            $this->template->render();
        }
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Products Add | Warehouse Report Tools');
        $this->template->write_view('content', 'admin/warehouse_products_add');
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $MaSP = isset($_POST['MaSP']) ? trim($_POST['MaSP']) : '';
        $TypeProducts = isset($_POST['TypeProducts']) ? (int) $_POST['TypeProducts'] : 0;
        $TypeProducts = $TypeProducts > 9 ? 0 : $TypeProducts;
        $AcceptGift = isset($_POST['AcceptGift']) ? $_POST['AcceptGift'] : 0;
        $AcceptGift = $AcceptGift === "on" ? 1 : 0;
        $Featured = isset($_POST['Featured']) ? (int) $_POST['Featured'] : 0;
        $SaleGift = isset($_POST['SaleGift']) ? trim($_POST['SaleGift']) : '';
        $CategoriesID = isset($_POST['CategoriesID']) ? $_POST['CategoriesID'] : array();
        $CategoriesID = array_unique($CategoriesID);
        $PropertiesID = isset($_POST['PropertiesID']) ? $_POST['PropertiesID'] : array();
        $PropertiesPublished = isset($_POST['PropertiesPublished']) ? $_POST['PropertiesPublished'] : array();
        $VariantValue = array('Properties' => $PropertiesID, 'Published' => $PropertiesPublished);
        $VariantType = isset($_POST['VariantType']) ? trim($_POST['VariantType']) : 0;
        $Title = isset($_POST['Title']) ? trim($_POST['Title']) : '';
        $ShortName = isset($_POST['ShortName']) ? trim($_POST['ShortName']) : '';
        $Description = isset($_POST['Description']) ? trim($_POST['Description']) : '';
        $MetaTitle = isset($_POST['MetaTitle']) ? trim($_POST['MetaTitle']) : '';
        $MetaKeywords = isset($_POST['MetaKeywords']) ? trim($_POST['MetaKeywords']) : '';
        $MetaDescription = isset($_POST['MetaDescription']) ? trim($_POST['MetaDescription']) : '';
        $Capacity = isset($_POST['Capacity']) ? trim($_POST['Capacity']) : '';
        $Weight = isset($_POST['Weight']) ? trim($_POST['Weight']) : 0;
        $Length = isset($_POST['Length']) ? trim($_POST['Length']) : 0;
        $Width = isset($_POST['Width']) ? trim($_POST['Width']) : 0;
        $Height = isset($_POST['Height']) ? trim($_POST['Height']) : 0;
        $Startday = isset($_POST['Startday']) ? trim($_POST['Startday']) : '';
        $Stopday = isset($_POST['Stopday']) ? trim($_POST['Stopday']) : '';
        $Published = isset($_POST['Published']) ? trim($_POST['Published']) : 0;
        $Show = isset($_POST['Show']) ? trim($_POST['Show']) : 0;
        $CountryID = isset($_POST['CountryID']) ? trim($_POST['CountryID']) : 0;
        $TrademarkID = isset($_POST['TrademarkID']) ? trim($_POST['TrademarkID']) : 0;
        $Donvi = isset($_POST['Donvi']) ? trim($_POST['Donvi']) : '';
//        $BarcodeClient = isset($_POST['BarcodeClient']) ? trim($_POST['BarcodeClient']) : '';
        $Chimmoi = isset($_POST['Chimmoi']) ? trim($_POST['Chimmoi']) : 0;
        $Loyalty_card = isset($_POST['Loyalty_card']) ? trim($_POST['Loyalty_card']) : 0;
        $Price = isset($_POST['Price']) ? trim($_POST['Price']) : 0;
        $SocialPrice = isset($_POST['SocialPrice']) ? trim($_POST['SocialPrice']) : 0;
        $BasePrice = isset($_POST['BasePrice']) ? trim($_POST['BasePrice']) : '';
        $RootPrice = isset($_POST['RootPrice']) ? trim($_POST['RootPrice']) : 0;
        $SpecialPrice = isset($_POST['SpecialPrice']) ? trim($_POST['SpecialPrice']) : '';
        $Special_startday = isset($_POST['Special_startday']) ? trim($_POST['Special_startday']) : '';
        $Special_stopday = isset($_POST['Special_stopday']) ? trim($_POST['Special_stopday']) : '';
        $VAT = isset($_POST['VAT']) ? trim($_POST['VAT']) : 0;
        $SLCurrent = isset($_POST['SLCurrent']) ? trim($_POST['SLCurrent']) : 0;
        $SLOutOfStock = isset($_POST['SLOutOfStock']) ? trim($_POST['SLOutOfStock']) : 0;
        $SLMinInCart = isset($_POST['SLMinInCart']) ? trim($_POST['SLMinInCart']) : 0;
        $SLMaxInCart = isset($_POST['SLMaxInCart']) ? trim($_POST['SLMaxInCart']) : 0;
        $AddcartNegativeNumber = isset($_POST['AddcartNegativeNumber']) ? trim($_POST['AddcartNegativeNumber']) : 0;
        $Available = isset($_POST['Available']) ? trim($_POST['Available']) : 0;
        $SupplierID = isset($_POST['SupplierID']) ? trim($_POST['SupplierID']) : 0;
        $PriceGroup_Title = isset($_POST['PriceGroup_Title']) ? $_POST['PriceGroup_Title'] : array();
        $PriceGroup_Price = isset($_POST['PriceGroup_Price']) ? $_POST['PriceGroup_Price'] : array();
        $PriceRange_Title = isset($_POST['PriceRange_Title']) ? $_POST['PriceRange_Title'] : array();
        $PriceRange_Amount = isset($_POST['PriceRange_Amount']) ? $_POST['PriceRange_Amount'] : array();
        $PriceRange_Price = isset($_POST['PriceRange_Price']) ? $_POST['PriceRange_Price'] : array();
        $Title = $Title == "" ? "Sản phẩm mới" : $Title;
        $MaSP = $MaSP == "" ? time() : $MaSP;
        $Content = isset($_POST['Content']) ? $_POST['Content'] : '';
        $Content = str_replace("<iframe", '<div class="video-embed embed-responsive embed-responsive-16by9"><iframe', $Content);
        $Content = str_replace("</iframe>", '</iframe></div>', $Content);
        $Instruction = isset($_POST['Instruction']) ? $_POST['Instruction'] : '';
        $Instruction = str_replace("<iframe", '<div class="video-embed embed-responsive embed-responsive-16by9"><iframe', $Instruction);
        $Instruction = str_replace("</iframe>", '</iframe></div>', $Instruction);
        $Tankage = isset($_POST['Tankage']) ? (int) $_POST['Tankage'] : 0;
        $Orders = isset($_POST['Orders']) ? (int) $_POST['Orders'] : 0;
        $OrdersCat = isset($_POST['OrdersCat']) ? (int) $_POST['OrdersCat'] : 0;
        if ($MaSP != '' && $Title != '') {
            $check = $this->db->query("select * from ttp_report_products where MaSP='$MaSP'")->row();
            if ($check) {
                echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                echo "<script>alert('Mã sản phẩm này đã có trong hệ thống , vui lòng chọn mã khác !');window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
                return false;
            }

//            if ($BarcodeClient != '') {
//                $check = $this->db->query("select BarcodeClient from ttp_report_products where BarcodeClient='$BarcodeClient' and BarcodeClient!=''")->row();
//                if ($check) {
//                    echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
//                    echo "<script>alert('Mã Barcode nhà cung cấp bị trùng , vui lòng chọn mã khác !');window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
//                    return false;
//                }
//            }
            $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
            $max = $max ? $max->ID + 1 : 1;
            $data = array(
                'ID' => $max,
                'Path' => $max,
                'Tankage' => $Tankage,
//                'BarcodeClient' => $BarcodeClient,
                'Orders' => $Orders,
                'OrdersCat' => $OrdersCat,
                'MaSP' => $MaSP,
                'Title' => $Title,
                'ShortName' => $ShortName,
                'Content' => $Content,
                'Instruction' => $Instruction,
                'Description' => $Description,
                'Capacity' => $Capacity,
                'Weight' => $Weight,
                'Length' => $Length,
                'Width' => $Width,
                'Height' => $Height,
//                'NewStartDay' => $Startday,
//                'NewStopDay' => $Stopday,
//                'SpecialStartday' => $Special_startday,
//                'SpecialStopday' => $Special_stopday,
                'Status' => $Show,
                'Published' => $Published,
                'CountryID' => $CountryID,
                'TrademarkID' => $TrademarkID,
                'VAT' => $VAT,
                'Chimmoi' => $Chimmoi,
                'CurrentAmount' => $SLCurrent,
                'WarningAmount' => $SLOutOfStock,
                'MinCartAmount' => $SLMinInCart,
                'MaxCartAmount' => $SLMaxInCart,
                'PutcartNegative' => $AddcartNegativeNumber,
                'InventoryStatus' => $Available,
                'Price' => $Price,
                'RootPrice' => $RootPrice,
                'BasePrice' => $BasePrice,
                'SpecialPrice' => $SpecialPrice,
                'SocialPrice' => $SocialPrice,
                'Donvi' => $Donvi,
                'CategoriesID' => json_encode($CategoriesID),
                'VariantValue' => json_encode($VariantValue),
                'VariantType' => $VariantType,
                'MetaTitle' => $MetaTitle,
                'MetaKeywords' => $MetaKeywords,
                'MetaDescription' => $MetaDescription,
                'AcceptGift' => $AcceptGift,
                'SaleGift' => $SaleGift,
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s'),
                'TypeProducts' => $TypeProducts,
                'Featured' => $Featured,
                'SupplierID' => $SupplierID
            );

            $data['Created'] = date('Y-m-d H:i:s');
            $data['LastEdited'] = date('Y-m-d H:i:s');
            $data['Owner'] = $this->user->ID;

            $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title;
            $data['Alias'] = $this->lib->alias($Alias);
            $check = $this->db->query('select ID from ttp_report_products where Alias="' . $Alias . '"')->row();
            if ($check) {
                $data['Alias'] .= $max;
            }
            $IDstr = str_pad($max, 8, '0', STR_PAD_LEFT);
            $data['Barcode'] = "603" . $TypeProducts . $IDstr . '1';
            if ($this->db->insert('ttp_report_products', $data)) {
                $history = array(
                    'Created' => date('Y-m-d H:i:s'),
                    'ProductsID' => $this->db->insert_id(),
                    'Type' => 'New Product',
                    'Owner' => $this->user->ID
                );
                $this->db->insert('ttp_report_products_history', $history);
            }

            if (count($CategoriesID) > 0) {
                $str_categories_map = array();
                foreach ($CategoriesID as $row) {
                    $str_categories_map[] = "($max,$row)";
                }
                $str_categories_map = implode(',', $str_categories_map);
                $str_categories_map = "insert into ttp_report_products_categories(ProductsID,CategoriesID) values" . $str_categories_map;
                $this->db->query($str_categories_map);
            }

            if (count($PropertiesID) > 0) {
                $PropertiesID = implode(',', $PropertiesID);
                $Properties = $this->db->query("select * from ttp_report_properties where ID in($PropertiesID)")->result();
                if (count($Properties) > 0) {
                    $arr_sql = array();
                    $arr = array();
                    foreach ($Properties as $row) {
                        $checked = in_array($row->ParentID, $PropertiesPublished) ? 1 : 0;
                        if (!isset($arr[$row->ParentID])) {
                            $arr_sql[] = "($max,$row->ParentID,0,$checked)";
                            $arr[$row->ParentID] = 1;
                        }
                        $arr_sql[] = "($max,$row->ID,$row->ParentID,$checked)";
                    }
                    if (count($arr_sql) > 0) {
                        $arr_sql = "insert into ttp_report_product_properties(ProductsID,PropertiesID,ParentID,PublishedOnSite) values" . implode(',', $arr_sql);
                        $this->db->query($arr_sql);
                    }
                }
            }
            $this->create_shipment_default($max);
            $SaveAndExit = isset($_POST['SaveAndExit']) ? $_POST['SaveAndExit'] : 0;
            if ($SaveAndExit == 1) {
                redirect(ADMINPATH . '/report/warehouse_products/');
            }
            $currenttab = isset($_POST['currenttab']) ? $_POST['currenttab'] : '';
            $currenttab = $currenttab != '' ? '?tab=' . $currenttab : '';
            $sql = array(
                'ProductsID' => $max,
                'VAT' => $VAT,
                'Price' => $Price,
                'RootPrice' => $RootPrice,
                'SocialPrice' => $SocialPrice,
                'SpecialPrice' => $SpecialPrice,
//                'SpecialStartday' => $Special_startday,
//                'SpecialStopday' => $Special_stopday,
                'Created' => date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_report_products_price_analytics', $sql);
            $this->write_log("Add products from ttp_report_products where ID=" . $max . "\n JSON = " . json_encode($data));
            redirect(ADMINPATH . '/report/warehouse_products/edit/' . $max . $currenttab);
        }
    }

    public function create_shipment_default($ProductsID = 0) {
        $data = array(
            'UserID' => $this->user->ID,
            'ShipmentCode' => 'Default',
            'ProductsID' => $ProductsID,
            'DateProduction' => date('Y-m-d'),
            'DateExpiration' => date('Y-m-d', time() + 24 * 3600 * 365),
            'Created' => date('Y-m-d H:i:s')
        );
        $this->db->insert("ttp_report_shipment", $data);
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? trim($_POST['ID']) : 0;
        $MaSP = isset($_POST['MaSP']) ? trim($_POST['MaSP']) : '';
        $TypeProducts = isset($_POST['TypeProducts']) ? (int) $_POST['TypeProducts'] : 0;
        $TypeProducts = $TypeProducts > 9 ? 0 : $TypeProducts;
        $AcceptGift = isset($_POST['AcceptGift']) ? $_POST['AcceptGift'] : 0;
        $AcceptGift = $AcceptGift === "on" ? 1 : 0;
        $Featured = isset($_POST['Featured']) ? (int) $_POST['Featured'] : 0;
        $SaleGift = isset($_POST['SaleGift']) ? trim($_POST['SaleGift']) : '';
        $CategoriesID = isset($_POST['CategoriesID']) ? $_POST['CategoriesID'] : array();
        $CategoriesID = array_unique($CategoriesID);

        $PromotionsID = isset($_POST['PromotionsID']) ? $_POST['PromotionsID'] : array();
        $PromotionsID = array_unique($PromotionsID);

        $PropertiesID = isset($_POST['PropertiesID']) ? $_POST['PropertiesID'] : array();
        $PropertiesPublished = isset($_POST['PropertiesPublished']) ? $_POST['PropertiesPublished'] : array();
        $VariantValue = array('Properties' => $PropertiesID, 'Published' => $PropertiesPublished);
        $VariantType = isset($_POST['VariantType']) ? trim($_POST['VariantType']) : 0;

        $AddOnCategories = isset($_POST['AddOnCategories']) ? trim($_POST['AddOnCategories']) : 0;

        $TabsPromotion = isset($_POST['TabsPromotion']) ? $_POST['TabsPromotion'] : array();
        $TabsPromotion = array_unique($TabsPromotion);

        $Title = isset($_POST['Title']) ? trim($_POST['Title']) : '';

        $ShortName = isset($_POST['ShortName']) ? trim($_POST['ShortName']) : '';
        $Description = isset($_POST['Description']) ? trim($_POST['Description']) : '';
        $MetaTitle = isset($_POST['MetaTitle']) ? trim($_POST['MetaTitle']) : '';
        $MetaKeywords = isset($_POST['MetaKeywords']) ? trim($_POST['MetaKeywords']) : '';
        $MetaDescription = isset($_POST['MetaDescription']) ? trim($_POST['MetaDescription']) : '';
        $YoutubeCode = isset($_POST['YoutubeCode']) ? trim($_POST['YoutubeCode']) : '';
        $Capacity = isset($_POST['Capacity']) ? trim($_POST['Capacity']) : '';
        $Weight = isset($_POST['Weight']) ? trim($_POST['Weight']) : 0;
        $Length = isset($_POST['Length']) ? trim($_POST['Length']) : 0;
        $Width = isset($_POST['Width']) ? trim($_POST['Width']) : 0;
        $Height = isset($_POST['Height']) ? trim($_POST['Height']) : 0;
        $Startday = isset($_POST['Startday']) ? trim($_POST['Startday']) : '';
        $Stopday = isset($_POST['Stopday']) ? trim($_POST['Stopday']) : '';
        $Published = (isset($_POST['Published']) && count($CategoriesID) > 0) ? trim($_POST['Published']) : 0;
        $Show = isset($_POST['Show']) ? trim($_POST['Show']) : 0;
        $CountryID = isset($_POST['CountryID']) ? trim($_POST['CountryID']) : 0;
        $TrademarkID = isset($_POST['TrademarkID']) ? trim($_POST['TrademarkID']) : 0;
        $Donvi = isset($_POST['Donvi']) ? trim($_POST['Donvi']) : '';
        $Barcode = isset($_POST['Barcode']) ? trim($_POST['Barcode']) : '';
        $Chimmoi = isset($_POST['Chimmoi']) ? trim($_POST['Chimmoi']) : 0;
        $Loyalty_card = isset($_POST['Loyalty_card']) ? trim($_POST['Loyalty_card']) : 0;
        $Price = isset($_POST['Price']) ? trim($_POST['Price']) : 0;
        $SocialPrice = isset($_POST['SocialPrice']) ? trim($_POST['SocialPrice']) : 0;
        $RootPrice = isset($_POST['RootPrice']) ? trim($_POST['RootPrice']) : 0;
        $SpecialPrice = isset($_POST['SpecialPrice']) ? trim($_POST['SpecialPrice']) : '';
        $Special_startday = isset($_POST['Special_startday']) ? trim($_POST['Special_startday']) : '';
        $Special_stopday = isset($_POST['Special_stopday']) ? trim($_POST['Special_stopday']) : '';
        $VAT = isset($_POST['VAT']) ? trim($_POST['VAT']) : 0;
        $SLCurrent = isset($_POST['SLCurrent']) ? trim($_POST['SLCurrent']) : 0;
        $SLOutOfStock = isset($_POST['SLOutOfStock']) ? trim($_POST['SLOutOfStock']) : 0;
        $SLMinInCart = isset($_POST['SLMinInCart']) ? trim($_POST['SLMinInCart']) : 0;
        $SLMaxInCart = isset($_POST['SLMaxInCart']) ? trim($_POST['SLMaxInCart']) : 0;
        $AddcartNegativeNumber = isset($_POST['AddcartNegativeNumber']) ? trim($_POST['AddcartNegativeNumber']) : 0;
        $Available = isset($_POST['Available']) ? trim($_POST['Available']) : 0;
        $PriceGroup_Title = isset($_POST['PriceGroup_Title']) ? $_POST['PriceGroup_Title'] : array();
        $PriceGroup_Price = isset($_POST['PriceGroup_Price']) ? $_POST['PriceGroup_Price'] : array();
        $PriceRange_Title = isset($_POST['PriceRange_Title']) ? $_POST['PriceRange_Title'] : array();
        $PriceRange_Amount = isset($_POST['PriceRange_Amount']) ? $_POST['PriceRange_Amount'] : array();
        $PriceRange_Price = isset($_POST['PriceRange_Price']) ? $_POST['PriceRange_Price'] : array();
        $Title = $Title == "" ? "Sản phẩm mới" : $Title;

        $Content = isset($_POST['Content']) ? $_POST['Content'] : '';
        $Content = str_replace('<div class="video-embed embed-responsive embed-responsive-16by9">', '', $Content);
        $Content = str_replace('</iframe></div>', '</iframe>', $Content);
        $Content = str_replace("<iframe", '<div class="video-embed embed-responsive embed-responsive-16by9"><iframe', $Content);
        $Content = str_replace("</iframe>", '</iframe></div>', $Content);

        $Instruction = isset($_POST['Instruction']) ? $_POST['Instruction'] : '';
        $Instruction = str_replace('<div class="video-embed embed-responsive embed-responsive-16by9">', '', $Instruction);
        $Instruction = str_replace('</iframe></div>', '</iframe>', $Instruction);
        $Instruction = str_replace("<iframe", '<div class="video-embed embed-responsive embed-responsive-16by9"><iframe', $Instruction);
        $Instruction = str_replace("</iframe>", '</iframe></div>', $Instruction);
        $Tankage = isset($_POST['Tankage']) ? (int) $_POST['Tankage'] : 0;
        $Orders = isset($_POST['Orders']) ? (int) $_POST['Orders'] : 0;
        $OrdersCat = isset($_POST['OrdersCat']) ? (int) $_POST['OrdersCat'] : 0;
        $SupplierID = isset($_POST['SupplierID']) ? trim($_POST['SupplierID']) : 0;
        $MaSP = $MaSP == "" ? time() : $MaSP;
        $BasePrice = isset($_POST['BasePrice']) ? trim($_POST['BasePrice']) : '';
        if ($MaSP != '' && $Title != '') {

            $check = $this->db->query("select MaSP from ttp_report_products where MaSP='$MaSP' and ID!=$ID")->row();
            if ($check) {
                echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                echo "<script>alert('Mã sản phẩm này đã có trong hệ thống , vui lòng chọn mã khác !');window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
                return false;
            }
//            if ($BarcodeClient != '') {
//                $check = $this->db->query("select BarcodeClient from ttp_report_products where BarcodeClient='$BarcodeClient' and BarcodeClient!='' and ID!=$ID")->row();
//                if ($check) {
//                    echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
//                    echo "<script>alert('Mã Barcode nhà cung cấp bị trùng , vui lòng chọn mã khác !');window.location='" . $_SERVER['HTTP_REFERER'] . "'</script>";
//                    return false;
//                }
//            }

            $object_products = $this->db->query("select * from ttp_report_products where ID=$ID")->row();
            if ($object_products) {

                $data = array(
                    'MaSP' => $MaSP,
                    'Title' => $Title,
                    'ShortName' => $ShortName,
                    'Barcode' => $Barcode,
                    'Tankage' => $Tankage,
                    'Orders' => $Orders,
                    'OrdersCat' => $OrdersCat,
                    'Content' => $Content,
                    'Instruction' => $Instruction,
                    'Description' => $Description,
                    'Capacity' => $Capacity,
                    'Weight' => $Weight,
                    'Length' => $Length,
                    'Width' => $Width,
                    'Height' => $Height,
//                    'NewStartDay' => $Startday,
//                    'NewStopDay' => $Stopday,
//                    'SpecialStartday' => $Special_startday,
//                    'SpecialStopday' => $Special_stopday,
                    'Status' => $Show,
                    'Published' => $Published,
                    'CountryID' => $CountryID,
                    'TrademarkID' => $TrademarkID,
                    'VAT' => $VAT,
                    'Chimmoi' => $Chimmoi,
                    'Loyalty_card' => $Loyalty_card,
                    'CurrentAmount' => $SLCurrent,
                    'WarningAmount' => $SLOutOfStock,
                    'MinCartAmount' => $SLMinInCart,
                    'MaxCartAmount' => $SLMaxInCart,
                    'PutcartNegative' => $AddcartNegativeNumber,
                    'InventoryStatus' => $Available,
                    'Price' => $Price,
                    'BasePrice' => $BasePrice,
                    'RootPrice' => $RootPrice,
                    'SocialPrice' => $SocialPrice,
                    'SpecialPrice' => $SpecialPrice,
                    'Donvi' => $Donvi,
                    'CategoriesID' => json_encode($CategoriesID),
                    'PromotionsID' => json_encode($PromotionsID),
                    'VariantValue' => json_encode($VariantValue),
                    'VariantType' => $VariantType,
                    'AddOnCategories' => $AddOnCategories,
                    'TabsPromotion' => json_encode($TabsPromotion),
                    'MetaTitle' => $MetaTitle,
                    'MetaKeywords' => $MetaKeywords,
                    'MetaDescription' => $MetaDescription,
                    'AcceptGift' => $AcceptGift,
                    'SaleGift' => $SaleGift,
                    //'LastEdited' => date('Y-m-d H:i:s'),
                    'TypeProducts' => $TypeProducts,
                    'Featured' => $Featured,
                    'SupplierID' => $SupplierID
                );



                if (($object_products->Owner != $this->user->ID) && ($object_products->LastEdited == '0000-00-00 00:00:00')) {
                    $data['Created'] = date('Y-m-d H:i:s');
                    $data['Owner'] = $this->user->ID;
                }

//                $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title ;
                $data['Alias'] = $this->lib->alias($Title);
//                $check = $this->db->query('select ID from ttp_report_products where Alias="'.$Alias.'" and ID!='.$ID)->row();
//                if($check){
//                    $data['Alias'] .= $ID;
//                }
                $IDstr = str_pad($ID, 8, '0', STR_PAD_LEFT);
//$data['Barcode'] = "603" . $TypeProducts . $IDstr . '1';
                $video = json_decode($object_products->Video);
                $video = is_object($video) ? $video : (object) array();
                $videoarr = array('YoutubeCode' => $YoutubeCode, 'VideoSource' => $video->VideoSource, 'ImageVideo' => $video->ImageVideo);
                if (isset($_FILES['VideoSource'])) {
                    if ($_FILES['VideoSource']['error'] == 0) {
                        $videoarr['YoutubeCode'] = '';
                        $videoarr['VideoSource'] = $this->upload_image_single('VideoSource', 'video');
                    }
                }
                if (isset($_FILES['ImageVideo'])) {
                    if ($_FILES['ImageVideo']['error'] == 0) {
                        $videoarr['ImageVideo'] = $this->upload_image_single('ImageVideo');
                    }
                }
                $data['Video'] = json_encode($videoarr);
                $this->db->where('ID', $ID);

                $msg = array(
                    0 => 'Ngưng kích hoạt',
                    1 => 'Kích hoạt'
                );
//                echo "<pre>"
//                var_dump($data); exit();
                if ($this->db->update('ttp_report_products', $data)) {

//                    $this->cache->delete('DataProducts!' . substr(md5($this->user->ID . 'DataProducts'), 27, 06));
                    $notes = array();
                    if ($object_products->Title != $Title) {
                        $notes[] = "Title modified from: <font color='green'>" . $object_products->Title . "</font>";
                    }
                    if ($object_products->Description != $Description) {
                        $notes[] = "Short Description modified from <font color='green'>" . $object_products->Description . "</font>";
                    }
                    if ($object_products->Alias != $this->lib->alias($Title)) {
                        $notes[] = "Alias modified from <font color='green'>" . $object_products->Alias . "</font>";
                    }
                    if ($object_products->Price != $Price) {
                        $notes[] = "Price modified from <font color='red'>" . $object_products->Price . "</font>";
                    }
                    if ($object_products->Barcode != $Barcode) {
                        $notes[] = "Barcode modified from <font color='red'>" . $object_products->Barcode . "</font>";
                    }
                    if ($object_products->SocialPrice != $SocialPrice) {
                        $notes[] = "SocialPrice modified from <font color='red'>" . $object_products->SocialPrice . "</font>";
                    }
                    if ($object_products->Published != $Published) {
                        $notes[] = "Published modified from <font color='green'>" . $msg[$object_products->Published] . "</font>";
                    }
                    if ($object_products->Status != $Show) {
                        $notes[] = "Active modified from <font color='green'>" . $msg[$object_products->Status] . "</font>";
                    }

                    foreach ($notes as $item) {
                        $history = array(
                            'Created' => date('Y-m-d H:i:s'),
                            'ProductsID' => $ID,
                            'Type' => 'Update',
                            'Notes' => $item,
                            'Owner' => $this->user->ID
                        );
                        $this->db->insert('ttp_report_products_history', $history);
                    }
                }

                if ($VariantType == 0) {
                    $this->db->query("delete from ttp_report_images_products where ProductsID in(select ID from ttp_report_products where ParentID=$ID)");
//$this->db->query("delete from ttp_report_products where ParentID=$ID");
                }

                if (count($CategoriesID) > 0) {
                    $str_categories_map = array();
                    foreach ($CategoriesID as $row) {
                        $str_categories_map[] = "($ID,$row)";
                    }
                    $str_categories_map = implode(',', $str_categories_map);
                    $str_categories_map = "insert into ttp_report_products_categories(ProductsID,CategoriesID) values" . $str_categories_map;
                    $this->db->query("delete from ttp_report_products_categories where ProductsID=$ID");
                    $this->db->query($str_categories_map);
                }

                if (count($PromotionsID) > 0) {
                    $str_promotions_map = array();
                    foreach ($PromotionsID as $row) {
                        $str_promotions_map[] = "($ID,$row)";
                    }
                    $str_promotions_map = implode(',', $str_promotions_map);
                    $str_promotions_map = "insert into ttp_report_products_promotions(ProductsID,PromotionsID) values" . $str_promotions_map;
                    $this->db->query("delete from ttp_report_products_promotions where ProductsID=$ID");
                    $this->db->query($str_promotions_map);
                }

                if (count($PropertiesID) > 0) {
                    $compare = json_encode($VariantValue);
                    if ($object_products->VariantValue != $compare) {
                        $PropertiesID = implode(',', $PropertiesID);
                        $Properties = $this->db->query("select * from ttp_report_properties where ID in($PropertiesID)")->result();
                        if (count($Properties) > 0) {
                            $arr_sql = array();
                            $arr = array();
                            foreach ($Properties as $row) {
                                $checked = in_array($row->ParentID, $PropertiesPublished) ? 1 : 0;
                                if (!isset($arr[$row->ParentID])) {
                                    $arr_sql[] = "($ID,$row->ParentID,0,$checked)";
                                    $arr[$row->ParentID] = 1;
                                }
                                $arr_sql[] = "($ID,$row->ID,$row->ParentID,$checked)";
                            }
                            if (count($arr_sql) > 0) {
                                $this->db->query("delete from ttp_report_product_properties where ProductsID=$ID");
                                $arr_sql = "insert into ttp_report_product_properties(ProductsID,PropertiesID,ParentID,PublishedOnSite) values" . implode(',', $arr_sql);
                                $this->db->query($arr_sql);
                            }
                        }
                    }
                }

                $SaveAndExit = isset($_POST['SaveAndExit']) ? $_POST['SaveAndExit'] : 0;
                if ($SaveAndExit == 1) {
                    redirect(ADMINPATH . '/report/warehouse_products/');
                }

#Draft
                if (in_array($this->user->ID, isCollaborators())) {
                    $updateAssign = array(
                        'Status' => 2
                    );
                    $this->db->where('ProductID', $ID);
                    $this->db->where('Owner', $this->user->ID);
                    $this->db->update('auc_assign', $updateAssign);

                    $WaitApproval = isset($_POST['WaitApproval']) ? $_POST['WaitApproval'] : 0;
                    if ($WaitApproval == 1) {
                        #Allow Display Tol List Products
                        $data = array(
                            'LastEdited' => date('Y-m-d H:i:s'),
                        );
                        $this->db->where('ID', $ID);
                        $this->db->update('ttp_report_products', $data);

                        #START: Wait Approval
                        $updateAssign = array(
                            'Status' => 3,
                            'LastEdited' => date('Y-m-d H:i:s'),
                        );
                        $this->db->where('ProductID', $ID);
                        $this->db->where('Owner', $this->user->ID);
                        $this->db->update('auc_assign', $updateAssign);

                        $this->load->model('assign_model', 'assign');
                        $status_last = $this->assign->getStatus($ID)->Status;
                        $max_created = $this->assign->getMaxCreated($ID)->ID;

                        if ($status_last == 3) {
                            $history_update = array(
                                'Created' => date('Y-m-d H:i:s'),
                                'Owner' => $this->user->ID
                            );
                            $this->db->where('ProductsID', $ID);
                            $this->db->where('ID', $max_created);
                            $this->db->update('ttp_report_products_history', $history_update);
                        } else {
                            $history = array(
                                'Created' => date('Y-m-d H:i:s'),
                                'ProductsID' => $ID,
                                'Type' => 'Submit',
                                'Notes' => 'Wait Approval',
                                'Owner' => $this->user->ID
                            );
                            $this->db->insert('ttp_report_products_history', $history);
                        }#END: Wait Approval

                        redirect(ADMINPATH . '/report/warehouse_products/');
                    }
                }

                $currenttab = isset($_POST['currenttab']) ? $_POST['currenttab'] : '';
                $temptab = $currenttab;
                $currenttab = $currenttab == "tab8" && $VariantType == 0 ? "tab1" : $currenttab;
                $currenttab = $currenttab != '' ? '?tab=' . $currenttab : '';
                if ($temptab == 'tab3') {
                    $nutrition_value = isset($_POST['nutrition_value']) ? $_POST['nutrition_value'] : array();
                    $nutrition_percent = isset($_POST['nutrition_percent']) ? $_POST['nutrition_percent'] : array();
                    $nutrition = $this->db->query("select * from ttp_report_nutrition_facts")->result();
                    if (count($nutrition) > 0) {
                        $sql = array();
                        $i = 0;
                        foreach ($nutrition as $row) {
                            $nutrition_value_row = isset($nutrition_value[$i]) ? $nutrition_value[$i] : '';
                            $nutrition_percent_row = isset($nutrition_percent[$i]) ? $nutrition_percent[$i] : '';
                            $sql[] = "('$nutrition_value_row','$nutrition_percent_row',$ID,$row->ID)";
                            $i++;
                        }
                        if (count($sql) > 0) {
                            $this->db->query("delete from ttp_report_products_nutrition_facts where ProductsID=$ID");
                            $this->db->query("insert into ttp_report_products_nutrition_facts(Data,Percent,ProductsID,NutritionID) values" . implode(',', $sql));
                        }
                    }
                }
                if ($object_products->Price != $Price || $object_products->RootPrice != $RootPrice || $object_products->SocialPrice != $SocialPrice || $object_products->SpecialPrice != $SpecialPrice) {
                    $sql = array(
                        'ProductsID' => $ID,
                        'VAT' => $VAT,
                        'Price' => $Price,
                        'RootPrice' => $RootPrice,
                        'SocialPrice' => $SocialPrice,
                        'SpecialPrice' => $SpecialPrice,
//                        'SpecialStartday' => $Special_startday,
//                        'SpecialStopday' => $Special_stopday,
                        'Created' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('ttp_report_products_price_analytics', $sql);
                }
                $this->write_log("Update products from ttp_report_products where ID=" . $ID . "\n JSON = " . json_encode($data));
                redirect(ADMINPATH . '/report/warehouse_products/edit/' . $ID . $currenttab);
            }
        }
    }

    public function approved() {
        $ID = isset($_POST['ID']) ? trim($_POST['ID']) : 0;
        $token = isset($_POST['token']) ? trim($_POST['token']) : 0;
        $my_token = substr(md5($ID . 'approved'), 27, 06);
        if ($my_token == $token) {
            $this->load->model('assign_model', 'assign');
            if ($this->assign->updateStatus($ID, 5)) {
                $history = array(
                    'Created' => date('Y-m-d H:i:s'),
                    'ProductsID' => $ID,
                    'Type' => 'Approved',
                    'Notes' => '<font color="blue">OK</font>',
                    'Owner' => $this->user->ID
                );
                $this->db->insert('ttp_report_products_history', $history);
            }
            echo "OK";
        }
    }

    public function rejected() {
        $ID = isset($_POST['ID']) ? trim($_POST['ID']) : 0;
        $token = isset($_POST['token']) ? trim($_POST['token']) : 0;
        $msg = isset($_POST['msg']) ? trim($_POST['msg']) : "";
        $my_token = substr(md5($ID . 'rejected'), 27, 06);
        if ($my_token == $token) {
            $this->load->model('assign_model', 'assign');
            if ($this->assign->updateStatus($ID, 4)) {
                $history = array(
                    'Created' => date('Y-m-d H:i:s'),
                    'ProductsID' => $ID,
                    'Type' => 'Rejected',
                    'Notes' => '<font color="red">' . $msg . '</font>',
                    'Owner' => $this->user->ID
                );
                $this->db->insert('ttp_report_products_history', $history);
            }
            echo "OK";
        }
    }

    public function dupplicate($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $object = $this->db->query("select * from ttp_report_products where ID=$id")->row();
        if ($object) {
            $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
            $max = $max ? $max->ID + 1 : 1;
            $data = array(
                'ID' => $max,
                'Path' => $max,
                'TypeProducts' => $object->TypeProducts,
                'Content' => $object->Content,
                'Instruction' => $object->Instruction,
                'MaSP' => time(),
                'Title' => $object->Title . ' (Sao chép)',
                'Description' => $object->Description,
                'Weight' => $object->Weight,
                'Length' => $object->Length,
                'Width' => $object->Width,
                'Height' => $object->Height,
                'NewStartDay' => $object->NewStartDay,
                'NewStopDay' => $object->NewStopDay,
                'SpecialStartday' => $object->SpecialStartday,
                'SpecialStopday' => $object->SpecialStopday,
                'Status' => 0,
                'Published' => $object->Published,
                'CountryID' => $object->CountryID,
                'TrademarkID' => $object->TrademarkID,
                'VAT' => $object->VAT,
                'Chimmoi' => $object->Chimmoi,
                'CurrentAmount' => $object->CurrentAmount,
                'WarningAmount' => $object->WarningAmount,
                'MinCartAmount' => $object->MinCartAmount,
                'MaxCartAmount' => $object->MaxCartAmount,
                'PutcartNegative' => $object->PutcartNegative,
                'InventoryStatus' => $object->InventoryStatus,
                'Price' => $object->Price,
                'RootPrice' => $object->RootPrice,
                'SpecialPrice' => $object->SpecialPrice,
                'Donvi' => $object->Donvi,
                'CategoriesID' => $object->CategoriesID,
                'VariantValue' => $object->VariantValue,
                'VariantType' => $object->VariantType,
                'MetaTitle' => $object->MetaTitle,
                'MetaKeywords' => $object->MetaKeywords,
                'MetaDescription' => $object->MetaDescription,
                'AcceptGift' => $object->AcceptGift,
                'SaleGift' => $object->SaleGift,
                'SupplierID' => $object->SupplierID,
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s')
            );
            $IDstr = str_pad($object->ID, 8, '0', STR_PAD_LEFT);
//$data['Barcode'] = "603" . $object->TypeProducts . $IDstr . '1';
            $Alias = $this->lib->alias($object->Title . ' (Sao chép)');
            $data['Alias'] = $Alias;
            $check = $this->db->query('select ID from ttp_report_products where Alias="' . $Alias . '"')->row();
            if ($check) {
                $data['Alias'] .= $Alias . $max;
            }
            $this->db->insert('ttp_report_products', $data);
            $this->create_shipment_default($max);
            $PropertiesID = json_decode($object->VariantValue, true);
            $PropertiesID = $PropertiesID["Properties"];
            $PropertiesPublished = $PropertiesID["Published"];
            if (count($PropertiesID) > 0) {
                $PropertiesID = implode(',', $PropertiesID);
                $Properties = $this->db->query("select * from ttp_report_properties where ID in($PropertiesID)")->result();
                if (count($Properties) > 0) {
                    $arr_sql = array();
                    $arr = array();
                    foreach ($Properties as $row) {
                        $checked = in_array($row->ParentID, $PropertiesPublished) ? 1 : 0;
                        if (!isset($arr[$row->ParentID])) {
                            $arr_sql[] = "($max,$row->ParentID,0,$checked)";
                            $arr[$row->ParentID] = 1;
                        }
                        $arr_sql[] = "($max,$row->ID,$row->ParentID,$checked)";
                    }
                    if (count($arr_sql) > 0) {
                        $arr_sql = "insert into ttp_report_product_properties(ProductsID,PropertiesID,ParentID,PublishedOnSite) values" . implode(',', $arr_sql);
                        $this->db->query($arr_sql);
                    }
                }
            }
            $this->write_log("Duplicate products from ttp_report_products where ID=" . $max . "\n JSON = " . json_encode($data));
            redirect(ADMINPATH . '/report/warehouse_products/edit/' . $max);
        }
    }

    public function variant_add() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $result = $this->db->query("select * from ttp_report_products where ID=$ID")->row();
        if ($result) {
            $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
            $max = $max ? $max->ID + 1 : 1;
            $data = array(
                'ID' => $max,
                'Path' => $result->Path . '/' . $max,
                'MaSP' => $result->MaSP . "_" . $max,
                'TypeProducts' => $result->TypeProducts,
                'Title' => $result->Title,
                'Description' => $result->Description,
                'Weight' => $result->Weight,
                'Length' => $result->Length,
                'Width' => $result->Width,
                'Height' => $result->Height,
                'NewStartDay' => $result->NewStartDay,
                'NewStopDay' => $result->NewStopDay,
                'SpecialStartday' => $result->SpecialStartday,
                'SpecialStopday' => $result->SpecialStopday,
                'Status' => $result->Status,
                'Published' => $result->Published,
                'CountryID' => $result->CountryID,
                'TrademarkID' => $result->TrademarkID,
                'VAT' => $result->VAT,
                'CurrentAmount' => 0,
                'WarningAmount' => $result->WarningAmount,
                'MinCartAmount' => $result->MinCartAmount,
                'MaxCartAmount' => $result->MaxCartAmount,
                'PutcartNegative' => $result->PutcartNegative,
                'InventoryStatus' => $result->InventoryStatus,
                'Price' => $result->Price,
                'RootPrice' => $result->RootPrice,
                'SpecialPrice' => $result->SpecialPrice,
                'Donvi' => $result->Donvi,
                'CategoriesID' => $result->CategoriesID,
                'SupplierID' => $result->SupplierID,
                'VariantValue' => "",
                'VariantType' => 1,
                'ParentID' => $result->ID,
                'AcceptGift' => $result->AcceptGift,
                'SaleGift' => $result->SaleGift,
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s')
            );
            $IDstr = str_pad($result->ID, 8, '0', STR_PAD_LEFT);
//$data['Barcode'] = "603" . $result->TypeProducts . $IDstr . '1';
            $this->db->insert('ttp_report_products', $data);
            $this->create_shipment_default($max);
            $result1 = $this->db->query("select * from ttp_report_products where ID=$max")->row();
            $this->load->view('warehouse_products_variant_add', array('data' => $result1, 'parentdata' => $result));
            $this->write_log("add variant from ttp_report_products where ID=" . $max . "\n JSON = " . json_encode($data));
        }
    }

    public function update_variant() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? trim($_POST['ID']) : 0;
        $MaSP = isset($_POST['SKU']) ? trim($_POST['SKU']) : '';
        $VariantValue = isset($_POST['Properties']) ? trim($_POST['Properties']) : '';
        $VariantValue = explode("|", $VariantValue);
        if (isset($VariantValue[0])) {
            unset($VariantValue[0]);
        }
        $Description = isset($_POST['Description']) ? trim($_POST['Description']) : '';
        $Weight = isset($_POST['Weight']) ? trim($_POST['Weight']) : 0;
        $Length = isset($_POST['Length']) ? trim($_POST['Length']) : 0;
        $Width = isset($_POST['Width']) ? trim($_POST['Width']) : 0;
        $Height = isset($_POST['Height']) ? trim($_POST['Height']) : 0;
        $Published = isset($_POST['Published']) ? trim($_POST['Published']) : 0;
        $Available = isset($_POST['InventoryStatus']) ? trim($_POST['InventoryStatus']) : 0;
        $Price = isset($_POST['Price']) ? trim($_POST['Price']) : 0;
        $SpecialPrice = isset($_POST['SpecialPrice']) ? trim($_POST['SpecialPrice']) : '';
        $Special_startday = isset($_POST['StartSpecialPrice']) ? trim($_POST['StartSpecialPrice']) : '';
        $Special_stopday = isset($_POST['StopSpecialPrice']) ? trim($_POST['StopSpecialPrice']) : '';
        $Namebonus = isset($_POST['Namebonus']) ? trim($_POST['Namebonus']) : '';
        $data = array(
            'Title' => $Namebonus,
            'Alias' => $this->lib->alias($Namebonus),
            'MaSP' => $MaSP,
            'Description' => $Description,
            'Weight' => $Weight,
            'Length' => $Length,
            'Width' => $Width,
            'Height' => $Height,
            'Published' => $Published,
            'Status' => $Published,
            'InventoryStatus' => $Available,
            'Price' => $Price,
            'SpecialPrice' => $SpecialPrice,
            'VariantValue' => json_encode($VariantValue),
//            'SpecialStartday' => $Special_startday,
//            'SpecialStopday' => $Special_stopday,
            'LastEdited' => date('Y-m-d H:i:s')
        );
        $this->db->where('ID', $ID);
        $this->db->update('ttp_report_products', $data);
        $this->write_log("update variant from ttp_report_products where ID=" . $ID . "\n JSON = " . json_encode($data));
    }

    public function deletehistory($id = 0) {
        $ProductsID = $this->db->query("select ProductsID from ttp_report_products_history where ID=$id")->row()->ProductsID;
        $this->db->query("delete from ttp_report_products_history where ID=$id");
        $this->write_log("delete history from ttp_report_products_history where ID=" . $id);
        redirect(ADMINPATH . '/report/warehouse_products/edit/' . $ProductsID);
    }

    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        $checkorder = $this->db->query("select ProductsID from ttp_report_orderdetails where ProductsID=$id")->row();
        if (!$checkorder) {
            $this->db->query("delete from ttp_report_products where ID=$id");
            $this->db->query("delete from ttp_report_products where ParentID=$id");
            $this->db->query("delete from ttp_report_images_products where ProductsID=$id");
            $this->db->query("delete from ttp_report_images_products where ProductsID in(select ID from ttp_report_products where ParentID=$id)");
            $this->write_log("delete products from ttp_report_products where ID=" . $id);
            $this->write_log("delete products from ttp_report_products_ingredients where ProductsID=" . $id);
        }
        redirect(ADMINPATH . '/report/warehouse_products/');
    }

    public function delete_variant() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $checkorder = $this->db->query("select ProductsID from ttp_report_orderdetails where ProductsID=$ID")->row();
        if (!$checkorder) {
            $this->db->query("delete from ttp_report_products where ID=$ID");
            $this->db->query("delete from ttp_report_products where ParentID=$ID");
            $this->db->query("delete from ttp_report_images_products where ProductsID=$ID");
            $this->write_log("delete variant from ttp_report_products where ID=" . $ID);
        }
    }

    public function changeimagerow() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        if (isset($_FILES['file'])) {
            if ($_FILES['file']['error'] == 0) {
                $this->upload_to = "products/$ID";
                $data = $this->upload_image_single_report("ttp_report_images_products", array(), false, IMAGECROPS);
                if ($data != '') {
                    $this->write_log("change image to ttp_report_images_products where ProductsID=" . $ID);
                    $imagerow = $this->db->query("select ProductsID,Url from ttp_report_images_products where ID=$ID")->row();
                    if ($imagerow) {
                        $this->db->query("update ttp_report_images_products set Url='$data' where ID=$ID");
                        $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$imagerow->ProductsID order by STT ASC")->result();
                        if (count($data_image) > 0) {
                            $datachange = array();
                            $primary = "";
                            foreach ($data_image as $row) {
                                $primary = $row->PrimaryImage == 1 ? $row->Url : $primary;
                                $datachange[] = $row->Url;
                            }
                            $primary = $primary == '' ? $row->Url : $primary;
                            $datachange = json_encode($datachange);
                            $this->db->query("update ttp_report_products set AlbumImage='$datachange',PrimaryImage='$primary' where ID=$imagerow->ProductsID");
                        }
                        echo file_exists($this->lib->get_thumb($data)) ? $this->lib->get_thumb($data) : '';
                        return;
                    }
                }
            }
        }
        echo "False";
    }

    public function upload_image() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        if (isset($_FILES['file'])) {
            if ($_FILES['file']['error'] == 0) {
                $this->upload_to = date('Y') . "/" . date('m');
                $this->upload_image_single_report("ttp_report_images_products", array('ProductsID' => $ID, 'Primary' => 0), true, IMAGECROPS);
                $this->write_log("upload image to ttp_report_images_products where ProductsID=" . $ID);
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$ID order by STT ASC")->result();
                if (count($data_image) > 0) {
                    $data = array();
                    $primary = "";
                    foreach ($data_image as $row) {
                        $primary = $row->PrimaryImage == 1 ? $row->Url : $primary;
                        $data[] = $row->Url;
                    }
                    $primary = $primary == '' ? $row->Url : $primary;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$ID");
                }
                echo "OK";
            } else {
                echo "False";
            }
        } else {
            echo "False";
        }
    }

    public function delete_images() {
        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;
        if ($ID != '') {
            $image = $this->db->query("select ProductsID from ttp_report_images_products where ID=$ID")->row();
            if ($image) {
                $this->db->query("delete from ttp_report_images_products where ID=" . $ID);
                $this->write_log("delete from ttp_report_images_products where ID=" . $ID);
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$image->ProductsID order by STT ASC")->result();
                if (count($data_image) > 0) {
                    $data = array();
                    $primary = "";
                    foreach ($data_image as $row) {
                        $primary = $row->PrimaryImage == 1 ? $row->Url : $primary;
                        $data[] = $row->Url;
                    }
                    $primary = $primary == '' ? $row->Url : $primary;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$image->ProductsID");
                }
            }
        }
    }

    public function removefileondisc($Url) {
        @unlink($Url);
        $thumbfile = $this->get_thumb($Url);
        if (file_exists($thumbfile)) {
            @unlink($thumbfile);
        }
        $cropimage = explode(",", IMAGECROPS);
        if (count($cropimage) > 0) {
            $image = explode("/", $Url);
            $filename = count($image) > 0 ? $image[count($image) - 1] : $image[0];
            foreach ($cropimage as $key) {
                $size = explode("x", $key);
                $width = isset($size[0]) ? (int) $size[0] : 0;
                $height = isset($size[1]) ? (int) $size[1] : 0;
                $crop = $width . "x" . $height . "_" . $filename;
                $file = str_replace($filename, $crop, $Url);
                if (file_exists($file)) {
                    @unlink($file);
                }
            }
        }
    }

    public function delete_images_variant() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $listID = isset($_POST['listID']) ? $_POST['listID'] : array();
        if (count($listID) > 0) {
            $listID = json_decode($listID, true);
            $listID = implode(',', $listID);
            if ($listID != '') {
                $this->write_log("delete from ttp_report_images_products where ProductsID=" . $ID . " and ID in($listID)");
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$ID order by STT ASC")->result();
                if (count($data_image) > 0) {
                    $data = array();
                    $primary = "";
                    foreach ($data_image as $row) {
                        $primary = $row->PrimaryImage == 1 ? $row->Url : $primary;
                        $data[] = $row->Url;
                    }
                    $primary = $primary == '' ? $row->Url : $primary;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$ID");
                }
            }
        }
    }

    public function setprimary_images_variant() {
        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;
        $ImageID = isset($_POST['ImageID']) ? (int) $_POST['ImageID'] : 0;
        $this->db->query("update ttp_report_images_products set PrimaryImage=0,MiniImage=0,ThumbImage=0 where ProductsID=$ID");
        $this->db->query("update ttp_report_images_products set PrimaryImage=1,MiniImage=1,ThumbImage=1 where ID=$ImageID");
        $image = $this->db->query("select Url from ttp_report_images_products where ID=$ImageID")->row();
        if ($image) {
            $this->db->query("update ttp_report_products set PrimaryImage='$image->Url' where ID=$ID");
        }
        $this->write_log("update ttp_report_images_products set PrimaryImage=1,MiniImage=1,ThumbImage=1 where ID=$ImageID");
    }

    public function get_child_categories() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $result = $this->db->query("select * from ttp_report_categories where ParentID=$ID")->result();
        if (count($result) > 0) {
            echo "<ul>";
            foreach ($result as $row) {
                echo "<li><span onclick='show_child_next(this)' data='$row->ID'>+</span><input type='checkbox' value='$row->ID' onchange='check_full(this)' name='CategoriesID[]' /> $row->Title</li>";
            }
            echo "</ul>";
        } else {
            echo "False";
        }
    }

    public function add_properties() {
        $this->load->view("warehouse_add_properties");
    }

    public function add_value_properties() {
        $data = isset($_POST['Data']) ? $_POST['Data'] : 0;
        if ($data != '' && $data > 0) {
            $this->load->view("warehouse_add_properties", array('data' => $data));
        }
    }

    public function save_properties() {
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '';
        $ParentID = isset($_POST['ParentID']) ? $_POST['ParentID'] : 0;
        if ($Title != '' && $ParentID != '') {
            $path = $this->db->query("select Path from ttp_report_properties where ID=$ParentID")->row();
            $path = $path ? $path->Path : '';
            $max = $this->db->query("select max(ID) as ID from ttp_report_properties")->row();
            $max = $max ? $max->ID + 1 : 1;
            $data = array(
                'ID' => $max,
                'Title' => $Title,
                'ParentID' => $ParentID,
                'Published' => 1,
                'Path' => $ParentID > 0 ? $path . '/' . $max : $max
            );
            $data['Level'] = count(explode('/', $data['Path']));
            $this->db->insert("ttp_report_properties", $data);
            $ID = $this->db->insert_id();
            if ($ParentID == 0) {
                echo "OK|$ParentID|<option value='$ID'> $Title</option>";
            } else {
                echo "OK|$ParentID|<div class='col-xs-3'><input type='checkbox' value='$ID' /> $Title</div>";
            }
        } else {
            echo "False|False|False";
        }
    }

    public function add_newrow_properties() {
        $ID = isset($_POST['Data']) ? $_POST['Data'] : '';
        if ($ID != '') {
            $data = $this->db->query("select * from ttp_report_properties where ID=$ID")->row();
            if ($data) {
                $this->load->view("admin/warehouse_properties_addnewrow", array('data' => $data));
                return;
            }
        }
        echo "False";
    }

    public function row_image_update() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '';
        $Label = isset($_POST['Label']) ? $_POST['Label'] : '';
        $STT = isset($_POST['STT']) ? $_POST['STT'] : '';
        $Primary = isset($_POST['Primary']) ? $_POST['Primary'] : 0;
        $Small = isset($_POST['Small']) ? $_POST['Small'] : 0;
        $Thumb = isset($_POST['Thumb']) ? $_POST['Thumb'] : 0;
        if ($ID != '') {
            $data = array(
                'Label' => $Label,
                'STT' => $STT,
                'PrimaryImage' => $Primary,
                'MiniImage' => $Small,
                'ThumbImage' => $Thumb,
            );
            $this->db->where("ID", $ID);
            $this->db->update("ttp_report_images_products", $data);
            $image = $this->db->query("select ProductsID from ttp_report_images_products where ID=$ID")->row();
            if ($image) {
                $data_image = $this->db->query("select * from ttp_report_images_products where ProductsID=$image->ProductsID order by STT ASC")->result();
                if (count($data_image) > 0) {
                    $data = array();
                    $primary = "";
                    foreach ($data_image as $row) {
                        $primary = $row->PrimaryImage == 1 ? $row->Url : $primary;
                        $data[] = $row->Url;
                    }
                    $primary = $primary == '' ? $row->Url : $primary;
                    $data = json_encode($data);
                    $this->db->query("update ttp_report_products set AlbumImage='$data',PrimaryImage='$primary' where ID=$image->ProductsID");
                }
            }
            echo "OK";
        } else {
            echo "False";
        }
    }

    public function load_fillter_by_type_and_field() {
        $field = isset($_POST['FieldName']) ? $_POST['FieldName'] : '';
        if ($field == 'trademark') {
            $result = $this->db->query("select * from ttp_report_trademark order by Title ASC")->result();
            if (count($result) > 0) {
                echo "<select name='FieldText[]' class='form-control'>";
                foreach ($result as $row) {
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
                echo "</select>";
            }
        } elseif ($field == 'bird' || $field == 'status') {
            echo "<select name='FieldText[]' class='form-control'>";
            echo "<option value='1'>Yes</option>";
            echo "<option value='0'>No</option>";
            echo "</select>";
        } elseif ($field == 'variant') {
            echo "<select name='FieldText[]' class='form-control'>";
            echo "<option value='0'>Sản phẩm đơn</option>";
            echo "<option value='1'>Sản phẩm biến thể</option>";
            echo "<option value='2'>Sản phẩm Bundle</option>";
            echo "<option value='3'>Sản phẩm nhóm</option>";
            echo "</select>";
        } elseif ($field == 'typeproducts') {
            echo "<select name='FieldText[]' class='form-control'>";
            $productype = $this->lib->get_config_define("products", "productstype", 1, "code");
            foreach ($productype as $row) {
                echo "<option value='$row->code'>$row->name</option>";
            }
            echo "</select>";
        } else {
            echo '<input type="text" name="FieldText[]" id="textsearch" class="form-control" />';
        }
    }

    public function setfillter() {
        $arr_fieldname = array(0 => "a.MaSP", 1 => "a.Title", 2 => "b.ID", 3 => "a.Price", 4 => "a.CurrentAmount", 5 => "a.Chimmoi", 6 => "a.VariantType", 7 => "a.Status", 8 => "a.TypeProducts", 9 => "a.Owner");
        $arr_oparation = array(0 => 'like', 1 => '=', 2 => '!=', 3 => '>', 4 => '<', 5 => '>=', 6 => '<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array();
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array();
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array();
        $str = array();
        if (count($FieldName) > 0) {
            $i = 0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if (!in_array($value, $arrtemp)) {
                    if (isset($arr_fieldname[$value]) && isset($FieldOparation[$i])) {
                        if (isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])) {
                            if ($FieldText[$i] != '') {
                                if ($arr_oparation[$FieldOparation[$i]] == 'like') {
                                    $str[] = $arr_fieldname[$value] . ' ' . $arr_oparation[$FieldOparation[$i]] . " '%" . mysql_real_escape_string($FieldText[$i]) . "%'";
                                } else {
                                    $str[] = $arr_fieldname[$value] . ' ' . $arr_oparation[$FieldOparation[$i]] . " '" . mysql_real_escape_string($FieldText[$i]) . "'";
                                }
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if (count($str) > 0) {
                $sql = implode(' and ', $str);
                $this->session->set_userdata("fillter_products", $sql);
            } else {
                $this->session->set_userdata("fillter_products", "");
            }
        } else {
            $this->session->set_userdata("fillter_products", "");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        redirect($referer);
    }

    public function filterByOwner() {
        $Owner = isset($_POST['Owner']) ? $_POST['Owner'] : 0;
        $this->session->set_userdata("fillter_owner", $Owner);
        $this->session->set_userdata("fillter_products", "a.Owner = $Owner");
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
        redirect($referer);
    }

    public function collaboratorsStatus() {
        $Code = isset($_REQUEST['Code']) ? $_REQUEST['Code'] : -1;
        $where = array(
            'code' => $Code,
            'group' => 'products',
            'type' => 'collaborators_status'
        );
        $status_define = $this->lib->getFieldsByValue('name', $where);
        $this->session->set_userdata("Collaborators", $status_define->name);
        if ($Code == 2) {
            $this->session->set_userdata("Assign", FALSE);
        } else {
            $this->session->set_userdata("Assign", TRUE);
        }
    }

    public function RemoveCache() {
        $params = array(
            'DataProducts',
            'PhotoReady',
            'AssignData',
            'getDataAssign',
            'getDataImport',
            'UserCollaborators',
        );
        foreach ($params as $cache) {
            $this->cache->delete($cache . '!' . substr(md5($this->user->ID . $cache), 27, 06));
        }
    }

    public function showAll() {
        $this->session->unset_userdata('limit');
        $this->session->unset_userdata('Collaborators');
        $this->session->unset_userdata('fillter_products');
        $this->session->set_userdata('Assign', TRUE);
        $this->RemoveCache();
        redirect(ADMINPATH . "/report/warehouse_products");
    }

    public function changeLimit() {
        $new_limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : -1;
        if ($new_limit) {
            $this->session->set_userdata("limit", $new_limit);
        }
        if ($this->session->userdata("Assign", FALSE)) {
            $this->session->set_userdata("Assign", TRUE);
        } else {
            $this->session->set_userdata("Assign", FALSE);
        }
    }

    public function assign() {
        $Code = 2; //Photo Ready
        $where = array(
            'code' => $Code,
            'group' => 'products',
            'type' => 'collaborators_status'
        );
        $status_define = $this->lib->getFieldsByValue('name', $where);
        $this->session->set_userdata("Collaborators", $status_define->name);
        $this->session->set_userdata("Assign", FALSE);
    }

    public function applyAssign() {
        $selected = isset($_REQUEST['selected']) ? $_REQUEST['selected'] : '[]';
        $owner = isset($_REQUEST['owner']) ? $_REQUEST['owner'] : 0;

        $data = explode(",", $selected);
        foreach ($data as $item) {
            $this->db->where('ProductID', $item);
            $query = $this->db->get("auc_assign");
            if ($query->num_rows() > 0) {
                continue;
            } else {
                $insert = array(
                    'ProductID' => $item,
                    'From' => $this->user->ID,
                    'Owner' => $owner,
                    'Assigned' => date('Y-m-d H:i:s'),
                    'Status' => 2
                );
                if ($selected != 0 && $owner != 0) {
                    $this->db->insert('auc_assign', $insert);
                    $this->RemoveCache();
                }
            }
        }
        echo $owner;
//$this->session->set_userdata("Assign", FALSE);
    }

    public function set_barcode($code) {
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        Zend_Barcode::render('code128', 'image', array('text' => $code), array());
    }

    public function export_products() {
        if ($this->user->IsAdmin == 1 || $this->user->UserType == 6) {
            $TypeExport = isset($_POST['TypeExport']) ? $_POST['TypeExport'] : 0;
            $day_export = isset($_POST['Export_date']) ? $_POST['Export_date'] : date('Y-m-d', time());
            $daystop_export = isset($_POST['ExportStop_date']) ? $_POST['ExportStop_date'] : date('Y-m-d', time());
            if ($day_export != '') {
                if ($TypeExport == 1) {
                    $this->export_barcode($day_export, $daystop_export);
                }
            }
        }
    }

    public function export_barcode($day_export, $daystop_export) {
        $result = $this->db->query("select a.ID,a.MaSP,a.SocialPrice,a.Title,a.Donvi,a.BarcodeClient,a.RootPrice,a.Price,a.BasePrice,a.SpecialPrice,a.SpecialStopday,a.SpecialStartday,a.CategoriesID,b.Title as TrademarkTitle,c.Title as SupplierTitle,a.VariantType,a.TypeProducts,a.Status,a.PrintPriceList from ttp_report_products a,ttp_report_trademark b,ttp_report_production c where a.TrademarkID=b.ID and a.SupplierID=c.ID and date(a.Created)>='$day_export' and date(a.Created)<='$daystop_export'")->result();
        if (count($result) > 0) {
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                    ->setLastModifiedBy("Maarten Balliauw")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'SystemID')
                    ->setCellValue('B1', 'UCC_ProductID')
                    ->setCellValue('C1', 'Tên sản phẩm')
                    ->setCellValue('D1', 'Đơn vị tính')
                    ->setCellValue('E1', 'Nhà Cung Cấp')
                    ->setCellValue('F1', 'Thương hiệu')
                    ->setCellValue('G1', 'Mã số NCC')
                    ->setCellValue('H1', 'Ngành hàng cấp 1')
                    ->setCellValue('I1', 'Ngành hàng cấp 2')
                    ->setCellValue('J1', 'Ngành hàng cấp 3')
                    ->setCellValue('K1', 'Giá mua vào')
                    ->setCellValue('L1', 'Giá bán ra')
                    ->setCellValue('M1', 'Giá ĐV chuẩn')
                    ->setCellValue('N1', 'Giá đặc biệt')
                    ->setCellValue('O1', 'Ngày bắt đầu giá đặt biệt')
                    ->setCellValue('P1', 'Ngày kết thúc giá đặt biệt')
                    ->setCellValue('Q1', 'Loại sản phẩm')
                    ->setCellValue('R1', 'Loại thực phẩm')
                    ->setCellValue('S1', 'Trạng thái hiển thị')
                    ->setCellValue('T1', 'Trạng thái in')
                    ->setCellValue('U1', 'Giá thị trường');
            $arr_products = array(0 => "Sản phẩm đơn", 1 => "Sản phẩm biến thể", 2 => "Sản phẩm combo", 3 => "Sản phẩm nhóm");
            $products_type = $this->lib->get_config_define("products", "productstype", 1, "code");
            $arr_products_type = array();
            if (count($products_type) > 0) {
                foreach ($products_type as $row) {
                    $row->code = (int) $row->code;
                    $arr_products_type[$row->code] = $row->name;
                }
            }
            $categories = $this->db->query("select Title,Path,ID from ttp_report_categories")->result();
            $arr_categories = array();
            if (count($categories) > 0) {
                foreach ($categories as $row) {
                    $level = explode('/', $row->Path);
                    $arr_categories[$row->ID]['level'] = count($level);
                    $arr_categories[$row->ID]['title'] = $row->Title;
                }
            }
            $i = 2;
            foreach ($result as $row) {
                $json_categories = json_decode($row->CategoriesID);
                $row_level1 = "";
                $row_level2 = "";
                $row_level3 = "";
                if (count($json_categories) > 0) {
                    foreach ($json_categories as $value) {
                        if (isset($arr_categories[$value]['level'])) {
                            if ($arr_categories[$value]['level'] == 1) {
                                $row_level1 = $arr_categories[$value]['title'];
                            }
                            if ($arr_categories[$value]['level'] == 2) {
                                $row_level2 = $arr_categories[$value]['title'];
                            }
                            if ($arr_categories[$value]['level'] == 3) {
                                $row_level3 = $arr_categories[$value]['title'];
                            }
                        }
                    }
                }
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $row->ID)
                        ->setCellValue('B' . $i, $row->MaSP)
                        ->setCellValue('C' . $i, $row->Title)
                        ->setCellValue('D' . $i, $row->Donvi)
                        ->setCellValue('E' . $i, $row->SupplierTitle)
                        ->setCellValue('F' . $i, $row->TrademarkTitle)
                        ->setCellValue('G' . $i, $row->BarcodeClient)
                        ->setCellValue('H' . $i, $row_level1)
                        ->setCellValue('I' . $i, $row_level2)
                        ->setCellValue('J' . $i, $row_level3)
                        ->setCellValue('K' . $i, $row->RootPrice)
                        ->setCellValue('L' . $i, $row->Price)
                        ->setCellValue('M' . $i, $row->BasePrice)
                        ->setCellValue('N' . $i, $row->SpecialPrice)
                        ->setCellValue('O' . $i, $row->SpecialStartday)
                        ->setCellValue('P' . $i, $row->SpecialStopday)
                        ->setCellValue('Q' . $i, $arr_products[$row->VariantType])
                        ->setCellValue('R' . $i, $arr_products_type[$row->TypeProducts])
                        ->setCellValue('S' . $i, $row->Status)
                        ->setCellValue('T' . $i, $row->PrintPriceList)
                        ->setCellValue('U' . $i, $row->SocialPrice);
                $i++;
            }
            $objPHPExcel->setActiveSheetIndex(0)->setTitle("PRODUCTS");
            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->setActiveSheetIndex(1);
            $sheet->setTitle("ProductType");
            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Tên loại sản phẩm');
            $i = 2;
            foreach ($arr_products as $key => $row) {
                $objPHPExcel->setActiveSheetIndex(1)
                        ->setCellValue('A' . $i, $key)
                        ->setCellValue('B' . $i, $row);
                $i++;
            }

            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->setActiveSheetIndex(2);
            $sheet->setTitle("Categories");
            $i = 2;
            $objPHPExcel->setActiveSheetIndex(2)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Tên ngành hàng')
                    ->setCellValue('C1', 'Cấp ngành hàng');
            foreach ($categories as $row) {
                $level = explode('/', $row->Path);
                $objPHPExcel->setActiveSheetIndex(2)
                        ->setCellValue('A' . $i, $row->ID)
                        ->setCellValue('B' . $i, $row->Title)
                        ->setCellValue('C' . $i, count($level));
                $i++;
            }

            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->setActiveSheetIndex(3);
            $sheet->setTitle("Supplier");
            $i = 2;
            $objPHPExcel->setActiveSheetIndex(3)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Nhà cung cấp');
            $Supplier = $this->db->query("select * from ttp_report_production")->result();
            foreach ($Supplier as $row) {
                $objPHPExcel->setActiveSheetIndex(3)
                        ->setCellValue('A' . $i, $row->ID)
                        ->setCellValue('B' . $i, $row->Title);
                $i++;
            }

            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->setActiveSheetIndex(4);
            $sheet->setTitle("Brand");
            $i = 2;
            $objPHPExcel->setActiveSheetIndex(4)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Thương hiệu');
            $Brand = $this->db->query("select ID,Title from ttp_report_trademark")->result();
            foreach ($Brand as $row) {
                $objPHPExcel->setActiveSheetIndex(4)
                        ->setCellValue('A' . $i, $row->ID)
                        ->setCellValue('B' . $i, $row->Title);
                $i++;
            }

            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->setActiveSheetIndex(5);
            $sheet->setTitle("ProductType");
            $objPHPExcel->setActiveSheetIndex(5)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Tên loại hàng hóa');
            $i = 2;

            foreach ($products_type as $row) {
                $objPHPExcel->setActiveSheetIndex(5)
                        ->setCellValue('A' . $i, $row->code)
                        ->setCellValue('B' . $i, $row->name);
                $i++;
            }


            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BC_PRODUCTS.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        } else {
            echo "Data is empty !. Can't export data .";
        }
    }

    public function get_products_list() {
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '';
        $result = $this->db->query("select ID,Title,Price,MaSP,Published,Status from ttp_report_products where (VariantType=0 or (VariantType=1 and ParentID>0)) and (Title like '%$Title%' or MaSP = '$Title') order by ID limit 0,100")->result();
        if (count($result) > 0) {
            $str = "<div class='tools_search_products'>
                        <span>Tìm kiếm sản phẩm: </span>
                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' onchange='input_search_products(this)' /></span>
                    </div>
                    <table><tr><th>STT</th><th>Mã Sản phẩm</th><th>Sản phẩm</th><th>Kích hoạt</th><th>Hiển thị</th></tr>";
            $i = 1;
            foreach ($result as $row) {
                $published = $row->Published == 1 ? '<span class="label label-success">Enabled</span>' : '<span class="label label-danger">Disable</span>';
                $status = $row->Status == 1 ? '<span class="label label-success">Enabled</span>' : '<span class="label label-danger">Disable</span>';
                $str .= "<tr>";
                $str .= "<td>$i</td>";
                $str .= "<td style='width: 130px;'><a onclick='addproductstobundle(this,$row->ID)'>" . $row->MaSP . "</a></td>";
                $str .= "<td><a onclick='addproductstobundle(this,$row->ID)'>" . $row->Title . "</a></td>";
                $str .= "<td>$published</td>";
                $str .= "<td>$status</td>";
                $str .= "</tr>";
                $i++;
            }
            echo $str . "</table>";
        } else {
            echo "FALSE";
        }
    }

    public function addproductstobundle() {
        $BundleID = isset($_POST['BundleID']) ? (int) $_POST['BundleID'] : 0;
        $ProductsID = isset($_POST['ProductsID']) ? (int) $_POST['ProductsID'] : 0;
        $checkorder = $this->db->query("select count(ID) as SL from ttp_report_orderdetails where ProductsID=$BundleID")->row();
        if ($checkorder->SL == 0) {
            $products = $this->db->query("select Price from ttp_report_products where ID=$ProductsID")->row();
            $price = $products ? $products->Price : 0;
            $data = array(
                'Des_ProductsID' => $ProductsID,
                'Src_ProductsID' => $BundleID,
                'Quantity' => 1,
                'MusHave' => 1,
                'Price' => $price
            );
            $this->db->insert('ttp_report_products_bundle', $data);
            $this->write_log("insert ttp_report_products_bundle \n   'Des_ProductsID'    => $ProductsID \n   'Src_ProductsID'    => $BundleID \n   'Quantity'          => 1");
            echo "OK";
        } else {
            echo "FALSE1";
        }
    }

    public function removeoutbundle() {
        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;
        $object = $this->db->query("select * from ttp_report_products_bundle where ID=$ID")->row();
        if ($object) {
            $checkorder = $this->db->query("select count(ID) as SL from ttp_report_orderdetails where ProductsID=$object->Src_ProductsID")->row();
            if ($checkorder->SL == 0) {
                $this->db->query("delete from ttp_report_products_bundle where ID=$ID");
                $this->write_log("delete bundle products where ID=$ID (ttp_report_products_bundle)");
                echo "OK";
            } else {
                echo "FALSE1";
            }
        } else {
            echo "FALSE";
        }
    }

    public function updatequantitybundle() {
        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;
        $Quantity = isset($_POST['Quantity']) ? $_POST['Quantity'] : 1;
        $object = $this->db->query("select * from ttp_report_products_bundle where ID=$ID")->row();
        if ($object) {
            $checkorder = $this->db->query("select count(ID) as SL from ttp_report_orderdetails where ProductsID=$object->Src_ProductsID")->row();
            if ($checkorder->SL == 0) {
                $this->db->query("update ttp_report_products_bundle set Quantity=$Quantity where ID=$ID");
                $this->write_log("update bundle products set Quantity=$Quantity where ID=$ID (ttp_report_products_bundle)");
                echo "OK";
            } else {
                echo "FALSE1";
            }
        } else {
            echo "FALSE";
        }
    }

    public function updatepricebundle() {
        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;
        $Price = isset($_POST['Price']) ? $_POST['Price'] : 0;
        $object = $this->db->query("select * from ttp_report_products_bundle where ID=$ID")->row();
        if ($object) {
            $this->db->query("update ttp_report_products_bundle set Price=$Price where ID=$ID");
            $this->write_log("update bundle products set Price=$Price where ID=$ID (ttp_report_products_bundle)");
            echo "OK";
        } else {
            echo "FALSE";
        }
    }

    public function get_products_bundle_list($ID = 0) {
        $ID = (int) $ID;
        $result = $this->db->query("select b.*,a.Quantity,a.ID as BundleID,a.Price as PriceCombo,a.MusHave from ttp_report_products_bundle a,ttp_report_products b where Src_ProductsID=$ID and a.Des_ProductsID=b.ID")->result();
        if (count($result) > 0) {
            foreach ($result as $row) {
                $active = $row->MusHave == 1 ? "active" : "";
                echo '<div class="col-xs-4">
                        <div class="form-group panel panel-default" style="overflow:hidden">
                            <div class="col-xs-3"><img class="img-responsive" src="' . $row->PrimaryImage . '" style="width:90%;" />
                            <a class="' . $active . '" onclick="mushave(this,' . $row->MusHave . ',' . $row->BundleID . ')"><i class="fa fa-check-square" aria-hidden="true"></i> <br>Bắt buộc</a>
                            </div>,' . $row->ID . '
                            <div class="col-xs-9">
                                <p>' . $row->Title . '</p>
                                <p class="text-danger">' . number_format($row->Price) . 'đ</p>
                                <div class="col-xs-3"><input class="form-control" type="number" value="' . $row->Quantity . '" onchange="updatequantitybundle(this,' . $row->BundleID . ')" /></div>
                                <div class="col-xs-6" style="margin-left: 5px;">
                                    <input type="number" value="' . $row->PriceCombo . '" class="form-control" onchange="updatepricebundle(this,' . $row->BundleID . ')" />
                                </div>
                                <div class="col-xs-2"><a class="pull-right btn btn-default" title="Loại bỏ sản phẩm này ra khỏi bộ bundle" onclick="removeoutbundle(this,' . $row->BundleID . ')"><i class="fa fa-times"></i></a></div>
                            </div>
                        </div>
                    </div>';
            }
        }
    }

    public function changemushave() {
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        $MusHave = isset($_POST['MusHave']) ? $_POST['MusHave'] : 0;
        $check = $this->db->query("update ttp_report_products_bundle set MusHave=$MusHave where ID=$ID");
    }

    public function buffer_data_to_products() {
        $max = $this->db->query("select Max(ID) as ID from ttp_report_products")->row();
        $max = $max ? $max->ID + 1 : 1;
        $alias = $this->lib->alias($_POST['Title']);
        $data = array(
            'ID' => $max,
            'Title' => $_POST['Title'],
            'MetaTitle' => $_POST['Title'],
            'Alias' => $alias,
            'Description' => $_POST['Description'],
            'MetaDescription' => $_POST['Description'],
            'VariantType' => 3,
            'Published' => 1,
            'Content' => $_POST['Content'],
            'Instruction' => $_POST['Cals'],
            'MaSP' => time(),
            'TrademarkID' => 1,
            'TrademarkID' => 1,
            'Path' => $max,
            'CategoriesID' => '["66"]',
            'Created' => date('Y-m-d H:i:s'),
            'LastEdited' => date('Y-m-d H:i:s')
        );
        if ($_POST['Title'] != '') {
            $IDstr = str_pad($max, 8, '0', STR_PAD_LEFT);
//$data['Barcode'] = "6030" . $IDstr . '1';
            $this->db->insert("ttp_report_products", $data);
            $id = $this->db->insert_id();
            if ($_POST['Thumb'] != '') {
                $state = $this->insert_image_download($id, $_POST['Thumb'], $alias);
                if ($state != 'false') {
                    $this->db->query("update ttp_report_products set PrimaryImage='$state',AlbumImage='[$state]' where ID=$id");
                }
            }
            $Ingredients = isset($_POST['Ingredients']) ? $_POST['Ingredients'] : array();
            if (count($Ingredients) > 0) {
                $arr = array();
                foreach ($Ingredients as $row) {
                    $arr[] = "('$row',$id)";
                }
                if (count($arr) > 0) {
                    $arr = "insert into ttp_report_products_ingredients(Title,ProductsID) values" . implode(',', $arr);
                    $this->db->query($arr);
                }
            }
        }
        redirect(ADMINPATH . "/report/warehouse_products");
    }

    public function insert_image_download($ID = 0, $thumb = '', $title = '') {
        $this->upload_to = date('Y') . "/" . date('m');
        $this->make_folder($this->upload_to, 'assets/');
        $ext = explode('.', $thumb);
        $ext = $ext[count($ext) - 1];
        $image = @file_get_contents($thumb);
        if ($image != '') {
            if (file_put_contents("assets/" . $this->upload_to . '/' . $title . '.' . $ext, $image)) {
                $data = array(
                    'Name' => $title,
                    'Label' => $title,
                    'Created' => date('Y-m-d H:i:s'),
                    'STT' => 1,
                    'Published' => 1,
                    'ProductsID' => $ID,
                    'PrimaryImage' => 1,
                    'Url' => "assets/" . $this->upload_to . '/' . $title . '.' . $ext
                );
                $this->db->insert('ttp_report_images_products', $data);
                $this->created_thumb_single("assets/" . $this->upload_to . '/' . $title . '.' . $ext);
                $cropimage = explode(",", IMAGECROP);
                foreach ($cropimage as $row) {
                    $size = explode("x", $row);
                    $width = isset($size[0]) ? (int) $size[0] : 0;
                    $height = isset($size[1]) ? (int) $size[1] : 0;
                    if ($width > 0 && $height > 0)
                        $this->lib->cropimage("assets/" . $this->upload_to . '/' . $title . '.' . $ext, $width, $height);
                }
                return "assets/" . $this->upload_to . '/' . $title . '.' . $ext;
            }
        }
        return "false";
    }

    public function changeIngredients() {
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '';
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;
        if ($Title != '') {
            $data = array("Title" => $Title);
            $this->db->where("ID", $ID);
            $this->db->update("ttp_report_products_ingredients", $data);
        }
    }

    /*
     * ***************************************************
     *   Bots get data from site feedy.vn               *
     *   $link : Destination link to get data           *
     *   $data : Code setting to get data from this site*
     *                                                  *
     * ***************************************************
     */

    public function allrecipes() {
        $link = isset($_POST['Link']) ? $_POST['Link'] : '';
        if ($link != '') {
            include_once('application/libraries/simple_html_dom.php');
            $output = $this->crawl_apply($link);
            $html = str_get_html($output);
            echo "<form action='" . base_url() . ADMINPATH . "/report/warehouse_products/buffer_data_to_products" . "' method='post'><div class='col-xs-12' style='background:#f8f8f8;border:1px solid #f1f1f1;padding-bottom:20px;'><h4>Kết quả lấy nội dung</h4>";
            $Title = $html->find("h1.recipe-summary__h1", 0) ? trim($html->find("h1.recipe-summary__h1", 0)->innertext) : "";
            echo $Title;
            echo "<input type='hidden' name='Title' value='$Title' />";
            echo "<hr>";
            $Thumb = $html->find(".rec-photo", 0) ? $html->find(".rec-photo", 0)->src : "";
            echo "<input type='hidden' name='Thumb' value='$Thumb' />";
            $Description = $html->find(".submitter__description", 0) ? trim($html->find(".submitter__description", 0)->innertext) : "";
            $Description = str_replace('"', '', $Description);
            echo '<input type="hidden" name="Description" value="' . $Description . '" />';
            echo "<div class='col-xs-2'><img src='$Thumb' style='width:100%' /></div><div class='col-xs-10'>" . $Description . "</div>";
            echo "<div class='col-xs-12'><hr>";
            $Line = $html->find(".checkList__line");
            foreach ($Line as $list) {
                $row = trim(str_replace('ADVERTISEMENT ', '', $list->plaintext));
                $row = trim(str_replace('Add all ingredients to list', '', $row));
                echo "<div class='col-xs-3'>" . $row . "</div>";
                if ($row != '') {
                    echo "<input type='hidden' name='Ingredients[]' value='$row' />";
                }
            }
            echo "<div class='col-xs-12'><hr>";
            $content = $html->find(".recipe-directions__list--item");
            $valuecontent = '';
            foreach ($content as $list) {
                $row = trim($list->plaintext);
                echo "<p>$row</p>";
                if ($row != '') {
                    $valuecontent .= "<p>$row</p>";
                }
            }
            echo "<input type='hidden' name='Content' value='$valuecontent' />";
            echo "</div>";
            $readytime = trim($html->find(".ready-in-time__container", 0)->plaintext);
            $servings_count = trim($html->find("#servings", 0)->value);
            $servings = trim($html->find(".servings-count", 0)->plaintext);
            $servings = trim($servings_count . ' ' . $servings);
            $calorie = trim($html->find(".calorie-count", 0)->plaintext);
            echo "<input type='hidden' name='Readytime' value='$readytime' />";
            echo "<input type='hidden' name='Servings' value='$servings' />";
            echo "<input type='hidden' name='Calorie' value='$calorie' />";
            echo "<div class='col-xs-12'><hr>$readytime - $servings - $calorie<hr></div>";
            $recipe = $html->find(".nutrientLine");
            $cals = "";
            foreach ($recipe as $list) {
                $row = trim($list->innertext);
                echo "<div class='col-xs-3'>$row</div>";
                $cals .= $row;
            }
            echo "<input type='hidden' name='Cals' value='$cals' />";
            echo "</div><div class='col-xs-12'><hr><button type='submit' class='btn btn-success'><i class='fa fa-check-square'></i> Lưu thông tin</button><a class='btn btn-danger' style='margin-left:10px' onclick='backandnext()'><i class='fa fa-times'></i> Không lưu</a></div></div></form>";
        } else {
            echo "<div class='col-xs-12'>Không lấy được thông tin từ nguồn này .</div>";
        }
    }

    /*
     * ***************************************************
     *   Setting Bots before get data from link         *
     *   $link : Destination link to get data           *
     *   $https : security mode of destanation resource *
     *                                                  *
     * ***************************************************
     */

    public function crawl_apply($link = "", $https = false) {
        $setting = $this->db->query("select Data from ttp_crawler_setting limit 0,1")->row();
        if ($setting) {
            $data = json_decode($setting->Data, true);
            $ckfile = tempnam("/tmp", "CURLCOOKIE");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $link);
            curl_setopt($ch, CURLOPT_USERAGENT, $data['USERAGENT']);
            curl_setopt($ch, CURLOPT_REFERER, $data['REFERER']);
            curl_setopt($ch, CURLOPT_TIMEOUT, $data['TIMEOUT']);
            $UseProxy = isset($data['UseProxy']) ? $data['UseProxy'] : 0;
            if ($UseProxy == 1) {
                curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
                curl_setopt($ch, CURLOPT_PROXY, $data['PROXYIP'] . ":" . $data['PROXYPORT']);
                curl_setopt($ch, CURLOPT_PROXY, $data['PROXYIP'] . ":" . $data['PROXYPORT']);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $data['PROXYUSER'] . ":" . $data['PROXYPASSWORD']);
            }
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $https);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            return $output;
        }
        return "";
    }

    public function transfer_assets_site($json = "", $site = "") {
        if ($json != "") {
            $data = array(
                'USERAGENT' => 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
                'REFERER' => 'http://tools.ucancook.vn/',
                'TIMEOUT' => 100
            );
            $field = array(
                'key' => sha1($this->key),
                'json' => $json,
                'site' => $site
            );
            $ckfile = tempnam("/tmp", "CURLCOOKIE");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->mediasite . '?trans=true');
            curl_setopt($ch, CURLOPT_USERAGENT, $data['USERAGENT']);
            curl_setopt($ch, CURLOPT_REFERER, $data['REFERER']);
            curl_setopt($ch, CURLOPT_TIMEOUT, $data['TIMEOUT']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($field));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $output = curl_exec($ch);
            if ($output == "true") {
                echo "OK";
            } else {
                echo "False";
            }
        }
    }

    public function import_products() {
        $file = $this->Upload_xls();
        if (file_exists($file)) {
            $result = array();
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $title = "Nhập thông tin sản phẩm";
            $rest = 0;
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 1; $row <= $highestRow; $row ++) {
                    $arrcol = array();
                    for ($col = 0; $col <= $highestColumnIndex - 1; $col++) {
                        $arrcol[] = trim($worksheet->getCellByColumnAndRow($col, $row)->getValue());
                    }
                    $result[] = $arrcol;
                }

                $sheetname = $worksheet->getTitle();
//                var_dump($sheetname); exit();
                if ($sheetname == "PHOTO") {
                    $title = "Import Photo";
                    $rest = 1;
                } else if ($sheetname == "METI") {
                    $title = "Products From METI";
                    $rest = 2;
                }
            }

            if (count($result) > 0) {
                $this->template->add_title($title);
                $this->template->write_view('content', 'admin/warehouse_products_import', array('data' => $result, 'title' => $title, 'rest' => $rest));
                $this->template->render();
                return;
            }
        }
        echo "This file is invalid !";
    }

    public function Upload_xls() {
        $this->load->library("upload");
        $this->upload->initialize(array(
            "upload_path" => "./assets/logitics",
            'allowed_types' => 'xls|xlsx',
            'max_size' => '30000',
            'encrypt_name' => TRUE
        ));
        if ($this->upload->do_upload("Image_upload")) {
            $image_data = $this->upload->data();
            return "assets/logitics/" . $image_data['file_name'];
        } else {
            $error = $this->upload->display_errors();
            echo $error;
            return '';
        }
        return '';
    }

    public function importall() {
        $data = isset($_POST['data']) ? json_decode($_POST['data']) : '';
        if (is_object($data)) {
            $result = array();
            $result['MaSP'] = isset($data->MaSP) ? $data->MaSP : '';
            if ($result['MaSP'] == '') {
                $result['MaSP'] = time();
            } else {
                $check = $this->db->query("select MaSP from ttp_report_products where MaSP='" . $result['MaSP'] . "'")->row();
                if ($check) {
                    echo "Mã sản phẩm đã tồn tại";
                    return;
                }
            }
            $result['Title'] = isset($data->Title) ? $data->Title : '';
            if ($result['Title'] == '') {
                echo "Tên sản phẩm trống";
                return;
            }
            $result['Alias'] = $this->lib->alias($result['Title']);
            $result['MetaTitle'] = isset($data->Title) ? $data->Title : '';
            $result['Donvi'] = isset($data->Donvi) ? $data->Donvi : '';
            if ($result['Donvi'] == '') {
                echo "Đơn vị trống";
                return;
            }
            $result['SupplierID'] = isset($data->SupplierID) ? $data->SupplierID : 0;
            if ($result['SupplierID'] == '' || $result['SupplierID'] == 0) {
                echo "Nhà cung cấp trống";
                return;
            }
            $result['TrademarkID'] = isset($data->TrademarkID) ? $data->TrademarkID : 0;
            if ($result['TrademarkID'] == '' || $result['TrademarkID'] == 0) {
                echo "Thương hiệu trống";
                return;
            }
            $result['BarcodeClient'] = isset($data->BarcodeClient) ? $data->BarcodeClient : '';
            $arr_categories = array();
            $CategoriesID1 = isset($data->CategoriesID1) ? $data->CategoriesID1 : 0;
            if ($CategoriesID1 > 0) {
                $arr_categories[] = $CategoriesID1;
            }
            $CategoriesID2 = isset($data->CategoriesID2) ? $data->CategoriesID2 : 0;
            if ($CategoriesID2 > 0) {
                $arr_categories[] = $CategoriesID2;
            }
            $CategoriesID3 = isset($data->CategoriesID3) ? $data->CategoriesID3 : 0;
            if ($CategoriesID3 > 0) {
                $arr_categories[] = $CategoriesID3;
            }
            $result['CategoriesID'] = json_encode($arr_categories);
            $result['RootPrice'] = isset($data->RootPrice) ? $data->RootPrice : 0;
            $result['Price'] = isset($data->Price) ? $data->Price : 0;
            $result['BasePrice'] = isset($data->BasePrice) ? $data->BasePrice : '';
            $result['SpecialPrice'] = isset($data->SpecialPrice) ? $data->SpecialPrice : 0;
            $result['SpecialStartday'] = isset($data->SpecialStartday) ? $data->SpecialStartday : '';
            $result['SpecialStopday'] = isset($data->SpecialStopday) ? $data->SpecialStopday : '';
            $result['VariantType'] = isset($data->VariantType) ? $data->VariantType : 0;
            $result['CountryID'] = 1;
            $result['TypeProducts'] = isset($data->TypeProducts) ? $data->TypeProducts : 0;
            $result['Status'] = isset($data->Status) ? $data->Status : 0;
            $result['Created'] = date('Y-m-d H:i:s');
            $result['LastEdited'] = date('Y-m-d H:i:s');
            $result['PrintPriceList'] = $data->PrintPriceList;
            $result['SocialPrice'] = $data->SocialPrice;
            $this->db->insert('ttp_report_products', $result);
            $id = $this->db->insert_id();
            if (count($arr_categories) > 0) {
                $sql = array();
                foreach ($arr_categories as $row) {
                    $sql[] = "($id,$row)";
                }
                $sql = "insert into ttp_report_products_categories(ProductsID,CategoriesID) values" . implode(',', $sql);
                $this->db->query($sql);
            }
            echo "OK";
            return;
        }
        echo "false";
    }

    public function syncall() {
        $item = isset($_POST['data']) ? json_decode($_POST['data']) : '';
        if (is_object($item)) {
            $query1 = $this->db->query("SELECT ID,MaSP,Barcode FROM ttp_report_products
                           WHERE `MaSP` = '" . trim($item->SKU) . "' limit 1");

            if ($query1->num_rows() == 0) {
                $data = array(
                    'MaSP' => trim($item->SKU),
                    'Barcode' => trim($item->EAN),
                    'ShortName' => trim($item->Name),
                    'Imported' => date('Y-m-d H:i:s'),
                    'TrademarkID' => 7 //ARV
                );
                $this->db->insert('ttp_report_products', $data);
            } else {
                $query = $this->db->query("SELECT ID,MaSP,Barcode FROM ttp_report_products
                           WHERE `Barcode` LIKE '%" . trim($item->EAN) . "%' limit 1");
                if ($query->num_rows() == 0) {
                    $query2 = $this->db->query("SELECT Barcode FROM ttp_report_products
                           WHERE `MaSP` = '" . $item->SKU . "' limit 1");
                    if ($query2->row()->Barcode == "" || $query2->row()->Barcode == NULL || empty($query2->row()->Barcode)) {
                        $new_barcode = trim($item->EAN);
                    } else {
                        $new_barcode = trim($query1->row()->Barcode) . "," . trim($item->EAN);
                    }
                    $update = array(
                        'Barcode' => $new_barcode,
                        'Imported' => date('Y-m-d H:i:s'),
                    );
                    $this->db->where('MaSP', trim($item->SKU));
                    $this->db->update('ttp_report_products', $update);
                }
            }
            echo "OK";
            return;
        }
        echo "false";
    }

    public function importphoto() {
        $item = isset($_POST['data']) ? json_decode($_POST['data']) : '';
        if (is_object($item)) {
            $query1 = $this->db->query("SELECT SKU FROM auc_import
                           WHERE `SKU` = '" . $item->SKU . "' limit 1");
            $result = $query1->num_rows();
            if ($result) {
                $data_update = array(
                    'Imported' => date('Y-m-d H:i:s')
                );
                $this->db->update('auc_import', $data_update);
            } else {
                $data_insert = array(
                    'SKU' => $item->SKU,
                    'Imported' => date('Y-m-d H:i:s')
                );
                if ($this->db->insert('auc_import', $data_insert)) {
                    echo "OK";
                }
            }
            return;
        }
        echo "false";
    }

    public function updateall() {
        $data = isset($_POST['data']) ? json_decode($_POST['data']) : '';
        if (is_object($data)) {
            $ID = isset($data->ID) ? (int) $data->ID : 0;
            $check = $this->db->query("select ID from ttp_report_products where ID=$ID")->row();
            if (!$check) {
                echo "Sản phẩm không tồn tại";
                return;
            }
            $result = array();
            $result['MaSP'] = isset($data->MaSP) ? $data->MaSP : '';
            if ($result['MaSP'] == '') {
                $result['MaSP'] = time();
            } else {
                $check = $this->db->query("select MaSP from ttp_report_products where MaSP='" . $result['MaSP'] . "' and ID!=$ID")->row();
                if ($check) {
                    echo "Mã sản phẩm đã tồn tại";
                    return;
                }
            }
            $result['Title'] = isset($data->Title) ? $data->Title : '';
            if ($result['Title'] == '') {
                echo "Tên sản phẩm trống";
                return;
            }
            $result['MetaTitle'] = isset($data->Title) ? $data->Title : '';
            $result['Donvi'] = isset($data->Donvi) ? $data->Donvi : '';
            if ($result['Donvi'] == '') {
                echo "Đơn vị trống";
                return;
            }
            $result['SupplierID'] = isset($data->SupplierID) ? $data->SupplierID : 0;
            if ($result['SupplierID'] == '' || $result['SupplierID'] == 0) {
                echo "Nhà cung cấp trống";
                return;
            }
            $result['TrademarkID'] = isset($data->TrademarkID) ? $data->TrademarkID : 0;
            if ($result['TrademarkID'] == '' || $result['TrademarkID'] == 0) {
                echo "Thương hiệu trống";
                return;
            }
            $result['BarcodeClient'] = isset($data->BarcodeClient) ? $data->BarcodeClient : '';
            $arr_categories = array();
            $CategoriesID1 = isset($data->CategoriesID1) ? $data->CategoriesID1 : 0;
            if ($CategoriesID1 > 0) {
                $arr_categories[] = $CategoriesID1;
            }
            $CategoriesID2 = isset($data->CategoriesID2) ? $data->CategoriesID2 : 0;
            if ($CategoriesID2 > 0) {
                $arr_categories[] = $CategoriesID2;
            }
            $CategoriesID3 = isset($data->CategoriesID3) ? $data->CategoriesID3 : 0;
            if ($CategoriesID3 > 0) {
                $arr_categories[] = $CategoriesID3;
            }
            $result['CategoriesID'] = json_encode($arr_categories);
            $result['RootPrice'] = isset($data->RootPrice) ? $data->RootPrice : 0;
            $result['Price'] = isset($data->Price) ? $data->Price : 0;
            $result['BasePrice'] = isset($data->BasePrice) ? $data->BasePrice : '';
            $result['SpecialPrice'] = isset($data->SpecialPrice) ? $data->SpecialPrice : 0;
            $result['SpecialStartday'] = isset($data->SpecialStartday) ? $data->SpecialStartday : '';
            $result['SpecialStopday'] = isset($data->SpecialStopday) ? $data->SpecialStopday : '';
            $result['VariantType'] = isset($data->VariantType) ? $data->VariantType : 0;
            $result['TypeProducts'] = isset($data->TypeProducts) ? $data->TypeProducts : 0;
            $result['Status'] = isset($data->Status) ? $data->Status : 0;
            $result['LastEdited'] = date('Y-m-d H:i:s');
            $result['PrintPriceList'] = $data->PrintPriceList;
            $result['SocialPrice'] = $data->SocialPrice;
            $this->db->where('ID', $ID);
            $this->db->update('ttp_report_products', $result);
            if (count($arr_categories) > 0) {
                $sql = array();
                foreach ($arr_categories as $row) {
                    $sql[] = "($ID,$row)";
                }
                $sql = "insert into ttp_report_products_categories(ProductsID,CategoriesID) values" . implode(',', $sql);
                $this->db->query("delete from ttp_report_products_categories where ProductsID=$ID");
                $this->db->query($sql);
            }
            echo "OK";
            return;
        }
        echo "false";
    }

}

?>
