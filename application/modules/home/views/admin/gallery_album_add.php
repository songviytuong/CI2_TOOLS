<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>GALLERY GROUP</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ALBUM <small> / Add new album</small></h2>
                    <div class="clearfix"></div>
                </div>
                <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."addnew" ?>" method="post" enctype="multipart/form-data">
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class=" col-md-3 col-sm-3 col-xs-12">Published</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Published" value="1" checked="checked"> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Published" value="0"> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 col-sm-6 col-xs-12">Title (<span class="required">*</span>)</label>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="CreateLink" required="required" class="form-control col-md-12 col-xs-12" name="Title">
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-12">
                                <div>
                                    <label>
                                        <span class="btn btn-default btn-file btn-primary">
                                            Browse <input type="file" name="Images_upload[]" id="choosefile" multiple />
                                        </span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                        <div class="form-group dvPreview">
                            <div class='row rowheader'>
                                <div class='col-md-1 col-sm-1 col-xs-12'>Preview</div>
                                <div class='col-md-5 col-sm-5 col-xs-12'>Image Name</div>
                                <div class='col-md-3 col-sm-2 col-xs-12'>Image Type</div>
                                <div class='col-md-3 col-sm-2 col-xs-12'>Image Size</div>
                            </div>
                            <div id="dvPreview">
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .dvPreview{display:none;}
    .filerow{padding:3px 0px;border-bottom:1px solid #E1e1e1;min-height: 50px}
    .rowheader{font-weight: bold;padding:5px 0px;}
    .filerow:hover{border-bottom:1px solid #ccc;}
    .filerow i{margin-right:5px;}
    .filerow a{cursor: pointer}
</style>
<script>
    $(document).ready(function(){
        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var numfile = Fileinput.files.length;
            var dvPreview = $("#dvPreview");
            if(numfile>0){
                var j=0;
                for (var i = 0; i < numfile; i++) {
                    var reader = new FileReader();
                    var file = Fileinput.files[i];
                    var imageType = /image.*/;
                    var head = "";
                    if(file.type.match(imageType)){
                        reader.onloadend = function (e) {
                            var file = Fileinput.files[j];
                            dvPreview.append("<div class='row filerow file"+j+"'><div class='col-md-1 col-sm-1 col-xs-12'><img src='"+e.target.result+"' class='img-responsive' /></div><div class='col-md-5 col-sm-5 col-xs-12'>"+file.name+"</div><div class='col-md-3 col-sm-3 col-xs-12'>"+file.type+"</div><div class='col-md-3 col-sm-3 col-xs-12'>"+file.size+"</div></div>");
                            j++;
                        }
                    }else{
                        console.log("Not an Image");
                    }
                    reader.readAsDataURL(file);
                }
                $(".dvPreview").fadeIn('slow');
            }
        });
    });
</script>