<?php

class Notice_model extends CI_Model
{

    var $tablename = 'auc_notices';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function lastUpdate()
    {
        $query = $this->db->query('SELECT Notice FROM ' . $this->tablename . ' ORDER BY ID DESC');
        $result = $query->row();
        return $result;
    }
}
