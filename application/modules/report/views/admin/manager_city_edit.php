<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa tỉnh thành</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên tỉnh / thành</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Thuộc khu vực</span></div>
					<div class='block2'>
						<select name="CategoriesID" class="form-control">
							<option value="0">-- Chọn khu vực --</option>
							<?php 
							$categories = $this->db->query("select * from ttp_report_area")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									$select = $row->ID==$data->AreaID ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $select>$row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Kho mặc định Online</span></div>
					<div class='block2'>
						<select name="DefaultWarehouse" class="form-control">
							<option value="0">-- Chọn kho --</option>
							<?php 
							$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
							if(count($warehouse)>0){
								foreach($warehouse as $row){
									$select = $row->ID==$data->DefaultWarehouse ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $select>$row->MaKho</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Kho mặc định MT</span></div>
					<div class='block2'>
						<select name="MT" class="form-control">
							<option value="0">-- Chọn kho --</option>
							<?php 
							if(count($warehouse)>0){
								foreach($warehouse as $row){
									$select = $row->ID==$data->MT ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $select>$row->MaKho</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Kho mặc định GT</span></div>
					<div class='block2'>
						<select name="GT" class="form-control">
							<option value="0">-- Chọn kho --</option>
							<?php 
							if(count($warehouse)>0){
								foreach($warehouse as $row){
									$select = $row->ID==$data->GT ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $select>$row->MaKho</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
	.row{margin:0px;}
</style>