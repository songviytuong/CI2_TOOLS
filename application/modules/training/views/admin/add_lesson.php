<?php 
$url = base_url().ADMINPATH.'/training';
?>
<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
    <!-- warning message -->
    <div class="alert alert_error hidden"><h5></h5><p></p></div>  
    <form action="" method="POST">
		<input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
            <div class="import_select_progress">
                 <div class="block1">
                    <h1>TẠO BÀI HỌC</h1>
                 </div>
                 <div class="block2">
                    <button type="button" onclick="saves(this);return false;" align="right" class="btn btn-primary"><i class="fa fa-check-circle"></i> Tạo bài học</button>
                    <a href="<?php echo $url.'/training/add_course/'.$cid.'?tab' ?>" align="right" class="btn btn-danger"><i class="fa fa-times"></i> Đóng</a>
                 </div>
            </div>
                    
            <div class="box_training_tab">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Tên bài học: </label>
                            <div class="col-xs-9">
                               <input class="form-control" id="title" name='title' placeholder="Bài học mới..." type="text" value="<?php echo isset($data["title"]) ? $data["title"] : "" ; ?>">
                            </div>
                         </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Thời lượng : </label>
                            <div class="col-xs-9">
                                    <div class="input-group pull-left">
                                        <select id="hour" name='hour' class="form-control">
                                            <?php 
                                            for($i=0;$i<24;$i++){
                                                echo "<option value='$i'>$i giờ</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <select class="form-control" id="minute" name='minute'>
                                            <?php 
                                           for($i=1;$i<60;$i++){
                                                echo "<option value='$i'>$i phút</option>";
                                           }
                                           ?>
                                        </select>
                                    </div>
                                   
                            </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Loại bài học: </label>
                            <div class="col-xs-9">
                               <select class="form-control" name="type" id="type">
                                    <option value="0">Chọn loại bài học</option>
                                        <?php foreach ($type as $key => $value) { 
                                        $sl= '';
                                        if($tip == $key){
                                            $sl = 'selected="selected"';
                                        }
                                        ?>
                                        <option <?php echo $sl; ?> value="<?php echo $key ?>"><?php echo $value ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                         </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Khóa học: </label>
                            <div class="col-xs-9">
                                <select class="form-control disabled" name="courseid" id="courseid" readonly="true">
                                    <?php foreach ($course as $key => $value) {
                                        $sl = '';
                                        if($cid == $value["id"]){
                                            $sl = 'selected="selected"';
                                            echo "<option $sl value='".$value["id"]."''>".$value["title"]."</option>";
                                        }
                                        ?>
                                    <?php } ?>
                                </select>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Phần : </label>
                            <div class="col-xs-9">
                               <div class="input-group">
                                    <span class="input-group-addon btn" onclick="edit_part()"><i class="fa fa-pencil-square-o"></i></span>
                                    <select class="form-control" name="partid" id="partid">
                                        <option value="0">Chọn Phần</option>
                                        <?php foreach ($part as $key => $value) {
                                            $sl = '';
                                            if($data["partid"] == $value["id"]){
                                                $sl = 'selected="selected"';
                                            }
                                            ?>
                                            <option <?php echo $sl; ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"]; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" onclick="add_part()"><i class="fa fa-plus"></i> Thêm</button>
                                    </span>
                                </div>
                            </div>
                         </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Chương: </label>
                            <div class="col-xs-9">
                                <div class="input-group">
                                    <span class="input-group-addon btn" onclick="edit_chapter()"><i class="fa fa-pencil-square-o"></i></span>
                                    <select class="form-control" name="chapterid" id="chapterid">
                                        <option value="0">Chọn Chương</option>
                                        <?php foreach ($chapter as $key => $value) {
                                            $sl = '';
                                            if($data["chapterid"] == $value["id"]){
                                                $sl = 'selected="selected"';
                                            }
                                            ?>
                                            <option <?php echo $sl; ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"]; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" onclick="add_chapter()"><i class="fa fa-plus"></i> Thêm</button>
                                    </span>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="row" id="videos" style="<?php echo $tip==2 ? "display:block" : "display:none" ; ?>">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Link Video: </label>
                            <div class="col-xs-9">
                                <input type='text' value="<?php echo isset($data["video"]) ? $data["video"] : "" ; ?>"placeholder='https://www.youtube.com/embed/UMp57kh03y0' class="form-control" id="video" name='video' />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label ">Mô tả ngắn:</label>
                            <textarea id="des_soft" class="form-control" name="des_soft"><?php echo isset($data["des_soft"]) ? $data["des_soft"] : "" ; ?></textarea>
                        </div>
                        <div class="form-group"  style="display:<?php echo $display1 ?>" id="mota">
                            <label class="control-label ">Mô tả dài:</label>
                            <textarea id="des_full" class="form-control ckeditor" name="des_full"><?php echo isset($data["des_full"]) ? $data["des_full"] : "" ; ?></textarea>
                        </div>
                    </div>
                </div>
                
            </div>
			<input type="hidden" id="full" name="full"></input>
	    	<input type="hidden" name="id" id="id" value="<?php echo isset($data["id"]) ? $data["id"] : 0; ?>"></input>
		
		    <input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
        
        <div id="list_question" style="display:<?php echo $display3 ?>">
            <h3>DANH SÁCH CÂU HỎI <a onclick="save_beforewrite(this)" data="<?php echo $url;?>/training/add_answer/0/<?php echo isset($cid) ? $cid : 0; ?>" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Thêm câu hỏi</a>  </h3>
            <div  class="table_data" >
                <table>
                    <thead>
                      <tr>
                        <th>STT</th>
                        <th>Câu hỏi</th>
                        <th class="th_action">Thao tác</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($list_question)>0){
                    $stt =0;
                    foreach ($list_question as $key => $value) { ?>
                        <tr>
                        <td><?php echo ++$stt;?></td>
                        <td><?php echo $value["title"];?> </td>
                        <td><a href="<?php echo $url;?>/training/add_answer/<?php echo $value["id"];?>" class="td_link_edit"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a> <a onclick="del_answer(<?php echo $value["id"];?>);return false;"><i class="fa fa-trash-o"> </i> Xóa</a></td>
                        </tr>
                    <?php }
                    }else{
                        echo "<tr><td colspan='3'>Chưa có câu hỏi nào cho bài kiểm tra này .</td></tr>";
                    } ?>
                    </tbody>
                  </table>
            </div>
        </div>
    </form>
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function show_addbox(ob){
        var id = $("#id").val();
        var link = $('#link').val(); 
        enablescrollsetup();
        
        $(".over_lay .block1_inner h1").html("Thêm câu hỏi vào bài kiểm tra");
        $(".over_lay .block2_inner").load(link+'/training/add_answer_view/'+id);
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    function edit_question(question){
        var link = $('#link').val();
        var id = $("#id").val();
        enablescrollsetup();
        $(".over_lay .block1_inner h1").html("Thêm câu hỏi vào bài kiểm tra");
        $(".over_lay .block2_inner").load(link+'/training/add_answer_view/'+id+'/'+question);
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    $("#close_overlay").click(function(){
        $(".over_lay").hide();
        disablescrollsetup();
        $(".over_lay .block2_inner").html('');
    });

    function enablescrollsetup(){
        $(window).scrollTop(70);
        $("body").css({'height':'100%','overflow-y':'hidden'});
        h = window.innerHeight;
        h = h-200;
        $(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
    }

    function disablescrollsetup(){
        $("body").css({'height':'auto','overflow-y':'scroll'});
    }

    function del_answer(ob,id){
        var result = confirm('Bạn có chắc chắn muốn xóa không');
        if(result){
            var link = $('#link').val();
            $.post(link+"/training/remove_question/"+id, $("form").serialize(),function(resp){
                $(ob).parent('td').parent('tr').fadeOut();
            });
        }
    }

    function goBack() {
        var link = $('#link').val();
        var courseid = $('#courseid').val();
        window.location = link+'/training/add_course/'+courseid+"/1";
    }

	function saves(ob){
            $(ob).addClass('saving');
            $(ob).find('i').hide();
            var des_full = CKEDITOR.instances.des_full.getData();
    		$('#full').val(des_full);
            var courseid = $('#courseid').val();
        	var link = $('#link').val();
        	
            $.post(link+"/training/save_lesson/", $("form").serialize(),function(resp){
                $.each(resp, function (i, obj) {
        			var id = obj.id;
                    var msg = obj.msg;
                    if(msg != "ok"){
                        $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                        $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ").show();
                    	$(".alert_error p").html(msg);
                    	$('#' + id).focus();
                        $(ob).removeClass('saving');
                        $(ob).find('i').show();
                    }else{
                        $(".alert_error").removeClass('alert-danger').addClass("alert-success").removeClass('hidden');
                        $(".alert_error h5").hide();
                        $(".alert_error p").html("<i class='fa fa-check-circle'></i> Lưu dữ liệu thành công !");
                        $(ob).removeClass('saving');
                        $(ob).find('i').show();
                        $("form").get(0).reset();
                    }
        		});
        	}, 'json');
    }

    function save_beforewrite(){
            var des_full = CKEDITOR.instances.des_full.getData();
            $('#full').val(des_full);
            var courseid = $('#courseid').val();
            var link = $('#link').val();
            $.post(link+"/training/save_lesson/", $("form").serialize(),function(resp){
                $.each(resp, function (i, obj) {
                    var id = obj.id;
                    var msg = obj.msg;
                    if(msg != "ok"){
                        $(".alert_error").removeClass('alert-success').addClass("alert-danger").removeClass('hidden');
                        $(".alert_error h5").html("<i class='fa fa-exclamation-circle'></i> Bạn cần bổ sung các thông tin dưới đây : ");
                        $(".alert_error p").html(msg);
                        $('#' + id).focus();
                        $(ob).removeClass('saving');
                        $(ob).find('i').show();
                    }else{
                        window.location=link+"/training/add_lesson/"+id+"/?showpopup";
                    }
                });
            }, 'json');
    }

    function resettype(type){
        if(type ==1){
            $('#des_full').html('');
            $('#mota').show();

            $('#video').val('');
            $('#videos').hide();
            $('#list_question').hide(); 
            $('.videos').hide();

        }
        if(type ==2){
            $('#des_full').html('');
            $('#mota').hide();
            $('#videos').show();
            $('#list_question').hide(); 
        }
        if(type ==3){
            $('#des_full').html('');
            $('#mota').hide();

            $('#video').val('');
            $('#videos').hide();
            $('#list_question').show(); 
        }
    }

    function add_part(){
        var getValue = prompt("Điền tên phần mới : ", "");
        if(getValue!=null){
            var link = $('#link').val();
            var courseid = $('#courseid').val();
            var url = link+"/training/add_new_part";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&CoursesID="+courseid,
                success: function(result){
                    if(result!='False'){
                        $('#partid').append("<option value='"+result+"'>"+getValue+"</option>");
                        $('#partid').val(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function edit_part(){
        var ID = $('#partid').val();
        var text = $('#partid :selected').text();
        var getValue = prompt("Điền tên phần mới thay cho tên phần cũ : ", text);
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/update_part";
            var courseid = $('#courseid').val();
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&ID="+ID+"&CoursesID="+courseid,
                success: function(result){
                    if(result!='False'){
                        $('#partid :selected').text(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function add_chapter(){
        var getValue = prompt("Điền tên chương mới : ", "");
        if(getValue!=null){
            var link = $('#link').val();
            var courseid = $('#courseid').val();
            var partid = $('#partid').val();
            var url = link+"/training/add_new_chapter";
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&CoursesID="+courseid+"&PartID="+partid,
                success: function(result){
                    if(result!='False'){
                        $('#chapterid').append("<option value='"+result+"'>"+getValue+"</option>");
                        $('#chapterid').val(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function edit_chapter(){
        var ID = $('#chapterid').val();
        var text = $('#chapterid :selected').text();
        var getValue = prompt("Điền tên chương mới thay cho tên chương cũ : ", text);
        if(getValue!=null){
            var link = $('#link').val();
            var url = link+"/training/update_chapter";
            var courseid = $('#courseid').val();
            var partid = $('#partid').val();
            $.ajax({
                dataType: "html",
                type: "POST",
                url: url,
                data: "Title="+getValue+"&ID="+ID+"&CoursesID="+courseid+"&PartID="+partid,
                success: function(result){
                    if(result!='False'){
                        $('#chapterid :selected').text(result);
                    }else{
                        alert("Yêu cầu không được thực hiện ! Vui lòng kiểm tra lại đường truyền .");
                    }
                }
            });
        }
    }

    function savechapter(){
        var link = $('#link').val();
        var courseid = $('#courseid').val();
        var partid = $('#partid').val();
            $.post(link+"/training/save_chapter/", $("form").serialize(),function(resp){
            $.each(resp, function (i, obj) {
                var id = obj.id;
                var msg = obj.msg;
                if(id != "ok"){
                    alert(msg);
                    $('#' + id).focus();
                    return false;
                }else{
                    var url = link+"/training/get_chapter_by_course_by_part/"+courseid+"/"+partid;
                    $('#chapterid').html('<option value="0">Chọn Chương</option>');
                    $.ajax({
                          dataType: "json",
                          type: "GET",
                          url: url,
                          data: "",
                          success: function(resp){
                            $.each(resp,function(i,obj){
                                var html = "<option value='"+resp[i].id+"'> "+resp[i].title+" </option>";
                                $('#chapterid').append(html);
                            });
                        }
                      });

                    alert('Lưu thành công');
                    $('#title_chapter_new').val('');
                    $('.frm_chapter').hide();
                }
            });
        }, 'json');
        
    }
    $(document).ready(function(){
        $('.add_chapter').click(function(){
            $('.frm_chapter').show();
        });
        $('.add_part').click(function(){
            $('.frm_part').show();
        });

        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        $('#type').change(function(){
            var type = $('#type').val();
            resettype(type);
        });

        var link = $('#link').val();
        $('#courseid').change(function(){
            $('#partid').html('<option value="0">Chọn Phần</option>');
            $('#chapterid').html('<option value="0">Chọn chương</option>');
            var courseid = $('#courseid').val();
            var url = link+"/training/get_course_by_id/"+courseid;
            $.ajax({
              dataType: "json",
              type: "GET",
              url: url,
              data: "",
              success: function(resp){
                $.each(resp,function(i,obj){
                    var html = "<option value='"+resp[i].id+"'> "+resp[i].title+" </option>";
                    $('#partid').append(html);
                });
                

              }
          });
        });

        $('#partid').change(function(){
            $('#chapterid').html('<option value="0">Chọn chương</option>');
            var partid = $('#partid').val();
            var url = link+"/training/get_part_by_id/"+partid;
            $.ajax({
              dataType: "json",
              type: "GET",
              url: url,
              data: "",
              success: function(resp){
                $.each(resp,function(i,obj){
                    var html = "<option value='"+resp[i].id+"'> "+resp[i].title+" </option>";
                    $('#chapterid').append(html);
                });
                

              }
          });
        });
    });
</script>