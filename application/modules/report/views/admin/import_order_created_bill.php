<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p><b>CÔNG TY CP DƯỢC PHẨM HOA THIÊN PHÚ</b></p>
				<p>10 Nguyễn Cửu Đàm, P.Tân Sơn Nhì, Q.Tân Phú, TPHCM</p>
				<p>Hotline: 19006033</p>
				<p>Email: support@hoathienphu.com.vn</p>
			</div>
			<div class="block1_2">
				<?php
				$export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$data->ID")->row();
				if($export){
					echo '<a id="print_page"><i class="fa fa-print"></i> In Liên</a>';
					//echo '<a id="print_page1" href="'.ADMINPATH.'/pos/pos_sales/print_order/'.$data->ID.'"><i class="fa fa-print"></i> In Bill</a>';
					echo '<a id="print_page1" href="'.current_url().'?VAT=10"><i class="fa fa-print"></i> In VAT</a>';
				}
				?>
				<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/import_order/preview/$data->ID" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<h1>Phiếu xuất kho bán hàng</h1>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaXK : '--' ; ?></span></p>
			</div>
			<div class="block1_3" style="margin-left:-20px">
				<img src="public/admin/images/logo_in_bill.png" />
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Họ tên người nhận : </li>
				<li><?php echo $data->Name ?> </li>
				<li>Số điện thoại 1 : </li>
				<li><?php echo $data->Phone ?></li>
				<li>Số điện thoại 2 : </li>
				<li><?php echo $data->Phone2 ?></li>

			</div>
			<div class="row">
				<li>Theo đơn hàng số : </li>
				<li><?php echo $data->MaDH ?></li>
				<li>Ngày đặt hàng : </li>
				<li><?php echo date("d/m/Y",strtotime($data->Ngaydathang)) ?></li>
				<li>Ngày đề nghị giao : </li>
				<?php
				$haftday = date('H',strtotime($data->DeliveryTime));
            	$haftstate = $haftday<12 ? "sáng" : " chiều";
				?>
				<li> <?php echo $haftstate.' - '.date("d/m/Y",strtotime($data->DeliveryTime)) ?></li>
			</div>
			<div class="row">
				<li>Địa chỉ giao hàng:</li>
				<?php
				$district = $this->db->query("select Title from ttp_report_district where ID=$data->DistrictID")->row();
				$district = $district ? $district->Title : "" ;
				$city = $this->db->query("select Title from ttp_report_city where ID=$data->CityID")->row();
				$city = $city ? $city->Title : "" ;
				?>
				<li class="special"><?php echo $data->AddressOrder." - $district - $city" ?></li>
			</div>
			<div class="row">
				<?php
				$pay_status = $this->define_model->get_order_status('payment','order');
				$arr_payment = array();
				$pay = '';
				foreach($pay_status as $key=>$ite){
					$code = (int)$ite->code;
					$pay = $data->Payment==$code ? $ite->name : $pay ;
				}
				?>
				<li>Hình thức thanh toán:</li>
				<li><?php echo $pay ; ?></li>
			</div>
			<div class="row">
				<li>Ghi chú khách hàng:</li>
				<li class="special"><?php echo $data->Note ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th>Số TT</th>
					<th>Mã sản phẩm</th>
					<th>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th>Đơn vị tính</th>
					<th>Số lượng</th>
					<th>Đơn giá</th>
					<th>Thành tiền (VNĐ)</th>
				</tr>
				<?php
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,b.* from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						$ProductsChild = $this->db->query("select a.Title,b.Quantity from ttp_report_products a,ttp_report_products_bundle b where a.ID=b.Des_ProductsID and b.Src_ProductsID=$row->ProductsID")->result();
						$child = "";
						if(count($ProductsChild)>0){
							foreach($ProductsChild as $item){
								$child.=" $item->Quantity $item->Title ,";
							}
							$child = "($child)";
						}
						$giaban = $row->Price+$row->PriceDown;
    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Title $child</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>".number_format($row->Price)."</td>";
						echo "<td>".number_format($row->Total)."</td>";
						echo "</tr>";
						$i++;
					}
				}

				$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
				$tongtienhang = $data->Total - $data->Chietkhau;
		    	$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
				?>

				<tr>
					<td></td>
					<td colspan="5" style="text-align:right;">TỔNG CỘNG <br>GIÁ TRỊ CHIẾT KHẤU<br>TỔNG CỘNG SAU CHIẾT KHẤU<br>CHI PHÍ VẬN CHUYỂN<br>
					<?php 
					if(isset($_GET['VAT'])){
						echo 'TỔNG GIÁ TRỊ CHƯA TÍNH THUẾ<br>TỔNG GIÁ TRỊ PHẢI THANH TOÁN';
					}else{
						echo 'TỔNG GIÁ TRỊ PHẢI THANH TOÁN';
					}
					?>
					</td>
					<td>
					<?php echo number_format($data->Total) ?><br>
					<?php echo number_format($data->Chietkhau) ?><br>
					<?php echo number_format($tongtienhang) ?><br>
					<?php echo number_format($data->Chiphi) ?><br>
					<?php echo number_format($tonggiatrithanhtoan) ?>
					<?php 
					if(isset($_GET['VAT'])){
						$VAT = (int)$_GET['VAT'];
						echo "<br>";
						echo number_format($tonggiatrithanhtoan+($tonggiatrithanhtoan*$VAT)/100);
					}
					?>
					</td>
				</tr>
			</table>
		</div>

		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
			</div>

			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
		</div>
	</div>
</div>
<style>body{background:#FFF;}</style>
<script>
	$("#print_page").click(function(){
	    window.print();
	});

	$(document).on('keydown', function(e) {
	    if(e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
	        e.cancelBubble = true;
	        e.preventDefault();
	        e.stopImmediatePropagation();
	    }
	});
</script>
