<?php 
class Manager_categories extends Admin_Controller { 
 
    public $limit = 15;
 	public $user;
 	public $classname="manager_categories";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        
        $cid = $this->session->userdata("report_filter_parent_CategoriesID");
        $where = "";
        if($cid){
            $where .= " where Path like '$cid/%' order by ID DESC ";
        } else {
            $where .= " where ParentID = 0 order by STT ASC ";
        }
        
        $where_counter = "";
        if($cid){
            $where_counter .= " where Path like '$cid/%'";
        } else {
            $where_counter .= " where ParentID = 0";
        }
        $nav = $this->db->query("select count(ID) as nav from ttp_report_categories $where_counter")->row();
        $nav = $nav ? $nav->nav : 0;
        
        $object = $this->db->query("select * from ttp_report_categories $where $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_categories/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_categories/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Categories | Manager Report Tools');
		$this->template->write_view('content','admin/manager_categories_home',$data);
		$this->template->render();
	}

    public function search($link='search'){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $CategoriesID = $this->session->userdata("report_filter_parent_CategoriesID");
        $str_nav = "select count(1) as nav from ttp_report_categories";
        $str = "select * from ttp_report_categories";
        if($CategoriesID>0){
            $str.=" where Path like '$CategoriesID/%'";
            $str_nav.=" where Path like '$CategoriesID/%'";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data=array(
            'data'  => $this->db->query($str." order by ID DESC $limit_str")->result(),
            'nav'   => $this->lib->nav(base_url().ADMINPATH.'/report/manager_categories/'.$link,5,$nav,$this->limit),
            'start' => $start,
            'find'      =>  $nav,
            'base_link' =>  base_url().ADMINPATH.'/report/manager_categories/',
        );
        $this->template->write_view('content','admin/manager_categories_home',$data);
        $this->template->render();
    }

    public function setsessionsearch(){
        if(isset($_POST['CategoriesID'])){
            $CategoriesID = mysql_real_escape_string($_POST['CategoriesID']);
            $this->session->set_userdata("report_filter_parent_CategoriesID",$CategoriesID);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_parent_CategoriesID");
        $this->search('setsessionsearch');
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Categories add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_categories/'
        );
        $this->template->write_view('content','admin/manager_categories_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $MetaTitle = isset($_POST['MetaTitle']) ? strip_tags($_POST['MetaTitle']) : '' ;
        $MetaDescription = isset($_POST['MetaDescription']) ? strip_tags($_POST['MetaDescription']) : '' ;
        $MetaKeywords = isset($_POST['MetaKeywords']) ? strip_tags($_POST['MetaKeywords']) : '' ;
        $Special = isset($_POST['Special']) ? $_POST['Special'] : 0 ;
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : '' ;
        $Alias = $Alias=='' ? $this->lib->alias($Title) : $Alias ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
        if(!is_numeric($CategoriesID)) return;
        if($Title!='' && $CategoriesID!=''){
            $path = $this->db->query("select Path from ttp_report_categories where ID=$CategoriesID")->row();
            $path = $path ? $path->Path : '' ;
            $max = $this->db->query("select max(ID) as ID from ttp_report_categories")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $data = array(
                'ParentID'      => $CategoriesID,
                'Title'         => $Title,
                'MetaTitle'     => $MetaTitle,
                'MetaKeywords'     => $MetaKeywords,
                'MetaDescription'     => $MetaDescription,
                'Path'          => $CategoriesID>0 ? $path.'/'.$max : $max ,
                'IsLast'        => 1,
                'Alias'         => $Alias,
                'Special'   => $Special
            );
            if(isset($_FILES['Image_upload'])){
                if($_FILES['Image_upload']['error']==0){
                    $this->upload_to = "categories_thumb";
                    $data['Thumb'] = $this->upload_attach_file('Image_upload');
                }
            }
            if(isset($_FILES['Image_upload1'])){
                if($_FILES['Image_upload1']['error']==0){
                    $this->upload_to = "categories_thumb";
                    $data['Banner'] = $this->upload_attach_file('Image_upload1');
                }
            }
            $this->db->insert("ttp_report_categories",$data);
            $this->db->query("update ttp_report_categories set IsLast=0 where ID=$CategoriesID");
        }
        redirect(ADMINPATH.'/report/manager_categories/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_categories where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_categories where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Categories | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_categories/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_categories_edit',$data);
            $this->template->render();
        }
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $MetaTitle = isset($_POST['MetaTitle']) ? strip_tags($_POST['MetaTitle']) : '' ;
        $MetaDescription = isset($_POST['MetaDescription']) ? strip_tags($_POST['MetaDescription']) : '' ;
        $MetaKeywords = isset($_POST['MetaKeywords']) ? strip_tags($_POST['MetaKeywords']) : '' ;
        $Special = isset($_POST['Special']) ? $_POST['Special'] : 0 ;
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : '' ;
        $Alias = $Alias=='' ? $this->lib->alias($Title) : $Alias ;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0 ;
        if($Title!='' && $CategoriesID!=''){
            $path = $this->db->query("select Path from ttp_report_categories where ID=$CategoriesID")->row();
            $path = $path ? $path->Path : '' ;
            $data = array(
                'ParentID'  => $CategoriesID,
                'Title'     => $Title,
                'Path'      => $CategoriesID>0 ? $path.'/'.$ID : $ID ,
                'IsLast'    => 1,
                'MetaTitle'     => $MetaTitle,
                'MetaKeywords'     => $MetaKeywords,
                'MetaDescription'     => $MetaDescription,
                'Alias'     => $Alias,
                'Special'   => $Special
            );
            $check = $this->db->query("select ID from ttp_report_categories where ParentID=$ID")->row();
            if($check){
                $data['IsLast'] = 0;
            }
            if(isset($_FILES['Image_upload'])){
                if($_FILES['Image_upload']['error']==0){
                    $this->upload_to = "categories_thumb";
                    $data['Thumb'] = $this->upload_attach_file('Image_upload');
                }
            }
            if(isset($_FILES['Image_upload1'])){
                if($_FILES['Image_upload1']['error']==0){
                    $this->upload_to = "categories_thumb";
                    $data['Banner'] = $this->upload_attach_file('Image_upload1');
                }
            }
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_categories",$data);
            $this->db->query("update ttp_report_categories set IsLast=0 where ID=$CategoriesID");
        }
        redirect(ADMINPATH.'/report/manager_categories/');
    }
}
?>
