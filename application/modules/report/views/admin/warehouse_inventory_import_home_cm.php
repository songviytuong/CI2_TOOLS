<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month 			= array();
$dayofmonth		= array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1><?php echo $type==1 ? 'DANH SÁCH PHIẾU CHỜ NHẬP KHO' : 'DANH SÁCH YÊU CẦU NHẬP KHO' ; ?></h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="import_orderlist">
    	<div class="block2">
    		<div class="block_2_1 col-xs-8">
                <?php 
                $view_import = 'import_from_production' ;
                $view_import = $type==1 ? 'import_from_rejectorder' : $view_import ;
                $view_import = $type==2 ? 'import_from_production' : $view_import ;
                ?>
    			<div class='row'>
                    <div class="form-group">
                    <div class="col-xs-4">
                        <a class="btn btn-danger" href="<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_import/import_from_production' ?>"><i class="fa fa-plus"></i> Tạo yêu cầu nhập kho mới</a>
                    </div>
                    <div class='col-xs-3' style='margin-right: 10px;'>
                    <select class="form-control" onchange="change_warehouse(this)">
                        <option value=''>-- Tại tất cả kho --</option>
                        <?php 
                        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
                        $warehouse_id = isset($_GET['warehouse']) ? $_GET['warehouse'] : '' ;
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                $selected = $row->ID==$warehouse_id ? "selected='selected'" : '' ;
                                echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                            }
                        }
                        ?>
                    </select>
                    </div>
                    <div class='col-xs-3'>
                    <select class="form-control" onchange="change_type(this)">
                        <?php 
                        $TypeImport = $this->session->userdata('TypeImport');
                        ?>
                        <option value='0' <?php echo $TypeImport==0 ? "selected='selected'" : '' ; ?>>Mua từ nhà cung cấp</option>
                        <option value='2' <?php echo $TypeImport==2 ? "selected='selected'" : '' ; ?>>Thành phẩm, trả kho nội bộ</option>
                        <?php 
                        if($this->user->UserType!=6){
                        ?>
                        <option value='1' <?php echo $TypeImport==1 ? "selected='selected'" : '' ; ?>>Hàng bán bị trả lại</option>
                        <option value='2' <?php echo $TypeImport==2 ? "selected='selected'" : '' ; ?>>Thành phẩm, trả kho nội bộ</option>
                        <?php 
                        }
                        ?>
                    </select>
                    </div>
                    </div>
                </div>
    		</div>
    		<div class="block_2_2 col-xs-4">
    			<button class="btn btn-default pull-right" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
    		</div>
    	</div>
        <div class="clear"></div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"a.KhoID",1=>"a.Type",2=>"a.UserID",3=>"a.MaNK",4=>"d.POCode");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Tên Kho",1=>"Hình thức nhập",2=>"Người tạo",3=>"Mã nhập kho",4=>"Mã PO");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                    <div class="list_toolls"><p class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></p></div>
                    <div class="list_toolls">
                        <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <p class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></p>
                        <ul class="dropdownbox">
                            <li><a onclick="setfield(this,0,'makho')">Mã kho</a></li>
                            <li><a onclick="setfield(this,1,'hinhthucnhap')">Hình thức nhập</a></li>
                            <li><a onclick="setfield(this,2,'nguoitao')">Người tạo</a></li>
                            <li><a onclick="setfield(this,3,'manhapkho')">Mã YC nhập kho</a></li>
                            <li><a onclick="setfield(this,4,'mapo')">Mã PO</a></li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation">
                        <select class="oparation" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter">
                        <?php 
                        if($value_field==0){
                            $result = $this->db->query("select MaKho,ID from ttp_report_warehouse order by MaKho ASC")->result();    
                            if(count($result)>0){
                                echo "<select name='FieldText[]'>";
                                foreach($result as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->MaKho : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                                }
                                echo "</select>";
                            }
                        }elseif ($value_field==1) {
                            echo "<select name='FieldText[]'>";
                            echo $param_value==0 ? "<option value='0' selected='selected'>Mua từ nhà cung cấp</option>" : "<option value='0'>Mua từ nhà cung cấp</option>" ;
                            echo $param_value==1 ? "<option value='1' selected='selected'>Hàng bán bị trả lại</option>" : "<option value='1'>Hàng bán bị trả lại</option>" ;
                            echo $param_value==2 ? "<option value='2' selected='selected'>Thành phẩm, trả kho nội bộ</option>" : "<option value='2'>Thành phẩm, trả kho nội bộ</option>" ;
                            $fill_temp_value = $param_value==0 ? "Mua từ nhà cung cấp" : $fill_temp_value ;
                            $fill_temp_value = $param_value==1 ? "Hàng bán bị trả lại" : $fill_temp_value ;
                            $fill_temp_value = $param_value==2 ? "Thành phẩm, trả kho nội bộ" : $fill_temp_value ;
                            echo "</select>";
                        }elseif ($value_field==2) {
                            $userlist = $this->db->query("select ID,UserName from ttp_user where UserType=1 or IsAdmin=1")->result();
                            if(count($userlist)>0){
                                echo "<select name='FieldText[]'>";
                                foreach($userlist as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->UserName : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->UserName</option>";
                                }
                                echo "</select>";
                            }
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                        $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="list_toolls"><p class="title first-title">Chọn</p></div>
                        <div class="list_toolls">
                            <input type="hidden" class="FieldName" name="FieldName[]" />
                            <p class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></p>
                            <ul class="dropdownbox">
                                <li><a onclick="setfield(this,0,'makho')">Mã kho</a></li>
                                <li><a onclick="setfield(this,1,'hinhthucnhap')">Hình thức nhập</a></li>
                                <li><a onclick="setfield(this,2,'nguoitao')">Người tạo</a></li>
                                <li><a onclick="setfield(this,3,'manhapkho')">Mã yêu cầu nhập kho</a></li>
                                <li><a onclick="setfield(this,4,'mapo')">Mã PO</a></li>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation">
                            <select class="oparation" name="FieldOparation[]">
                                <option value="1">Bằng</option>
                                <option value="0">Có chứa</option>
                                <option value="2">Khác</option>
                                <option value="3">Lớn hơn</option>
                                <option value="4">Nhỏ hơn</option>
                                <option value="5">Lớn hơn hoặc bằng</option>
                                <option value="6">Nhỏ hơn hoặc bằng</option>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter">
                            <input type="text" name="FieldText[]" id="textsearch" />
                        </div>
                        <a class='remove_row_x' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <a class="btn" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                    <button class="btn" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                </div>
            </form>
        </div>
    	<div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
    		<table id="table_data">
    			<tr>
    				<th>STT</th>
                    <th>Mã phiếu</th>
    				<th>Hình thức nhập</th>
    				<th>Ngày</th>
    				<th>Người tạo</th>
    				<?php 
                    echo $type=='' || $type==0 || $type==2 ? '<th>Nhà cung cấp</th>' : '' ;
                    echo $type=='' || $type==0 || $type==2 ? '<th>Mã PO</th>' : '' ;
                    ?>
    				<th>Tổng SL</th>
    				<th>Tổng GT</th>
    				<th>Tình trạng</th>
    			</tr>
    			<?php 
                $arr_type = array(
                    0=>"Mua từ nhà cung cấp",
                    1=>"Hàng trả về",
                    2=>"Thành phẩm / Trả kho nội bộ"
                );
                $arr_status = array(
                    0=>'Yêu cầu nhập kho',
                    1=>'Yêu cầu bị trả về từ kho',
                    2=>'Yêu cầu chờ kế toán xử lý',
                    3=>'Yêu cầu bị trả về từ kế toán',
                    4=>'Hàng đã nhập kho thành công',
                    5=>'Yêu cầu bị hủy'
                );
                $i=$start+1;
                if(count($data)>0){
    				foreach($data as $row){
                        $type = isset($arr_type[$row->Type]) ? $arr_type[$row->Type] : '--' ;
                        $status = isset($arr_status[$row->Status]) ? $arr_status[$row->Status] : '--' ;
    					echo "<tr>
			    				<td style='text-align:center'>$i</td>
                                <td style='text-align:center;'><a href='{$base_link}edit/$row->ID'>$row->MaNK</a></td>
			    				<td><a href='{$base_link}edit/$row->ID'>$type</a></td>
                                <td style='width: 90px;'><a href='{$base_link}edit/$row->ID'>".date('d/m/Y',strtotime($row->NgayNK))."</a></td>
			    				<td><a href='{$base_link}edit/$row->ID'>$row->UserName</a></td>";
			    		if(isset($row->Title)){
                            echo $type=='' || $type==0 || $type==2 ? "<td><a href='{$base_link}edit/$row->ID'>$row->Title</a></td>" : "" ;
                            echo $type=='' || $type==0 || $type==2 ? "<td><a href='{$base_link}edit/$row->ID' class='ValuePO'>$row->POCode</a></td>" : "" ;
                        }
			    		echo "<td style='text-align:right'>".number_format($row->TotalAmount)."</td>
			    				<td style='text-align:right'>".number_format($row->TotalPrice)."</td>
			    				<td style='text-align:center'>".$status."</td>";
                        $i++;
    				}
    			}else{
                    $colspan = $type=='' || $type==0 || $type==2 ? 10 : 9 ;
    				echo "<tr><td colspan='$colspan'>Không tìm thấy đơn hàng.</td></tr>";
    			}
    			?>
    		</table>
            <?php 
                echo $nav;
            ?>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });

	$("#show_thaotac").click(function(event){
        event.stopPropagation();
		$(this).parent('li').find('ul').toggle();
	});

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });

    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });
	
    function checkfull(ob){
        if(ob.checked===true){
            $("#table_data .selected_order").prop('checked', true);
        }else{
            $("#table_data .selected_order").prop('checked', false);
        }
    }

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("p.second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"/warehouse_inventory_import/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="makho" || fieldname=="hinhthucnhap" || fieldname=="nguoitao" || fieldname=="mapo"){
            return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
        }
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });

    function change_type(ob){
        var type = $(ob).val();
        var baselink = $("#baselink_report").val();
        window.location=baselink+"/warehouse_inventory_import/set_import_type?Type="+type;
    }

    function change_warehouse(ob){
        var ID = $(ob).val();
        var baselink = $("#baselink_report").val();
        if(ID==''){
            window.location=baselink+"/warehouse_inventory_import";    
        }else{
            window.location=baselink+"/warehouse_inventory_import?warehouse="+ID;
        }
    }

</script>
<style>
    .body_content .containner .import_orderlist .export_tools_kt:before{right:177px;}
</style>
