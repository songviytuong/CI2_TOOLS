<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST" enctype="multipart/form-data">
			<input type='hidden' name='ID' value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Thêm ngành hàng</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row" style="margin:0px">
					<div class="form-group">
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Tên ngành hàng</label>
							<div class="col-xs-8">
								<input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Thuộc nhóm ngành hàng</label>
							<div class="col-xs-8">
								<select name="CategoriesID" class="form-control">
									<option value="0">-- Chọn nhóm ngành hàng --</option>
									<?php 
									$categories = $this->db->query("select * from ttp_report_categories")->result();
									if(count($categories)>0){
										foreach($categories as $row){
											$selected = $row->ID==$data->ParentID ? "selected='selected'" : '' ;
											echo "<option value='$row->ID' $selected>$row->Title</option>";
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin:0px">
					<div class="form-group">
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Alias / đường dẫn</label>
							<div class="col-xs-8">
								<input type="text" name="Alias" class="form-control" value="<?php echo $data->Alias ?>" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Hiển thị ra trang chủ</label>
							<div class="col-xs-8">
								<select class="form-control" name="Special">
									<option value="0" <?php echo $data->Special==0 ? 'selected="selected"' : '' ; ?>>Không hiển thị</option>
									<option value="1" <?php echo $data->Special==1 ? 'selected="selected"' : '' ; ?>>Hiển thị</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<h4>Meta tags</h4>
				<div class="row" style="margin:0px">
					<div class="form-group">
						<label for="" class="control-label">Meta title</label>
						<div class="col-xs-12">
							<input type="text" name="MetaTitle" class="form-control" value="<?php echo $data->MetaTitle ?>">
						</div>
					</div>
				</div>
				<div class="row" style="margin:0px">
					<div class="form-group">
						<label for="">Meta description</label>
						<div class="col-xs-12">
							<textarea name="MetaDescription" class="form-control" value="<?php echo $data->MetaKeywords ?>"></textarea>
						</div>
					</div>
				</div>
				<div class="row" style="margin:0px">
					<div class="form-group">
						<label for="">Meta keywords</label>
						<div class="col-xs-12">
							<textarea name="MetaKeywords" class="form-control" value="<?php echo $data->MetaDescription ?>"></textarea>
						</div>
					</div>
				</div>
				<hr>
				<h4>Hình ảnh liên kết</h4>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Hình đại diện (200x200)</label>
							<div class="col-xs-8">
								<input type="file" name="Image_upload" id="choosefile" onchange="viewimage()" />
							</div>
						</div>
						<div class="col-xs-6">
							<label for="" class="control-label col-xs-4">Banner ngang (1200x200)</label>
							<div class="col-xs-8">
								<input type="file" name="Image_upload1" id="choosefile1" onchange="viewimage1()" /> 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-xs-6">
							<div class="col-xs-4"></div>
							<div class="col-xs-3 dvPreview">
								<?php echo file_exists($data->Thumb) ? "<img src='$data->Thumb' class='img-responsive' />" : "" ; ?>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="col-xs-4"></div>
							<div class="col-xs-8 dvPreview1">
								<?php echo file_exists($data->Banner) ? "<img src='$data->Banner' class='img-responsive' />" : "" ; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>
<script>
	function viewimage(){
        var Fileinput = document.getElementById("choosefile");
        var file = Fileinput.files[0];
        var imageType = /image.*/
        var dvPreview = $(".dvPreview");
        dvPreview.html("");
        if(file.type.match(imageType)){
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $("<img />");
                img.attr("class", "img-responsive");
                img.attr("src", e.target.result);
                dvPreview.html(img);
            }
            reader.readAsDataURL(file);
        }else{
            console.log("Not an Image");
        }
    }

    function viewimage1(){
        var Fileinput = document.getElementById("choosefile1");
        var file = Fileinput.files[0];
        var imageType = /image.*/
        var dvPreview = $(".dvPreview1");
        dvPreview.html("");
        if(file.type.match(imageType)){
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = $("<img />");
                img.attr("class", "img-responsive");
                img.attr("src", e.target.result);
                dvPreview.html(img);
            }
            reader.readAsDataURL(file);
        }else{
            console.log("Not an Image");
        }
    }
</script>