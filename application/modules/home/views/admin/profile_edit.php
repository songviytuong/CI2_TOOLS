<?php 
    $error = isset($_GET['error']) ? $_GET['error'] : "" ;
    echo $error=="user_exists" ? "<script>alert('Username already exists !');</script>" : "" ;
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>SYSTEM GROUP</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>USERS <small> / Edit user</small></h2>
                    <div class="clearfix"></div>
                </div>
                <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
                <div class="x_content">
                        <?php 
                        if(isset($_GET['mustchange'])){
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <p>Bạn cần thay đổi mật khẩu tài khoản để bảo mật thông tin cho hệ thống cũng như cho chính bạn !</p>
                        </div>
                        <?php 
                        }
                        ?>
                        <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="UserName" value="<?php echo $data->UserName ?>" readonly="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" required="required" class="form-control col-md-7 col-xs-12" name="Password" value="<?php echo $data->Password ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" required="required" class="form-control col-md-7 col-xs-12" name="Email" value="<?php echo $data->Email ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Last Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="LastName" value="<?php echo $data->LastName ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="FirstName" value="<?php echo $data->FirstName ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Gender" value="1" <?php echo $data->Gender==1 ? "checked='checked'" : '' ; ?>> &nbsp; Male &nbsp;
                                        <input type="radio" name="Gender" value="0" <?php echo $data->Gender==0 ? "checked='checked'" : '' ; ?>> &nbsp; Female &nbsp;
                                        <input type="radio" name="Gender" value="2" <?php echo $data->Gender==2 ? "checked='checked'" : '' ; ?>> &nbsp; Orther &nbsp;
                                    </label>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- Image Thumb  -->
                <div class="x_title">
                    <h2>IMAGE <small> / Change thumb user</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"> 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div>
                                <label>
                                    <span class="btn btn-default btn-file btn-primary">
                                        Browse <input type="file" name="Image_upload" id="choosefile" />
                                    </span>
                                </label>
                            </div>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Preview Image</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="dvPreview">
                                <?php 
                                if(file_exists($data->Thumb)){
                                    echo "<img src='$data->Thumb' style='max-width:100px;max-height:100px' />";
                                }else{
                                    echo '<span style="padding-top:9px;display:block">No image selected</span>';    
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- Role  -->
                <div class="x_title">
                    <h2>ROLES <small> / Change user role</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content"> 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Role of User: </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div style='padding-top: 8px;'>
                                <?php 
                                $result = $this->db->query("select * from ttp_role where ID=$data->RoleID")->row();
                                echo $result ? $result->Title : "" ;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" id="recive_ajax">
                            <?php 
                            $result = $this->db->query("select * from ttp_sitetree")->result();
                            if(count($result)>0){
                                echo '<table class="table table-striped responsive-utilities jambo_table">';
                                echo '<thead>
                                        <tr class="headings">
                                            <th>STT</th>
                                            <th>Module </th>
                                            <th>Read</th>
                                            <th>Write</th>
                                            <th>Modify</th>
                                            <th>Delete</th>
                                            <th>Full</th>
                                        </tr>
                                    </thead><tbody>';
                                $i=0;
                                $permission = $data ? json_decode($data->DetailRole,true) : "" ;
                                foreach($result as $row){
                                    $i++;
                                    $read='';
                                    $write='';
                                    $modify='';
                                    $delete='';
                                    $full='';
                                    $temp=0;
                                    if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('r',$permission[$row->Classname])){
                                        $read='<i class="fa fa-check-square"></i>';
                                        $temp++;
                                    }
                                    if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('w',$permission[$row->Classname])){
                                        $write='<i class="fa fa-check-square"></i>';
                                        $temp++;
                                    }
                                    if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('m',$permission[$row->Classname])){
                                        $modify='<i class="fa fa-check-square"></i>';
                                        $temp++;
                                    }
                                    if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('d',$permission[$row->Classname])){
                                        $delete='<i class="fa fa-check-square"></i>';
                                        $temp++;
                                    }
                                    if($temp==4){
                                        $full='<i class="fa fa-check-square"></i>';
                                    }
                                ?>
                                
                                    <tr class="even pointer">
                                        <td class="a-center ">
                                            <?php echo $i; ?>
                                        </td>
                                        <td><?php echo $row->Title; ?></td>
                                        <td><?php echo $read ?></td>
                                        <td><?php echo $write ?></td>
                                        <td><?php echo $modify ?></td>
                                        <td><?php echo $delete ?></td>
                                        <td><?php echo $full ?></td>
                                        
                                    </tr>

                                <?php 
                                }
                                echo "</tbody></table>";
                            }
                        ?>
                        </div>
                    </div>
                    <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#choosefile").change(function(){
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if(file.type.match(imageType)){
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:100px;max-width: 100px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            }else{
                console.log("Not an Image");
            }
        });
    });
</script>