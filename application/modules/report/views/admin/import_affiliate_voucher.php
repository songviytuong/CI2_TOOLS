<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách voucher</h1>
			</div>
			<div class="block2">
				<form action="<?php echo base_url().ADMINPATH."/report/partner_affiliate/voucher" ?>" method="get">
					<a class="pull-left btn"><i class="fa fa-search" style="font-size:16px"></i></a><input type="text" class="form-control" name="keywords" value="<?php echo isset($_GET['keywords']) ? strip_tags($_GET['keywords']) : '' ; ?>" placeholder="Tìm mã voucher" style="width:250px;border:none;box-shadow:none;padding-left:0px;padding-right:0px;">
				</form>
			</div>
		</div>
		<div class="row" style="margin:0px;margin-top:-10px;margin-bottom:5px;">
			<a class="btn btn-danger" onclick="add_voucher()"><i class="fa fa-plus"></i> Tạo voucher mới</a>
		</div>
		<table class="table table-bordered table-hover">
			<tr>
				<th>STT</th>
				<th>Mã số voucher</th>
				<th>% giảm giá</th>
				<th>Giá trị giảm giá</th>
				<th>Thời gian áp dụng</th>
				<th>Tình trạng sử dụng</th>
				<th>Loại voucher</th>
				<th>Created</th>
				<th></th>
			</tr>
			<?php 
			if(count($data)>0){
				$arr = $this->lib->get_config_define("voucher","status");
				$arr1 = $this->lib->get_config_define("voucher","type");
				$i = $start+1;
				foreach($data as $row){
					echo "<tr>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>$i</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>$row->Code</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>$row->PercentReduce</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>$row->PriceReduce</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>".date('d/m/Y',strtotime($row->Startday)).' <i style="color:#999">đến</i> '.date('d/m/Y',strtotime($row->Stopday))."</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>".$arr[$row->Status]."</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>".$arr1[$row->UseOne]."</a></td>";
					echo "<td><a onclick='load_edit($row->ID,\"edit_voucher\")'>$row->Created</a></td>";
					echo "<td><a class='delete' href='".base_url().ADMINPATH."/report/partner_affiliate/delete/$row->ID'><i class='fa fa-trash'></i></a></td>";
					echo "</tr>";
					$i++;
				}
			}else{
				echo "<tr><td colspan='9'>Không tìm thấy dữ liệu</td></tr>";
			}
			?>
		</table>
		<?php echo $nav ?>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script>
	var baselink = $("#baselink_report").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function add_voucher(){
		enablescrollsetup();
		$(".over_lay .box_inner").css({'margin-top':'50px'});
		$(".over_lay .box_inner .block1_inner h1").html("Tạo mới voucher");
		$(".over_lay .box_inner .block2_inner").load(baselink+"partner_affiliate/add_voucher");        	
		$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function load_edit(id,action){
		enablescrollsetup();
		$(".over_lay .box_inner").css({'margin-top':'50px'});
		$(".over_lay .box_inner .block1_inner h1").html("Thông tin voucher");
		$(".over_lay .box_inner .block2_inner").load(baselink+"partner_affiliate/"+action+"/"+id);
		$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}
</script>