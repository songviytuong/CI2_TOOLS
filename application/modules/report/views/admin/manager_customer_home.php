<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<form action="<?php echo $base_link ?>setsessionsearch" method="post">
					<span>Lọc dữ liệu</span>
					<input type="text" name="Search" placeholder="Search..." class="form-control" />
					<input type="submit" class="btn btn-default" value="Filter" />
                    <a class="btn btn-default" href="<?php echo $base_link.'clearfilter' ?>">Clear Filter</a>
				</form>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary">Add new</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Tên khách hàng</th>
					<th>Phone1</th>
					<th>Phone2</th>
					<th>Address</th>
					<th>Action</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Name</td>";
						echo "<td>$row->Phone1</td>";
						echo "<td>$row->Phone2</td>";
						echo "<td>$row->Address</td>";
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;max-width: 400px;overflow: hidden;}
</style>