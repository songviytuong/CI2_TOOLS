<?php 
	$base_link = base_url().ADMINPATH.'/report/manager_system_menu/';
?>
<div class="containner">
	<form action="<?php echo $base_link.'update' ?>" method="post">
	<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
	<div class="row" style="margin:0px;">
		<div class="form-group">
			<div class="col-xs-6">
				<h3 style="margin:0px">Chỉnh sửa dữ liệu</h3>
			</div>
			<div class="col-xs-6 text-right">
				<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Lưu thông tin</button>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<label class="col-xs-3 control-label">Trạng thái hiển thị </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Loại menu </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Thuộc menu cha </label>
			<label class="col-xs-1 control-label"></label>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-3">
				<select name="Published" id="" class="form-control">
					<option value='1' <?php echo $data->Published==1 ? "selected='selected'" : "" ; ?>>Hiển thị</option>
					<option value='0' <?php echo $data->Published==0 ? "selected='selected'" : "" ; ?>>Không hiển thị</option>
				</select>
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<select name="Type" id="" class="form-control">
					<option value='0' <?php echo $data->Type==0 ? "selected='selected'" : "" ; ?>>Menu header</option>
					<option value='1' <?php echo $data->Type==1 ? "selected='selected'" : "" ; ?>>Menu content</option>
				</select>
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<select name="ParentID" id="" class="form-control">
					<option value='0'>Menu cấp cao nhất</option>
					<?php 
					$menu = $this->db->query("select * from ttp_system_menu")->result();
					if(count($menu)>0){
						foreach($menu as $row){
							$selected = $row->ID==$data->ParentID ? "selected='selected'" : "" ;
							echo "<option value='$row->ID' $selected>$row->Title</option>";
						}
					}
					?>
				</select>
			</div>
			<div class="col-xs-1"></div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<label class="col-xs-3 control-label">Tiêu đề menu </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Đường dẫn đích (sau ADMINPATH)</label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Thứ tự hiển thị cùng cấp menu</label>
			<label class="col-xs-1 control-label"></label>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-3">
				<input type="text" class="form-control required" name="Title" placeholder="Nhập tiêu đề menu cần khởi tạo ..." value="<?php echo $data->Title ?>" required />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="text" class="form-control" name="Alias" placeholder="Nhập đường dẫn đích ..." value="<?php echo $data->Alias ?>" />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="number" class="form-control" name="Position" placeholder="Nhập thứ tự hiển thị ..." value="<?php echo $data->Position ?>" />
			</div>
			<div class="col-xs-1"></div>
		</div>
	</div>
	<hr>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-6">
				<h3 style="margin:0px">Bắt trạng thái kích hoạt cho menu</h3>
			</div>
			<div class="col-xs-6 text-right">
				<a class="btn btn-default" onclick="addactivelinks(this)"><i class="fa fa-plus"></i> Thêm link</a>
			</div>
			<div class="col-xs-12" id="activelinks" style="margin-top: 10px;">
				<?php 
				$checkedlinks = json_decode($data->CurrentChecked,true);
				if(count($checkedlinks)>0){
					foreach($checkedlinks as $row){
						echo "<div class='col-xs-3'><input type='hidden' name='ActiveLinks[]' value='$row' /> <i class='fa fa-circle-o' aria-hidden='true' style='margin-right:5px;font-size:10px'></i> $row <a onclick='removethis(this)'>[x]</a></div>";
					}
				}
				?>
			</div>
		</div>
	</div>
	<hr>
	<div class="row" style="margin:0px;">
		<div class="form-group">
			<div class="col-xs-6">
				<h3 style="margin:0px">Tài khoản có thể nhìn thấy menu này</h3>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px;">
		<div class="form-group">
			<?php 
			$permission_user = $this->db->query("select * from ttp_system_menu_user where MenuID=$data->ID")->result();
			$arr_user = array();
			if(count($permission_user)>0){
				foreach($permission_user as $row){
					$arr_user[$row->UserID] = 1;
				}
			}
			$user = $this->db->query("select UserName,ID from ttp_user")->result();
			if(count($user)>0){
				foreach($user as $row){
					$checked = isset($arr_user[$row->ID]) ? "checked='checked'" : "" ;
					echo "<div class='col-xs-2'><input type='checkbox' name='UserList[]' value='$row->ID' onchange='activechange()' $checked /> $row->UserName</div>";
				}
			}
			?>
		</div>
	</div>
	<input type='hidden' name='Ischange' value='0' id='Ischange' />
	</form>
</div>
<script>
	function activechange(){
		$("#Ischange").val('1');
	}

	function addactivelinks(){
		var getValue = prompt("Điền link để kích hoạt menu này", "");
		if(getValue!=null){
			$("#activelinks").append('<div class="col-xs-3"><input type="hidden" name="ActiveLinks[]" value="'+getValue+'" /> <i class="fa fa-circle-o" aria-hidden="true" style="margin-right:5px;font-size:10px"></i> '+getValue+" <a onclick='removethis(this)'>[x]</a></div>");
		}
	}

	function removethis(ob){
		$(ob).parent('div').remove();
	}
</script>
