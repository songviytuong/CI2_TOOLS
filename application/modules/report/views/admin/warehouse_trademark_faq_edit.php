<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>

<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa câu hỏi của thương hiệu</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-8'>
								<input type='radio' name="Published" value="1" <?php echo $data->Status==1 ? 'checked="true"' : '' ; ?> /> Enable 
								<input type='radio' name="Published" value="0" <?php echo $data->Status==0 ? 'checked="true"' : '' ; ?> /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Thuộc thương hiệu</label>							
							<div class='col-xs-8'>
								<select class="form-control" name="TrademarkID">
									<option>Chọn thương hiệu</option>
									<?php foreach ($list_brand as $row) :?>
										<option value="<?php echo $row->ID ?>" <?php echo $data->TrademarkID == $row->ID ? 'selected' : '' ?>><?php echo $row->Title ?></option>
									<?php endforeach ?>
								</select>								
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Câu hỏi</label>
							<div class='col-xs-8'>								
								<textarea rows="5" class="form-control" name="Question" required ><?php echo isset($data->Question) ? $data->Question : '' ; ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Câu trả lời</label>
							<div class='col-xs-8'>
								<textarea class="form-control" name="Answer" rows="10" required><?php echo isset($data->Answer)? $data->Answer : '' ?> </textarea>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>

<style type="text/css">
	.logo, .banner{
		margin-top: 10px;
	}
	.logo img, .banner img{
		border: 1px solid #ccc;
		padding: 3px;
	}
	.banner img{
		width: 80px;
	}
</style>

<script type="text/javascript">
	$(function(){

	});
</script>