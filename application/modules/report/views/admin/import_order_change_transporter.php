<form action="<?php echo base_url().ADMINPATH.'/report/import_order/change_transport' ?>" method="POST">
	<div class="row">
		<div class="col-xs-2"><label class="control-label">Chọn nhà vận chuyển</label></div>
		<div class="col-xs-3">
			<select name="TransportID" class="form-control">
				<?php 
				$first = $data->TransportID;
				$transport = $this->db->query("select ID,Title from ttp_report_transport")->result();
				if(count($transport)>0){
					foreach($transport as $row){
						$selected = $data->TransportID==$row->ID ? 'selected="selected"' : '' ; 
						echo "<option value='$row->ID' $selected>$row->Title</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="col-xs-2"><label class="control-label">Chọn nhân viên</label></div>
		<div class="col-xs-3">
			<select name="UserID" class="form-control" id="Defaultchange" onchange="loadtransporter(this)">
				<?php 
				$transporter = $this->db->query("select a.ID,a.FirstName,a.LastName from ttp_user a,ttp_user_transport b where a.ID=b.UserID and b.TransportID=$first")->result();
				if(count($transporter)>0){
					foreach($transporter as $row){
						$select = $row->ID==$UserID ? 'selected="selected"' : '' ;
						echo "<option value='$row->ID' $select>$row->FirstName $row->LastName</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="col-xs-2"><button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Xác nhận</button></div>
	</div>
	<hr>
	<div class='row' id='info_transporter'></div>
	<hr>
	<div class="row">
		<div class="col-xs-2">Mã đơn hàng </div><div class="col-xs-10">: <b><?php echo $data->MaDH ?></b></div>
	</div>
	<div class="row">
		<div class="col-xs-2">Tên khách hàng </div><div class="col-xs-4">: <b><?php echo $data->Name ?></b></div>
		<div class="col-xs-2">Số điện thoại </div><div class="col-xs-4">: <b><?php echo $data->Phone ?></b></div>
	</div>
	<div class="row">
		<div class="col-xs-2">Tỉnh / Thành </div><div class="col-xs-4">: <b><?php echo $data->City ?></b></div>
		<div class="col-xs-2">Quận / Huyện </div><div class="col-xs-4">: <b><?php echo $data->District ?></b></div>
	</div>
	<div class="row">
		<div class="col-xs-2">Địa chỉ giao hàng </div><div class="col-xs-10">: <b><?php echo $data->AddressOrder ?></b></div>
	</div>
	<?php 
	echo "<input type='hidden' name='OrderID[]' value='$data->ID' />";
	?>
</form>

<script>
	
	function loadtransporter(ob){
		var transporter = $(ob).val();
		$("#info_transporter").load("<?php echo base_url().ADMINPATH.'/report/import_order/loadtransporter/' ?>"+transporter);
	}

	$("#Defaultchange").change();
</script>