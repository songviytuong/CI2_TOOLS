<?php

class Warehouse_model extends CI_Model {

    var $tablename = 'ttp_report_warehouse';

    function __construct() {

        parent::__construct();

        // Your own constructor code
    }

    function getGroupCity($orderby = 'c.ID', $sort = 'ASC') {
        $this->db->cache_on();
        $this->db->distinct();
        $this->db->select('w.CityID as ID,c.Title as Title');
        $this->db->from('ttp_report_warehouse w');
        $this->db->join('ttp_report_city c', 'c.ID = w.CityID', 'inner');
        $this->db->where('Published', 1);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get()->result();
        return $result;
    }

    function defineGroupCity() {
        $data_GroupCity = $this->getGroupCity();
        $arr = array();
        if ($data_GroupCity) {
            foreach ($data_GroupCity as $key => $row) {
                $json = $row;
                $arr[$key]['city_id'] = isset($json->ID) ? $json->ID : 0;
                $arr[$key]['city_name'] = isset($json->Title) ? $json->Title : "";
            }
        }
        return $arr;
    }

    #City

    function getListCityByCity($object) {
        $this->db->cache_on();
        $this->db->distinct();
        $this->db->select('w.DistrictID, d.Title as Title');
        $this->db->from('ttp_report_warehouse w');
        $this->db->join('ttp_report_district d', 'd.ID = w.DistrictID', 'INNER');
        $this->db->where('d.CityID', $object['id']);
        $result = $this->db->get()->result();
        return $result;
    }

    function defineListCityByCity($object) {
        $data_ListCityByArea = $this->getListCityByCity($object);
        $arr = array();
        if ($data_ListCityByArea) {
            foreach ($data_ListCityByArea as $key => $row) {
                $json = $row;
                $arr[$key]['district_id'] = isset($json->DistrictID) ? $json->DistrictID : -1;
                $arr[$key]['district_name'] = isset($json->Title) ? $json->Title : "";
            }
        }
        return $arr;
    }

    #District

    function getStoreListByDistrict($object) {
        $this->db->cache_on();
        $this->db->select('*,w.Title as Name,c.Title as CityName');
        $this->db->from('ttp_report_warehouse w');
        $this->db->join('ttp_report_city c', 'c.ID = w.CityID', 'inner');
        $this->db->where('w.DistrictID', $object['id']);
        $result = $this->db->get()->result();
        return $result;
    }

    function defineStoreListByDistrict($object) {
        $data_StoreListByDistrict = $this->getStoreListByDistrict($object);
        $arr = array();
        if ($data_StoreListByDistrict) {
            foreach ($data_StoreListByDistrict as $key => $row) {
                $json = $row;
                $arr[$key]['store_id'] = isset($json->ID) ? $json->ID : -1;
                $arr[$key]['store_code'] = isset($json->MaKho) ? $json->MaKho : "";
                $arr[$key]['store_name'] = isset($json->Name) ? $json->Name : "";
                $arr[$key]['store_address'] = isset($json->Address) ? $json->Address : "";
                $arr[$key]['store_phone'] = isset($json->Phone1) ? $json->Phone1 : "";
                $arr[$key]['store_cityname'] = isset($json->CityName) ? $json->CityName : "";
            }
        }
        return $arr;
    }
    
    #All Store List
    function getAllStoreList($object) {
        $this->db->cache_on();
        $this->db->select('*,w.Title as Name,c.Title as CityName');
        $this->db->from('ttp_report_warehouse w');
        $this->db->join('ttp_report_city c', 'c.ID = w.CityID', 'inner');
        if($object['id'] != -1){
            $this->db->where('w.DistrictID', $object['id']);
        }
        if($object['limit']){
            $this->db->limit($object['limit'], 0);
        }
        $result = $this->db->get()->result();
        return $result;
    }
    function defineAllStoreList($object){
        $data_AllStoreList = $this->getAllStoreList($object);
        $arr = array();
        if ($data_AllStoreList) {
            foreach ($data_AllStoreList as $key => $row) {
                $json = $row;
                $arr[$key]['store_id'] = isset($json->ID) ? $json->ID : -1;
                $arr[$key]['store_code'] = isset($json->MaKho) ? $json->MaKho : "";
                $arr[$key]['store_name'] = isset($json->Name) ? $json->Name : "";
                $arr[$key]['store_address'] = isset($json->Address) ? $json->Address : "";
                $arr[$key]['store_phone'] = isset($json->Phone1) ? $json->Phone1 : "";
                $arr[$key]['store_cityname'] = isset($json->CityName) ? $json->CityName : "";
            }
        }
        return $arr;
    }
    
    #All Store List
    function getStoreListByCity($object) {
        $this->db->cache_on();
        $this->db->select('*,w.Title as Name,c.Title as CityName');
        $this->db->from('ttp_report_warehouse w');
        $this->db->join('ttp_report_city c', 'c.ID = w.CityID', 'inner');
        if($object['id'] != -1){
            $this->db->where('w.CityID', $object['id']);
        }
        if($object['limit']){
            $this->db->limit($object['limit'], 0);
        }
        $result = $this->db->get()->result();
        return $result;
    }
    function defineStoreListByCity($object){
        $data_StoreListByCity = $this->getStoreListByCity($object);
        $arr = array();
        if ($data_StoreListByCity) {
            foreach ($data_StoreListByCity as $key => $row) {
                $json = $row;
                $arr[$key]['store_id'] = isset($json->ID) ? $json->ID : -1;
                $arr[$key]['store_code'] = isset($json->MaKho) ? $json->MaKho : "";
                $arr[$key]['store_name'] = isset($json->Name) ? $json->Name : "";
                $arr[$key]['store_address'] = isset($json->Address) ? $json->Address : "";
                $arr[$key]['store_phone'] = isset($json->Phone1) ? $json->Phone1 : "";
                $arr[$key]['store_cityname'] = isset($json->CityName) ? $json->CityName : "";
            }
        }
        return $arr;
    }
    
    #TotalStore
    function getTotalStore(){
        $this->db->select('COUNT(*) as COUNT');
        $result = $query = $this->db->get($this->tablename)->row_array();
        return $result['COUNT'];
    }

}