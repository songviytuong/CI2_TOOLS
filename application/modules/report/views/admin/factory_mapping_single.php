<div class="containner">
	<h3 class="step_number_title"><span>1</span> Chọn kho / nhà máy quét sản phẩm</h3>
	<div class="row">
		<div class="col-xs-12">
			<select class="form-control" name="WarehouseID" id="WarehouseID">
				<?php 
				$warehouse = $this->db->query("select MaKho,ID from ttp_report_warehouse")->result();
				$titlewarehouse = "";
				if(count($warehouse)>0){
					$warehousedefault = isset($_GET['warehouse']) ? $_GET['warehouse'] : 7 ;
					foreach($warehouse as $row){
						$titlewarehouse = $row->ID==$warehousedefault ? $row->MaKho : $titlewarehouse ;
						$selected = $row->ID==$warehousedefault ? "selected='selected'" : "" ;
						echo "<option value='$row->ID' $selected>$row->MaKho</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<h3 class="step_number_title"><span>2</span> Chọn loại sản phẩm & lô sản xuất</h3>
	<div class="row">
    	<div class="col-xs-6">
    		<select class="form-control" id="ProductsID">
    			<option value="0">-- Chọn loại sản phẩm --</option>
    			<?php 
    			$products = $this->db->query("select ID,MaSP,Title from ttp_report_products where CodeManage=1")->result();
    			$titleproducts = "";
    			if(count($products)>0){
    				$default = isset($_GET['products']) ? $_GET['products'] : 0 ;
    				foreach($products as $row){
    					$titleproducts = $row->ID==$default ? $row->Title : $titleproducts ;
    					$selected = $default==$row->ID ? "selected='selected'" : "" ;
    					echo "<option value='$row->ID' $selected>$row->MaSP - $row->Title</option>";
    				}
    			}
    			?>
    		</select>
    	</div>
    	<div class="col-xs-6">
    		<div class="input-group">
	    		<select class="form-control" id="ShipmentID">
	    			<option value="0">-- Chọn lô sản xuất --</option>
	    			<?php 
	    			$shipment = $this->db->query("select a.ID,a.ShipmentCode,b.MaSP from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and b.CodeManage=1 order by b.ID DESC")->result();
	    			$titleshipment="";
	    			if(count($shipment)>0){
	    				$default = isset($_GET['shipment']) ? $_GET['shipment'] : 0 ;
	    				foreach($shipment as $row){
	    					$titleshipment = $row->ID==$default ? $row->ShipmentCode : $titleshipment ;
	    					$selected = $default==$row->ID ? "selected='selected'" : "" ;
	    					echo "<option value='$row->ID' $selected>$row->MaSP - $row->ShipmentCode</option>";
	    				}
	    			}
	    			?>
	    		</select>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="add_shipment(this)"><i class="fa fa-plus"></i> Tạo lô mới</button>
                </span>
            </div>
    	</div>
    </div>
	<h3 class="step_number_title"><span>3</span> Quét mã QR Code</h3>
    <div class="row">
    	<div class="col-xs-12">
    		<input type="text" class="form-control" placeholder="Quét mã QR Code ..." onchange="enterqrcode(this)" autofocus="true" />
		</div>
    </div>
    <div class="row" style="margin:0px;" id="message_box">
		<?php 
		if(isset($_GET['success'])){
		?>
		<div class="alert alert-success">
			<i class="fa fa-check" aria-hidden="true"></i> Mã QR Code được quét thành công cho sản phẩm <b>"<?php echo $titleproducts ?>"</b> với số lô sản xuất <b>"<?php echo $titleshipment ?>"</b> tại kho / nhà máy <b>"<?php echo $titlewarehouse ?>"</b>
		</div>
		<?php 
		}
		?>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>

<style>
	table tr td{padding: 0px 10px !important;}
	table tr td input{border:none !important;box-shadow:none !important;}
</style>
<script>
	var baselink = "<?php echo $baselink?>";
	var arr = new Array();
	var arr_qrcode = new Array();
	
	function enterqrcode(ob){
		var data = $(ob).val();
		var code = data.split('=',2);
		code = code[1];
		if(code!=''){
			var warehouse = $("#WarehouseID").val();
			var products = $("#ProductsID").val();
			var shipment = $("#ShipmentID").val();
			var carton = $("#defaultproducts").val();
			var warehousetext = $('#WarehouseID').find('option:selected').text();
			$.ajax({
	            dataType: "json",
	            data: "data="+data+"&WarehouseID="+warehouse+"&WarehouseTitle="+warehousetext+"&ProductsID="+products+"&ShipmentID="+shipment,
	            url: baselink+'save_mapping_single',
	            cache: false,
	            method: 'POST',
	            success: function(result) {
	            	if(result.error==null){
		                var defaultwarehouse = $("#WarehouseID").val();
	                	var defaultproducts = $("#ProductsID").val();
	                	var defaultshipment = $("#ShipmentID").val();
	                	window.location = baselink+"map_qrcode_barcode_single?warehouse="+defaultwarehouse+"&products="+defaultproducts+"&shipment="+defaultshipment+"&success=true";
					}else{
						$("#message_box").html('<div class="alert alert-danger"><i class="fa fa-check" aria-hidden="true"></i> '+result.error);
						$(ob).val('');
						$(ob).focus();
					}
	            },
	            error: function(result){
	            	console.log(result);
	            }
	        });	
		}
	}

	function add_shipment(ob){
		var ProductsID = $("#ProductsID").val();
		if(ProductsID==0 || ProductsID==''){
			alert("Vui lòng chọn loại sản phẩm");
			return false;
		}
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
		$(".over_lay .box_inner .block2_inner").html("");
		$(".over_lay .box_inner .block2_inner").load("<?php echo base_url().ADMINPATH.'/report/factory/' ?>box_add_shipment/"+ProductsID);
		$(".over_lay").removeClass('in');
    	$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: "<?php echo base_url().ADMINPATH.'/report/factory/' ?>save_shipment",
            dataType: "json",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	if(result.Error==''){
	            	$(".over_lay").hide();
					disablescrollsetup();
					$(ob).removeClass("saving");
					$("#ShipmentID").append('<option value="'+result.ID+'">'+result.Title+'</option');
					$("#ShipmentID").val(result.ID);
				}else{
					alert("Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server !");
				}
            }
        });
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$(document).ready(function(){
		$("#defaultproducts").change();
	});

	$("#ProductsID").change(function(){
		var datachange = $(this).val();
		$("#ShipmentID").load("<?php echo base_url().ADMINPATH.'/report/factory/load_shipment_by_products/' ?>"+datachange);
	});
</script>
<style>
    .daterangepicker{width: auto;}
</style>