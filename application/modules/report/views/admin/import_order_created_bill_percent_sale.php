<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p><b>CÔNG TY CP DƯỢC PHẨM HOA THIÊN PHÚ</b></p>
				<p>10 Nguyễn Cửu Đàm, P.Tân Sơn Nhì, Q.Tân Phú, TPHCM</p>
				<p>Hotline: 19006033</p>
				<p>Email: support@hoathienphu.com.vn</p>
			</div>
			<div class="block1_2">
				<a id="print_page"><i class="fa fa-print"></i> Lưu & In Phiếu</a>
				<a id="save_page"><i class="fa fa-check-circle"></i> Lưu</a>
				<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/import_order/preview/$data->ID" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<?php
				$show = $data->OrderType==1 ? "GT" : "";
				$show = $data->OrderType==2 ? "MT" : $show;
				$show = $data->OrderType==4 ? "TD" : $show;
				$show = $data->OrderType==5 ? "NB" : $show;

				$Type = $data->OrderType==1 ? " and TypeExport=1" : "";
				$Type = $data->OrderType==2 ? " and TypeExport=2" : $Type;
				$Type = $data->OrderType==4 ? " and TypeExport=4" : $Type;
				$Type = $data->OrderType==5 ? " and TypeExport=5" : $Type;
				$export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$data->ID")->row();
				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=1 $Type")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
				$max = "BH".$show.$thisyear.$thismonth.'.TA.'.str_pad($max, 5, '0', STR_PAD_LEFT);
				?>
				<h1>Phiếu xuất kho</h1>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaXK : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<p>Mẫu số : 02-VT</p>
				<p>(Ban hành theo TT200/2014/TT-BTC ngày 22/12/2014 của bộ trưởng BTC)</p>
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $export ? $export->TKNO : "1311" ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $export ? $export->TKCO : "5115" ; ?>" /></td>
					</tr>
					<tr>
						<td>KPP</td>
						<?php
						$KPP = $data->OrderType==1 ? 'GT' : '' ;
						$KPP = $data->OrderType==2 ? 'MT' : $KPP ;
						$KPP = $data->OrderType==4 ? 'TD' : $KPP ;
						$KPP = $data->OrderType==5 ? 'NB' : $KPP ;
						?>
						<td><input type="text" id="KPP" value="<?php echo $export ? $export->KPP : $KPP ; ?>" /></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Họ tên người nhận:</li>
				<li><?php echo $data->OrderType==2 ? $data->Company : $data->Name ?> </li>
			</div>
			<div class="row">
				<li>Số điện thoại liên hệ:</li>
				<li><?php echo $data->Phone1 ?> </li>
			</div>
			<div class="row">
				<li>Lý do xuất kho:</li>
				<li><input type="text" id="lydoxuatkho" value="<?php echo $export ? $export->Lydoxuatkho : "Bán hàng DIVASHOP" ; ?>" placeholder="Điền lý do xuất kho" /></li>
			</div>
			<div class="row">
				<li>Xuất tại kho:</li>
				<li>
					<?php echo $data->KhoTitle ?>
					<input type="hidden" id="KhoID" value="<?php echo $data->KhoID ?>" />
				</li>
			</div>
			<div class="row">
				<li>Ghi chú khách hàng:</li>
				<li class="special"><?php echo $data->Note ?></li>
			</div>
			<div class="row">
				<li>Địa chỉ giao hàng:</li>
				<li class="special"><?php echo $data->AddressOrder ?></li>
			</div>
			<div class="row">
				<li>Hình thức xuất:</li>
				<li>
				<select id="hinhthucxuatkho">
					<option value='1' <?php echo $export && $export->Hinhthucxuatkho==1 ? "selected='selected'" : "" ; ?>>TA</option>
					<option value='0' <?php echo $export && $export->Hinhthucxuatkho==0 ? "selected='selected'" : "" ; ?>>NB</option>
				</select>
				</li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th rowspan='2'>Số TT</th>
					<th rowspan='2'>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th rowspan='2'>Mã số</th>
					<th rowspan='2'>Đơn vị tính</th>
					<th colspan='2'>Số lượng</th>
					<th rowspan='2'>Đơn giá</th>
					<th rowspan='2'>& chiết khấu</th>
					<th rowspan='2'>Giá trị chiết khấu</th>
					<th rowspan='2'>Giá sau chiết khấu</th>
					<th rowspan='2'>Thành tiền (VNĐ)</th>
				</tr>
				<tr>
					<th style="width:100px">Theo chứng từ</th>
					<th>Thực xuất</th>
				</tr>
				<?php
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,b.* from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
				$i=1;
				$total_amount = 0;
				if(count($details)>0){
					foreach($details as $row){
						$giaban = $row->Price+$row->PriceDown;
    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td style='text-align:left'>$row->MaSP</td>";
						echo "<td style='text-align:left'>$row->Donvi</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>$row->Amount</td>";
						echo "<td style='text-align:right'>".number_format($giaban)."</td>";
						echo "<td style='text-align:right'>".number_format($phantramck)."</td>";
						echo "<td style='text-align:right'>".number_format($row->PriceDown,2)."</td>";
						echo "<td style='text-align:right'>".number_format($row->Price,2)."</td>";
						echo "<td style='text-align:right'>".number_format($row->Total)."</td>";
						echo "</tr>";
						$total_amount+=$row->Amount;
						$i++;
					}
					if($data->Chietkhau>0){
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>Chiết khấu theo hợp đồng</td>";
						echo "<td style='text-align:left'></td>";
						echo "<td style='text-align:left'></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td style='text-align:right'>(".number_format($data->Chietkhau).")</td>";
						echo "</tr>";
						$i++;
					}
				}

				$reduce = $this->db->query("select a.*,b.Title from ttp_report_reduce_order a,ttp_report_reduce b where a.ReduceID=b.ID and a.OrderID=$data->ID")->result();
    			$arr_reduce = array();
    			if(count($reduce)>0){
    				foreach($reduce as $row){
    					echo "<tr>";
    					echo "<td>$i</td>";
						echo "<td>$row->Title $row->TimeReduce</td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td></td>";
						echo "<td style='text-align:right'>(".number_format($row->ValueReduce).")</td>";
    					echo "</tr>";
    					$arr_reduce[]='"data'.$row->ReduceID.'"';
    					$i++;
    				}
    			}

				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				$phantramchietkhau = round($data->Chietkhau/($data->Total/100));
				$tongtienhang = $data->Total - $data->Chietkhau;
		    	$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi-$data->Reduce;
				?>

				<tr>
					<td></td>
					<td>TỔNG CỘNG</td>
					<td></td>
					<td></td>
					<td><?php echo number_format($total_amount) ?></td>
					<td><?php echo number_format($total_amount) ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style='text-align:right;font-size:18px !important'>
					<?php echo number_format($tonggiatrithanhtoan) ?>
					</td>
				</tr>
			</table>
		</div>
		<div style='margin-bottom:20px'>Xuất kho theo đơn hàng số : <?php echo $data->MaDH.' ngày đặt hàng '.date('d/m/Y',strtotime($data->Ngaydathang)) ?></div>
		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
			</div>

			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
		</div>
	</div>
</div>
<style>body{background:#FFF;}</style>
<script>
	$("#print_page").click(function(){
	    window.print();
	});

	$("#hinhthucxuatkho").change(function(){
		var hinhthucxuatkho = $("#hinhthucxuatkho").val();
		$.ajax({
        	url: "<?php echo $base_link ?>get_next_warehouse",
            dataType: "html",
            type: "POST",
            context:this,
            data: "Hinhthucxuatkho="+hinhthucxuatkho+"&OrderID=<?php echo $data->ID ?>",
            success: function(result){
            	$("#next_MaXK").html(result);
            }
        });
	});

	$("#save_page").click(function(){
		$(this).addClass("saving");
		var TKNO 	= $("#TKNO").val();
		var TKCO 	= $("#TKCO").val();
		var KPP 	= $("#KPP").val();
		var Lydo 	= $("#lydoxuatkho").val();
		var KhoID 	= $("#KhoID").val();
		var hinhthucxuatkho 	= $("#hinhthucxuatkho").val();
		if(TKNO=="" || TKCO=="" || KPP=="" || Lydo=='' || KhoID=="" || hinhthucxuatkho==""){
			$(this).removeClass("saving");
			alert("Vui lòng điền đầy đủ các thông tin cần thiết trước khi lưu và in phiếu xuất kho !");
			return false;
		}else{
			$.ajax({
	        	url: "<?php echo $base_link ?>print_export_warehouse",
	            dataType: "html",
	            type: "POST",
	            context:this,
	            data: "OrderID=<?php echo $data->ID ?>&TKNO="+TKNO+"&TKCO="+TKCO+"&KPP="+KPP+"&Lydo="+Lydo+"&KhoID="+KhoID+"&hinhthucxuatkho="+hinhthucxuatkho,
	            success: function(result){
	            	$(this).removeClass("saving");
	            	alert("Lưu thông tin thành công !");
	            }
	        });
		}
	});
</script>
