<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Tạo mới client</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Lưu thông tin</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-10'>
								<input type='radio' name="Published" value="1" checked="true" /> Enable 
								<input type='radio' name="Published" value="0" /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Tên miền (Domain)</label>
							<div class='col-xs-10'>
								<select class="form-control" name="WebsiteID">
									<?php 
									$website = $this->db->query("select * from ttp_system_website")->result();
									if(count($website)>0){
										foreach($website as $row){
											echo "<option value='$row->ID'>$row->Domain</option>";
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Client ID</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ClientID" required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Client Key</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ClientKey" required /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label for="" class="col-xs-2 control-label">Client Password</label>
							<div class='col-xs-10'><input type='text' class="form-control required" name="ClientPassword" required /></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>