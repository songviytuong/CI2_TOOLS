<?php 
$segment = $this->uri->segment(4);
switch ($segment) {
	case '':
		$title = "Sản phẩm cần phải lấy";
		break;
	case 'shipping':
		$title = "Đơn hàng chờ giao hàng";
		break;
	case 'order_has_ship':
		$title = "Thống kê đơn hàng đã giao";
		break;
	default:
		$title = "";
		break;
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand"><?php echo $title; ?></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
          <li class='<?php echo $segment=='' ? "active" : "" ; ?>'><a href="<?php echo base_url().ADMINPATH."/report/transporter" ?>">Chờ lấy hàng</a></li>
			    <li class='<?php echo $segment=='shipping' ? "active" : "" ; ?>'><a href="<?php echo base_url().ADMINPATH."/report/transporter/shipping" ?>">Chờ giao hàng</a></li>
			    <li class='<?php echo $segment=='order_has_ship' ? "active" : "" ; ?>'><a href="<?php echo base_url().ADMINPATH."/report/transporter/order_has_ship" ?>">Thống kê giao nhận</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>