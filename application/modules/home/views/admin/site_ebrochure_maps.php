<div class="page-title">
    <div class="title_left">
        <h3>SITE GROUP
            <small></small>
        </h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Maps E-Brochure<small> / view all item</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a href="<?php echo $base_link . "maps_add/".$byID; ?>" class="btn btn-success" role="button"><i class="fa fa-plus-circle"></i> Add Maps</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>Detail</th>
                            <th>Data</th>
                            <th class="text-right last"><span class="nobr">Action</span>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($data_details) > 0) {
                            foreach ($data_details as $row) {
                                ?>
                                <tr class="even pointer">
                                    <td><a href='<?php echo $base_link . "maps_edit/$row->ID" ?>'><?= $row->Name; ?></a></td>
                                    <td><?= $row->Data; ?></td>
                                    <td class="text-right last">
                                        <a href='<?php echo $base_link . "maps_edit/$row->ID" ?>'><i class="fa fa-pencil-square-o"></i> Edit</a>
                                        &nbsp;&nbsp;&nbsp;
                                        <a href='<?php echo $base_link . "maps_delete/$row->ID" ?>' class='delete_link'><i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr class="even pointer">
                                <td class="a-center" colspan="6" style="text-align:center">Data is empty .</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
