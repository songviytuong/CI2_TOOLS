<?php 
	if(!isset($data)){
		if(isset($error) && $error!=''){
			echo "<div class='alert alert-danger text-center'>$error</div>";
		}
		echo "
			<form id='form-file' method=l'post' enctype='multipart/form-data'>
			<p class='text-center' style='color:#ccc;font-size:150px'><i class='fa fa-cloud-upload' aria-hidden='true'></i></p>
			<h4 class='text-center'>Vui lòng chọn file import từ máy tính</h4>
			<p class='text-center'><button class='btn btn-primary' style='position:relative;overflow:hidden;margin-top:20px'><input type='file' name='file' id='file_import' onchange='activeimport()' style='position:absolute;left:0px;top:0px;padding-left: 200px;height: 36px;cursor: pointer;' /><i class='fa fa-upload'></i> Brown file</button></p>
			<p class='text-center' style='margin-top:20px'>(*) chỉ chấp nhận file excel (xls,xlsx)</p></form>
		";
	}else{
		if(count($data)>0){
			echo "<div class='alert alert-success text-center'>Nội dung chi tiết file <b>\"$filename\"</b> bạn vừa tải lên hệ thống . Vui lòng kiểm tra lại thật kỹ trước khi nhấn lệnh nhập dữ liệu . <br>
				<p class='text-center' style='margin-top: 10px;'><a class='btn btn-success' onclick='active_import(this)'><i class='fa fa-upload'></i> Nhập vào hệ thống</a></p>
			</div>";
			echo "<h4>Map QR Code với số chứng từ cụ thể</h4>";
			echo "<div class='row' style='margin-bottom:10px;padding-bottom:10px;'><label class='control-label col-xs-3'>Chọn số chứng từ của kho</label><div class='col-xs-3'><select class='form-control' onchange='getbill(this)'>";
			$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
			if(count($warehouse)>0){
				foreach($warehouse as $row){
					$selected = $row->MaKho=='SPM' ? "selected='selected'" : "" ;
					echo "<option value='$row->ID' $selected>$row->MaKho</option>";
				}
			}
			echo "</select></div><label class='control-label col-xs-3'>Chọn số chứng từ cụ thể</label><div class='col-xs-3'><select class='form-control' id='Billcode'>";
			$result = $this->db->query("select a.Code,a.ID from ttp_report_transferorder_mobilization a,ttp_report_transferorder b where a.OrderID=b.ID and WarehouseReciver=7")->result();
	        if(count($result)>0){
	            foreach($result as $row){
	                echo "<option value='$row->ID'>$row->Code</option>";
	            }
	        }
			echo "</select></div></div>";
			
			echo "<table>";
			$i=1;
			foreach($data as $row){
				$td = $i==1 ? "th" : "td" ;
				$first = $i==1 ? "" : "treach" ;
				echo "<tr class='$first'>";
				if(count($row)>0){
					for($k=0;$k<count($row);$k++){
						echo "<$td class='td$k' style='width:auto'>".$row[$k]."</$td>";
					}
					echo $i==1 ? "<th></th>" : "<$td style='width:30px'><a title='Xóa mã QR CODE này ?' onclick='rmtr(this)'><i class='fa fa-times' aria-hidden='true'></i></a></$td>";
				}
				echo "</tr>";
				$i++;
			}
			echo "</table>";
		}else{
			echo "
				<form id='form-file' method=l'post' enctype='multipart/form-data'>
				<p class='text-center' style='color:#ccc;font-size:150px'><i class='fa fa-cloud-upload' aria-hidden='true'></i></p>
				<h4 class='text-center'>Vui lòng chọn file import từ máy tính</h4>
				<p class='text-center'><button class='btn btn-primary' style='position:relative;overflow:hidden;margin-top:20px'><input type='file' name='file' id='file_import' onchange='activeimport()' style='position:absolute;left:0px;top:0px;padding-left: 200px;height: 36px;cursor: pointer;' /><i class='fa fa-upload'></i> Brown file</button></p>
				<p class='text-center' style='margin-top:20px'>(*) chỉ chấp nhận file excel (xls,xlsx)</p></form>
			";
		}
	}
?>
