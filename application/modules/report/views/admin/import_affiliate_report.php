<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$numday = $this->lib->get_nume_day($startday,$stopday);
$arr_oldcustomer = array();
if(count($oldcustomer)>0){
	foreach($oldcustomer as $row){
		$arr_oldcustomer[$row->CustomerID] = '';
	}
}
$newcustomer = 0;
$reorder_customer = 0;
$ordersuccess_customer = 0;
$cancel_customer = 0;
$commission_newcustomer = 0;
$commission_oldcustomer = 0;
$sales_newcustomer = 0;
$sales_oldcustomer = 0;
$totalcustomer = count($data);
$arr_day = array();
if(count($data)>0){
	foreach($data as $row){
		$keytime = date('Y-m-d',strtotime($row->Ngaydathang));
		if(!isset($row->CustomerID)){
			$newcustomer++;
			if($row->Status==0){
				if(isset($arr_day[$keytime]['old'])){
					$arr_day[$keytime]['old'] = $arr_day[$keytime]['old']+($row->Total-$row->Chiphi-$row->Reduce);
				}else{
					$arr_day[$keytime]['old'] = ($row->Total-$row->Chiphi-$row->Reduce);
				}
				$commission_newcustomer = $commission_newcustomer+$row->AffiliateCost;
				$sales_newcustomer = $sales_newcustomer+($row->Total-$row->Chiphi-$row->Reduce);
			}
		}else{
			$reorder_customer++;
			if($row->Status==0){
				if(isset($arr_day[$keytime]['new'])){
					$arr_day[$keytime]['new'] = $arr_day[$keytime]['new']+($row->Total-$row->Chiphi-$row->Reduce);
				}else{
					$arr_day[$keytime]['new'] = ($row->Total-$row->Chiphi-$row->Reduce);
				}
				$commission_oldcustomer = $commission_oldcustomer+$row->AffiliateCost;
				$sales_oldcustomer = $sales_oldcustomer+($row->Total-$row->Chiphi-$row->Reduce);
			}
		}
		if($row->Status==0){
			$ordersuccess_customer++;
		}
		if($row->Status==1){
			$cancel_customer++;
		}
	}
}
?>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<h1 style="font-size: 20px;font-weight: bold;">BÁO CÁO TỔNG QUAN</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="row">
    	<div id="chart_div" style="width:100%;height:400px;"></div>
    </div>
    <hr>
    <div class="row">
    	<div class="col-xs-4">
    		<h5>Tổng số khách hàng đặt mua</h5>
    		<h2><?php echo number_format($totalcustomer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>Tổng số khách hàng mua thành công</h5>
    		<h2><?php echo number_format($ordersuccess_customer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>Tổng số khách hàng hủy</h5>
    		<h2><?php echo number_format($cancel_customer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>SL khách hàng mới</h5>
    		<h2><?php echo number_format($newcustomer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>SL khách hàng mua lại</h5>
    		<h2><?php echo number_format($reorder_customer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>Doanh thu từ khách hàng mới</h5>
    		<h2><?php echo number_format($sales_newcustomer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>Doanh thu từ khách hàng cũ</h5>
    		<h2><?php echo number_format($sales_oldcustomer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>Hoa hồng từ khách hàng mới(VNĐ)</h5>
    		<h2><?php echo number_format($commission_newcustomer) ?></h2>
    	</div>
    	<div class="col-xs-4">
    		<h5>Hoa hồng từ khách hàng cũ(VNĐ)</h5>
    		<h2><?php echo number_format($commission_oldcustomer) ?></h2>
    	</div>
    </div>
	<div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<style>
	.col-xs-4{border-right:1px solid #eee;margin-bottom:20px;min-height: 100px;}
	.col-xs-4 h5{text-transform: uppercase;}
</style>
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                location.reload();
	            }
	        });	
		});
    });
</script>
<!-- /datepicker -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Ngày', 'Khách mới', 'Khách cũ'],
          <?php 
        	$day = array();
        	for ($i=0; $i < $numday+1; $i++) { 
        		$time = date('d/m/Y',strtotime($startday)+$i*3600*24);
        		$keytime = date('Y-m-d',strtotime($startday)+$i*3600*24);
        		$old = isset($arr_day[$keytime]['old']) ? $arr_day[$keytime]['old'] : 0 ;
        		$new = isset($arr_day[$keytime]['new']) ? $arr_day[$keytime]['new'] : 0 ;
        		$day[] = "['$time',$old,$new]";
        	}
        	echo implode(',', $day);
        	?>]);

        var options = {
          title: 'Biểu đồ doanh thu từ khách hàng mang lại theo ngày',
          hAxis: {title: 'Ngày bán hàng',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>


