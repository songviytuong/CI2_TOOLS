<div class="containner">
	<form action="<?php echo ADMINPATH.'/report/warehouse/save_inventory_details' ?>" method="post" id="formdata">
		<div class="row">
			<div class="col-xs-3" style="position:relative;">
				<input type="text" class="form-control" placeholder="Tìm tên hoặc mã sản phẩm" onkeyup="get_products_list(this)" id="keywords" autocomplete="off">
				<div id="auto_complete" class='hidden'></div>
			</div>
			<div class="col-xs-2">
				<select name="WarehouseID" class="form-control">
					<option value="1">Kho DOHATO</option>
				</select>
			</div>
			<div class="col-xs-7">
				<span id='products'><b>Bạn đang chọn :</b> ----</span>
				<input type="hidden" name="productsid" id="productsid" value='0' />
			</div>
		</div>
		<hr>
		<div  id="box_add_row">
			<div class="row">
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
			</div>
			<div class="row">
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
			</div>
			<div class="row">
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
				<div class="col-xs-1">
					<input class="form-control" type="number" name="Amount[]" placeholder="SL" />
				</div>
				<div class="col-xs-3">
					<input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" />
				</div>
			</div>
		</div>
		<hr>
		<div class="alert alert-danger hidden"></div>
		<a onclick="add_row_input()" class="btn btn-primary"><i class="fa fa-plus"></i> Thêm khung nhập</a>
		<a onclick="send_data(this)" class="btn btn-danger"><i class="fa fa-pencil"></i> Lưu thông tin</a>
	</form>
</div>
<script>
	function add_row_input(){
		$("#box_add_row").append('<div class="row"><div class="col-xs-1"><input class="form-control" type="number" name="Amount[]" placeholder="SL" /></div><div class="col-xs-3"><input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" /></div><div class="col-xs-1"><input class="form-control" type="number" name="Amount[]" placeholder="SL" /></div><div class="col-xs-3"><input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" /></div><div class="col-xs-1"><input class="form-control" type="number" name="Amount[]" placeholder="SL" /></div><div class="col-xs-3"><input class="form-control" type="text" name="Note[]" placeholder="Ghi chú" /></div></div>');
	}

	function get_products_list(ob){
		var keywords = $(ob).val();
		if(keywords.length>2){
			$.post("<?php echo base_url().ADMINPATH.'/report/warehouse/get_products_list' ?>", {keywords: keywords}, function(result){
		        $("#auto_complete").html(result).removeClass('hidden');
		    });
		}else{
			$("#auto_complete").html('').addClass('hidden');
		}
	}

	function apply_products(ob,id){
		$("#products").html("<b>Bạn đang chọn :</b> "+$(ob).text());
		$("#auto_complete").html('').addClass('hidden');
		$("#keywords").val('');
		$("#productsid").val(id);
	}

	function send_data(){
		var productsid = $("#productsid").val();
		if(productsid>0){
			$("#formdata").submit();
		}else{
			$(".alert").removeClass('hidden').html('<i class="fa fa-warning"></i> Vui lòng chọn sản phẩm trước khi lưu thông tin');
			$("#keywords").focus();
		}
	}
</script>
<style>
	#auto_complete{position: absolute;
    top: 40px;
    left: 15px;
    right: 15px;
    background: #FFF;
    z-index: 9;
    width: 200%;
    box-shadow: 1px 3px 5px #f1f1f1;
    border-radius: 0px 0px 3px 3px;
    padding: 15px;
    border: 1px solid #e1e1e1;}
	#auto_complete p{padding:5px 0px;border-bottom:1px solid #eee;}
	#auto_complete p:last-child{border:none;}
	#products{
		    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 100%;
    display: block;
    padding: 8px 0px;
	}
</style>