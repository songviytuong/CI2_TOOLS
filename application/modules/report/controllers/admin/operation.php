<?php

class Operation extends Admin_Controller {

    public $user;
    public $classname = "operation";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);

        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar', 'admin/operation_sitebar', array('user' => $this->user));
        $this->template->write_view('header', 'admin/header', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $this->template->add_title('Operation Manager');

        $ga = new GoogleAuthenticator();
        $secret = $ga->createSecret();
//        echo "Secret is: " . $secret . "\n\n";
//        $qrCodeUrl = $ga->getQRCodeGoogleUrl('Lee', $secret);
//        echo "Google Charts URL for the QR-Code: " . $qrCodeUrl . "\n\n";

        $oneCode = $ga->getCode($secret);
//        echo "Checking Code '$oneCode' and Secret '$secret':\n";

        $checkResult = $ga->verifyCode($secret, $oneCode, 2);    // 2 = 2*30sec clock tolerance
        if ($checkResult) {
//            echo 'OK';
        } else {
//            echo 'FAILED';
        }

        $startday = $this->session->userdata("import_startday");
        $startday = $startday != '' ? $startday : date('Y-m-01', time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $this->load->model('orders_model', 'orders');
        $orders_data = $this->orders->getOrders(array('Status' => 2));

        $this->load->model('operation_model', 'operation');
        $pick_status = $this->operation->PickStatus();
        $store_option = $this->operation->StoreOption();

        $data = array(
            'startday' => $startday,
            'stopday' => $stopday,
            'orders_data' => $orders_data,
            'pick_status' => $pick_status,
            'store_option' => $store_option,
            'base_link' => base_url() . ADMINPATH . '/report/operation/',
        );

        $view = 'operation_home';
        $this->template->write_view('content', $view, $data);
        $this->template->render();
    }

    public function pickup_plan() {

        $startday = $this->session->userdata("import_startday");
        $startday = $startday != '' ? $startday : date('Y-m-01', time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $this->load->model('orders_model', 'orders');
        $orders_data = $this->orders->getOrders(array('Status' => 2));

        $this->load->model('operation_model', 'operation');
        $pick_status = $this->operation->PickStatus();
        $pickup_action = $this->operation->PickUpAction();


        $data = array(
            'startday' => $startday,
            'stopday' => $stopday,
            'orders_data' => $orders_data,
            'pick_status' => $pick_status,
            'pickup_action' => $pickup_action,
            'base_link' => base_url() . ADMINPATH . '/report/operation/',
        );

        $view = 'operation_pickup_plan';
        $this->template->write_view('content', $view, $data);
        $this->template->render();
    }

}
