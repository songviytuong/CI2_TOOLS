<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>
    </div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link . "update" ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel tab1 tabcontent">
                    <div class="x_title">
                        <h2>BANNERS <small> / Edit banner</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="ID" value="<?php echo $data ? $data->ID : 0; ?>" />
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Positon</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label>
                                        <select name="PositionID" class="form-control" id="PositionSelect">
                                            <?php
                                            $result = $this->db->query("select * from ttp_banner_position")->result();
                                            if (count($result) > 0) {
                                                foreach ($result as $row) {
                                                    $selected = $row->ID == $data->PositionID ? "selected='selected'" : "";
                                                    $title = ($row->Description) ? $row->Description : $row->Title;
                                                    echo "<option value='$row->ID' title='$row->Description' $selected>$title</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="recive_ajax">

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image
                                <span class="btn btn-default btn-file btn-primary">
                                    Browse <input type="file" name="Image_upload" id="choosefile" />
                                </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="dvPreview">
                                    <?php
                                    if (file_exists($data->Thumb)) {
                                        echo "<img src='$data->Thumb' style='max-width:100%' />";
                                    } else {
                                        echo '<span style="padding-top:9px;display:block">No image selected</span>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Start/End</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input id="Input_Startday" type="text" class="form-control" name="start" value="<?= $data->Start ?>"/>

                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input id="Input_Stopday" type="text" class="form-control"  name="end" value="<?= $data->End ?>"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <select name="store[]" multiple class="form-control">
                                        <?php 
                                            foreach($warehouse as $row){
                                        ?>
                                        <option value="<?=$row['city_id']?>" <?=(in_array($row['city_id'], json_decode($data->Store))) ? "selected='selected'":"";?>><?=$row['city_name']?></option>
                                            <?php } ?>
                                    </select>
                                </div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
                <div class="x_panel tab2 tabcontent">
                    <div class="x_title">
                        <h2>BANNERS <small> / Edit banner</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content"> 
                        <div class="form-group" id="recive_ajax_en">

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</form>
</div>
</div>

<script>
    $(document).ready(function () {
        $('#Input_Startday,#Input_Stopday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD 00:00:00',

        });

        $.ajax({
            url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_view_by_ID_banner/' . $data->ID ?>",
            success: function (result) {
                $("#recive_ajax").html(result);
            }
        });

        $.ajax({
            url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_view_by_ID_banner/' . $data->ID ?>" + "/en",
            success: function (result) {
                $("#recive_ajax_en").html(result);
            }
        });

        $("#PositionSelect").change(function () {
            var ID = parseInt($(this).val());
            if (ID ==<?php echo $data->PositionID ?>) {
                var url = "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_view_by_ID_banner/' . $data->ID ?>";
            } else {
                var url = "<?php echo base_url() . ADMINPATH . '/home/system_ajax/site_load_view_by_position_banner/' ?>" + ID;
            }
            $.ajax({
                url: url,
                success: function (result) {
                    $("#recive_ajax").html(result);
                }
            });
            $.ajax({
                url: url + "/en",
                success: function (result) {
                    $("#recive_ajax_en").html(result);
                }
            });
        });

        $("#choosefile").change(function () {
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if (file.type.match(imageType)) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:300px;max-width: 400px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            } else {
                console.log("Not an Image");
            }
        });
    });
</script>