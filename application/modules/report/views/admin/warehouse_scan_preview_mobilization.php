<?php 
$bill = $this->db->query("select * from ttp_report_transferorder_mobilization where OrderID=$data->ID")->row();
?>

<div class="containner">
	<div class="bill_print mobilization">
		<div class="block1">
			<div class="block1_1">
				<img src="public/admin/images/logo.png" />
				<p class="title">CÔNG TY TNHH TM DV TRẦN TOÀN PHÁT</p>
				<p>Đại chỉ : 246/9 Bình Quới, Phường 28, Quận Bình Thạnh, Tp.HCM</p>
				<p>Số : <b><?php echo $bill ? $bill->Code : "" ; ?></b></p>
			</div>
			<div class="block1_3">
				<h1>LỆNH ĐIỀU ĐỘNG</h1>
				<p style='font-style:italic'>Tp.Hồ Chí Minh, Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Nơi đi</li>
				<li class="special">: <?php echo $data->SenderTitle.' - '.$data->SenderAddress ?></li>
			</div>
			<div class="row">
				<li>Nơi đến</li>
				<li class="special">: <?php echo $data->ReciverTitle.' - '.$data->ReciverAddress ?></li>
			</div>
			<div class="row">
				<li>Lý do điều động</li>
				<li>: <?php echo $bill ? $bill->Note : "Chuyển kho bán hàng" ; ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th>Số TT</th>
					<th>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th>Mã số</th>
					<th>Đơn vị tính</th>
					<th>Số lượng</th>
					<th>Ghi chú</th>
				</tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_transferorder_details a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td>".number_format($row->TotalExport)."</td>";
						echo "<td>".$row->Note."</td>";
						echo "</tr>";
						$i++;
					}
				}
				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				?>
				
				<tr style="border-top:1px solid #ccc;">
					<td colspan="2" style="text-align:center"><b>Ban giám đốc duyệt</b></td>
					<td colspan="2" style="text-align:center"><b>Phụ trách đơn vị</b> </td>
					<td colspan="3" style="text-align:center"><b>Người đề nghị</b></td>
				</tr>
				<tr>
					<td colspan="2" style="height:100px"></td>
					<td colspan="2"></td>
					<td colspan="3"></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<style>
	.body_content .containner .bill_print{
	    background: #FFF;
	    padding: 25px;
	    border: 1px solid #ccc;
	}
</style>

