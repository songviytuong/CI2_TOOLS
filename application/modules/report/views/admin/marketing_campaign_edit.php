<?php
    $readonly = "";
    $disabled = "";
    $hidden = "";
    
    $disabled_priority = "";
    $disabled_success = "";
    $disabled_status = "";
    
    $addKPI = "";
    $deleteKPI = "";
    $open = "";
    $editMarket = "";
    $hoverMarket = "";
    
    $addPlaner = "";
    $deletePlaner = "";
    $attach = "";
    
    $disabled_addPlaner = "";
    $isAfter = false;
    
    
    if($market_status_active == 0){
        $addKPI = "add";
        $deleteKPI = "deleteKPI";
        $addPlaner = "planer";
        $attach = "attach";
        $editMarket = "editMarket";
        $hoverMarket = "hoverMarket";
        $disabled_success = "disabled";
        $deletePlaner = "deletePlaner";
        $open = "open";
    }else if($market_status_active == 1){
        $hidden = "hidden";
        $readonly = "readonly";
        $disabled = "disabled";
        $open = "open";
        $editMarket = "editMarket";
        $hoverMarket = "hoverMarket";
        $addPlaner = "planer";
        $attach = "attach";
        $deleteKPI = "deleteKPI";
        $deletePlaner = "deletePlaner";
    }else if($market_status_active == 2){
        $hidden = "hidden";
        $disabled_success = "disabled";
        $disabled_priority = "disabled";
        $disabled_status = "disabled";
        $open = "open";
        $attach = "attach";
        $readonly = "readonly";
        $editMarket = "editMarket";
        $disabled = "disabled";
        $disabled_addPlaner = "disabled";
        $deletePlaner = "deletePlaner";
    }else if($market_status_active == 3){
        $readonly = "readonly";
        $disabled = "disabled";
        $open = "open";
        $editMarket = "editMarket";
        $hoverMarket = "hoverMarket";
        $addPlaner = "planer";
        $attach = "attach";
        $deleteKPI = "deleteKPI";
        $deletePlaner = "deletePlaner";
    }else if($market_status_active == 4){
        $readonly = "readonly";
        $disabled = "disabled";
        $open = "open";
        $editMarket = "editMarket";
        $hoverMarket = "hoverMarket";
        $addPlaner = "planer";
        $attach = "attach";
        $deleteKPI = "deleteKPI";
        $deletePlaner = "deletePlaner";
    }else if($market_status_active == 5){
        $disabled_addPlaner = "disabled";
        $hidden = "hidden";
        $readonly = "readonly";
        $disabled = "disabled";
        $disabled_priority = "disabled";
        $disabled_success = "disabled";
        $attach = "attach";
        $addPlaner = "planer";
        $editMarket = "editMarket";
        $disabled_status = "disabled";
        $deletePlaner = "deletePlaner";
    }else if($market_status_active == 6){
        $readonly = "readonly";
        $disabled = "disabled";
        $open = "open";
        $editMarket = "editMarket";
        $hoverMarket = "hoverMarket";
        $addPlaner = "planer";
        $attach = "attach";
        $deletePlaner = "deletePlaner";
    }else if($market_status_active == 7){
        $readonly = "readonly";
        $disabled = "disabled";
        $addPlaner = "planer";
        $attach = "attach";
        $disabled_success = "disabled";
        $disabled_priority = "disabled";
        $disabled_status = "disabled";
        $deletePlaner = "deletePlaner";
    }
    
    if($this->user->IsAdmin==1){
        
    }
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>CHIẾN DỊCH MARKETING</h1>
        </div>
        <div class="block2">
            <div class="btn-group">
                <?php if($this->user->IsAdmin==1){?>
                    <?php if($market_status_active == 5){ ?>
                        <a class="btn btn-info updateStatus" rel="6"><i class="fa fa-check"></i> Duyệt</a>
                        <a class="btn btn-danger updateStatus" rel="7"><i class="fa fa-times-circle"></i> Không Duyệt</a>
                    <?php } ?>
                <?php } else { ?>
                
                <?php if($market_status_active == 7){ ?>
                <a class="btn btn-default resetKPI" rel="<?=$market_id;?>"><i class="fa fa-edit"></i> Làm mới</a>
                <?php } else if($market_status_active == 6){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 5){ ?>
                <?php if($this->user->IsAdmin==1){?>
                <a class="btn btn-info updateStatus" rel="6"><i class="fa fa-check"></i> Duyệt</a>
                <a class="btn btn-danger updateStatus" rel="7"><i class="fa fa-times-circle"></i> Không Duyệt</a>
                <?php } else { ?>
                <a class="btn btn-danger"><i class="fa fa-info"></i> Chờ duyệt</a>
                <?php } ?>
                <!--<a class="btn btn-default resetKPI hidden" rel="<?=$market_id;?>"><i class="fa fa-edit"></i> Sửa chữa</a>-->
                <?php } else if($market_status_active == 4){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 3){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 2){ ?>
                <?php } else if($market_status_active == 1){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 0){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Lưu nháp</a>
                <a class="btn btn-danger updateStatus" rel="5"><i class="fa fa-location-arrow"></i> Chuyển duyệt</a>
                <?php } ?>
                
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
        $startParent = $this->db->query("select start as startParent,id as parentid from ttp_marketing_campaign_details where id = (select min(id) as min from ttp_marketing_campaign_details Where market_id = $market_id and parent=0)")->row();
        $parentid = ($startParent) ? $startParent->parentid : 0;
        $startAction = $this->db->query("select action_start as startAction from ttp_marketing_campaign_details where id = (select min(id) as min from ttp_marketing_campaign_details Where market_id = $market_id and parent= ".$parentid.")")->row();
        if($startAction){ $startParent = $startParent->startParent; }else{ $startParent = date('Y-m-d H:i:s'); }
        if($startAction){ $startAction = $startAction->startAction; }else{ $startAction = date('Y-m-d H:i:s'); }
        $endParent = $this->db->query("select end as endParent,id as parentid from ttp_marketing_campaign_details where id = (select max(id) as max from ttp_marketing_campaign_details Where market_id = $market_id and parent=0)")->row();
        $endparentid = ($endParent) ? $endParent->parentid : 0;
        $endAction = $this->db->query("select action_end as endAction from ttp_marketing_campaign_details where id = (select max(id) as max from ttp_marketing_campaign_details Where market_id = $market_id and parent=$endparentid)")->row();
        if($endParent){ $endParent = $endParent->endParent; }else{ $endParent = date('Y-m-d H:i:s'); }
        if($endAction){ $endAction = $endAction->endAction; }else{ $endAction = date('Y-m-d H:i:s'); }
    ?>
    <form name="frmMarket" role="form" data-toggle="validator" id="frmMarket" method="post">
        <div class="row">
            <div class="col-lg-2"><label for="">
                Tên chiến dịch:</label>
            </div>
            <div class="col-lg-10">
                <input type="text" name="market_name" id="market_name" class="form-control" value="<?=$dataParent->market_name;?>" placeholder="Nhập tên chiến dịch..." <?=$readonly?>/>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label for="">
                Mục tiêu chiến dịch:</label>
            </div>
            <div class="col-lg-10">
                <textarea name="market_desc" id="market_desc" class="form-control" placeholder="Nhập nội dung mô tả cho chiến dịch..." <?=$readonly?>/><?=$dataParent->market_description;?></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label for="">
                Phân loại:</label>
            </div>
            <div class="col-lg-4">
                <select id="market_type" name="market_type[]" class="form-control" <?=$disabled?>>
                    <?php
                        $market_type = $this->define_model->get_order_status('type','campaign','position','asc');
                        foreach($market_type as $key=>$type){
                    ?>
                    <option value="<?=$type->code;?>" <?=(in_array($type->code, $type_active)) ? "selected":''?>><?= $type->name; ?></option>
                    <?php } ?>
                </select>
            </div>
        
            <div class="col-lg-2"><label for="">
                Website mục tiêu:</label>
            </div>
            <div class="col-lg-4">
                <select id="market_domain" name="market_domain[]" class="form-control" <?=$disabled?>>
                    <?php
                        foreach($domain as $key=>$dom){
                    ?>
                    <option value="<?=$dom->ID;?>" <?=(in_array($dom->ID, $domain_active)) ? "selected":''?>><?= $dom->DomainName; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-2"><label for="">
                Người phụ trách:</label>
            </div>
            <div class="col-lg-4">
                <select id="owner" name="owner[]" class="form-control" <?=$disabled?>>
                    <?php
                        foreach($owner as $key=>$own){
                    ?>
                    <option value="<?=$own->ID;?>" <?=(in_array($own->ID, $owner_active)) ? "selected":''?>><?=$own->FirstName." ".$own->LastName; ?></option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="col-lg-2"><label for="">
                Người hỗ trợ:</label>
            </div>
            <div class="col-lg-4">
                <select id="owner_action" name="owner_action[]" multiple="multiple" <?=$disabled?>>
                    <?php
                        foreach($owner as $key=>$own){
                    ?>
                    <option value="<?=$own->ID;?>" <?=(in_array($own->ID, $owner_action_active)) ? "selected":''?> <?=(in_array($own->ID, $owner_active)) ? "disabled":''?>><?=$own->FirstName." ".$own->LastName; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-4"><br/><label for="">Báo cáo các hoạt động chiến dịch:</label>
                <br/>
                <a class="btn btn-danger <?=$addKPI?> <?=$disabled?>"><i class="fa fa-plus"></i> Thêm mới KPI</a>
                <a class="btn btn-default listDVT"><i class="fa fa-list"></i> Đơn vị tính (ĐVT)</a>
                
            </div>
            <div class="col-lg-8"></div>
            <div class="col-lg-12">
                <div class="table_data">
                <table id="table_data">
                    <tr>
                        <th class="text-center <?=$hidden;?>" style="width:60px;">Xóa</th>
                        <?php if($hidden){?>
                        <th class="text-center" style="width:60px;">STT</th>
                        <?php } ?>
                        <th class="text-center col-xs-1 hidden">Chi tiết</th>
                        <th class="text-center col-xs-2">Tên KPI</th>
                        <th class="text-center" colspan="2">Mục tiêu</th>
                        <th class="text-center col-xs-1 hidden">Kế hoạch</th>
                        <th class="text-center">File</th>
                    </tr>
                    
                    <?php
                        $market_symbol = $this->define_model->get_order_status('symbol','campaign','position','asc');
                        $array_symbol = array();
                        foreach($market_symbol as $key=>$ite){
                            $code = (int)$ite->code;
                            $array_symbol[$code] = $ite->name;
                        }
                        $i = 1;
                        if($i<10){
                        $stt = str_pad($i,2,"0", STR_PAD_LEFT);
                        }
                        foreach($data as $row){
                            $valParent = json_decode($row->value);
                            $actionTitle = json_decode($row->action_title);
                            $symParent = json_decode($row->symbol);
                            #Hiển thị + kế hoạch
                            $countPlaner = $this->db->query("SELECT count(*) as count from ttp_marketing_campaign_details WHERE parent=$row->id")->row()->count;
                    ?>
                    <tr class="opentd tr_<?=$row->id;?>">
                        <td class="text-center <?=$hidden;?>"><i style="cursor: pointer" class="fa fa-trash-o <?=$deleteKPI;?>" rel="<?=$row->id;?>" data-token="<?=substr(md5('REMOVE'.$row->id),0,8);?>"></i></td>
                        <?php if($hidden){?>
                        <td class="text-center"><?=$stt;?></td>
                        <?php } $i++; ?>
                        <td class="text-center hidden"><i style="cursor: pointer" class="fa fa-chevron-down <?=$open;?>" rel="<?=$row->id;?>"></i></td>
                        <td class="col-xs-2 <?=$hoverMarket?>"><a class="<?=$editMarket?> namer_<?=$row->id;?>" rel="<?=$row->id?>" ><?=$row->name;?></a><br/>
                            <small>(<?=$row->description;?>)</small>
                            <span class="pull-right text-success"><i class="fa fa-edit edit<?=$row->id?> hidden <?=($market_status_active != 5) ? 'editMarket':''?>" rel="<?=$row->id?>" style="cursor: pointer"></i></span></td>
                        <td class="text-center" colspan="2">
                            <?php
                                $cnt = count($valParent);
                                if($cnt == 1 || $cnt == 2 || $cnt == 3){
                                    $class = 2;
                                }else{
                                    $class = 1;
                                }
                                $col = $cnt * 2;
                            ?>
                            <table style="font-size:11px;">
                                <tr>
                                    <th class="hidden"></td>
                                    <th class=""></th>
                                    <?php
                                        $j=0;
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $title = $actionTitle[$j];
                                                $symbol = $symParent[$j];
                                                $j++;
                                    ?>
                                    <th class="text-center col-xs-<?=$class;?>"><?=$title;?> (<?=$array_symbol[$symbol]?>)</th>
                                    <?php } else { ?>
                                    <th class="text-center col-xs-<?=$class;?> hidden">ĐVT</th>
                                    <?php } ?>
                                    <?php } ?>
                                    <th class="text-center">Bắt đầu</td>
                                    <th class="text-center">Kết thúc</td>
                                    <th class="text-center">Ngân sách</td>
                                </tr>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">Plan</td>
                                    <?php
                                        $j=0;
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $value = $valParent[$j];
                                                $symbol = $symParent[$j];
                                                $j++;
                                    ?>
                                    <td class="col-xs-<?=$class;?>"><?=number_format($value);?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"><?=$array_symbol[$symbol]?></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>"><?=date('d-M-y',strtotime($row->start));?></td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=date('d-M-y',strtotime($row->end));?></td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format($row->money_need);?></td>
                                </tr>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">Actual</td>
                                    <?php
                                    $countActual = $this->db->query("SELECT sum(money_action) as cntActual, count(*) as c FROM ttp_marketing_campaign_details WHERE market_id = $market_id and parent=$row->id")->row();
                                    $cntA = $countActual->c;
                                    $countActual = ($countActual) ? $countActual->cntActual : 0;
                                    $action = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE market_id = $market_id and parent=$row->id")->result();
                                    $rss = array();
                                    foreach($action as $ke=>$aaa){
                                        $temp = json_decode($aaa->action,true);
                                        if(count($temp)>0){
                                            foreach($temp as $key=>$item){
                                                if(isset($rss[$key])){
                                                    $rss[$key] = $rss[$key]+$item;
                                                }else{
                                                    $rss[$key] = $item;
                                                }
                                            }
                                        }
                                    }
                                    
                                    $sumActual = 0;
                                    $j=0;
                                    $_start_action = ($startAction) ? date('d-M-y',strtotime($startAction)) : date('d-M-y');
                                    $_end_action = ($endAction) ? date('d-M-y',strtotime($endAction)) : date('d-M-y');
                                    for($i = 1; $i<=$col; $i++){
                                        if ($i % 2 == 1) {
                                            $value = $valParent[$j];
                                            $symbol = $symParent[$j];
                                            $sumActual = ($rss) ? $rss[$j] : 0;
                                            $j++;
                                                
                                    ?>
                                    <td class="col-xs-<?=$class;?>"><?=number_format($sumActual);?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$_start_action;?></td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$_end_action;?></td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format($countActual);?></td><!--Count Actual-->
                                </tr>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">Variance</td>
                                    <?php
                                        $variance = (int)$row->money_need - $countActual;
                                        $percent_variancce = round(100 - ($variance / ($row->money_need / 100)),1)."%";
                                        $sumActual = 0;
                                        $j=0;
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $value = $valParent[$j];
                                                $symbol = $symParent[$j];
                                                $sumActual = ($rss) ? $rss[$j] : 0;
                                                $varianceRow = $value - $sumActual;
                                                $j++;
                                                
                                    ?>
                                    <td class="col-xs-<?=$class;?>"><?=number_format($varianceRow)?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format($variance);?></td>
                                </tr>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">% Plan</td>
                                    <?php
                                        $j=0;
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $value = $valParent[$j];
                                                $symbol = $symParent[$j];
                                                $sumActual = ($rss) ? $rss[$j] : 0;
                                                $percent_plan = round($sumActual / ($value / 100),1)."%";
                                                $j++;
                                    ?>
                                    <td class="col-xs-<?=$class;?>"><?=$percent_plan?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$percent_variancce;?></td>
                                </tr>
                            </table>
                        </td>
                        <td class="text-center hidden">
                            <?php if($countPlaner != 0){?>
                            <i class="fa fa-search-plus <?=$addPlaner;?>" data-parent="<?=$row->id;?>" rel="1" style="cursor: pointer;"></i>
                            <?php } else { ?>
                            <i class="fa fa-plus <?=$addPlaner;?>" rel="0" data-parent="<?=$row->id;?>" style="cursor: pointer;"></i>
                            <?php } ?>
                        </td>
                        
                        <td class="text-center"><i style="cursor: pointer;" class="fa fa-paperclip <?=$attach;?>" rel="<?=$row->id?>"></i></td>
                    </tr>
                    <tr>
                        <td class="text-center" colspan="10">
                            <div class="text-left" style="padding-bottom:7px;">
                                <a class="btn btn-danger <?=$addPlaner;?> <?=$disabled_addPlaner;?>" data-parent="<?=$row->id;?>" rel="1"><i class="fa fa-plus-circle"></i> Hoạt động</a>
                                <a class="<?=$open;?>" rel="<?=$row->id;?>" style="font-size:85%;"><i class="fa fa-chevron-up"></i> Thu gọn / mở rộng</a>
                            </div>
                            <table id="table_data" class="show_<?=$row->id?>">
                                <tr>
                                    <?php if($deletePlaner){?>
                                    <th class="text-center">Xóa</th>
                                    <?php } ?>
                                    <th class="text-center col-xs-2">Hành động</th>
                                    <th class="text-center">Mục tiêu</th>
                                    <th class="text-center">File</th>
                                    
                                </tr>
                                <?php
                                    $array_symbol = array();
                                    foreach($market_symbol as $key=>$ite){
                                        $code = (int)$ite->code;
                                        $array_symbol[$code] = $ite->name;
                                    }
                                    
                                    $ns = $row->money_need;
                                    
                                    $resultChild = $this->db->query("SELECT * FROM ttp_marketing_campaign_details WHERE parent=$row->id")->result();
                                    $sumMoneyChild = $this->db->query("SELECT sum(money_action) as totalmoney FROM ttp_marketing_campaign_details WHERE parent=$row->id")->row();
                                    
                                    $overNS = (int)$sumMoneyChild->totalmoney - (int)$ns;
                                    $k = 0;
                                    foreach ($resultChild as $rowChild){
                                        $val = json_decode($rowChild->value);
                                        $val = ($val) ? $val[0] : 0;
                                        
                                        $symChild = json_decode($rowChild->symbol);
                                        $symChild = ($symChild) ? $symChild[0] : 0;
                                        
                                        //$percent_value = ((int)$val / $valParent)*100;
                                        $percent_money = round($rowChild->money_action / ($ns/100),1);
                                ?>
                                <tr>
                                    <?php if($deletePlaner){?>
                                    <td><i class="fa fa-trash-o" style="cursor: pointer;" onclick="removePlaner(<?=$rowChild->id?>,<?=$market_status_active?>);"></i></td>
                                    <?php } ?>
                                    <td class="text-left hoverPlaner"><a class="editPlaner" rel="<?=$rowChild->id?>"><?=strip_tags($rowChild->name);?></a>
                                        <?php if($deletePlaner) {?>
                                        <span class="pull-right text-danger"><i class="fa fa-times edit<?=$rowChild->id?> hidden" onclick='removePlaner(<?=$rowChild->id?>,<?=$market_status_active?>);' style="cursor: pointer"></i></span>
                                        <?php } ?>
                                    </td>
                                    <td class="text-right">
                                        <?php
                                            $valChild = json_decode($rowChild->value);
                                            $cnt = count($valChild);
                                            if($cnt == 1 || $cnt == 2 || $cnt == 3){
                                                $class = 2;
                                            }else{
                                                $class = 1;
                                            }
                                            $col = $cnt * 2;
                                        ?>
                                        <table style="font-size:11px;">
                                            <tr>
                                                <th class="hidden"></td>
                                                <th class=""></td>
                                                <?php
                                                
                                                    $j=0;
                                                    for($i = 1; $i<=$col; $i++){
                                                        if ($i % 2 == 1) {
                                                            $title = $actionTitle[$j];
                                                            $symbol = $symParent[$j];
                                                            $j++;
                                                ?>
                                                <th class="text-center col-xs-<?=$class;?>"><?=$title;?> (<?=$array_symbol[$symbol]?>)</th>
                                                <?php } else { ?>
                                                <th class="text-center col-xs-<?=$class;?> hidden">ĐVT</th>
                                                <?php } ?>
                                                <?php } ?>
                                                <th class="text-center col-xs-<?=$class;?>">Bắt đầu</th>
                                                <th class="text-center col-xs-<?=$class;?>">Kết thúc</th>
                                                <th class="text-center col-xs-<?=$class;?>">Ngân sách</th>
                                            </tr>
                                            <tr>
                                                <td class="hidden"></td>
                                                <td class="text-left">Plan</td>
                                                <?php
                                                    $j=0;
                                                    $_start = ($rowChild->start) ? date('d-M-y',strtotime($rowChild->start)) : date('d-M-y');
                                                    $_end = ($rowChild->end) ? date('d-M-y',strtotime($rowChild->end)) : date('d-M-y');
                                                    for($i = 1; $i<=$col; $i++){
                                                        if ($i % 2 == 1) {
                                                            $value = $valChild[$j];
                                                            $j++;
                                                ?>
                                                <td class="text-center col-xs-<?=$class;?>"><?=number_format($value);?></td>
                                                <?php } else { ?>
                                                <td class="text-center col-xs-<?=$class;?> hidden"></td>
                                                <?php } ?>
                                                <?php } ?>
                                                <td class="text-center col-xs-<?=$class;?>"><?=$_start;?></td>
                                                <td class="text-center col-xs-<?=$class;?>"><?=$_end;?></td>
                                                <td class="text-center col-xs-<?=$class;?>"><?=number_format($rowChild->money);?></td>
                                            </tr>
                                            <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">Actual</td>
                                    <?php
                                        $actionChild = json_decode($rowChild->action);
                                        $j=0;
                                        $action_start = ($rowChild->action_start) ? date('d-M-y',strtotime($rowChild->action_start)) : date('d-M-y');
                                        $action_end = ($rowChild->action_end) ? date('d-M-y',strtotime($rowChild->action_end)) : date('d-M-y');
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $valueAc = ($actionChild) ? $actionChild[$j] : 0;
                                                $symbol = $symParent[$j];
                                                $j++;
                                    ?>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format($valueAc);?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$action_start;?></td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$action_end;?></td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format($rowChild->money_action);?></td>
                                </tr>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">Variance</td>
                                    <?php
                                        $j=0;
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $value = $valChild[$j];
                                                $valueAc = ($actionChild) ? $actionChild[$j] : 0;
                                                $j++;
                                                $variance_i = $value - $valueAc;
                                                $percent_variancce = round($valueAc / ($value / 100),1)."%";
                                    ?>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format($variance_i);?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=number_format((int)$rowChild->money - (int)$rowChild->money_action);?></td>
                                </tr>
                                <tr>
                                    <td class="hidden"></td>
                                    <td class="text-left">% Plan</td>
                                    <?php
                                        $percent_plan = round((int)$rowChild->money_action / ((int)$rowChild->money / 100),1)."%";
                                        $j=0;
                                        for($i = 1; $i<=$col; $i++){
                                            if ($i % 2 == 1) {
                                                $value = $valParent[$j];
                                                $symbol = $symParent[$j];
                                                $j++;
                                    ?>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$percent_variancce;?></td>
                                    <?php } else { ?>
                                    <td class="col-xs-<?=$class;?> hidden"></td>
                                    <?php } ?>
                                    <?php } ?>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>">--</td>
                                    <td class="text-center col-xs-<?=$class;?>"><?=$percent_plan;?></td>
                                </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <?php
                                        echo "<i style='cursor: pointer;' class='fa fa-paperclip attach' rel='".$rowChild->id."'></i>";
                                        ?>
                                    </td>
                                    
                                </tr>
                                <?php $k++;} ?>
                                <tr class="hidden">
                                    <td colspan="5" class="text-right"><strong>Tổng cộng:</strong></td>
                                    <td class="text-right"><?=number_format($sumMoneyChild->totalmoney);?></td>
                                    <td colspan="4" class="text-danger"><?php if ($overNS > 0) { ?>Vượt ngân sách: <?=number_format($overNS);?><?php } ?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
            </div>
            
        </div>
        
            <style>
                .products_containner {padding:5px;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                }
                .content_products {margin-top:0px !important;
                
                }
                .containner .products_containner .content_products .content_tab6 ul li span {
                    color:#000 !important;
                    margin-left:5px;
                }
                .ms-parent {position:absolute; margin-top:10px; width:95%;}
                .ms-drop {margin-top:4px;}
                .open {margin-top:5px;}
                
                #customer-list li:hover {
              background: #eee;
              cursor: pointer;
            }
            .badge{
                font-weight:normal  !important;
            }
            .editPlaner p {line-height:0.4em; position: relative}
            </style>
            
            <div class="row">
            <div class="col-lg-3">
                <table id="table_data" class="timer">
                    <tr>
                        <td class="text-center hidden"><span class="fa fa-calendar"></span></td>
                        <td>Kế hoạch bắt đầu:</td>
                        <td class="span"><?=date('d-M-y',strtotime($startParent));?></td>
                    </tr>
                    <tr>
                        <td class="text-center hidden"><span class="fa fa-calendar"></span></td>
                        <td>Kế hoạch kết thúc:</td>
                        <td class="span"><?=date('d-M-y',strtotime($endParent));?></td>
                    </tr>
                    <tr>
                        <td style="border:0px solid #ccc;">---------------------------</td>
                    </tr>
                    <tr>
                        <td class="text-center hidden"><span class="fa fa-calendar"></span></td>
                        <td>Thực tế bắt đầu:</td>
                        <?php
                            $startAction = ($startAction) ? date('d-M-y',strtotime($startAction)) : date('d-M-y');
                            $endAction = ($endAction) ? date('d-M-y',strtotime($endAction)) : date('d-M-y');
                        ?>
                        <td class="span"><?=$startAction;?></td>
                    </tr>
                    <tr>
                        <td class="text-center hidden"><span class="fa fa-calendar"></span></td>
                        <td>Thực tế kết thúc:</td>
                        <td class="span"><?=$endAction;?></td>
                    </tr>
                </table>
            </div>
            <div class="" style="float:right">
                    <div class="col-lg-4 text-right">
                        <?php 
                            if($market_status_active != 0){
                        ?>
                        <label class="pull-left">Tinh trạng:</label>
                        <select class="form-control" id="market_status" name="market_status" <?=$disabled_status?>>
                        <?php if($market_status_active == 1) { /*Đang triển khai*/ ?>
                        <option value="1" <?=($market_status_active == 1) ? 'selected':''?>>Đang triển khai</option>
                        <option value="2" <?=($market_status_active == 2) ? 'selected':''?>>Hoàn tất</option>
                        <option value="3" <?=($market_status_active == 3) ? 'selected':''?>>Đang chờ người khác</option>
                        <option value="4" <?=($market_status_active == 4) ? 'selected':''?>>Tạm dừng</option>
                        <?php } else if($market_status_active == 2) {?>
                        <option value="2" <?=($market_status_active == 2) ? 'selected':''?>>Hoàn tất</option>
                        <?php } else if($market_status_active == 3) {?>
                        <option value="1" <?=($market_status_active == 1) ? 'selected':''?>>Đang triển khai</option>
                        <option value="2" <?=($market_status_active == 2) ? 'selected':''?>>Hoàn tất</option>
                        <option value="3" <?=($market_status_active == 3) ? 'selected':''?>>Đang chờ người khác</option>
                        <option value="4" <?=($market_status_active == 4) ? 'selected':''?>>Tạm dừng</option>
                        <?php } else if($market_status_active == 4) {?>
                        <option value="1" <?=($market_status_active == 1) ? 'selected':''?>>Đang triển khai</option>
                        <option value="2" <?=($market_status_active == 2) ? 'selected':''?>>Hoàn tất</option>
                        <option value="3" <?=($market_status_active == 3) ? 'selected':''?>>Đang chờ người khác</option>
                        <option value="4" <?=($market_status_active == 4) ? 'selected':''?>>Tạm dừng</option>
                        <?php } else if($market_status_active == 5) {?>
                        <option value="5" <?=($market_status_active == 5) ? 'selected':''?>>Chờ duyệt</option>
                        <?php } else if($market_status_active == 6) {?>
                        <option value="1" <?=($market_status_active == 1) ? 'selected':''?>>Đang triển khai</option>
                        <option value="2" <?=($market_status_active == 2) ? 'selected':''?>>Hoàn tất</option>
                        <option value="3" <?=($market_status_active == 3) ? 'selected':''?>>Đang chờ người khác</option>
                        <option value="4" <?=($market_status_active == 4) ? 'selected':''?>>Tạm dừng</option>
                        <?php } else if($market_status_active == 7) {?>
                        <option value="7" <?=($market_status_active == 7) ? 'selected':''?>>Không được duyệt</option>
                        <?php } ?>
                        </select>
                        <?php } ?>
                        <?php if(($market_status_active == 6 || $market_status_active == 7) && ($market_notes != null)) {?>
                        <div class="showNotes hidden">
                            <label class="pull-left">Ghi chú:</label>
                            <textarea type="text" class="form-control" readonly="" style="resize: vertical; background-color: #000; color: #fff; min-height:80px; font-size:90%;"><?=$market_notes;?></textarea>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4 text-right">
                        <label class="pull-left">Mức độ ưu tiên:</label>
                        <select class="form-control" name="market_priority" <?=$disabled_priority?>>
                        <?php
                        $market_priority = $this->define_model->get_order_status('priority','campaign','position','asc');
                        foreach($market_priority as $key=>$priority){
                        ?>
                        <option value="<?=$priority->code;?>" <?=($market_priority_active == $priority->code) ? "selected":""?>><?=$priority->name; ?></option>
                        <?php } ?>
                        </select>
                    </div>
                    
                    <div class="col-lg-4">
                        <label class="pull-left">% Hoàn thành:</label>
                        <select class="form-control" id="market_success" name="market_success" <?=$disabled_success?>>
                            <?php
                                if($market_status_active == 0 || $market_status_active == 5 || $market_status_active == 7){
                            ?>
                            <option value="0" selected disabled>Chưa bắt đầu</option>
                            <?php
                                } else {
                                for($i=0;$i<=10;$i++){
                                    if($i==0){
                                        $number = $i;
                                    }else{
                                        if($i==10){
                                            $number = str_pad($i,3,"0");
                                        }else{
                                            $number = str_pad($i,2,"0");
                                        }
                                    }
                            ?>
                            <option value="<?=$number;?>" <?=($market_success_active == $number) ? 'selected' : '';?>><?=$number;?> %</option>
                            <?php } } ?>
                        </select>
                    </div>
                
                   
                </div>
        </div>

        <div class="import_select_progress hiddenVIP">
            
        </div>
            <input type='hidden' name="market_id" value="<?=$market_id;?>" />
            <?php 
                if($isAfter == true){
            ?>
            <input type='hidden' name="after" value="true" />
            <?php } ?>
    </form>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
    <div class="import_select_progress">
        <div class="block1">
        </div>
        <div class="block2">
            <div class="btn-group">
                <?php if($this->user->IsAdmin==1){?>
                    <?php if($market_status_active == 5){ ?>
                        <a class="btn btn-info updateStatus" rel="6"><i class="fa fa-check"></i> Duyệt</a>
                        <a class="btn btn-danger updateStatus" rel="7"><i class="fa fa-times-circle"></i> Không Duyệt</a>
                    <?php } ?>
                <?php } else { ?>
                
                <?php if($market_status_active == 7){ ?>
                <a class="btn btn-default resetKPI" rel="<?=$market_id;?>"><i class="fa fa-edit"></i> Làm mới</a>
                <?php } else if($market_status_active == 6){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 5){ ?>
                <?php if($this->user->IsAdmin==1){?>
                <a class="btn btn-info updateStatus" rel="6"><i class="fa fa-check"></i> Duyệt</a>
                <a class="btn btn-danger updateStatus" rel="7"><i class="fa fa-times-circle"></i> Không Duyệt</a>
                <?php } else { ?>
                <a class="btn btn-danger"><i class="fa fa-info"></i> Chờ duyệt</a>
                <?php } ?>
                <!--<a class="btn btn-default resetKPI hidden" rel="<?=$market_id;?>"><i class="fa fa-edit"></i> Sửa chữa</a>-->
                <?php } else if($market_status_active == 4){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 3){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 2){ ?>
                <?php } else if($market_status_active == 1){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Cập nhật trạng thái</a>
                <?php } else if($market_status_active == 0){ ?>
                <a class="btn btn-primary updateCompaign"><i class="fa fa-save"></i> Lưu nháp</a>
                <a class="btn btn-danger updateStatus" rel="5"><i class="fa fa-location-arrow"></i> Chuyển duyệt</a>
                <?php } ?>
                
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="history_status">
	    		<h4>Lịch sử trạng thái:</h4>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_marketing_campaign_history a,ttp_user b where a.UserID=b.ID and a.TranID=$market_id order by a.ID ASC")->result();
	    		if(count($history)>0){
	    			$arr_status = $this->define_model->get_order_status('status','campaign','position','asc');
                                $array_status = array();
                                foreach($arr_status as $key=>$ite){
                                    $code = (int)$ite->code;
                                    $array_status[$code] = $ite->name;
                                }
	    			echo "<table width=100%><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Created))."</td>";
	    				echo isset($row->Notes) ? "<td>".$row->Notes."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
</div>

<style>
    .modal {
        display:    none;
        position:   fixed;
        z-index:    1000;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 ) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat;
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    body.loading {
        overflow: hidden;   
    }

    /* Anytime the body has the loading class, our
       modal element will be visible */
    body.loading .modal {
        display: block;
    }

    .history_status {
        margin-bottom: 20px;
        margin-top: 20px;
        width: 100%;
        background: #FFF;
        padding: 10px;
        border: 1px solid #E1E1E1 !important;
    }
    .history_status td{
        border-top: 1px solid #E1E1E1 !important;
        border-bottom: 1px solid #E1E1E1 !important;
        height:25px;
    }

    .daterangepicker{width: auto;}
    .bgTR {background-color:#D8FFD5}
    .ms-parent {margin-top:-5px;}
    .timer td {height:25px;border:1px solid #ccc; padding:5px;}
    td.span {background-color:#fff; padding-left:5px; padding-right:5px; border:1px solid #ccc; width: 80px; text-align: center;}
</style>

<script type="text/javascript">
var baselink = $("#baselink_report").val();

$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $("#close_overlay").click();
    }
});

$('#market_status').change(function(){
    var cboID = $(this).val();
    if(cboID == 2){ /*Khi hoàn tất*/
        $("#market_success option[value='100']").prop('selected', true);
    }
});

$('#market_success').change(function(){
    var cboID = $(this).val();
    if(cboID == 100){ /*Khi hoàn tất*/
        $("#market_status option[value='2']").prop('selected', true);
    }
});

$("#close_overlay").click(function(){
    $(".over_lay").removeClass('in');
    $(".over_lay").addClass('out');
    setTimeout(function(){
        $(".over_lay").hide();
        disablescrollsetup();
    },200);
});

function enablescrollsetup(){
    $(window).scrollTop(70);
    $("body").css({'height':'100%','overflow-y':'hidden'});
    h = window.innerHeight;
    h = h-100;
    $(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
}

function disablescrollsetup(){
    $("body").css({'height':'auto','overflow-y':'scroll'});
}

$('.attach').click(function(){

    var id = "<?= $market_id ?>";
    var FileID = $(this).attr("rel");
    enablescrollsetup();
    $.ajax({
        url: baselink+"marketing_campaign/marketing_campaign_attach",
        dataType: "html",
        type: "POST",
        data: "market_id="+id+"&FileID="+FileID,
        success: function(result){
            $(".over_lay .box_inner").css({'width':'600px'});
            $(".over_lay .box_inner .block1_inner h1").html("<i class='fa fa-download'></i> Danh sách tài liệu đính kèm");
            $(".over_lay .box_inner .block2_inner").html(result);        	
            $(".over_lay").removeClass('in');
            $(".over_lay").fadeIn('fast');
            $(".over_lay").addClass('in');
        }
    });

});

$('.listDVT').click(function(){
    var baselink = $("#baselink_report").val();
    enablescrollsetup();
    $.ajax({
        url: baselink+"marketing_campaign/listDVT",
        dataType: "html",
        type: "POST",
        data: "",
        success: function(result){
            setTimeout(function(){
                $(".over_lay .box_inner").css({'width':'300px'});
                $(".over_lay .box_inner .block1_inner h1").html("<i class='fa fa-list'></i> Danh sách ĐVT");
                $(".over_lay .box_inner .block2_inner").html(result);        	
                $(".over_lay").removeClass('in');
                $(".over_lay").fadeIn('fast');
                $(".over_lay").addClass('in');
            },301);
        },
        beforeSend: function() { $('.modal').show(); },
        complete: function() {
            setTimeout(function(){
                $('.modal').hide();
            },300);
            
        },
    });
    return false;
});

$('.add').click(function(){
    var baselink = $("#baselink_report").val();
    var id = "<?= $market_id ?>";
    enablescrollsetup();
    $.ajax({
        url: baselink+"marketing_campaign/marketing_campaign_edit_add",
        dataType: "html",
        type: "POST",
        data: "market_id="+id,
        success: function(result){
            $(".over_lay .box_inner").css({'width':'600px','margin-top':'20px'});
            $(".over_lay .box_inner .block1_inner h1").html("<i class='fa fa-plus'></i> Thêm mới KPI");
            $(".over_lay .box_inner .block2_inner").html(result);        	
            $(".over_lay").removeClass('in');
            $(".over_lay").fadeIn('fast');
            $(".over_lay").addClass('in');
        }
    });

});
$('.updateStatus').click(function(){

    var baselink = $("#baselink_report").val();
    var market_id = "<?= $market_id ?>";
    var status = $(this).attr("rel");
    var ghichu = '';
    var getValue = prompt("Điền ghi chú thay đổi (nếu có): ", "");
    if(getValue != null){
        ghichu = getValue;
    }
    $.ajax({
        url: baselink+"marketing_campaign/updateStatus",
        dataType: "html",
        type: "POST",
        data: "notes="+ghichu+"&status="+status+"&market_id="+market_id,
        success: function(result){
            if(result == "OK"){
                window.location = baselink+"marketing_campaign/all";
            }else{
                console.log(result);
            }
        }
    });
});
$('.planer').click(function(){
    var market_id = "<?= $market_id ?>";
    var id = $(this).attr("data-parent");
    var namer = $('.namer_'+id).text();
    var rel = $(this).attr("rel");
    var icon = "";
    if(rel == 0){
        icon = "<i class='fa fa-plus'></i> " + namer;
    }else{
        icon = "<i class='fa fa-list'></i> THÊM HÀNH ĐỘNG CHO " + namer;
    }
    var parent = $(this).attr("data-parent");
    enablescrollsetup();
    $.ajax({
        url: baselink+"marketing_campaign/marketing_campaign_planer",
        dataType: "html",
        type: "POST",
        data: "parent="+parent+"&rel="+rel+"&market_id="+market_id+"&id="+id,
        success: function(result){
            $(".over_lay .box_inner").css({'width':'600px','margin-top':'20px'});
            $(".over_lay .box_inner .block1_inner h1").html(icon);
            $(".over_lay .box_inner .block2_inner").html(result);        	
            $(".over_lay").removeClass('in');
            $(".over_lay").fadeIn('fast');
            $(".over_lay").addClass('in');
        }
    });

});

setTimeout(function () {
    $('#owner_action').multipleSelect({
        width: '92%',
        isOpen: false,
        keepOpen: false,
        placeholder: "Lựa chọn",
        selectAllText: '-- All --',
        allSelected: 'Tất cả',
        maxHeight: '170',
    });
});

$('.resetKPI').click(function(){
    var market_id = $(this).attr("rel");
    $.ajax({
        url: baselink+"marketing_campaign/resetKPI",
        dataType: "html",
        type: "POST",
        data: "market_id="+market_id,
        success: function(result){
            if(result == "OK"){
                location.reload();
            }else{
                alert('Lỗi hệ thống !!!');
            }
        }
    });
});

$('.open').click(function(){
    var id = $(this).attr("rel");
    $('.show_'+id).toggleClass("hidden");
    $(this).find("i").toggleClass("fa-chevron-down").toggleClass("fa-chevron-up");
    $(this).parent("tr").find("td").remove();
    $('.tr_'+id).toggleClass("bgTR");
});

$('.addMore').click(function(){
    var id = $(this).attr("rel");
});

$('.deleteKPI').click(function(){
    if (!confirm('Bạn muốn xóa KPI?\nLưu ý: Toàn bộ kế hoạch thuộc KPI này sẽ bị xóa :(')) {
        return false;
    }else{
        var baselink = $("#baselink_report").val();
        var id = $(this).attr("rel");
        var token = $(this).attr("data-token");
        $.ajax({
            url: baselink+"marketing_campaign/deleteKPI",
            dataType: "html",
            type: "POST",
            data: "id="+id+"&token="+token,
            success: function(result){
                if(result == "OK"){
                    location.reload();
                }else{
                    alert('Lỗi hệ thống !!!');
                }
            }
        });
    }
});

$('.hoverPlaner').hover(function(){
    var id = $(this).children().attr("rel");
    if($('.edit'+id).hasClass("hidden") == true){
        $('.edit'+id).removeClass("hidden");
    }else{
        $('.edit'+id).addClass("hidden");
    }
});

$('.hoverMarket').hover(function(){
    var id = $(this).children().attr("rel");
    if($('.edit'+id).hasClass("hidden") == true){
        $('.edit'+id).removeClass("hidden");
    }else{
        $('.edit'+id).addClass("hidden");
    }
});

$('.editMarket').click(function(){
    var id = $(this).attr("rel");
    var icon = "";
    enablescrollsetup();
    $.ajax({
        url: baselink+"marketing_campaign/editMarket",
        dataType: "html",
        type: "POST",
        data: "id="+id,
        success: function(result){
            icon = "<i class='fa fa-edit'></i> Cập nhật KPI";
            $(".over_lay .box_inner").css({'width':'600px','margin-top':'20px'});
            $(".over_lay .box_inner .block1_inner h1").html(icon);
            $(".over_lay .box_inner .block2_inner").html(result);        	
            $(".over_lay").removeClass('in');
            $(".over_lay").fadeIn('fast');
            $(".over_lay").addClass('in');
            //location.reload(true);
        }
    });
});
$('.editPlaner').click(function(){
    var id = $(this).attr("rel");
    var icon = "";
    enablescrollsetup();
    $.ajax({
        url: baselink+"marketing_campaign/editPlaner",
        dataType: "html",
        type: "POST",
        data: "id="+id,
        success: function(result){
            icon = "<i class='fa fa-edit'></i> Cập nhật kế hoạch";
            $(".over_lay .box_inner").css({'width':'600px','margin-top':'20px'});
            $(".over_lay .box_inner .block1_inner h1").html(icon);
            $(".over_lay .box_inner .block2_inner").html(result);        	
            $(".over_lay").removeClass('in');
            $(".over_lay").fadeIn('fast');
            $(".over_lay").addClass('in');
            //location.reload(true);
        }
    });
});
    
function removePlaner(id,status){
    if (!confirm('Bạn muốn xóa?\nLưu ý: Toàn bộ hành động thuộc kế hoạch này sẽ bị xóa :(')) {
        return false;
    }else{
        $.ajax({
            url: baselink+"marketing_campaign/removePlaner",
            dataType: "html",
            type: "POST",
            data: "id="+id+"&status="+status,
            success: function(result){
                if(result == "OK"){
                    location.reload(true);
                }else{
                    alert('Không xóa được dữ liệu!!!');
                }
            }
        });
    }
}

$('.updateCompaign').click(function(){
    if($('#market_status').val() == 2 || $('#market_success').val() == 100)
    {
        if (!confirm('Bạn muốn hoàn thành chiến dịch?')) {
            return false;
        }
    }
        
    $(this).addClass('saving');
    $.ajax({
        url: baselink+"marketing_campaign/updateCompaign",
        dataType: "html",
        type: "POST",
        data: $('#frmMarket').serialize(),
        success: function(result){
            if(result == "OK"){
                location.reload();
            }else{
                console.log(result);
                alert('Lỗi hệ thống');
            }
        }
    });
    
});
</script>
