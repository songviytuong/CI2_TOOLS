<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
</style>
<div class="table_data">
    <table id="table_data">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center" colspan="2">
                <div class="input-group">
                <input type="text" id="dvtname" name="dvtname" value="" placeholder="Nhập tên ĐVT..." class="form-control"/>
                <span class="input-group-addon btnAction" onclick="addDVT()" style="cursor: pointer;"><i class="fa fa-save"></i> Thêm mới</span>
                </div>
            </th>
        </tr>
        <tbody class="loadDVT"></tbody>
    </table>
</div>
<script type="text/javascript">
    setTimeout(function(){
        loadDVT();
    });
    
    var base_link = "<?=$base_link?>";
    function loadDVT(){
        $.ajax({
            url: base_link + "loadDVT",
            dataType: "html",
            type: "POST",
            data: "",
            success: function (result) {
                $('.loadDVT').html(result);
            }
        });
    }
    
    $('#dvtname').keypress(function(e){
        if(e.which == 13) {
            addDVT();
        }
    });
    
    function addDVT(){
        var name = $('#dvtname').val();
        if(name == ""){
//            alert('Nhập tên ĐVT');
            $('#dvtname').focus();
        }else{
            $.ajax({
                url: base_link + "addDVT",
                dataType: "html",
                type: "POST",
                data: "dvt=" + name,
                success: function (result) {
                    if(result == "OK"){
                        loadDVT();
                        $('#dvtname').val("");
                    }
                },
                beforeSend: function() { $('.modal').show(); },
                complete: function() {
                    setTimeout(function(){
                        $('.modal').hide();
                    },300);

                },
                
            });
        }
    }
    
    function updateDVT(id){
        var name = $('#dvtname').val();
        if(name == ""){
//            alert('Nhập tên ĐVT');
            $('#dvtname').focus();
        }else{
            $.ajax({
                url: base_link + "updateDVT",
                dataType: "html",
                type: "POST",
                data: "id=" + id + "&name=" + name,
                success: function (result) {
                    if(result == "OK"){
                        loadDVT();
                        $('#dvtname').val("");
                        $('.btnAction').removeAttr("onclick");
                        $('.btnAction').attr("onclick","addDVT("+id+")");
                        $('.btnAction').html("<i class='fa fa-save'></i> Thêm mới");
                    }
                }
            });
        }
    }
    
    function editDVT(id){
        var name = $('.th-'+id).text();
        $('#dvtname').val(name);
        $('.btnAction').removeAttr("onclick");
        $('.btnAction').attr("onclick","updateDVT("+id+")");
        $('.btnAction').html("<i class='fa fa-save'></i> Cập nhật");
    }
    function deleteDVT(id){
        $.ajax({
            url: base_link + "deleteDVT",
            dataType: "html",
            type: "POST",
            data: "id=" + id,
            success: function (result) {
                if(result == "OK"){
                    loadDVT();
                }
            }
        });
    }
</script>