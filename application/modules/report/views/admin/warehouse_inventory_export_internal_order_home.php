<?php 

$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$total_tienhang = 0;
$total_chiphi 	= 0;
$total_giatri 	= 0;
$total_huy 		= 0;
$month 			= array();
$result_fillter = array();
$fill_temp_value="";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH SÁCH YÊU CẦU XUẤT KHO CHO / TẶNG / HỦY</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="import_orderlist">
    	<div class="block2">
            <?php 
            if($this->user->UserType!=9){
            ?>
            <div class="block_2_1">
    		      <a class="btn btn-danger" href="<?php echo $base_link.'add' ?>"><i class="fa fa-plus"></i> Yêu cầu</a>
            </div>
            <div class="block_2_2">
                <a class="btn btn-default" href="<?php echo $base_link.'internal_export_excel' ?>" style="margin-right:0px"><i class="fa fa-download"></i> Xuất báo cáo</a>
                <a class="btn btn-primary" href="<?php echo $base_link.'internal_export' ?>" style="margin-right:0px"><i class="fa fa-history"></i> PXK đã xuất</a>
            </div>
            <?php 
            }
            ?>
    	</div>
        <div class="clear"></div>
        
    	<div class="block3 table_data">
    		<?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            ?>
            <table id="table_data">
    			<tr>
    				<th>STT</th>
    				<th style='width: 90px;'>Ngày / Giờ</th>
    				<th>Người nhận</th>
                    <th>Lý do xuất</th>
    				<th>SL hàng xuất</th>
                    <th>Tiền hàng</th>
                    <th>Tổng giá trị</th>
    				<?php 
                    echo $this->user->UserType==9 ? "" : "<th>Tình trạng</th>";
                    ?>
                    <th>Người gửi</th>
    			</tr>
    			<?php 
                $arr_user_list = array();
                $userlist = $this->db->query("select UserName,ID from ttp_user")->result();
                if(count($userlist)>0){
                    foreach($userlist as $row){
                        $arr_user_list[$row->ID] = $row->UserName;
                    }
                }
                $array_status = array(
                    4=>'Yêu cầu bị trả về',
                    3=>'Đã được duyệt',
                    2=>'Yêu cầu chờ duyệt',
                    0=>'Đã xuất kho',
                    1=>'Yêu cầu bị hủy'
                );
    			if(count($data)>0){
    				$i = $start+1;
                    foreach($data as $row){
                        $json = json_decode($row->Note,true);
                        $nguoinhan = isset($json['Nguoinhanhang']) ? $json['Nguoinhanhang'] : '--' ;
                        $phongban = isset($json['Phongban']) ? $json['Phongban'] : '--' ;
                        $lydo = isset($json['Note']) ? $json['Note'] : '--' ;
    					$tienhang = $row->Total-$row->Chietkhau;
    					$status = isset($array_status[$row->Status]) ? $array_status[$row->Status] : "--" ;
    					$total_huy += $row->Status==1 ? $row->Total : 0 ;
    					$total_tienhang += $tienhang;
    					$total_chiphi += $row->Chiphi;
    					$total_giatri += $row->Total;
    					echo "<tr>
			    				<td style='text-align:center;'><a href='{$base_link}edit/$row->ID' style='display:block'>$i</a></td>
			    				<td style='width: 136px;'><a href='{$base_link}edit/$row->ID' style='display:block'>".date('d/m/Y H:i:s',strtotime($row->Ngaydathang))."</a></td>
			    				<td style='width:110px'><a href='{$base_link}edit/$row->ID' style='display:block'>$nguoinhan</a></td>
                                <td><a href='{$base_link}edit/$row->ID' style='display:block'>$lydo</a></td>
                                <td style='text-align:right;width:100px'><a href='{$base_link}edit/$row->ID' style='display:block'>$row->SoluongSP</a></td>
			    				<td style='text-align:right;width:100px'><a href='{$base_link}edit/$row->ID' style='display:block'>".number_format($tienhang)."</a></td>
			    				<td style='text-align:right;width:100px'><a href='{$base_link}edit/$row->ID' style='display:block'>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</a></td>";
                        echo $this->user->UserType==9 ? "" : "<td style='text-align:center;width:150px'><a href='{$base_link}edit/$row->ID' style='display:block'>$status</a></td>";
                        echo isset($arr_user_list[$row->UserID]) ? "<td><a href='{$base_link}edit/$row->ID' style='display:block'>".$arr_user_list[$row->UserID]."</a></td>" : "" ;
			    		echo "</tr>";
			    		$temp1 = date('mY',strtotime($row->Ngaydathang));
                        $i++;
    				}
                    $total_tienhang = $total-$chietkhau;
                    $total_giatri = $total + $chiphi-$chietkhau;
    				echo "<tr>
    						<td colspan='4' style='text-align:center;'><p><span style='font-size:17px;font-weight:bold;'>Tổng cộng</span> <br>Tìm thấy <b>".number_format($find)."</b> đơn hàng theo yêu cầu .</p></td>
    						<td style='text-align:right'>".number_format($SoluongSP,2)."</td>
                            <td style='text-align:right'>".number_format($total_tienhang)."</td>
    						<td style='text-align:right'>".number_format($total_giatri)."</td>
    						<td colspan='2'></td>
    					</tr>";
    			}else{
    				echo $this->user->UserType==9 ? "<tr><td colspan='8'>Không tìm thấy yêu cầu .</td></tr>" : "<tr><td colspan='9'>Không tìm thấy yêu cầu .</td></tr>";
    			}
    			?>

    		</table>
            <?php 
                echo $nav;
            ?>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });

    var baselink = $("#baselink_report").val();

	$("#show_thaotac").click(function(event){
        event.stopPropagation();
		$(this).parent('li').find('ul').toggle();
	});

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });

    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });

    var baselink = $("#baselink_report").val();
    var getnewsorder = function(){
        var data = $("#newsdescription").html();
        $.ajax({
            url: baselink+"import/get_news_order",
            dataType: "html",
            type: "POST",
            data: "",
            success: function(result){
                if(data!=result){
                    $("#newsdescription").html(result);
                    $("#newsdescription").slideDown();
                }
            }
        });

    }

    setInterval(getnewsorder,10000);
	
    

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("p.second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"import_order/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="vung" || fieldname=="tinhthanh" || fieldname=="quanhuyen" || fieldname=='loaikhachhang' || fieldname=='trangthaidonhang' || fieldname=='doitacvanchuyen' || fieldname=='user'){
            return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="tenkhachhang" || fieldname=="sodienthoai"){
                return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    function export_data(ob){
        $(".export_tools_kt").toggle();
    }

    $("#falseclass").submit(function(){
        $(this).find('button').removeClass("saving");
    });

</script>
