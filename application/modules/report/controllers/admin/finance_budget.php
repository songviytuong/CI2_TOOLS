<?php
class Finance_budget extends Admin_Controller {
    public $user;
    public $classname="finance_budget";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/finance_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function cash(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Finance Budget Tools');
        $id = isset($_GET['ID']) ? (int)$_GET['ID'] : 1 ;
        $cash = $this->db->query("select * from ttp_report_finance_cash where ID=$id")->row();
        $cashtitle = $cash ? $cash->Title." (".number_format($cash->Price)."đ)" : '' ;
        $cashid = $cash ? $cash->ID : 0 ;
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $export = $this->db->query("select * from ttp_report_finance_export where Dayfinance>='$startday' and Dayfinance<='$stopday' and CashPayment=$cashid")->result();
        $import = $this->db->query("select * from ttp_report_finance_import where Dayfinance>='$startday' and Dayfinance<='$stopday' and CashPayment=$cashid")->result();
        $data = array(
            'cashid'      => $cashid,
            'cashtitle'   => $cashtitle,
            'export'      => $export,
            'import'      => $import,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'base_link' =>  base_url().ADMINPATH.'/report/finance_export/'
        );
        $view = 'admin/finance_budget_cash';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function bank(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Finance Budget Tools');
        $id = isset($_GET['ID']) ? (int)$_GET['ID'] : 1 ;
        $cash = $this->db->query("select * from ttp_report_finance_bankaccount where ID=$id")->row();
        $cashtitle = $cash ? $cash->Title." (".number_format($cash->Price)."đ)" : '' ;
        $cashid = $cash ? $cash->ID : 0 ;
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $export = $this->db->query("select * from ttp_report_finance_export where Dayfinance>='$startday' and Dayfinance<='$stopday' and AccountPayment=$cashid")->result();
        $import = $this->db->query("select * from ttp_report_finance_import where Dayfinance>='$startday' and Dayfinance<='$stopday' and AccountPayment=$cashid")->result();
        $data = array(
            'cashid'      => $cashid,
            'cashtitle'   => $cashtitle,
            'export'      => $export,
            'import'      => $import,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'base_link' =>  base_url().ADMINPATH.'/report/finance_export/'
        );
        $view = 'admin/finance_budget_bank';
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }
}
