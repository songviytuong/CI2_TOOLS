<?php 
class Warehouse_trademark_faq extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_trademark_faq";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from web_trademark_faq")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select faq.*, t.ID as t_ID, t.Title as t_Title
            from web_trademark_faq faq, ttp_report_trademark t
            where faq.TrademarkID = t.ID 
            order by ID DESC $limit_str")->result();          
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_trademark_faq/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_trademark_faq/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Trademark FAQ | Manager Report Tools');
		$this->template->write_view('content','admin/warehouse_trademark_faq_home',$data);
		$this->template->render();
	}

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Trademark FAQ add | Manager report Tools');
        // brand
        $list_brand = $this->db->query("select ID, Title from ttp_report_trademark order by Title ASC")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_trademark_faq/',
            'list_brand' => $list_brand
        );
        $this->template->write_view('content','admin/warehouse_trademark_faq_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $TrademarkID = isset($_POST['TrademarkID']) ? $_POST['TrademarkID'] : '' ;        
        $Status = isset($_POST['Published']) ? mysql_real_escape_string($_POST['Published']) : '' ;
        $Question = isset($_POST['Question']) ? mysql_real_escape_string($_POST['Question']) : '' ;
        $Answer = isset($_POST['Answer']) ? mysql_real_escape_string($_POST['Answer']) : '' ; 

        if($Question!=''){
            
            $data = array(
                'TrademarkID'   => $TrademarkID,
                'Status'        => $Status,
                'Question'      => $Question,
                'Answer'        => $Answer,
                'Created'       => date('Y-m-d H:i:s')
            );           
            $this->db->insert("web_trademark_faq",$data);            
        }
        redirect(ADMINPATH.'/report/warehouse_trademark_faq/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from web_trademark_faq where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select faq.*, t.ID as t_ID, t.Title as t_Title
            from web_trademark_faq faq, ttp_report_trademark t
            where faq.TrademarkID = t.ID and faq.ID=$id")->row();             
            if(!$result) return;

            // brand
            $list_brand = $this->db->query("select ID, Title from ttp_report_trademark order by Title ASC")->result();

            $this->template->add_title('Edit Trademark FAQ | Manager report Tools');
            $data = array(
                'base_link' => base_url().ADMINPATH.'/report/warehouse_trademark_faq/',
                'data'      => $result,
                'list_brand'=> $list_brand
            );
            $this->template->write_view('content','admin/warehouse_trademark_faq_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $TrademarkID = isset($_POST['TrademarkID']) ? $_POST['TrademarkID'] : '' ;        
        $Status = isset($_POST['Published']) ? $_POST['Published'] : '' ;
        $Question = isset($_POST['Question']) ? $_POST['Question'] : '' ;
        $Answer = isset($_POST['Answer']) ? $_POST['Answer'] : '' ;        
        if($Question!=''){
            $data = array(
                'TrademarkID'   => $TrademarkID,
                'Status'        => $Status,
                'Question'      => $Question,
                'Answer'        => $Answer
            );
            $this->db->where("ID",$ID);
            $this->db->update("web_trademark_faq", $data);        
        }
        redirect(ADMINPATH.'/report/warehouse_trademark_faq/');
    }
}
?>
