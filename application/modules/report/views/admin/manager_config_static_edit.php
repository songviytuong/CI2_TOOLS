<?php 
	$base_link = base_url().ADMINPATH.'/report/manager_config/';
?>
<div class="containner">
	<form action="<?php echo $base_link.'update' ?>" method="post">
	<input type="hidden" name="ID" value="<?php echo $data->id ?>" />
	<div class="row" style="margin:0px;">
		<div class="form-group">
			<div class="col-xs-6">
				<h3 style="margin:0px">Cập nhật dữ liệu</h3>
			</div>
			<div class="col-xs-6 text-right">
				<button type="submit" class="btn btn-success"><i class="fa fa-check-square"></i> Lưu thông tin</button>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<label class="col-xs-3 control-label">Nhóm dữ liệu (group) </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Loại dữ liệu (type) </label>
			<label class="col-xs-1 control-label"></label>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-3">
				<input type="text" name="group" class="form-control" value="<?php echo $data->group ?>" placeholder="Nhập nhóm dữ liệu ..." />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="text" name="type" class="form-control" value="<?php echo $data->type ?>" placeholder="Nhập loại dữ liệu ..." />
			</div>
			<div class="col-xs-1"></div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<label class="col-xs-3 control-label">Mã dữ liệu (code) </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Tên mô tả dữ liệu (name) </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Ngôn ngữ dữ liệu (lang)</label>
			<label class="col-xs-1 control-label"></label>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-3">
				<input type="text" class="form-control" name="code" value="<?php echo $data->code ?>" placeholder="Nhập mã dữ liệu cần khởi tạo ..." />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="text" class="form-control" name="name" value="<?php echo $data->name ?>" placeholder="Nhập tên dữ liệu cần khởi tạo ..." />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="text" class="form-control" name="lang" value="<?php echo $data->lang ?>" placeholder="Nhập ngôn ngữ dữ liệu cần khởi tạo ..." />
			</div>
			<div class="col-xs-1"></div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<label class="col-xs-3 control-label">Màu sắc (nếu có) (color)</label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Vị trí (position) </label>
			<label class="col-xs-1 control-label"></label>
			<label class="col-xs-3 control-label">Trạng thái khuyến mãi (promotion_active) </label>
			<label class="col-xs-1 control-label"></label>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-3">
				<input type="text" class="form-control" name="color" value="<?php echo $data->color ?>" placeholder="Nhập mã màu dữ liệu cần khởi tạo ..." />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="text" class="form-control" name="position" value="<?php echo $data->position ?>" placeholder="Nhập mã vị trí dữ liệu cần khởi tạo ..." />
			</div>
			<div class="col-xs-1"></div>
			<div class="col-xs-3">
				<input type="text" class="form-control" name="promotion_active" value="<?php echo $data->promotion_active ?>" placeholder="Nhập mã dữ liệu cần khởi tạo ..." />
			</div>
			<div class="col-xs-1"></div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<label class="col-xs-3 control-label">Mô tả ngắn cho loại dữ liệu này (description)</label>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<textarea name="description" class="form-control" cols="30" rows="8"><?php echo $data->description ?></textarea>
		</div>
	</div>
	</form>
</div>
