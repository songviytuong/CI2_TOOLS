<?php 
class Report_sales extends Admin_Controller { 
 
 	public $user;
 	public $classname="report_sales";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$this->template->add_title('Sales | Report Tools');
		$this->template->write_view('content','admin/sales_overview');
		$this->template->render();
	}

    public function compare_targets_doanhso(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets');
        $this->template->render();
    }

    public function analytics_funnels(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Funnels | Report Tools');
        $this->template->write_view('content','admin/sales_funnels');
        $this->template->render();
    }

    public function report_transport(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $express = isset($_GET['express']) ? (int)$_GET['express'] : 1 ;
        $shipper = isset($_GET['shipper']) ? (int)$_GET['shipper'] : 0 ;
        $bonus = ' and f.ID='.$express;
        $bonus = $shipper>0 ? $bonus.' and c.ID='.$shipper : $bonus ;

        $result = $this->db->query("select a.MaDH,a.Status as OrderStatus,a.Total,a.Chiphi,a.Chietkhau,b.*,c.UserName,d.MaSP,f.Title from ttp_report_order a,ttp_report_order_send_supplier b,ttp_user c,ttp_report_products d,ttp_user_transport e,ttp_report_transport f where c.ID=e.UserID and e.TransportID=f.ID and b.ProductsID=d.ID and a.ID=b.OrderID and b.UserReciver=c.ID and date(b.TimeReciver)>='$startday' and date(b.TimeReciver)<='$stopday'".$bonus)->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'express'   => $express,
            'shipper'   => $shipper,
            'base_link' => base_url().ADMINPATH.'/report/'
        );
        $this->template->add_title('Transport | Report Tools');
        $this->template->write_view('content','admin/sales_transport',$data);
        $this->template->render();
    }

    public function report_sanphamban(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_sanphamban');
        $this->template->render();
    }

    public function compare_targets_sanphamban(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets_sanpham');
        $this->template->render();
    }

    public function analytics_event(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Analytics event | Report Tools');
        $this->template->write_view('content','admin/sales_analytics_event');
        $this->template->render();
    }

    public function report_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Order report | Report Tools');
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
        $ordertype = isset($_GET['order_type']) ? $_GET['order_type'] : 0 ;
        switch ($ordertype) {
            case 0:
                $ordertype = " and a.OrderType=0";
                break;
            case 1:
                $ordertype = " and a.OrderType in(1,2,4,5)";
                break;
            case 2:
                $ordertype = " and a.OrderType=3";
                break;
            default:
                $ordertype = "";
                break;
        }
        $view_status = $current_view==0 ? " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'" : " and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday'" ;
        if($current_view==0){
            $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype $fillter group by a.ID order by a.Status")->result();
            $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype and a.Status=1 and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' $fillter group by g.SaleReasonID")->result();
        }else{
            $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype $fillter group by a.ID order by a.Status")->result();
            $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype and a.Status=1 $fillter group by g.SaleReasonID")->result();
        }
        
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'reason'    => $reason,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'base_link' => base_url().ADMINPATH.'/report/import_order/'
        );
        $this->template->write_view('content',"admin/sales_order",$data);
        $this->template->render();
    }

    public function export_report_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Order report | Report Tools');
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
        $ordertype = isset($_GET['order_type']) ? $_GET['order_type'] : 0 ;
        switch ($ordertype) {
            case 0:
                $ordertype = " and a.OrderType=0";
                break;
            case 1:
                $ordertype = " and a.OrderType in(1,2,4,5)";
                break;
            case 2:
                $ordertype = " and a.OrderType=3";
                break;
            default:
                $ordertype = "";
                break;
        }
        $view_status = $current_view==0 ? " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'" : " and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday'" ;
        if($current_view==0){
            $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ngaydathang,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,d.Title as Thanhpho,c.Company,e.Title as Area from ttp_report_order a,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype $fillter group by a.ID order by a.Status")->result();
            $reason = $this->db->query("select g.PLReason,g.OrderID,g.Another3PL from ttp_report_order a,ttp_report_order_reason_details g where g.OrderID=a.ID $view_status and a.CustomerID!=9996 $ordertype and a.Status=1 and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'")->result();
        }else{
            $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,d.Title as Thanhpho,c.Company,e.Title as Area from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype $fillter group by a.ID order by a.Status")->result();
            $reason = $this->db->query("select g.PLReason,g.OrderID,g.Another3PL from ttp_report_order a,ttp_report_order_reason_details g where g.OrderID=a.ID $view_status and a.CustomerID!=9996 $ordertype and a.Status=1")->result();
        }
        
        if(count($result)>0){
            ini_set('memory_limit', '3500M');
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $arr = array();
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày lên đơn hàng')
                        ->setCellValue('B1', 'Mã đơn hàng')
                        ->setCellValue('C1', 'Đối tác /  khách hàng')
                        ->setCellValue('D1', 'Khu vực')
                        ->setCellValue('E1', 'Tỉnh thành')
                        ->setCellValue('F1', 'Số lượng sản phẩm')
                        ->setCellValue('G1', 'Giá trị đơn hàng')
                        ->setCellValue('H1', 'Chiết khấu trên đơn hàng')
                        ->setCellValue('I1', 'Giảm trừ các khoản')
                        ->setCellValue('J1', 'Chi phí vận chuyển')
                        ->setCellValue('K1', 'Tổng tiền phải trả')
                        ->setCellValue('L1', 'Trạng thái đơn hàng')
                        ->setCellValue('M1', 'Lý do cho trạng thái hủy');

            $arr_status = $this->define_model->get_order_status('status','order');
            $array_status = array();
            foreach($arr_status as $key=>$ite){
                $code = (int)$ite->code;
                $array_status[$code] = $ite->name;
            }

            $arr_reasonlist = array();
            $resonlist = $this->db->query("select * from ttp_report_order_reason")->result();
            if(count($resonlist)>0){
                foreach($resonlist as $row){
                    $arr_reasonlist[$row->ID] = $row->Title;
                }
            }

            $arr_reason = array();
            if(count($reason)>0){
                foreach($reason as $row){
                    $arr_reason[$row->OrderID] = array(
                        'Title'=>isset($arr_reasonlist[$row->PLReason]) ? $arr_reasonlist[$row->PLReason] : '',
                        'Another'=>$row->Another3PL
                    );
                }
            }
            
            $i=2;
            foreach($result as $row){
                if(isset($arr_reason[$row->ID]['Title']) && isset($arr_reason[$row->ID]['Another'])){
                    if($arr_reason[$row->ID]['Another']!=''){
                        $reason_row = $arr_reason[$row->ID]['Another'];
                    }else{
                        $reason_row = $arr_reason[$row->ID]['Title'];
                    }
                }else{
                    $reason_row = '';
                }
                
                $partner = $row->Company=='' ? $row->Name : $row->Name.' - '.$row->Company ;
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('B'.$i, $row->MaDH)
                            ->setCellValue('C'.$i, $partner)
                            ->setCellValue('D'.$i, $row->Area)
                            ->setCellValue('E'.$i, $row->Thanhpho)
                            ->setCellValue('F'.$i, $row->SoluongSP)
                            ->setCellValue('G'.$i, $row->Total)
                            ->setCellValue('H'.$i, $row->Chietkhau)
                            ->setCellValue('I'.$i, $row->Reduce)
                            ->setCellValue('J'.$i, $row->Chiphi)
                            ->setCellValue('K'.$i, $row->Total - $row->Chietkhau - $row->Reduce + $row->Chiphi)
                            ->setCellValue('L'.$i, $array_status[$row->Status])
                            ->setCellValue('M'.$i, $reason_row);
                $i++;
            }
            $objPHPExcel->getActiveSheet()->setTitle('BC_TRANGTHAIDONHANG');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BC_TRANGTHAIDONHANG.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !";
        }
    }

    public function compare_targets_order(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets_order');
        $this->template->render();
    }

    public function report_customer(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Order report | Report Tools');
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
        $ordertype = isset($_GET['order_type']) ? $_GET['order_type'] : 0 ;
        $customertype = isset($_GET['cus_type']) ? $_GET['cus_type'] : "" ;
        switch ($ordertype) {
            case 0:
                $ordertype = " and a.OrderType=0";
                break;
            case 1:
                $ordertype = " and a.OrderType in(1,2,4,5)";
                break;
            case 2:
                $ordertype = " and a.OrderType=3";
                break;
            default:
                $ordertype = "";
                break;
        }
        $bonus = !isset($_GET['cus_type']) ? "" : " and a.CustomerType=$customertype" ;
        $view_status = $current_view==0 ? " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'" : " and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday'" ;
        $result = $this->db->query("select a.ID,a.MaDH,a.Status,a.CustomerID,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.CustomerType,a.SourceID,d.AreaID,a.KenhbanhangID,c.Name from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e where a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and a.Status!=1 $view_status and a.CustomerID!=9996 $ordertype $fillter $bonus")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'base_link' => base_url().ADMINPATH.'/report/report_sales/'
        );
        $this->template->write_view('content',"admin/sales_customer",$data);
        $this->template->render();
    }

    public function load_data_customer_repeat($num=0,$page=1){
        if($num==0) exit();
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
        $ordertype = isset($_GET['order_type']) ? $_GET['order_type'] : 0 ;
        $customertype = isset($_GET['cus_type']) ? $_GET['cus_type'] : "" ;
        switch ($ordertype) {
            case 0:
                $ordertype = " and a.OrderType=0";
                break;
            case 1:
                $ordertype = " and a.OrderType in(1,2,4,5)";
                break;
            case 2:
                $ordertype = " and a.OrderType=3";
                break;
            default:
                $ordertype = "";
                break;
        }
        $bonus = !isset($_GET['cus_type']) ? "" : " and a.CustomerType=$customertype" ;
        $view_status = $current_view==0 ? " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'" : " and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday'" ;
        $result = $this->db->query("select a.ID,a.CustomerID,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.SourceID,a.KenhbanhangID,d.AreaID,c.Name,a.CustomerType from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e where a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and a.Status!=2 $view_status and a.CustomerID!=9996 $ordertype $fillter $bonus")->result();
        $data = array(
            'num'       => $num,
            'page'      => $page,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'base_link' => base_url().ADMINPATH.'/report/report_sales/'
        );
        $this->load->view("admin/sales_customer_repeat",$data);
    }

    public function load_order_by_customer_repeat($id=0){
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $id = (int)$id;
        $result = $this->db->query("select a.ID,a.Ngaydathang,a.MaDH,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.AddressOrder,b.UserName from ttp_report_order a,ttp_user b where a.CustomerID=$id and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.UserID=b.ID and a.Status!=2")->result();
        if(count($result)>0){
            $url = base_url().ADMINPATH.'/report/import_order/edit/';
            echo "<div class='col-xs-2'><b>Ngày đặt hàng</b></div>";
            echo "<div class='col-xs-2'><b>Mã đơn hàng</b></div>";
            echo "<div class='col-xs-2'><b>Giá trị đơn hàng</b></div>";
            echo "<div class='col-xs-3'><b>Địa chỉ giao hàng</b></div>";
            echo "<div class='col-xs-2'><b>Tài khoản</b></div>";
            echo "<div class='col-xs-1'><b>Thao tác</b></div>";
            foreach($result as $row){
                $total = $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                echo "<div class='col-xs-2' style='clear:both'>".date('d/m/Y',strtotime($row->Ngaydathang))."</div>";
                echo "<div class='col-xs-2'>$row->MaDH</div>";
                echo "<div class='col-xs-2'>".number_format($total)."</div>";
                echo "<div class='col-xs-3'><a title='$row->AddressOrder'>$row->AddressOrder</a></div>";
                echo "<div class='col-xs-2'>$row->UserName</div>";
                echo "<div class='col-xs-1'><a target='_blank' href='$url".$row->ID."'><i class='fa fa-search' aria-hidden='true'></i> Preview</a></div>";
            }
        }
    }

    public function report_search(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_define where `group`='sales' and type='special_search'")->result();
        $data = array(
            'data'=>$result
        );
        $this->template->add_title('Search data | Report Tools');
        $this->template->write_view('content','admin/sales_search',$data);
        $this->template->render();
    }

    public function report_search_case_1($page=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_define where `group`='sales' and type='special_search' and code=1")->row();
        $result_1 = $this->db->query("select DISTINCT b.ID from ttp_report_order a,ttp_report_customer b where a.CustomerID=b.ID and date(a.Ngaydathang)>='2016-04-01'")->result();
        $result_2 = $this->db->query("select b.ID,b.Name,b.Phone1,b.Address,a.Total,count(a.ID) as SLDH from ttp_report_order a,ttp_report_customer b where a.CustomerID=b.ID and date(a.Ngaydathang)<'2016-04-01' and a.Status!=2 and a.Status!=1 group by b.ID having SLDH=1")->result();
        if($result){
            $data = array(
                'data'=>$result,
                'result1'=>$result_1,
                'result2'=>$result_2,
                'page'  =>$page,
                'base_link'=>base_url().ADMINPATH.'/report/report_sales/report_search_case_1'
            );
            $this->template->add_title('Search data | Report Tools');
            $this->template->write_view('content','admin/sales_search_case_1',$data);
            $this->template->render();
        }
    }

    public function export_search_case_1(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $result_1 = $this->db->query("select DISTINCT b.ID from ttp_report_order a,ttp_report_customer b where a.CustomerID=b.ID and date(a.Ngaydathang)>='2016-04-01'")->result();
        $result_2 = $this->db->query("select b.ID,b.Name,b.Phone1,b.Address,a.Total,b.Phone2,a.Ngaydathang,c.UserName,count(a.ID) as SLDH from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and date(a.Ngaydathang)<'2016-04-01' and a.Status!=2 and a.Status!=1 group by b.Phone1 having SLDH=1")->result();
        $arr_new = array();
        
        if(count($result_1)>0){
            foreach($result_1 as $row){
                $arr_new[$row->ID] = 1;
            }
        }
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');
        require_once 'public/plugin/PHPExcel.php';
        $arr = array();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Tên khách hàng')
                    ->setCellValue('B1', 'Số điện thoại 1')
                    ->setCellValue('C1', 'Số điện thoại 2')
                    ->setCellValue('D1', 'Địa chỉ')
                    ->setCellValue('E1', 'Giá trị đơn hàng')
                    ->setCellValue('F1', 'Ngày đặt hàng')
                    ->setCellValue('G1', 'Nhân viên phụ trách');
        if(count($result_2)>0){
            $i=2;
            foreach($result_2 as $item){
                if(!isset($arr_new[$item->ID]) && $item->Total<=5000000 && $item->SLDH==1){
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $item->Name)
                        ->setCellValue('B'.$i, $item->Phone1)
                        ->setCellValue('C'.$i, $item->Phone2)
                        ->setCellValue('D'.$i, $item->Address)
                        ->setCellValue('E'.$i, $item->Total)
                        ->setCellValue('F'.$i, date('d/m/Y',strtotime($item->Ngaydathang)))
                        ->setCellValue('G'.$i, $item->UserName);
                    $i++;
                }
            }
        }
        $objPHPExcel->getActiveSheet()->setTitle('KHACHHANGMUA1LAN');
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="KHACHHANGMUA1LAN.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function report_search_case_2($page=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $numday = $this->lib->get_nume_day($startday,$stopday)+1;

        $this->session->set_userdata("startday_vs");
        $this->session->set_userdata("stopday_vs");

        $startday_vs = $this->session->userdata("startday_vs");
        $startday_vs = $startday_vs!='' ? $startday_vs : date('Y-m-d',strtotime($startday)-$numday*24*60*60);
        $stopday_vs = $this->session->userdata("stopday_vs");
        $stopday_vs = $stopday_vs!='' ? $stopday_vs : date('Y-m-d',strtotime($startday)-$numday*1*60*60);

        $type=isset($_GET['type']) ? (int)$_GET['type'] : 67 ;

        $result = $this->db->query("select * from ttp_define where `group`='sales' and type='special_search' and code=2")->row();
        $result1 = $this->db->query("select a.ID,a.Title,a.VariantType,a.Chimmoi,a.Created,a.LastEdited,b.CategoriesID from ttp_report_products a,ttp_report_products_categories b where a.ID=b.ProductsID and date(a.Created)>='$startday' and date(a.Created)<='$stopday'")->result();
        $result2 = $this->db->query("select a.ID,a.Title,a.VariantType,a.Chimmoi,a.Created,a.LastEdited,b.CategoriesID from ttp_report_products a,ttp_report_products_categories b where a.ID=b.ProductsID and date(a.LastEdited)>='$startday' and date(a.LastEdited)<='$stopday'")->result();
        $result3 = $this->db->query("select a.ID,a.Title,a.VariantType,a.Chimmoi,a.Created,a.LastEdited,b.CategoriesID from ttp_report_products a,ttp_report_products_categories b where a.ID=b.ProductsID and date(a.Created)>='$startday_vs' and date(a.Created)<='$stopday_vs'")->result();
        $result4 = $this->db->query("select a.ID,a.Title,a.VariantType,a.Chimmoi,a.Created,a.LastEdited,b.CategoriesID from ttp_report_products a,ttp_report_products_categories b where a.ID=b.ProductsID and date(a.LastEdited)>='$startday_vs' and date(a.LastEdited)<='$stopday_vs'")->result();
        $categories = $this->db->query("select a.ID,a.Title,count(b.ProductsID) as Total from ttp_report_categories a,ttp_report_products_categories b where a.ID=b.CategoriesID and a.ParentID=$type group by a.ID")->result();
        $categories_all = $this->db->query("select ID,Title,ParentID,Path from ttp_report_categories where ParentID=1 order by Path ASC")->result();
        if($result){
            $data = array(
                'data'=>$result,
                'result_created'=>$result1,
                'result_lastedited'=>$result2,
                'result_created_vs'=>$result3,
                'result_lastedited_vs'=>$result4,
                'categories'=>$categories,
                'categories_all'=>$categories_all,
                'page'  =>$page,
                'startday'  => $startday,
                'stopday'   => $stopday,
                'startday_vs' => $startday_vs,
                'stopday_vs' => $stopday_vs,
                'type' => $type,
                'base_link'=>base_url().ADMINPATH.'/report/report_sales/report_search_case_2'
            );
            $this->template->add_title('Search data | Report Tools');
            $this->template->write_view('content','admin/sales_search_case_2',$data);
            $this->template->render();
        }
    }

    public function report_search_case_3($page=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        $ShipmentID = isset($_GET['ShipmentID']) ? (int)$_GET['ShipmentID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? (int)$_GET['TrademarkID'] : 0 ;
        $ProductsID = isset($_GET['ProductsID']) ? (int)$_GET['ProductsID'] : 0 ;
        $OrderType = isset($_GET['OrderType']) ? $_GET['OrderType'] : -1 ;

        $str = $TrademarkID!=0 ? " and c.TrademarkID=$TrademarkID" : "" ;
        $str = $ProductsID!=0 ? $str." and c.ID=$ProductsID" : $str ;
        $str = $OrderType >=0 ? $str." and a.Status=$OrderType" : $str ;
        $str = $ShipmentID !=0 ? $str." and b.ShipmentID=$ShipmentID" : $str ;

        $str_combo = $ProductsID!=0 ? $str." and c.ID=$ProductsID" : "" ;
        $str_combo = $OrderType >=0 ? $str." and a.Status=$OrderType" : $str ;
        $str_combo = $ShipmentID !=0 ? $str." and b.ShipmentID=$ShipmentID" : $str ;

        $result = $this->db->query("select a.MaDH,a.Status,a.Ngaydathang,b.Amount,c.Title,c.Donvi,c.MaSP,d.ShipmentCode from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c,ttp_report_shipment d where a.ID=b.OrderID and b.ProductsID=c.ID and b.ShipmentID=d.ID and date(a.Ngaydathang)>='".date('Y-m-d',strtotime($startday))."' and date(a.Ngaydathang)<='".date('Y-m-d',strtotime($stopday))."' and c.VariantType in(0,1) and a.CustomerID!=9996 and a.KhoID=$WarehouseID $str")->result();
        $result1 = $this->db->query("select a.MaDH,a.Status,a.Ngaydathang,b.Amount,e.ProductsID,c.Title,c.Donvi,c.MaSP,d.ShipmentCode from ttp_report_order a,ttp_report_orderdetails_bundle b,ttp_report_products c,ttp_report_shipment d,ttp_report_orderdetails e where a.ID=e.OrderID and e.ID=b.DetailsID and b.ProductsID=c.ID and b.ShipmentID=d.ID and date(a.Ngaydathang)>='".date('Y-m-d',strtotime($startday))."' and date(a.Ngaydathang)<='".date('Y-m-d',strtotime($stopday))."' and c.VariantType in(0,1) and a.CustomerID!=9996 and a.KhoID=$WarehouseID $str_combo")->result();
        $data = array(
            'data'=>$result,
            'data1'=>$result1,
            'startday'      => $startday,
            'stopday'       => $stopday,
            'WarehouseID'   => $WarehouseID,
            'ShipmentID'    => $ShipmentID,
            'TrademarkID'   => $TrademarkID,
            'ProductsID'    => $ProductsID,
            'OrderType'     => $OrderType,
            'base_link'=>base_url().ADMINPATH.'/report/report_sales/report_search_case_3'
        );
        $this->template->add_title('Search data | Report Tools');
        $this->template->write_view('content','admin/sales_search_case_3',$data);
        $this->template->render();
    }

    public function export_report_search_3(){
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());

        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        $ShipmentID = isset($_GET['ShipmentID']) ? (int)$_GET['ShipmentID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? (int)$_GET['TrademarkID'] : 0 ;
        $ProductsID = isset($_GET['ProductsID']) ? (int)$_GET['ProductsID'] : 0 ;
        $OrderType = isset($_GET['OrderType']) ? $_GET['OrderType'] : -1 ;

        $str = $TrademarkID!=0 ? " and c.TrademarkID=$TrademarkID" : "" ;
        $str = $ProductsID!=0 ? $str." and c.ID=$ProductsID" : $str ;
        $str = $OrderType >=0 ? $str." and a.Status=$OrderType" : $str ;
        $str = $ShipmentID !=0 ? $str." and b.ShipmentID=$ShipmentID" : $str ;

        $result = $this->db->query("select a.MaDH,a.Status,a.Ngaydathang,b.Amount,c.Title,c.Donvi,c.MaSP,d.ShipmentCode from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c,ttp_report_shipment d where a.ID=b.OrderID and b.ProductsID=c.ID and b.ShipmentID=d.ID and date(a.Ngaydathang)>='".date('Y-m-d',strtotime($startday))."' and date(a.Ngaydathang)<='".date('Y-m-d',strtotime($stopday))."' and c.VariantType in(0,1) and a.CustomerID!=9996 and a.KhoID=$WarehouseID $str")->result();
        $result1 = $this->db->query("select a.MaDH,a.Status,a.Ngaydathang,b.Amount,e.ProductsID,c.Title,c.Donvi,c.MaSP,d.ShipmentCode from ttp_report_order a,ttp_report_orderdetails_bundle b,ttp_report_products c,ttp_report_shipment d,ttp_report_orderdetails e where a.ID=e.OrderID and e.ID=b.DetailsID and b.ProductsID=c.ID and b.ShipmentID=d.ID and date(a.Ngaydathang)>='".date('Y-m-d',strtotime($startday))."' and date(a.Ngaydathang)<='".date('Y-m-d',strtotime($stopday))."' and c.VariantType in(0,1) and a.CustomerID!=9996 and a.KhoID=$WarehouseID $str")->result();

        $products_bundle = $this->db->query("select Title,ID from ttp_report_products where VariantType=2")->result();
        $arr_bundle = array();
        if(count($products_bundle) >0){
            foreach($products_bundle as $row){
                $arr_bundle[$row->ID] = $row->Title;
            }
        }

        $arr_status = $this->define_model->get_order_status('status','order');
        $array_status = array();
        foreach($arr_status as $key=>$ite){
            $code = (int)$ite->code;
            $array_status[$code] = $ite->name;
        }   

        ini_set('memory_limit', '3500M');
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');
        require_once 'public/plugin/PHPExcel.php';
        $arr = array();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                     ->setLastModifiedBy("Maarten Balliauw")
                                     ->setTitle("Office 2007 XLSX Test Document")
                                     ->setSubject("Office 2007 XLSX Test Document")
                                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Test result file");
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'STT')
                    ->setCellValue('B1', 'Mã SP')
                    ->setCellValue('C1', 'Tên SP')
                    ->setCellValue('D1', 'Đơn vị')
                    ->setCellValue('E1', '')
                    ->setCellValue('F1', 'Số lượng bán')
                    ->setCellValue('G1', 'Mã đơn hàng')
                    ->setCellValue('H1', 'Ngày bán hàng')
                    ->setCellValue('I1', 'Trạng thái DH');
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A2', 'HÀNG BÁN LẺ');
        if(count($result)>0){
            $i=3;
            $total = 0;
            $j=1;
            foreach($result as $key=>$row){
                $total = $total+$row->Amount;
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $row->MaSP)
                        ->setCellValue('C'.$i, $row->Title)
                        ->setCellValue('D'.$i, $row->Donvi)
                        ->setCellValue('E'.$i, '')
                        ->setCellValue('F'.$i, $row->Amount)
                        ->setCellValue('G'.$i, $row->MaDH)
                        ->setCellValue('H'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                        ->setCellValue('I'.$i,$array_status[$row->Status]);
                    $i++;
                    $j++;
            }
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, '')
                        ->setCellValue('C'.$i, '')
                        ->setCellValue('D'.$i, '')
                        ->setCellValue('E'.$i, 'TỔNG CỘNG')
                        ->setCellValue('F'.$i, $total)
                        ->setCellValue('G'.$i, '')
                        ->setCellValue('H'.$i, '')
                        ->setCellValue('I'.$i,'');
                    $i++;
                    $j++;
        }
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, 'HÀNG COMBO');
                    $i++;
                    $j++;
        if(count($result1)>0){
            $total = 0;
            $j=1;
            foreach($result1 as $key=>$row){
                $combo = isset($arr_bundle[$row->ProductsID]) ? $arr_bundle[$row->ProductsID] : 'Không xác định combo' ;
                $total = $total+$row->Amount;
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $row->MaSP)
                        ->setCellValue('C'.$i, $row->Title)
                        ->setCellValue('D'.$i, $row->Donvi)
                        ->setCellValue('E'.$i, '')
                        ->setCellValue('F'.$i, $row->Amount)
                        ->setCellValue('G'.$i, $row->MaDH)
                        ->setCellValue('H'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                        ->setCellValue('I'.$i,$array_status[$row->Status]);
                    $i++;
                    $j++;
            }
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, '')
                        ->setCellValue('C'.$i, '')
                        ->setCellValue('D'.$i, '')
                        ->setCellValue('E'.$i, 'TỔNG CỘNG')
                        ->setCellValue('F'.$i, $total)
                        ->setCellValue('G'.$i, '')
                        ->setCellValue('H'.$i, '')
                        ->setCellValue('I'.$i,'');
                    $i++;
                    $j++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('CHITIETBANHANG_SANPHAM');
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="CHITIETBANHANG_SANPHAM.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function report_staff(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Staff sales | Report Tools');
        $this->template->write_view('content','admin/sales_staff');
        $this->template->render();
    }

    public function report_products(){
       $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Order report | Report Tools');
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
        $ordertype = isset($_GET['order_type']) ? $_GET['order_type'] : 0 ;
        switch ($ordertype) {
            case 0:
                $ordertype = " and a.OrderType=0";
                break;
            case 1:
                $ordertype = " and a.OrderType in(1,2,4,5)";
                break;
            case 2:
                $ordertype = " and a.OrderType=3";
                break;
            default:
                $ordertype = "";
                break;
        }
        $view_status = " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'" ;
        if($current_view==0){
            $result = $this->db->query("select a.ID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,i.CategoriesID,i.TrademarkID from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and a.Status!=1 $view_status and a.CustomerID!=9996 and a.Status!=1 $ordertype $fillter group by a.ID order by a.Status")->result();
            $result_prodducts = $this->db->query("select h.OrderID,h.Total,h.PriceDown,h.Price,i.CategoriesID,i.TrademarkID,i.RootPrice,h.ProductsID,h.OrderID,h.Amount,i.Title,i.CategoriesID,i.TrademarkID,k.Title as Brand,d.AreaID,e.Title as AreaTitle from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_orderdetails h,ttp_report_products i,ttp_report_trademark k where i.TrademarkID=k.ID and a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and a.Status!=1 $view_status and a.CustomerID!=9996 $ordertype $fillter")->result();
        }else{
            $result = $this->db->query("select a.ID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,i.CategoriesID,i.TrademarkID,i.RootPrice from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 and a.Status!=1 $ordertype $fillter group by a.ID order by a.Status")->result();
            $result_prodducts = $this->db->query("select h.OrderID,h.Total,h.PriceDown,h.Price,i.CategoriesID,i.TrademarkID,i.RootPrice,h.ProductsID,h.OrderID,h.Amount,i.Title,i.CategoriesID,i.TrademarkID,k.Title as Brand,d.AreaID,e.Title as AreaTitle from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i,ttp_report_trademark k where i.TrademarkID=k.ID and a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and a.Status!=1 $view_status and a.CustomerID!=9996 $ordertype $fillter")->result();
        }
        
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result,
            'data_prodducts'=>$result_prodducts,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'base_link' => base_url().ADMINPATH.'/report/import_order/'
        );
        $this->template->write_view('content',"admin/sales_products",$data);
        $this->template->render();
    }

    public function report_supplier(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        $SupplierID = isset($_GET['SupplierID']) ? (int)$_GET['SupplierID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? (int)$_GET['TrademarkID'] : 0 ;
        $ProductsID = isset($_GET['ProductsID']) ? (int)$_GET['ProductsID'] : 0 ;
        $OrderType = isset($_GET['OrderType']) ? (int)$_GET['OrderType'] : 0 ;
        $PaymentStatus = isset($_GET['PaymentStatus']) ? (int)$_GET['PaymentStatus'] : 0 ;
        $data = array(
            'startday'      => $startday,
            'stopday'       => $stopday,
            'WarehouseID'   => $WarehouseID,
            'SupplierID'    => $SupplierID,
            'TrademarkID'   => $TrademarkID,
            'ProductsID'    => $ProductsID,
            'OrderType'     => $OrderType,
            'PaymentStatus' => $PaymentStatus,
            'base_link'     => base_url().ADMINPATH.'/report/import_order/'
        );
        $this->template->add_title('Supplier sales | Report Tools');
        $this->template->write_view('content','admin/sales_supplier',$data);
        $this->template->render();
    }

    public function export_report_supplier(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        $SupplierID = isset($_GET['SupplierID']) ? (int)$_GET['SupplierID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? (int)$_GET['TrademarkID'] : 0 ;
        $ProductsID = isset($_GET['ProductsID']) ? (int)$_GET['ProductsID'] : 0 ;
        $OrderType = isset($_GET['OrderType']) ? (int)$_GET['OrderType'] : 0 ;
        $PaymentStatus = isset($_GET['PaymentStatus']) ? (int)$_GET['PaymentStatus'] : 0 ;

        $payment_ok = $this->db->query("select * from ttp_report_payment_supplier where ProductsID=$ProductsID and SupplierID=$SupplierID")->result();
        $arr_payment_ok = array();
        if(count($payment_ok)>0){
            foreach($payment_ok as $row){
                $arr_payment_ok[$row->ProductsID][$row->ExportID][$row->SupplierID] = $row->Note;
            }
        }

        $arr_shipment = array();
        $arr_shipment_title = array();
        $shipment = $this->db->query("select DISTINCT c.ProductsID,c.ShipmentID,c.PriceCurrency,c.ValueCurrency,c.VAT,b.ShipmentCode from ttp_report_inventory_import a,ttp_report_perchaseorder_details c,ttp_report_shipment b where b.ID=c.ShipmentID and a.POID=c.POID and a.ProductionID=$SupplierID")->result();
        if(count($shipment)>0){
            foreach($shipment as $row){
                $arr_shipment[$row->ProductsID][$row->ShipmentID] = ($row->PriceCurrency*$row->ValueCurrency) + ((($row->PriceCurrency*$row->ValueCurrency)/100)*$row->VAT);
                $arr_shipment_title[$row->ProductsID][$row->ShipmentID] = $row->ShipmentCode;
            }
        }
        $str = $TrademarkID!=0 ? " and a.TrademarkID=$TrademarkID" : "" ;
        $str = $ProductsID!=0 ? " and a.ID=$ProductsID" : "" ;
        $str_combo = $ProductsID!=0 ? " and a.ID=$ProductsID" : "" ;
        $arr_products = array();
        $products = $this->db->query("select a.MaSP,a.ID,a.Title,a.Donvi,d.MaXK,d.ID as ExportID,c.Total,c.Amount,c.Price,c.ShipmentID from ttp_report_products a,ttp_report_order b,ttp_report_orderdetails c,ttp_report_export_warehouse d where b.ID=d.OrderID and a.ID=c.ProductsID and b.ID=c.OrderID and date(d.DateTransferMoney)>='".date('Y-m-d',strtotime($startday))."' and date(d.DateTransferMoney)<='".date('Y-m-d',strtotime($stopday))."' and b.Status=$OrderType and a.VariantType in(0,1) and b.CustomerID!=9996 and b.KhoID=$WarehouseID $str")->result();
        if(count($products)>0){
            foreach($products as $row){
                if($PaymentStatus==0){
                    if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                        if(!isset($arr_products[$row->ID])){
                            $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                            $arr_products[$row->ID]['Title'] = $row->Title;
                            $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                            $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                            $arr_products[$row->ID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                            $arr_products[$row->ID]['List'] = array(
                                0 => array(
                                    'MaXK'  => $row->MaXK,
                                    'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                    'Amount'=> $row->Amount,
                                    'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                    'Type'  => 0,
                                    'Title' => $row->Title,
                                    'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                    'ExportID'=>$row->ExportID
                                )
                            );
                        }else{
                            $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                            $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                            $arr_products[$row->ID]['List'][] = array(
                                'MaXK'  => $row->MaXK,
                                'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                'Amount'=> $row->Amount,
                                'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                'Type'  => 0,
                                'Title' => $row->Title,
                                'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                'ExportID'=>$row->ExportID
                            );
                        }
                    }
                }
                $temp_payment = isset($arr_payment_ok[$row->ID][$row->ExportID][$SupplierID]) ? $arr_payment_ok[$row->ID][$row->ExportID][$SupplierID] : '' ;
                if($PaymentStatus==1){
                    if($temp_payment!=''){
                        if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                            if(!isset($arr_products[$row->ID])){
                                $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                                $arr_products[$row->ID]['Title'] = $row->Title;
                                $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                                $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                                $arr_products[$row->ID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                                $arr_products[$row->ID]['List'] = array(
                                    0 => array(
                                        'MaXK'  => $row->MaXK,
                                        'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                        'Amount'=> $row->Amount,
                                        'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                        'Type'  => 0,
                                        'Title' => $row->Title,
                                        'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                        'ExportID'=>$row->ExportID
                                    )
                                );
                            }else{
                                $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                                $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                                $arr_products[$row->ID]['List'][] = array(
                                    'MaXK'  => $row->MaXK,
                                    'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                    'Amount'=> $row->Amount,
                                    'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                    'Type'  => 0,
                                    'Title' => $row->Title,
                                    'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                    'ExportID'=>$row->ExportID
                                );
                            }
                        }
                    }
                }

                if($PaymentStatus==2){
                    if($temp_payment==''){
                        if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                            if(!isset($arr_products[$row->ID])){
                                $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                                $arr_products[$row->ID]['Title'] = $row->Title;
                                $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                                $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                                $arr_products[$row->ID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                                $arr_products[$row->ID]['List'] = array(
                                    0 => array(
                                        'MaXK'  => $row->MaXK,
                                        'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                        'Amount'=> $row->Amount,
                                        'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                        'Type'  => 0,
                                        'Title' => $row->Title,
                                        'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                        'ExportID'=>$row->ExportID
                                    )
                                );
                            }else{
                                $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                                $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                                $arr_products[$row->ID]['List'][] = array(
                                    'MaXK'  => $row->MaXK,
                                    'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                    'Amount'=> $row->Amount,
                                    'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                    'Type'  => 0,
                                    'Title' => $row->Title,
                                    'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                    'ExportID'=>$row->ExportID
                                );
                            }
                        }
                    }
                }
            }
        }

        $products = $this->db->query("select a.MaSP,a.ID,a.Title,f.Title as Combo,a.Donvi,d.MaXK,d.ID as ExportID,c.Amount,c.Price,c.ShipmentID from ttp_report_products a,ttp_report_order b,ttp_report_orderdetails_bundle c,ttp_report_export_warehouse d,ttp_report_orderdetails e,ttp_report_products f where e.OrderID=b.ID and e.ID=c.DetailsID and e.ProductsID=f.ID and b.ID=d.OrderID and a.ID=c.ProductsID and date(d.DateTransferMoney)>='".date('Y-m-d',strtotime($startday))."' and date(d.DateTransferMoney)<='".date('Y-m-d',strtotime($stopday))."' and b.Status=$OrderType and a.VariantType in(0,1) and b.CustomerID!=9996 and b.KhoID=$WarehouseID $str_combo")->result();
        if(count($products)>0){
            foreach($products as $row){
                $temp_payment = isset($arr_payment_ok[$row->ID][$row->ExportID][$SupplierID]) ? $arr_payment_ok[$row->ID][$row->ExportID][$SupplierID] : '' ;
                if($PaymentStatus==0){
                    if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                        if(!isset($arr_products[$row->ID])){
                            $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                            $arr_products[$row->ID]['Title'] = $row->Title;
                            $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                            $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                            $arr_products[$row->ID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                            $arr_products[$row->ID]['List'] = array(
                                0 => array(
                                    'MaXK'  => $row->MaXK,
                                    'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                    'Amount'=> $row->Amount,
                                    'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                    'Type'  => 1,
                                    'Title' => $row->Combo,
                                    'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                    'ExportID'=>$row->ExportID
                                )
                            );
                        }else{
                            $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                            $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                            $arr_products[$row->ID]['List'][] = array(
                                'MaXK'  => $row->MaXK,
                                'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                'Amount'=> $row->Amount,
                                'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                'Type'  => 1,
                                'Title' => $row->Combo,
                                'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                'ExportID'=>$row->ExportID
                            );
                        }
                    }
                }

                if($PaymentStatus==1){
                    if($temp_payment!=''){
                        if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                            if(!isset($arr_products[$row->ID])){
                                $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                                $arr_products[$row->ID]['Title'] = $row->Title;
                                $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                                $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                                $arr_products[$row->ID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                                $arr_products[$row->ID]['List'] = array(
                                    0 => array(
                                        'MaXK'  => $row->MaXK,
                                        'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                        'Amount'=> $row->Amount,
                                        'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                        'Type'  => 1,
                                        'Title' => $row->Combo,
                                        'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                        'ExportID'=>$row->ExportID
                                    )
                                );
                            }else{
                                $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                                $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                                $arr_products[$row->ID]['List'][] = array(
                                    'MaXK'  => $row->MaXK,
                                    'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                    'Amount'=> $row->Amount,
                                    'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                    'Type'  => 1,
                                    'Title' => $row->Combo,
                                    'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                    'ExportID'=>$row->ExportID
                                );
                            }
                        }
                    }
                }

                if($PaymentStatus==2){
                    if($temp_payment==''){
                        if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                            if(!isset($arr_products[$row->ID])){
                                $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                                $arr_products[$row->ID]['Title'] = $row->Title;
                                $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                                $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                                $arr_products[$row->ID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                                $arr_products[$row->ID]['List'] = array(
                                    0 => array(
                                        'MaXK'  => $row->MaXK,
                                        'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                        'Amount'=> $row->Amount,
                                        'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                        'Type'  => 1,
                                        'Title' => $row->Combo,
                                        'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                        'ExportID'=>$row->ExportID
                                    )
                                );
                            }else{
                                $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                                $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                                $arr_products[$row->ID]['List'][] = array(
                                    'MaXK'  => $row->MaXK,
                                    'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                    'Amount'=> $row->Amount,
                                    'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                    'Type'  => 1,
                                    'Title' => $row->Combo,
                                    'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                    'ExportID'=>$row->ExportID
                                );
                            }
                        }
                    }
                }
            }
        }
        $arr_type = array(0=>"Lẻ",1=>"Combo");
        if(count($arr_products)>0){
            ini_set('memory_limit', '3500M');
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'STT')
                        ->setCellValue('B1', 'Mã sản phẩm')
                        ->setCellValue('C1', 'Tên sản phẩm')
                        ->setCellValue('D1', 'Đơn vị tính')
                        ->setCellValue('E1', 'Tổng số lượng bán')
                        ->setCellValue('F1', 'Tổng số tiền thanh toán (Gồm VAT)')
                        ->setCellValue('G1', 'Số phiếu xuất kho bán hàng')
                        ->setCellValue('H1', 'Loại bán')
                        ->setCellValue('I1', 'Tên combo')
                        ->setCellValue('J1', 'Đơn giá (Chưa VAT)')
                        ->setCellValue('K1', 'Đơn giá (Gồm VAT)')
                        ->setCellValue('L1', 'Số lượng')
                        ->setCellValue('M1', 'Thành tiền thanh toán(Gồm VAT)')
                        ->setCellValue('N1', 'Lô hàng')
                        ->setCellValue('O1', 'Đợt thanh toán');
            $i=2;
            $TotalAmount = 0;
            $TotalPrice = 0;
            foreach($arr_products as $key=>$row){
                $j = $i-1;
                $rowspan = count($row['List'])+1;
                $TotalAmount += $row['Totalamount'];
                $temp = number_format($row['Total']);
                $temp = str_replace(',','',$temp);
                $TotalPrice = $TotalPrice+(int)$temp;
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $j)
                        ->setCellValue('B'.$i, $row['MaSP'])
                        ->setCellValue('C'.$i, $row['Title'])
                        ->setCellValue('D'.$i, $row['Donvi'])
                        ->setCellValue('E'.$i, number_format($row['Totalamount']))
                        ->setCellValue('F'.$i, number_format($row['Total']));
                if(count($row['List'])>0){
                    foreach($row['List'] as $item){
                        $PriceVAT = $item['Price']/1.1;
                        $payment_status = isset($arr_payment_ok[$key][$item['ExportID']][$SupplierID]) ? $arr_payment_ok[$key][$item['ExportID']][$SupplierID] : "" ;
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('G'.$i, $item['MaXK'])
                            ->setCellValue('H'.$i, $arr_type[$item['Type']])
                            ->setCellValue('I'.$i, $item['Title'])
                            ->setCellValue('J'.$i, number_format($PriceVAT))
                            ->setCellValue('K'.$i, number_format($item['Price']))
                            ->setCellValue('L'.$i, number_format($item['Amount']))
                            ->setCellValue('M'.$i, number_format($item['Total']))
                            ->setCellValue('N'.$i, '')
                            ->setCellValue('O'.$i, $payment_status);
                        $i++;
                    }
                }
            }
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('C'.$i, "TỔNG CỘNG")
                        ->setCellValue('F'.$i, number_format($TotalPrice))
                        ->setCellValue('L'.$i, number_format($TotalAmount));
            $objPHPExcel->getActiveSheet()->setTitle('BCKINHDOANH');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="BCKINHDOANH.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Data is empty !";
        }
    }

    public function report_supplier_payment(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        $SupplierID = isset($_GET['SupplierID']) ? (int)$_GET['SupplierID'] : 0 ;
        $TrademarkID = isset($_GET['TrademarkID']) ? (int)$_GET['TrademarkID'] : 0 ;
        $data = array(
            'startday'      => $startday,
            'stopday'       => $stopday,
            'WarehouseID'   => $WarehouseID,
            'SupplierID'    => $SupplierID,
            'TrademarkID'   => $TrademarkID,
            'base_link'     => base_url().ADMINPATH.'/report/import_order/'
        );
        $this->template->add_title('Supplier sales payment | Report Tools');
        $this->template->write_view('content','admin/sales_supplier_payment',$data);
        $this->template->render();
    }

    public function report_source(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Report by source | Report Tools');
        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time());
        $viewtype = isset($_GET['type']) ? 3 : 0 ;

        $result_source = $this->db->query("select b.ID,b.Title,sum(a.SoluongSP) as SoluongSP,sum(a.Total) as Total,count(a.ID) as SLDH from ttp_report_order a,ttp_report_source b where a.SourceID=b.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.OrderType=$viewtype group by b.ID")->result();
        $result_channel = $this->db->query("select b.ID,b.Title,a.SoluongSP,a.Total,a.SourceID,c.Title as SourceTitle from ttp_report_order a,ttp_report_saleschannel b,ttp_report_source c where a.SourceID=c.ID and a.KenhbanhangID=b.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.OrderType=$viewtype")->result();
        $data = array(
            'startday'  => $startday,
            'stopday'   => $stopday,
            'data'      => $result_source,
            'channel'   => $result_channel,
            'base_link' => base_url().ADMINPATH.'/report/import_order/'
        );
        $this->template->add_title('Report by source | Report Tools');
        $this->template->write_view('content','admin/sales_source',$data);
        $this->template->render();
    }

    public function compare_targets_customer(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Sales | Report Tools');
        $this->template->write_view('content','admin/sales_compare_targets_customer');
        $this->template->render();
    }

    public function set_fillter($sessionname='',$sessionvalue=''){
        if($sessionname!=''){
            $this->session->set_userdata($sessionname,$sessionvalue);
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH.'/report/report' ;
        redirect($refer);
    }

    public function set_session_compare(){
        $KPI = "";
        $targets = "";
        if(isset($_POST['KPI'])){
            $this->session->set_userdata('KPI',(int)$_POST['KPI']);
            $KPI = $_POST['KPI'];
        }
        if(isset($_POST['Target'])){
            $this->session->set_userdata('targets',(int)$_POST['Target']);
            $targets = $_POST['Target'];
        }
        if(isset($_POST['Orderstatus'])){
            $this->session->set_userdata('Orderstatus',(int)$_POST['Orderstatus']);
        }
        if($KPI==1){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_doanhso";
            }
        }
        if($KPI==2){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales/report_sanphamban";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_sanphamban";
            }
        }
        if($KPI==3){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales/report_order";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_order";
            }
        }
        if($KPI==4){
            if($targets==''){
                echo base_url().ADMINPATH."/report/report_sales/report_customer";
            }else{
                echo base_url().ADMINPATH."/report/report_sales/compare_targets_customer";
            }
        }
    }

    public function remove_session_conpare(){}

    public function active_payment(){
        $ExportID = isset($_POST['ExportID']) ? (int)$_POST['ExportID'] : 0 ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $SupplierID = isset($_POST['SupplierID']) ? (int)$_POST['SupplierID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        if($ExportID>0 && $Note!='' && $SupplierID>0 && $ProductsID>0){
            if($this->user->UserType==6 || $this->user->IsAdmin==1){
                $check = $this->db->query('select ID from ttp_report_payment_supplier where ExportID='.$ExportID.' and ProductsID='.$ProductsID.' and SupplierID='.$SupplierID.' and Note="$Note"')->row();
                if(!$check){
                    $data = array(
                        'ExportID'  => $ExportID,
                        'Note'      => $Note,
                        'SupplierID'=> $SupplierID,
                        'ProductsID'=> $ProductsID,
                        'Status'    => 1,
                        'UserID'    => $this->user->ID,
                        'Created'   => date('Y-m-d H:i:s')
                    );
                    $this->db->insert("ttp_report_payment_supplier",$data);
                    echo "Đã thanh toán";
                    return;
                }
            }
        }
        echo "False";
    }

}
?>
