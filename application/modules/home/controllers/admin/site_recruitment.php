<?php

class Site_recruitment extends Admin_Controller {

    public $user;
    public $classname = "site_recruitment";
    public $limit = 20;

    public function __construct() {
        parent::__construct();

        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('Recruitment | Config Site');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from auc_recruitment")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from auc_recruitment order by ID ASC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_recruitment/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_recruitment/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/site_recruitment', $data);
        $this->template->render();
    }

    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if ($id != 0) {
            $this->db->delete('ttp_report_promotions', array('ID' => $id));
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add user | Config System');
        $this->load->model('warehouse_model', 'warehouse');
        $WH = $this->warehouse->defineGroupCity();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_promotions/addnew',
            'warehouse' => $WH
        );
        $this->template->write_view('content', 'admin/site_promotions_edit', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : 0;

        $Store = isset($_POST['store']) ? $_POST['store'] : array();
        $Store = array_unique($Store);
        $Store = json_encode($Store);

        $Start = isset($_POST['Start']) ? $_POST['Start'] : '';
        $End = isset($_POST['End']) ? $_POST['End'] : '';
        $data = array(
            'Name' => $Name,
            'Description' => $Description,
            'Status' => $Status,
            'Created' => date('Y-m-d H:i:s'),
            'LastEdited' => date('Y-m-d H:i:s'),
            'Start' => $Start,
            'End' => $End,
            'Store' => $Store
        );
        $this->db->insert("ttp_report_promotions", $data);
        redirect(ADMINPATH . '/home/site_promotions/');
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from auc_recruitment where ID=$id")->row();
            if (!$result)
                show_error("Data is empty !");
            $this->load->model('warehouse_model', 'warehouse');
            $WH = $this->warehouse->defineGroupCity();
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_recruitment/update',
                'data' => $result,
                'warehouse' => $WH
            );
            $this->template->add_title('Edit Recruitment');
            $this->template->write_view('content', 'admin/site_recruitment_edit', $data);
            $this->template->render();
        }
    }

    function autosave() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';
        $Name = isset($_POST['Name']) ? $_POST['Name'] : '';
        $Content = isset($_POST['Content']) ? $_POST['Content'] : '';
        $Content = str_replace("script", "", $Content);

        if ($Name != '') {
            $data = array(
                'Name' => $Name,
                'Content' => $Content,
                'LastEdited' => date("Y-m-d H:i:s")
            );
            $this->db->where("ID", $ID);
            $this->db->update("auc_recruitment", $data);
        }
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = $_POST['ID'];
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $Content = isset($_POST['Content']) ? trim($_POST['Content']) : '';

        $City = isset($_POST['city']) ? $_POST['city'] : array();
        $City = array_unique($City);
        $City = json_encode($City);

        $Start = isset($_POST['Start']) ? $_POST['Start'] : '';
        $End = isset($_POST['End']) ? $_POST['End'] : '';
        if ($Name != '') {
            $recruitment = $this->db->query("select * from auc_recruitment where ID=$ID")->row();
            if ($recruitment) {

                $data = array(
                    'Name' => $Name,
                    'Description' => $Description,
                    'Content' => $Content,
                    'LastEdited' => date('Y-m-d H:i:s'),
                    'Start' => $Start,
                    'End' => $End,
                    'Published' => $Published,
                    'City' => $City
                );
                $this->db->where("ID", $ID);
                $this->db->update("auc_recruitment", $data);
            }
        }
        redirect(ADMINPATH . '/home/site_recruitment/');
    }

}

?>