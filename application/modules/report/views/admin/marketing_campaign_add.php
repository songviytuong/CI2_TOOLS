<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>CHIẾN DỊCH MARKETING</h1>
        </div>
        <div class="block2">
            <!--a class="btn btn-default btn-close"><i class="fa fa-times" aria-hidden="true"></i> Đóng</a>-->
            <div class="btn-group">
                <a class="btn btn-warning addCompaign">
                    <i class="fa fa-save" aria-hidden="true"></i> Tạo mới</a>
            </div>
        </div>
    </div>
    <form name="frmMarket" role="form" data-toggle="validator" id="frmMarket" method="post">
        <div class="row">
            <div class="col-lg-2"><label for="">
                Tên chiến dịch:</label>
            </div>
            <div class="col-lg-10">
                <input type="text" name="market_name" id="market_name" class="form-control" value="" placeholder="Nhập tên chiến dịch..."/>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label for="">
                Mục tiêu chiến dịch:</label>
            </div>
            <div class="col-lg-10">
                <textarea name="market_desc" id="market_desc" class="form-control" placeholder="Nhập nội dung mô tả cho chiến dịch..."></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"><label for="">
                Phân loại:</label>
            </div>
            <div class="col-lg-4">
                <select id="market_type" name="market_type[]" class="form-control">
                    <?php
                        $market_type = $this->define_model->get_order_status('type','campaign','position','asc');
                        foreach($market_type as $key=>$type){
                    ?>
                    <option value="<?=$type->code;?>"><?= $type->name; ?></option>
                    <?php } ?>
                </select>
            </div>
        
            <div class="col-lg-2"><label for="">
                Website mục tiêu:</label>
            </div>
            <div class="col-lg-4">
                <select id="market_domain" name="market_domain[]" class="form-control">
                    <?php
                        foreach($domain as $key=>$dom){
                    ?>
                    <option value="<?=$dom->ID;?>"><?= $dom->DomainName; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-2"><label for="">
                Người phụ trách:</label>
            </div>
            <div class="col-lg-4">
                <select id="owner" name="owner[]" class="form-control">
                    <?php
                        foreach($owner as $key=>$own){
                    ?>
                    <option value="<?=$own->ID;?>"><?=$own->FirstName." ".$own->LastName; ?></option>
                    <?php } ?>
                </select>
            </div>
            
            <div class="col-lg-2"><label for="">
                Người hỗ trợ:</label>
            </div>
            <div class="col-lg-4">
                <select id="owner_action" name="owner_action[]" multiple="multiple">
                    <?php
                        foreach($owner as $key=>$own){
                    ?>
                    <option value="<?=$own->ID;?>"><?=$own->FirstName." ".$own->LastName; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        
            <style>
                .products_containner {padding:5px;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                }
                .content_products {margin-top:0px !important;
                
                }
                .containner .products_containner .content_products .content_tab6 ul li span {
                    color:#000 !important;
                    margin-left:5px;
                }
                .ms-parent {position:absolute; margin-top:10px; width:95%;}
                .ms-drop {margin-top:4px;}
                .open {margin-top:5px;}
                
                #customer-list li:hover {
              background: #eee;
              cursor: pointer;
            }
            .badge{
                font-weight:normal  !important;
            }
            </style>
    </form>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
    .bgTR {background-color:#D8FFD5}
    .ms-parent {margin-top:-5px;}
    .timer td {height:25px;border:1px solid #ccc; padding:5px;}
    td.span {background-color:#fff; padding-left:5px; padding-right:5px; border:1px solid #ccc; width: 80px; text-align: center;}
</style>

<script type="text/javascript">
var baselink = $("#baselink_report").val();

$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $("#close_overlay").click();
    }
});

$("#close_overlay").click(function(){
    $(".over_lay").removeClass('in');
    $(".over_lay").addClass('out');
    setTimeout(function(){
        $(".over_lay").hide();
        disablescrollsetup();
    },200);
});

function enablescrollsetup(){
    $(window).scrollTop(70);
    $("body").css({'height':'100%','overflow-y':'hidden'});
    h = window.innerHeight;
    h = h-200;
    $(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
}

function disablescrollsetup(){
    $("body").css({'height':'auto','overflow-y':'scroll'});
}

setTimeout(function () {
    $('#owner_action').multipleSelect({
        width: '92%',
        isOpen: false,
        keepOpen: false,
        placeholder: "Lựa chọn",
        selectAllText: '-- All --',
        allSelected: 'Tất cả',
        maxHeight: '170',
    });
});
$('#market_name').keypress(function(e){
if(e.which == 13) {
        $('.addCompaign').click();
        return false;
    }
});

$('.addCompaign').click(function(){
    var name = $('#market_name').val();
    if(name == ""){
        alert('Vui lòng nhập tên chiến dịch !!!');
        $('#market_name').focus();
    }else{
        $.ajax({
            url: baselink + "marketing_campaign/addCampaignAction",
            dataType: "JSON",
            type: "POST",
            data: $('#frmMarket').serialize(),
            success: function(result){
                if(result.status == "OK"){
                    window.location = baselink + "marketing_campaign/edit/"+result.lastid+"?_token="+result.token;
                }else{
                    console.log(result);
                    alert('Lỗi hệ thống');
                }
            }
        });
    }
});
</script>
