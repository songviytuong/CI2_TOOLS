<?php

class Products_model extends CI_Model {

    var $tablename = 'ttp_report_products';
    var $total_old_products = 5180;

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function update($id, $params = array()) {
        $update = array();
        if (!empty($params['Status_Import'])) {
            $update['Status_Import'] = $params['Status_Import'];
        }
        $this->db->where('ID', $id);
        $this->db->update($this->tablename, $update);
    }

    function getTotalProducts($action = "") {
        $where = " Where ";
        if ($action == 'published') {
            $where .= "Published = 1 and Status = 1";
        } else {
            $where .= "1";
        }
        $query = $this->db->query('SELECT * FROM ' . $this->tablename . $where);
        $result = $query->num_rows();
        return $result;
    }

    function getProductCounterByDate($params = array()) {
//        $this->db->cache_on();
        $this->db->from($this->tablename);
        $this->db->where('MONTH(LastEdited)', $params['month']);
        $this->db->where('YEAR(LastEdited)', $params['year']);
        if (!empty($params['done'])) {
            $this->db->where('Published', 1);
            $this->db->where('Status', 1);
        }
        if (!empty($params['Owner'])) {
            $this->db->where('Owner', $params['Owner']);
        }
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    function getProducts($fields, $params = array()) {
//        $this->db->cache_on();
        $this->db->select($fields);
        if (!empty($params['ID'])) {
            $this->db->where('ID', $params['ID']);
        }
        $result = $this->db->get($this->tablename)->row();
        if ($result) {
            return $result;
        }
    }

    function getAllProducts() {
        $this->db->select('*');
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function defineAllProducts() {
        $data_products = $this->getAllProducts();
        $res = array();
        foreach ($data_products as $key => $row) {
            $res['ID'] = $row->ID;
        }
        return $res;
    }

    function getTotalPhotoReady() {
        $this->load->model('Imported_model', 'Imported');
        $counter = $this->imported->getCounterDataImport();
        return $counter;
    }

    function getLatestImport() {
        $this->load->model('Imported_model', 'Imported');
        $Lastest = $this->imported->getLastestImport();
        $lastest_import = $Lastest;
        return $lastest_import;
    }

    function getProductCounter($type) {
        $counter = 0;
        switch ($type) {
            case 'addnew':
                $counter = $this->getTotalProducts() - $this->total_old_products;
                break;
            case 'published':
                $counter = $this->getTotalProducts('published');
                break;
            case 'notedited':
                $counter = $this->getTotalProducts() - $this->getTotalProducts('edited');
                break;
            case 'photoready':
                $counter = $this->getTotalPhotoReady();
                break;
        }
        return $counter;
    }

}
