<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
    .body_content .containner .black .box_inner .block1_inner h1{
        font-size:13px;
    }
</style>
<div class="table_data">
    <?php
//        echo $market_id;
//        echo $parent;
        if(true){
    ?>
    <table id="table_data">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center"><input type="text" id="planer_name" name="planer_name" autofocus="true" value="" placeholder="Nhập tên kế hoạch cần làm..." class="form-control"/></th>
            <th class="text-center col-xs-2"><button class="btn btn-danger addPlaner disabled" rel=""><i class="fa fa-plus"></i> Thêm</button></th>
        </tr>
<!--        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center" colspan="2"><textarea id="planer_desc" name="planer_desc" placeholder="Nhập mô tả kế hoạch..." class="form-control"></textarea></th>
        </tr>-->
    </table>
    <div class="clearfix"><br/></div>
    <?php } ?>
    <table id="table_data" class="tablePlaner">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center col-xs-1">STT</th>
            <th class="text-center">Kế hoạch thực hiện</th>
            <th class="text-center col-xs-1"></th>
        </tr>
        <tbody class="showContent">
            
        </tbody>
    </table>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
var baselink = $("#baselink_report").val();
var marketID = "<?= $market_id; ?>";
var parentID = "<?= $parent; ?>";

$(document).ready(function () {
    setTimeout(function(){
        loadPlaner();
    },100);
});

$('#planer_name').keypress(function(e){
    if(e.which == 13) {
        $('.addPlaner').click();
    }
});

$('#planer_name').keyup(function(){
    if($(this).val() == ""){
        $('.addPlaner').addClass("disabled");
    }else{
        $('.addPlaner').removeClass("disabled");
    }
});

function loadPlaner(){
    $.ajax({
        url: baselink+"marketing_campaign/loadPlaner",
        dataType: "html",
        type: "POST",
        data: "market_id="+marketID+"&parent="+parentID,
        success: function(result){
            $('.showContent').html(result);
        }
    });
}

$('.addPlaner').click(function(){
    var planerName = $('#planer_name').val();
    if(planerName == ""){
        alert('Nhập tên kế hoạch cần làm !!!');
        $('#planer_name').focus();
    }else{
        $.ajax({
            url: baselink+"marketing_campaign/addPlaner",
            dataType: "html",
            type: "POST",
            data: "planerName="+planerName+"&market_id="+marketID+"&parent="+parentID,
            success: function(result){
                if(result == "OK"){
                    loadPlaner();
                    $('#planer_name').val("");
                    $('#planer_name').focus();
                }else{
                    alert('Lỗi hệ thống');
                }
            }
        });
    }
});

$("#close_overlay").click(function(){
    $(".over_lay").removeClass('in');
    $(".over_lay").addClass('out');
    location.reload();
});

function removePlaner(id){
    if (!confirm('Bạn muốn xóa?\nLưu ý: Toàn bộ hành động thuộc kế hoạch này sẽ bị xóa :(')) {
        return false;
    }else{
        $.ajax({
            url: baselink+"marketing_campaign/removePlaner",
            dataType: "html",
            type: "POST",
            data: "id="+id,
            success: function(result){
                if(result == "OK"){
                    loadPlaner();
                }else{
                    alert('Lỗi hệ thống');
                }
            }
        });
    }
}
</script>