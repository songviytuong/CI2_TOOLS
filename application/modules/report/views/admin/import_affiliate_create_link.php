<div class="containner">
	<h3 style='margin-top:0px'>Tạo link giới thiệu sản phẩm</h3>
	<hr>
	<div class="row">
		<label for="" class="col-xs-2 control-label">Copy link</label>
		<div class="col-xs-10">
			<input type='text' id="baselink_create" class="form-control" placeholder="Ví dụ : http://example.con/details-products-number-01" />
		</div>
	</div>
	<div class="row">
		<label for="" class="col-xs-2 control-label"></label>
		<div class="col-xs-10">
			<a class='btn btn-default' onclick='createlink()'><i class="fa fa-pencil" aria-hidden="true"></i> Tạo liên kết mới cho bạn</a>
		</div>
	</div>
	<div class="row">
		<label for="" class="col-xs-2 control-label">Link mới của bạn :</label>
		<label class="col-xs-10 control-label"><textarea id="linkcreated" class="form-control"></textarea></label>
	</div>
	<div class="alert alert-success">
		<p style='text-align:justify;'>Bạn có thể dùng liên kết này để giới thiệu với khách hàng tiềm năng mua hàng thông qua Email, mạng xã hội hoặc tất cả các hình thức quảng cáo dạng hình ảnh hay keywords khác do bạn tự tạo. </p>
	</div>
</div>
<script>
	function createlink(){
		var link = $("#baselink_create").val();
		if(link==''){
			$("#baselink_create").focus();
		}else{
			$("#linkcreated").val("Loading...");
			$.ajax({
	            url: "<?php echo $base_link.'active_create_link?link=' ?>"+link,
	            dataType: "html",
	            type: "POST",
	            data: "",
	            success: function(result){
	                $("#linkcreated").val(result);
	            }
	        });
			
		}
	}

</script>