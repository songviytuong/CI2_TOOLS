<?php 
	if(!isset($_GET['type'])){
		$_GET['type'] = 2;
	}
	$arr = array(0=>"Header menu",1=>"Content menu");
?>
<style>table tr th{background:#EEE;}</style>
<div class="containner">
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-6">
				<h3 style="margin:0px">Cấu hình menu chức năng</h3>
			</div>
			<div class="col-xs-6 text-right">
				<a class="btn btn-danger" href="<?php echo $base_link.'add'; ?>"> <i class="fa fa-plus"></i> Tạo mới</a>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-12">
				<label class="col-xs-6 control-label">
					Tìm thấy <?php echo number_format($find) ?> dữ liệu theo yêu cầu .
				</label>
				<div class="col-xs-6">
					<div class="col-xs-6 pull-right">
						<form action="<?php echo $base_link ?>">
	                		<input type="text" class="form-control" name="keywords" value="<?php echo $keywords ?>" placeholder="Nhập từ khóa cần tìm..." />
	                	</form>
	                </div>
					<div class="btn-group pull-right" style="margin-right:10px">
	                    <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Loại menu</a>
	                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	                    <ul class="dropdown-menu">
	                    	<?php
	                    	$select = $_GET['type']==2 ? "style='background:#beddfb'" : "" ; 
	                    	echo "<li $select><a href='".$base_link."?type=2'>Tất cả</a></li>";
							$select = 0==$_GET['type'] ? "style='background:#beddfb'" : "" ;
							echo "<li $select><a href='".$base_link."?type=0'>Menu header</a></li>";
							$select = 1==$_GET['type'] ? "style='background:#beddfb'" : "" ;
							echo "<li $select><a href='".$base_link."?type=1'>Menu content</a></li>";
							?>
	                    </ul>
	                </div>
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<table class="table table-bordered">
			<tr>
				<th>STT</th>
				<th>Loại menu</th>
				<th>Tiêu đề menu</th>
				<th>Đích đến</th>
				<th>Position</th>
				<th>Trạng thái</th>
				<th>Action</th>
			</tr>
			<?php 
			if(count($data)>0){
				$arr_status = array(0=>"Không hiển thị",1=>"Hiển thị");
				$i=$start+1;
				foreach($data as $row){
					echo "<tr>";
					echo "<td>$i</td>";
					echo "<td>".$arr[$row->Type]."</td>";
					echo "<td>$row->Title</td>";
					echo "<td>$row->Alias</td>";
					echo "<td>$row->Position</td>";
					echo "<td>".$arr_status[$row->Published]."</td>";
					echo "<td><a href='".$base_link."edit/$row->ID'>Edit</a> | <a class='delete' href='".$base_link."delete/$row->ID'>Delete</a></td>";
					echo "</tr>";
					$i++;
				}
			}else{
				echo "<tr><td colspan='7'>Không tìm thấy dữ liệu .</td></tr>";
			}
			?>
		</table>
		<?php echo str_replace("href=","onclick='changelinks(this)' data=",$nav); ?>
	</div>
</div>
<script>
	function changelinks(ob){
		var data = $(ob).attr('data');
		var query = "?<?php echo http_build_query($_GET); ?>";
		window.location = data+query;
	}
</script>