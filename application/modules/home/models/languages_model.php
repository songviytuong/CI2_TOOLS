<?php

class Languages_model extends CI_Model {

    var $tablename = 'ttp_languages';
    var $tablename_traslate = 'ttp_languages_translate';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getAllLanguages($orderby = 'LangPosition', $sort = 'ASC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }
    
    function getAllTranslates($orderby = 'LangID', $sort = 'DESC') {
        $this->db->select('*');
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get($this->tablename_traslate)->result();
        return $result;
    }

}