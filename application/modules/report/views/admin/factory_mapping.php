<div class="containner">
	<h3 class="step_number_title"><span>1</span> Chọn kho / nhà máy quét sản phẩm</h3>
	<div class="row">
		<div class="col-xs-12">
			<select class="form-control" name="WarehouseID" id="WarehouseID">
				<?php 
				$warehouse = $this->db->query("select MaKho,ID from ttp_report_warehouse")->result();
				if(count($warehouse)>0){
					$warehousedefault = isset($_GET['warehouse']) ? $_GET['warehouse'] : 7 ;
					foreach($warehouse as $row){
						$selected = $row->ID==$warehousedefault ? "selected='selected'" : "" ;
						echo "<option value='$row->ID' $selected>$row->MaKho</option>";
					}
				}
				?>
			</select>
		</div>
	</div>
	<h3 class="step_number_title"><span>2</span> Chọn loại sản phẩm & lô sản xuất</h3>
	<div class="row">
    	<div class="col-xs-6">
    		<select class="form-control" id="ProductsID">
    			<option value="0">-- Chọn loại sản phẩm --</option>
    			<?php 
    			$products = $this->db->query("select ID,MaSP,Title from ttp_report_products where CodeManage=1")->result();
    			if(count($products)>0){
    				$default = isset($_GET['products']) ? $_GET['products'] : 0 ;
    				foreach($products as $row){
    					$selected = $default==$row->ID ? "selected='selected'" : "" ;
    					echo "<option value='$row->ID' $selected>$row->MaSP - $row->Title</option>";
    				}
    			}
    			?>
    		</select>
    	</div>
    	<div class="col-xs-6">
    		<div class="input-group">
	    		<select class="form-control" id="ShipmentID">
	    			<option value="0">-- Chọn lô sản xuất --</option>
	    			<?php 
	    			$shipment = $this->db->query("select a.ID,a.ShipmentCode,b.MaSP from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and b.CodeManage=1 order by b.ID DESC")->result();
	    			if(count($shipment)>0){
	    				$default = isset($_GET['shipment']) ? $_GET['shipment'] : 0 ;
	    				foreach($shipment as $row){
	    					$selected = $default==$row->ID ? "selected='selected'" : "" ;
	    					echo "<option value='$row->ID' $selected>$row->MaSP - $row->ShipmentCode</option>";
	    				}
	    			}
	    			?>
	    		</select>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" onclick="add_shipment(this)"><i class="fa fa-plus"></i> Tạo lô mới</button>
                </span>
            </div>
    	</div>
    </div>
	<h3 class="step_number_title"><span>3</span> Chọn loại thùng hàng</h3>
    <div class="row">
    	<div class="col-xs-12">
    		<select class="form-control" onchange="changecarton(this)" id="defaultproducts">
    			<option value="0">-- Chọn loại thùng hàng --</option>
    			<?php 
    			if(count($data)>0){
    				$default = isset($_GET['default']) ? $_GET['default'] : 0 ;
    				foreach($data as $row){
    					$selected = $default==$row->ID ? "selected='selected'" : "" ;
    					echo "<option value='$row->ID' $selected>$row->MaSP - $row->Title</option>";
    				}
    			}
    			?>
    		</select>
    	</div>
    </div>
    <h3 class="step_number_title"><span>4</span> Quét mã QR Code</h3>
    <div class="row table_data" style="width:auto">
    	<div class="col-xs-12" id="tankage">
    		<table>
    			<tr>
    				<th>STT</th>
    				<th>Mã QR Code</th>
    				<th>Nội dung</th>
    				<th class="width50"></th>

    			</tr>
    			<tr>
    				<td colspan='4' style='padding:10px !important'>Chưa chọn thùng hàng</td>
    			</tr>
    		</table>
    	</div>
    </div>
    <h3 class="step_number_title"><span>5</span> Quét mã BAR Code thùng hàng</h3>
    <div class="row">
    	<div class="col-xs-12">
    		<input type="text" id="barcode" class="form-control" placeholder="Quét mã barcode thùng ..." onchange="enterbarcode(this)" />
		</div>
	</div>
	<div class="row activefull hidden">
		<div class="col-xs-10">
			<a class='btn btn-success' onclick='save_mapping(this,0)'><i class="fa fa-location-arrow" aria-hidden="true"></i> Tiếp tục với thùng đang chọn</a>
			<a class='btn btn-primary' onclick='save_mapping(this,1)'><i class="fa fa-archive" aria-hidden="true"></i> Tiếp tụp với thùng khác</a>
			<a class='btn btn-danger' onclick='save_mapping(this,2)'><i class="fa fa-times" aria-hidden="true"></i> Lưu & kết thúc</a>
		</div>
		<div class="cancel_"></div>
	</div>

	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>

<style>
	table tr td{padding: 0px 10px !important}
	table tr td input{border:none !important;box-shadow:none !important;}
</style>
<script>
	var baselink = "<?php echo $baselink?>";
	var arr = new Array();
	var arr_qrcode = new Array();
	function changecarton(ob){
		var arr = new Array();
		var arr_qrcode = new Array();
		var data = $(ob).val();
		if(data>0){
			$("#tankage").load("<?php echo $baselink.'load_tankage/' ?>"+data);
		}
	}

	function enterbarcode(ob){
		var barcode = $(ob).val();
		if(barcode!=''){
			$.ajax({
	            dataType: "json",
	            data: "data="+barcode,
	            url: baselink+'check_barcode',
	            cache: false,
	            method: 'POST',
	            success: function(result) {
	            	if(result.error!=null){
	            		$("#barcode").attr('data',result.error);
	            		$(".activefull").prepend('<div class="col-xs-2"><img src="'+baselink+"barcode_generator/"+barcode+'" /></div>');
	            		if(arr.length==$(".qrcodeinput").length){
							$(".activefull").removeClass("hidden");
						}
					}else{
						alert("Mã BAR Code không được chấp nhận . Vui lòng thử lại nhé !");
						$(ob).val('');
						$(ob).focus();
					}
	            }
	        });	
		}
	}

	function enterqrcode(ob){
		var data = $(ob).val();
		var code = data.split('=',2);
		code = code[1];
		if(jQuery.inArray( "data"+code, arr )<0){
			var next = parseInt($(ob).attr('data')) + 1;
			$.ajax({
	            dataType: "json",
	            data: "data="+data,
	            url: baselink+'check_qrcode',
	            cache: false,
	            method: 'POST',
	            success: function(result) {
	            	console.log(result);
	                if(result.error!=null){
		                $(ob).attr("data",result.error);
		                $(ob).attr("readonly","true");
		                $(".qrcodeinput"+next).focus();
						$(ob).parent('td').parent('tr').find('span').css({'background':'#62A8EA'});
						$(ob).parent('td').parent('tr').find('.text-success').find('i').removeClass('hidden');
						$(ob).parent('td').parent('tr').find('.qrcodeimage').find('img').removeClass('hidden').attr('src',baselink+"qrcode_generator/"+result.error);
						arr.push("data"+code);
						arr_qrcode.push(result.error);
						if(arr.length==$(".qrcodeinput").length){
							$("#barcode").focus();
						}
					}else{
						alert("Mã QR Code không được chấp nhận . Vui lòng thử lại nhé !");
						$(ob).val('');
						$(ob).focus();
					}
	            }
	        });	
		}else{
			alert("Mã QR Code đã sử dụng . Vui lòng thử lại nhé !");
			$(ob).val('');
			$(ob).focus();
		}
	}

	function save_mapping(ob,status){
		$(ob).addClass("saving");
		$(ob).find('i').hide();
		var barcode = $("#barcode").attr('data');
		var warehouse = $("#WarehouseID").val();
		var products = $("#ProductsID").val();
		var shipment = $("#ShipmentID").val();
		var carton = $("#defaultproducts").val();
		var warehousetext = $('#WarehouseID').find('option:selected').text();
		if(barcode!='' && carton!=0 && warehouse!=0 && products!=0 && shipment!=0){
			$.ajax({
	            dataType: "json",
	            data: "qrcode="+JSON.stringify(arr_qrcode)+"&barcode="+barcode+"&WarehouseID="+warehouse+"&WarehouseTitle="+warehousetext+"&ProductsID="+products+"&ShipmentID="+shipment+"&Carton="+carton,
	            url: baselink+'save_mapping',
	            method: 'POST',
	            success: function(result) {
	            	console.log(result);
	            	if(result.error==null){
	            		if(status==2){
	            			window.location = baselink+"barcode";
	            		}
		            	if(status==1){
		                	var defaultwarehouse = $("#WarehouseID").val();
		                	var defaultproducts = $("#ProductsID").val();
		                	var defaultshipment = $("#ShipmentID").val();
		                	window.location = baselink+"map_qrcode_barcode?warehouse="+defaultwarehouse+"&products="+defaultproducts+"&shipment="+defaultshipment;
		                }
		                if(status==0){
		                	var defaultwarehouse = $("#WarehouseID").val();
		                	var defaultcarton = $("#defaultproducts").val();
		                	var defaultproducts = $("#ProductsID").val();
		                	var defaultshipment = $("#ShipmentID").val();
		                	window.location = baselink+"map_qrcode_barcode?default="+defaultcarton+"&warehouse="+defaultwarehouse+"&products="+defaultproducts+"&shipment="+defaultshipment;
		                }
	                }else{
	                	alert(result.error);
	                }
	            },
	            error:function(result){
	            	console.log(result);
	            	alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu !");
	            }
	        });
        }
	}

	function add_shipment(ob){
		var ProductsID = $("#ProductsID").val();
		if(ProductsID==0 || ProductsID==''){
			alert("Vui lòng chọn loại sản phẩm");
			return false;
		}
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
		$(".over_lay .box_inner .block2_inner").html("");
		$(".over_lay .box_inner .block2_inner").load("<?php echo base_url().ADMINPATH.'/report/factory/' ?>box_add_shipment/"+ProductsID);
		$(".over_lay").removeClass('in');
    	$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: "<?php echo base_url().ADMINPATH.'/report/factory/' ?>save_shipment",
            dataType: "json",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	if(result.Error==''){
	            	$(".over_lay").hide();
					disablescrollsetup();
					$(ob).removeClass("saving");
					$("#ShipmentID").append('<option value="'+result.ID+'">'+result.Title+'</option');
					$("#ShipmentID").val(result.ID);
				}else{
					alert("Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server !");
				}
            }
        });
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$(document).ready(function(){
		$("#defaultproducts").change();
	});

	$("#ProductsID").change(function(){
		var datachange = $(this).val();
		$("#ShipmentID").load("<?php echo base_url().ADMINPATH.'/report/factory/load_shipment_by_products/' ?>"+datachange);
	});
</script>
<style>
    .daterangepicker{width: auto;}
</style>