<form action="<?php echo base_url().ADMINPATH.'/report/partner_affiliate/create_voucher' ?>" method="post">
<div class="row">
	<label class="col-xs-3 control-label">Giá trị chiết khấu (% hoặc giá trị)</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-6">
				<input type="number" class="form-control" name="PercentReduce" id="PercentReduce" placeholder="% chiết khấu" />
			</div>
			<div class="col-xs-6">
				<input type="number" class="form-control" name="PriceReduce" id="PriceReduce" placeholder="Giá trị chiết khấu" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<label class="col-xs-3 control-label">Thời gian hiệu lực</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-6">
				<input type="text" class="form-control required" name="Startday" id="Startday" value="<?php echo date('Y-m-d') ?>" placeholder="Ngày bắt đầu" required />
			</div>
			<div class="col-xs-6">
				<input type="text" class="form-control required" name="Stopday" id="Stopday" value="<?php echo date('Y-m-d') ?>" placeholder="Ngày kết thúc" required />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<label class="col-xs-3 control-label">Loại hình sử dụng voucher</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-12">
				<select class="form-control" name="UseOne">
					<option value='0'>Sử dụng 1 lần</option>
					<option value='1'>Sử dụng nhiều lần</option>
				</select>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<label class="col-xs-3 control-label">Số lượng voucher cần khởi tạo</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-6">
				<input type="number" class="form-control required" name="Amount" value="1" max="100" required />
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12">
		<button type="submit" class="btn btn-danger"><i class="fa fa-reply-all"></i> Khởi tạo</button>
	</div>
</div>
</form>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Startday,#Stopday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });
</script>