<?php
$url = base_url().ADMINPATH.'/report';
$segment_current = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-shopping-cart"></i> Doanh số</p>
    <ul>
        <li><a href="<?php echo $url.'/import_targets' ?>" <?php echo $segment_current=='import_targets' ? "class='active'" : '' ; ?>>Chỉ tiêu doanh số</a></li>
        <li><a href="<?php echo $url.'/import_order' ?>" <?php echo $segment_current=='import_order' ? "class='active'" : '' ; ?>>Đơn hàng</a></li>
        <li><a href="<?php echo $url.'/request_cancel' ?>" <?php echo $segment_current=='request_cancel' ? "class='active'" : '' ; ?>>Yêu cầu hủy ĐH</a>
        <?php
        if($this->user->UserType==5 || $this->user->UserType==7 || $this->user->UserType==8){
            $bonus = "";
            $bonus = $this->user->UserType==5 ? " and b.UserType=1" : $bonus ;
            $bonus = $this->user->UserType==7 ? " and b.UserType=3" : $bonus ;
            $bonus = $this->user->UserType==8 ? " and b.UserType=2" : $bonus ;
            $cancel = $this->db->query("select count(1) as Soluong from ttp_report_request_cancelorder a,ttp_user b where a.Status=0 and a.UserID=b.ID $bonus")->row();
            echo $cancel->Soluong>0  ? "<span class='notification_num'>$cancel->Soluong</span>" : "" ;
        }
        ?>
        </li>
        <li><a href="<?php echo $url.'/asign_order' ?>" <?php echo $segment_current=='asign_order' ? "class='active'" : '' ; ?>>Đơn hàng từ website</a></li>
    </ul>
    <p class="title"><i class="fa fa-line-chart"></i> Kênh bán hàng</p>
    <ul>
        <li><a href="<?php echo $url.'/import_customer' ?>" <?php echo $segment_current=='import_customer' && ($segment_current_bonus=='' || $segment_current_bonus=='add' || $segment_current_bonus=='edit') ? "class='active'" : '' ; ?>>Danh sách khách hàng</a></li>
        <li><a href="<?php echo $url.'/import_customer/comments' ?>" <?php echo $segment_current=='import_customer' && $segment_current_bonus=='comments' ? "class='active'" : '' ; ?>>Khách hàng (comments)</a></li>
        <li><a href="<?php echo $url.'/import_customer/callme' ?>" <?php echo $segment_current=='import_customer' && $segment_current_bonus=='callme' ? "class='active'" : '' ; ?>>Khách hàng (call to me)</a></li>
        <li><a href="<?php echo $url.'/import_customer/prize' ?>" <?php echo $segment_current=='import_customer' && $segment_current_bonus=='prize' ? "class='active'" : '' ; ?>>Khách hàng (lucky draw)</a></li>
        <li><a href="<?php echo $url.'/import_customer/lucky_number' ?>" <?php echo $segment_current=='import_customer' && $segment_current_bonus=='lucky_number' ? "class='active'" : '' ; ?>>Khách hàng (lucky number)</a></li>
        <li><a href="<?php echo $url.'/import_customer/sms' ?>" <?php echo $segment_current=='import_customer' && $segment_current_bonus=='sms' ? "class='active'" : '' ; ?>>Khách hàng (SMS voucher)</a></li>
    </ul>
    <p class="title"><i class="fa fa-trophy" aria-hidden="true"></i> Game data</p>
    <ul>
        <li><a href="<?php echo $url.'/import_customer/quiz_question' ?>" <?php echo $segment_current=='import_customer' && $segment_current_bonus=='quiz_question' ? "class='active'" : '' ; ?>>Quiz | Danh sách câu hỏi</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>

<script src="public/admin/js/notify.js"></script>
<?php
if($this->user->UserType==5 || $this->user->UserType==7 || $this->user->UserType==8){
?>
<script>
    var maxnoti = <?php echo isset($cancel) ? $cancel->Soluong : 0 ; ?>;
    var accept = '';
    var username = '';
    var madh = '';
    var getcurrentorder = function(){
        $.ajax({
            url: "<?php echo base_url().ADMINPATH; ?>/report/import/check_notify",
            dataType: "html",
            type: "POST",
            data: "Max="+maxnoti,
            success: function(result){
                if(result!="false"){
                    var arrnotify = result.split("|");
                    maxnoti = arrnotify[0];
                    var accept = arrnotify[1];
                    var username = arrnotify[2];
                    var madh = arrnotify[3];
                    var img = arrnotify[4];
                    if(accept==0){
                        notifyMe(img,username +' vừa gửi yêu cầu hủy đơn hàng '+madh);
                    }else{
                        var useraccept = arrnotify[5];
                        notifyMe(img,useraccept + ' vừa xử lý yêu cầu hủy đơn hàng '+ madh +' của '+username);
                    }
                }
            }
        });
    }
    setInterval(getcurrentorder,5000);

    function notifyMe(image,message){
        notify('THÔNG BÁO HỆ THỐNG', {
            body: message,
            icon: image,
            onclick: function(e) {
                window.location = "<?php echo base_url().ADMINPATH.'/report/request_cancel' ?>";
            },
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }
<?php
}
?>
</script>
