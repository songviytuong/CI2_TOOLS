<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$baselink = ADMINPATH.'/report/import_order/';
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <button class="btn btn-default hidden-xs hidden-sm" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
            <a class="btn btn-primary hidden-xs hidden-sm" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export to Excel</a>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="filltertools">
        <form action="<?php echo $base_link."setfillter" ?>" method="post">
            <?php 
                $arr_fieldname = array(0=>"c.Name",1=>"a.CustomerType",2=>"e.ID",3=>"d.ID",4=>"b.ID",5=>"a.SoluongSP",6=>"a.Total",7=>"a.Chiphi",8=>"a.Status",9=>"a.UserID",10=>"a.TransportID",11=>"c.Phone1",12=>"a.MaDH",15=>"i.MaSP",16=>"i.Title",17=>"i.CategoriesID",18=>"i.TradeMarkID");
                $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                $arr_showfieldname = array(0=>"Tên khách hàng",1=>"Loại khách hàng",2=>"Khu vực",3=>"Tỉnh thành",4=>"Quận huyện",5=>"Số lượng sản phẩm",6=>"Tổng tiền hàng",7=>"Chi phí vận chuyển",8=>"Trạng thái đơn hàng",9=>"Nhân viên khởi tạo",10=>"Đối tác vận chuyển",11=>"Số điện thoại",12=>"Mã đơn hàng",15=>"Mã SKU",16=>"Tên sản phẩm",17=>"Nhóm ngành hàng",18=>"Thương hiệu",19=>"Nhà cung cấp");
                $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                $fill_data_arr = explode(" and ",$fill_data);
                if(count($fill_data_arr)>0 && $fill_data!=''){
                    $temp_tools=0;
                    foreach($fill_data_arr as $row){
                        $param = explode(' ',$row,3);
                        $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                        $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                        $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                        $param_value = isset($param[2]) ? $param[2] : '' ;
                        $param_value = str_replace("\'","",$param_value);
                        $param_value = str_replace("'","",$param_value);
                        $param_value = str_replace("%","",$param_value);
                        $param_value = str_replace('"',"",$param_value);
            ?> 
            <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                <div class="list_toolls col-xs-1 hidden-xs hidden-sm"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                <div class="list_toolls col-xs-12 col-sm-3 col-md-2">
                    <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                    <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                    <ul class="dropdownbox">
                        
                        <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                            <ul>
                                <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                <li><a onclick="setfield(this,11,'sodienthoai')">Số điện thoại</a></li>
                            </ul>
                        </li>
                        <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                            <ul>
                                <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                            </ul>
                        </li>
                        <li><span onclick="showul(this)"><b class="caret"></b> Sản phẩm</span>
                            <ul>
                                <li><a onclick="setfield(this,15,'sku')">Mã SKU</a></li>
                                <li><a onclick="setfield(this,16,'tensanpham')">Tên sản phẩm</a></li>
                                <li><a onclick="setfield(this,17,'nhomnganhhang')">Nhóm ngành hàng</a></li>
                                <li><a onclick="setfield(this,18,'thuonghieu')">Thương hiệu</a></li>
                                <!--<li><a onclick="setfield(this,17,'nhacungcap')">Nhà cung cấp</a></li>-->
                            </ul>
                        </li>
                        <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                            <ul>
                                <li><a onclick="setfield(this,12,'madh')">Mã đơn hàng</a></li>
                                <li><a onclick="setfield(this,9,'user')">Nhân viên khởi tạo</a></li>
                                <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="list_toolls reciveroparation col-xs-4 col-sm-3 col-md-2">
                    <select class="oparation form-control" name="FieldOparation[]">
                        <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                        <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                        <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                        <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                        <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                        <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                        <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                    </select>
                </div>
                <div class="list_toolls reciverfillter col-xs-6 col-sm-3 col-md-3">
                    <?php 
                    if($value_field==2 || $value_field==3 || $value_field==4){
                        if($value_field==2){
                            $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();    
                        }
                        if($value_field==3){
                            $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();    
                        }
                        if($value_field==4){
                            $result = $this->db->query("select * from ttp_report_district order by Title ASC")->result();    
                        }
                        if(count($result)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($result as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==8) {
                        echo "<select name='FieldText[]' class=' form-control'>";
                        echo $param_value==10 ? "<option value='10' selected='selected'>Đơn hàng chờ hủy</option>" : "<option value='10'>Đơn hàng chờ hủy</option>" ;
                        echo $param_value==9 ? "<option value='9' selected='selected'>Đơn hàng chuyển sang nv điều phối</option>" : "<option value='9'>Đơn hàng chuyển sang nv điều phối</option>" ;
                        echo $param_value==8 ? "<option value='8' selected='selected'>Đơn hàng bị trả về</option>" : "<option value='8'>Đơn hàng bị trả về</option>" ;
                        echo $param_value==7 ? "<option value='7' selected='selected'>Chuyển sang bộ phận giao hàng</option>" : "<option value='7'>Chuyển sang bộ phận giao hàng</option>" ;
                        echo $param_value==6 ? "<option value='6' selected='selected'>Đơn hàng bị trả về từ kế toán</option>" : "<option value='6'>Đơn hàng bị trả về từ kế toán</option>" ;
                        echo $param_value==5 ? "<option value='5' selected='selected'>Đơn hàng chờ kế toán duyệt</option>" : "<option value='5'>Đơn hàng chờ kế toán duyệt</option>" ;
                        echo $param_value==4 ? "<option value='4' selected='selected'>Đơn hàng bị trả về từ kho</option>" : "<option value='4'>Đơn hàng bị trả về từ kho</option>" ;
                        echo $param_value==3 ? "<option value='3' selected='selected'>Đơn hàng mới chờ kho duyệt</option>" : "<option value='3'>Đơn hàng mới chờ kho duyệt</option>" ;
                        echo $param_value==2 ? "<option value='2' selected='selected'>Đơn hàng nháp</option>" : "<option value='2'>Đơn hàng nháp</option>" ;
                        echo $param_value==0 ? "<option value='0' selected='selected'>Đơn hàng thành công</option>" : "<option value='0'>Đơn hàng thành công</option>" ;
                        echo $param_value==1 ? "<option value='1' selected='selected'>Đơn hàng hủy</option>" : "<option value='1'>Đơn hàng hủy</option>" ;
                        $fill_temp_value = $param_value==10 ? "Đơn hàng chờ hủy" : $fill_temp_value ;
                        $fill_temp_value = $param_value==9 ? "Đơn hàng chuyển sang nv điều phối" : $fill_temp_value ;
                        $fill_temp_value = $param_value==8 ? "Đơn hàng bị trả về" : $fill_temp_value ;
                        $fill_temp_value = $param_value==7 ? "Chuyển sang bộ phận giao hàng" : $fill_temp_value ;
                        $fill_temp_value = $param_value==6 ? "Đơn hàng bị trả về từ kế toán" : $fill_temp_value ;
                        $fill_temp_value = $param_value==5 ? "Đơn hàng chờ kế toán duyệt" : $fill_temp_value ;
                        $fill_temp_value = $param_value==4 ? "Đơn hàng bị trả về từ kho" : $fill_temp_value ;
                        $fill_temp_value = $param_value==3 ? "Đơn hàng mới chờ kho duyệt" : $fill_temp_value ;
                        $fill_temp_value = $param_value==2 ? "Đơn hàng nháp" : $fill_temp_value ;
                        $fill_temp_value = $param_value==1 ? "Đơn hàng hủy" : $fill_temp_value ;
                        $fill_temp_value = $param_value==0 ? "Đơn hàng thành công" : $fill_temp_value ;
                        echo "</select>";
                    }elseif ($value_field==9) {
                        $userlist = $this->db->query("select ID,UserName from ttp_user where UserType=1")->result();
                        if(count($userlist)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($userlist as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->UserName : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->UserName</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==17) {
                        $categories = $this->db->query("select ID,Title from ttp_report_categories where IsLast=1")->result();
                        if(count($categories)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($categories as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==18) {
                        $categories = $this->db->query("select ID,Title from ttp_report_trademark")->result();
                        if(count($categories)>0){
                            echo "<select name='FieldText[]' class=' form-control'>";
                            foreach($categories as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }
                    }elseif ($value_field==1) {
                        echo "<select name='FieldText[]' class=' form-control'>
                                <option value='0'>Khách hàng mới</option>
                                <option value='1'>Khách hàng cũ</option>
                            </select>";
                        $fill_temp_value = $param_value==1 ? "Khách hàng cũ" : $fill_temp_value ;
                        $fill_temp_value = $param_value==0 ? "Khách hàng mới" : $fill_temp_value ;
                    }elseif ($value_field==10) {
                        echo "<select name='FieldText[]' class=' form-control'>";
                        $transport = $this->db->query("select * from ttp_report_transport")->result();
                        foreach($transport as $row){
                            $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                            $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                        }
                        echo "</select>";
                    }else{
                        $fill_temp_value = $param_value ;
                        echo '<input type="text" name="FieldText[]" class="form-control" id="textsearch" value="'.$param_value.'" />';
                    }
                    ?>
                </div>
                <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
            </div>
            <?php   
                    $temp_tools++;
                    $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                    $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                    }
                }else{
            ?>
                <div class="row base_row">
                    <div class="list_toolls col-xs-1 hidden-xs hidden-sm"><label class="title first-title">Chọn</label></div>
                    <div class="list_toolls col-xs-12 col-sm-3 col-md-2">
                        <input type="hidden" class="FieldName" name="FieldName[]" />
                        <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                        <ul class="dropdownbox">
                            
                            <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                    <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                    <li><a onclick="setfield(this,11,'sodienthoai')">Số điện thoại</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                <ul>
                                    <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                    <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                    <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Sản phẩm</span>
                                <ul>
                                    <li><a onclick="setfield(this,15,'sku')">Mã SKU</a></li>
                                    <li><a onclick="setfield(this,16,'tensanpham')">Tên sản phẩm</a></li>
                                    <li><a onclick="setfield(this,17,'nhomnganhhang')">Nhóm ngành hàng</a></li>
                                    <li><a onclick="setfield(this,18,'thuonghieu')">Thương hiệu</a></li>
                                    <!--<li><a onclick="setfield(this,16,'nhacungcap')">Nhà cung cấp</a></li>-->
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,12,'madh')">Mã đơn hàng</a></li>
                                    <li><a onclick="setfield(this,9,'user')">Nhân viên khởi tạo</a></li>
                                    <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                    <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                    <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                    <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                    <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-4 col-sm-3 col-md-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1">Bằng</option>
                            <option value="0">Có chứa</option>
                            <option value="2">Khác</option>
                            <option value="3">Lớn hơn</option>
                            <option value="4">Nhỏ hơn</option>
                            <option value="5">Lớn hơn hoặc bằng</option>
                            <option value="6">Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-6 col-sm-3 col-md-3">
                        <input type="text" class=" form-control" name="FieldText[]" id="textsearch" />
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
            <?php
                }
            ?>
            <div class="add_box_data"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="col-xs-1 hidden-xs hidden-sm"></div>
                        <div class="col-xs-11">
                            <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                            <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="quick_view">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                <canvas id="canvas_pie"></canvas>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                <?php 
                $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
                ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3"><input type='checkbox' value="0" <?php echo $current_view==0 ? 'checked="checked"' : "" ; ?> onclick="change_viewtype(0)" /> Theo ngày đặt hàng</div>
                    <div class="col-xs-12 col-sm-5 col-md-3"><input type='checkbox' value="1" <?php echo $current_view==1 ? 'checked="checked"' : "" ; ?> onclick="change_viewtype(1)" /> Theo ngày cập nhật trạng thái</div>
                    <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs hidden-sm">
                        <label for="" class="control-label">
                            Chọn kênh bán hàng : 
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 hidden-xs hidden-sm">
                        <?php 
                        $order_type = isset($_GET['order_type']) ? (int)$_GET['order_type'] : 0 ;
                        ?>
                        <select onchange="changetype(this)" style="padding:3px 10px" class="form-control">
                            <option value="0" <?php echo $order_type==0 ? "selected='selected'" : "" ; ?>>Online</option>
                            <option value="1" <?php echo $order_type==1 ? "selected='selected'" : "" ; ?>>MT-GT</option>
                            <option value="2" <?php echo $order_type==2 ? "selected='selected'" : "" ; ?>>Gốm sứ</option>
                        </select>
                    </div>
                    <input type="hidden" id="value_view_type" value="<?php echo $current_view ?>">
                    <input type="hidden" id="value_order_type" value="<?php echo $order_type ?>">
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="title-sm">Tổng hợp theo trạng thái đơn hàng</h3>
                    </div>
                </div>
                
                <?php 
                $arr = array();
                $arr_total = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
                $totalpercent = 0;
                $arr_status = $this->define_model->get_order_status('status','order');
                $array_status = array();
                foreach($arr_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $array_status[$code] = $ite->name;
                }
                $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#D173D6");
                if(count($data)>0){
                    foreach($data as $row){
                        $arr_total['SLDH'] = $arr_total['SLDH']+1;
                        $arr_total['SLSP'] = $arr_total['SLSP']+$row->SoluongSP;
                        $arr_total['Total'] = $arr_total['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                        if(isset($arr[$row->Status])){
                            $arr[$row->Status]['SLDH'] = $arr[$row->Status]['SLDH']+1;
                            $arr[$row->Status]['SLSP'] = $arr[$row->Status]['SLSP']+$row->SoluongSP;
                            $arr[$row->Status]['Total'] = $arr[$row->Status]['Total']+$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                            $totalpercent += $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                        }else{
                            $arr[$row->Status] = array(
                                'SLDH'  =>1,
                                'SLSP'  =>$row->SoluongSP,
                                'Total' =>$row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi
                            );
                            $totalpercent+= $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
                        }
                    }
                }
                $arr_chart = array();
                if(count($arr)>0){
                    echo "<div class='table_data'><table class='table'>
                            <tr>
                                <th>Tình trạng</th>
                                <th>SL đơn hàng</th>
                                <th>SL sản phẩm</th>
                                <th>Tổng doanh số</th>
                                <th>Tỷ lệ %</th>
                            </tr>
                            ";
                    foreach($arr as $key=>$value){
                        $percent = $totalpercent==0 ? 0 : round($value['Total']/($totalpercent/100),1);
                        echo "<tr>";
                        echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$key]."'></span><a href='{$baselink}setfillter_report?Status=$key'>".$array_status[$key]."</a></td>";
                        echo "<td>".number_format($value['SLDH'])."</td>";
                        echo "<td>".number_format($value['SLSP'])."</td>";
                        echo "<td>".number_format($value['Total'])."</td>";
                        echo "<td>".$percent."%</td>";
                        echo "</tr>";
                        $arr_chart[] = "{
                                            value: ".$value['SLDH'].",
                                            color: '".$array_color[$key]."',
                                            highlight: '".$array_color[$key]."',
                                            label: 'Số lượng ".$array_status[$key]."'
                                        }";
                    }
                    echo "<tr>
                            <th>Tổng cộng</th>
                            <th>".number_format($arr_total['SLDH'])."</th>
                            <th>".number_format($arr_total['SLSP'])."</th>
                            <th>".number_format($arr_total['Total'])."</th>
                            <th>100%</th>
                        </tr>";
                    echo "</table></div>";
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                <canvas id="canvas_Doughnut1"></canvas>
                <canvas id="canvas_Doughnut2"></canvas>
                <canvas id="canvas_Doughnut3"></canvas>
                <div class="color_view_chart">
                <?php 
                    echo "<span class='current' style='background:".$array_color[6]."' onclick='showpie(this,1,\"Bán trong kỳ\")'></span>";
                    echo "<span style='background:".$array_color[1]."' onclick='showpie(this,2,\"Hủy trong kỳ\")'></span>";
                    echo "<span style='background:".$array_color[0]."' onclick='showpie(this,3,\"Tổng hợp giao dịch trong kỳ\")'></span>";
                ?>
                </div>
                <div class='current_view_chart'>Biểu đồ : <span>Bán trong kỳ</span></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                <h3 class="title-sm">Chi tiết theo trạng thái đơn hàng</h3>
                <?php 

                $arr_chart1 = array();
                $arr_chart2 = array();
                $arr_chart3 = array();
                if(count($arr)>0){
                    echo "<div class='table_data'><table class='table'>
                            <tr>
                                <th style='width:252px'>Chỉ tiêu</th>
                                <th>SL đơn hàng</th>
                                <th>SL sản phẩm</th>
                                <th>Tổng doanh số</th>
                                <th>Tỷ lệ %</th>
                            </tr>
                            ";
                    $value0 = isset($arr[0]) ? $arr[0] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value1 = isset($arr[1]) ? $arr[1] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value2 = isset($arr[2]) ? $arr[2] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value3 = isset($arr[3]) ? $arr[3] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value4 = isset($arr[4]) ? $arr[4] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value5 = isset($arr[5]) ? $arr[5] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value6 = isset($arr[6]) ? $arr[6] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value7 = isset($arr[7]) ? $arr[7] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value8 = isset($arr[8]) ? $arr[8] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value9 = isset($arr[9]) ? $arr[9] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value10 = isset($arr[10]) ? $arr[10] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value11 = isset($arr[11]) ? $arr[11] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;
                    $value11 = isset($arr[11]) ? $arr[11] : array('SLDH'=>0,'SLSP'=>0,'Total'=>0) ;

                    $arr_chart1[] = "{
                                            value: ".$value9['SLDH'].",
                                            color: '".$array_color[9]."',
                                            highlight: '".$array_color[9]."',
                                            label: 'Số lượng ".$array_status[9]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value7['SLDH'].",
                                            color: '".$array_color[7]."',
                                            highlight: '".$array_color[7]."',
                                            label: 'Số lượng ".$array_status[7]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value0['SLDH'].",
                                            color: '".$array_color[0]."',
                                            highlight: '".$array_color[0]."',
                                            label: 'Số lượng ".$array_status[0]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value2['SLDH'].",
                                            color: '".$array_color[2]."',
                                            highlight: '".$array_color[2]."',
                                            label: 'Số lượng ".$array_status[2]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value3['SLDH'].",
                                            color: '".$array_color[3]."',
                                            highlight: '".$array_color[3]."',
                                            label: 'Số lượng ".$array_status[3]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value4['SLDH'].",
                                            color: '".$array_color[4]."',
                                            highlight: '".$array_color[4]."',
                                            label: 'Số lượng ".$array_status[4]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value5['SLDH'].",
                                            color: '".$array_color[5]."',
                                            highlight: '".$array_color[5]."',
                                            label: 'Số lượng ".$array_status[5]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value6['SLDH'].",
                                            color: '".$array_color[6]."',
                                            highlight: '".$array_color[6]."',
                                            label: 'Số lượng ".$array_status[6]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value8['SLDH'].",
                                            color: '".$array_color[8]."',
                                            highlight: '".$array_color[8]."',
                                            label: 'Số lượng ".$array_status[8]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value11['SLDH'].",
                                            color: '".$array_color[10]."',
                                            highlight: '".$array_color[10]."',
                                            label: 'Số lượng ".$array_status[11]."'
                                        }";
                    $arr_chart1[] = "{
                                            value: ".$value11['SLDH'].",
                                            color: '".$array_color[10]."',
                                            highlight: '".$array_color[10]."',
                                            label: 'Số lượng ".$array_status[11]."'
                                        }";

                    $total_percent_1 = $value9['Total']+$value7['Total']+$value0['Total']+$value2['Total']+$value3['Total']+$value4['Total']+$value5['Total']+$value6['Total']+$value8['Total']+$value10['Total']+$value11['Total'];
                    
                    echo "<tr><td colspan='5'><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[6]."'></span> Bán trong kỳ</td></tr>";
                    $percent = $total_percent_1==0 ? 0 : round($value2['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=2'>".$array_status[2]."</a></td>";
                    echo "<td>".number_format($value2['SLDH'])."</td>";
                    echo "<td>".number_format($value2['SLSP'])."</td>";
                    echo "<td>".number_format($value2['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $total_percent_1==0 ? 0 : round($value3['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=3'>".$array_status[3]."</a></td>";
                    echo "<td>".number_format($value3['SLDH'])."</td>";
                    echo "<td>".number_format($value3['SLSP'])."</td>";
                    echo "<td>".number_format($value3['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $total_percent_1==0 ? 0 : round($value4['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=4'>".$array_status[4]."</a></td>";
                    echo "<td>".number_format($value4['SLDH'])."</td>";
                    echo "<td>".number_format($value4['SLSP'])."</td>";
                    echo "<td>".number_format($value4['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $total_percent_1==0 ? 0 : round($value5['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=5'>".$array_status[5]."</a></td>";
                    echo "<td>".number_format($value5['SLDH'])."</td>";
                    echo "<td>".number_format($value5['SLSP'])."</td>";
                    echo "<td>".number_format($value5['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $total_percent_1==0 ? 0 : round($value6['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=6'>".$array_status[6]."</a></td>";
                    echo "<td>".number_format($value6['SLDH'])."</td>";
                    echo "<td>".number_format($value6['SLSP'])."</td>";
                    echo "<td>".number_format($value6['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $total_percent_1==0 ? 0 : round($value7['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=7'>".$array_status[7]."</a></td>";
                    echo "<td>".number_format($value7['SLDH'])."</td>";
                    echo "<td>".number_format($value7['SLSP'])."</td>";
                    echo "<td>".number_format($value7['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = ($total_percent_1)==0 ? 0 : round($value8['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=8'>".$array_status[8]."</a></td>";
                    echo "<td>".number_format($value8['SLDH'])."</td>";
                    echo "<td>".number_format($value8['SLSP'])."</td>";
                    echo "<td>".number_format($value8['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";
                    
                    $percent = ($total_percent_1)==0 ? 0 : round($value9['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=9'>".$array_status[9]."</a></td>";
                    echo "<td>".number_format($value9['SLDH'])."</td>";
                    echo "<td>".number_format($value9['SLSP'])."</td>";
                    echo "<td>".number_format($value9['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";
                    
                    $percent = $total_percent_1==0 ? 0 : round($value0['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=0'>".$array_status[0]."</a></td>";
                    echo "<td>".number_format($value0['SLDH'])."</td>";
                    echo "<td>".number_format($value0['SLSP'])."</td>";
                    echo "<td>".number_format($value0['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $total_percent_1==0 ? 0 : round($value11['Total']/($total_percent_1/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=11'>".$array_status[11]."</a></td>";
                    echo "<td>".number_format($value11['SLDH'])."</td>";
                    echo "<td>".number_format($value11['SLSP'])."</td>";
                    echo "<td>".number_format($value11['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $total_percent_sldh = $value9['SLDH']+$value7['SLDH']+$value0['SLDH']+$value2['SLDH']+$value3['SLDH']+$value4['SLDH']+$value5['SLDH']+$value6['SLDH']+$value8['SLDH']+$value10['SLDH'];
                    $total_percent_slsp = $value9['SLSP']+$value7['SLSP']+$value0['SLSP']+$value2['SLSP']+$value3['SLSP']+$value4['SLSP']+$value5['SLSP']+$value6['SLSP']+$value8['SLSP']+$value10['SLSP'];
                    echo "<tr>
                            <th><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span>Tổng cộng</th>
                            <th>".number_format($total_percent_sldh)."</th>
                            <th>".number_format($total_percent_slsp)."</th>
                            <th>".number_format($total_percent_1)."</th>
                            <th>100%</th>
                        </tr>";
                    echo "<tr><td colspan='5'><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[1]."'></span> Hủy trong kỳ</td></tr>";
                    $total_reason = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
                    $i=0;
                    if(count($reason)>0){
                        foreach($reason as $row){
                            $percent = $value1['Total']==0 ? 0 : round($row->Total/($value1['Total']/100),1);
                            echo "<tr>";
                            echo "<td style='padding-left:30px'>".$row->Title."</td>";
                            echo "<td>".number_format($row->SLDH)."</td>";
                            echo "<td>".number_format($row->SoluongSP)."</td>";
                            echo "<td>".number_format($row->Total)."</td>";
                            echo "<td>$percent%</td>";
                            echo "</tr>";
                            $total_reason['SLDH'] = $total_reason['SLDH']+$row->SLDH;
                            $total_reason['SLSP'] = $total_reason['SLSP']+$row->SoluongSP;
                            $total_reason['Total'] = $total_reason['Total']+$row->Total;
                            $arr_chart2[] = "{
                                            value: ".$total_reason['SLDH'].",
                                            color: '".$array_color[$i]."',
                                            highlight: '".$array_color[$i]."',
                                            label: 'Số lượng $row->Title'
                                        }";
                            $i++;
                        }
                    }
                    $temp_arr_chart2 = $value1['SLDH']-$total_reason['SLDH'];
                    $arr_chart2[] = "{
                                            value: ".$temp_arr_chart2.",
                                            color: '".$array_color[$i]."',
                                            highlight: '".$array_color[$i]."',
                                            label: 'Số lượng không xác định'
                                        }";
                    $percent =  ($value1['Total']-$total_reason['Total'])==0 ? 0 : round(($value1['Total']-$total_reason['Total'])/($value1['Total']/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span> Không xác định</td>";
                    echo "<td>".number_format($value1['SLDH']-$total_reason['SLDH'])."</td>";
                    echo "<td>".number_format($value1['SLSP']-$total_reason['SLSP'])."</td>";
                    echo "<td>".number_format($value1['Total']-$total_reason['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    echo "<tr>
                            <th><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span>Tổng cộng</th>
                            <th>".number_format($value1['SLDH'])."</th>
                            <th>".number_format($value1['SLSP'])."</th>
                            <th>".number_format($value1['Total'])."</th>
                            <th>100%</th>
                        </tr>";

                    echo "<tr><td colspan='5'><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[0]."'></span> Tổng hợp giao dịch trong kỳ</td></tr>";
                    
                    $arr_chart3[] = "{
                                            value: ".$value0['SLDH'].",
                                            color: '".$array_color[7]."',
                                            highlight: '".$array_color[7]."',
                                            label: 'Số lượng ".$array_status[0]."'
                                        }";
                    $arr_chart3[] = "{
                                            value: ".$value1['SLDH'].",
                                            color: '".$array_color[1]."',
                                            highlight: '".$array_color[1]."',
                                            label: 'Số lượng ".$array_status[1]."'
                                        }";
                    $arr_chart3_temp = $arr_total['SLDH']-$value0['SLDH']-$value1['SLDH'];
                    $arr_chart3[] = "{
                                            value: ".$arr_chart3_temp.",
                                            color: '".$array_color[0]."',
                                            highlight: '".$array_color[0]."',
                                            label: 'Số lượng đơn hàng đang xử lý'
                                        }";

                    $percent = $arr_total['Total']==0 ? 0 : round($value0['Total']/($arr_total['Total']/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=0'>".$array_status[0]."</a></td>";
                    echo "<td>".number_format($value0['SLDH'])."</td>";
                    echo "<td>".number_format($value0['SLSP'])."</td>";
                    echo "<td>".number_format($value0['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";
                    
                    $percent = ($arr_total['Total']-$value0['Total']-$value1['Total'])==0 ? 0 : round(($arr_total['Total']-$value0['Total']-$value1['Total'])/($arr_total['Total']/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span> <a href='{$baselink}setfillter_report?Status=processing'>Đơn hàng đang xử lý</a></td>";
                    echo "<td>".number_format($arr_total['SLDH']-$value0['SLDH']-$value1['SLDH'])."</td>";
                    echo "<td>".number_format($arr_total['SLSP']-$value0['SLSP']-$value1['SLSP'])."</td>";
                    echo "<td>".number_format($arr_total['Total']-$value0['Total']-$value1['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    $percent = $arr_total['Total']==0 ? 0 : round($value1['Total']/($arr_total['Total']/100),1);
                    echo "<tr>";
                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span><a href='{$baselink}setfillter_report?Status=1'>".$array_status[1]."</a></td>";
                    echo "<td>".number_format($value1['SLDH'])."</td>";
                    echo "<td>".number_format($value1['SLSP'])."</td>";
                    echo "<td>".number_format($value1['Total'])."</td>";
                    echo "<td>$percent%</td>";
                    echo "</tr>";

                    echo "<tr>
                            <th><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:#FFF'></span>Tổng cộng</th>
                            <th>".number_format($arr_total['SLDH'])."</th>
                            <th>".number_format($arr_total['SLSP'])."</th>
                            <th>".number_format($arr_total['Total'])."</th>
                            <th>100%</th>
                        </tr>";
                    echo "</table></div>";
                }
                ?>
            </div>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var sharePiePolorDoughnutData = [
        <?php 
        echo implode(',', $arr_chart);
        ?>
    ];

    var sharePiePolorDoughnutData1 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];

    var sharePiePolorDoughnutData2 = [
        <?php 
        echo implode(',', $arr_chart2);
        ?>
    ];

    var sharePiePolorDoughnutData3 = [
        <?php 
        echo implode(',', $arr_chart3);
        ?>
    ];


    $(document).ready(function () {
        window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_Doughnut1").getContext("2d")).Doughnut(sharePiePolorDoughnutData1, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_Doughnut2").getContext("2d")).Doughnut(sharePiePolorDoughnutData2, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_Doughnut3").getContext("2d")).Doughnut(sharePiePolorDoughnutData3, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });

    function showpie(ob,num,title){
        $(".color_view_chart span").removeClass("current");
        $(ob).addClass("current");
        $("#canvas_Doughnut3").hide();
        $("#canvas_Doughnut2").hide();
        $("#canvas_Doughnut1").hide();
        $("#canvas_Doughnut"+num).show();
        $(".current_view_chart span").html(title);
    }

    function change_viewtype(type){
        var value_order_type = $("#value_order_type").val();
        window.location="<?php echo current_url().'?view_type=' ?>"+type+"&order_type="+value_order_type;
    }

    function changetype(ob){
        var value_view_type = $("#value_view_type").val();
        var typeorder = $(ob).val();
        window.location="<?php echo current_url().'?order_type=' ?>"+typeorder+"&view_type="+value_view_type;
    }

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"import_order/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="vung" || fieldname=="tinhthanh" || fieldname=="quanhuyen" || fieldname=='loaikhachhang' || fieldname=='trangthaidonhang' || fieldname=='doitacvanchuyen' || fieldname=='user' || fieldname=='nhomnganhhang' || fieldname=='thuonghieu'){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="tenkhachhang" || fieldname=="sodienthoai" || fieldname=="madh"){
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="0">Có chứa</option><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    function export_data(ob){
        window.location = "<?php echo base_url().ADMINPATH.'/report/report_sales/export_report_order/?'.http_build_query($_GET) ?>";
    }
</script>
<style>
    .version{display:none;}
    .copyright{display:none;}
</style>