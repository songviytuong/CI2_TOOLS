<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$numday = $this->lib->get_nume_day($startday,$stopday);
?>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<h1 style="font-size: 20px;font-weight: bold;">BÁO CÁO DOANH THU CHI TIẾT</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <table class="table table-bordered table-hover">
        <tr>
            <th>STT</th>
            <th>Ngày đặt hàng</th>
            <th>Ngày giao dịch thành công</th>
            <th>Tên khách hàng</th>
            <th>Là khách hàng mới</th>
            <th>Lần mua</th>
            <th>Giá trị đơn hàng</th>
            <th>Hoa hồng của bạn</th>
        </tr>
        <?php 
        $arr_customers = array();
        if(count($oldcustomer)>0){
            foreach($oldcustomer as $row){
                $arr_customers[$row->CustomerID] = $row->Total;
            }
        }

        if(count($data)>0){
            $i=1;
            $total = 0;
            $totalprice = 0;
            foreach($data as $row){
                $totalbuy = 1;
                if(isset($arr_customers[$row->CustomerID])){
                    $arr_customers[$row->CustomerID] = $arr_customers[$row->CustomerID]+1;
                    $totalbuy = $arr_customers[$row->CustomerID];
                }else{
                    $arr_customers[$row->CustomerID] = 1;
                }
                $isnew = $totalbuy==1 ? "yes" : "no" ;
                $datesuccess = $row->Status==0 ? date('d/m/Y',strtotime($row->DateSuccess)) : '--' ;
                $row->AffiliateCost = $row->Status==0 ? $row->AffiliateCost : '--' ;
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>";
                echo "<td>".$datesuccess."</td>";
                echo "<td>$row->Name</td>";
                echo "<td>$isnew</td>";
                echo "<td class='text-right'>".number_format($totalbuy)."</td>";
                echo "<td class='text-right'>".number_format($row->Total-$row->Chietkhau-$row->Reduce)."</td>";
                if($row->AffiliateCost!='--'){
                    echo "<td class='text-right'>".number_format($row->AffiliateCost)."</td>";
                }else{
                    echo "<td class='text-right'>--</td>";
                }
                echo "</tr>";
                $i++;
                if($row->AffiliateCost!='--'){
                    $total = $total + $row->AffiliateCost;
                }
                $totalprice = $totalprice+($row->Total-$row->Chietkhau-$row->Reduce);
            }
            echo "<tr><th colspan='6' class='text-right'>TỔNG CỘNG</th><th class='text-right'>".number_format($totalprice)."</th><th class='text-right'>".number_format($total)."</th></tr>";
        }else{
            echo "<tr><td colspan='8'>Không tìm thấy dữ liệu. </td></tr>";
        }
        ?>
    </table>
    <div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<style>
	.col-xs-4{border-right:1px solid #eee;margin-bottom:20px;min-height: 100px;}
	.col-xs-4 h5{text-transform: uppercase;}
</style>
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                location.reload();
	            }
	        });	
		});
    });
</script>
<!-- /datepicker -->