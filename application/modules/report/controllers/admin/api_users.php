<?php 
class Api_users extends Admin_Controller { 
 
 	public $user;
 	public $classname="api_users";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/api_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 30;

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_system_user a,ttp_report_customer b,ttp_system_website c where c.ID=a.WebsiteID and a.CustomerID=b.ID")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select a.*,b.Name,c.Domain from ttp_system_user a,ttp_report_customer b,ttp_system_website c where c.ID=a.WebsiteID and a.CustomerID=b.ID $limit_str")->result();
        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/api_users/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/api_users/index',5,$nav,$this->limit)
        );
        $this->template->add_title('User system | Report Tools');
        $this->template->write_view('content','admin/api_users_home',$data);
        $this->template->render();
    }
    
    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Add user system | Report Tools');
        $this->template->write_view('content','admin/api_users_add',array('base_link'=>base_url().ADMINPATH.'/report/api_users/'));
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Edit user system | Report Tools');
        $result = $this->db->query("select b.*,a.UserName,a.Password,a.Published,a.WebsiteID,a.ID as UserID from ttp_system_user a,ttp_report_customer b where a.ID=$id and a.CustomerID=b.ID")->row();
        if($result){
            $this->template->write_view('content','admin/api_users_edit',
                array(
                    'base_link' =>base_url().ADMINPATH.'/report/api_users/',
                    'data'      =>$result
                )
            );
            $this->template->render();
        }
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_system_user where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Published = isset($_POST['Published']) ? (int)$_POST['Published'] : 0 ;
        $WebsiteID = isset($_POST['WebsiteID']) ? (int)$_POST['WebsiteID'] : 0 ;
        $Type = isset($_POST['Type']) ? (int)$_POST['Type'] : 0 ;
        $UserName = isset($_POST['UserName']) ? trim(mysql_real_escape_string($_POST['UserName'])) : '' ;
        $Password = isset($_POST['Password']) ? trim(mysql_real_escape_string($_POST['Password'])) : '' ;
        $RePassword = isset($_POST['RePassword']) ? trim(mysql_real_escape_string($_POST['RePassword'])) : '' ;
        if($Password!=$RePassword){
            redirect(base_url().ADMINPATH.'/report/api_users/add?error=0');
            return;
        }
        $Name = isset($_POST['Name']) ? trim(mysql_real_escape_string($_POST['Name'])) : '' ;
        $Phone1 = isset($_POST['Phone1']) ? trim(mysql_real_escape_string($_POST['Phone1'])) : '' ;
        $Phone2 = isset($_POST['Phone1']) ? trim(mysql_real_escape_string($_POST['Phone2'])) : '' ;
        $Email = isset($_POST['Email']) ? trim(mysql_real_escape_string($_POST['Email'])) : '' ;
        $Birthday = isset($_POST['Birthday']) ? trim(mysql_real_escape_string($_POST['Birthday'])) : '' ;
        $Age = isset($_POST['Age']) ? trim(mysql_real_escape_string($_POST['Age'])) : '' ;
        $Sex = isset($_POST['Sex']) ? trim(mysql_real_escape_string($_POST['Sex'])) : '' ;
        $Job = isset($_POST['Job']) ? trim(mysql_real_escape_string($_POST['Job'])) : '' ;
        $Company = isset($_POST['Company']) ? trim(mysql_real_escape_string($_POST['Company'])) : '' ;
        $AreaID = isset($_POST['AreaID']) ? trim(mysql_real_escape_string($_POST['AreaID'])) : '' ;
        $CityID = isset($_POST['CityID']) ? trim(mysql_real_escape_string($_POST['CityID'])) : '' ;
        $DistrictID = isset($_POST['DistrictID']) ? trim(mysql_real_escape_string($_POST['DistrictID'])) : '' ;
        $Address = isset($_POST['Address']) ? trim(mysql_real_escape_string($_POST['Address'])) : '' ;
        $AddressOrder = isset($_POST['AddressOrder']) ? trim(mysql_real_escape_string($_POST['AddressOrder'])) : '' ;
        if($UserName!='' && $Password!='' && $RePassword!='' && $Name!='' && $Phone1!=''){
            $checkphone = $this->db->query("select ID from ttp_report_customer where Phone1='$Phone1'")->row();
            if($checkphone){
                redirect(base_url().ADMINPATH.'/report/api_users/add?error=1');
                return;
            }
            $checkuser = $this->db->query("select ID from ttp_system_user where UserName='$UserName'")->row();
            if($checkuser){
                redirect(base_url().ADMINPATH.'/report/api_users/add?error=2');
                return;
            }
            $data = array(
                'Name'          =>$Name,
                'Address'       =>$Address,
                'AddressOrder'  =>$AddressOrder,
                'Phone1'        =>$Phone1,
                'Phone2'        =>$Phone2,
                'Email'         =>$Email,
                'Birthday'      =>$Birthday,
                'Age'           =>$Age,
                'Sex'           =>$Sex,
                'Job'           =>$Job,
                'Company'       =>$Company,
                'AreaID'        =>$AreaID,
                'CityID'        =>$CityID,
                'DistrictID'    =>$DistrictID,
                'Type'          =>$Type,
                'CreatedCustomer'=>date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_report_customer',$data);
            $id = $this->db->insert_id();
            $data = array(
                'UserName'  => $UserName,
                'UserEncrypt'  => sha1($UserName),
                'Password'  => sha1($Password),
                'WebsiteID' => $WebsiteID,
                'Published' => $Published,
                'CustomerID'=>$id,
                'Created'   =>date('Y-m-d H:i:s'),
                'LastEdited'=>date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_system_user',$data);
        }
        redirect(base_url().ADMINPATH.'/report/api_users/');
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? trim(mysql_real_escape_string($_POST['ID'])) : 0 ;
        $UserID = isset($_POST['UserID']) ? trim(mysql_real_escape_string($_POST['UserID'])) : 0 ;
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Published = isset($_POST['Published']) ? (int)$_POST['Published'] : 0 ;
        $WebsiteID = isset($_POST['WebsiteID']) ? (int)$_POST['WebsiteID'] : 0 ;
        $Type = isset($_POST['Type']) ? (int)$_POST['Type'] : 0 ;
        $UserName = isset($_POST['UserName']) ? trim(mysql_real_escape_string($_POST['UserName'])) : '' ;
        $Password = isset($_POST['Password']) ? trim(mysql_real_escape_string($_POST['Password'])) : '' ;
        $RePassword = isset($_POST['RePassword']) ? trim(mysql_real_escape_string($_POST['RePassword'])) : '' ;
        if($Password!=$RePassword){
            redirect(base_url().ADMINPATH.'/report/api_users/edit/'.$UserID.'?error=0');
            return;
        }
        $Name = isset($_POST['Name']) ? trim(mysql_real_escape_string($_POST['Name'])) : '' ;
        $Phone1 = isset($_POST['Phone1']) ? trim(mysql_real_escape_string($_POST['Phone1'])) : '' ;
        $Phone2 = isset($_POST['Phone1']) ? trim(mysql_real_escape_string($_POST['Phone2'])) : '' ;
        $Email = isset($_POST['Email']) ? trim(mysql_real_escape_string($_POST['Email'])) : '' ;
        $Birthday = isset($_POST['Birthday']) ? trim(mysql_real_escape_string($_POST['Birthday'])) : '' ;
        $Age = isset($_POST['Age']) ? trim(mysql_real_escape_string($_POST['Age'])) : '' ;
        $Sex = isset($_POST['Sex']) ? trim(mysql_real_escape_string($_POST['Sex'])) : '' ;
        $Job = isset($_POST['Job']) ? trim(mysql_real_escape_string($_POST['Job'])) : '' ;
        $Company = isset($_POST['Company']) ? trim(mysql_real_escape_string($_POST['Company'])) : '' ;
        $AreaID = isset($_POST['AreaID']) ? trim(mysql_real_escape_string($_POST['AreaID'])) : '' ;
        $CityID = isset($_POST['CityID']) ? trim(mysql_real_escape_string($_POST['CityID'])) : '' ;
        $DistrictID = isset($_POST['DistrictID']) ? trim(mysql_real_escape_string($_POST['DistrictID'])) : '' ;
        $Address = isset($_POST['Address']) ? trim(mysql_real_escape_string($_POST['Address'])) : '' ;
        $AddressOrder = isset($_POST['AddressOrder']) ? trim(mysql_real_escape_string($_POST['AddressOrder'])) : '' ;
        if($UserName!='' && $Name!='' && $Phone1!=''){
            $checkphone = $this->db->query("select ID from ttp_report_customer where Phone1='$Phone1' and ID!=$ID")->row();
            if($checkphone){
                redirect(base_url().ADMINPATH.'/report/api_users/edit/'.$UserID.'?error=1');
                return;
            }
            $checkuser = $this->db->query("select ID from ttp_system_user where UserName='$UserName' and ID!=$UserID")->row();
            if($checkuser){
                redirect(base_url().ADMINPATH.'/report/api_users/edit/'.$UserID.'?error=2');
                return;
            }
            $data = array(
                'Name'          =>$Name,
                'Address'       =>$Address,
                'AddressOrder'  =>$AddressOrder,
                'Phone1'        =>$Phone1,
                'Phone2'        =>$Phone2,
                'Email'         =>$Email,
                'Birthday'      =>$Birthday,
                'Age'           =>$Age,
                'Sex'           =>$Sex,
                'Job'           =>$Job,
                'Company'       =>$Company,
                'AreaID'        =>$AreaID,
                'CityID'        =>$CityID,
                'DistrictID'    =>$DistrictID,
                'Type'          =>$Type
            );
            $this->db->where("ID",$ID);
            $this->db->update('ttp_report_customer',$data);
            $data = array(
                'UserName'  => $UserName,
                'UserEncrypt'  => sha1($UserName),
                'WebsiteID' => $WebsiteID,
                'Published' => $Published,
                'LastEdited'=>date('Y-m-d H:i:s')
            );
            if($Password!='' && $RePassword!=''){
                $data['Password']  = sha1($Password);
            }
            $this->db->where("ID",$UserID);
            $this->db->update('ttp_system_user',$data);
        }
        redirect(base_url().ADMINPATH.'/report/api_users/');
    }
}
?>