<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
			<h3>Thông tin đơn vị vận chuyển <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Save</button></h3>
			<hr>
			<div class="row">
				<label for="" class="col-xs-2 control-label">Tên đơn vị vận chuyển</label>
				<div class="col-xs-4">
					<input type="text" class="form-control" name="Title" value="<?php echo $data->Title ?>" required />
				</div>
				<label for="" class="col-xs-2 control-label text-right">Mã viết tắt </label>
				<div class="col-xs-4">
					<input type="text" class="form-control" name="Code" value="<?php echo $data->Code ?>" required />
				</div>
			</div>
			<hr>
			<h3>Danh sách nhân viên giao nhận</h3>
			<hr>
			<div class="row">
				<?php 
				$userlist = $this->db->query("select * from ttp_user_transport where TransportID=$data->ID")->result();
				$arr_user = array();
				if(count($userlist)>0){
					foreach($userlist as $row){
						$arr_user[] = $row->UserID;
					}
				}
				$result = $this->db->query("select * from ttp_user where UserType=15")->result();
				if(count($result)>0){
					foreach($result as $row){
						$checked = in_array($row->ID,$arr_user) ? "checked='checked'" : '' ;
						echo "<div class='col-xs-2'><input type='checkbox' name='user[]' value='$row->ID' $checked /> $row->UserName</div>";
					}
				}
				?>
			</div>
		</form>
	</div>
</div>
<style>
	h3{margin:0px;}
	.body_content .containner{min-height: 569px !important;}
</style>