<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('isOpenSideBar')) {

    function isOpenSideBar($groupid) {
        $CI = & get_instance();
        $CI->load->model('users_model', 'users');
        $arr = $CI->users->getUsersByGroupID($groupid);
        foreach ($arr as $key => $item) {
            $ids[] = $item->ID;
        }
        return $ids;
    }

}

if (!function_exists('isCollaborators')) {

    function isCollaborators() {
        $CI = & get_instance();
        $CI->load->model('users_model', 'users');
        $arr = $CI->users->getUsersByGroupID(2);
        foreach ($arr as $key => $item) {
            $ids[] = $item->ID;
        }
        return $ids;
    }

}

if (!function_exists('isWriterManager')) {

    function isWriterManager() {
        $CI = & get_instance();
        $CI->load->model('users_model', 'users');
        $arr = $CI->users->getUsersByGroupID(3);
        foreach ($arr as $key => $item) {
            $ids[] = $item->ID;
        }
        return $ids;
    }

}

if (!function_exists('isSupperAdmin')) {

    function isSupperAdmin() {
        $CI = & get_instance();
        $CI->load->model('users_model', 'users');
        $arr = $CI->users->getUsersByGroupID(2);
        foreach ($arr as $key => $item) {
            $ids[] = $item->ID;
        }
        return $ids;
    }

}


