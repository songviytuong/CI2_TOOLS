<?php
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$month          = array();
$dayofmonth     = array();
$dayofmonth_real= array();
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>DANH SÁCH PHIẾU XUẤT KHO BÁN HÀNG ĐÃ XUẤT</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <form action="" method="get">
        <div class="row">
            <div class="col-xs-3">
                <input class="form-control" type="text" name="search_key" placeholder="Tìm phiếu xuất hoặc mã vận đơn ..." value="<?php echo $search ?>" style="padding:5px 10px">
            </div>
            <div class="col-xs-3">
                <div class="btn-group">
                    <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Tình trạng nhận tiền</a>
                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo $base_link."export_warehouse/?Payment=-1" ?>"><i class="fa fa-check-square" aria-hidden="true" style="margin-right:5px;<?php echo $payment==-1 ? "color:#090" : "color:#ccc" ; ?>"></i> Tất cả</a></li>
                        <li><a href="<?php echo $base_link."export_warehouse/?Payment=1" ?>"><i class="fa fa-check-square" aria-hidden="true" style="margin-right:5px;<?php echo $payment==1 ? "color:#090" : "color:#ccc" ; ?>"></i> Đã nhận tiền</a></li>
                        <li><a href="<?php echo $base_link."export_warehouse/?Payment=0" ?>"><i class="fa fa-check-square" aria-hidden="true" style="margin-right:5px;<?php echo $payment==0 ? "color:#090" : "color:#ccc" ; ?>"></i> Chưa nhận tiền</a></li>
                    </ul>
        </div>
            </div>
            <div class="col-xs-6">
                <?php
                if($this->user->IsAdmin==1 || $this->user->UserType==3){
                ?>
                <a onclick="showimport()" class="btn btn-danger pull-right hidden"> <i class="fa fa-upload"></i> Nhập đối soát</a>
                <a href="<?php echo base_url().'assets/template/template_update_pxk.xlsx' ?>" class="btn btn-default pull-right hidden" style="margin-right:10px"><i class="fa fa-download"></i> File mẫu</a>
                <a href="<?php echo base_url().ADMINPATH.'/report/import_order/excel_export_warehouse?Payment='.$payment ?>" class="btn btn-primary pull-right"> <i class="fa fa-download"></i> Export excel</a>
                <?php
                }
                ?>
            </div>
        </div>
    </form>
    <div class="import_orderlist">
        <div class="clear"></div>
        <div class="block3 table_data">
            <table id="table_data">
                <tr>
                    <th>STT</th>
                    <th>Ngày</th>
                    <th>Mã XK</th>
                    <th>Mã kho</th>
                    <th>User</th>
                    <th>Trạng thái DH</th>
                    <th>Tình trạng nhận tiền</th>
                    <th>Xem phiếu</th>
                </tr>
                <?php
                $arr_status = $this->define_model->get_order_status('status','order');
                $array_status = array();
                foreach($arr_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $array_status[$code] = $ite->name;
                }

                $arr_payment = $this->define_model->get_order_status('payment','payment_status');
                $array_payment = array();
                foreach($arr_payment as $key=>$ite){
                    $code = (int)$ite->code;
                    $array_payment[$code] = $ite->name;
                }

                $arr_money = array(
                    1=>'<i class="fa fa-circle text-success" aria-hidden="true"></i> Đã thu',
                    0=>'<i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa thu'
                );

                if(count($data)>0){
                    $i = $start+1;
                    foreach($data as $row){
                        $color = $row->TransferMoney==0 ? "class='text-danger'" : "class='text-success'";
                        echo "<tr>";
                        echo "<td>$i</td>";
                        echo "<td>".date('d/m/Y',strtotime($row->Ngayxuatkho))."</td>";
                        $ref = $row->TransportRef !='' ? "<br><span class='label label-primary'>".$row->TransportRef."</span>" : '' ;
                        echo "<td>".$row->MaXK."$ref</td>";
                        echo "<td>$row->MaKho</td>";
                        echo "<td>".$row->UserName."</td>";
                        echo "<td>".$array_status[$row->Status]."</td>";
                        echo isset($arr_money[$row->FinanceMoney]) ? "<td><a class='status-button'>".$arr_money[$row->FinanceMoney]."</a></td>" : "<td>--</td>";
                        $pxk = $this->user->UserType==2 || $this->user->IsAdmin==1 ? "<a style='color:#27c;text-decoration:underline' href='".base_url().ADMINPATH."/report/import_order/edit/$row->OrderID'>Đơn hàng</a> <br><a style='color:#27c;text-decoration:underline' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$row->OrderID'>Phiếu xuất kho</a>" : "<a style='color:#27c;text-decoration:underline' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$row->OrderID'>Phiếu xuất kho</a>" ;
                        echo "<td>$pxk</td>";
                        echo "</tr>";
                        $i++;
                    }
                }else{
                    echo "<tr><td colspan='10'>Không tìm thấy đơn hàng.</td></tr>";
                }
                ?>
            </table>
            <?php
                echo $nav;
            ?>
        </div>
    </div>
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/import_order/" ?>" />
</div>

<?php
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<script>
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: "<?php echo base_url().ADMINPATH ?>/report/import/set_day_warehouse",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            });
        });
    });

    var baselink = $("#baselink_report").val();

    function showimport(){
        $(".over_lay .box_inner").css({'margin-top':'10px'});
        $(".over_lay .box_inner .block1_inner h1").html("Cập nhật đơn hàng đã nhận tiền từ đối tác");
        $(".over_lay .box_inner .block2_inner").load(baselink+"load_data_from_file");
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    $("#close_overlay").click(function(){
        $(".over_lay").hide();
    });

    function activeimport(){
        var Fileinput = document.getElementById("file_import");
        var file = Fileinput.files[0];
        var fd = new FormData();
        fd.append('file', file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', baselink+'upload_import', true);
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                $(".over_lay .box_inner .block2_inner").css({'overflow-x':'hidden'});
                $(".over_lay .box_inner .block2_inner #form-file p:first-child").html('<div class="loader">Loading...</div>');
                $(".over_lay .box_inner .block2_inner #form-file h4").html("Đang tải file lên hệ thống ...");
            }
        };
        xhr.onload = function() {
            if (this.status == 200) {
                $(".over_lay .box_inner .block2_inner").html(xhr.responseText);
            }else{
                console.log(xhr.responseText);
            }
        };
        xhr.send(fd);
    }

    function rmtr(ob){
        $(ob).parent('td').parent('tr').remove();
    }

    function active_import(ob){
        $(ob).addClass("saving");
        $(ob).html('Loading...');
        var arr = new Array();
        $(".over_lay .box_inner .block2_inner table tr.treach").each(function(){
            var pxk = $(this).find('.td0').html();
            var ngaynhantien = $(this).find('.td1').html();
            if(pxk!=''){
                var jsonArg1 = new Object();
                jsonArg1.pxk = pxk;
                jsonArg1.ngaynhantien = ngaynhantien;
                arr.push(jsonArg1);
            }
        });
        var billcode = $("#Billcode").val();
        $.ajax({
            dataType: "json",
            data: "bill="+billcode+"&data="+JSON.stringify(arr),
            url: baselink+'active_upload_import',
            cache: false,
            method: 'POST',
            success: function(result) {
                if(result.Error==null){
                    download_error();
                }else{
                    $(".over_lay .box_inner .block2_inner").load(baselink+"load_data_from_file");
                    alert(result.Error);
                }
            },
            error:function(result){
                if(result.status==500){
                    alert("Dữ liệu QRCode bị trùng ! Vui lòng kiểm tra lại file!");
                }
                console.log(result.responseText);
                $(ob).removeClass("saving");
                $(ob).html("<i class='fa fa-upload'></i> Nhập vào hệ thống");
                $(".over_lay .box_inner .block2_inner").load(baselink+"load_data_from_file");
            }
        });
    }


    function download_error() {
        $(".over_lay").hide();
        var arr = new Array();
        $(".over_lay .box_inner .block2_inner table tr.trfalse").each(function(){
            var pxk = $(this).find('.td0').html();
            var ngaynhantien = $(this).find('.td1').html();
            if(pxk!=''){
                var jsonArg1 = new Object();
                jsonArg1.pxk = pxk;
                jsonArg1.ngaynhantien = ngaynhantien;
                arr.push(jsonArg1);
            }
        });

        var form = document.createElement("form");
        var element1 = document.createElement("input");
        form.method = "POST";
        form.action = baselink+'download_error';
        element1.value=JSON.stringify(arr);
        element1.name="Data";
        form.appendChild(element1);
        document.body.appendChild(form);

        form.submit();
    }

</script>
