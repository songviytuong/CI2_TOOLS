<?php

class System_user extends Admin_Controller {

    public $user;
    public $classname = "system_user";
    public $limit = 40;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('User list | Config System');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_user a,ttp_role b where a.RoleID=b.ID")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Title from ttp_user a,ttp_role b where a.RoleID=b.ID order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/system_user/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/system_user/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/system_user_home', $data);
        $this->template->render();
    }

    public function search($link = 'search') {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $limit_str = "limit $start,$this->limit";
        $keyword = $this->session->userdata("user_filter_Keywords");
        $RoleID = $this->session->userdata("user_filter_RoleID");
        $Published = $this->session->userdata("user_filter_Published");
        $str_nav = "select count(1) as nav from ttp_user a,ttp_role b where a.RoleID=b.ID";
        $str = "select a.*,b.Title from ttp_user a,ttp_role b where a.RoleID=b.ID";
        $str = $keyword != '' ? $str . " and (a.UserName like '%" . urldecode($keyword) . "%' or b.Title like '%" . urldecode($keyword) . "%')" : $str;
        $str_nav = $keyword != '' ? $str_nav . " and (a.UserName like '%" . urldecode($keyword) . "%' or b.Title like '%" . urldecode($keyword) . "%')" : $str_nav;
        if ($RoleID > 0) {
            $str.=" and a.RoleID=$RoleID";
            $str_nav.=" and a.RoleID=$RoleID";
        }
        if ($Published != '') {
            $str.=" and a.Published=$Published";
            $str_nav.=" and a.Published=$Published";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data = array(
            'data' => $this->db->query($str . " order by ID DESC $limit_str")->result(),
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/system_user/' . $link, 5, $nav, $this->limit),
            'start' => $start,
            'base_link' => base_url() . ADMINPATH . '/home/system_user/',
        );
        $this->template->write_view('content', 'admin/system_user_home', $data);
        $this->template->render();
    }

    public function setsessionsearch() {
        if (isset($_POST['Keywords'])) {
            $Keywords = mysql_real_escape_string($_POST['Keywords']);
            $this->session->set_userdata("user_filter_Keywords", $Keywords);
        }
        if (isset($_POST['RoleID'])) {
            $RoleID = mysql_real_escape_string($_POST['RoleID']);
            $this->session->set_userdata("user_filter_RoleID", $RoleID);
        }
        if (isset($_POST['Published'])) {
            $Published = mysql_real_escape_string($_POST['Published']);
            $this->session->set_userdata("user_filter_Published", $Published);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter() {
        $this->session->unset_userdata("user_filter_Keywords");
        $this->session->unset_userdata("user_filter_RoleID");
        $this->session->unset_userdata("user_filter_Published");
        $this->search('setsessionsearch');
    }

    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 1) {
            $user = $this->db->query("select Thumb from ttp_user where ID=$id")->row();
            if ($user)
                $this->delete_file_path($user->Thumb);
            $this->db->query("delete from ttp_user where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add user | Config System');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/system_user/'
        );
        $this->template->write_view('content', 'admin/system_user_add', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $UserType = isset($_POST['UserType']) ? $this->lib->fill_data($_POST['UserType']) : '';
        $UserName = isset($_POST['UserName']) ? $this->lib->fill_data($_POST['UserName']) : '';
        $Password = isset($_POST['Password']) ? $this->lib->fill_data($_POST['Password']) : '';
        $Email = isset($_POST['Email']) ? $this->lib->fill_data($_POST['Email']) : '';
        $LastName = isset($_POST['LastName']) ? $this->lib->fill_data($_POST['LastName']) : '';
        $FirstName = isset($_POST['FirstName']) ? $this->lib->fill_data($_POST['FirstName']) : '';
        $HomePage = isset($_POST['HomePage']) ? $this->lib->fill_data($_POST['HomePage']) : 'home';
        $Gender = isset($_POST['Gender']) ? $this->lib->fill_data($_POST['Gender']) : 0;
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $Chanel = isset($_POST['Chanel']) ? $this->lib->fill_data($_POST['Chanel']) : 0;
        $RoleID = isset($_POST['RoleID']) ? $this->lib->fill_data($_POST['RoleID']) : 0;
        $DepartmentID = isset($_POST['DepartmentID']) ? $this->lib->fill_data($_POST['DepartmentID']) : 0;
        $AllowRemote = isset($_POST['AllowRemote']) ? $this->lib->fill_data($_POST['AllowRemote']) : 0;

        $check = $this->db->query("select * from ttp_user where UserName='$UserName'")->row();
        if ($check) {
            $return = ADMINPATH . "/home/" . $this->classname . "/add?error=user_exists";
            redirect($return);
        }
        if ($UserName != '' && $Password != '' && $Email != '' && $Gender != '' && $RoleID != 0) {
            $newuser = $this->db->query("select max(ID) as ID from ttp_user")->row();
            $NewID = $newuser ? $newuser->ID + 1 : 1;
            $data = array(
                'ID' => $NewID,
                'DepartmentID' => $DepartmentID,
                'AllowRemote' => $AllowRemote,
                'Published' => $Published,
                'Channel' => $Chanel,
                'Gender' => $Gender,
                'UserName' => $UserName,
                'Password' => sha1($Password),
                'Email' => $Email,
                'LastName' => $LastName,
                'FirstName' => $FirstName,
                'HomePage' => $HomePage,
                'RoleID' => $RoleID,
                'UserType' => $UserType,
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s')
            );
            if (isset($_FILES['Image_upload'])) {
                if ($_FILES['Image_upload']['error'] == 0) {
                    $this->upload_to = "user_thumb/$NewID";
                    $data['Thumb'] = $this->upload_image_single();
                }
            }
            if (count($_POST) > 0) {
                $arr = array();
                foreach ($_POST as $key => $value) {
                    if (!array_key_exists($key, $data) && $key != 'Thumb') {
                        $arr[$key] = $value;
                    }
                }
                $arr = json_encode($arr);
                $data['DetailRole'] = $arr;
            }
            $this->db->insert("ttp_user", $data);
        }
        redirect(ADMINPATH . '/home/system_user/');
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_user where ID=$id")->row();
            if (!$result)
                show_error("Data is empty !");
            $this->template->add_title('Edit user | Config System');
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/system_user/',
                'data' => $result
            );
            $this->template->write_view('content', 'admin/system_user_edit', $data);
            $this->template->render();
        }
    }

    public function update() {

        $Clone = isset($_POST['Clone']) ? $this->lib->fill_data($_POST['Clone']) : 0;

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';
        $UserType = isset($_POST['UserType']) ? $this->lib->fill_data($_POST['UserType']) : '';
        $UserName = isset($_POST['UserName']) ? $this->lib->fill_data($_POST['UserName']) : '';
        $Password = isset($_POST['Password']) ? $this->lib->fill_data($_POST['Password']) : '';
        $Email = isset($_POST['Email']) ? $this->lib->fill_data($_POST['Email']) : '';
        $LastName = isset($_POST['LastName']) ? $this->lib->fill_data($_POST['LastName']) : '';
        $FirstName = isset($_POST['FirstName']) ? $this->lib->fill_data($_POST['FirstName']) : '';
        $HomePage = isset($_POST['HomePage']) ? $this->lib->fill_data($_POST['HomePage']) : 'home';
        $Gender = isset($_POST['Gender']) ? $this->lib->fill_data($_POST['Gender']) : 0;
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $Chanel = isset($_POST['Chanel']) ? $this->lib->fill_data($_POST['Chanel']) : 0;
        $RoleID = isset($_POST['RoleID']) ? $this->lib->fill_data($_POST['RoleID']) : 0;
        $GroupID = isset($_POST['GroupID']) ? $this->lib->fill_data($_POST['GroupID']) : 1;
        $DepartmentID = isset($_POST['DepartmentID']) ? $this->lib->fill_data($_POST['DepartmentID']) : 0;
        $AllowRemote = isset($_POST['AllowRemote']) ? $this->lib->fill_data($_POST['AllowRemote']) : 0;
        $check = $this->db->query("select * from ttp_user where UserName='$UserName' and ID!=$ID")->row();
        if ($check) {
            $return = ADMINPATH . "/home/" . $this->classname . "/edit/$ID/?error=user_exists";
            redirect($return);
        }
        if ($UserName != '' && $Password != '' && $Email != '' && $Gender != '' && $RoleID != 0) {
            $user = $this->db->query("select * from ttp_user where ID=$ID")->row();
            if ($user) {
                $newPassword = $Password == $user->Password ? $Password : sha1($Password);
                $Published = $user->IsAdmin == 1 ? 1 : $Published;
                $data = array(
                    'DepartmentID' => $DepartmentID,
                    'Published' => $Published,
                    'AllowRemote' => $AllowRemote,
                    'Channel' => $Chanel,
                    'Gender' => $Gender,
                    'UserName' => $UserName,
                    'Password' => $newPassword,
                    'Email' => $Email,
                    'LastName' => $LastName,
                    'FirstName' => $FirstName,
                    'HomePage' => $HomePage,
                    'RoleID' => $RoleID,
                    'GroupID' => $GroupID,
                    'UserType' => $UserType,
                    'LastEdited' => date('Y-m-d H:i:s')
                );
                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['tmp_name'] != '') {
                        $data['Thumb'] = $this->upload_image_single();
                        $this->delete_file_path($user->Thumb);
                    }
                }
                if (count($_POST) > 0) {
                    $arr = array();
                    foreach ($_POST as $key => $value) {
                        if (!array_key_exists($key, $data) && $key != 'Thumb') {
                            $arr[$key] = $value;
                        }
                    }
                    $arr = json_encode($arr);
                    $data['DetailRole'] = $arr;
                }
                if ($Clone == 1) {
                    $data['Created'] = date('Y-m-d H:i:s');
                    $this->db->insert("ttp_user", $data);
                } else {
                    $this->db->where("ID", $ID);
                    $this->db->update("ttp_user", $data);
                }
            }
        }
        redirect(ADMINPATH . '/home/system_user/');
    }

}

?>