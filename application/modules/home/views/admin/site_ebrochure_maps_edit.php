<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <!--<a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>-->
    </div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ?>" method="post" enctype="multipart/form-data">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel tab1 tabcontent">
                    <div class="x_title">
                        <h2>E-Brochure <small> / Edit item</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="ID" value="<?php echo isset($data) ? $data->ID : 0; ?>" />
                    <div class="x_content"> 
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="Name" type="text" class="form-control" name="Name" value="<?= isset($data) ? $data->Name : '' ?>"/>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($data->Data) {
                            $maps = json_decode($data->Data);
                            $count = count(json_decode($maps->coords));
                            for ($i = 0; $i < $count; $i++) {
                                ?>
                                <div class="form-group" id="klon<?= $i + 1 ?>">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" <?php if ($i > 0) : ?>id="removeDiv"<?php endif; ?>><?php if ($i > 0) : ?><i class="fa fa-minus-circle"></i><?php else: ?><i class="fa fa-plus-circle" id="cloneDiv"></i><?php endif; ?></label>
                                    <div class="form-group">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <input type="text" class="form-control" name="coords[]" value="<?= json_decode($maps->coords)[$i]; ?>"/>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <input type="text" class="form-control" name="alt[]" value="<?= json_decode($maps->alt)[$i]; ?>"/>
                                        </div>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <input type="text" class="form-control" name="href[]" value="<?= json_decode($maps->href)[$i]; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="form-group" id="klon1">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"><i class="fa fa-plus-circle" id="cloneDiv"></i></label>
                                <div class="form-group">
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="text" class="form-control" name="coords[]" value="" placeholder="499,444,400"/>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="text" class="form-control" name="alt[]" value="" placeholder="alt"/>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <input type="text" class="form-control" name="href[]" value="" placeholder="href"/>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image
                                <span class="btn btn-default btn-file btn-primary">
                                    Browse <input type="file" name="Image_upload" id="choosefile" />
                                </span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="dvPreview">
                                    <?php
                                    if (isset($data->Image)) {
                                        if (file_exists($data->Image)) {
                                            echo "<img src='$data->Image' style='max-width:200px' />";
                                        }
                                    } else {
                                        echo '<span style="padding-top:9px;display:block">No image selected</span>';
                                    }
                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Submit</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</form>
</div>
</div>

<script>
    $(document).ready(function () {

        $("#choosefile").change(function () {
            var Fileinput = document.getElementById("choosefile");
            var file = Fileinput.files[0];
            var imageType = /image.*/
            var dvPreview = $(".dvPreview");
            if (file.type.match(imageType)) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr("style", "max-height:300px;max-width: 400px");
                    img.attr("src", e.target.result);
                    dvPreview.html(img);
                }
                reader.readAsDataURL(file);
            } else {
                console.log("Not an Image");
            }
        });

        $('#cloneDiv').click(function () {
            var $div = $('div[id^="klon"]:last');
            var num = parseInt($div.prop("id").match(/\d+/g), 10) + 1;
            var $klon = $div.clone().prop('id', 'klon' + num);
            $div.after($klon.html('<label class="control-label col-md-3 col-sm-3 col-xs-12" id="removeDiv"><i class="fa fa-minus-circle"></i></label><div class="form-group"><div class="col-md-2 col-sm-2 col-xs-12"><input type="text" class="form-control" name="coords[]" value="" placeholder="499,444,400"/></div><div class="col-md-2 col-sm-2 col-xs-12"><input type="text" class="form-control" name="alt[]" value="" placeholder="alt"/></div><div class="col-md-2 col-sm-2 col-xs-12"><input type="text" class="form-control" name="href[]" value="" placeholder="href"/></div></div>'));
        });

        $(document).on("click", "#removeDiv", function () {
            $(this).parent('div .form-group').remove();
        });

    });
</script>