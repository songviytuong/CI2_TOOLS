<?php 
$type = $this->lib->get_config_define('products','productstype');
?>
<div class="sub_menu height">
	<div class="list-open-bill pull-left">
		<a class="btn current" onclick="opentab(this,'tab2')"><b><i class="fa fa-bar-chart" aria-hidden="true"></i> Báo cáo tổng quan</b></a>
		<a class="btn" onclick="opentab(this,'tab1')"><b><i class="fa fa-stack-overflow" aria-hidden="true" style="margin-right:5px;"></i> Báo cáo chi tiết</b></a>
	</div>
	<div class="find_record">
		<a class="btn btn-danger pull-right" style="margin-right:20px" href="<?php echo base_url().ADMINPATH.'/pos/pos_inventory/create' ?>"><i class="fa fa-plus"></i> Thực hiện kiểm kho</a>
	</div>
</div>
<div class="containner height">
	<div class="tab2 tab-content">
		<div class="row" style="margin:15px 0px">
			<?php 
			$arr_value = array();
			$arr_products = array();
			if(count($data)>0){
				foreach($data as $row){
					if(isset($arr_value[$row->TypeProducts])){
						$arr_value[$row->TypeProducts] = $arr_value[$row->TypeProducts]+($row->OnHand*$row->RootPrice);
					}else{
						$arr_value[$row->TypeProducts] = ($row->OnHand*$row->RootPrice);
					}
					$arr_products[$row->TypeProducts][$row->ID] = 1;
				}
			}
			if(count($type)>0){
				foreach($type as $key=>$value){
					$row_type = isset($arr_products[$key]) ? count($arr_products[$key]) : 0 ;
					$row_value = isset($arr_value[$key]) ? $arr_value[$key] : 0 ;
					echo '<div class="col-xs-6">
							<h4>'.$value.'</h4>
							<hr>
							<div class="row">
								<div class="col-xs-6">
									<p>Số loại sản phẩm <small>(Loại)</small></p>
									<h3>'.number_format($row_type).'</h3>
								</div>
								<div class="col-xs-6">
									<p>Giá trị tồn kho <small>(vnđ)</small></p>
									<h3>'.number_format($row_value).'</h3>
								</div>
							</div>
						</div>';
				}
			}
			?>
		</div>
	</div>
	<div class="tab1 hidden tab-content">
		<div class="table_div" style="margin:15px 0px">
			<table class="table">
				<tr>
					<th>STT</th>
					<th>Mã SKU</th>
					<th>Tên sản phẩm</th>
					<th>Loại thực phẩm</th>
					<th>Đơn vị</th>
					<th class='text-right'>Giá mua</th>
					<th class='text-right'>Số có thể bán</th>
					<th class='text-right'>Số tồn thực tế</th>
					<th class='text-right'>Giá trị tồn</th>
				</tr>
				<?php 
				$total = 0;
				if(count($data)>0){
					$i=1;
					foreach ($data as $row) {
						$total = $total+($row->OnHand*$row->RootPrice);
						$type_row = isset($type[$row->TypeProducts]) ? $type[$row->TypeProducts] : '--' ;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Title</td>";
						echo "<td>".$type_row."</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td class='text-right'>".number_format($row->RootPrice)."</td>";
						echo "<td class='text-right'>".number_format($row->Available,2)."</td>";
						echo "<td class='text-right'>".number_format($row->OnHand,2)."</td>";
						echo "<td class='text-right'>".number_format($row->RootPrice*$row->OnHand)."</td>";
						echo "</tr>";
						$i++;
					}
				}
				?>
				<tr>
					<th class='text-right' colspan="8">TỔNG CỘNG</th>
					<th class='text-right'><?php echo number_format($total) ?></th>
				</tr>
			</table>
		</div>
	</div>
</div>
<style>
	table tr td{border-bottom:1px dotted #EEE !important;}
</style>
<script>
	function opentab(ob,tab){
		$(".list-open-bill a").removeClass('current');
		$(ob).addClass('current');
		$(".tab-content").addClass('hidden');
		$("."+tab).removeClass('hidden');
	}
</script>