<div class="containner">
    <form action="<?php echo base_url().ADMINPATH.'/report/warehouse/add_check_warehouse' ?>" method="POST">
        <div class="row">
            <div class="form-group">
                <div class="col-xs-3">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Bắt đầu kiểm kho</button>
                </div>
                <div class="col-xs-9">
                    <div class="breakcrumb">
                        <a class="active">Dự thảo</a>
                        <a>Đang xử lý</a>
                        <a>Đã xác nhận</a>
                        <span>Trạng thái xử lý</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"><div class="form-group"><hr></div></div>
        <div class="row">
            <div class="form-group">
                <label class="col-xs-2 control-label">Ngày kiểm kho</label>
                <label class="col-xs-10 control-label"> :  <?php echo date('d/m/Y H:i:s') ?></label>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="col-xs-2 control-label">Tiêu đề kiểm kho</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control required" name="Title" placeholder="Nhập mô tả tiêu đề cho lần kiểm kho ..." required />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <label class="col-xs-2 control-label">Địa điểm kiểm kho</label>
                <div class="col-xs-4">
                    <select name="WarehouseID" id="" class="form-control">
                        <?php 
                        $warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                echo "<option value='$row->ID'>$row->MaKho - $row->Title</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <label class="col-xs-2 control-label text-right">Loại sản phẩm <span style="margin:0px 10px">:</span> </label>
                <div class="col-xs-4">
                    <select name="Type" id="" class="form-control">
                        <?php 
                        $arr_status = $this->define_model->get_order_status('products','productstype','position');
                        foreach($arr_status as $row){
                            echo "<option value='$row->code'>$row->name</option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>
<style>
    .row{margin:0px;}
    hr{margin:0px;}
</style>