<?php 
$url = base_url().ADMINPATH.'/report';
$segment = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-arrow-circle-up fa-fw" aria-hidden="true"></i> Operation</p>
    <ul>
        <li><a href="<?php echo $url.'/operation/pickup_plan' ?>" <?php echo $segment=="operation" && $segment_current_bonus=="pickup_plan" ? "class='active'" : '' ; ?>>Pick-up Plan</a></li>
        <li><a href="<?php echo $url.'/' ?>" <?php echo $segment=="operation" && $segment_current_bonus=="due" ? "class='active'" : '' ; ?>>Delivery Plan</a></li>
        <li><a href="<?php echo $url.'/' ?>" <?php echo $segment=="operation" && $segment_current_bonus=="due" ? "class='active'" : '' ; ?>>Print Delivery Notes</a></li>
        <li><a href="<?php echo $url.'/' ?>" <?php echo $segment=="operation" && $segment_current_bonus=="due" ? "class='active'" : '' ; ?>>Delivery Tracking</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>

<script>
    $(".sitebar ul li p i").click(function(){
        $(".sitebar ul li ul").slideUp();
        $(".sitebar ul li p i.fa").removeClass('fa-caret-up').addClass('fa-caret-down');
        $(this).removeClass('fa-caret-down').addClass('fa-caret-up');
        $(this).parent('p').parent('li').find('ul').slideDown();
    });
</script>
