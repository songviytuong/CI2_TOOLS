<?php 
$keywords = str_replace(' ',"+",$keywords);
$sortname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
$sortvalue = isset($_GET['sortvalue']) ? $_GET['sortvalue'] : 0 ;
$warehouseid  = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
$productionid  = isset($_GET['production']) ? (int)$_GET['production'] : 0 ;
?>
<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Quét QR Code nhập hàng từ nhà cung cấp </h1>
	    </div>
	    <div class="block2">
		    <form action="" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" name="keywords" placeholder="Nhập mã phiếu , tên KH ..." value="<?php echo str_replace('+',' ',$keywords); ?>" />
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </span>
                </div>   
            </form>
	    </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
                <label class="control-label">Tìm thấy <b style="background:#F00;color:#FFF;padding: 2px 5px;border-radius: 20px;font-size: 11px;"><?php echo count($data) ?> / <?php echo number_format($find) ?></b> phiếu cần quét QR Code</label>
            </div>
        </div>
        <div class="col-xs-8">
            <div class="form-group">
                <div class="col-xs-12">
                    <div class="btn-group pull-right">
                        <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Kho nhập hàng</a>
                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php 
                            $checked_sort = $warehouseid==0 ? "color:#090" : "color:#ccc" ;
                            echo '<li><a href="'.$base_link.'/import_production?keywords='.$keywords.'&sortname=pxk&sortvalue='.$sortvalue.'&production='.$productionid.'&warehouse=0"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Tất cả kho</a></li>';
                            if(count($warehouse)>0){
                                foreach($warehouse as $row){
                                    $checked_sort = $warehouseid==$row->ID ? "color:#090" : "color:#ccc" ;
                                    echo '<li><a href="'.$base_link.'/import_production?keywords='.$keywords.'&sortname=pxk&sortvalue='.$sortvalue.'&production='.$productionid.'&warehouse='.$row->ID.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> '.$row->MaKho.'</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>

                    <div class="btn-group pull-right" style="margin-right:8px">
                        <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Nhà cung cấp</a>
                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php 
                            $checked_sort = $productionid==0 ? "color:#090" : "color:#ccc" ;
                            echo '<li><a href="'.$base_link.'/import_production?keywords='.$keywords.'&sortname=pxk&sortvalue='.$sortvalue.'&warehouse='.$warehouseid.'&production=0"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> Tất cả nhà cung cấp</a></li>';
                            if(count($production)>0){
                                foreach($production as $row){
                                    $checked_sort = $productionid==$row->ID ? "color:#090" : "color:#ccc" ;
                                    echo '<li><a href="'.$base_link.'/import_production?keywords='.$keywords.'&sortname=pxk&sortvalue='.$sortvalue.'&warehouse='.$warehouseid.'&production='.$row->ID.'"><i class="fa fa-check-square" style="margin-right:10px;'.$checked_sort.'" aria-hidden="true"></i> '.$row->Title.'</a></li>';
                                }
                            }
                            ?>
                        </ul>
                    </div>

                    <div class="btn-group pull-right" style="margin-right:8px">
                        <a class="btn btn-default"><i class="fa fa-sort" aria-hidden="true"></i> Sắp xếp phiếu</a>
                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php 
                            $checked_sort0 = $sortname=='pxk' && $sortvalue==0 ? "color:#090" : "color:#ccc" ;
                            $checked_sort1 = $sortname=='pxk' && $sortvalue==1 ? "color:#090" : "color:#ccc" ;
                            ?>
                            <li><a href="<?php echo $base_link."/import_production?keywords=$keywords&sortname=pxk&sortvalue=0&warehouse=$warehouseid&production=$productionid" ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $checked_sort0 ?>" aria-hidden="true"></i> Mới nhất</a></li>
                            <li><a href="<?php echo $base_link."/import_production?keywords=$keywords&sortname=pxk&sortvalue=1&warehouse=$warehouseid&production=$productionid" ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $checked_sort1 ?>" aria-hidden="true"></i> Cũ nhất</a></li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="row scanlist">
    	<?php 
    	if(count($data)>0){
    		foreach($data as $row){
    			echo "<div class='col-xs-3' id='exportid$row->ID'>
						<h4><a href='".$base_link."preview_import_production/$row->ID'>$row->MaNK</a> <b onclick='openscanbox($row->ID)' title='Quét nhanh phiếu nhập này'><i class='fa fa-qrcode'></i></b></h4>
                        <p>Theo PO số : $row->POCode</p>
			    		<p>Kho nhập hàng : <span class='trans'>$row->MaKho</span></p>
			    		<p>Số lượng cần quét : <span class='amount'>$row->Amount sản phẩm</span></p>
			    	</div>";
    		}
    		echo $nav;
    	}else{
    		echo "<h1 class='welcome_title'><i class='fa fa-info-circle' aria-hidden='true' style='margin-right:20px'></i> Not found data to scan</h1>";
    	}
    	?>
    </div>

    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>

</div>
<script>
	function openscanbox(datascan){
		$(window).scrollTop(110);
		$(".over_lay .box_inner").css({'margin-top':'150px'});
    	$(".over_lay .box_inner .block1_inner h1").html("Quét mã QRCODE / BARCODE");
    	$(".over_lay .box_inner .block2_inner").load("<?php echo base_url().ADMINPATH.'/report/warehouse_scancode/scan_from_import_production/' ?>"+datascan);
    	$(".over_lay").removeClass('in');
    	$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
	});

	function enterqrcode(ob,dataid){
		var data = $(ob).val();
		var remain = $(ob).attr('remain');
		$.ajax({
            dataType: "json",
            data: "data="+data+"&ID="+dataid+"&remain="+remain,
            url: '<?php echo base_url().ADMINPATH.'/report/warehouse_scancode/' ?>check_import_production_code',
            cache: false,
            method: 'POST',
            beforeSend:function(){
            	$(ob).parent('div').find('.alert').addClass('hidden');
            	$(ob).parent('div').find('#checking').removeClass('hidden');
            },
            success: function(result) {
            	console.log(result);
                if(result.error==false){
                	if(result.ready!=false){
                		$("#exportid"+result.ready).remove();
                		$(".over_lay").hide();
                	}else{
                		openscanbox(dataid);
                	}
				}else{
                    $(ob).parent('div').find('#checking').addClass('hidden');
					if(result.error==true){
                        $(ob).parent('div').find('.alert').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Mã Code không được chấp nhận . Vui lòng thử lại nhé !').removeClass('hidden');
                    }else{
                        $(ob).parent('div').find('.alert').html('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> '+result.error).removeClass('hidden');
                    }
					$(ob).val('');
					$(ob).focus();
				}
            }
        });
	}

    function welcome(){
        $(".welcome_title").addClass("animated");
        $(".welcome_title").addClass("fadeInDown");
    }

    setTimeout(welcome,500);
</script>