<form action="<?php echo base_url().ADMINPATH.'/report/warehouse_warehouse/update_position' ?>" method="post">
	<input type='hidden' name="ID" value='<?php echo $data->ID ?>' />
	<div class="row">
		<p><b>Khu vực</b></p>
		<div><input type='text' name="Position" value="<?php echo $data->Position ?>" required /></div>
	</div>
	<div class="row">
		<p><b>Tên kệ</b></p>
		<div><input type='text' name="Rack" value="<?php echo $data->Rack ?>" required /></div>
	</div>
	<div class="row">
		<p><b>Tên cột</b></p>
		<div><input type='text' name="Colum" value="<?php echo $data->Col ?>" required /></div>
	</div>
	<div class="row">
		<p><b>Tên hàng</b></p>
		<div><input type='text' name="Row" value="<?php echo $data->Row ?>" required /></div>
	</div>
	<div class="row">
		<button type="submit" class='btn btn-primary'>Thêm vị trí</button>
	</div>
</form>
<style>
	.row{margin-bottom:10px !important;}
	.row p{margin-bottom: 5px !important}
	.row .btn{background-color: #1A82C3 !important;border-color: #1A82C3 !important;color:#FFF !important;}
</style>