<form>
	<?php 
	if(isset($data)){
	?>
		<div class="row">
			<label for="" class="control-label col-xs-4 text-right">Tên thuộc tính : </label>
			<?php 
			$parent = $this->db->query("select Title from ttp_report_properties where ID=$data")->row();
			echo $parent ? "<label class='control-label'>$parent->Title</label>" : '' ;
			?>
			<input type='hidden' name="ParentID" value="<?php echo $data ?>" />
		</div>
		<div class="row">
			<div class="form-group">
			<label for="" class="control-label col-xs-4 text-right">Giá trị thuộc tính : </label>
				<div class="col-xs-6">
					<input type='text' name="Title" class='form-control' placeholder="Điền giá trị thuộc tính" />
				</div>
			</div>
		</div>
	<?php 
	}else{
	?>
		<div class="row">
			<div class="form-group">
				<label for="" class="control-label col-xs-4 text-right">Tên thuộc tính : </label>
				<div class="col-xs-6">
					<input type='hidden' name="ParentID" value="0" />
					<input type='text' name="Title" class="form-control" placeholder="Điền tên thuộc tính" />
				</div>
			</div>
		</div>
	<?php 
	}
	?>
	<div class="row">
		<div class="form-group">
			<div class="col-xs-4"></div>
			<div class="col-xs-6">
				<a class="btn btn-primary" onclick="save_properties(this)"><i class="fa fa-check-square-o" aria-hidden="true"></i> Lưu thông tin</a>
			</div>	
		</div>
	</div>
</form>
