<?php 

class Report extends Admin_Controller { 

 

 	public $user;

 	public $classname="report";



    public function __construct() { 

        parent::__construct();   

        $session = $this->session->userdata('ttp_usercp');

		$this->user = $this->lib->get_user($session,$this->classname);

        $this->load->library('template');

        $this->template->set_template('report');

        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));

        $this->template->write_view('header','admin/header',array('user'=>$this->user));

        $this->template->add_js("public/admin/js/script_report.js");

        $this->template->add_doctype(); 

    }



    public $limit = 30;

	

	public function index(){

        $this->template->add_title('Report Tools');

		$this->template->write_view('content','admin/content');

		$this->template->render();

	}

    public function load_order_by_products($id=0){

        $startday = $this->session->userdata("import_startday");

        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;

        $stopday = $this->session->userdata("import_stopday");

        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $result = $this->db->query("select a.* from ttp_report_order a,ttp_report_orderdetails b where a.ID=b.OrderID and b.ProductsID=$id and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'")->result();

        echo "<table class='table table-bordered'>";

            echo "<tr>

                <th>STT</th>

                <th>MÃ DH</th>

                <th>NGÀY ĐẶT HÀNG</th>

                <th>KHÁCH HÀNG</th>

                <th>ĐỊA CHỈ</th>

                <th>GIÁ TRỊ</th>

            </tr>";

        if(count($result)>0){

            $i=1;

            $link = base_url().ADMINPATH;

            foreach($result as $row){

                echo "<tr>";

                echo "<td>$i</td>";

                echo "<td><a target='_blank' href='$link/report/import_order/edit/$row->ID'>$row->MaDH</a></td>";

                echo "<td><a target='_blank' href='$link/report/import_order/edit/$row->ID'>".date('d/m/Y',strtotime($row->Ngaydathang))."</a></td>";

                echo "<td><a target='_blank' href='$link/report/import_order/edit/$row->ID'>$row->Name</a></td>";

                echo "<td title='$row->AddressOrder'><a target='_blank' href='$link/report/import_order/edit/$row->ID'>$row->AddressOrder</a></td>";

                echo "<td><a target='_blank' href='$link/report/import_order/edit/$row->ID'>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</a></td>";

                echo "</tr>";

                $i++;

            }

        }else{

            echo "<tr><td colspan='6'>Không tìm thấy dữ liệu theo yêu cầu</td></tr>";

        }

        echo "</table>";

    }



    public function set_day(){

        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $active = isset($_POST['active']) ? $_POST['active'] : 0 ;

        if($active==1){

            $this->session->set_userdata("active_vs",1);

        }else{

            $this->session->set_userdata("active_vs",0);

        }

        $group1 = isset($_POST['group1']) ? $_POST['group1'] : "" ;

        if($group1!=''){

            $group1 = explode("-",$group1);

            $startday = isset($group1[0]) ? trim($group1[0]) : '';

            $stopday = isset($group1[1]) ? trim($group1[1]) : '';

            if($startday!='' && $stopday!=''){

                $startday = explode('/',$startday);

                $stopday = explode('/',$stopday);

                if(count($startday)==3 && count($stopday)==3){

                    $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];

                    $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];

                    $timestart = strtotime($startday);

                    $timestop = strtotime($stopday);

                    if($timestart>$timestop){

                        echo "fasle";

                        return;

                    }

                    $this->session->set_userdata("startday",$startday);

                    $this->session->set_userdata("stopday",$stopday);

                }

            }

        }

        if($active==1){

            $group2 = isset($_POST['group2']) ? $_POST['group2'] : "" ;

            if($group2!=''){

                $group2 = explode("-",$group2);

                $startday = isset($group2[0]) ? trim($group2[0]) : '';

                $stopday = isset($group2[1]) ? trim($group2[1]) : '';

                if($startday!='' && $stopday!=''){

                    $startday = explode('/',$startday);

                    $stopday = explode('/',$stopday);

                    if(count($startday)==3 && count($stopday)==3){

                        $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];

                        $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];

                        $timestart = strtotime($startday);

                        $timestop = strtotime($stopday);

                        if($timestart>$timestop){

                            echo "fasle";

                            return;

                        }

                        $this->session->set_userdata("startday_vs",$startday);

                        $this->session->set_userdata("stopday_vs",$stopday);

                    }

                }

            }

        }

        echo 'OK';

    }

}

?>