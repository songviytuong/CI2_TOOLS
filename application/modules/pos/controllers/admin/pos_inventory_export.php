<?php 
class Pos_inventory_export extends Admin_Controller { 
 
 	public $user;
 	public $classname="pos_inventory_export";
    public $limit=15;
    public $WarehouseID=2;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('pos');
        $this->template->add_doctype();
        $warehouse = $this->session->userdata('branch');
        $this->WarehouseID = $warehouse!='' ? $warehouse : $this->WarehouseID ;
    }

    public function create(){
        $this->lib->check_permission_function($this->user);
        $files = scandir('log/pos/transfer_warehouse');
        if(count($files)==2){
            $txt = time();
            file_put_contents('log/pos/transfer_warehouse/draft_order_'.$txt.'.txt',json_encode(array('products'=>array(),'price'=>array(),'partner'=>array('WarehouseSender'=>$this->WarehouseID,'WarehouseReciver'=>'','Active'=>0,'Note'=>''))));
        }
        $this->template->add_title('Import warehouse | POS Tools');
		$this->template->write_view('content','admin/transfer_warehouse');
        $this->template->write_view('header','admin/header',array(
            'user'=>$this->user,
            'controllers'=>'Giao dịch',
            'function'=>'<a href="'.ADMINPATH.'/pos/pos_inventory_export">Chuyển hàng</a>',
            'bonus'=>'<a>Tạo phiếu chuyển hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'transfer_warehouse'));
		$this->template->render();
	}

    public function index(){
        $this->template->add_title('Transfer warehouse | POS Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_transferorder where WarehouseSender=$this->WarehouseID or WarehouseReciver=$this->WarehouseID")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select * from ttp_report_transferorder where WarehouseSender=$this->WarehouseID or WarehouseReciver=$this->WarehouseID order by ID DESC ".$limit_str)->result();
        $data = array(
            'data'  =>$result,
            'total' =>$nav,
            'start' =>$start,
            'limit' =>$this->limit,
            'nav'   =>$this->lib->nav(base_url().ADMINPATH.'/pos/pos_inventory_export/bill',5,$nav,$this->limit)
        );
        $this->template->write_view('content','admin/transfer_warehouse_bill',$data);
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Giao dịch','function'=>'<a>Chuyển hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'transfer_warehouse'));
        $this->template->render();
    }

    public function view_order($id=0){
        $result = $this->db->query("select a.*,b.UserName from ttp_report_transferorder a,ttp_user b where a.UserID=b.ID and a.ID=$id")->row();
        if($result){
            $this->template->add_title('Transfer warehouse | POS Tools');
            $this->template->write_view('content','admin/transfer_warehouse_view',array('data'=>$result));
            $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Giao dịch','function'=>'<a href="'.ADMINPATH.'/pos/pos_inventory_export/">Chuyển hàng</a>','bonus'=>'<a>Thông tin phiếu chuyển</a>'));
            $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'transfer_bill'));
            $this->template->render();
        }
    }

    public function draftorder(){
        $txt = time();
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$txt.'.txt',json_encode(array('products'=>array(),'partner'=>array('Note'=>'','WarehouseSender'=>$this->WarehouseID,'WarehouseReciver'=>'','Active'=>0))));
        $response['error'] = 0;
        $response['order'] = $txt;
        echo json_encode($response);
    }

    public function remove_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : '' ;
        @unlink("log/pos/transfer_warehouse/draft_order_".$order.".txt");
        $files = scandir('log/pos/transfer_warehouse');
        $first = 0;
        if(count($files)>0){
            $i=1;
            foreach($files as $row){
                if($row!='.' && $row!='..'){
                    $name = explode('_',$row);
                    $name = str_replace('.txt','',$name[2]);
                    if($i==1){
                        $first = $name;
                    }
                    $i++;
                }
            }
        }
        echo $first;
    }

    public function export_data($currentday=0){
        if($currentday==0){
            $month = date('m');
            $year = date('Y');
            $result = $this->db->query("select c.ID,c.MaSP,c.Title,c.Donvi,a.OrderCode,a.Created,a.WarehouseSender,a.WarehouseReciver,a.ExportDate,a.ImportDate,b.*,a.Status from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and month(a.Created)='$month' and year(a.Created)='$year' and (a.WarehouseSender=".$this->WarehouseID." or a.WarehouseReciver=$this->WarehouseID)")->result();
        }else{
            $result = $this->db->query("select c.ID,c.MaSP,c.Title,c.Donvi,a.OrderCode,a.Created,a.WarehouseSender,a.WarehouseReciver,a.ExportDate,a.ImportDate,b.*,a.Status from ttp_report_transferorder a,ttp_report_transferorder_details b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and date(a.Created)='".date('Y-m-d')."' and (a.WarehouseSender=".$this->WarehouseID." or a.WarehouseReciver=$this->WarehouseID)")->result();
        }
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'SKU')
                        ->setCellValue('B1', 'Tên sản phẩm')
                        ->setCellValue('C1', 'Donvi')
                        ->setCellValue('D1', 'Yêu cầu xuất')
                        ->setCellValue('E1', 'Thực xuất')
                        ->setCellValue('F1', 'Kho xuất')
                        ->setCellValue('G1', 'Ngày xuất')
                        ->setCellValue('H1', 'Thực nhận')
                        ->setCellValue('I1', 'Kho nhận')
                        ->setCellValue('J1', 'Ngày nhận')
                        ->setCellValue('K1', 'Mã phiếu chuyển')
                        ->setCellValue('L1', 'Trạng thái phiếu');
            $warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
            $arr_warehouse = array();
            if(count($warehouse)>0){
                foreach ($warehouse as $row) {
                    $arr_warehouse[$row->ID] = $row->MaKho;
                }
            }
            $arr = array(
                4=>'Thành công',
                3=>'Chờ nhập',
                1=>'Đã hủy',
                0=>'Nháp'
            );
            $i=2;
            foreach($result as $row){
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row->MaSP)
                        ->setCellValue('B'.$i, $row->Title)
                        ->setCellValue('C'.$i, $row->Donvi)
                        ->setCellValue('D'.$i, $row->Request)
                        ->setCellValue('E'.$i, $row->TotalExport)
                        ->setCellValue('F'.$i, $arr_warehouse[$row->WarehouseSender])
                        ->setCellValue('G'.$i, $row->ExportDate)
                        ->setCellValue('H'.$i, $row->TotalImport)
                        ->setCellValue('I'.$i, $arr_warehouse[$row->WarehouseReciver])
                        ->setCellValue('J'.$i, $row->ImportDate)
                        ->setCellValue('K'.$i, $row->OrderCode)
                        ->setCellValue('L'.$i, $arr[$row->Status]);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('RESULT_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data !";
        }
    }

    public function get_products_by_barcode(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select ID from ttp_report_products where Barcode='$keywords'")->row();
        $response = array('error'=>1);
        if($result){
            $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$result->ID])){
                $data['products'][$result->ID] = $data['products'][$result->ID] + 1;
            }else{
                $data['products'][$result->ID] = 1;
            }
            file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
            $response = array('error'=>0);
        }
        echo json_encode($response);
    }

    public function get_products_list(){
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select MaSP,Title,ID from ttp_report_products where Title like '%$keywords%' or MaSP like '%$keywords%' or Barcode='$keywords' limit 0,20")->result();
        if(count($result)>0){
            foreach ($result as $key => $value) {
                echo "<p><a onclick='transfer_warehouse_apply_products($value->ID)'>($value->MaSP) | $value->Title</a></p>";
            }
        }
    }

    public function reset_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['products'] = array();
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(!isset($data['products'])){
            $data['products'] = array();
        }
        if(!isset($data['reduce'])){
            $data['reduce'] = 0;
        }
        if(isset($data['products'][$products])){
            $data['products'][$products] = $data['products'][$products]+1;
        }else{
            $data['products'][$products] = 1;
        }
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function remove_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(isset($data['products'][$products])){
            unset($data['products'][$products]);
        }
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function change_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 1 ;
        if($amount>0 && $products>0){
            $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$products])){
                $data['products'][$products] = $amount;
            }
            file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function change_products_price_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $price = isset($_POST['price']) ? $_POST['price'] : 0 ;
        if($price>=0 && $products>0){
            $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['price'][$products] = $price;
            file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function load_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $this->load->view("admin/transfer_warehouse_draft_order_products",array('data'=>$data));
    }

    public function load_customers_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        echo json_encode($data['partner']);
    }

    public function apply_note_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $note = isset($_POST['note']) ? $_POST['note'] : '' ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['Note'] = $note;
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_warehousesender_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $warehouse = isset($_POST['warehouse']) ? $_POST['warehouse'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['WarehouseSender'] = $warehouse;
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_warehousereciver_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $warehouse = isset($_POST['warehouse']) ? $_POST['warehouse'] : 0 ;
        $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['WarehouseReciver'] = $warehouse;
        file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_reduce_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $reduce = isset($_POST['reduce']) ? $_POST['reduce'] : 0 ;
        if($reduce>=0){
            $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['reduce'] = $reduce;
            file_put_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function change_products_import(){
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 0 ;
        $id = isset($_POST['id']) ? $_POST['id'] : 0 ;
        $check = $this->db->query("select * from ttp_report_transferorder_details where ID=$id")->row();
        $response = array('warning'=>0);
        if($check && $amount>=0){
            if($amount!=$check->TotalExport){
                $response['warning'] = 1;
            }
            $this->db->query("update ttp_report_transferorder_details set TotalImport=$amount where ID=$id");
        }
        echo json_encode($response);
    }

    public function print_order($id=0){
        $order = $this->db->query("select * from ttp_report_transferorder where ID=$id")->row();
        if($order){
            $this->load->view("admin/transfer_warehouse_draft_order_print",array('data'=>$order));
        }
    }

    public function create_order(){
        $order = isset($_POST['order']) ? $_POST['order'] : '' ;
        $WarehouseSender = isset($_POST['WarehouseSender']) ? $_POST['WarehouseSender'] : '' ;
        $note = isset($_POST['note']) ? $_POST['note'] : '' ;
        $WarehouseReciver = isset($_POST['WarehouseReciver']) ? $_POST['WarehouseReciver'] : '' ;
        $response = array("error"=>1,"message"=>"Chuyển hàng thất bại !");
        if($WarehouseSender>0 && $WarehouseReciver>0 && $WarehouseReciver!=$WarehouseSender){
            $data = file_get_contents('log/pos/transfer_warehouse/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products']) && count($data['products'])>0){
                /*
                *   Check inventory warehouse sender
                */
                $total_amount=0;
                $key = array_keys($data['products']);
                $products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
                if(count($products)>0){
                    foreach ($products as $row) {
                        $onhand = $this->db->query("select sum(OnHand) as total from ttp_report_inventory where ProductsID=$row->ID and WarehouseID=$WarehouseSender and LastEdited=1")->row();
                        $onhand = $onhand ? $onhand->total : 0 ;
                        $amount = $data['products'][$row->ID];
                        if($amount>$onhand){
                            $response = array("error"=>1,"message"=>"Xuất hàng thất bại ! Một trong các sản phẩm của bạn cần chuyển đã hết hàng.");
                            echo json_encode($response);
                            return;
                        }
                        $total_amount = $total_amount+$amount;
                    }
                }

                $monthuser = date("m",time());
                $yearuser = date("Y",time());
                $userorder = $this->db->query("select count(ID) as SL from ttp_report_transferorder where MONTH(Created)=$monthuser and YEAR(Created)=$yearuser")->row();
                $yearuser = date("y",time());
                $userorder = $userorder ? $userorder->SL+1+$monthuser+$yearuser : 1+$monthuser+$yearuser ;
                $monyear = date('ym');
                $idbyUser = str_pad($userorder, 5, '0', STR_PAD_LEFT);
                $MaDH = 'TF'.$monyear."_".$idbyUser;

                $sql = array(
                    'WarehouseSender'   =>$WarehouseSender,
                    'WarehouseReciver'  =>$WarehouseReciver,
                    'OrderCode'         =>$MaDH,
                    'TotalProducts'     =>count($data['products']),
                    'TotalRequest'      =>$total_amount,
                    'DateCreated'       =>date('Y-m-d H:i:s'),
                    'Status'            =>3,
                    'Note'              =>$note,
                    'UserID'            =>$this->user->ID,
                    'Created'           =>date('Y-m-d H:i:s'),
                    'LastEdited'        =>date('Y-m-d H:i:s'),
                    'ExportDate'        =>date('Y-m-d H:i:s')
                );
                $this->db->insert("ttp_report_transferorder",$sql);
                $orderid = $this->db->insert_id();
                $data_his = array(
                    'OrderID'   => $orderid,
                    'Status'    => 3,
                    'Ghichu'    => 'Tạo lệnh chuyển hàng từ  POS',
                    'UserID'    => $this->user->ID,
                    'Thoigian'  => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_transferorder_history',$data_his);
                $this->lib->write_log_data("<b>".$this->user->UserName."</b> tạo yêu cầu chuyển hàng <b class='text-primary'>$MaDH</b>",3);
                $details_insert = array();
                if(count($products)>0){
                    foreach ($products as $row) {
                        $amount = $data['products'][$row->ID];
                        $shipment = $this->db->query("select ID from ttp_report_shipment where ProductsID=$row->ID")->row();
                        if(!$shipment){
                            $shipment = $this->create_shipment_default($row->ID);
                            $this->db->query("update ttp_report_inventory set ShipmentID=$shipment where ProductsID=$row->ID");
                        }else{
                            $shipment = $shipment->ID;
                        }
                        $details_insert[] = "($orderid,$row->ID,$shipment,$WarehouseSender,$amount,$amount,$shipment,$WarehouseReciver,$amount)";
                        $this->release_change_inventory($row->ID,$shipment,$WarehouseSender,$amount);
                    }
                    if(count($details_insert)>0){
                        $query = "insert into ttp_report_transferorder_details(OrderID,ProductsID,ShipmentID,WarehouseID,TotalExport,TotalImport,ShipmentIDImport,WarehouseIDImport,Request) values".implode(',',$details_insert);
                        $this->db->query($query);
                    }
                    @unlink("log/pos/transfer_warehouse/draft_order_".$order.".txt");
                }
                $response = array("error"=>0,"message"=>"Xuất hàng thành công !","id"=>$orderid);
            }else{
                $response = array("error"=>1,"message"=>"Xuất hàng thất bại. Đơn hàng không có sản phẩm");
            }
        }else{
            if($WarehouseReciver==$WarehouseSender){
                $response = array("error"=>1,"message"=>"Xuất hàng thất bại. Kho xuất hàng và kho nhận hàng không được trùng nhau !");
            }
        }
        echo json_encode($response);
    }

    public function drop_products_import(){
        $id = isset($_POST['id']) ? $_POST['id'] : 0 ;
        $check = $this->db->query("select * from ttp_report_transferorder where ID=$id")->row();
        if($check){
            if($check->Status==3){
                $details = $this->db->query("select * from ttp_report_transferorder_details where OrderID=$id")->result();
                if(count($details)>0){
                    foreach($details as $row){
                        $this->change_inventory($row->ProductsID,$row->ShipmentID,$row->WarehouseID,$row->TotalExport);
                    }
                }
                $this->db->query("update ttp_report_transferorder set Status=1,LastEdited='".date('Y-m-d H:i:s')."' where ID=$id");
				$this->lib->write_log_data("<b>".$this->user->UserName."</b> hủy yêu cầu chuyển hàng <b class='text-primary'>$check->OrderCode</b>",1);
                $data_his = array(
                    'OrderID'   => $id,
                    'Status'    => 1,
                    'Ghichu'    => 'Hủy lệnh điều chuyển hàng',
                    'UserID'    => $this->user->ID,
                    'Thoigian'  => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_transferorder_history',$data_his);
                $response = array('error'=>0,'message'=>'Nhập hàng thành công !');
            }else{
                $response = array('error'=>1,'message'=>'Trạng thái phiếu đã bị khóa không thao tác được !');
            }
        }else{
            $response = array('error'=>1,'message'=>'Phiếu chuyển không tồn tại trên hệ thống!');
        }
        echo json_encode($response);
    }

    public function accept_products_import(){
        $id = isset($_POST['id']) ? $_POST['id'] : 0 ;
        $check = $this->db->query("select * from ttp_report_transferorder where ID=$id")->row();
        if($check){
            if($check->Status==3){
                $details = $this->db->query("select * from ttp_report_transferorder_details where OrderID=$id")->result();
                if(count($details)>0){
                    foreach($details as $row){
                        $this->change_inventory($row->ProductsID,$row->ShipmentID,$row->WarehouseIDImport,$row->TotalImport);
                    }
                }
                $this->db->query("update ttp_report_transferorder set Status=4,LastEdited='".date('Y-m-d H:i:s')."',ImportDate='".date('Y-m-d H:i:s')."' where ID=$id");
				$this->lib->write_log_data("<b>".$this->user->UserName."</b> xác nhận nhập hàng thành công từ yêu cầu chuyển hàng <b class='text-primary'>$check->OrderCode</b>",0);
                $data_his = array(
                    'OrderID'   => $id,
                    'Status'    => 4,
                    'Ghichu'    => 'Xác nhận nhập hàng',
                    'UserID'    => $this->user->ID,
                    'Thoigian'  => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_transferorder_history',$data_his);
                $response = array('error'=>0,'message'=>'Nhập hàng thành công !');
            }else{
                $response = array('error'=>1,'message'=>'Trạng thái phiếu đã bị khóa không thao tác được !');
            }
        }else{
            $response = array('error'=>1,'message'=>'Phiếu chuyển không tồn tại trên hệ thống!');
        }
        echo json_encode($response);
    }

    public function create_shipment_default($ProductsID=0){
        $data = array(
            'UserID'    => $this->user->ID,
            'ShipmentCode'=>'Default',
            'ProductsID'=>$ProductsID,
            'DateProduction'=>date('Y-m-d'),
            'DateExpiration'=>date('Y-m-d',time()+24*3600*365*5),
            'Created'   =>date('Y-m-d H:i:s')
        );
        $this->db->insert("ttp_report_shipment",$data);
        $shipmentid = $this->db->insert_id();
        return $shipmentid;
    }

    public function release_change_inventory($ProductsID,$ShipmentID,$WarehouseID,$Amount){
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1");
        if($check_exists){
            if($check_exists->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=Available-$Amount,OnHand=OnHand-$Amount,LastEdited=1 where ID=$check_exists->ID");
            }else{
                $Available = $check_exists->Available-$Amount;
                $OnHand = $check_exists->OnHand-$Amount;
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
            }
        }
    }

    public function change_inventory($ProductsID,$ShipmentID,$WarehouseID,$Amount){
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1");
        if($check_exists){
            if($check_exists->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=Available+$Amount,OnHand=OnHand+$Amount,LastEdited=1 where ID=$check_exists->ID");
            }else{
                $Available = $check_exists->Available+$Amount;
                $OnHand = $check_exists->OnHand+$Amount;
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
            }
        }else{
            $Available = $Amount;
            $OnHand = $Amount;
            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
        }
    }
}
?>