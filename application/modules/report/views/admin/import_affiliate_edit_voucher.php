<form action="<?php echo base_url().ADMINPATH.'/report/partner_affiliate/update_voucher' ?>" method="post">
<input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
<div class="row">
	<label class="col-xs-3 control-label">Giá trị chiết khấu (% hoặc giá trị)</label>
	<label class="col-xs-6 control-label" style='margin-left:15px;'>: <?php echo $data->Code ?></label>
</div>
<div class="row">
	<label class="col-xs-3 control-label">Giá trị chiết khấu (% hoặc giá trị)</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-6">
				<input type="number" class="form-control" name="PercentReduce" id="PercentReduce" value="<?php echo $data->PercentReduce>0 ? $data->PercentReduce : '' ; ?>" placeholder="% chiết khấu" />
			</div>
			<div class="col-xs-6">
				<input type="number" class="form-control" name="PriceReduce" id="PriceReduce" value="<?php echo $data->PriceReduce>0 ? $data->PriceReduce : '' ; ?>" placeholder="Giá trị chiết khấu" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<label class="col-xs-3 control-label">Thời gian hiệu lực</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-6">
				<input type="text" class="form-control required" name="Startday" id="Startday" value="<?php echo $data->Startday ?>" placeholder="Ngày bắt đầu" required />
			</div>
			<div class="col-xs-6">
				<input type="text" class="form-control required" name="Stopday" id="Stopday" value="<?php echo $data->Stopday ?>" placeholder="Ngày kết thúc" required />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<label class="col-xs-3 control-label">Loại hình sử dụng voucher</label>
	<div class="col-xs-9">
		<div class="input-group">
			<div class="col-xs-12">
				<select class="form-control" name="UseOne">
					<option value='0' <?php echo $data->UseOne==0 ? "selected='selected'" : ""; ?>>Sử dụng 1 lần</option>
					<option value='1' <?php echo $data->UseOne==1 ? "selected='selected'" : ""; ?>>Sử dụng nhiều lần</option>
				</select>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-xs-12">
		<button type="submit" class="btn btn-danger"><i class="fa fa-reply-all"></i> Lưu cập nhật</button>
	</div>
</div>
</form>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Startday,#Stopday').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });
</script>