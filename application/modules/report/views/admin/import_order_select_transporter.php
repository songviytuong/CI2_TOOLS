<form action="<?php echo base_url().ADMINPATH.'/report/import_order/apply_transport' ?>" method="POST">
	<div class="row">
		<div class="col-xs-2"><label class="control-label">Chọn nhà vận chuyển</label></div>
		<div class="col-xs-3">
			<select name="TransportID" class="form-control">
				<?php 
				$first = 0;
				$transport = $this->db->query("select ID,Title from ttp_report_transport")->result();
				if(count($transport)>0){
					foreach($transport as $row){
						if($first==0){
							$first = $row->ID;
						}
						echo "<option value='$row->ID'>$row->Title</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="col-xs-2"><label class="control-label">Chọn nhân viên</label></div>
		<div class="col-xs-3">
			<select name="UserID" class="form-control" id="Defaultchange" onchange="loadtransporter(this)">
				<option value="0">Chọn người vận chuyển</option>
				<?php 
				$transporter = $this->db->query("select a.ID,a.FirstName,a.LastName from ttp_user a,ttp_user_transport b where a.ID=b.UserID and b.TransportID=$first")->result();
				if(count($transporter)>0){
					foreach($transporter as $row){
						echo "<option value='$row->ID'>$row->FirstName $row->LastName</option>";
					}
				}
				?>
			</select>
		</div>
		<div class="col-xs-2"><button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> Xác nhận</button></div>
	</div>
	<hr>
	<div class='row' id='info_transporter'></div>
	<hr>
	<h4>Danh sách đơn hàng bàn giao</h4>
	<table class="table">
		<?php 
		if(count($data)>0){
			$i=1;
			$total =0;
			foreach($data as $row){
				$price = $row->Total+$row->Chiphi-$row->Reduce;
				$total = $total+$price;
				$day = date('d/m/Y',strtotime($row->DeliveryTime));
            	$haftday = date('H',strtotime($row->DeliveryTime));
            	$haftstate = $haftday<12 ? "<small style='background: #a2cd3a;color: #FFF;padding: 0px 5px;border-radius: 20px;'>Sáng</small>" : "<small style='background: #f96868;color: #FFF;padding: 0px 5px;border-radius: 20px;'>Chiều</small>" ;
				echo "<tr>
					<td style='width:30px'>$i</td>
					<td style='width: 150px;'>$row->MaDH <br> <small style='margin-top:3px;color:#888;'>".$haftstate.' '.$day."</small></td>
					<td style='vertical-align: middle'>$row->City - $row->District</td>
					<td style='width: 350px;vertical-align: middle'>$row->AddressOrder</td>
					<td class='text-right' style='width:100px;vertical-align: middle'>".number_format($price)."</td>
				</tr>";
				echo "<input type='hidden' name='OrderID[]' value='$row->ID' />";
				$i++;
			}
			echo "<tr><td colspan='4' class='text-right'><b>TỔNG CỘNG</b></td><td class='text-right'><b>".number_format($total)."</b></td></tr>";
		}
		?>
	</table>
</form>

<script>
	
	function loadtransporter(ob){
		var transporter = $(ob).val();
		$("#info_transporter").load("<?php echo base_url().ADMINPATH.'/report/import_order/loadtransporter/' ?>"+transporter);
	}

	$("#Defaultchange").change();
</script>