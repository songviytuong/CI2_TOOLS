<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p>CÔNG TY TNHH TM DV TRẦN TOÀN PHÁT</p>
				<p>246/9 Bình Quới, P.28, Q.Bình Thạnh, TPHCM</p>
				<p>Mã số thuế: 0310717887</p>
			</div>
			<div class="block1_2">
				<a id="print_page"><i class="fa fa-print"></i> In Phiếu</a>
				<p class="print_number_links"><a href="<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/preview/'.$data->ID ?>">In có số lẻ</a> | <a href="<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/preview/'.$data->ID.'?number=none' ?>">In không số lẻ</a></p>
				<h1>Phiếu đề nghị mua hàng</h1>
				<p>Ngày <?php echo date('d/m/Y',strtotime($data->DatePO)) ?></p>
				<p>Số phiếu : <span id="next_MaXK"><?php echo $data->POCode ; ?></span></p>
			</div>
			<div class="block1_3">
				<p>Mẫu số : 02-VT</p>
				<p>(Ban hành theo TT200/2014/TT-BTC ngày 22/12/2014 của bộ trưởng BTC)</p>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Họ tên người đề xuất</li>
				<li>: <?php echo $data->FirstName.' '.$data->LastName ?> </li>
			</div>
			<div class="row">
				<li>Nhà cung cấp</li>
				<li>: <?php echo $data->Production ?></li>
			</div>
			<div class="row">
				<li>Địa chỉ nhà cung cấp</li>
				<li>: <?php echo $data->ProAddress ?></li>
			</div>
			<div class="row">
				<li>Diễn giải chi tiết</li>
				<li>: <?php echo $data->Note ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
    				<th>STT</th>
    				<th>Mã SP</th>
    				<th>Tên sản phẩm</th>
    				<th>ĐVT</th>
    				<th>Số lượng</th>
    				<th>Đơn vị <br>tiền tệ</th>
    				<th>Tỷ giá <br>nguyên tệ</th>
    				<th>Đơn giá <br>nguyên tệ</th>
    				<th>Thành tiền <br>nguyên tệ</th>
    				<th>Thành tiền VNĐ</th>
    				<th>VAT (%)</th>
    			</tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,b.VAT from ttp_report_perchaseorder_details a,ttp_report_products b where a.POID=$data->ID and a.ProductsID=b.ID")->result();
    			$i=1;
    			if(count($details)>0){
    				foreach($details as $row){
    					$amount = isset($_GET['number']) ? number_format($row->Amount) : number_format($row->Amount,3) ;
    					$row->Title = str_replace("\'","'",$row->Title);
    					echo '<tr>
    							<td>'.$i.'</td>
    							<td>'.$row->MaSP.'</td> 
    							<td>'.$row->Title.'</td>
    							<td>'.$row->Donvi.'</td> 
    							<td style="text-align:right">'.$amount.'</td>
								<td>'.$row->Currency.'</td>
    							<td style="text-align:right">'.number_format($row->ValueCurrency).'</td>
    							<td style="text-align:right">'.number_format($row->PriceCurrency).'</td>
    							<td style="text-align:right">'.number_format($row->TotalCurrency).'</td>
    							<td style="text-align:right">'.number_format($row->TotalVND).'</td>
    							<td style="text-align:right">'.$row->VAT.'</td>
    						</tr>';
    					$i++;
    				}
    				echo "<tr><td colspan='9'>Tổng cộng</td><td style='text-align:right'>".number_format($data->TotalPrice)."</td><td></td></tr>";
    			}
				?>
			</table>
		</div>

		<div class="block4">
			<div style="width:25%">
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
				<div><?php echo $this->user->FirstName." ".$this->user->LastName ?></div>
				<p>In từ tools.trantoanphat.com <br><?php echo date("H:i A d/m/Y",time()) ?></p>
			</div>
			<div style="width:25%">
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div style="width:25%">
				<p><?php echo $this->user->DepartmentID==5 ? "Giám đốc quản trị nguồn nhân lực" : "Giám đốc DIVASHOP" ; ?></p>
				<p>(Ký, họ tên)</p>
				<div><?php echo $this->user->DepartmentID==5 ? "N.S.H.T.C.Tâm" : "Nguyễn Thế Đông" ; ?></div>
			</div>
		</div>
	</div>
</div>

<script>
	$("#print_page").click(function(){
		window.print();
	});

	$(document).on('keydown', function(e) {
	    if(e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
	        e.cancelBubble = true;
	        e.preventDefault();
	        e.stopImmediatePropagation();
	    }  
	});
</script>