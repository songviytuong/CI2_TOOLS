<?php
if (!$data)
    return;
$json = $lang == "vi" ? json_decode($data->Data) : json_decode($data->Data_en);

if ($lang == "vi") {
    ?>
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div>
                <label style="padding-top:7px">
                    <input type="radio" name="<?php echo $lang == "vi" ? "Published" : "Published_en"; ?>" value="1" <?php echo $data->Published == 1 ? "checked='checked'" : ''; ?>> &nbsp; Enable &nbsp;
                    <input type="radio" name="<?php echo $lang == "vi" ? "Published" : "Published_en"; ?>" value="0" <?php echo $data->Published == 0 ? "checked='checked'" : ''; ?>> Disable
                </label>
            </div>
        </div>
    </div>
    <?php
}
?>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="<?php echo $lang == "vi" ? "Title" : "Title_en"; ?>" value="<?php echo isset($json->Title) ? $json->Title : ""; ?>" required>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea class="resizable_textarea form-control" name="<?php echo $lang == "vi" ? "Description" : "Description_en"; ?>" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 95px;"><?php echo isset($json->Description) ? $json->Description : ""; ?></textarea>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Link</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" id="first-name" class="form-control col-md-7 col-xs-12" name="<?php echo $lang == "vi" ? "Link" : "Link_en"; ?>" value="<?php echo isset($json->Link) ? $json->Link : ""; ?>">
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="target">Target</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select name="<?php echo $lang == "vi" ? "Target" : "Target"; ?>" class="form-control">
            <option value="_blank" <?= (isset($json->Target) == '_blank') ? "selected=selected" : ""; ?>>_blank</option>
            <option value="_self" <?= (isset($json->Target) == '_self') ? "selected=selected" : ""; ?>>_self</option>
        </select>
    </div>
</div>