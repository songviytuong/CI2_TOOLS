<div class="containner">
    <div class="import_select_progress row" style="padding:0px;border:none;position:relative;">
	    <div class="block1 col-xs-10">
    	    <h1>Tồn kho chi tiết sản phẩm</h1>
	    </div>
	    <div class="block2 col-xs-2">
            <a href="<?php echo ADMINPATH.'/report/warehouse/add_inventory_details' ?>" class="btn btn-danger pull-right"><i class="fa fa-plus"></i> Thêm dữ liệu</a>
        </div>
    </div>
    <div class="import_orderlist">
    	<div class="block3 table_data">
            <?php 
            $result = $this->db->query("select DISTINCT a.MaSP,a.Title,a.ID from ttp_report_inventory_details b,ttp_report_products a where a.ID=b.ProductsID")->result();
            ?>
    		<table>
    			<tr>
    				<th>STT</th>
    				<th>
                        <select onchange="fillproducts(this)" style="background:none;border:none;outline:none;">
                            <option value='0'>Tên sản phẩm</option>
                            <?php 
                            if(count($result)>0){
                                foreach($result as $row){
                                    $selected = $row->ID==$Products ? 'selected="selected"' : '' ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                            }
                            ?>
                        </select>
                    </th>
                    <th>Ghi chú</th>
    				<th>Trọng lượng</th>
                    <th>Kho lưu trữ</th>
                    <th>Trạng thái</th>
    			</tr>
    		<?php 
    		if(count($Data)>0){
    			$i = 1;
    			foreach($Data as $row){
    				echo "<tr>";
    				echo "<td>$i</td>";
    				echo "<td>$row->Title</td>";
                    echo "<td>$row->ProductsNote</td>";
    				echo "<td>".number_format($row->Amount,3)."</td>";
                    echo "<td>$row->MaKho</td>";
                    $color = $row->Published==0 ? 'primary' : 'danger' ;
                    $text = $row->Published==0 ? 'Lấy hàng' : 'Đã lấy' ;
                    echo '<td class="text-center"><a style="cursor:pointer" onclick="getproduct(this,'.$row->ID.')" class="label label-'.$color.'">'.$text.'</a></td>';
    				echo "</tr>";
    				$i++;
    			}
    		}else{
                echo "<tr><td colspan='5'>Không có dữ liệu tình trạng tồn kho của sản phẩm .</td></tr>";
            }
    		?>
    		</table>
    	</div>
	</div>
</div>
<script>
    function get_products_list(ob){
        var keywords = $(ob).val();
        if(keywords.length>2){
            $.post("<?php echo base_url().ADMINPATH.'/report/warehouse/get_products_list' ?>", {keywords: keywords}, function(result){
                $("#auto_complete").html(result).removeClass('hidden');
            });
        }else{
            $("#auto_complete").html('').addClass('hidden');
        }
    }

    function apply_products(ob,id){
        $("#products").html("<b>Bạn đang chọn :</b> "+$(ob).text());
        $("#auto_complete").html('').addClass('hidden');
        $("#keywords").val('');
        $("#productsid").val(id);
    }

    function fillproducts(ob){
        if($(ob).val()==0){
            window.location = "<?php echo base_url().ADMINPATH.'/report/warehouse/inventory_details' ?>";
        }else{
            window.location = "<?php echo base_url().ADMINPATH.'/report/warehouse/inventory_details?product=' ?>"+$(ob).val();
        }
    }

    function getproduct(ob,id){
        $.ajax({
            url: "<?php echo base_url().ADMINPATH.'/report/warehouse/put_products' ?>",
            dataType: "html",
            type: "POST",
            data: "ID="+id,
            success: function(result){
                if(result=='a'){
                    $(ob).removeClass("label-info").addClass("label-danger");
                    $(ob).html("Trả lại");
                }else{
                    $(ob).removeClass("label-danger").addClass("label-info");
                    $(ob).html("Lấy hàng");
                }
            }
        }); 
    }
</script>
<style>
    .auto_complete{position: absolute;
    top: 28px;
    left: 0px;
    right: 0px;
    background: #FFF;
    z-index: 9;
    box-shadow: 1px 3px 5px #f1f1f1;
    border-radius: 0px 0px 3px 3px;
    padding: 15px;
    border: 1px solid #e1e1e1;}
    .auto_complete p{padding:5px 0px;border-bottom:1px solid #eee;}
    .auto_complete p:last-child{border:none;}
</style>