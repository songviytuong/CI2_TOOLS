<?php
$error = isset($_GET['error']) ? $_GET['error'] : "";

echo $error == "user_exists" ? "<script>alert('Username already exists !');</script>" : "";
?>

<div class="">

    <div class="page-title">

        <div class="title_left">

            <h3>SYSTEM GROUP</h3>

        </div>

        <div class="title_right">

            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

                <form action="<?php echo $base_link . 'search'; ?>" method="get">

                    <div class="input-group">

                        <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>

                        <span class="input-group-btn">

                            <button class="btn btn-default" type="button">Go!</button>

                        </span>

                    </div>

                </form>

            </div>

        </div>

    </div>

    <div class="clearfix"></div>

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            <div class="x_panel">

                <div class="x_title">

                    <h2>USERS <small> / Edit user</small></h2>

                    <div class="clearfix"></div>

                </div>

                <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link . "update" ?>" method="post" enctype="multipart/form-data">

                    <div class="x_content">

                        <br />
                        <?php
                        $segment = end($this->uri->segment_array());
                        if ($segment == 'clone') {
                            ?>
                            <input type="hidden" name="Clone" value="1" />
                        <?php } ?>
                        <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">UserType <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="UserType" class="form-control col-md-7 col-xs-12">
                                    <?php
                                    $usertype = $this->lib->get_config_define('position', 'users', 1, 'id');
                                    if (count($usertype) > 0) {
                                        foreach ($usertype as $row) {
                                            $selected = $row->code == $data->UserType ? "selected='selected'" : '';
                                            echo "<option value='$row->code' $selected>$row->name</option>";
                                        }
                                    }
                                    ?>
                                </select> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                Phòng ban <span class="required">*</span>
                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select name="DepartmentID" class="form-control col-md-7 col-xs-12">

                                    <?php
                                    $department = $this->db->query("select * from ttp_department")->result();

                                    if (count($department) > 0) {

                                        foreach ($department as $row) {

                                            $selected = $row->ID == $data->DepartmentID ? "selected='selected'" : '';

                                            echo "<option value='$row->ID' $selected>$row->Code - $row->Title</option>";
                                        }
                                    }
                                    ?>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Username <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="UserName" value="<?= ($segment == 'clone') ? '' : $data->UserName ?>" autocomplete="off">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <input type="password" required="required" class="form-control col-md-7 col-xs-12" name="Password" value="<?= ($segment == 'clone') ? '' : $data->Password ?>" autocomplete="off">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <input type="email" required="required" class="form-control col-md-7 col-xs-12" name="Email" value="<?php echo $data->Email ?>">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Last Name <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="LastName" value="<?php echo $data->LastName ?>">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="FirstName" value="<?php echo $data->FirstName ?>">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Home Page <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <input type="text" required="required" class="form-control col-md-7 col-xs-12" name="HomePage" value="<?php echo $data->HomePage ?>">

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Allow remote <span class="required">*</span>

                            </label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select name="AllowRemote" class="form-control col-md-7 col-xs-12">

                                    <option value='0' <?php echo $data->AllowRemote == 0 ? 'selected="selected"' : ''; ?>>Disable</option>

                                    <option value='1' <?php echo $data->AllowRemote == 1 ? 'selected="selected"' : ''; ?>>Enable</option>

                                </select>

                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Group <span class="required">*</span></label>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="GroupID" class="form-control col-md-7 col-xs-12">
                                    <?php
                                    $this->load->model('groups_model', 'groups');
                                    $groups = $this->groups->defineGroups();
                                    foreach ($groups as $group) {
                                        ?>
                                        <option value='<?= $group['ID']; ?>' <?php echo $data->GroupID == $group['ID'] ? 'selected="selected"' : ''; ?>><?= $group['Name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div>

                                    <label style="padding-top:7px">

                                        <input type="radio" name="Gender" value="1" <?php echo $data->Gender == 1 ? "checked='checked'" : ''; ?>> &nbsp; Male &nbsp;

                                        <input type="radio" name="Gender" value="0" <?php echo $data->Gender == 0 ? "checked='checked'" : ''; ?>> &nbsp; Female &nbsp;

                                        <input type="radio" name="Gender" value="2" <?php echo $data->Gender == 2 ? "checked='checked'" : ''; ?>> &nbsp; Orther &nbsp;

                                    </label>

                                </div>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div>

                                    <label style="padding-top:7px">

                                        <input type="radio" name="Published" value="1" <?php echo $data->Published == 1 ? "checked='checked'" : ''; ?>> &nbsp; Enable &nbsp;

                                        <input type="radio" name="Published" value="0" <?php echo $data->Published == 0 ? "checked='checked'" : ''; ?>> &nbsp; Disable &nbsp;

                                    </label>

                                </div>

                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Chanel</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div>

                                    <label style="padding-top:7px">

                                        <input type="radio" name="Chanel" value="0" <?php echo $data->Channel == 0 ? "checked='checked'" : ''; ?>> &nbsp; Online &nbsp;

                                        <input type="radio" name="Chanel" value="1" <?php echo $data->Channel == 1 ? "checked='checked'" : ''; ?>> MT & GT &nbsp;

                                        <input type="radio" name="Chanel" value="2" <?php echo $data->Channel == 2 ? "checked='checked'" : ''; ?>> Gốm sứ

                                    </label>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- Image Thumb  -->

                    <div class="x_title">

                        <h2>IMAGE <small> / Change thumb user</small></h2>

                        <div class="clearfix"></div>

                    </div>

                    <div class="x_content"> 

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Image</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div>

                                    <label>

                                        <span class="btn btn-default btn-file btn-primary">

                                            Browse <input type="file" name="Image_upload" id="choosefile" />

                                        </span>

                                    </label>

                                </div>



                            </div>

                        </div>

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Preview Image</label>

                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="dvPreview">

                                    <?php
                                    if (file_exists($data->Thumb)) {

                                        echo "<img src='$data->Thumb' style='max-width:100px;max-height:100px' />";
                                    } else {

                                        echo '<span style="padding-top:9px;display:block">No image selected</span>';
                                    }
                                    ?>

                                </div>

                            </div>

                        </div>



                    </div>

                    <!-- Role  -->

                    <div class="x_title">

                        <h2>ROLES <small> / Change user role</small></h2>

                        <div class="clearfix"></div>

                    </div>

                    <div class="x_content"> 

                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Role</label>

                            <div class="col-md-4">

                                <div>

                                    <label>

                                        <select name="RoleID" class="form-control" id="RoleDetails">

                                            <?php
                                            $result = $this->db->query("select * from ttp_role")->result();

                                            if (count($result) > 0) {

                                                foreach ($result as $row) {

                                                    $select = $data->RoleID == $row->ID ? "selected='selected'" : "";

                                                    $status = $row->Published == 1 ? "Enable" : "Disable";

                                                    echo "<option value='$row->ID' $select>$row->Title - $status</option>";
                                                }
                                            }
                                            ?>

                                        </select>

                                    </label>

                                </div>

                            </div>
                            <div class="col-md-1">
                                <button type="submit" class="btn btn-success">Apply</button>
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12" id="recive_ajax">

                                <?php
                                $result = $this->db->query("select * from ttp_sitetree")->result();

                                if (count($result) > 0) {

                                    echo '<table class="table table-striped responsive-utilities jambo_table">';

                                    echo '<thead>

                                        <tr class="headings">

                                            <th>STT</th>

                                            <th>Module </th>

                                            <th>Read</th>

                                            <th>Write</th>

                                            <th>Modify</th>

                                            <th>Delete</th>

                                            <th>Full</th>

                                        </tr>

                                    </thead><tbody>';

                                    $i = 0;

                                    $permission = $data ? json_decode($data->DetailRole, true) : "";

                                    foreach ($result as $row) {

                                        $i++;

                                        $read = '';

                                        $write = '';

                                        $modify = '';

                                        $delete = '';

                                        $full = '';

                                        $temp = 0;

                                        if (is_array($permission) && isset($permission[$row->Classname]) && @in_array('r', $permission[$row->Classname])) {

                                            $read = "checked='checked'";

                                            $temp++;
                                        }

                                        if (is_array($permission) && isset($permission[$row->Classname]) && @in_array('w', $permission[$row->Classname])) {

                                            $write = "checked='checked'";

                                            $temp++;
                                        }

                                        if (is_array($permission) && isset($permission[$row->Classname]) && @in_array('m', $permission[$row->Classname])) {

                                            $modify = "checked='checked'";

                                            $temp++;
                                        }

                                        if (is_array($permission) && isset($permission[$row->Classname]) && @in_array('d', $permission[$row->Classname])) {

                                            $delete = "checked='checked'";

                                            $temp++;
                                        }

                                        if ($temp == 4) {

                                            $full = "checked='checked'";
                                        }
                                        ?>



                                        <tr class="even pointer">

                                            <td class="a-center ">

                                                <?php echo $i; ?>

                                            </td>

                                            <td><?php echo $row->Title; ?></td>

                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="r" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $read ?> /></td>

                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="w" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $write ?> /></td>

                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="m" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $modify ?> /></td>

                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="d" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $delete ?> /></td>

                                            <td><input type='checkbox' class='checkfull' data="<?php echo $row->Classname ?>" <?php echo $full ?> /></td>



                                        </tr>



                                        <?php
                                    }

                                    echo "</tbody></table>";
                                }
                                ?>

                            </div>

                        </div>

                        <div class="form-group">

                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

                                <button type="submit" class="btn btn-success">Save</button>

                            </div>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

</div>



<script>

    $(document).ready(function () {

        $("#RoleDetails").change(function () {

            var ID = $(this).val();

            $.ajax({
                url: "<?php echo base_url() . ADMINPATH . '/home/system_ajax/system_user_loadrole_by_id/' ?>" + ID,
                success: function (result) {

                    $("#recive_ajax").html(result);

                }

            });

        });



        $("#choosefile").change(function () {

            var Fileinput = document.getElementById("choosefile");

            var file = Fileinput.files[0];

            var imageType = /image.*/

            var dvPreview = $(".dvPreview");

            if (file.type.match(imageType)) {

                var reader = new FileReader();

                reader.onload = function (e) {

                    var img = $("<img />");

                    img.attr("style", "max-height:100px;max-width: 100px");

                    img.attr("src", e.target.result);

                    dvPreview.html(img);

                }

                reader.readAsDataURL(file);

            } else {

                console.log("Not an Image");

            }

        });

    });

</script>