<?php

class Site_Ebrochure extends Admin_Controller {

    public $user;
    public $classname = "site_ebrochure";
    public $limit = 20;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('E-Brochure Manager');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_ebrochure")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_ebrochure order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_ebrochure/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_ebrochure/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/site_ebrochure', $data);
        $this->template->render();
    }

    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if ($id != 0) {
            $this->db->delete('ttp_ebrochure', array('ID' => $id));
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add user | Config System');
        $this->load->model('warehouse_model', 'warehouse');
        $WH = $this->warehouse->defineGroupCity();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_ebrochure/addnew',
            'warehouse' => $WH
        );
        $this->template->write_view('content', 'admin/site_ebrochure_edit', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;

        $Store = isset($_POST['store']) ? $_POST['store'] : array();
        $Store = array_unique($Store);
        $Store = json_encode($Store);

        $Start = isset($_POST['Start']) ? $_POST['Start'] : '';
        $End = isset($_POST['End']) ? $_POST['End'] : '';
        $data = array(
            'Name' => $Name,
            'Description' => $Description,
            'Published' => $Published,
            'Created' => date('Y-m-d H:i:s'),
            'LastEdited' => date('Y-m-d H:i:s'),
            'Start' => $Start,
            'End' => $End,
            'Store' => $Store
        );

        if (isset($_FILES['Image_upload'])) {
            if ($_FILES['Image_upload']['tmp_name'] != '') {
                $this->upload_to = "banner_thumb/$ID";
                $data['Image_S'] = $this->upload_image_single();
                $this->delete_file_path($user->Image_S);
            }
        }

        $this->db->insert("ttp_ebrochure", $data);
        redirect(ADMINPATH . '/home/site_ebrochure/');
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_ebrochure where ID=$id")->row();
            if (!$result)
                show_error("Data is empty !");
            $this->load->model('warehouse_model', 'warehouse');
            $WH = $this->warehouse->defineGroupCity();
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_ebrochure/update',
                'data' => $result,
                'warehouse' => $WH
            );
            $this->template->add_title('Edit E-Brochure');
            $this->template->write_view('content', 'admin/site_ebrochure_edit', $data);
            $this->template->render();
        }
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = $_POST['ID'];
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;

        $Store = isset($_POST['store']) ? $_POST['store'] : array();
        $Store = array_unique($Store);
        $Store = json_encode($Store);

        $Start = isset($_POST['Start']) ? $_POST['Start'] : '';
        $End = isset($_POST['End']) ? $_POST['End'] : '';
        if ($Name != '') {
            $ebrochure = $this->db->query("select * from ttp_ebrochure where ID=$ID")->row();
            if ($ebrochure) {

                $data = array(
                    'Name' => $Name,
                    'Description' => $Description,
                    'Published' => $Published,
                    'LastEdited' => date('Y-m-d H:i:s'),
                    'Start' => $Start,
                    'End' => $End,
                    'Store' => $Store
                );

                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['tmp_name'] != '') {
                        $this->upload_to = "banner_thumb/$ID";
                        $data['Image_S'] = $this->upload_image_single();
                        $this->delete_file_path($user->Image_S);
                    }
                }
                $this->db->where("ID", $ID);
                if ($this->db->update("ttp_ebrochure", $data)) {
                    $delete = array(
                        'EbrochureID' => $ID
                    );
                    $this->db->delete('ttp_ebrochure_city', $delete);

                    foreach ($_POST['store'] as $item) {
                        $insert = array(
                            'EbrochureID' => $ID,
                            'CityID' => $item
                        );
                        $this->db->insert('ttp_ebrochure_city', $insert);
                    }
                }
            }
        }
        redirect(ADMINPATH . '/home/site_ebrochure/');
    }

    public function maps($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_ebrochure_flyer where BrochureID=$id")->result();
            $result = ($result) ? $result : array();
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_ebrochure/',
                'data_details' => $result,
                'byID' => $id
            );
            $this->template->write_view('content', 'admin/site_ebrochure_maps', $data);
        }
        $this->template->add_title('Maps E-Brochure');
        $this->template->render();
    }

    public function maps_add($id = 0) {
        $current = $this->uri->segment(5);
        if (isset($current)) {
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_ebrochure/add_maps/' . $current,
            );
            $this->template->write_view('content', 'admin/site_ebrochure_maps_edit', $data);
            $this->template->render();
        }
    }
    
    public function maps_delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if ($id != 0) {
            $this->db->delete('ttp_ebrochure_flyer', array('ID' => $id));
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function maps_edit($id = 0) {
        $current = $this->uri->segment(5);
        if (isset($current)) {
            $result = $this->db->query("select * from ttp_ebrochure_flyer where ID=$current")->row();
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_ebrochure/update_maps/',
                'data' => $result
            );
            $this->template->write_view('content', 'admin/site_ebrochure_maps_edit', $data);
            $this->template->render();
        }
    }

    public function add_maps() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $BrochureID = $this->uri->segment(5);
        $data = array(
            'Name' => $Name,
            'BrochureID' => $BrochureID
        );
        if ($BrochureID) {
            if ($BrochureID) {
                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['tmp_name'] != '') {
                        $this->upload_to = "banner_thumb/$ID";
                        $data['Image'] = $this->upload_image_single();
                        $this->delete_file_path($user->Image);
                    }
                }
                $this->db->insert("ttp_ebrochure_flyer", $data);
            }
        }
        redirect(ADMINPATH . '/home/site_ebrochure/maps/' . $BrochureID);
    }

    public function update_maps() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $coords = isset($_POST['coords']) ? $_POST['coords'] : '';
        $alt = isset($_POST['alt']) ? $_POST['alt'] : '';
        $href = isset($_POST['href']) ? $_POST['href'] : '';
        $ID = $_POST['ID'];
        $data = array(
            'Name' => $Name
        );
        
        $rest = array(
            'coords' => json_encode($coords),
            'alt' => json_encode($alt),
            'href' => json_encode($href)
        );
        
        if ($ID) {
            $ebrochure_flyer = $this->db->query("select * from ttp_ebrochure_flyer where ID=$ID")->row();
            if ($ebrochure_flyer) {
                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['tmp_name'] != '') {
                        $this->upload_to = "banner_thumb/$ID";
                        $data['Image'] = $this->upload_image_single();
                        $this->delete_file_path($user->Image);
                    }
                }
                $data['Data'] = json_encode($rest);
                $this->db->where("ID", $ID);
                $this->db->update("ttp_ebrochure_flyer", $data);
            }
        }
        redirect(ADMINPATH . '/home/site_ebrochure/maps/' . $ebrochure_flyer->BrochureID);
    }

}

?>