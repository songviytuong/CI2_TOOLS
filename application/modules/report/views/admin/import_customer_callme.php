<div class="containner">
		<div class="import_orderlist">
			<div class="block2 row">
	    		<div class="block_2_1 col-xs-12">
	    			<h3 style="margin:0px;" class="pull-left">Danh sách khách hàng cần gọi lại</h3>
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>Tên khách hàng</th>
						<th>Số điện thoại</th>
						<th>Thời gian</th>
						<th>Khách yêu cầu</th>
						<th>Ghi chú xử lý</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					<?php
					if(count($data)>0){
						$i=$start+1;
						foreach ($data as $row) {
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>$row->Name</td>";
							echo "<td>$row->Phone</td>";
							echo "<td>".date('d-m-Y H:i',strtotime($row->Created))."</td>";
							echo "<td title='".$row->Note."'>".$row->Note."</td>";
							echo "<td title='".$row->Response."'>".$row->Response."</td>";
							$status = $row->Success==0 ? 'False' : 'Ok' ;
							echo "<td>$status</td>";
							$icon = $row->Success==0 ? "<i class='fa fa-check-square' style='color:#ddd'></i>" : "<i class='fa fa-check-square' style='color:#090'></i>" ;
							echo "<td>
									<a onclick='has_order_luckynumber(this,$row->ID)' data-state='$row->Success'>$icon</a>
									<a onclick='add_note_luckynumber(this,$row->ID)' title='$row->Note'><i class='fa fa-pencil' style='color:#777;margin-left:5px'></i></a>
							</td>";
							echo "</tr>";
							$i++;
						}
					}
          			?>
				</table>
				<?php
				$httpbuild = http_build_query($_GET);
		        echo str_replace('href=','onclick="nav(this)" data-get="?'.$httpbuild.'" data=',$nav);
				?>
			</div>
		</div>
</div>
<script>
	function has_order_luckynumber(ob,id){
		var status = $(ob).attr('data-state');
		if(status==1){
			var temp=0;
		}else{
			var temp=1;
		}
		$(ob).attr('data-state',temp);
		$(ob).load("<?php echo $base_link.'has_success_callme/' ?>"+id+"/"+temp);
	}

	function add_note_luckynumber(ob,id){
		var note = prompt("Nhập ghi chú khách hàng");
		if (note != null) {
		    var data = {
				Note:note,
				ID:id
			};
			$.ajax({
				type: 'POST',
				url: '<?php echo $base_link.'add_note_callme' ?>',
				dataType: 'json',
				data: 'Data='+JSON.stringify(data),
				success: function (result) {
					if(result.error==0){
						alert("Lưu thành công");
						location.reload();
					}
				}
			});
		}
	}
</script>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;max-width:150px}
	.body_content .containner table tr td:nth-child(5){max-width: 200px}
</style>
