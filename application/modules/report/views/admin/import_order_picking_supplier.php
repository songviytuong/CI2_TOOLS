<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href='<?php echo base_url().ADMINPATH.'/report/import_order' ?>'>TRẠNG THÁI SOẠN HÀNG CHO ĐƠN HÀNG</a></h1>
	    </div>
	    <div class="block2">
		    <a class="btn btn-danger" href="<?php echo base_url().ADMINPATH."/report/import_order/group_supplier" ?>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Yêu cầu lấy hàng</a>
	    </div>
    </div>
    <table class="table table-bordered">
		<tr style='background:#f5f5f5;'>
			<th>STT</th>
			<th>MÃ ĐƠN HÀNG</th>
			<th>TÊN SẢN PHẨM</th>
			<th>DV</th>
			<th>SỐ LƯỢNG</th>
			<th>CHI NHÁNH</th>
			<th style='width:105px'>TRẠNG THÁI</th>
    		<th class='text-center' style='width:120px'>THAO TÁC</th>
		</tr>
		<?php 
		if(count($data)>0){
			$arr_supplier = array();
			$supplier = $this->db->query("select a.Title as SupplierTitle,b.ID,b.Title from ttp_report_production a,ttp_report_branch b where a.ID=b.SupplierID")->result();
			if(count($supplier)>0){
				foreach($supplier as $row){
					$arr_supplier[$row->ID] = "<small>".$row->SupplierTitle."</small> <br>".$row->Title;
				}
			}

			$arr_warehouse = array();
			$warehouse = $this->db->query("select ID,Title from ttp_report_warehouse")->result();
			if(count($warehouse)>0){
				foreach($warehouse as $row){
					$arr_warehouse[$row->ID] = $row->Title;
				}
			}

			$i=0;
			$temp=1;
			$group_order = array();
			$notready_order = array();
			foreach($data as $row){
				if(isset($group_order[$row->ID])){
					$group_order[$row->ID] = $group_order[$row->ID]+1;
				}else{
					$group_order[$row->ID] = 1;
				}
				if($row->Status==0){
					$notready_order[$row->ID] = 1;
				}
			}

			$arr_status = $this->lib->get_config_define('status','picking_status');

			foreach($data as $row){
				$rowspan = $group_order[$row->ID];
				$group = $temp==1 ? "<td rowspan='$rowspan' style='vertical-align:middle'>$row->MaDH</td>" : "" ;
				$i = $temp==1 ? $i+1 : $i ;
				$stt = $temp==1 ? "<td rowspan='$rowspan' style='vertical-align:middle'>$i</td>" : "" ;
				$button = $temp==1 ? "<td rowspan='$rowspan' class='text-center' style='vertical-align:middle'><a class='btn-sm btn-danger' data-state='0' onclick='readytopick(this,$row->ID)'><i class='fa fa-check-square-o' aria-hidden='true'></i> Xác nhận</a></td>" : "" ;
				if(isset($arr_supplier[$row->BranchID]) || isset($arr_warehouse[$row->WarehouseID])){
					$branch_title = $row->BranchID>0 ? $arr_supplier[$row->BranchID] : $arr_warehouse[$row->WarehouseID];
				}else{
					$branch_title = '--';
				}
				$color = $row->Status==0 ? "text-danger" : "text-success" ;
				$icon = $row->Status==0 ? '<small style="background:#f5f5f5;padding:3px 10px;border-radius:10px;">' : '<small style="background:#46be8a;color:#FFF;padding:3px 10px;border-radius:10px;">' ;
				if(isset($notready_order[$row->ID])){
					$button = $temp==1 ? "<td rowspan='$rowspan' class='text-center' style='vertical-align:middle;'>Chờ xác nhận</td>" : "";
				}
				if($temp<$rowspan){
					$temp++;
				}else{
					$temp=1;
				}
				echo "<tr>
					$stt
					$group
					<td>$row->Title</td>
					<td>$row->Donvi</td>
					<td class='text-right'><span>".number_format($row->Amount,2)."</span><br><a onclick='changeamountpicking(this,$row->DetailsID,".number_format($row->Amount,2).")' style='text-decoration:underline'>Thay đổi</a></td>
					<td>".$branch_title."</td>
					<td class='$color text-center' style='vertical-align:middle'>$icon".$arr_status[$row->Status]."</small></td>
					$button
				</tr>";
				
			}
		}else{
			echo '<tr><td colspan="8">Không tìm thấy dữ liệu theo yêu cầu .</td></tr>';
		}
		?>
	</table>
</div>
<script>
	var link = "<?php echo $base_link ?>";

	function readytopick(ob,orderid){
		$(ob).addClass('saving');
		$.ajax({
            url: link+"readytopick",
            dataType: "html",
            type: "POST",
            data: "ID="+orderid,
            success: function(result){
                $(ob).removeClass('saving');
                if(result=='true'){
                	location.reload();
                }else{
                    alert("Không thực thi được yêu cầu .");
                }
            },error:function(result){
                console.log(result.responseText);
            }
        });
	}

	var sitebar_open = "true";
    $("#closesitebar").click(function(){
        sitebar_open = "false";
        $(".sitebar").addClass("closesitebar");
        $(this).addClass("closesitebar");
        $("#opensitebar").addClass("opensitebar");
        $(".containner").addClass("opensitebar");
        $(".copyright").addClass("opensitebar");
        $(".warning_message").addClass("opensitebar");
    });

    function changeamountpicking(ob,ID,Amount){
      var getValue = prompt("Nhập số lượng cần thay đổi",Amount);
      if(getValue!=null){
        $.ajax({
            dataType: "html",
            data: "ID="+ID+"&Amount="+getValue,
            url: link+'change_real_picking',
            method: 'POST',
            success: function(result) {
                if(result=="true"){
                    $(ob).parent('td').find('span').html(getValue);
                    alert("Thay đổi thành công");
                }else{
                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
                }
            },
            error:function(result){
                alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu .");
            }
        });
      }
    }
</script>