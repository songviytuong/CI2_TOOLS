<style>
    .body_content .containner .black .box_inner .block2_inner {
        overflow: auto;
        padding: 15px 20px 20px 20px;
    }
</style>
    <table id="myTable" style="border-collapse:collapse;">
        <tr>
            <th width="30%" colspan="2">
                <div style="vertical-align: middle; float:left; margin-top:5px;">Mã vận đơn: <font color="red" size="5"><?=$OrderCode;?></font></div>
                <div style="float:right;" class="btn btn-success btn-sm sync"><i class="fa fa-refresh" aria-hidden="true"></i> Đồng bộ</div>
            </th>
  </tr>
  <tr>
    <td width="45%">Đối tác vận chuyển</td>
    <td><?=$Company;?></td>
  </tr>
  <tr>
    <td>Mã tham chiếu TTP</td>
    <td><?=$ClientOrderCode;?></td>
  </tr>
  
  <tr>
    <td>Cước phí</td>
    <td><?=$TotalFee;?> VNĐ</td>
  </tr>
  
  <tr>
    <td>GHN thu của KH</td>
    <td><?=$CoDAmount;?> VNĐ</td>
  </tr>
  
  <tr>
    <td>Hình thức</td>
    <td><?=$Payment;?></td>
  </tr>
  
  <tr>
    <td>Trạng thái</td>
    <td><span style="font-weight:bold;color:green;font-style: italic" id="currentStatus"><?=$CurrentStatus;?></span> <div style="float:right;"><a style="font-size:90%;" class="error_more hidden">Xem thêm <i class="fa fa-angle-down"></i></a></div></td>
  </tr>
  <tr class="dis hidden bg-danger">
      <td colspan="2" class="text-center text-danger note" style="font-style: italic"></td>
  </tr>
  <tr>
    <td>Ngày tạo</td>
    <td><?=$Created;?></td>
  </tr>
  
  <tr>
    <td>Đã đồng bộ</td>
    <td id="currentSync"><?=$Sync?></td>
  </tr>
</table>

<script type="text/javascript">
    $().ready(function(){
        var crr = "<?=$Crr;?>";
        setTimeout(function(){
            $('.sync').click();
            if(crr == "Delivery"){
                $('.sync').addClass("disabled");
            }
            if(crr == "Cancel"){
                $('.error_more').removeClass("hidden");
            }
        }, 100);
        $('.error_more').click(function(){
            $('.dis').toggleClass("hidden");
            $(".error_more i").removeClass('fa-angle-up');
            $(".error_more i").addClass('fa-angle-down');
            if($(this).next("div").is(":visible")){
                $(this).next("div").slideUp("slow");
            } else {
                $(this).find('i').removeClass('fa-angle-down')
                $(this).find('i').addClass('fa-angle-up');
                $(".error_more").slideUp("slow");
                $(this).next("div").slideToggle("slow");
            }
        });
        
        $('.sync').click(function(){
            $(this).text("Đang đồng bộ...");
            $(this).addClass("saving");
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/report/import_order' ?>/updateCurrentStatus",
                dataType: "JSON",
                type: "GET",
                context: this,
                data: "OrderCode=<?=$OrderCode;?>&OrderID=<?=$OrderID;?>",
                success: function(result){
                    if(result.updated==="True"){
                        $(this).removeClass('saving');
                        $(this).html("<i class='fa fa-refresh' aria-hidden='true'></i> Đồng bộ");  
                        if(result.Cancel == "True"){
                            $('.error_more').removeClass("hidden");
                            $('.note').text("Lý do: " + result.Note);
                        }
                        $('#currentStatus').text(result.CurentStatus);
                        $('#currentSync').text(result.Sync);
                    }else if(result.updated=="NotConnect"){
                        $(this).removeClass('saving');
                        $(this).addClass('break');
                        $(this).html("Lỗi kết nối");  
                    }
                }
            });
        });
    });
</script>