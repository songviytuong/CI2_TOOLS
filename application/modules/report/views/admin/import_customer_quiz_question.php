<div class="containner">
	<div class="import_orderlist">
		<div class="block2 row">
    		<div class="block_2_1 col-xs-12">
    			<h3 style="margin:0px;" class="pull-left">Danh sách câu hỏi cho quiz game</h3>
    			<a onclick="addnew(0)" class="pull-right btn btn-danger" style="margin:0px;"><i class='fa fa-plus'></i> Thêm câu hỏi</a>
    		</div>
    		<div class="clear"></div>
    	</div>
    	<div class="block3 table_data">
			<table id="table_data">
				<tr>
					<th>STT</th>
					<th>Câu hỏi</th>
					<th>Cấp độ</th>
					<th>Điểm số</th>
					<th style='width: 150px;'>Thao tác</th>
				</tr>
				<?php
				if(count($data)>0){
					$i=$start+1;
					foreach ($data as $row) {
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->Level</td>";
						echo "<td class='text-success'>+ $row->Point</td>";
						echo "<td><a onclick='loadeditform($row->ID)'><i class='fa fa-pencil'></i> Edit</a> | <a onclick='remove_question(this,$row->ID)'><i class='fa fa-trash-o'></i> Remove</a></td>";
						echo "</tr>";
						$i++;
					}
				}else{
					echo "<tr><td colspan='5'>Không tìm thấy dữ liệu</td></tr>";
				}
      			?>
			</table>
			<?php
			$httpbuild = http_build_query($_GET);
	        echo str_replace('href=','onclick="nav(this)" data-get="?'.$httpbuild.'" data=',$nav);
			?>
		</div>
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<script>
	function loadeditform(id){
		$(".over_lay").show();
    	$(".over_lay .box_inner .block1_inner h1").html("Điều chỉnh câu hỏi");
    	$(".over_lay .box_inner .block2_inner").load("<?php echo $base_link ?>edit_question/"+id);
    	$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function addnew(id){
		$(".over_lay").show();
    	$(".over_lay .box_inner .block1_inner h1").html("Thêm câu hỏi mới");
    	$(".over_lay .box_inner .block2_inner").load("<?php echo $base_link ?>new_question/"+id);
    	$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	$("#close_overlay").click(function(){
		location.reload();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function remove_question(ob,id){
		var r = confirm("Bạn có chắc muốn xóa câu hỏi này ?");
		if (r == true) {
			$.ajax({
				url: "<?php echo base_url().ADMINPATH.'/report/import_customer/' ?>remove_question",
	            type: "POST",
	            data: "ID="+id,
	            dataType: "text",
	            success: function(result){
	            	console.log(result);
	            	$(ob).parent('td').parent('tr').remove();
	            }
			});
		}
	}
</script>
<style>
	.body_content .containner .black .box_inner .block2_inner{max-height: 999px}
</style>