<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url() ?>">
        <?php 
        $segment = $this->uri->segment(4);
        echo $segment=="lapphieuxuatkho" ? "" : '<meta name="viewport" content="width=device-width, initial-scale=1">' ;
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="image/x-icon" href="public/admin/images/favicon.png" rel="shortcut icon">
	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="public/admin/css/bootstrap.min.css" rel="stylesheet">
	    <link href="public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="public/admin/css/animate.min.css" rel="stylesheet">
	    <!-- Custom styling plus plugins -->
        <link href="public/admin/css/template.css" rel="stylesheet">
        <link href="public/admin/css/person_study.css" rel="stylesheet">
	    <link href="public/admin/css/style_report.css" rel="stylesheet">
        <link href="public/admin/css/multiple-select.css" rel="stylesheet">
        <script src="public/admin/js/jquery.min.js"></script>
        <script src="public/admin/js/bootstrap.min.js"></script>
        <!-- chart js -->
        <script src="public/admin/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="public/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="public/admin/js/moment.min2.js"></script>
        <script type="text/javascript" src="public/admin/js/datepicker/daterangepicker.js"></script>
        <!-- flot js -->
        <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.pie.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.orderBars.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.time.min.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/date.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.spline.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.stack.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/curvedLines.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.resize.js"></script>
        <script type="text/javascript" src="public/admin/js/flot/jquery.flot.axislabels.js"></script>
        <script type="text/javascript" src="public/admin/js/multiple-select.js"></script>
        <script type="text/javascript" src="public/admin/js/md5.min.js"></script>
        <script src="public/admin/js/ERPPlugin.js"></script>
        <?php echo $_styles; ?>
        <?php echo $_title; ?>
    </head>
    <body>
        <div class="body_content">
            <?php echo $header; ?>
            <?php echo $sitebar; ?>
            <?php echo $content; ?>
            <div class="version">Develop by Mr.Binh
              <span>Report Tools version v1.1 last update 09/2017 - &copy; Auchan Group</span>
            </div>
        </div>
        <?php echo $_scripts; ?>
        <script type="text/javascript">
            $(document).bind('contextmenu', function(e){return false;});
            function nav(ob){
                var get = $(ob).attr('data-get');
                var href= $(ob).attr('data');
                window.location= href+get;
            }
        </script>
</body>
</html>

