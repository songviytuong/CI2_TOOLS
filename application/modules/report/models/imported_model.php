<?php

class Imported_model extends CI_Model {

    var $tablename = 'auc_import';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function getCounterDataImport() {
        $query = $this->db->query('SELECT Count(DISTINCT SKU) as CNT FROM '.$this->tablename.' ORDER BY SKU');
        $counter = $query->row()->CNT;
        return $counter;
    }

    function getDataImport() {
        $this->db->distinct();
        $this->db->select('ID,SKU');
        $result = $this->db->get($this->tablename)->result();
        return $result;
    }

    function getLastestImport() {
        $this->db->select('max(Imported) as Lastest');
        $result = $this->db->get($this->tablename)->row()->Lastest;
        return $result;
    }

}
