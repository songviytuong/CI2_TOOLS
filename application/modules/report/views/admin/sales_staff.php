<?php 
$active_vs = 1;
$startday = $this->session->userdata("startday");
$startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
$stopday = $this->session->userdata("stopday");
$stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

$numday = $this->lib->get_nume_day($startday,$stopday);

$startday_vs = $this->session->userdata("startday_vs");
$startday_vs = $startday_vs!='' ? $startday_vs : date('Y-m-d',strtotime($startday)-($numday*3600*24)) ;
$stopday_vs = $this->session->userdata("stopday_vs");
$stopday_vs = $stopday_vs!='' ? $stopday_vs : date('Y-m-d',strtotime($startday)) ;


$Orderstatus = $this->session->userdata("status_report");
$bonus_fillter = $Orderstatus=='' || $Orderstatus==0 ? '' : ' and a.Status=0' ;
$Orderstatus = $Orderstatus=='' || $Orderstatus==0 ? 0 : $Orderstatus ;


$ordertype = $this->session->userdata("ordertype_report");
$ordertype = $ordertype=='' ? 0 : $ordertype ;

$filltype = $this->session->userdata("filltype_report");
$filltype = $filltype=='' ? 0 : $filltype ;

$current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0 ;
$view_status = $current_view==0 ? " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'" : " and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday'" ;
$view_status_vs = $current_view==0 ? " and date(a.Ngaydathang)>='$startday_vs' and date(a.Ngaydathang)<='$stopday_vs'" : " and date(a.HistoryEdited)>='$startday_vs' and date(a.HistoryEdited)<='$stopday_vs'" ;

/*
*********************************
*	Define variable				*
*								*
*********************************
*/
$point_target = array();
$point_sales = array();
$point_history = array();

$team = $this->db->query("select a.*,b.UserName from ttp_report_team a,ttp_user b where a.UserID=b.ID and a.ID !=4 and a.ID!=5")->result();
$userlist = $this->db->query("select UserName,ID from ttp_user where UserType=1 order by UserName ASC")->result();
$arr_team = array();
$arr_team_vs = array();
$arr_user = array();
$arr_user_vs = array();
$arr_userlist = array();
$div_fillter_by_user = array('<li><a onclick="checkalluser(this)" class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Check all</a><a class="text-danger" onclick="uncheckalluser(this)"><i class="fa fa-times" aria-hidden="true"></i> Uncheck all</a></li>');

if(count($team)>0){
	foreach($team as $row){
		$temp = json_decode($row->Data,true);
		if(count($temp)>0){
			foreach($temp as $item){
				$arr_user[$item]['TeamID']=$row->ID;
				$arr_user[$item]['Total']=0;
				$arr_user[$item]['UserName']=$row->UserName;

				$arr_user_vs[$item]['TeamID']=$row->ID;
				$arr_user_vs[$item]['Total']=0;
				$arr_user_vs[$item]['UserName']=$row->UserName;
			}
		}
	}
}

if(count($userlist)>0){
	foreach($userlist as $row){
		$arr_userlist[$row->ID] = $row->UserName;
		$div_fillter_by_user[] = "<li><input type='checkbox' checked='checked' onchange='onlyshowme(this)' data='$row->ID' />".$row->UserName."</li>";
	}
}

/*
*********************************************
*	Excute and set variable	in first range	*
*											*
*********************************************
*/

$total_order = $this->db->query("select a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.CityID,a.Ngaydathang,a.UserID from ttp_report_order a,ttp_user b where a.UserID=b.ID $view_status and a.CustomerID!=9996 and a.CustomerID!=0 and a.OrderType=$ordertype $bonus_fillter")->result();
if(count($total_order)>0){
	foreach($total_order as $row){
		if(isset($arr_user[$row->UserID])){
			$ofteam = $arr_user[$row->UserID]['TeamID'];
			if(isset($arr_team[$ofteam])){
				$arr_team[$ofteam] = $arr_team[$ofteam]+($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}else{
				$arr_team[$ofteam] = ($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}
			$arr_user[$row->UserID]['Total'] = $arr_user[$row->UserID]['Total']+($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
		}
	}	
}

/*
*********************************************
*	Excute and set variable	in second range	*
*											*
*********************************************
*/

$total_order_vs = $this->db->query("select a.ID,a.Status,a.CustomerType,a.CustomerID,a.SoluongSP,a.Total,a.Chietkhau,a.Chiphi,a.Reduce,a.CityID,a.Ngaydathang,a.UserID from ttp_report_order a,ttp_user b where a.UserID=b.ID $view_status_vs and a.CustomerID!=9996 and a.CustomerID!=0 and a.OrderType=$ordertype $bonus_fillter")->result();
if(count($total_order_vs)>0){
	foreach($total_order_vs as $row){
		if(isset($arr_user[$row->UserID])){
			$ofteam = $arr_user[$row->UserID]['TeamID'];
			if(isset($arr_team_vs[$ofteam])){
				$arr_team_vs[$ofteam] = $arr_team_vs[$ofteam]+($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}else{
				$arr_team_vs[$ofteam] = ($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
			}
			$arr_user_vs[$row->UserID]['Total'] = $arr_user_vs[$row->UserID]['Total']+($row->Total-$row->Reduce-$row->Chietkhau+$row->Chiphi);
		}
	}
}

?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
	<div class="select_progress row">
	    <div class="block1 col-xs-12 col-sm-6 col-md-6">
	    	<h3 style="margin:0px;margin-bottom: 10px;">Báo cáo theo nhân viên</h3>
	    </div>
	    <div class="block2 col-xs-12 col-sm-6 col-md-6">
		    <div class="dropdown">
		    	<a class='value_excute pull-right'><span><?php echo date('d/m/Y',strtotime($startday))." - ".date('d/m/Y',strtotime($stopday)) ?></span> <b class="caret"></b></a>
		    	<p class="value_excute_vs hidden-xs hidden-sm hidden" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>So sánh với : <span><?php echo date('d/m/Y',strtotime($startday_vs))." - ".date('d/m/Y',strtotime($stopday_vs)) ?></span></p>
		    	<div class="box_dropdown">
		    		<div id="reportrange" class="list_div">
				        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
				        <span></span> <b class="caret"></b>
				    </div>
				    <div class="center_div list_div">
				    	<span><input type='checkbox' id='active_vs' <?php echo $active_vs==1 ? "checked='checked'" : "" ; ?> />So sánh với</span>
				    	<select id="vs_selectbox" <?php echo $active_vs==1 ? "style='opacity:1'" : "style='opacity:0.6'" ; ?>>
				    		<option value="tuychinh">Tùy chỉnh</option>
				    		<option value="lastmonth">Tháng trước</option>
				    		<option value="last7day">7 ngày trước</option>
				    		<option value="last30day">30 ngày trước</option>
				    	</select>
				    </div>
				    <div id="reportrange1" class="list_div" <?php echo $active_vs==1 ? "style='display:block'" : "style='display:none'" ; ?>>
				        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
				        <span></span> <b class="caret"></b>
				    </div>
				    <div class='compare list_div'>
				    	<a id="showcompare" class="btn btn-danger"><i class="fa fa-exchange"></i> Áp dụng</a>
				    	<a id="cancel_showcompare" class="btn btn-default">Hủy</a>
				    </div>
		    	</div>
		    </div>
	    </div>
    </div>
    <div class="report_sales_fill">
    	<div class="block1">
    		<div>
	    		<select id="orderstatus">
	    			<option value="0" <?php echo $Orderstatus==0 ? "selected='selected'" : '' ; ?>>Tất cả đơn hàng</option>
	    			<option value="1" <?php echo $Orderstatus==1 ? "selected='selected'" : '' ; ?>>Đơn hàng thành công</option>
	    		</select>
	    	</div>
    		<div>
	    		<select id="viewstatus" onchange="changeview(this)">
	    			<option value="0" <?php echo $current_view==0 ? "selected='selected'" : '' ; ?>>Ngày đơn hàng</option>
	    			<option value="1" <?php echo $current_view==1 ? "selected='selected'" : '' ; ?>>Ngày cập nhật</option>
	    		</select>
    		</div>
    		<div>
	    		<input type='radio' value="0" <?php echo $filltype==0 ? 'checked="checked"' : '' ; ?> onclick="changeradio(this)" /> Theo nhóm
	    		<input type='radio' value="1" <?php echo $filltype==1 ? 'checked="checked"' : '' ; ?> onclick="changeradio(this)" /> Theo cá nhân
    		</div>
    	</div>

    	<div class="block2 select_day_week_month">
    		
    	</div>

    	<div class="row">
	    	<div class="col-xs-12">
	    		<div class="table_data">
	    		<?php 
	    		$target = $this->db->query("select * from ttp_report_targets_year where Year=".date('Y'))->row();
    			$target = $target ? json_decode($target->DataDepartment,true) : array() ;
    			?>
	    		<table>
	    			<tr>
	    				<th class="width150" style="position: relative"><?php echo $filltype==0 ? "Tên nhóm" : '<span>Nhân viên <i style="float:right;border:1px solid #E1e1e1;background:#FFF;padding:2px 5px;cursor:pointer" class="fa fa-sort-desc" aria-hidden="true" onclick="showboxfillter(this)"></i></span>' ; ?>
							<div class="fillter_user" style="display:none"><ul><?php echo implode('',$div_fillter_by_user); ?></ul></div>
	    				<th>Chỉ tiêu</th>
	    				<th>Thực hiện</th>
	    				<th>Lịch sử</th>
	    				<th>% tăng trưởng</th>
	    				<th>% chỉ tiêu</th>
	    				<th>Chênh lệch / chỉ tiêu</th>
	    			</tr>
	    			<?php 
	    			$total_target = 0;
	    			$total_sale = 0;
	    			$total_his = 0;
	    			$total_range = 0;
	    			$total_tangtruong = 0;
	    			$arr_teamleader = array();
	    			if($filltype==0){
		    			if(count($team)>0){
		    				foreach($team as $row){
		    					$target_team = isset($target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$row->ID]['Total']) ? $target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$row->ID]['Total'] : 0 ;	    				
		    					
		    					$sales = isset($arr_team[$row->ID]) ? $arr_team[$row->ID] : 0 ;
		    					$sales_his = isset($arr_team_vs[$row->ID]) ? $arr_team_vs[$row->ID] : 0 ;
		    					$percent_sale = $target_team==0 ? 0 : round($sales/($target_team/100),1);
		    					$range_sale = $sales-$target_team;
		    					$total_sale+= $sales;
		    					$total_his+= $sales_his;
		    					$total_target+=$target_team;
		    					$total_range+=$range_sale;

		    					$point_target[$row->ID] = $target_team;
		    					$point_sales[$row->ID] = $sales;
		    					$point_history[$row->ID] = $sales_his;
		    					$tangtruong = $sales_his==0 ? 0 : (($sales/$sales_his)-1)*100;
		    					$icon_tangtruong = $tangtruong<0 ? '<i class="fa fa-caret-down text-danger"></i>' : '<i class="fa fa-caret-up text-success"></i>' ;
		    					$text_tangtruong = $tangtruong<0 ? 'text-danger' : 'text-success' ;
		    					$arr_teamleader[] = $row->Title;
		    					echo "<tr>";
		    					echo "<td>$row->Title <br>($row->UserName)</td>";
		    					echo "<td>".number_format($target_team)."</td>";
		    					echo  "<td>".number_format($sales)."</td>";
		    					echo "<td>".number_format($sales_his)."</td>";
		    					echo "<td class='$text_tangtruong'>".round($tangtruong,2)." % $icon_tangtruong</td>";
		    					echo "<td>$percent_sale %</td>";
		    					echo "<td>".number_format($range_sale)."</td>";
		    					echo "</tr>";
		    				}
		    			}
		    		}else{
		    			foreach($arr_user as $key=>$value){
		    				$teamID = isset($value['TeamID']) ? $value['TeamID'] : 0 ;
	    					$target_person = isset($target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$teamID]['Person'][$key]['Total']) ? $target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$teamID]['Person'][$key]['Total'] : 0 ;
	    					
	    					$sales = isset($value['Total']) ? $value['Total'] : 0 ;
	    					$sales_his = isset($arr_user_vs[$key]['Total']) ? $arr_user_vs[$key]['Total'] : 0 ;
	    					$percent_sale = ($target_person/100)==0 ? 0 : round($sales/($target_person/100),1);
	    					$range_sale = $sales-$target_person;
	    					$total_sale+= $sales;
	    					$total_his+= $sales_his;
	    					$total_target+=$target_person;
	    					$total_range+=$range_sale;

	    					$sales_team = isset($arr_team[$teamID]) ? $arr_team[$teamID] : 0 ;
	    					$point_sales[$teamID] = $sales_team;
	    					$target_team = isset($target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$teamID]['Total']) ? $target['type2']['Department'][2]['Month'][(int)date('m')]['Team'][$teamID]['Total'] : 0 ;
	    					$point_target[$teamID] = $target_team;
	    					$his_team = isset($arr_team_vs[$teamID]) ? $arr_team_vs[$teamID] : 0 ;
	    					$point_history[$teamID] = $his_team;
	    					$tangtruong = $sales_his==0 ? 0 : (($sales/$sales_his)-1)*100;
	    					$icon_tangtruong = $tangtruong<0 ? '<i class="fa fa-caret-down text-danger"></i>' : '<i class="fa fa-caret-up text-success"></i>' ;
	    					$text_tangtruong = $tangtruong<0 ? 'text-danger' : 'text-success' ;
	    					$name = isset($arr_userlist[$key]) ? $arr_userlist[$key] : '--' ;
	    					echo "<tr class='userdata userdata$key'>";
	    					echo "<td>".$name."</td>";
	    					echo "<td>".number_format($target_person)."</td>";
	    					echo  "<td>".number_format($sales)."</td>";
	    					echo "<td>".number_format($sales_his)."</td>";
	    					echo "<td class='$text_tangtruong'>".round($tangtruong,2)." % $icon_tangtruong</td>";
	    					echo "<td>$percent_sale %</td>";
	    					echo "<td>".number_format($range_sale)."</td>";
	    					echo "</tr>";
		    			}
		    		}
	    			$percent_total = $total_target==0 ? 0 : round($total_sale/($total_target/100),1);
	    			$total_tangtruong = $total_his==0 ? 0 : (($total_sale/$total_his)-1)*100;
	    			$icon_tangtruong = $total_tangtruong<0 ? '<i class="fa fa-caret-down text-danger"></i>' : '<i class="fa fa-caret-up text-success"></i>' ;
					$text_tangtruong = $total_tangtruong<0 ? 'text-danger' : 'text-success' ;

	    			?>
	    			<tr>
	    				<td class="width150">Tổng cộng</td>
	    				<td><?php echo number_format($total_target) ?></td>
	    				<td><?php echo number_format($total_sale) ?></td>
	    				<td><?php echo number_format($total_his) ?></td>
	    				<td class="<?php echo $text_tangtruong ?>"><?php echo round($total_tangtruong,2).' % '.$icon_tangtruong ?></td>
	    				<td><?php echo $percent_total.' %'; ?></td>
	    				<td><?php echo number_format($total_range) ?></td>
	    			</tr>
	    		</table>
	    		</div>
	    	</div>
	    </div>
    </div>
    <div class="chart linechart" style="overflow:hidden">
		<div id="columnchart_material" style="width: 100%; "></div>
	</div>
	<div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<!-- datepicker -->
<?php 
	$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
	$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
	
?>
<script type="text/javascript">
	$(".table_data table tr td").click(function(){
		$(".fillter_user").hide();
	});

	function showboxfillter(ob){
		$(".fillter_user").toggle();
	}

	function onlyshowme(ob){
		$(".userdata").hide();
		$(".fillter_user ul li input").each(function(){
			if(this.checked===true){
				var dataid = $(this).attr('data');
				$(".userdata"+dataid).show();
				console.log(".userdata"+dataid);
			}
		});
	}

	function checkalluser(ob){
		var parent = $(ob).parent('li').parent('ul');
		parent.find('input[type="checkbox"]').prop("checked",true);
		onlyshowme();
	}

	function uncheckalluser(ob){
		var parent = $(ob).parent('li').parent('ul');
		parent.find('input[type="checkbox"]').prop("checked",false);
		onlyshowme();
	}

    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /****  reportrange 2  ****/
        var cb1 = function (start, end, label) {
            $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        var optionSet2 = {
            startDate: moment().startOf('month'),
            endDate: moment(),
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 120
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange1 span').html(moment().startOf('month').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange1').daterangepicker(optionSet2, cb1);

        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".value_excute span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
		});

		$('#reportrange1').on('apply.daterangepicker', function(ev, picker) {
		    $(".value_excute_vs span").html(picker.startDate.format('DD/MM/YYYY')+" - "+picker.endDate.format('DD/MM/YYYY'));
		    $(".value_excute_vs").show();
		});
    });
</script>
<!-- /datepicker -->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	
	<?php 
	if(count($team)>0){
		$arr_chart = array(
			0=>array('Nhóm nhân viên telesales','Chỉ tiêu','Lịch sử','Doanh số')
		);
		$i=1;
		foreach($team as $key=>$row){
			$arr_chart[$i] = isset($arr_teamleader[$key]) ? array($arr_teamleader[$key]) : array();
			$row_point_target = isset($point_target[$row->ID]) ? (int)$point_target[$row->ID] : 0 ;
			$arr_chart[$i][] = $row_point_target;
			$row_point_history = isset($point_history[$row->ID]) ? $point_history[$row->ID] : 0 ;
			$arr_chart[$i][] = $row_point_history;
			$row_point_sales = isset($point_sales[$row->ID]) ? $point_sales[$row->ID] : 0 ;
			$arr_chart[$i][] = $row_point_sales;
			$i++;
		}
	}
	?>
	
	google.charts.load('current', {'packages':['bar']});
  	google.charts.setOnLoadCallback(drawChart);
  	function drawChart() {
    	var data = google.visualization.arrayToDataTable([
	      	<?php 
	      	$i=1;
	      	foreach($arr_chart as $row){
	      		echo $i==count($arr_chart) ? json_encode($row) : json_encode($row).',';
	      		$i++;
	      	}
	      	?>
	    ]);

	    var options = {
      		chart: {
        	title: 'Biểu đồ chỉ tiêu , lịch sử , doanh số của từng nhóm kinh doanh .',
        	subtitle: 'Từ ngày <?php echo date('d/m/Y',strtotime($startday))." đến ngày ".date('d/m/Y',strtotime($stopday)) ?>',
      	},
      	bars: 'vertical',
      	vAxis: {format: 'decimal'},
      	height: 400,
      	colors: ['#1b9e77', '#d95f02', '#4572A7']
    	};

	    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
    	chart.draw(data, options);
  	}
	
</script>
<!-- /flot -->

<!-- ajax compare -->
<script>
	var baselink = $("#baselink_report").val();
	$("#vs_sosanh").change(function(){
		var data = $(this).val();
		var kpi = $("#value_sosanh").val();
		var status = $("#orderstatus").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+data+"&KPI="+kpi+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	$("#orderstatus").change(function(){
		var data = $(this).val();
		window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/set_fillter/status_report/' ?>"+data;
	});

	$("#value_sosanh").change(function(){
		var data = $(this).val();
		var status = $("#orderstatus").val();
		var targets = $("#vs_sosanh").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+targets+"&KPI="+data+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	function changeordertype(ob){
		var dataordertype = $(ob).val();
		window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/set_fillter/ordertype_report/' ?>"+dataordertype;
	}

	function changeradio(ob){
		var filltype = $(ob).val();
		window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/set_fillter/filltype_report/'; ?>"+filltype;
	}

	function changeview(ob){
		var view = $(ob).val();
		if(view==0){
			window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/report_staff'; ?>";
		}else{
			window.location="<?php echo base_url().ADMINPATH.'/report/report_sales/report_staff'; ?>?view_type="+view;
		}
	}
</script>