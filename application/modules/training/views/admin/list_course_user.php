<?php 
$url = base_url().ADMINPATH.'/training';
?>
<form action="" id="form_search" method='post'>
<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<select class="form-control" name="groupid" id="groupid" onchange="searchs()">
					<option value="">-- Lọc theo nhóm --</option>
					<?php foreach ($group as $key => $value) {
						$sl='';
						if($groupid ==$value["id"]){
							$sl = 'selected = "selected"';
						}
						?>
						<option <?php echo $sl ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"]; ?></option>
					<?php } ?>
				</select>
	        </div>
			<div class="block2">
				<a href="<?php echo $url;?>/training/add_course_user" class="btn btn-danger"><i class="fa fa-plus"></i> Phân bổ khóa học</a>
			</div>
		</div>
		<div class="table_data">	
		    <table>
	            <thead>
		    	<tr>
		    		<th>STT</th>
		    		<th>Tên khóa học</th>
		    		<th>Nhóm khóa học</th>
		    		<th>Tên NV</th>
		    		<th>Cấp độ</th>
		    		<th>Ngày yêu cầu</th>
		    		<th>Ngày hoàn tất</th>
		    		<th class="th_state">Tình trạng</th>
		    		<th class="th_action">Thao tác</th>
		    	</tr>
		    	</thead>
		    	<tbody>
		    	<?php
		    	$stt =0;
		    	 foreach ($list as $key => $value) {
		    	 	
		    	 	$status = '';
		    	 	if($value["status"] == 0){
		    	 		$status = '<td class="text-warning"><i class="fa fa-minus-circle"></i> Chưa bắt đầu</td>';
		    	 	}
		    	 	if($value["status"] == 1){
		    	 		$status = '<td class="text-success"><i class="fa fa-check-circle"></i> Hoàn thành</td>';
		    	 	}
		    	 	if($value["status"] == 2){
		    	 		$status = '<td class="text-danger"><i class="fa fa-info-circle"></i> Chưa hoàn thành</td>';
		    	 	}
		    		?>
		    		
		    		<tr>
		    		<td> <?php echo ++$stt; ?></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><?php echo $value["title"]; ?></a></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><?php echo $name_group[$value["groupid"]]; ?></a></td>
		    		<td><a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><?php echo $name_user[$value["userid"]]; ?></a></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><?php echo $name_level[$value["level"]]; ?></a></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><?php echo date('d/m/Y',strtotime($value["deathline"])); ?></a></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><?php echo isset($value["enddate"]) ? date('d/m/Y',strtotime($value["enddate"])) : ''; ?></a></td>
		    		<?php echo $status ?>
		    		<td> <a href="<?php echo $url;?>/training/add_course_user/<?php echo $value["idcourse"];?>"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a>&nbsp;&nbsp;<a onclick="del(<?php echo $value["idcourse"];?>);return false;"><i class="fa fa-trash-o"> </i> Xóa</a></td>
		    		</tr>
		    	<?php } ?>
		    	</tbody>
		    </table>
		</div>
</div>
<input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
</form>
<script>
	function del(id){
		var result = confirm('Bạn có chắc chắn muốn xóa không');
		if(result){
			var link = $('#link').val();
	        $.post(link+"/course/delete_course_user/"+id, $("form").serialize(),function(resp){
	        	alert('Xóa thành công');
	        	location.reload();
	        });
		}
	}
	function searchs(){
		$('#form_search').submit();
	}
	   
</script>
