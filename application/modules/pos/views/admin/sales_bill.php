<div class="sub_menu">
	<div class="find_record">
		<?php 
		$to = $start+$limit>$total ? $total : $start+$limit ;
		?>
		Tìm thấy <b><?php echo number_format($start+1).'-'.number_format($to) ?> / <?php echo number_format($total) ?></b> đơn hàng. <a href="<?php echo ADMINPATH.'/pos/pos_sales/export_data' ?>"><i class="fa fa-download"></i> Xuất file excel toàn bộ tháng này</a> | <a href="<?php echo ADMINPATH.'/pos/pos_sales/export_data/1' ?>"><i class="fa fa-download"></i> Xuất file excel ngày hôm nay</a>
	</div>
</div>
<div class="containner">
	<div class="table_div">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã ĐH</th>
				<th>Tên khách hàng</th>
				<th style="width:120px;">Số điện thoại</th>
				<th>Giá trị</th>
				<th style="width:120px;">Chiết khấu</th>
				<th>Ghi chú</th>
				<th>Thời gian</th>
				<th style="width:120px;">Trạng thái</th>
				<th style="width:120px;">Nhận tiền KH</th>
				<th style="width:120px;">Quản lý nhận</th>
				<th style="width:120px;">Kế toán nhận</th>
			</tr>
			<tr>
				<?php 
				$arr = array(
					0=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Thành công</a>',
					2=>'<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Nháp</a>',
					1=>'<a class="status-button"><i class="fa fa-circle text-danger" aria-hidden="true"></i> Đã hủy</a>',
					7=>'<a class="status-button"><i class="fa fa-circle text-warning" style="color:orange" aria-hidden="true"></i> Đang giao</a>'
				);

				$arr_money = array(
					1=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Đã thu</a>',
					0=>'<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa thu</a>'
				);
				if(count($data)>0){
					$i=$start+1;
					foreach ($data as $row) {
						$row->Status = isset($arr[$row->Status]) ? $arr[$row->Status] : '<a class="status-button"><i class="fa fa-circle text-warning" style="color:orange" aria-hidden="true"></i> Đang xử lý</a>' ;
						$row->Note = $row->Note=='' ? '--' : $row->Note ;
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.$row->MaDH.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.$row->Name.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.$row->Phone.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.number_format($row->Total-$row->Chietkhau).'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.number_format($row->Chietkhau).'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.$row->Note.'</a></td>
						<td style="width:120px;"><a href="'.ADMINPATH.'/pos/pos_sales/view_order/'.$row->ID.'">'.date('d/m/Y H:i',strtotime($row->Ngaydathang)).'</a></td>
						<td style="width:120px;">'.$row->Status.'</td>
						<td style="width:120px;">'.$arr_money[$row->TransportMoney].'</td>
						<td title="Click để thay đổi trạng thái" style="width:120px;" onclick="sales_active_managermoney(this,'.$row->ID.','.$row->ManagerMoney.')">'.$arr_money[$row->ManagerMoney].'</td>
						<td title="Click để thay đổi trạng thái" style="width:120px;" onclick="sales_active_financemoney(this,'.$row->ID.','.$row->FinanceMoney.')">'.$arr_money[$row->FinanceMoney].'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
		</table>
		<?php echo str_replace('href="#"','',$nav); ?>
	</div>
</div>