<?php

/**
 * Ghn Class
 *
 * Partial Caching library for Codeignter
 *
 * @category	Libraries
 * @author	Thanh Binh
 * @version	1.0
 */

class Api_ghn extends Admin_Controller
{
	private $apiUrl;
        private $clientID;
        private $password;
        private $apiKey;
        private $apiSecretKey;
        private $_ghn = null;
        private $_sessionToken = null;
        
        public $user;
 	public $classname="report";

	public function __construct() {
            parent::__construct();   
            $session = $this->session->userdata('ttp_usercp');
            $this->user = $this->lib->get_user($session,$this->classname);
            $this->load->library('template');
            $this->template->set_template('report');
            $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
            $this->template->write_view('header','admin/header',array('user'=>$this->user));
            $this->template->add_js("public/admin/js/script_report.js");
            $this->template->add_doctype(); 
//        
//            $this->apiUrl = "https://testapipds.ghn.vn:9999/External/B2C/";
//            $this->clientID = 53813;
//            $this->password = "a5p6zawrPyqWRGpsS";
//            $this->apiKey = "caquEdYxJU15C2N0";
//            $this->apiSecretKey = "E731CA7CEA7F0585C35B185DCD77ACEB";
            
            $this->apiUrl = "https://testapipds.ghn.vn:9999/external/b2c/";
            $this->clientID = 53813;
            $this->password = "1234567890";
            $this->apiKey = "dSLjeJstcjwcbcLe";
            $this->apiSecretKey = "F7BB3C08E9BCA7E8D880B3D9D57FB4DF";
            
            //-->Hnam
//            $this->apiUrl = "https://apipds.ghn.vn/External/B2C";
//            $this->clientID = 26684;
//            $this->password = "a5p6zawrPyqWRGpsS";
//            $this->apiKey = "caquEdYxJU15C2N0";
//            $this->apiSecretKey = "E731CA7CEA7F0585C35B185DCD77ACEB";
            
            $this->_ghn = new Ghn($this->apiUrl, $this->clientID, $this->password, $this->apiKey, $this->apiSecretKey);
            $this->_sessionToken = $this->_ghn->SignIn();
	}
        
        public function index(){
            $district = $this->getProvinceData();
            $pickupHub = $this->getPickupHub();
            $service = $this->getService();

            $this->template->add_title('Ghn');
            $data = array(
                'PickHub'  => $pickupHub,                
                'District'  => $district,                
                'Service'  => $service,                
            );
            $this->template->write_view('content','admin/api_ghn',$data);
            $this->template->render();
        }
        
        public function getProvinceData() {        
            $districtProvinceDataRequest = array("SessionToken" => $this->_sessionToken);
            $responseDistrictProvinceData = $this->_ghn->GetDistrictProvinceData($districtProvinceDataRequest);
            return $responseDistrictProvinceData["Data"];
        }
        
        private function getPickupHub() {
            $getPickHubRequest = array("SessionToken" => $this->_sessionToken);
            $responseGetPickHubs = $this->_ghn->GetClientHubs($getPickHubRequest);
            if (empty($responseGetPickHubs['ErrorMessage'])) {
                return $responseGetPickHubs["HubInfo"];
            }
        }
        
        public function createSo() {
                
        //Create SO
        $SOCode = $this->genShippingOrderID();
        
        $RecipientName = isset($_GET['RecipientName']) ? $_GET['RecipientName'] : '' ; 
        $DeliveryAddress = isset($_GET['DeliveryAddress']) ? $_GET['DeliveryAddress'] : '' ; 
        $RecipientPhone = isset($_GET['RecipientPhone']) ? $_GET['RecipientPhone'] : '' ;
        $CODAmount = isset($_GET['CODAmount']) ? $_GET['CODAmount'] : '' ;
        $ContentNote = isset($_GET['ContentNote']) ? $_GET['ContentNote'] : '' ;
        
        $DeliveryDistrictCode = isset($_GET['DeliveryDistrictCode']) ? $_GET['DeliveryDistrictCode'] : '' ;
        
        $ServiceID = isset($_GET['ServiceID']) ? $_GET['ServiceID'] : '' ;
        $PickHubID = isset($_GET['pickupHubID']) ? $_GET['pickupHubID'] : '' ;
        
        $Weight = isset($_GET['Weight']) ? $_GET['Weight'] : '' ;
        $Length = isset($_GET['Length']) ? $_GET['Length'] : '' ;
        $Width = isset($_GET['Width']) ? $_GET['Width'] : '' ;
        $Height = isset($_GET['Height']) ? $_GET['Height'] : '' ;
        $payment = isset($_GET['paymentMethod']) ? $_GET['paymentMethod'] : '' ;
        $invoice = isset($_GET['invoice']) ? $_GET['invoice'] : '' ;
        $Fee = isset($_GET['Fee']) ? $_GET['Fee'] : '' ;
        $isChargeFee = isset($_GET['isChargeFee']) ? $_GET['isChargeFee'] : '' ;

        $CODAmount =$this->convertNumber($CODAmount, ",");
        $Fee = $this->convertNumber($Fee, ",");
        
        if ($isChargeFee==1) {
            $CODAmount = $CODAmount + $Fee;
        }
        
        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "RecipientName" => $RecipientName,
            "DeliveryAddress" => $DeliveryAddress,
            "RecipientPhone" => $RecipientPhone,
            "ClientOrderCode" => $SOCode,
            "CODAmount" => $this->convertNumber($CODAmount, ","),
            "ContentNote" => $ContentNote,
            "DeliveryDistrictCode" => $DeliveryDistrictCode,
            "ServiceID" => $ServiceID,
            "PickHubID" => $PickHubID,
            "Weight" => $Weight,
            "Length" => $Length,
            "Width" => $Width,
            "Height" => $Height
        );
        $responseCreateShippingOrder = $this->_ghn->CreateShippingOrder($shippingOrderRequest);
            
//        if ($responseCreateShippingOrder["OrderCode"] != null) {
//            $ghnData = array();
//            $ghnData["order_code"] = $responseCreateShippingOrder["OrderCode"];
//            $ghnData["client_order_code"] = $SOCode;
//            $ghnData["total_fee"] = $responseCreateShippingOrder["TotalFee"];;
//            $ghnData["payment"] = $payment;
//            $ghnData["invoice_info"] = $invoice;
//            $ghnData["note"] = $ContentNote;
//            $ghnData["datetime"] = date("Y-m-d H:i:s");
//            Business_Addon_Ghn::getInstance()->insert($ghnData);
//        }
          
        echo json_encode($responseCreateShippingOrder);
    }
    
    private function convertNumber($number, $delimiter) {
        return str_replace($delimiter, "", $number);
    }
    
    public function getOrderInfo(){
        //157327486564
        $OrderCode = isset($_GET['OrderCode']) ? $_GET['OrderCode'] : '' ;
        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "OrderCode" => $OrderCode
                );
        $responseGetOrderInfo = $this->_ghn->GetOrderInfo($shippingOrderRequest);
        echo "<pre>";
        var_dump($responseGetOrderInfo); exit();
        echo $this->getStatus($responseGetOrderInfo["CurrentStatus"]);
    }
    
    public function cancelOrder(){
        $OrderCode = isset($_GET['OrderCode']) ? $_GET['OrderCode'] : '' ;
        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "OrderCode" => $OrderCode
                );
        $responseGetOrderInfo = $this->_ghn->CancelOrder($shippingOrderRequest);
        echo "<pre>";
        var_dump($responseGetOrderInfo); exit();
        echo $this->getStatus($responseGetOrderInfo["CurrentStatus"]);
    }
    
    public function getCities($ghnProvince) {
        $result = array();
        foreach($ghnProvince as $item) {
            $pcode = $item["ProvinceCode"];
            $pname = $item["ProvinceName"];
            $result[$pcode] = $pname;
        }
        return $result;
    }
    
    public function getDistrictsByCityCode($ghnProvince, $cityCode) {
        $result = array();
            
        foreach($ghnProvince as $item) {
            if ($item["ProvinceCode"] ==$cityCode){
                $result[$item["DistrictCode"]] = $item["DistrictName"];
            }
        }
        return $result;
    }
    
    private function genShippingOrderID () {
        $pattern = "TTP-SO-%s-%s-%s-%s";
        $day = date("d");
        $month = date("m");
        $year = date("Y");
        $timestamp = date("His");
        return sprintf($pattern, $day, $month, $year, $timestamp);
        
    }
        public function calculateFee(){
            $Length = isset($_GET['Length']) ? $_GET['Length'] : 0 ;
            $Weight = isset($_GET['Weight']) ? $_GET['Weight'] : 0 ;
            $Height = isset($_GET['Height']) ? $_GET['Height'] : 0 ;
            $Width = isset($_GET['Width']) ? $_GET['Width'] : 0 ;
            $FromDistrictCode = isset($_GET['PickHubID']) ? $_GET['PickHubID'] : '' ;
            $ToDistrictCode = isset($_GET['DeliveryDistrictCode']) ? $_GET['DeliveryDistrictCode'] : '' ;
            $ServiceID = isset($_GET['ServiceID']) ? $_GET['ServiceID'] : '' ;
//            var_dump($Length,$Weight,$Height,$FromDistrictCode,$ToDistrictCode,$ServiceID);

            // Caculate Service Fee
            $c = array("FromDistrictCode" => $FromDistrictCode,
                "ServiceID" => $ServiceID,
                "ToDistrictCode" => $ToDistrictCode,
                "Weight" => $Weight,
                "Length" => $Length,
                "Width" => $Width,
                "Height" => $Height
            );
            $items[] = $c;
            $calculateServiceFeeRequest = array("SessionToken" => $this->_sessionToken, "Items" => $items);
            $responseCalculateServiceFee = $this->_ghn->CalculateServiceFee($calculateServiceFeeRequest);
            echo json_encode($responseCalculateServiceFee);
        }
        
        public function getServiceList() {
            $FromDistrictCode = isset($_GET['FromDistrictCode']) ? $_GET['FromDistrictCode'] : 0 ;
            $ToDistrictCode = isset($_GET['ToDistrictCode']) ? $_GET['ToDistrictCode'] : 0 ;
            $ServiceList = $this->getServiceListData($FromDistrictCode, $ToDistrictCode);
            echo json_encode($ServiceList);
        }
        
        private function getServiceListData($FromDistrictCode, $ToDistrictCode) {
            $infoServiceRequest = array("SessionToken" => $this->_sessionToken, "FromDistrictCode" => $FromDistrictCode, "ToDistrictCode" => $ToDistrictCode);
            $responseServiceInfo = $this->_ghn->GetServiceList($infoServiceRequest);
            return $responseServiceInfo;
        }
        
        private function getService() {
        $result = array(
            53319	=> "6 Giờ",
            53320	=> "1 Ngày",
            53321	=> "2 Ngày",
            53322	=> "3 Ngày",
            53323	=> "4 Ngày",
            53324	=> "5 Ngày"
        );
        return $result;
    }
    
    private function getStatus($id = null) {
        $status = array(
            'ReadyToPick'       => "Chờ lấy hàng",
            'Picking'           => "Đang lấy hàng",
            'Storing'           => "Lưu kho",
            'Delivering'        => "Đang giao hàng",
            'Delivered'         => "Đã giao hàng",
            'WaitingToFinish'	=> "Đơn hàng đã được chuyển sang chờ thanh toán",
            'Finish'            => "Kết thúc đơn hàng",
            'Return'            => "Đơn hàng được trả lại",
            'Cancel'            => "Hủy đơn hàng"
        );
        if ($id === null) {
            return $status;
        }
        return $status[$id];
    }
}