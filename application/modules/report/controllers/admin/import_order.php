<?php

class Import_order extends Admin_Controller {

    //-->Giao hàng nhanh
    private $_ghn = null;
    private $_sessionToken = null;
    //-->End Giao hàng nhanh
    public $user;
    public $classname = "import_order";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar', 'admin/import_sitebar', array('user' => $this->user));
        $this->template->write_view('header', 'admin/header', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public $response = array('error' => false, 'message' => array());
    public $limit = 30;

    public function index() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $this->template->add_title('Report Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $startday = $this->session->userdata("import_startday");
        $startday = $startday != '' ? $startday : date('Y-m-01', time());
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $status_leader = 0;
        $bonus = $this->user->IsAdmin == 1 || $this->user->UserType == 5 || $this->user->UserType == 13 || $this->user->UserType == 8 ? "" : " and a.UserID=" . $this->user->ID;
        if ($this->user->IsAdmin == 0) {
            $checkleader = $this->db->query("select Data from ttp_report_team where UserID=" . $this->user->ID)->row();
            if ($checkleader) {
                $userlist = json_decode($checkleader->Data, true);
                if (count($userlist) > 0) {
                    $userlist[] = $this->user->ID;
                    $userlist = implode(',', $userlist);
                    $bonus = " and a.UserID in(" . $userlist . ")";
                    $status_leader = 1;
                }
            }
        }

        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter == '' ? "" : " and " . $fillter;
        $statusbonus = "";
        $orderby = "a.Ngaydathang DESC";
        switch ($this->user->UserType) {
            case 1:
                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
            case 2:
                $view = 'admin/import_order_home_kho';
                $statusbonus = $this->user->Channel == 1 ? " and a.Status=3 and a.OrderType in(1,2,4,5)" : "";
                $statusbonus = $this->user->Channel == 2 ? " and a.Status=3 and a.OrderType=3" : $statusbonus;
                $statusbonus = $this->user->Channel == 0 ? " and a.Status=3 and a.OrderType=0" : $statusbonus;
                $statusbonus = " and a.Status=3";
                $orderby = "a.Ngaydathang ASC";
                break;
            case 3:

                $view = 'admin/import_order_home_ketoan';
                $statusbonus = $this->user->Channel == 1 ? " and a.Status=5 and a.OrderType in(1,2,4,5)" : '';
                $statusbonus = $this->user->Channel == 2 ? " and a.Status=5 and a.OrderType=3" : $statusbonus;
                $statusbonus = $this->user->Channel == 0 ? " and a.Status=5 and a.OrderType=0" : $statusbonus;
                $statusbonus = " and a.Status=5";
                $orderby = "a.Ngaydathang ASC";
                break;

            case 4:

                $view = 'admin/import_order_home_dieuphoi';
                $statusbonus = " and a.Status in(7,8,9,11,0,1)";
                $statusbonus = $this->user->Channel == 1 ? $statusbonus . " and a.OrderType in(1,2,4,5)" : $statusbonus . " and a.OrderType in(0,3)";
                $statusbonus = " and a.Status in(7,8,9,11,0,1)";
                $orderby = "a.Ngaydathang DESC";
                break;

            case 5:

                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;

            case 6:

                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;

            case 7:

                $view = 'admin/import_order_home_ketoan_manager';
                $statusbonus = " and a.Status = 5 and a.Accept=1";
                break;

            case 8:

                $view = 'admin/import_order_home_kho';
                $statusbonus = $bonus;
                break;

            case 10:

                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;

            case 13:

                $view = 'admin/import_order_home_user';
                $statusbonus = $this->user->Channel == 1 ? $statusbonus . " and a.OrderType in(1,2,4,5)" : $statusbonus . " and a.OrderType in(0,3)";
                break;

            default:

                $view = 'admin/import_order_home_user';
                $statusbonus = $bonus;
                break;
        }

        if ($this->user->UserType == 4) {
            $nav = $this->db->query("
                select DISTINCT a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau
                from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_export_warehouse f
                where a.ID=f.OrderID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus")->row();

            $result = $this->db->query("select DISTINCT a.FinanceMoney,a.ID,a.MaDH,f.MaXK,f.RealMoney,a.UserID,a.Status,a.OrderType,a.Accept,a.PaymentStatus,a.Payment,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.CustomerID,a.CustomerType,a.SoluongSP,a.Total,a.Ngaydathang,a.TransportID,a.TransportRef,a.TransportStatus,a.Chiphi,a.Reduce,c.Name,c.Phone1,a.Name as NameOrder,a.Phone as PhoneOrder,c.Company,d.Title as Thanhpho,b.Title as Quanhuyen,a.DeliveryTime,f.RealTransferMoney from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_export_warehouse f where a.ID=f.OrderID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 $fillter $statusbonus order by $orderby $limit_str")->result();
        } elseif ($this->user->UserType == 2 || $this->user->UserType == 8) {
            $nav = $this->db->query("
                select DISTINCT a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau
                from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_warehouse g
                where a.KhoID=g.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus")->row();

            $result = $this->db->query("select DISTINCT a.ID,a.MaDH,a.UserID,a.Status,a.OrderType,a.Accept,a.PaymentStatus,a.Payment,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.CustomerID,a.CustomerType,a.SoluongSP,a.Total,a.Ngaydathang,a.Reduce,a.Chiphi,c.Name,c.Phone1,a.Name as NameOrder,a.Phone as PhoneOrder,c.Company,d.Title as Thanhpho,b.Title as Quanhuyen,g.MaKho,a.DeliveryTime from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_warehouse g where a.KhoID=g.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus order by $orderby $limit_str")->result();
        } else {

            $nav = $this->db->query("

                select DISTINCT a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau

                from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e

                where a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus")->row();

            $result = $this->db->query("select DISTINCT a.ID,a.MaDH,a.UserID,a.Status,a.OrderType,a.Accept,a.PaymentStatus,a.Payment,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.CustomerID,a.CustomerType,a.SoluongSP,a.Total,a.Ngaydathang,a.Reduce,a.Chiphi,c.Name,c.Phone1,a.Name as NameOrder,a.Phone as PhoneOrder,c.Company,d.Title as Thanhpho,b.Title as Quanhuyen,a.KhoID,a.DeliveryTime,a.FinanceMoney,a.TransportStatus,a.TransportRef from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e where a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus order by $orderby $limit_str")->result();
        }

        $total = $nav ? $nav->Total : 0;
        $chiphi = $nav ? $nav->Chiphi : 0;
        $soluongban = $nav ? $nav->SoluongBan : 0;
        $chietkhau = $nav ? $nav->Chietkhau : 0;
        $nav = $nav ? $nav->nav : 0;

        $data = array(
            'status_leader' => $status_leader,
            'fill_data' => $this->session->userdata('fillter'),
            'data' => $result,
            'total' => $total,
            'SoluongSP' => $soluongban,
            'chiphi' => $chiphi,
            'chietkhau' => $chietkhau,
            'startday' => $startday,
            'stopday' => $stopday,
            'fillter' => $fillter,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/',
            'start' => $start,
            'find' => $nav,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/report/import_order/index', 5, $nav, $this->limit)
        );

        $view = $this->user->IsAdmin == 1 ? 'admin/import_order_home' : $view;

        $this->template->write_view('content', $view, $data);

        $this->template->render();
    }

    public function enter_transport_ref() {

        $TransportRef = isset($_POST['TransportRef']) ? mysql_real_escape_string($_POST['TransportRef']) : '';

        $TransportRef = trim($TransportRef);

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        $Order = $this->db->query("select ID from ttp_report_order where ID=$ID")->row();

        if ($Order && $TransportRef != '') {

            $this->db->query("update ttp_report_order set TransportRef='$TransportRef' where ID=$ID");

            $this->response = array("error" => false, "message" => $TransportRef);
        } else {

            $this->response = array("error" => true, "message" => "Mã vận đơn không hợp lệ");
        }

        echo json_encode($this->response);
    }

    public function load_box_sync_transport_status() {

        $transport = $this->db->query("select * from ttp_report_transport")->result();

        $this->load->view("admin/import_order_sync_transport_status", array('Transport' => $transport));
    }

    public function sync_status_from_transport() {

        $TransportID = isset($_POST['TransportID']) ? (int) $_POST['TransportID'] : 0;

        $Transport = $this->db->query("select * from ttp_report_transport where ID=$TransportID and api=1")->row();

        if ($Transport) {

            $startday = $this->session->userdata("import_startday");

            $startday = $startday != '' ? $startday : date('Y-m-01', time());

            $stopday = $this->session->userdata("import_stopday");

            $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

            $order = $this->db->query("select ID,TransportRef,TransportStatus from ttp_report_order where TransportRef!='' and date(Ngaydathang)>='$startday' and date(Ngaydathang)<='$stopday'")->result();

            $data = $this->lib->get_status_order_from_viettelpost($startday);

            $this->load->view("admin/import_order_sync_transport_status_viettel", array('data' => $data, 'order' => $order));
        } else {

            echo '<div class="col-xs-12"><div class="alert-danger alert">Đơn vị vận chuyển này chưa cung cấp API để hỗ trợ đồng bộ trạng thái đơn hàng</div></div>';
        }
    }

    public function logistic() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        if ($this->user->UserType == 4 || $this->user->IsAdmin == 1) {

            $this->template->add_title('Coordinator');

            $status = isset($_GET['type']) ? (int) $_GET['type'] : 9;

            $result = $this->db->query("select a.ID,a.MaDH,a.DeliveryTime,b.Amount,b.ProductsID,b.ShipmentID,b.WarehouseID,b.BranchID,b.Status,b.UserReciver,b.TimeReciver,b.TimeSuccess,c.Donvi,c.Title,d.MisaCode,e.Title as District from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c,ttp_report_city d,ttp_report_district e where a.CityID=d.ID and a.DistrictID=e.ID and a.ID=b.OrderID and b.ProductsID=c.ID and a.Status=$status order by a.ID ASC")->result();



            $data = array(
                'data' => $result,
                'type' => $status,
                'base_link' => base_url() . ADMINPATH . '/report/import_order/'
            );



            switch ($status) {

                case 9:

                    $view = 'admin/import_order_select_transport';

                    break;

                case 7:

                    $view = 'admin/import_order_state_transport';

                    break;

                case 0:

                    $view = 'admin/import_order_success_transport';

                    break;

                case 1:

                    $view = 'admin/import_order_false_transport';

                    break;

                default:

                    $view = 'admin/import_order_select_transport';

                    break;
            }

            $this->template->write_view('content', $view, $data);

            $this->template->render();
        }
    }

    public function choosetransport() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        if ($this->user->UserType == 4 || $this->user->IsAdmin == 1) {

            $data = isset($_POST['data']) ? json_decode($_POST['data']) : array();

            $data[] = 0;

            $data = implode(',', $data);

            $data = $this->db->query("select a.MaDH,a.ID,a.DeliveryTime,a.Ngaydathang,a.Total,a.Reduce,a.Chiphi,b.Title as City,c.Title as District,a.AddressOrder from ttp_report_order a,ttp_report_city b,ttp_report_district c where a.CityID=b.ID and a.DistrictID=c.ID and a.ID in($data) and a.Status=9")->result();

            if (count($data) > 0) {

                $this->load->view("import_order_select_transporter", array('data' => $data));
            }
        }
    }

    public function viewtransport() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        if ($this->user->UserType == 4 || $this->user->IsAdmin == 1) {

            $ID = isset($_POST['ID']) ? json_decode($_POST['ID']) : 0;

            $UserID = isset($_POST['UserID']) ? json_decode($_POST['UserID']) : 0;

            $data = $this->db->query("select a.MaDH,a.Name,a.Phone,a.TransportID,a.ID,a.DeliveryTime,a.Ngaydathang,a.Total,a.Reduce,a.Chiphi,b.Title as City,c.Title as District,a.AddressOrder from ttp_report_order a,ttp_report_city b,ttp_report_district c where a.CityID=b.ID and a.DistrictID=c.ID and a.ID=$ID")->row();

            if (count($data) > 0) {

                $this->load->view("import_order_change_transporter", array('data' => $data, 'ID' => $ID, 'UserID' => $UserID));
            }
        }
    }

    public function loadtransporter($id = 0) {

        $info = $this->db->query("select * from ttp_user where ID=$id")->row();

        if ($info) {

            $sex = $info->Gender == 0 ? "Nữ" : "Nam";

            echo "<div class='col-xs-3 text-center'><img src='" . $info->Thumb . "' style='width:110px;height:110px;border-radius:50%;margin:20px 0px' /></div>";

            echo "<div class='col-xs-4'>

                <h4>$info->FirstName $info->LastName</h4>

                <p>Giới tính : $sex</p>

                <p>Số điện thoại : $info->Phone</p>

                <p>Email : $info->Email</p>

                <p>Cấp độ nhân viên : chuyên nghiệp</p>

            </div>";

            $month = date('m');

            $year = date('Y');

            $pending = $this->db->query("select DISTINCT OrderID from ttp_report_order_send_supplier where MONTH(TimeReciver)='$month' and YEAR(TimeReciver)='$year' and UserReciver=$id and Status=2")->result();

            $pending = count($pending);

            $success = $this->db->query("select DISTINCT OrderID from ttp_report_order_send_supplier where MONTH(TimeReciver)='$month' and YEAR(TimeReciver)='$year' and UserReciver=$id and Status=3")->result();

            $success = count($success);

            $false = $this->db->query("select DISTINCT OrderID from ttp_report_order_send_supplier where MONTH(TimeReciver)='$month' and YEAR(TimeReciver)='$year' and UserReciver=$id and Status=4")->result();

            $false = count($false);

            echo "<div class='col-xs-5'>

                <h4>Thành tích trong tháng</h4>

                <p><span style='float:left;width:200px;'>Đơn hàng giao thành công </span>: " . number_format($success) . " đơn hàng</p>

                <p><span style='float:left;width:200px;'>Đơn hàng hủy </span>: " . number_format($false) . " đơn hàng</p>

                <p><span style='float:left;width:200px;'>Số đơn hàng đang giao </span>: " . number_format($pending) . " đơn hàng</p>

                <p>Đánh giá :

                    <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>

                    <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>

                    <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>

                    <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>

                    <i class='fa fa-star' aria-hidden='true' style='color:#F00'></i>

                </p>

            </div>";
        }
    }

    public function change_transport() {

        $TransportID = isset($_POST['TransportID']) ? (int) $_POST['TransportID'] : 0;

        $UserID = isset($_POST['UserID']) ? (int) $_POST['UserID'] : 0;

        $OrderID = isset($_POST['OrderID']) ? $_POST['OrderID'] : array();

        if (count($OrderID) > 0 && $UserID > 0 && $TransportID > 0) {

            $arr = implode(',', $OrderID);

            $order = $this->db->query("select MaDH from ttp_report_order where ID in($arr) and Status=9")->result();

            if (count($order) > 0) {

                foreach ($order as $row) {

                    $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> chuyển cho người giao hàng đơn hàng <b class='text-primary'>$row->MaDH</b>", 3);
                }
            }

            $this->db->query("update ttp_report_order set Status=7,TransportID=$TransportID where ID in($arr) and Status=7");

            $this->db->query("update ttp_report_order_send_supplier set UserReciver=$UserID where OrderID in($arr)");

            $arr_sql = array();

            $time = date('Y-m-d H:i:s', time());

            foreach ($OrderID as $row) {

                $arr_sql[] = "($row,'$time',7," . $this->user->ID . ")";
            }

            $this->db->query('insert into ttp_report_orderhistory(OrderID,Thoigian,Status,UserID) values' . implode(',', $arr_sql));
        }

        redirect(ADMINPATH . '/report/import_order/logistic?type=7');
    }

    public function apply_transport() {

        $TransportID = isset($_POST['TransportID']) ? (int) $_POST['TransportID'] : 0;

        $UserID = isset($_POST['UserID']) ? (int) $_POST['UserID'] : 0;

        $OrderID = isset($_POST['OrderID']) ? $_POST['OrderID'] : array();

        if (count($OrderID) > 0 && $UserID > 0 && $TransportID > 0) {

            $arr = implode(',', $OrderID);

            $order = $this->db->query("select MaDH from ttp_report_order where ID in($arr) and Status=9")->result();

            if (count($order) > 0) {

                foreach ($order as $row) {

                    $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> chuyển cho người giao hàng đơn hàng <b class='text-primary'>$row->MaDH</b>", 3);
                }
            }

            $this->db->query("update ttp_report_order set Status=7,TransportID=$TransportID where ID in($arr) and Status=9");

            $this->db->query("update ttp_report_order_send_supplier set UserReciver=$UserID where OrderID in($arr) and Status=1");

            $arr_sql = array();

            $time = date('Y-m-d H:i:s', time());

            foreach ($OrderID as $row) {

                $arr_sql[] = "($row,'$time',7," . $this->user->ID . ")";
            }

            $this->db->query('insert into ttp_report_orderhistory(OrderID,Thoigian,Status,UserID) values' . implode(',', $arr_sql));
        }

        redirect(ADMINPATH . '/report/import_order/logistic');
    }

    public function group_supplier() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $this->template->add_title('Report Tools');

        $supplierid = isset($_GET['supplier']) ? $_GET['supplier'] : 0;

        $supplier = $supplierid > 0 ? " and c.SupplierID=$supplierid" : "";

        $result = $this->db->query("select a.ID,a.KhoID,a.MaDH,a.DeliveryTime,b.Amount,b.ProductsID,b.ShipmentID,c.RootPrice,c.Donvi,c.Title,d.MisaCode,e.Title as District,c.SupplierID from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c,ttp_report_city d,ttp_report_district e where a.CityID=d.ID and a.DistrictID=e.ID and a.ID=b.OrderID and b.ProductsID=c.ID and a.Status=3 and c.VariantType in(0,1) $supplier")->result();

        $result1 = $this->db->query("select a.ID,a.KhoID,a.MaDH,a.DeliveryTime,b.Amount,b.ProductsID,b.ShipmentID,c.RootPrice,c.Donvi,c.Title,d.MisaCode,e.Title as District,c.SupplierID from ttp_report_order a,ttp_report_orderdetails_bundle b,ttp_report_products c,ttp_report_city d,ttp_report_district e where a.CityID=d.ID and a.DistrictID=e.ID and a.ID=b.OrderID and b.ProductsID=c.ID and a.Status=3 and c.VariantType in(0,1) $supplier")->result();



        $data = array(
            'data' => $result,
            'data1' => $result1,
            'SupplierID' => $supplierid,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/'
        );

        $view = 'admin/import_order_group_supplier';

        $this->template->write_view('content', $view, $data);

        $this->template->render();
    }

    public function send_to_supplier() {

        $data = isset($_POST['data']) ? $_POST['data'] : array();

        if (count($data) > 0) {

            $data = json_decode($data);

            if (count($data) > 0) {

                $data_request = file_get_contents("log/supplier/request_picking.txt");

                $data_request = $data_request != '' ? json_decode($data_request, true) : array();

                $arr_ncc = array();

                $arr_ucc = array();

                $time = date('Y-m-d H:i:s');

                foreach ($data as $row) {

                    if ($row->AmountNcc > 0) {

                        $arr_ncc[] = "($row->OrderID,$row->ProductsID,$row->AmountNcc,$row->ShipmentID,$row->Branch,'$time'," . $this->user->ID . ",$row->RootPrice)";

                        if (isset($data_request[$row->Branch])) {

                            $data_request[$row->Branch] = $data_request[$row->Branch] + 1;
                        } else {

                            $data_request[$row->Branch] = 1;
                        }
                    }

                    if ($row->AmountUcc > 0) {

                        $arr_ucc[] = "($row->OrderID,$row->ProductsID,$row->AmountUcc,$row->ShipmentID,1,'$time'," . $this->user->ID . ",$row->RootPrice)";
                    }
                }

                file_put_contents("log/supplier/request_picking.txt", json_encode($data_request));

                if (count($arr_ncc) > 0) {

                    $arr_ncc = "insert into ttp_report_order_send_supplier(OrderID,ProductsID,Amount,ShipmentID,BranchID,TimeRequest,UserRequest,Price) values" . implode(',', $arr_ncc);

                    $this->db->query($arr_ncc);
                }

                if (count($arr_ucc) > 0) {

                    $arr_ucc = "insert into ttp_report_order_send_supplier(OrderID,ProductsID,Amount,ShipmentID,WarehouseID,TimeRequest,UserRequest,Price) values" . implode(',', $arr_ucc);

                    $this->db->query($arr_ucc);
                }

                echo "true";

                return;
            }
        }

        echo "false";
    }

    public function picking_supplier() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $this->template->add_title('Report Tools');

        $result = $this->db->query("select a.ID,a.MaDH,b.Amount,b.ProductsID,b.Price,b.Status,c.Donvi,c.Title,b.TimeRequest,b.TimeAccept,b.BranchID,b.WarehouseID,b.ID as DetailsID from ttp_report_order a,ttp_report_order_send_supplier b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and b.Status<2 and a.Status=3 order by a.ID ASC")->result();

        $data = array(
            'data' => $result,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/'
        );

        $view = 'admin/import_order_picking_supplier';

        $this->template->write_view('content', $view, $data);

        $this->template->render();
    }

    public function change_real_picking() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 2) {

            $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

            $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : 0;

            $this->db->query("update ttp_report_order_send_supplier set Amount=$Amount where ID=$ID");

            echo "true";
        } else {

            echo "false";
        }
    }

    public function readytopick() {

        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;

        if ($this->user->UserType == 2 || $this->user->IsAdmin == 1) {

            $check = $this->db->query("select ID from ttp_report_order_send_supplier where OrderID=$ID and Status=0")->row();

            if (!$check) {

                $checkorder = $this->db->query("select ID,MaDH from ttp_report_order where ID=$ID and Status=3")->row();

                if ($checkorder) {

                    $this->db->query("update ttp_report_order set Status=9 where ID=$ID");

                    $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> chuyển cho người điều phối xử lý đơn hàng <b class='text-primary'>$checkorder->MaDH</b>", 3);

                    $datahis = array(
                        'OrderID' => $ID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => 9,
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);

                    $this->create_export_warehouse($ID);

                    echo "true";

                    return;
                }
            }
        }

        echo "false";
    }

    public function create_export_warehouse($orderid = 0) {

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 3 || $this->user->UserType == 2) {

            if ($orderid > 0) {

                $order = $this->db->query("select * from ttp_report_order where ID=$orderid")->row();

                $TKNO = isset($_POST['TKNO']) ? $_POST['TKNO'] : '.................';

                $TKCO = isset($_POST['TKCO']) ? $_POST['TKCO'] : '.................';

                $KPP = isset($_POST['KPP']) ? $_POST['KPP'] : '';

                $Lydo = isset($_POST['Lydo']) ? $_POST['Lydo'] : '';

                $KhoID = $order->KhoID;

                $hinhthucxuatkho = 1;

                $data = array(
                    'Lydoxuatkho' => $Lydo,
                    'TKNO' => $TKNO,
                    'TKCO' => $TKCO,
                    'KPP' => $KPP,
                    'KhoID' => $KhoID,
                    'Hinhthucxuatkho' => 1
                );



                $thismonth = date('m', time());

                $thisyear = date('Y', time());

                $type = $order->OrderType == 0 ? ' and TypeExport=0' : '';

                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=$hinhthucxuatkho $type")->row();

                $max = $max ? $max->max + 1 : 1;

                $thisyear = date('y', time());

                $hinhthucxuatkho = $hinhthucxuatkho == 1 ? "TA" : "NB";

                $max = "BHOL" . $thisyear . $thismonth . ".$hinhthucxuatkho." . str_pad($max, 5, '0', STR_PAD_LEFT);

                $data['TypeExport'] = 0;

                $data['OrderID'] = $orderid;

                $data['MaXK'] = $max;

                $data['Ngayxuatkho'] = date("Y-m-d H:i:s", time());

                $data['UserID'] = $this->user->ID;

                $this->db->insert("ttp_report_export_warehouse", $data);
            }
        }
    }

    public function syncAllData() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $OrderID = $_GET["OrderID"];

        $Transport = $this->db->query("select OrderCode,CurrentStatus from ttp_transport where OrderID=" . $OrderID)->row();

        $OrderCode = $Transport->OrderCode;

        $CurrentStatus = $Transport->CurrentStatus;



        $res = array();

        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "OrderCode" => $OrderCode
        );

        sleep(5);

        $responseGetOrderInfo = $this->_ghn->GetOrderInfo($shippingOrderRequest);

        $newstatus = ($responseGetOrderInfo) ? $responseGetOrderInfo["CurrentStatus"] : $CurrentStatus;

        if ($newstatus == NULL) {

            $res["OrderID"] = "= NULL";
        } else {

            if ($newstatus != $CurrentStatus) {

                $updateTransport = array(
                    'CurrentStatus' => $newstatus,
                    'Sync' => date('Y-m-d H:i:s', time()),
                    'SyncBy' => $this->user->ID
                );

                $this->db->where("OrderID", $OrderID);

                $this->db->update("ttp_transport", $updateTransport);

                $res["Updated"] = "True";

                $res["CurrentStatus"] = $this->getStatus($newstatus);

                $res["OrderID"] = "Success:" . $OrderID;



                if ($newstatus == "Delivered") {

                    //Cập nhật Order khi trạng thái thành công

                    $updateOrder = array(
                        'CurrentStatus' => $newstatus,
                        'Status' => 0
                    );

                    $this->db->where("OrderID", $OrderID);

                    $this->db->update("ttp_report_order", $updateOrder);



                    //Cập nhật TTP_Report_Orderhistory

                    $dataHis = array(
                        'OrderID' => $OrderID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => 0,
                        "Ghichu" => "Đồng bộ trạng thái đơn hàng thành công",
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $dataHis);
                }
            } else {

                $uTransport = array(
                    'Sync' => date('Y-m-d H:i:s', time()),
                    'SyncBy' => $this->user->ID
                );

                $this->db->where("OrderID", $OrderID);

                $this->db->update("ttp_transport", $uTransport);



                $res["Updated"] = "False";

                $res["CurrentStatus"] = $this->getStatus($CurrentStatus);

                $res["OrderID"] = $OrderID . "|" . date('Y-m-d H:i:s', time()) . "|" . $this->user->ID;
            }
        }

        echo json_encode($res);
    }

    public function syncAll() {

        $this->template->add_title('Sync All | Report Tools');

        $startday = $this->session->userdata("import_startday");

        $startday = $startday != '' ? $startday : date('Y-m-01', time());

        $stopday = $this->session->userdata("import_stopday");

        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $start = 0;

        $status_leader = 0;

        $bonus = $this->user->IsAdmin == 1 || $this->user->UserType == 5 || $this->user->UserType == 8 ? "" : " and a.UserID=" . $this->user->ID;

        if ($this->user->IsAdmin == 0) {

            $checkleader = $this->db->query("select Data from ttp_report_team where UserID=" . $this->user->ID)->row();

            if ($checkleader) {

                $userlist = json_decode($checkleader->Data, true);

                if (count($userlist) > 0) {

                    $userlist[] = $this->user->ID;

                    $userlist = implode(',', $userlist);

                    $bonus = " and a.UserID in(" . $userlist . ")";

                    $status_leader = 1;
                }
            }
        }

        //Lấy ID của những đơn vị có API

        $this->db->select('ID');

        $this->db->where('Publish', 1);

        $this->db->where('Api', 1);

        $arr_transport = $this->db->get("ttp_report_transport")->result();

        $arr = "";

        foreach ($arr_transport as $row) {

            $arr[] = (int) $row->ID;
        }

        $arr = implode(",", $arr);



        $limit_str = "";

        $fillter = $this->session->userdata('fillter');

        $fillter = $fillter == '' ? "" : " and " . $fillter;

        $statusbonus = "";

        $orderby = "a.Ngaydathang DESC";

        switch ($this->user->UserType) {

            case 4:

                $statusbonus = " and a.Status in(11) and a.TransportID in($arr)";

                $statusbonus = $this->user->Channel == 1 ? $statusbonus . " and a.OrderType in(1,2,4,5)" : $statusbonus . " and a.OrderType in(0,3)";

                $orderby = "a.Ngaydathang DESC";

                break;

            default:

                break;
        }

        if ($this->user->UserType == 4) {

            $nav = $this->db->query("

                select DISTINCT a.ID,count(a.ID) as nav ,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.SoluongSP) as SoluongBan,sum(a.Chietkhau) as Chietkhau

                from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_export_warehouse f

                where a.ID=f.OrderID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus")->row();

            $result = $this->db->query("select DISTINCT a.ID,a.MaDH,f.MaXK,a.UserID,a.Status,a.OrderType,a.Accept,a.PaymentStatus,a.Payment,a.Chiphi,a.Ghichu,a.Chietkhau,a.Ngaydathang,a.CustomerID,a.CustomerType,a.SoluongSP,a.Total,a.Ngaydathang,a.TransportID,a.TransportRef,a.Chiphi,a.Reduce,c.Name,c.Phone1,c.Company,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_export_warehouse f where a.ID=f.OrderID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.SourceID!=11 and a.KenhbanhangID!=7 $fillter $statusbonus order by $orderby $limit_str")->result();
        }

        $total = $nav ? $nav->Total : 0;

        $chiphi = $nav ? $nav->Chiphi : 0;

        $soluongban = $nav ? $nav->SoluongBan : 0;

        $chietkhau = $nav ? $nav->Chietkhau : 0;

        $nav = $nav ? $nav->nav : 0;



        $data = array(
            'status_leader' => $status_leader,
            'fill_data' => $this->session->userdata('fillter'),
            'data' => $result,
            'total' => $total,
            'SoluongSP' => $soluongban,
            'chiphi' => $chiphi,
            'chietkhau' => $chietkhau,
            'startday' => $startday,
            'stopday' => $stopday,
            'fillter' => $fillter,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/',
            'start' => $start,
            'find' => $nav,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/report/import_order/index', 5, $nav, $this->limit)
        );

        $this->template->write_view('content', "admin/import_order_home_syncall", $data);

        $this->template->render();
    }

    public function warehouse_excuted() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $this->template->add_title('Report Tools');

        $page = $this->uri->segment(5);

        $start = is_numeric($page) ? $page : 0;

        if (!is_numeric($start))
            $start = 0;



        $limit_str = "limit $start,$this->limit";

        $startday = $this->session->userdata("import_startday");

        $startday = $startday != '' ? $startday : date('Y-m-01', time());

        $stopday = $this->session->userdata("import_stopday");

        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());



        $statusbonus = " and a.Status!=2";

        $orderby = "f.MaXK DESC";

        $bonus = "";

        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '';

        $warehouseid = isset($_GET['warehouseid']) ? (int) $_GET['warehouseid'] : 0;

        $ordertype = isset($_GET['ordertype']) ? (int) $_GET['ordertype'] : 6;

        $bonus = $warehouseid == '' || $warehouseid == 0 ? "" : " and a.KhoID=$warehouseid";

        $bonus = $ordertype == 6 ? $bonus . "" : $bonus . " and a.OrderType=$ordertype";

        $bonus = $keywords == "" ? $bonus . '' : $bonus . " and f.MaXK like '%$keywords%'";

        $view = 'admin/import_order_home_warehouse_excuted';

        $result = $this->db->query("select a.ID,a.OrderType,a.Status,b.ShipmentCode,c.MaKho,d.Amount,e.Title,f.MaXK,f.Ngayxuatkho,e.MaSP from ttp_report_order a,ttp_report_shipment b,ttp_report_warehouse c,ttp_report_orderdetails d,ttp_report_products e,ttp_report_export_warehouse f

            where a.ID=f.OrderID and a.ID=d.OrderID and d.ProductsID=e.ID and d.ShipmentID=b.ID and c.ID=a.KhoID and date(f.Ngayxuatkho)>='$startday' and date(f.Ngayxuatkho)<='$stopday' and a.CustomerID!=9996 $statusbonus $bonus order by $orderby $limit_str")->result();

        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_order a,ttp_report_shipment b,ttp_report_warehouse c,ttp_report_orderdetails d,ttp_report_products e,ttp_report_export_warehouse f

            where a.ID=f.OrderID and a.ID=d.OrderID and d.ProductsID=e.ID and d.ShipmentID=b.ID and c.ID=a.KhoID and date(f.Ngayxuatkho)>='$startday' and date(f.Ngayxuatkho)<='$stopday' and a.CustomerID!=9996 $statusbonus $bonus")->row();

        $bundle_result = $this->db->query("select a.ID,a.OrderType,a.Status,b.ShipmentCode,c.MaKho,d.Amount,e.Title,f.MaXK,f.Ngayxuatkho,e.MaSP from ttp_report_order a,ttp_report_shipment b,ttp_report_warehouse c,ttp_report_orderdetails_bundle d,ttp_report_products e,ttp_report_export_warehouse f

            where a.ID=f.OrderID and a.ID=d.OrderID and d.ProductsID=e.ID and d.ShipmentID=b.ID and c.ID=a.KhoID and date(f.Ngayxuatkho)>='$startday' and date(f.Ngayxuatkho)<='$stopday' and a.CustomerID!=9996 $statusbonus $bonus order by $orderby")->result();

        $nav = $nav ? $nav->nav : 0;

        $nav = $this->lib->nav(base_url() . ADMINPATH . '/report/import_order/warehouse_excuted', 5, $nav, $this->limit);

        $nav = str_replace('href=', 'onclick="navpage(this)" data=', $nav);

        $data = array(
            'data' => $result,
            'warehouseid' => $warehouseid,
            'keywords' => $keywords,
            'ordertype' => $ordertype,
            'bundle' => $bundle_result,
            'startday' => $startday,
            'stopday' => $stopday,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/',
            'nav' => $nav
        );

        $this->template->write_view('content', $view, $data);

        $this->template->render();
    }

    public function quick_view() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $this->template->add_title('Quick View | Report Tools');

        $startday = $this->session->userdata("import_startday");

        $startday = $startday != '' ? $startday : date('Y-m-01', time());

        $stopday = $this->session->userdata("import_stopday");

        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $fillter = $this->session->userdata('fillter');

        $fillter = $fillter == '' ? "" : " and " . $fillter;

        $current_view = isset($_GET['view_type']) ? $_GET['view_type'] : 0;

        $ordertype = isset($_GET['order_type']) ? $_GET['order_type'] : 0;

        switch ($ordertype) {

            case 0:

                $ordertype = " and a.OrderType=0";

                break;

            case 1:

                $ordertype = " and a.OrderType in(1,2,4,5)";

                break;

            case 2:

                $ordertype = " and a.OrderType=3";

                break;

            default:

                $ordertype = "";

                break;
        }

        if ($this->user->UserType == 13) {

            $ordertype = " and a.OrderType in(1,2,4,5)";
        }

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 5 || $this->user->UserType == 3 || $this->user->UserType == 9 || $this->user->UserType == 8 || $this->user->UserType == 13) {

            $view_status = $this->user->UserType == 8 ? "" : " and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday'";

            if ($this->user->UserType == 8) {

                $view_status = $view_status . " and a.Status in(1,7,9,3)";
            }

            if ($current_view == 0) {

                $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype $fillter group by a.ID order by a.Status")->result();

                $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype and a.Status=1 and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' $fillter group by g.SaleReasonID")->result();
            } else {

                $view_status = str_replace('Ngaydathang', 'HistoryEdited', $view_status);

                $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype $fillter group by a.ID order by a.Status")->result();

                $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID $view_status and a.CustomerID!=9996 $ordertype and a.Status=1 $fillter group by g.SaleReasonID")->result();
            }
        } else {

            if ($this->user->IsAdmin == 0) {

                $checkleader = $this->db->query("select Data from ttp_report_team where UserID=" . $this->user->ID)->row();

                if ($checkleader) {

                    $userlist = json_decode($checkleader->Data, true);

                    if (count($userlist) > 0) {

                        $userlist[] = $this->user->ID;

                        $userlist = implode(',', $userlist);

                        if ($current_view == 0) {

                            $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.UserID in(" . $userlist . ") $ordertype $fillter group by a.ID order by a.Status")->result();

                            $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 $ordertype and a.Status=1 and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.UserID in(" . $userlist . ") $fillter group by g.SaleReasonID")->result();
                        } else {

                            $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday' and a.CustomerID!=9996 and a.UserID in(" . $userlist . ") $ordertype $fillter group by a.ID order by a.Status")->result();

                            $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday' and a.CustomerID!=9996 $ordertype and a.Status=1 and a.UserID in(" . $userlist . ") $fillter group by g.SaleReasonID")->result();
                        }
                    }
                } else {

                    if ($current_view == 0) {

                        $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.UserID =" . $this->user->ID . " $ordertype $fillter group by a.ID order by a.Status")->result();

                        $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.CustomerID!=9996 and a.Status=1 and date(a.Ngaydathang)>='$startday' and date(a.Ngaydathang)<='$stopday' and a.UserID =" . $this->user->ID . " $ordertype $fillter group by g.SaleReasonID")->result();
                    } else {

                        $result = $this->db->query("select a.ID,a.MaDH,a.UserID,a.Status,a.Ghichu,a.Ngaydathang,a.CustomerID,a.SoluongSP,a.Total,a.Chiphi,a.Chietkhau,a.Reduce,a.Ngaydathang,c.Name,c.Phone1,d.Title as Thanhpho from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday' and a.CustomerID!=9996 and a.UserID =" . $this->user->ID . " $ordertype $fillter group by a.ID order by a.Status")->result();

                        $reason = $this->db->query("select f.Title,g.SaleReasonID,sum(a.Total) as Total,sum(a.Chiphi) as Chiphi,sum(a.Chietkhau) as Chietkhau,sum(a.Reduce) as Reduce,sum(a.SoluongSP) as SoluongSP,count(a.ID) as SLDH from ttp_report_order a,ttp_report_district b,ttp_report_customer c,ttp_report_city d,ttp_report_area e,ttp_report_order_reason f,ttp_report_order_reason_details g ,ttp_report_orderdetails h,ttp_report_products i where a.ID=h.OrderID and h.ProductsID=i.ID and g.OrderID=a.ID and g.SaleReasonID=f.ID and a.CityID=d.ID and a.DistrictID=b.ID and d.AreaID=e.ID and a.CustomerID=c.ID and date(a.HistoryEdited)>='$startday' and date(a.HistoryEdited)<='$stopday' and a.CustomerID!=9996 and a.Status=1 and a.UserID =" . $this->user->ID . " $ordertype $fillter group by g.SaleReasonID")->result();
                    }
                }
            }
        }



        $data = array(
            'startday' => $startday,
            'stopday' => $stopday,
            'data' => $result,
            'reason' => $reason,
            'fill_data' => $this->session->userdata('fillter'),
            'fillter' => $fillter,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/'
        );

        $this->template->write_view('content', "admin/import_order_quick_view", $data);

        $this->template->render();
    }

    public function setfillter_report() {

        if (isset($_GET['Status'])) {

            if ($_GET['Status'] == 'processing') {

                $_GET['Status'] = 11;
            }

            $status = (int) $_GET['Status'];

            if ($status == 11) {

                $this->session->set_userdata("fillter", "a.Status > 1");
            } else {

                $status = $status < 0 || $status > 10 ? 0 : $status;

                $this->session->set_userdata("fillter", "a.Status = $status");
            }
        } else {

            $this->session->set_userdata("fillter", "");
        }

        redirect(ADMINPATH . "/report/import_order");
    }

    public function setfillter() {

        $arr_fieldname = array(0 => "c.Name", 1 => "a.CustomerType", 2 => "e.ID", 3 => "d.ID", 4 => "b.ID", 5 => "a.SoluongSP", 6 => "a.Total", 7 => "a.Chiphi", 8 => "a.Status", 9 => "a.UserID", 10 => "a.TransportID", 11 => "c.Phone1", 12 => "a.MaDH", 13 => "f.MaXK", 14 => "a.KhoID", 15 => "i.MaSP", 16 => "i.Title", 17 => "i.CategoriesID", 18 => "i.TradeMarkID", 19 => "a.FinanceMoney");

        $arr_oparation = array(0 => 'like', 1 => '=', 2 => '!=', 3 => '>', 4 => '<', 5 => '>=', 6 => '<=');

        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array();

        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array();

        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array();

        $OparationVs = isset($_POST['OparationVs']) ? $_POST['OparationVs'] : array();

        if (count($FieldName) > 0) {

            $str = array();

            $i = 0;

            foreach ($FieldName as $key => $value) {

                if (isset($arr_fieldname[$value]) && isset($FieldOparation[$i])) {

                    if (isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])) {

                        if ($value == 17) {

                            $str[] = $arr_fieldname[$value] . " like '%\"" . mysql_real_escape_string($FieldText[$i]) . "\"%'";
                        } else {

                            if ($arr_oparation[$FieldOparation[$i]] == 'like') {

                                $str[] = $arr_fieldname[$value] . ' ' . $arr_oparation[$FieldOparation[$i]] . " '%" . mysql_real_escape_string($FieldText[$i]) . "%'";
                            } else {

                                $str[] = $arr_fieldname[$value] . ' ' . $arr_oparation[$FieldOparation[$i]] . " '" . mysql_real_escape_string($FieldText[$i]) . "'";
                            }
                        }

                        $i++;
                    }
                }
            }

            if (count($str) > 0) {

                $sql = implode(' and ', $str);

                $this->session->set_userdata("fillter", $sql);
            }
        } else {

            $this->session->set_userdata("fillter", "");
        }

        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

        redirect($referer);
    }

    public function export_warehouse() {

        $startday = $this->session->userdata("import_warehouse_startday");

        $startday = $startday != '' ? $startday : date('Y-m-01', time());

        $stopday = $this->session->userdata("import_warehouse_stopday");

        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $searchkey = isset($_GET['search_key']) ? mysql_real_escape_string($_GET['search_key']) : '';

        $searchkey = trim($searchkey);

        $page = $this->uri->segment(5);

        $start = is_numeric($page) ? $page : 0;

        if (!is_numeric($start))
            $start = 0;

        $limit_str = "limit $start,$this->limit";

        $bonus = '';

        if ($this->user->Channel == 0) {

            $bonus = " and b.OrderType=0";
        }

        if ($this->user->Channel == 1) {

            $bonus = " and b.OrderType in(1,2,4,5)";
        }

        if ($this->user->Channel == 2) {

            $bonus = " and b.OrderType=3";
        }

        if ($this->user->UserType == 7 || $this->user->IsAdmin == 1) {

            $bonus = "";
        }

        $payment = isset($_GET['Payment']) ? $_GET['Payment'] : -1;

        if ($payment >= 0) {

            $temp = (int) $payment;

            $bonus .= " and b.FinanceMoney=$temp";
        }

        $nav = $this->db->query("select count(1) as nav from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d,ttp_report_customer e where e.ID=b.CustomerID and a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and Hinhthucxuatkho in(0,1) $bonus and (a.MaXK like '%$searchkey%' or e.Name like '%$searchkey%' or b.TransportRef like '%$searchkey%')")->row();

        $result = $this->db->query("select a.*,b.MaDH,c.UserName,d.MaKho as MaKho,b.ID as IDDH,b.Status,b.TransportRef,b.FinanceMoney from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d,ttp_report_customer e where e.ID=b.CustomerID and a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and Hinhthucxuatkho in(0,1) $bonus and (a.MaXK like '%$searchkey%' or e.Name like '%$searchkey%' or b.TransportRef like '%$searchkey%') order by a.ID DESC $limit_str")->result();



        $nav = $nav ? $nav->nav : 0;



        $data = array(
            'data' => $result,
            'base_link' => base_url() . ADMINPATH . '/report/import_order/',
            'start' => $start,
            'startday' => $startday,
            'stopday' => $stopday,
            'payment' => $payment,
            'find' => $nav,
            'search' => $searchkey,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/report/import_order/export_warehouse', 5, $nav, $this->limit)
        );

        $view = 'admin/import_order_export_warehouse';

        $this->template->add_title('Phiếu xuất kho đã xuất');

        $this->template->write_view('content', $view, $data);

        $this->template->render();
    }

    public function excel_export_warehouse() {

        $startday = $this->session->userdata("import_warehouse_startday");

        $startday = $startday != '' ? $startday : date('Y-m-01', time());

        $stopday = $this->session->userdata("import_warehouse_stopday");

        $stopday = $stopday != '' ? $stopday : date('Y-m-d', time());

        $bonus = '';

        if ($this->user->Channel == 0) {

            $bonus = " and b.OrderType=0";
        }

        if ($this->user->Channel == 1) {

            $bonus = " and b.OrderType in(1,2,4,5)";
        }

        if ($this->user->Channel == 2) {

            $bonus = " and b.OrderType=3";
        }

        if ($this->user->UserType == 7 || $this->user->IsAdmin == 1) {

            $bonus = "";
        }

        $payment = isset($_GET['Payment']) ? $_GET['Payment'] : -1;

        if ($payment >= 0) {

            $temp = (int) $payment;

            $bonus .= " and a.TransferMoney=$temp";
        }

        $result = $this->db->query("select a.*,b.MaDH,c.UserName,d.MaKho as MaKho,b.ID as IDDH,b.Status from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c,ttp_report_warehouse d,ttp_report_customer e where e.ID=b.CustomerID and a.KhoID=d.ID and b.UserID=c.ID and a.OrderID=b.ID and date(a.Ngayxuatkho)>='$startday' and date(a.Ngayxuatkho)<='$stopday' and Hinhthucxuatkho in(0,1) $bonus order by a.ID DESC")->result();

        if (count($result) > 0) {

            ini_set('memory_limit', '3500M');

            error_reporting(E_ALL);

            ini_set('display_errors', TRUE);

            ini_set('display_startup_errors', TRUE);

            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');

            require_once 'public/plugin/PHPExcel.php';

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                    ->setLastModifiedBy("Maarten Balliauw")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'Ngày ra xuất kho')
                    ->setCellValue('B1', 'Mã phiếu xuất kho')
                    ->setCellValue('C1', 'Hình thức')
                    ->setCellValue('D1', 'Mã kho')
                    ->setCellValue('E1', 'User')
                    ->setCellValue('F1', 'Trạng thái đơn hàng')
                    ->setCellValue('G1', 'Tình trạng đối soát');

            $arr_status = $this->define_model->get_order_status('status', 'order');

            $array_status = array();

            foreach ($arr_status as $key => $ite) {

                $code = (int) $ite->code;

                $array_status[$code] = $ite->name;
            }



            $arr_payment = $this->define_model->get_order_status('payment', 'payment_status');

            $array_payment = array();

            foreach ($arr_payment as $key => $ite) {

                $code = (int) $ite->code;

                $array_payment[$code] = $ite->name;
            }



            $arr_hinhthuc = array(0 => "NB", 1 => "TA");

            $i = 2;

            foreach ($result as $row) {

                $hinhthuc = isset($arr_hinhthuc[$row->Hinhthucxuatkho]) ? $arr_hinhthuc[$row->Hinhthucxuatkho] : "";

                $payment = isset($array_payment[$row->TransferMoney]) ? $array_payment[$row->TransferMoney] : "";

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, date('d/m/Y', strtotime($row->Ngayxuatkho)))
                        ->setCellValue('B' . $i, $row->MaXK)
                        ->setCellValue('C' . $i, $hinhthuc)
                        ->setCellValue('D' . $i, $row->MaKho)
                        ->setCellValue('E' . $i, $row->UserName)
                        ->setCellValue('F' . $i, $array_status[$row->Status])
                        ->setCellValue('G' . $i, $payment);

                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('BC_DOISOAT');

            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="BC_DOISOAT.xls"');

            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');

            header('Cache-Control: cache, must-revalidate');

            header('Pragma: public');



            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            ob_end_clean();

            $objWriter->save('php://output');

            exit;
        } else {

            echo "Data is empty !";
        }
    }

    public function edit_warehouse($id = 0) {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        if (!is_numeric($id))
            return;

        $result = $this->db->query("select a.*,b.MaDH,c.UserName from ttp_report_export_warehouse a,ttp_report_order b,ttp_user c where b.UserID=c.ID and a.OrderID=b.ID and a.ID=$id")->row();

        if ($result) {

            $this->template->add_title('Edit export warehouse | Import Tools');

            $data = array(
                'data' => $result,
                'base_link' => base_url() . ADMINPATH . '/report/import_order/'
            );

            $view = 'admin/import_order_export_warehouse_edit';

            $this->template->write_view('content', $view, $data);

            $this->template->render();
        } else {

            redirect(ADMINPATH . "/report/import_order_export_warehouse");
        }
    }

    public function update_export_warehouse() {

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        $Lydoxuatkho = isset($_POST['Lydoxuatkho']) ? $_POST['Lydoxuatkho'] : '';

        $TKNO = isset($_POST['TKNO']) ? $_POST['TKNO'] : '';

        $TKCO = isset($_POST['TKCO']) ? $_POST['TKCO'] : '';

        $KPP = isset($_POST['KPP']) ? $_POST['KPP'] : '';

        $data = array(
            'Lydoxuatkho' => $Lydoxuatkho,
            'TKNO' => $TKNO,
            'TKCO' => $TKCO,
            'KPP' => $KPP
        );

        $this->db->where("ID", $ID);

        $this->db->update("ttp_report_export_warehouse", $data);

        redirect(ADMINPATH . "/report/import_order/export_warehouse");
    }

    public function add() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add Order | Import Tools');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/report/import_order/'
        );
        $view = $this->user->IsAdmin == 1 ? 'admin/import_order_add' : 'admin/import_order_add_user';
        $view = $this->user->UserType == 10 ? 'admin/import_order_add_saleuser' : $view;
        $this->template->write_view('content', $view, $data);
        $this->template->render();
    }

    public function add_old() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add Old Order | Import Tools');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/report/import_order/'
        );
        $view = $this->user->IsAdmin == 1 ? 'admin/import_order_add' : 'admin/import_order_add_old_user';
        $this->template->write_view('content', $view, $data);
        $this->template->render();
    }

    public function remove($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        $segment = end($this->uri->segment_array());
        if (md5($id . 'REMOVE') == $segment) {
            $this->db->where('ID', $id);
            $this->db->delete('ttp_report_order');
            
            $this->db->where('OrderID', $id);
            $this->db->delete('ttp_report_orderdetails');
            
            $this->db->where('OrderID', $id);
            $this->db->delete('ttp_report_orderhistory');
        }

        redirect(ADMINPATH . "/report/import_order");
    }

    public function edit($id = 0) {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (!is_numeric($id))
            return;

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 5 || $this->user->UserType == 13) {
            $result = $this->db->query("select a.*,b.Name,b.Company,b.Phone1,b.Phone2,c.UserName,b.Birthday,b.Age,b.Code,b.Address,b.ImageCMND,b.CMND,b.GroupID from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and a.ID=$id")->row();
        } else {
            $checkleader = $this->db->query("select Data from ttp_report_team where UserID=" . $this->user->ID)->row();
            if ($checkleader) {
                $userlist = json_decode($checkleader->Data, true);
                if (count($userlist) > 0) {
                    $userlist = implode(',', $userlist);
                    $bonus = " and a.UserID in(" . $userlist . ")";
                    $result = $this->db->query("select a.*,b.Company,b.Name,b.Phone1,b.Phone2,c.UserName,b.Birthday,b.Age,b.Code,b.Address,b.ImageCMND,b.CMND,b.GroupID from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and a.ID=$id and a.UserID in(" . $userlist . ")")->row();
                }
            } else {
                $result = $this->db->query("select a.*,b.Company,b.Name,b.Phone1,b.Phone2,c.UserName,b.Birthday,b.Age,b.Code,b.Address,b.ImageCMND,b.CMND,b.GroupID from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and a.ID=$id and a.UserID=" . $this->user->ID)->row();
            }
        }



        //Hiển thị Tạo vận đơn
        $this->db->select('ID');
        $this->db->where('Publish', 1);
        $this->db->where('Api', 1);
        $aa = $this->db->get("ttp_report_transport")->result();
        $arr = array();
        foreach ($aa as $row) {
            $arr[] = (int) $row->ID;
        }

        $isDisabled = "";
        $isStyle = "";
        if (in_array($result->TransportID, $arr)) {
            $isDisabled = " disabled ";
            $isStyle = ' style="display:none"';
        }

        if ($result) {
            $prize = $this->db->query("select a.* from ttp_result_luckydraw a,ttp_report_customer b where a.CustomerID=b.ID and b.Phone1='$result->Phone1'")->result();
            $arr_message = array("UserID" => $this->user->ID, "Level" => $this->user->UserType, "Time" => time());
            $this->template->add_title('Edit Order | Import Tools');
            $data = array(
                'data' => $result,
                'base_link' => base_url() . ADMINPATH . '/report/import_order/',
                'arr' => $arr,
                'dis' => $isDisabled,
                'style' => $isStyle,
                'prize' => $prize
            );

            $view = $this->user->IsAdmin == 1 ? 'admin/import_order_edit' : 'admin/import_order_edit_user';
            $view = $this->user->UserType == 10 || $this->user->UserType == 13 ? 'admin/import_order_edit_usersale' : $view;
            $this->template->write_view('content', $view, $data);
            $this->template->render();
        } else {
            redirect(ADMINPATH . "/report/import_order");
        }
    }

    public function preview($id) {

        if (!is_numeric($id))
            return;

        switch ($this->user->UserType) {

            case 1:

                $statusbonus = " and a.Status in(2,3) and a.UserID=" . $this->user->ID;

                break;

            case 2:

                $statusbonus = " and a.Status in(3,4)";

                break;

            case 3:

                $statusbonus = " and a.Status in(5,6)";

            case 7:

                $statusbonus = " and a.Status in(5,6)";

                break;

            case 4:

                $statusbonus = " and a.Status in(7,8,9,11)";

                break;

            default :

                $statusbonus = " and 1=2";
        }

        $statusbonus = $this->user->IsAdmin == 1 ? "" : $statusbonus;

        $result = $this->db->query("select a.*,b.Name,b.Code,b.Phone1,b.Phone2,c.UserName,b.Birthday,b.CMND,b.Age,b.ImageCMND from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and a.ID=$id $statusbonus")->row();

        if ($result) {

            $this->template->add_title('Preview Order | Import Tools');



            //Hiển thị Tạo vận đơn

            $this->db->select('ID');

            $this->db->where('Publish', 1);

            $this->db->where('Api', 1);

            $aa = $this->db->get("ttp_report_transport")->result();

            $arr = array();

            foreach ($aa as $row) {

                $arr[] = (int) $row->ID;
            }



            $isDisabled = "";

            $isStyle = "";

            if (in_array($result->TransportID, $arr)) {

                $isDisabled = " disabled ";

                $isStyle = ' style="display:none"';
            }



            $data = array(
                'data' => $result,
                'base_link' => base_url() . ADMINPATH . '/report/import_order/',
                'arr' => $arr,
                'dis' => $isDisabled,
                'style' => $isStyle
            );

            $view = $result->OrderType == 4 || $result->OrderType == 5 ? 'admin/import_order_preview_sale' : "admin/import_order_preview";

            $this->template->write_view('content', $view, $data);

            $this->template->render();
        } else {



            if ($this->user->UserType == 1 || $this->user->UserType == 10) {

                $statusbonus = " and a.UserID=" . $this->user->ID;

                $result = $this->db->query("select a.*,b.Name,b.Code,b.Phone1,b.Phone2,c.UserName,b.CMND,b.Birthday,b.Age,b.ImageCMND from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and a.ID=$id $statusbonus")->row();
            } else {

                $result = $this->db->query("select a.*,b.Name,b.Code,b.Phone1,b.Phone2,c.UserName,b.Birthday,b.Age,b.ImageCMND,b.CMND from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and a.ID=$id")->row();
            }

            if ($result) {

                $this->template->add_title('Preview Order | Import Tools');

                $data = array(
                    'data' => $result,
                    'base_link' => base_url() . ADMINPATH . '/report/import_order/'
                );

                $view = $result->OrderType == 4 || $result->OrderType == 5 ? 'admin/import_order_preview_sale_view' : "admin/import_order_preview_view";

                $this->template->write_view('content', $view, $data);

                $this->template->render();
            } else {

                redirect(ADMINPATH . "/report/import_order");
            }
        }
    }

    public function getcus() {

        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

        $find = strrpos($refer, "administrator/report/import_order/add");

        if (!is_numeric($find))
            return;

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 1 || $this->user->UserType == 5 || $this->user->UserType == 10) {

            $sdt_cus = isset($_POST['sdt_cus']) ? $_POST['sdt_cus'] : '';

            $name_cus = isset($_POST['name_cus']) ? $_POST['name_cus'] : '';

            $where = array();

            if ($sdt_cus != '') {

                $where[] = "(Phone1 like '%$sdt_cus%' or Phone2 like '%$sdt_cus%')";
            }

            if ($name_cus != '') {

                $where[] = "Name like '%$name_cus%'";
            }

            $where = count($where) > 0 ? implode(' and ', $where) : "";

            if ($where != '') {

                $result = $this->db->query("select a.*,b.Title as GroupTitle from ttp_report_customer a,ttp_report_customer_group b where a.GroupID=b.ID and $where limit 0,200")->result();
            } else {

                $result = $this->db->query("select a.*,b.Title as GroupTitle from ttp_report_customer a,ttp_report_customer_group b where a.GroupID=b.ID limit 0,200")->result();
            }

            if (count($result) > 0) {

                echo "<table><tr><th>Tên khách hàng</th><th>Số điện thoại</th><th>Địa chỉ</th><th>Nhóm khách hàng</th></tr>";

                foreach ($result as $row) {

                    $Phone1 = substr_replace($row->Phone1, str_repeat("x", 6), 0, 6);

                    if ($row->Name != '') {

                        echo "<tr>";

                        echo "<td class='name_td'><a onclick='select_this_custom(this,$row->ID,\"" . date('d/m/Y', strtotime($row->Birthday)) . "\",\"$row->CMND\",\"$row->Phone1\",\"$row->Phone2\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->GroupID\")'>$row->Name</a></td>";

                        echo "<td><a onclick='select_this_custom(this,$row->ID,\"" . date('d/m/Y', strtotime($row->Birthday)) . "\",\"$row->CMND\",\"$row->Phone1\",\"$row->Phone2\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->GroupID\")'>" . $Phone1 . "</a></td>";

                        echo "<td><a onclick='select_this_custom(this,$row->ID,\"" . date('d/m/Y', strtotime($row->Birthday)) . "\",\"$row->CMND\",\"$row->Phone1\",\"$row->Phone2\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->GroupID\")'>" . $row->Address . "</a></td>";

                        echo '<td style="width: 130px;"><a onclick="select_this_custom(this,' . $row->ID . ',\'' . date('d/m/Y', strtotime($row->Birthday)) . '\',\'$row->CMND\',\'' . $row->Phone1 . '\',\'' . $row->Phone2 . '\',\'' . str_replace("'", '"', $row->Address) . '\',\'$row->GroupID\')">' . $row->GroupTitle . '</a></td>';

                        echo "</tr>";
                    }
                }

                echo "</table>";

                return;
            }
        }

        echo "false";
    }

    public function getcusmtgt() {

        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 1 || $this->user->UserType == 5 || $this->user->UserType == 10) {

            $name_cus = isset($_POST['name_cus']) ? $_POST['name_cus'] : '';

            $type = isset($_POST['type']) ? $_POST['type'] : 1;

            $where = array();

            if ($name_cus != '')
                $where[] = "Name like '%$name_cus%'";

            if ($type != '')
                $where[] = "Type = $type";

            $where = count($where) > 0 ? implode(' and ', $where) : "";

            if ($where != '') {

                $result = $this->db->query("select * from ttp_report_customer where $where limit 0,200")->result();
            } else {

                $result = $this->db->query("select * from ttp_report_customer limit 0,200")->result();
            }

            if (count($result) > 0) {

                if ($type == 1 || $type == 4 || $type == 5) {

                    echo "<table><tr><th>Tên khách hàng</th><th>Số điện thoại</th><th>Địa chỉ</th><th></th></tr>";

                    foreach ($result as $row) {

                        echo "<tr>";

                        echo "<td class='name_td'><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\")'>$row->Name</a></td>";

                        echo "<td><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\")'>" . $row->Phone1 . "</a></td>";

                        echo "<td><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\")'>" . $row->AddressOrder . "</a></td>";

                        echo "</tr>";
                    }

                    echo "</table>";
                } else {

                    $system = $this->db->query("select * from ttp_report_system")->result();

                    $arr_system = array();

                    if (count($system) > 0) {

                        foreach ($system as $row) {

                            $arr_system[$row->ID] = $row->Title;
                        }
                    }

                    echo "<table><tr><th>Mã siêu thị</th><th>Hệ thống</th><th>Cửa hàng</th><th>Địa chỉ giao hàng</th><th></th></tr>";

                    foreach ($result as $row) {

                        $system = isset($arr_system[$row->SystemID]) ? $arr_system[$row->SystemID] : '--';

                        echo "<tr>";

                        echo "<td class='name_td' style='width:120px'><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\",\"$row->Surrogate\",\"$row->Phone2\",\"$row->Company\")'>$row->Code</a></td>";

                        echo "<td><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\",\"$row->Surrogate\",\"$row->Phone2\",\"$row->Company\")'>" . $system . "</a></td>";

                        echo "<td><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\",\"$row->Surrogate\",\"$row->Phone2\",\"$row->Company\")'>" . $row->Name . "</a></td>";

                        echo "<td style='width:250px'><a onclick='select_this_custom(this,$row->ID,\"$row->Phone1\",\"$row->Code\",\"" . str_replace('"', "'", $row->AddressOrder) . "\",\"" . str_replace('"', "'", $row->Address) . "\",\"$row->Name\",\"$row->Surrogate\",\"$row->Phone2\",\"$row->Company\")'>" . $row->AddressOrder . "</a></td>";

                        echo "</tr>";
                    }

                    echo "</table>";
                }

                return;
            }
        }

        echo "false";
    }

    public function get_reduce() {

        $data = $this->db->query("select * from ttp_report_reduce where Published=1")->result();

        if (count($data) > 0) {

            echo "<table><tr><th></th><th>Loại giảm trừ</th><th>Mô tả thêm</th><th>Giá trị giảm trừ</th></tr>";

            foreach ($data as $row) {

                echo "<tr>";

                echo "<td style='width:30px'><input type='checkbox' class='selected_reduce' data-id='$row->ID' data-name='$row->Title' /></td>";

                echo "<td style='width: 400px'>$row->Title</td>";

                echo "<td><input type='text' class='timereduce form-control' style='width:180px;height:30px;padding:0px 5px' /></td>";

                echo "<td><input type='number' class='valuereduce form-control' style='width:180px;height:30px;padding:0px 5px' value='0' /></td>";

                echo "</tr>";
            }

            echo "</table>";

            echo "<div class='fixedtools'><a class='btn btn-box-inner' onclick='add_reduce()'><i class='fa fa-check-square' style='margin-right:5px'></i> Thêm giảm trừ</a></div>";
        } else {

            echo "false";
        }
    }

    public function set_source() {

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        if (is_numeric($ID) && $ID > 0) {

            $order = $this->db->query("select SourceID from ttp_report_order where CustomerID=$ID order by ID DESC limit 0,1")->row();

            echo $order ? $order->SourceID : '0';
        }
    }

    public function get_history() {

        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

        $find = strrpos($refer, "administrator/report/import_order");

        if (!is_numeric($find))
            return;

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        $result = $this->db->query("select a.*,b.AddressOrder,b.Status,b.Total,b.Ngaydathang,b.MaDH from ttp_report_customer a,ttp_report_order b where a.ID=b.CustomerID and a.ID=$ID")->result();

        if (count($result) > 0) {

            echo "<table><tr><th>Mã ĐH</th><th>Ngày đặt hàng</th><th>Địa chỉ giao hàng</th><th>Tổng tiền hàng</th></tr>";

            foreach ($result as $row) {

                echo "<tr>";

                echo "<td>" . $row->MaDH . "</td>";

                echo "<td>" . $row->Ngaydathang . "</td>";

                echo "<td>" . $row->AddressOrder . "</td>";

                echo "<td>" . number_format($row->Total) . "</td>";

                echo "</tr>";
            }

            echo "</table>";

            return;
        }

        echo "false";
    }

    public function get_city_by_area() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        echo "<option value=''>-- Chọn tỉnh thành --</option>";

        if (is_numeric($ID)) {

            $result = $this->db->query("select * from ttp_report_city where AreaID=$ID order by Title ASC")->result();

            if (count($result) > 0) {

                foreach ($result as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }
            }
        }
    }

    public function get_district_by_city() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        $TypeOrder = isset($_POST['Type']) ? $_POST['Type'] : 0;

        $type = $TypeOrder == 1 ? 'GT' : 'DefaultWarehouse';

        $type = $TypeOrder == 2 ? 'MT' : $type;

        $data = array('DistrictHtml' => "<option value='0'>-- Chọn quận huyện --</option>", 'WarehouseHtml' => '<option value="">Không có kho hàng</option>');

        if (is_numeric($ID)) {

            $result = $this->db->query("select * from ttp_report_district where CityID=$ID order by Title ASC")->result();

            if (count($result) > 0) {

                foreach ($result as $row) {

                    $data['DistrictHtml'] .= "<option value='$row->ID'>$row->Title</option>";
                }
            }

            if ($TypeOrder == 4 || $TypeOrder == 5) {

                $data['WarehouseHtml'] = "<option value='1'>DOHATO</option>";
            } else {

                $warehouse = $this->db->query("select b.MaKho,b.ID from ttp_report_city a,ttp_report_warehouse b where a.$type=b.ID and a.ID=$ID")->row();

                if ($warehouse) {

                    if ($this->user->Channel == 2) {

                        $data['WarehouseHtml'] = "<option value='1'>DOHATO</option>";
                    } else {

                        $data['WarehouseHtml'] = "<option value='$warehouse->ID'>$warehouse->MaKho</option>";
                    }
                }
            }
        }

        echo json_encode($data);
    }

    public function getfee() {

        $id = isset($_POST['district']) ? $_POST['district'] : 0;

        $total = isset($_POST['total']) ? $_POST['total'] : 0;

        $check = $this->db->query("select * from ttp_report_district where ID=$id")->row();

        $fee = 0;

        if ($check) {

            if ($total < 499000) {

                $fee = $check->PriceCost;
            }

            if ($total >= 499000 && $total < 2000000) {

                $fee = $check->PriceCost1;
            }

            if ($total >= 2000000) {

                $fee = $check->PriceCost2;
            }

            if ($total >= 499000) {

                $fee = 0;
            }
        }

        echo $fee;
    }

    public function get_products() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '';

        $KhoID = isset($_POST['KhoID']) ? mysql_real_escape_string($_POST['KhoID']) : 0;

        if ($Title != '' && $Title != '*') {

            $result = $this->db->query("select a.* from ttp_report_products a where a.Title like '%$Title%' or a.MaSP like '$Title'")->result();
        } else {

            $result = $this->db->query("select * from ttp_report_products order by ID DESC limit 0,50")->result();
        }



        if (count($result) > 0) {

            $products = array();

            $str = "<div class='tools_search_products'>

                        <span>Tìm kiếm sản phẩm: </span>

                        <span><input type='text' placeholder='Nhập mã SKU hoặc tên sản phẩm' onchange='input_search_products(this)' /></span>

                    </div>

                    <table><tr><th></th><th>Mã Sản phẩm</th><th>Sản phẩm</th>

                    <th>

                        <select class='select_products' onchange='fillter_categories(this)'><option value='0'>-- Tất cả ngành hàng --</option>__</select>

                    </th></tr>";

            $categories = $this->db->query("select * from ttp_report_categories")->result();

            $arr_categories = array();

            if (count($categories)) {

                foreach ($categories as $row) {

                    $arr_categories[$row->ID] = $row->Title;
                }
            }

            if (count($result) > 0) {

                $option = array();

                foreach ($result as $row) {

                    if (!isset($products[$row->ID])) {

                        $products[$row->ID] = $row->ID;

                        $row->CategoriesID = json_decode($row->CategoriesID, true);

                        $row->CategoriesID = is_array($row->CategoriesID) ? $row->CategoriesID : array();

                        $class = '';

                        $current_categories = array();

                        if (count($row->CategoriesID)) {

                            foreach ($row->CategoriesID as $item) {

                                if (isset($arr_categories[$item])) {

                                    $current_categories[] = $arr_categories[$item];

                                    if (!isset($option[$item]))
                                        $option[$item] = "<option value='" . $item . "'>" . $arr_categories[$item] . "</option>";
                                }

                                $class .= " categories_$item";
                            }
                        }

                        $specialstart = strtotime($row->SpecialStartday);

                        $specialstop = strtotime($row->SpecialStopday);

                        $curday = date('Y-m-d');

                        $curday = strtotime($curday);

                        if ($curday <= $specialstop && $curday >= $specialstart) {

                            $row->Price = $row->SpecialPrice;
                        }

                        $str .= "<tr class='trcategories $class'>";

                        $str .= "<td style='width:30px'><input type='checkbox' class='selected_products' data-id='$row->ID' data-code='$row->MaSP' data-name='$row->Title' data-price='$row->Price' /></td>";

                        $str .= "<td style='width: 130px;'>" . $row->MaSP . "</td>";

                        $str .= "<td>" . $row->Title . "</td>";

                        $str .= "<td style='width:170px'>" . implode(',', $current_categories) . "</td>";

                        $str .= "</tr>";
                    }
                }
            }



            $option = implode('', $option);

            echo str_replace("__", $option, $str);

            echo "</table>";

            echo "<div class='fixedtools'><a class='btn btn-danger btn-box-inner' onclick='add_products()'><i class='fa fa-reply-all'></i> Đưa vào đơn hàng</a></div>";

            return;
        }

        echo "false";
    }

    public function add_new_order() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1) {

            $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0;
        } else {

            $UserCreated = isset($_POST["UserCreated"]) ? $_POST["UserCreated"] : 0;

            if ($UserCreated == 1) {

                $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0;
            } else {

                $UserID = $this->user->ID;
            }
        }

        $Voucher = isset($_POST['Voucher']) ? $_POST['Voucher'] : '';

        $Voucher = $this->voucher($Voucher);

        $Ngayban = isset($_POST["Ngayban"]) ? $_POST["Ngayban"] : date('m/d/Y', time());

        $Ngayban = strtotime($Ngayban);

        $Ngayban = date('Y-m-d', $Ngayban);

        $Gioban = date('H:i:s', time());

        $DayDeliveryTime = isset($_POST["DayDeliveryTime"]) ? $_POST["DayDeliveryTime"] : date('Y-m-d', time());

        $HourDeliveryTime = isset($_POST["HourDeliveryTime"]) ? $_POST["HourDeliveryTime"] : date('H', time());

        $DeliveryTime = $DayDeliveryTime . ' ' . $HourDeliveryTime . ":01:01";

        $Loaikhachhang = isset($_POST["Loaikhachhang"]) ? $_POST["Loaikhachhang"] : 0;

        $CustomerID = isset($_POST["CustomerID"]) ? $_POST["CustomerID"] : 0;

        $Tenkhachhang = isset($_POST["Tenkhachhang"]) ? trim($_POST["Tenkhachhang"]) : '';

        $NTNS = isset($_POST["NTNS"]) ? $_POST["NTNS"] : '';

        if ($NTNS != '') {

            $NTNS = explode('/', $NTNS);

            $NTNS = isset($NTNS[2]) && isset($NTNS[1]) && isset($NTNS[0]) ? $NTNS[2] . "-" . $NTNS[1] . "-" . $NTNS[0] : date('Y-m-d', time());
        }

        $KhoID = isset($_POST["KhoID"]) ? (int) $_POST["KhoID"] : 0;

        $CMND = isset($_POST["CMND"]) ? $_POST["CMND"] : '';

        $SourceID = isset($_POST["SourceID"]) ? $_POST["SourceID"] : 0;

        $Address = isset($_POST["Address"]) ? $_POST["Address"] : '';

        $CityID = isset($_POST["CityID"]) ? $_POST["CityID"] : 0;

        $DistrictID = isset($_POST["DistrictID"]) ? $_POST["DistrictID"] : 0;
        
        $Ward = isset($_POST["Ward"]) ? $_POST["Ward"] : '';

        $GroupID = isset($_POST["GroupID"]) ? $_POST["GroupID"] : 0;

        $Phone1 = isset($_POST["Phone1"]) ? trim($_POST["Phone1"]) : '';

        $Phone2 = isset($_POST["Phone2"]) ? trim($_POST["Phone2"]) : '';

        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';

        $phantramchietkhau = isset($_POST["phantramchietkhau"]) ? $_POST["phantramchietkhau"] : 0;

        $chiphivanchuyen = isset($_POST["chiphivanchuyen"]) ? $_POST["chiphivanchuyen"] : 0;

        $Tinhtrangdonhang = isset($_POST["Tinhtrangdonhang"]) ? $_POST["Tinhtrangdonhang"] : 2;

        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';

        $KenhbanhangID = isset($_POST["KenhbanhangID"]) ? $_POST["KenhbanhangID"] : 0;

        $PTthanhtoan = isset($_POST["PTthanhtoan"]) ? $_POST["PTthanhtoan"] : 0;

        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();

        $Amout = isset($_POST["Amout"]) ? $_POST["Amout"] : array();

        $Price = isset($_POST["Price"]) ? $_POST["Price"] : array();

        $PriceDown = isset($_POST["GiaCK"]) ? $_POST["GiaCK"] : array();

        $Khuyenmai = isset($_POST["Khuyenmai"]) ? $_POST["Khuyenmai"] : array();

        $arr_error = array();

        if ($SourceID < 1) {
            $arr_error[] = "Chưa chọn nguồn đơn hàng.";
        }

        if ($KenhbanhangID < 1) {
            $arr_error[] = "Chưa chọn kênh bán hàng.";
        }

        if ($Tenkhachhang == '') {
            $arr_error[] = "Tên khách hàng chưa điền.";
        }

        if ($Phone1 == '') {
            $arr_error[] = "Số di động 1 chưa điền.";
        }

        if ($CityID < 1) {
            $arr_error[] = "Chưa chọn Tỉnh / Thành phố .";
        }

        if ($DistrictID < 1) {
            $arr_error[] = "Chưa chọn Quận / Huyện .";
        }

        if (count($ProductsID) < 1) {
            $arr_error[] = "Chưa chọn sản phẩm vào đơn hàng.";
        }

        if ($UserID < 1) {
            $arr_error[] = "Không xác định nhân viên bán hàng.";
        }

        if ($KhoID < 1) {
            $arr_error[] = "Chưa chọn kho lấy hàng.";
        }

        if (count($arr_error) > 0) {
            $this->response['error'] = true;
        }

        if ($this->response['error'] == false) {

            if ($Loaikhachhang == 0) {

                if ($Tenkhachhang != '' && $Address != '' && $Phone1 != '') {

                    $customer = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();

                    if ($customer) {

                        $CustomerID = $customer->ID;

                        $Loaikhachhang = 1;
                    } else {

                        $checkCMND = $this->db->query("select ID from ttp_report_customer where CMND='$CMND' and CMND!=''")->row();

                        if ($checkCMND) {

                            $arr_error[] = "Số CMND của đã được người khác sử dụng.";

                            $this->response['error'] = true;

                            $this->response['message'] = $arr_error;

                            echo json_encode($this->response);

                            return;
                        }

                        $data_customer = array(
                            'Name' => $Tenkhachhang,
                            'Birthday' => $NTNS,
                            'CMND' => $CMND,
                            'Address' => $Address,
                            'Phone1' => $Phone1,
                            'Phone2' => $Phone2,
                            'UserID' => $UserID,
                            'GroupID' => $GroupID,
                            'CityID' => $CityID,
                            'DistrictID' => $DistrictID,
                            'Ward' => $Ward,
                            
                        );

                        $data_customer['Type'] = $this->user->Channel == 2 ? 3 : 0;

                        $this->db->insert("ttp_report_customer", $data_customer);

                        $CustomerID = $this->db->insert_id();
                    }
                }
            } else {

                $data_customer = array(
                    'Name' => $Tenkhachhang,
                    'Birthday' => $NTNS,
                    'Phone1' => $Phone1,
                    'Phone2' => $Phone2,
                    'GroupID' => $GroupID,
                    'CityID' => $CityID,
                    'DistrictID' => $DistrictID,
                    'Ward' => $Ward,
                );

                $this->db->where("ID", $CustomerID);

                $this->db->update("ttp_report_customer", $data_customer);
            }

            if (count($ProductsID) > 0 && count($Amout) > 0) {

                if ($Tinhtrangdonhang == 3) {

                    $ProductsID = $this->check_enought_available($ProductsID, $Amout, $KhoID);

                    if (is_string($ProductsID)) {

                        $arr_error[] = $ProductsID;

                        $this->response['error'] = true;

                        $this->response['message'] = $arr_error;

                        echo json_encode($this->response);

                        return;
                    }
                }

                $total = 0;

                $slsp = 0;

                $arr_productsid = array();

                foreach ($ProductsID as $key => $row) {

                    $dongia = isset($Price[$key]) ? $Price[$key] : 0;

                    $soluong = isset($Amout[$key]) ? $Amout[$key] : 0;

                    $thanhtien = $dongia * $soluong;

                    $slsp = $thanhtien > 0 ? $slsp + $soluong : $slsp;

                    $total += $thanhtien;

                    $ProductsID[$key] = is_array($ProductsID[$key]) ? $ProductsID[$key] : array("ProductsID" => $row, "Bundle" => (object) array());

                    if (!is_array($row)) {

                        $arr_productsid[] = $row;
                    }
                }



                if (count($ProductsID) > 0) {

                    $arr_productsid[] = 0;

                    $temp = implode(',', $arr_productsid);

                    $this->db->query("update ttp_report_products set HasBuy=HasBuy+1 where ID in($temp)");

                    $dayuser = date("d", time());
                    $monthuser = date("m", time());
                    $yearuser = date("Y", time());
                    $userorder = $this->db->query("select count(ID) as SL from ttp_report_order where DAY(Ngaydathang)=$dayuser and MONTH(Ngaydathang)=$monthuser and YEAR(Ngaydathang)=$yearuser")->row();
                    $userorder = $userorder ? $userorder->SL + 1 : 1;

                    $userorder = str_pad($userorder, 2, '0', STR_PAD_LEFT);
                    $UsersID = str_pad($this->user->ID, 3, '0', STR_PAD_LEFT);

//                    $MaDH = 'DH' . $monyear . "_" . $idbyUser;
                    $MaDH = date('dmy') . "-" . $UsersID . "-" . $userorder;

                    if (isset($_POST['giachietkhau'])) {

                        $giachietkhau = (int) $_POST['giachietkhau'];
                    } else {

                        $giachietkhau = $phantramchietkhau * ($total / 100);
                    }



                    $data = array(
                        'KhoID' => $KhoID,
                        'Ngaydathang' => $Ngayban . ' ' . $Gioban,
                        'MaDH' => $MaDH,
                        'DeliveryTime' => $DeliveryTime,
                        'Name' => $Tenkhachhang,
                        'Phone' => $Phone1,
                        'CustomerID' => $CustomerID,
                        'AddressOrder' => $Address,
                        'CityID' => $CityID,
                        'DistrictID' => $DistrictID,
                        'Ward' => $Ward,
                        'Status' => $Tinhtrangdonhang,
                        'Note' => $Note,
                        'UserID' => $UserID,
                        'SourceID' => $SourceID,
                        'Ghichu' => $Ghichu,
                        'Chiphi' => $chiphivanchuyen,
                        'CustomerType' => $Loaikhachhang,
                        'SoluongSP' => $slsp,
                        'Total' => $total,
                        'Chietkhau' => $giachietkhau,
                        'KenhbanhangID' => $KenhbanhangID,
                        'Payment' => $PTthanhtoan,
                        'VoucherID' => $Voucher['state'],
                        'HistoryEdited' => date('Y-m-d H:i:s', time())
                    );

                    $data['OrderType'] = $this->user->Channel == 2 ? 3 : 0;

                    $this->db->insert("ttp_report_order", $data);

                    $OrderID = $this->db->insert_id();

                    if (isset($Voucher['UseOne'])) {

                        if ($Voucher['UseOne'] == 0) {

                            $this->db->query("update ttp_report_voucher set Status=1 where ID=" . $Voucher['state']);
                        }
                    }



                    foreach ($ProductsID as $key => $row) {

                        $dongia = isset($Price[$key]) ? $Price[$key] : 0;

                        $soluong = isset($Amout[$key]) ? $Amout[$key] : 0;

                        $km = isset($Khuyenmai[$key]) ? $Khuyenmai[$key] : '';

                        $giaCK = isset($PriceDown[$key]) ? $PriceDown[$key] : 0;

                        $thanhtien = $dongia * $soluong;

                        $row_ProductsID = $row['ProductsID'];

                        if ($Tinhtrangdonhang == 3) {

                            $productstype = $row['Bundle'];

                            if (count($productstype) > 0) {

                                $arr = array('OrderID' => $OrderID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                                $this->db->insert("ttp_report_orderdetails", $arr);

                                $DetailsID = $this->db->insert_id();

                                foreach ($productstype as $item) {

                                    $tempsoluong = $soluong * $item->Quantity;

                                    $Shipment = $this->get_shipment_arr($item->Des_ProductsID, $KhoID, $tempsoluong);

                                    if (is_array($Shipment) && count($Shipment) > 0) {

                                        foreach ($Shipment as $keyitem => $valueitem) {

                                            if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                                $arr = array('OrderID' => $OrderID, 'DetailsID' => $DetailsID, 'ProductsID' => $item->Des_ProductsID, 'Amount' => $valueitem['Amount'], 'ShipmentID' => $keyitem, "Price" => $item->Price);

                                                $this->db->insert("ttp_report_orderdetails_bundle", $arr);

                                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                                if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->Des_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'insert',
                                                        'Method' => 'insert_order order id ' . $OrderID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                } else {

                                                    $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'update',
                                                        'Method' => 'insert_order order id ' . $OrderID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {

                                $Shipment = $this->get_shipment_arr($row_ProductsID, $KhoID, $soluong);

                                if (is_array($Shipment) && count($Shipment) > 0) {

                                    foreach ($Shipment as $keyitem => $valueitem) {

                                        if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                            $pricetemp = $dongia * $valueitem['Amount'];

                                            $arr = array('OrderID' => $OrderID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $valueitem['Amount'], 'Total' => $pricetemp, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => $keyitem, 'ImportPrice' => $valueitem['Price']);

                                            $this->db->insert("ttp_report_orderdetails", $arr);

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                            if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'insert',
                                                    'Method' => 'insert_order order id ' . $OrderID
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => $valueitem['DateInventory'],
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'update',
                                                    'Method' => 'insert_order order id ' . $OrderID
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {

                            $arr = array('OrderID' => $OrderID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                            $this->db->insert("ttp_report_orderdetails", $arr);
                        }
                    }



                    $datahis = array(
                        'OrderID' => $OrderID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => $Tinhtrangdonhang,
                        "Ghichu" => $Ghichu,
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);

                    $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> khởi tạo đơn hàng <b class='text-primary'>$MaDH</b> giá trị <b class='text-danger'>" . number_format($total - $giachietkhau + $chiphivanchuyen) . "</b>", 0);

                    if ($UserCreated == 1) {

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' maked orders with OrderID($MaDH) for UserID $UserID\n";
                    } else {

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' maked orders with OrderID($MaDH)\n";
                    }

                    file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", $messagelog . "\n", FILE_APPEND);
                }
            }
        }

        $this->response['message'] = $arr_error;

        echo json_encode($this->response);
    }

    public function add_new_order_mtgt() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1) {

            $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0;
        } else {

            $UserCreated = isset($_POST["UserCreated"]) ? $_POST["UserCreated"] : 0;

            if ($UserCreated == 1) {

                $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0;
            } else {

                $UserID = $this->user->ID;
            }
        }

        $DeliveryDate = isset($_POST["DeliveryDate"]) ? $_POST["DeliveryDate"] : date('Y-m-d', time());

        $DeliveryTime = isset($_POST["DeliveryTime"]) ? $_POST["DeliveryTime"] : date('H:i:s', time());

        $Loaidonhang = isset($_POST["Loaidonhang"]) ? $_POST["Loaidonhang"] : 1;

        $Loaikhachhang = isset($_POST["Loaikhachhang"]) ? $_POST["Loaikhachhang"] : 0;

        $CustomerID = isset($_POST["CustomerID"]) ? $_POST["CustomerID"] : 0;

        $Tenkhachhang = isset($_POST["Tenkhachhang"]) ? $_POST["Tenkhachhang"] : 0;

        $DeliveryTime = $DeliveryDate . ' ' . $DeliveryTime;

        $KhoID = isset($_POST["KhoID"]) ? (int) $_POST["KhoID"] : 0;

        $SourceID = isset($_POST["SourceID"]) ? $_POST["SourceID"] : 0;

        $Address = isset($_POST["Address"]) ? $_POST["Address"] : '';

        $CityID = isset($_POST["CityID"]) ? $_POST["CityID"] : 0;

        $DistrictID = isset($_POST["DistrictID"]) ? $_POST["DistrictID"] : 0;

        $Phone1 = isset($_POST["Phone1"]) ? $_POST["Phone1"] : '';

        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';

        $phantramchietkhau = isset($_POST["phantramchietkhau"]) ? $_POST["phantramchietkhau"] : 0;

        $chiphivanchuyen = isset($_POST["chiphivanchuyen"]) ? $_POST["chiphivanchuyen"] : 0;

        $Tinhtrangdonhang = isset($_POST["Tinhtrangdonhang"]) ? $_POST["Tinhtrangdonhang"] : 2;

        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';

        $KenhbanhangID = isset($_POST["KenhbanhangID"]) ? $_POST["KenhbanhangID"] : 0;

        $PTthanhtoan = isset($_POST["PTthanhtoan"]) ? $_POST["PTthanhtoan"] : 0;

        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();

        $Amout = isset($_POST["Amout"]) ? $_POST["Amout"] : array();

        $Money = isset($_POST["Thanhtien"]) ? $_POST["Thanhtien"] : array();

        $Price = isset($_POST["Price"]) ? $_POST["Price"] : array();

        $PriceDown = isset($_POST["GiaCK"]) ? $_POST["GiaCK"] : array();

        $Khuyenmai = isset($_POST["Khuyenmai"]) ? $_POST["Khuyenmai"] : array();

        $ReduceID = isset($_POST["ReduceID"]) ? $_POST["ReduceID"] : array();

        $Timereduce = isset($_POST["Timereduce"]) ? $_POST["Timereduce"] : array();

        $Valuereduce = isset($_POST["Valuereduce"]) ? $_POST["Valuereduce"] : array();

        if ($CityID < 1 || $DistrictID < 1 || $Tenkhachhang == '' || $Phone1 == '' || count($ProductsID) < 1) {

            $href = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH . '/report/import_order';

            redirect($href);
        }

        if ($UserID > 0 && $KhoID > 0) {

            if (count($ProductsID) > 0 && count($Amout) > 0) {

                if ($Tinhtrangdonhang == 3) {

                    $ProductsID = $this->check_enought_available($ProductsID, $Amout, $KhoID);

                    if (is_string($ProductsID)) {

                        return;
                    }
                }

                $total = 0;

                $slsp = 0;

                foreach ($ProductsID as $key => $row) {

                    $dongia = isset($Price[$key]) ? $Price[$key] : 0;

                    $soluong = isset($Amout[$key]) ? $Amout[$key] : 0;

                    $thanhtien = isset($Money[$key]) ? $Money[$key] : 0;

                    $slsp = $thanhtien > 0 ? $slsp + $soluong : $slsp;

                    $total += $thanhtien;

                    $ProductsID[$key] = is_array($ProductsID[$key]) ? $ProductsID[$key] : array("ProductsID" => $row, "Bundle" => (object) array());
                }

                $reduce_total = 0;

                if (count($ReduceID) > 0) {

                    $arr_reduce = array();

                    foreach ($ReduceID as $key => $row) {

                        $Timereduce_row = isset($Timereduce[$key]) ? $Timereduce[$key] : '';

                        $Valuereduce_row = isset($Valuereduce[$key]) ? $Valuereduce[$key] : '';

                        $arr_reduce[] = "(OrderIDV,$row,'$Timereduce_row',$Valuereduce_row)";

                        $reduce_total += $Valuereduce_row;
                    }
                }



                if (count($ProductsID) > 0) {

                    $monthuser = date("m", time());

                    $yearuser = date("Y", time());

                    $userorder = $this->db->query("select count(ID) as SL from ttp_report_order where MONTH(Ngaydathang)=$monthuser and YEAR(Ngaydathang)=$yearuser")->row();

                    $yearuser = date("y", time());

                    $userorder = $userorder ? $userorder->SL + 1 + $monthuser + $yearuser : 1 + $monthuser + $yearuser;

                    $monyear = date('ym');

                    $idbyUser = str_pad($userorder, 6, '0', STR_PAD_LEFT);

                    $MaDH = 'DH' . $monyear . "_" . $idbyUser;



                    if (isset($_POST['giachietkhau'])) {

                        $giachietkhau = (int) $_POST['giachietkhau'];
                    } else {

                        $giachietkhau = $phantramchietkhau * ($total / 100);
                    }



                    $data = array(
                        'ID' => $max,
                        'DeliveryTime' => $DeliveryTime,
                        'OrderType' => $Loaidonhang,
                        'KhoID' => $KhoID,
                        'Ngaydathang' => date('Y-m-d H:i:s'),
                        'MaDH' => $MaDH,
                        'CustomerID' => $CustomerID,
                        'AddressOrder' => $Address,
                        'CityID' => $CityID,
                        'DistrictID' => $DistrictID,
                        'Status' => $Tinhtrangdonhang,
                        'Note' => $Note,
                        'UserID' => $UserID,
                        'SourceID' => $SourceID,
                        'Ghichu' => $Ghichu,
                        'Chiphi' => $chiphivanchuyen,
                        'CustomerType' => $Loaikhachhang,
                        'SoluongSP' => $slsp,
                        'Total' => $total,
                        'Reduce' => $reduce_total,
                        'Chietkhau' => $giachietkhau,
                        'KenhbanhangID' => $KenhbanhangID,
                        'Payment' => $PTthanhtoan,
                        'HistoryEdited' => date('Y-m-d H:i:s', time())
                    );

                    $this->db->insert("ttp_report_order", $data);

                    $OrderID = $this->db->insert_id();

                    foreach ($ProductsID as $key => $row) {

                        $dongia = isset($Price[$key]) ? $Price[$key] : 0;

                        $soluong = isset($Amout[$key]) ? $Amout[$key] : 0;

                        $km = isset($Khuyenmai[$key]) ? $Khuyenmai[$key] : '';

                        $giaCK = isset($PriceDown[$key]) ? $PriceDown[$key] : 0;

                        $thanhtien = isset($Money[$key]) ? $Money[$key] : 0;

                        $row_ProductsID = $row['ProductsID'];

                        if ($Tinhtrangdonhang == 3) {

                            $productstype = $row['Bundle'];

                            if (count($productstype) > 0) {

                                $arr = array('OrderID' => $OrderID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                                $this->db->insert("ttp_report_orderdetails", $arr);

                                $DetailsID = $this->db->insert_id();

                                foreach ($productstype as $item) {

                                    $tempsoluong = $soluong * $item->Quantity;

                                    $Shipment = $this->get_shipment_arr($item->Des_ProductsID, $KhoID, $tempsoluong);

                                    if (is_array($Shipment) && count($Shipment) > 0) {

                                        foreach ($Shipment as $keyitem => $valueitem) {

                                            if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                                $arr = array('OrderID' => $OrderID, 'DetailsID' => $DetailsID, 'ProductsID' => $item->Des_ProductsID, 'Amount' => $valueitem['Amount'], 'ShipmentID' => $keyitem, "Price" => $item->Price);

                                                $this->db->insert("ttp_report_orderdetails_bundle", $arr);

                                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                                if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->Des_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'insert',
                                                        'Method' => '1. insert_order order id ' . $OrderID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                } else {

                                                    $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                    $data_log = array(
                                                        'ProductsID' => $row_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => $valueitem['DateInventory'],
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'update',
                                                        'Method' => '2. insert_order order id ' . $OrderID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {

                                $Shipment = $this->get_shipment_arr($row_ProductsID, $KhoID, $soluong);

                                if (is_array($Shipment) && count($Shipment) > 0) {

                                    foreach ($Shipment as $keyitem => $valueitem) {

                                        if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                            $pricetemp = count($Shipment) == 1 ? $thanhtien : $dongia * $valueitem['Amount'];

                                            $arr = array('OrderID' => $OrderID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $valueitem['Amount'], 'Total' => $pricetemp, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => $keyitem, 'ImportPrice' => $valueitem['Price']);

                                            $this->db->insert("ttp_report_orderdetails", $arr);

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                            if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'insert',
                                                    'Method' => '3. insert_order order id ' . $OrderID
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => $valueitem['DateInventory'],
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'update',
                                                    'Method' => '4. insert_order order id ' . $OrderID
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {

                            $arr = array('OrderID' => $OrderID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                            $this->db->insert("ttp_report_orderdetails", $arr);
                        }
                    }

                    $datahis = array(
                        'OrderID' => $OrderID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => $Tinhtrangdonhang,
                        "Ghichu" => $Ghichu,
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);

                    if (count($arr_reduce) > 0) {

                        $arr_reduce = "insert into ttp_report_reduce_order(OrderID,ReduceID,TimeReduce,ValueReduce) values" . implode(',', $arr_reduce);

                        $arr_reduce = str_replace('OrderIDV', $OrderID, $arr_reduce);

                        $this->db->query($arr_reduce);
                    }

                    if ($UserCreated == 1) {

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' maked orders with OrderID($MaDH) for UserID $UserID\n";
                    } else {

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' maked orders with OrderID($MaDH)\n";
                    }

                    file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", $messagelog . "\n", FILE_APPEND);
                }
            }
        }

        redirect(base_url() . ADMINPATH . '/report/import_order');
    }

    public function update_order_mtgt() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        $UserID = $this->user->ID;

        $ID = isset($_POST["IDOrder"]) ? $_POST["IDOrder"] : 0;

        if (($this->user->UserType == 5 || $this->user->IsAdmin == 1)) {

            if (isset($_POST['Follow'])) {

                $this->db->query("update ttp_report_order_reason_details set Follow=1 where OrderID=$ID");
            } else {

                $this->db->query("update ttp_report_order_reason_details set Follow=0 where OrderID=$ID");
            }
        }

        $CreatedDate = isset($_POST["CreatedDate"]) ? $_POST["CreatedDate"] : date('m/d/Y', time());

        $CreatedTime = isset($_POST["CreatedTime"]) ? $_POST["CreatedTime"] : date('H:i:s', time());

        $DeliveryDate = isset($_POST["DeliveryDate"]) ? $_POST["DeliveryDate"] : date('m/d/Y', time());

        $DeliveryTime = isset($_POST["DeliveryTime"]) ? $_POST["DeliveryTime"] : date('H:i:s', time());

        $DeliveryTime = $DeliveryDate . ' ' . $DeliveryTime;



        $IsChangeOrder = isset($_POST["IsChangeOrder"]) ? $_POST["IsChangeOrder"] : 0;

        $Loaidonhang = isset($_POST["Loaidonhang"]) ? $_POST["Loaidonhang"] : 1;

        $KhoID = isset($_POST["KhoID"]) ? (int) $_POST["KhoID"] : 0;

        $CustomerID = isset($_POST["CustomerID"]) ? (int) $_POST["CustomerID"] : 0;

        $Address = isset($_POST["Address"]) ? $_POST["Address"] : '';

        $CityID = isset($_POST["CityID"]) ? $_POST["CityID"] : 0;

        $DistrictID = isset($_POST["DistrictID"]) ? $_POST["DistrictID"] : 0;

        $Phone1 = isset($_POST["Phone1"]) ? $_POST["Phone1"] : '';

        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';

        $phantramchietkhau = isset($_POST["phantramchietkhau"]) ? $_POST["phantramchietkhau"] : 0;

        $chiphivanchuyen = isset($_POST["chiphivanchuyen"]) ? $_POST["chiphivanchuyen"] : 0;

        $Tinhtrangdonhang = isset($_POST["Tinhtrangdonhang"]) ? $_POST["Tinhtrangdonhang"] : 2;

        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';

        $PTthanhtoan = isset($_POST["PTthanhtoan"]) ? $_POST["PTthanhtoan"] : 0;

        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();

        $Amout = isset($_POST["Amount"]) ? $_POST["Amount"] : array();

        $Money = isset($_POST["Thanhtien"]) ? $_POST["Thanhtien"] : array();

        $Price = isset($_POST["Price"]) ? $_POST["Price"] : array();

        $Khuyenmai = isset($_POST["Khuyenmai"]) ? $_POST["Khuyenmai"] : array();

        $ReduceID = isset($_POST["ReduceID"]) ? $_POST["ReduceID"] : array();

        $Timereduce = isset($_POST["Timereduce"]) ? $_POST["Timereduce"] : array();

        $Valuereduce = isset($_POST["Valuereduce"]) ? $_POST["Valuereduce"] : array();

        if ($CityID < 1 || $DistrictID < 1 || $CustomerID < 1 || $Phone1 == '' || count($ProductsID) < 1) {

            $href = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH . '/report/import_order';

            redirect($href);
        }

        if ($UserID > 0) {

            $order = $this->db->query("select * from ttp_report_order a,ttp_report_customer b where a.ID=$ID and a.UserID=$UserID and a.CustomerID=b.ID")->row();

            if ($order) {

                if ($this->user->IsAdmin == 0) {

                    if ($order->Status != 2 && $order->Status != 4 && $order->Status != 6) {

                        redirect(base_url() . ADMINPATH . '/report/import_order');
                    }
                }

                $todetails = "";

                if (count($ProductsID) > 0 && count($Amout) > 0) {

                    if ($Tinhtrangdonhang == 3 && $order->Status == 2) {

                        $ProductsID = $this->check_enought_available($ProductsID, $Amout, $order->KhoID);

                        if (is_string($ProductsID)) {

                            return;
                        }
                    }

                    if ($order->Status == 4 || $order->Status == 6) {

                        $Tinhtrangdonhang = 3;

                        $ProductsID = $this->check_enought_available($ProductsID, $Amout, $order->KhoID);

                        if (is_string($ProductsID)) {

                            return;
                        }
                    }



                    $arr = array();

                    $total = 0;

                    $messagelog = "";

                    $slsp = 0;

                    $this->db->query("delete from ttp_report_orderdetails where OrderID=$ID");

                    $this->db->query("delete from ttp_report_orderdetails_bundle where OrderID=$ID");

                    foreach ($ProductsID as $key => $row) {

                        $dongia = isset($Price[$key]) ? $Price[$key] : 0;

                        $soluong = isset($Amout[$key]) ? $Amout[$key] : 0;

                        $km = isset($Khuyenmai[$key]) ? $Khuyenmai[$key] : '';

                        $giaCK = isset($PriceDown[$key]) ? $PriceDown[$key] : 0;

                        $thanhtien = isset($Money[$key]) ? $Money[$key] : 0;

                        $total += $thanhtien;

                        $slsp = $thanhtien > 0 ? $slsp + $soluong : $slsp;

                        $row_ProductsID = isset($row['ProductsID']) ? $row['ProductsID'] : $row;

                        if ($Tinhtrangdonhang == 3 && ($order->Status == 2 || $order->Status == 4 || $order->Status == 6)) {

                            $productstype = isset($row['Bundle']) ? $row['Bundle'] : (object) array();

                            if (count($productstype) > 0) {

                                $arr = array('OrderID' => $ID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                                $this->db->insert("ttp_report_orderdetails", $arr);

                                $DetailsID = $this->db->insert_id();

                                foreach ($productstype as $item) {

                                    $tempsoluong = $soluong * $item->Quantity;

                                    $Shipment = $this->get_shipment_arr($item->Des_ProductsID, $order->KhoID, $tempsoluong);

                                    if (is_array($Shipment) && count($Shipment) > 0) {

                                        foreach ($Shipment as $keyitem => $valueitem) {

                                            if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                                $arr = array('OrderID' => $ID, 'DetailsID' => $DetailsID, 'ProductsID' => $item->Des_ProductsID, 'Amount' => $valueitem['Amount'], 'ShipmentID' => $keyitem, "Price" => $item->Price);

                                                $this->db->insert("ttp_report_orderdetails_bundle", $arr);

                                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                                if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->Des_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'insert',
                                                        'Method' => '8. update order id ' . $ID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                } else {

                                                    $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'update',
                                                        'Method' => '9. update order id ' . $ID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {

                                $Shipment = $this->get_shipment_arr($row_ProductsID, $order->KhoID, $soluong);

                                if (is_array($Shipment) && count($Shipment) > 0) {

                                    foreach ($Shipment as $keyitem => $valueitem) {

                                        if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                            $pricetemp = count($Shipment) == 1 ? $thanhtien : $dongia * $valueitem['Amount'];

                                            $arr = array('OrderID' => $ID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $valueitem['Amount'], 'Total' => $pricetemp, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => $keyitem, 'ImportPrice' => $valueitem['Price']);

                                            $this->db->insert("ttp_report_orderdetails", $arr);

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                            if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'insert',
                                                    'Method' => '10. update order id ' . $ID
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'update',
                                                    'Method' => '11. update order id ' . $ID
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                }
                            }
                        } elseif ($Tinhtrangdonhang == 2) {

                            $arr = array('OrderID' => $ID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                            $this->db->insert("ttp_report_orderdetails", $arr);
                        }
                    }

                    $reduce_total = 0;

                    if (count($ReduceID) > 0) {

                        $arr_reduce = array();

                        foreach ($ReduceID as $key => $row) {

                            $Timereduce_row = isset($Timereduce[$key]) ? $Timereduce[$key] : '';

                            $Valuereduce_row = isset($Valuereduce[$key]) ? $Valuereduce[$key] : '';

                            $arr_reduce[] = "($ID,$row,'$Timereduce_row',$Valuereduce_row)";

                            $reduce_total += $Valuereduce_row;
                        }
                    }

                    if (count($ProductsID) > 0) {

                        if (isset($_POST['giachietkhau'])) {

                            $giachietkhau = (int) $_POST['giachietkhau'];
                        } else {

                            $giachietkhau = $phantramchietkhau * ($total / 100);
                        }



                        $data = array(
                            'CustomerID' => $CustomerID,
                            'OrderType' => $Loaidonhang,
                            'AddressOrder' => $Address,
                            'CityID' => $CityID,
                            'DistrictID' => $DistrictID,
                            'Status' => $Tinhtrangdonhang,
                            'Note' => $Note,
                            'UserID' => $UserID,
                            'Ghichu' => $Ghichu,
                            'Chiphi' => $chiphivanchuyen,
                            'SoluongSP' => $slsp,
                            'Total' => $total,
                            'Reduce' => $reduce_total,
                            'Chietkhau' => $giachietkhau,
                            'HistoryEdited' => date('Y-m-d H:i:s'),
                            'Payment' => $PTthanhtoan,
                            'KhoID' => $KhoID,
                            'DeliveryTime' => $DeliveryTime,
                            'Ngaydathang' => $CreatedDate . ' ' . $CreatedTime
                        );

                        /*

                         *   Write log on data change

                         */

                        $statuslog = 0;

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' change data from OrderID($order->MaDH)\n";

                        $logchange = array();

                        foreach ($data as $key => $value) {

                            if ($order->$key != $value)
                                $logchange[] = "    $key : from '" . $order->$key . "' => $value\n";
                        }

                        if (count($logchange) > 0) {

                            $statuslog = 1;

                            $logchange = implode("", $logchange);

                            $messagelog = $messagelog . $logchange;
                        }

                        /*                         * ** */

                        $this->db->where("ID", $ID);

                        $this->db->update("ttp_report_order", $data);



                        if (count($arr_reduce) > 0) {

                            $arr_reduce = "insert into ttp_report_reduce_order(OrderID,ReduceID,TimeReduce,ValueReduce) values" . implode(',', $arr_reduce);

                            $this->db->query("delete from ttp_report_reduce_order where OrderID=$ID");

                            $this->db->query($arr_reduce);
                        }



                        $statuslog = 1;

                        $messagelog .= "    SoluongSP : from '$order->SoluongSP' => " . count($ProductsID) . "\n";

                        $olddetails = $this->db->query("select * from ttp_report_orderdetails where OrderID=$ID")->result();

                        if (count($olddetails) > 0) {

                            $messagelog .= "    Details Order: \n";

                            $messagelog .= "         From : \n";

                            foreach ($olddetails as $row) {

                                $messagelog .= "              SP" . str_pad($row->ID, 6, '0', STR_PAD_LEFT) . "   (Price : $row->Price)   (Amount : $row->Amount)     (Total : $row->Total)\n";
                            }

                            $messagelog .= "         To : \n" . $todetails;
                        }

                        if ($statuslog == 1) {

                            file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", $messagelog . "\n", FILE_APPEND);
                        }

                        if ($order->Status != $Tinhtrangdonhang) {

                            $datahis = array(
                                'OrderID' => $ID,
                                'Thoigian' => date('Y-m-d H:i:s', time()),
                                'Status' => $Tinhtrangdonhang,
                                "Ghichu" => $Ghichu,
                                "UserID" => $this->user->ID
                            );

                            $this->db->insert('ttp_report_orderhistory', $datahis);
                        }
                    }
                }
            }
        }

        redirect(base_url() . ADMINPATH . '/report/import_order');
    }

    public function update_order() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 5) {

            $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0;
        } else {

            $UserID = $this->user->ID;
        }

        $ID = isset($_POST["IDOrder"]) ? $_POST["IDOrder"] : 0;

        if (($this->user->UserType == 5 || $this->user->IsAdmin == 1)) {

            if (isset($_POST['Follow'])) {

                $this->db->query("update ttp_report_order_reason_details set Follow=1 where OrderID=$ID");
            } else {

                $this->db->query("update ttp_report_order_reason_details set Follow=0 where OrderID=$ID");
            }
        }

        $Voucher = isset($_POST['Voucher']) ? $_POST['Voucher'] : '';

        $Voucher = $this->voucher($Voucher);

        $DayDeliveryTime = isset($_POST["DayDeliveryTime"]) ? $_POST["DayDeliveryTime"] : date('Y-m-d', time());

        $HourDeliveryTime = isset($_POST["HourDeliveryTime"]) ? $_POST["HourDeliveryTime"] : date('H', time());

        $DeliveryTime = $DayDeliveryTime . ' ' . $HourDeliveryTime . ":01:01";

        $IsChangeOrder = isset($_POST["IsChangeOrder"]) ? $_POST["IsChangeOrder"] : 0;

        $Tenkhachhang = isset($_POST["Tenkhachhang"]) ? $_POST["Tenkhachhang"] : 0;

        $NTNS = isset($_POST["NTNS"]) ? $_POST["NTNS"] : '';

        if ($NTNS != '') {

            $NTNS = explode('/', $NTNS);

            $NTNS = isset($NTNS[2]) && isset($NTNS[1]) && isset($NTNS[0]) ? $NTNS[2] . "-" . $NTNS[1] . "-" . $NTNS[0] : date('Y-m-d', time());
        }

        $KhoID = isset($_POST["KhoID"]) ? (int) $_POST["KhoID"] : 0;

        $CMND = isset($_POST["CMND"]) ? (int) $_POST["CMND"] : '';

        $SourceID = isset($_POST["SourceID"]) ? $_POST["SourceID"] : 0;

        $Address = isset($_POST["Address"]) ? $_POST["Address"] : '';

        $CityID = isset($_POST["CityID"]) ? $_POST["CityID"] : 0;

        $DistrictID = isset($_POST["DistrictID"]) ? $_POST["DistrictID"] : 0;

        $Phone1 = isset($_POST["Phone1"]) ? $_POST["Phone1"] : '';

        $Phone2 = isset($_POST["Phone2"]) ? $_POST["Phone2"] : '';

        $Note = isset($_POST["Note"]) ? $_POST["Note"] : '';

        $phantramchietkhau = isset($_POST["phantramchietkhau"]) ? $_POST["phantramchietkhau"] : 0;

        $chiphivanchuyen = isset($_POST["chiphivanchuyen"]) ? $_POST["chiphivanchuyen"] : 0;

        $Tinhtrangdonhang = isset($_POST["Tinhtrangdonhang"]) ? $_POST["Tinhtrangdonhang"] : 2;

        $Ghichu = isset($_POST["Ghichu"]) ? $_POST["Ghichu"] : '';

        $KenhbanhangID = isset($_POST["KenhbanhangID"]) ? $_POST["KenhbanhangID"] : 0;

        $PTthanhtoan = isset($_POST["PTthanhtoan"]) ? $_POST["PTthanhtoan"] : 0;

        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();

        $Amout = isset($_POST["Amount"]) ? $_POST["Amount"] : array();

        $Price = isset($_POST["Price"]) ? $_POST["Price"] : array();

        $PriceDown = isset($_POST["GiaCK"]) ? $_POST["GiaCK"] : array();

        $Khuyenmai = isset($_POST["Khuyenmai"]) ? $_POST["Khuyenmai"] : array();

        $arr_error = array();

        if ($SourceID < 1) {
            $arr_error[] = "Chưa chọn nguồn đơn hàng.";
        }

        if ($KenhbanhangID < 1) {
            $arr_error[] = "Chưa chọn kênh bán hàng.";
        }

        if ($Tenkhachhang == '') {
            $arr_error[] = "Tên khách hàng chưa điền.";
        }

        if ($Phone1 == '') {
            $arr_error[] = "Số di động 1 chưa điền.";
        }

        if ($CityID < 1) {
            $arr_error[] = "Chưa chọn Tỉnh / Thành phố .";
        }

        if ($DistrictID < 1) {
            $arr_error[] = "Chưa chọn Quận / Huyện .";
        }

        if (count($ProductsID) < 1) {
            $arr_error[] = "Chưa chọn sản phẩm vào đơn hàng.";
        }

        if ($UserID < 1) {
            $arr_error[] = "Không xác định nhân viên bán hàng.";
        }

        if ($KhoID < 1) {
            $arr_error[] = "Chưa chọn kho lấy hàng.";
        }

        if (count($arr_error) > 0) {
            $this->response['error'] = true;
        }

        if ($this->response['error'] == false) {

            $order = $this->db->query("select a.* from ttp_report_order a,ttp_report_customer b where a.ID=$ID and a.UserID=$UserID and a.CustomerID=b.ID")->row();

            if ($order) {

                if ($this->user->IsAdmin == 0) {

                    if ($order->Status != 2 && $order->Status != 4 && $order->Status != 6) {

                        $arr_error[] = "Đơn hàng đang bị khóa nên không thể cập nhật thông tin.";

                        $this->response['error'] = true;

                        $this->response['message'] = $arr_error;

                        echo json_encode($this->response);

                        return;
                    }
                }

                $todetails = "";

                if (count($ProductsID) > 0 && count($Amout) > 0) {

                    if ($Tinhtrangdonhang == 3 && $order->Status == 2) {

                        $ProductsID = $this->check_enought_available($ProductsID, $Amout, $order->KhoID);

                        if (is_string($ProductsID)) {

                            $arr_error[] = $ProductsID;

                            $this->response['error'] = true;

                            $this->response['message'] = $arr_error;

                            echo json_encode($this->response);

                            return;
                        }
                    }

                    if ($order->Status == 4 || $order->Status == 6) {

                        $Tinhtrangdonhang = 3;

                        $ProductsID = $this->check_enought_available($ProductsID, $Amout, $order->KhoID);

                        if (is_string($ProductsID)) {

                            $arr_error[] = $ProductsID;

                            $this->response['error'] = true;

                            $this->response['message'] = $arr_error;

                            echo json_encode($this->response);

                            return;
                        }
                    }



                    $arr = array();

                    $total = 0;

                    $messagelog = "";

                    $slsp = 0;

                    $this->db->query("delete from ttp_report_orderdetails where OrderID=$ID");

                    $this->db->query("delete from ttp_report_orderdetails_bundle where OrderID=$ID");



                    foreach ($ProductsID as $key => $row) {

                        $dongia = isset($Price[$key]) ? $Price[$key] : 0;

                        $soluong = isset($Amout[$key]) ? $Amout[$key] : 0;

                        $km = isset($Khuyenmai[$key]) ? $Khuyenmai[$key] : '';

                        $giaCK = isset($PriceDown[$key]) ? $PriceDown[$key] : 0;

                        $thanhtien = $dongia * $soluong;

                        $total += $thanhtien;

                        $slsp = $thanhtien > 0 ? $slsp + $soluong : $slsp;

                        $row_ProductsID = is_array($row) && isset($row['ProductsID']) ? $row['ProductsID'] : $row;

                        if ($Tinhtrangdonhang == 3 && ($order->Status == 2 || $order->Status == 4 || $order->Status == 6)) {

                            $productstype = isset($row['Bundle']) ? $row['Bundle'] : (object) array();

                            if (count($productstype) > 0) {

                                $arr = array('OrderID' => $ID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                                $this->db->insert("ttp_report_orderdetails", $arr);

                                $DetailsID = $this->db->insert_id();

                                foreach ($productstype as $item) {

                                    $tempsoluong = $soluong * $item->Quantity;

                                    $Shipment = $this->get_shipment_arr($item->Des_ProductsID, $order->KhoID, $tempsoluong);

                                    if (is_array($Shipment) && count($Shipment) > 0) {

                                        foreach ($Shipment as $keyitem => $valueitem) {

                                            if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                                $arr = array('OrderID' => $ID, 'DetailsID' => $DetailsID, 'ProductsID' => $item->Des_ProductsID, 'Amount' => $valueitem['Amount'], 'ShipmentID' => $keyitem, "Price" => $item->Price);

                                                $this->db->insert("ttp_report_orderdetails_bundle", $arr);

                                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                                if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->Des_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'insert',
                                                        'Method' => '16. update order id ' . $ID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                } else {

                                                    $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$item->Des_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                    $data_log = array(
                                                        'ProductsID' => $item->Des_ProductsID,
                                                        'OnHand' => $valueitem['OnHand'],
                                                        'Available' => $valueitem['Available'],
                                                        'WarehouseID' => $KhoID,
                                                        'ShipmentID' => $keyitem,
                                                        'DateInventory' => date('Y-m-d'),
                                                        'Type' => "Available",
                                                        'Amount' => $valueitem['Amount'],
                                                        'Action' => 'update',
                                                        'Method' => '17. update order id ' . $ID
                                                    );

                                                    $this->write_log_inventory($data_log);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {

                                $Shipment = $this->get_shipment_arr($row_ProductsID, $order->KhoID, $soluong);

                                if (is_array($Shipment) && count($Shipment) > 0) {

                                    foreach ($Shipment as $keyitem => $valueitem) {

                                        if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available']) && isset($valueitem['Price'])) {

                                            $pricetemp = $dongia * $valueitem['Amount'];

                                            $arr = array('OrderID' => $ID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $valueitem['Amount'], 'Total' => $pricetemp, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => $keyitem, 'ImportPrice' => $valueitem['Price']);

                                            $this->db->insert("ttp_report_orderdetails", $arr);

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and WarehouseID=$KhoID and LastEdited=1");

                                            if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row_ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$KhoID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'insert',
                                                    'Method' => '18. update order id ' . $ID
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$row_ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$KhoID");

                                                $data_log = array(
                                                    'ProductsID' => $row_ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $KhoID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'update',
                                                    'Method' => '19. update order id ' . $ID
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                }
                            }
                        } elseif ($Tinhtrangdonhang == 2) {

                            $arr = array('OrderID' => $ID, 'ProductsID' => $row_ProductsID, 'Price' => $dongia, 'Amount' => $soluong, 'Total' => $thanhtien, 'Khuyenmai' => $km, 'PriceDown' => $giaCK, 'ShipmentID' => 0, 'ImportPrice' => $dongia);

                            $this->db->insert("ttp_report_orderdetails", $arr);
                        }
                    }



                    if (count($ProductsID) > 0) {

                        if (isset($_POST['giachietkhau'])) {

                            $giachietkhau = (int) $_POST['giachietkhau'];
                        } else {

                            $giachietkhau = $phantramchietkhau * ($total / 100);
                        }

                        $data = array(
                            'CustomerID' => $order->CustomerID,
                            'AddressOrder' => $Address,
                            'DeliveryTime' => $DeliveryTime,
                            'CityID' => $CityID,
                            'Name' => $Tenkhachhang,
                            'Phone' => $Phone1,
                            'DistrictID' => $DistrictID,
                            'Status' => $Tinhtrangdonhang,
                            'Note' => $Note,
                            'UserID' => $UserID,
                            'SourceID' => $SourceID,
                            'Ghichu' => $Ghichu,
                            'Chiphi' => $chiphivanchuyen,
                            'SoluongSP' => $slsp,
                            'Total' => $total,
                            'Chietkhau' => $giachietkhau,
                            'HistoryEdited' => date('Y-m-d H:i:s'),
                            'KenhbanhangID' => $KenhbanhangID,
                            'Payment' => $PTthanhtoan,
                            'KhoID' => $KhoID,
                            'VoucherID' => $Voucher['state']
                        );



                        $statuslog = 0;

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' change data from OrderID($order->MaDH)\n";

                        $logchange = array();

                        foreach ($data as $key => $value) {

                            if ($order->$key != $value)
                                $logchange[] = "    $key : from '" . $order->$key . "' => $value\n";
                        }

                        if (count($logchange) > 0) {

                            $statuslog = 1;

                            $logchange = implode("", $logchange);

                            $messagelog = $messagelog . $logchange;
                        }



                        $this->db->where("ID", $ID);

                        $this->db->update("ttp_report_order", $data);

                        if (isset($Voucher['UseOne'])) {

                            if ($Voucher['UseOne'] == 0) {

                                $this->db->query("update ttp_report_voucher set Status=1 where ID=" . $Voucher['state']);
                            }
                        }

                        $statuslog = 1;

                        $messagelog .= "    SoluongSP : from '$order->SoluongSP' => " . count($ProductsID) . "\n";

                        $olddetails = $this->db->query("select * from ttp_report_orderdetails where OrderID=$ID")->result();

                        if (count($olddetails) > 0) {

                            $messagelog .= "    Details Order: \n";

                            $messagelog .= "         From : \n";

                            foreach ($olddetails as $row) {

                                $messagelog .= "              SP" . str_pad($row->ID, 6, '0', STR_PAD_LEFT) . "   (Price : $row->Price)   (Amount : $row->Amount)     (Total : $row->Total)\n";
                            }

                            $messagelog .= "         To : \n" . $todetails;
                        }

                        if ($statuslog == 1) {

                            file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", $messagelog . "\n", FILE_APPEND);
                        }

                        if ($order->Status != $Tinhtrangdonhang) {

                            $datahis = array(
                                'OrderID' => $ID,
                                'Thoigian' => date('Y-m-d H:i:s', time()),
                                'Status' => $Tinhtrangdonhang,
                                "Ghichu" => $Ghichu,
                                "UserID" => $this->user->ID
                            );

                            $this->db->insert('ttp_report_orderhistory', $datahis);

                            $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> thay đổi trạng thái đơn hàng <b class='text-primary'>$order->MaDH</b>", 3);
                        } else {

                            $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> thay đổi thông tin đơn hàng <b class='text-primary'>$order->MaDH</b>", 3);
                        }
                    }
                } else {

                    $this->response['error'] = true;

                    $arr_error[] = "Vui lòng chọn sản phẩm đưa vào đơn hàng.";
                }
            } else {

                $this->response['error'] = true;

                $arr_error[] = "Không tìm thấy đơn hàng để cập nhật thông tin.";
            }
        }

        $this->response['message'] = $arr_error;

        echo json_encode($this->response);
    }

    public function check_enought_available($ProductsID = array(), $Amount = array(), $KhoID = 0) {

        if (count($ProductsID) > 0 && count($Amount) > 0 && $KhoID > 0) {

            $arr_sum_amount = array();

            foreach ($ProductsID as $key => $row) {

                $amounttemp = isset($Amount[$key]) ? $Amount[$key] : 0;

                $productstype = $this->db->query("select a.*,b.Title from ttp_report_products_bundle a,ttp_report_products b where a.Src_ProductsID=$row and a.Des_ProductsID=b.ID")->result();

                if (count($productstype) > 0) {

                    foreach ($productstype as $item) {

                        $amounttemp1 = $amounttemp * $item->Quantity;

                        if (isset($arr_sum_amount[$item->Des_ProductsID])) {

                            //$amounttemp = $amounttemp+$arr_sum_amount[$item->Des_ProductsID];

                            $arr_sum_amount[$item->Des_ProductsID] = $amounttemp1 + $arr_sum_amount[$item->Des_ProductsID];
                        } else {

                            $arr_sum_amount[$item->Des_ProductsID] = $amounttemp1;
                        }

                        $check = $this->db->query("select sum(Available) as Available from ttp_report_inventory where ProductsID=$item->Des_ProductsID and WarehouseID=$KhoID and LastEdited=1")->row();

                        if ($check) {

                            if ($check->Available < $arr_sum_amount[$item->Des_ProductsID]) {

                                return "Đơn hàng khởi tạo không thành công ! Sản phẩm $item->Title ko đủ số lượng để bán !";
                            }
                        } else {

                            return "Đơn hàng khởi tạo không thành công ! Sản phẩm $item->Title ko đủ số lượng để bán !";
                        }
                    }
                } else {

                    if (isset($arr_sum_amount[$row])) {

                        $amounttemp += $arr_sum_amount[$row];

                        $arr_sum_amount[$row] = $amounttemp;
                    } else {

                        $arr_sum_amount[$row] = $amounttemp;
                    }

                    $check = $this->db->query("select sum(a.Available) as Available,b.Title from ttp_report_inventory a,ttp_report_products b where a.ProductsID=b.ID and a.ProductsID=$row and a.WarehouseID=$KhoID and a.LastEdited=1")->row();

                    if ($check) {

                        if ($check->Available < $amounttemp) {

                            return "Đơn hàng khởi tạo không thành công ! Sản phẩm $check->Title ko đủ số lượng để bán !";
                        }
                    } else {

                        return "Đơn hàng khởi tạo không thành công ! Sản phẩm ko đủ số lượng để bán !";
                    }
                }

                $ProductsID[$key] = array("ProductsID" => $row, "Bundle" => $productstype);
            }

            return $ProductsID;
        } else {

            return "Đơn hàng khởi tạo không thành công ! Sản phẩm ko đủ số lượng để bán !";
        }
    }

    public function get_targets_user() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);

        $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0;

        $UserID = $this->user->IsAdmin == 1 || $this->user->UserType == 5 ? $UserID : $this->user->ID;

        $thisday = date('d', time());

        $thismonth = date('m', time());

        $thisyear = date('Y', time());

        $orderid = isset($_POST['IDOrder']) ? $_POST['IDOrder'] : 0;

        if ($orderid > 0) {

            $order = $this->db->query("select Ngaydathang from ttp_report_order where ID=$orderid")->row();

            if ($order) {

                $thisday = date('d', strtotime($order->Ngaydathang));

                $thismonth = date('m', strtotime($order->Ngaydathang));

                $thisyear = date('Y', strtotime($order->Ngaydathang));
            }
        }

        $targets = $this->db->query("select * from ttp_report_targets_year where Year=$thisyear")->row();

        if ($targets) {

            $json = json_decode($targets->DataDepartment, true);

            $phongban = $this->db->query("select a.ID as PhongbanID,b.ID as TeamID from ttp_report_targets_department a,ttp_report_team b where a.ID=b.Department and b.Data like '%\"$UserID\"%'")->row();

            if ($phongban) {

                $chitieuthang = isset($json['type2']['Department'][$phongban->PhongbanID]['Month'][$thismonth]['Team'][$phongban->TeamID]['Person'][$UserID]['Total']) ? $json['type2']['Department'][$phongban->PhongbanID]['Month'][$thismonth]['Team'][$phongban->TeamID]['Person'][$UserID]['Total'] : 0;

                $thuctethang = $this->db->query("select SUM(Total) as Total from ttp_report_order where MONTH(Ngaydathang)=$thismonth and YEAR(Ngaydathang)=$thisyear and Status=0 and UserID=" . $UserID)->row();

                $thuctethang = isset($thuctethang->Total) ? number_format($thuctethang->Total) : 0;

                $thuctengay = $this->db->query("select SUM(Total) as Total from ttp_report_order where DAY(Ngaydathang)=$thisday and MONTH(Ngaydathang)=$thismonth and YEAR(Ngaydathang)=$thisyear and Status=0 and UserID=" . $UserID)->row();

                $thuctengay = isset($thuctengay->Total) ? number_format($thuctengay->Total) : 0;

                $songay = $this->NumDay($thismonth, $thisyear);

                $songay = is_numeric($songay) ? $songay : 1;

                $chitieungay = number_format($chitieuthang / $songay);

                $chitieuthang = number_format($chitieuthang, 0);

                $checkleader = $this->db->query("select Data from ttp_report_team where UserID=" . $this->user->ID)->row();

                if ($checkleader) {

                    $userlist = json_decode($checkleader->Data, true);

                    $userlist[] = 0;

                    $userlist = implode(',', $userlist);

                    $chitieuthang = isset($json['type2']['Department'][$phongban->PhongbanID]['Month'][$thismonth]['Team'][$phongban->TeamID]['Person'][$UserID]['Total']) ? $json['type2']['Department'][$phongban->PhongbanID]['Month'][$thismonth]['Team'][$phongban->TeamID]['Total'] : 0;

                    $thuctethang = $this->db->query("select SUM(Total) as Total from ttp_report_order where MONTH(Ngaydathang)=$thismonth and YEAR(Ngaydathang)=$thisyear and Status=0 and UserID in ($userlist)")->row();

                    $thuctethang = isset($thuctethang->Total) ? number_format($thuctethang->Total) : 0;

                    $thuctengay = $this->db->query("select SUM(Total) as Total from ttp_report_order where DAY(Ngaydathang)=$thisday and MONTH(Ngaydathang)=$thismonth and YEAR(Ngaydathang)=$thisyear and Status=0 and UserID in ($userlist)")->row();

                    $thuctengay = isset($thuctengay->Total) ? number_format($thuctengay->Total) : 0;

                    $chitieungay = number_format($chitieuthang / $songay);

                    $chitieuthang = number_format($chitieuthang, 0);
                }
            } else {

                $chitieuthang = 0;

                $chitieungay = 0;

                $thuctethang = 0;

                $thuctengay = 0;
            }

            echo "<table>

                    <tr><th>Chỉ tiêu ngày</th><th>Chỉ tiêu tháng</th><th>Thực tế ngày</th><th>Thực tế tháng</th></tr>

                    <tr><td>" . $chitieungay . "</td><td>" . $chitieuthang . "</td><td>$thuctengay</td><td>" . $thuctethang . "</td></tr>

                </table>";

            return;
        } else {

            echo "<table>

                        <tr><th>Chỉ tiêu ngày</th><th>Chỉ tiêu tháng</th><th>Thực tế ngày</th><th>Thực tế tháng</th></tr>

                        <tr><td>0</td><td>0</td><td>0</td><td>0</td></tr>

                    </table>";
        }
    }

    public function NumDay($month, $year) {

        $time = strtotime("$year-$month-1");

        $n = date('t', $time);

        return $n;
    }

    public function set_success() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 4) {

            $UserID = '';
        } else {

            $UserID = " and UserID=" . $this->user->ID;
        }

        $data = isset($_POST['data']) ? $_POST['data'] : '';

        if ($data != '') {

            $data = json_decode($data, true);

            if (count($data) > 0) {

                $integerIDs = array_map('intval', $data);

                $integerIDs = implode(',', $integerIDs);

                $this->db->query("update ttp_report_order set Status=0 where ID in($integerIDs) $UserID");

                file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", date('H:i:s', time()) . " => User '{$this->user->UserName}' Change Status OrderID($integerIDs) to 'Success Order'\n", FILE_APPEND);

                echo "OK";

                return;
            }
        }

        echo "False";
    }

    public function set_cancel() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 4) {

            $UserID = '';
        } else {

            $UserID = " and UserID=" . $this->user->ID;
        }

        $data = isset($_POST['data']) ? $_POST['data'] : '';

        if ($data != '') {

            $data = json_decode($data, true);

            if (count($data) > 0) {

                $integerIDs = array_map('intval', $data);

                $integerIDs = implode(',', $integerIDs);

                $this->db->query("update ttp_report_order set Status=1 where ID in($integerIDs) $UserID");

                file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", date('H:i:s', time()) . " => User '{$this->user->UserName}' Change Status OrderID($integerIDs) to 'Cancel Order'\n", FILE_APPEND);

                echo "OK";

                return;
            }
        }

        echo "False";
    }

    public function set_waiting() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1) {

            $UserID = '';
        } else {

            $UserID = " and UserID=" . $this->user->ID;
        }

        $data = isset($_POST['data']) ? $_POST['data'] : '';

        if ($data != '') {

            $data = json_decode($data, true);

            if (count($data) > 0) {

                $integerIDs = array_map('intval', $data);

                $integerIDs = implode(',', $integerIDs);

                $this->db->query("update ttp_report_order set Status=2 where ID in($integerIDs) $UserID");

                file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", date('H:i:s', time()) . " => User '{$this->user->UserName}' Change Status OrderID($integerIDs) to 'Waiting Order'\n", FILE_APPEND);

                echo "OK";

                return;
            }
        }

        echo "False";
    }

    public function droporder() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);

        if ($this->user->IsAdmin == 1) {

            $data = isset($_POST['data']) ? $_POST['data'] : '';

            if ($data != '') {

                $data = json_decode($data, true);

                if (count($data) > 0) {

                    $integerIDs = array_map('intval', $data);

                    $integerIDs = implode(',', $integerIDs);

                    $this->db->trans_start();

                    $this->db->query("delete from ttp_report_order where ID in($integerIDs)");

                    $this->db->query("delete from ttp_report_orderdetails where OrderID in($integerIDs)");

                    $this->db->query("delete from ttp_report_orderhistory where OrderID in($integerIDs)");

                    $this->db->trans_complete();

                    file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", date('H:i:s', time()) . " => User '{$this->user->UserName}' Delete OrderID($integerIDs)\n", FILE_APPEND);

                    echo "OK";

                    return;
                }
            }
        }

        echo "False";
    }

    public function save_from_kho() {

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 2) {

            $ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '';

            $tinhtrangdonhang = isset($_POST['Tinhtrangdonhang']) ? $_POST['Tinhtrangdonhang'] : 4;

            $orderid = isset($_POST['OrderID']) ? $_POST['OrderID'] : 0;

            if ($orderid > 0) {

                $result = $this->db->query("select * from ttp_report_order where ID=$orderid")->row();

                if ($result) {

                    if ($tinhtrangdonhang == $result->Status) {

                        echo "false";

                        return;
                    }

                    $data = array(
                        'Status' => $tinhtrangdonhang,
                        'Ghichu' => $ghichu,
                        'HistoryEdited' => date('Y-m-d H:i:s')
                    );

                    if ($tinhtrangdonhang == 5) {

                        if ($result->KhoID == 0) {

                            echo "false";

                            return;
                        }
                    }

                    $this->db->where("ID", $result->ID);

                    $this->db->update('ttp_report_order', $data);

                    if ($tinhtrangdonhang == 4) {

                        $details = $this->db->query("select b.ID,b.ProductsID,c.CategoriesID,b.Amount,b.ShipmentID from ttp_report_orderdetails b,ttp_report_products c where b.ProductsID=c.ID and b.OrderID=$orderid")->result();

                        $arr_del = array();

                        if (count($details) > 0) {

                            foreach ($details as $row) {

                                $bundle = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$row->ID")->result();

                                if (count($bundle) > 0) {

                                    foreach ($bundle as $item) {

                                        $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                                        if ($check) {

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1");

                                            if ($check->DateInventory == date('Y-m-d')) {

                                                $this->db->query("update ttp_report_inventory set Available=Available+$item->Amount,LastEdited=1 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$result->KhoID");

                                                $data_log = array(
                                                    'ProductsID' => $item->ProductsID,
                                                    'OnHand' => $check->OnHand,
                                                    'Available' => $check->Available + $item->Amount,
                                                    'WarehouseID' => $result->KhoID,
                                                    'ShipmentID' => $item->ShipmentID,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $item->Amount,
                                                    'Action' => 'update',
                                                    'Method' => 'update order id ' . $orderid
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$check->OnHand,$check->Available+$item->Amount,$result->KhoID,$item->ShipmentID,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $item->ProductsID,
                                                    'OnHand' => $check->OnHand,
                                                    'Available' => $check->Available + $item->Amount,
                                                    'WarehouseID' => $result->KhoID,
                                                    'ShipmentID' => $item->ShipmentID,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $item->Amount,
                                                    'Action' => 'update',
                                                    'Method' => 'update order id ' . $orderid
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                } else {

                                    $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                                    if ($check) {

                                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1");

                                        if ($check->DateInventory == date('Y-m-d')) {

                                            $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,LastEdited=1 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$result->KhoID");

                                            $data_log = array(
                                                'ProductsID' => $row->ProductsID,
                                                'OnHand' => $check->OnHand,
                                                'Available' => $check->Available + $row->Amount,
                                                'WarehouseID' => $result->KhoID,
                                                'ShipmentID' => $row->ShipmentID,
                                                'DateInventory' => date('Y-m-d'),
                                                'Type' => "Available",
                                                'Amount' => $row->Amount,
                                                'Action' => 'update',
                                                'Method' => 'update order id ' . $orderid
                                            );

                                            $this->write_log_inventory($data_log);
                                        } else {

                                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$check->OnHand,$check->Available+$row->Amount,$result->KhoID,$row->ShipmentID,'" . date('Y-m-d') . "',1)");

                                            $data_log = array(
                                                'ProductsID' => $row->ProductsID,
                                                'OnHand' => $check->OnHand,
                                                'Available' => $check->Available + $row->Amount,
                                                'WarehouseID' => $result->KhoID,
                                                'ShipmentID' => $row->ShipmentID,
                                                'DateInventory' => date('Y-m-d'),
                                                'Type' => "Available",
                                                'Amount' => $row->Amount,
                                                'Action' => 'update',
                                                'Method' => 'update order id ' . $orderid
                                            );

                                            $this->write_log_inventory($data_log);
                                        }
                                    }
                                }

                                $CategoriesID = json_decode($row->CategoriesID, true);

                                $CategoriesID = is_array($CategoriesID) ? $CategoriesID : array();

                                $carton = 62;

                                if (in_array($carton, $CategoriesID)) {

                                    $arr_del[] = $row->ProductsID;
                                }
                            }
                        }

                        if (count($arr_del) > 0) {

                            $arr_del = implode(',', $arr_del);

                            $this->db->query("delete from ttp_report_orderdetails where ProductsID in($arr_del) and OrderID=$orderid");
                        }
                    }

                    $datahis = array(
                        'OrderID' => $result->ID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => $tinhtrangdonhang,
                        "Ghichu" => $ghichu,
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);

                    echo "OK";

                    return;
                }
            }
        }

        echo "false";
    }

    public function add_from_kho() {

        $ProductsID = isset($_POST["ProductsID"]) ? $_POST["ProductsID"] : array();

        $Amount = isset($_POST["Amount"]) ? $_POST["Amount"] : array();

        $OrderID = isset($_POST["OrderID"]) ? $_POST["OrderID"] : 0;

        $WarehouseID = isset($_POST["WarehouseID"]) ? $_POST["WarehouseID"] : 0;

        if ($OrderID > 0) {

            $order = $this->db->query("select ID,KhoID,Status,OrderType from ttp_report_order where ID=$OrderID")->row();

            if ($order) {

                if ($order->Status == 4) {

                    echo "OK";

                    return;
                }

                $details = $this->db->query("select b.ProductsID,c.CategoriesID,b.Amount,b.ShipmentID from ttp_report_orderdetails b,ttp_report_products c where b.ProductsID=c.ID and b.OrderID=$OrderID")->result();

                $arr_details = array();

                if (count($details) > 0) {

                    foreach ($details as $row) {

                        $Categories = json_decode($row->CategoriesID, true);

                        $Categories = is_array($Categories) ? $Categories : array();

                        if (!in_array(62, $Categories)) {

                            $arr_details[] = $row->ProductsID;
                        }
                    }
                }

                if (count($ProductsID > 0)) {

                    if ($order->KhoID == $WarehouseID) {

                        $arr = array();

                        foreach ($ProductsID as $key => $row) {

                            if (!in_array($row, $arr_details)) {

                                $soluong = isset($Amount[$key]) ? $Amount[$key] : 0;

                                $Shipment = $this->get_shipment_arr($row, $order->KhoID, $soluong);

                                if (is_array($Shipment) && count($Shipment) > 0) {

                                    foreach ($Shipment as $keyitem => $valueitem) {

                                        if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available'])) {

                                            $arr[] = "($OrderID,$row,0," . $valueitem['Amount'] . ",0,'',0,$keyitem)";

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row and ShipmentID=$keyitem and WarehouseID=$order->KhoID and LastEdited=1");

                                            if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$order->KhoID,$keyitem,'" . date('Y-m-d') . "',1)");
                                            } else {

                                                $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$row and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$order->KhoID");
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (count($arr) > 0) {

                            $arr = "insert into ttp_report_orderdetails(OrderID,ProductsID,Price,Amount,Total,Khuyenmai,PriceDown,ShipmentID) values" . implode(',', $arr);

                            $this->db->query($arr);
                        }
                    }
                }

                echo "OK";
            }
        } else {

            echo "false";
        }
    }

    public function save_from_ketoan() {

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 3 || $this->user->UserType == 7) {

            $ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '';

            $tinhtrangdonhang = isset($_POST['Tinhtrangdonhang']) ? $_POST['Tinhtrangdonhang'] : 3;

            $orderid = isset($_POST['OrderID']) ? $_POST['OrderID'] : 0;

            if ($orderid > 0) {

                $result = $this->db->query("select * from ttp_report_order where ID=$orderid")->row();

                if ($result) {

                    if ($tinhtrangdonhang == $result->Status) {

                        echo "false";

                        return;
                    }

                    $data = array(
                        'Status' => $tinhtrangdonhang,
                        'Ghichu' => $ghichu,
                        'HistoryEdited' => date('Y-m-d H:i:s')
                    );

                    $this->db->where("ID", $result->ID);

                    $this->db->update('ttp_report_order', $data);

                    if ($tinhtrangdonhang == 6) {

                        $details = $this->db->query("select b.ID,b.ProductsID,c.CategoriesID,b.Amount,b.ShipmentID from ttp_report_orderdetails b,ttp_report_products c where b.ProductsID=c.ID and b.OrderID=$orderid")->result();

                        $arr_del = array();

                        if (count($details) > 0) {

                            foreach ($details as $row) {

                                $bundle = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$row->ID")->result();

                                if (count($bundle) > 0) {

                                    foreach ($bundle as $item) {

                                        $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                                        if ($check) {

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1");

                                            if ($check->DateInventory == date('Y-m-d')) {

                                                $this->db->query("update ttp_report_inventory set Available=Available+$item->Amount,LastEdited=1 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$result->KhoID");

                                                $data_log = array(
                                                    'ProductsID' => $item->ProductsID,
                                                    'OnHand' => $check->OnHand,
                                                    'Available' => $check->Available + $item->Amount,
                                                    'WarehouseID' => $result->KhoID,
                                                    'ShipmentID' => $item->ShipmentID,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $item->Amount,
                                                    'Action' => 'update',
                                                    'Method' => 'update order id ' . $orderid
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$check->OnHand,$check->Available+$item->Amount,$result->KhoID,$item->ShipmentID,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $item->ProductsID,
                                                    'OnHand' => $check->OnHand,
                                                    'Available' => $check->Available + $item->Amount,
                                                    'WarehouseID' => $result->KhoID,
                                                    'ShipmentID' => $item->ShipmentID,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $item->Amount,
                                                    'Action' => 'update',
                                                    'Method' => 'update order id ' . $orderid
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                } else {

                                    $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                                    if ($check) {

                                        $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and WarehouseID=$result->KhoID and LastEdited=1");

                                        if ($check->DateInventory == date('Y-m-d')) {

                                            $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,LastEdited=1 where ProductsID=$row->ProductsID and ShipmentID=$row->ShipmentID and DateInventory='$check->DateInventory' and WarehouseID=$result->KhoID");

                                            $data_log = array(
                                                'ProductsID' => $row->ProductsID,
                                                'OnHand' => $check->OnHand,
                                                'Available' => $check->Available + $row->Amount,
                                                'WarehouseID' => $result->KhoID,
                                                'ShipmentID' => $row->ShipmentID,
                                                'DateInventory' => date('Y-m-d'),
                                                'Type' => "Available",
                                                'Amount' => $row->Amount,
                                                'Action' => 'update',
                                                'Method' => 'update order id ' . $orderid
                                            );

                                            $this->write_log_inventory($data_log);
                                        } else {

                                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$check->OnHand,$check->Available+$row->Amount,$result->KhoID,$row->ShipmentID,'" . date('Y-m-d') . "',1)");

                                            $data_log = array(
                                                'ProductsID' => $row->ProductsID,
                                                'OnHand' => $check->OnHand,
                                                'Available' => $check->Available + $row->Amount,
                                                'WarehouseID' => $result->KhoID,
                                                'ShipmentID' => $row->ShipmentID,
                                                'DateInventory' => date('Y-m-d'),
                                                'Type' => "Available",
                                                'Amount' => $row->Amount,
                                                'Action' => 'update',
                                                'Method' => 'update order id ' . $orderid
                                            );

                                            $this->write_log_inventory($data_log);
                                        }
                                    }
                                }

                                $CategoriesID = json_decode($row->CategoriesID, true);

                                $CategoriesID = is_array($CategoriesID) ? $CategoriesID : array();

                                $carton = 62;

                                if (in_array($carton, $CategoriesID)) {

                                    $arr_del[] = $row->ProductsID;
                                }
                            }
                        }

                        if (count($arr_del) > 0) {

                            $arr_del = implode(',', $arr_del);

                            $this->db->query("delete from ttp_report_orderdetails where ProductsID in($arr_del) and OrderID=$orderid");
                        }
                    }

                    $datahis = array(
                        'OrderID' => $result->ID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => $tinhtrangdonhang,
                        "Ghichu" => $ghichu,
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);

                    echo "OK";

                    return;
                }
            }
        }

        echo "false";
    }

    public function update_transport_status() {

        $TransportRef = isset($_POST['TransportRef']) ? mysql_real_escape_string($_POST['TransportRef']) : '';

        $TransportStatus = isset($_POST['TransportStatus']) ? mysql_real_escape_string($_POST['TransportStatus']) : 'Phát thành công';

        $Code = isset($_POST['Code']) ? mysql_real_escape_string($_POST['Code']) : 0;

        $Fee = isset($_POST['Fee']) ? mysql_real_escape_string($_POST['Fee']) : 0;

        $order = $this->db->query("select ID,Status from ttp_report_order where TransportRef='$TransportRef'")->row();

        if ($order) {

            if ($Code == 420) {

                if ($order->Status != 0) {

                    $this->db->query("update ttp_report_order set TransportStatus='$TransportStatus',Status=0,TransportFee=$Fee,DateSuccess='" . date('Y-m-d H:i:s') . "' where ID=$order->ID");

                    $datahis = array(
                        'OrderID' => $order->ID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => 0,
                        "Ghichu" => "Cập nhật trạng thái từ đối tác vận chuyển",
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);
                }
            } else {

                $this->db->query("update ttp_report_order set TransportStatus='$TransportStatus',TransportFee=$Fee where ID=$order->ID");
            }

            echo "OK";
        } else {

            echo 'False';
        }
    }

    public function save_from_dieuphoi() {

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 4) {

            $ghichu = isset($_POST['Ghichu']) ? $_POST['Ghichu'] : '';

            $tinhtrangdonhang = isset($_POST['Tinhtrangdonhang']) ? $_POST['Tinhtrangdonhang'] : 7;

            $TransportID = isset($_POST['TransportID']) ? $_POST['TransportID'] : 1;

            $orderid = isset($_POST['OrderID']) ? $_POST['OrderID'] : 0;

            if ($orderid > 0) {

                $result = $this->db->query("select ID,Status,KhoID from ttp_report_order where ID=$orderid")->row();

                if ($result) {

                    if ($tinhtrangdonhang == $result->Status) {

                        echo "false";

                        return;
                    }

                    $data = array(
                        'Status' => $tinhtrangdonhang,
                        'Ghichu' => $ghichu,
                        'HistoryEdited' => date('Y-m-d H:i:s')
                    );

                    if ($tinhtrangdonhang == 7 && $TransportID == '') {

                        echo 'false';

                        return;
                    } else {

                        if ($tinhtrangdonhang == 7) {

                            $data['TransportID'] = $TransportID;

                            $details = $this->db->query("select ID,Amount,ProductsID,ShipmentID,ImportPrice from ttp_report_orderdetails where OrderID=$orderid")->result();

                            $this->change_inventory($result, $details, 'OnHand');
                        }
                    }

                    if ($tinhtrangdonhang == 0) {

                        $data['PaymentStatus'] = 1;

                        $data['DateSuccess'] = date('Y-m-d H:i:s');
                    }

                    $this->db->where("ID", $result->ID);

                    $this->db->update('ttp_report_order', $data);

                    $datahis = array(
                        'OrderID' => $result->ID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => $tinhtrangdonhang,
                        "Ghichu" => $ghichu,
                        "UserID" => $this->user->ID
                    );

                    $this->db->insert('ttp_report_orderhistory', $datahis);

                    echo "OK";

                    return;
                }
            }
        }

        echo "false";
    }

    public function get_last_order_city($ID = 0) {

        $result = $this->db->query("select a.CityID,b.AreaID,a.DistrictID,c.Title as DistrictTitle,b.Title as CityTitle from ttp_report_order a,ttp_report_city b,ttp_report_district c where a.DistrictID=c.ID and a.CityID=b.ID and a.CustomerID=$ID order by a.ID DESC limit 0,1")->row();

        $data = array('CityID' => 0, 'AreaID' => 0, 'DistrictID' => 0, 'CityHtml' => '', 'DistrictHtml' => '', 'WarehouseHtml' => '<option value="">Không có kho hàng</option>');

        if ($result) {

            $data = array(
                'AreaID' => $result->AreaID,
                'CityID' => $result->CityID,
                'DistrictID' => $result->DistrictID,
                'CityHtml' => "<option value='$result->CityID'>$result->CityTitle</option>",
                'DistrictHtml' => "<option value='$result->DistrictID'>$result->DistrictTitle</option>"
            );

            $warehouse = $this->db->query("select b.MaKho,b.ID from ttp_report_city a,ttp_report_warehouse b where a.DefaultWarehouse=b.ID and a.ID=$result->CityID")->row();

            if ($warehouse) {

                if ($this->user->Channel == 2) {

                    $data['WarehouseHtml'] = "<option value='1'>TTP</option>";
                } else {

                    $data['WarehouseHtml'] = "<option value='$warehouse->ID'>$warehouse->MaKho</option>";
                }
            }
        }

        echo json_encode($data);
    }

    public function accept_money($id = 0) {

        if ($id > 0) {

            $order = $this->db->query("select ID from ttp_report_order where ID=$id")->row();

            if ($order) {

                $this->db->query("update ttp_report_order set PaymentStatus=1 where ID=$id");
            }
        }

        redirect(ADMINPATH . '/report/import_order/preview/' . $id);
    }

    public function get_order_area($ID = 0, $Type = 1) {

        $result = $this->db->query("select a.CityID,b.AreaID,a.DistrictID,c.Title as DistrictTitle,b.Title as CityTitle from ttp_report_customer a,ttp_report_city b,ttp_report_district c where a.DistrictID=c.ID and a.CityID=b.ID and a.ID=$ID order by a.ID DESC limit 0,1")->row();

        $data = array('CityID' => 0, 'AreaID' => 0, 'DistrictID' => 0, 'CityHtml' => '', 'DistrictHtml' => '', 'WarehouseHtml' => '<option value="">Không có kho hàng</option>');

        if ($result) {

            $data = array(
                'AreaID' => $result->AreaID,
                'CityID' => $result->CityID,
                'DistrictID' => $result->DistrictID,
                'CityHtml' => "<option value='$result->CityID'>$result->CityTitle</option>",
                'DistrictHtml' => "<option value='$result->DistrictID'>$result->DistrictTitle</option>"
            );

            $type = $Type == 1 ? 'GT' : 'DefaultWarehouse';

            $type = $Type == 2 ? 'MT' : $type;

            $warehouse = $this->db->query("select b.MaKho,b.ID from ttp_report_city a,ttp_report_warehouse b where a.$type=b.ID and a.ID=$result->CityID")->row();

            if ($warehouse) {

                $data['WarehouseHtml'] = "<option value='$warehouse->ID'>$warehouse->MaKho</option>";
            }
        }

        echo json_encode($data);
    }

    public function acceptsingle($id = 0) {

        if ($this->user->UserType == 7 || $this->user->IsAdmin == 1) {

            $this->db->query("update ttp_report_order set Accept=2 where Accept=1 and ID=$id");
        }

        $next = $this->db->query("select ID from ttp_report_order where Accept=1 and Status=5 order by ID ASC limit 0,1")->row();

        if ($next) {

            redirect(ADMINPATH . "/report/import_order/preview/" . $next->ID);
        } else {

            redirect(ADMINPATH . "/report/import_order");
        }
    }

    public function accept_level1($id = 0) {

        if ($this->user->UserType == 3 || $this->user->IsAdmin == 1) {

            $this->db->query("update ttp_report_order set Accept=1 where Accept=0 and ID=$id");

            redirect(ADMINPATH . "/report/import_order/preview/" . $id);
        }
    }

    public function load_data_from_file() {

        $this->load->view("admin/import_order_load_data_from_file");
    }

    public function upload_import() {

        $file = isset($_FILES['file']['tmp_name']) ? $_FILES['file']['tmp_name'] : "";

        $name = isset($_FILES['file']['name']) ? $_FILES['file']['name'] : "";

        if ($name != '') {

            $ext = explode('.', $name);

            $ext = $ext[count($ext) - 1];

            if ($ext != 'xls' && $ext != 'xlsx') {

                $this->load->view("admin/import_order_data_form", array('error' => 'Định dạng file không được chấp nhận. Vui lòng chọn file theo đúng định dạng quy định.'));

                return;
            }
        }

        if (file_exists($file)) {

            $result = array();

            require_once 'public/plugin/PHPExcel.php';

            $objPHPExcel = PHPExcel_IOFactory::load($file);

            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $highestRow = $worksheet->getHighestRow();

                $highestColumn = $worksheet->getHighestColumn();

                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                for ($row = 1; $row <= $highestRow; $row ++) {

                    $data = array();

                    for ($key = 0; $key <= $highestColumnIndex; $key++) {

                        $value = trim($worksheet->getCellByColumnAndRow($key, $row)->getValue());

                        $data[] = $value;
                    }

                    $result[] = $data;
                }
            }

            $this->load->view("admin/import_order_data_form", array('data' => $result, 'filename' => $name));
        } else {

            $this->load->view("admin/import_order_data_form");
        }
    }

    public function active_upload_import() {

        $result = isset($_POST['data']) ? $_POST['data'] : '[]';

        $result = json_decode($result);

        if (count($result) > 0) {

            $query_date = date('Y-m-d');

            $start = date('Y-m-01', strtotime($query_date));

            $stop = date('Y-m-t', strtotime($query_date));

            foreach ($result as $row) {

                if ($row->pxk != '' && $row->ngaynhantien != '') {

                    //if(strtotime($row->ngaynhantien)>=strtotime($start) && strtotime($row->ngaynhantien)<=strtotime($stop)){

                    $this->db->query("update ttp_report_export_warehouse set TransferMoney=1,DateTransferMoney='$row->ngaynhantien' where TransferMoney=0 and MaXK='$row->pxk'");

                    //}
                }
            }

            $data = array("Error" => null);
        } else {

            $data = array("Error" => "Không có thông tin để lưu vào hệ thống .");
        }

        echo json_encode($data);
    }

    public function download_error() {

        $result = isset($_POST['Data']) ? $_POST['Data'] : '[]';

        $result = json_decode($result);

        if (count($result) > 0) {

            ini_set('memory_limit', '3500M');

            error_reporting(E_ALL);

            ini_set('display_errors', TRUE);

            ini_set('display_startup_errors', TRUE);

            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');

            require_once 'public/plugin/PHPExcel.php';

            $arr = array();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                    ->setLastModifiedBy("Maarten Balliauw")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");

            $i = 1;

            foreach ($result as $row) {

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $row->pxk)
                        ->setCellValue('B' . $i, $row->ngaynhantien);

                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('PHIEULOI');

            $objPHPExcel->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="PHIEULOI.xls"');

            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');

            header('Cache-Control: cache, must-revalidate');

            header('Pragma: public');



            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            ob_end_clean();

            $objWriter->save('php://output');

            exit;
        } else {

            redirect(ADMINPATH . "/report/import_order/export_warehouse");
        }
    }

    public function request_single_available_to_supplier() {

        $id = isset($_POST['id']) ? (int) $_POST['id'] : 0;

        $check = $this->db->query("select a.*,b.VariantType,b.SupplierID from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.ID=$id")->row();

        if ($check) {

            $data_request = file_get_contents("log/supplier/request_available.txt");

            $data_request = $data_request != '' ? json_decode($data_request, true) : array();

            $timerequest = date('Y-m-d H:i:s');

            if ($check->VariantType == 2) {

                $products = array();

                $details = $this->db->query("select * from ttp_report_products_bundle where Src_ProductsID=$check->ProductsID")->result();

                if (count($details) > 0) {

                    foreach ($details as $item) {

                        if (isset($products[$item->Des_ProductsID])) {

                            $products[$item->Des_ProductsID] = $products[$item->Des_ProductsID] + ($item->Quantity * $check->Amount);
                        } else {

                            $products[$item->Des_ProductsID] = $item->Quantity * $check->Amount;
                        }

                        if (isset($data_request[$check->SupplierID])) {

                            $data_request[$check->SupplierID] = $data_request[$check->SupplierID] + 1;
                        } else {

                            $data_request[$check->SupplierID] = 1;
                        }
                    }
                }

                if (count($products) > 0) {

                    $sql = array();

                    foreach ($products as $key => $row) {

                        $sql[] = "($id," . $this->user->ID . ",$key,$row,'$timerequest')";
                    }

                    if (count($sql) > 0) {

                        $sql = "insert into ttp_report_order_request_available(OrderID,UserID,ProductsID,Amount,TimeRequest) values" . implode(',', $sql);

                        $this->db->query($sql);
                    }
                }
            } else {

                $data = array(
                    'OrderID' => $check->OrderID,
                    'UserID' => $this->user->ID,
                    'ProductsID' => $check->ProductsID,
                    'Amount' => $check->Amount,
                    'TimeRequest' => $timerequest
                );

                $this->db->insert('ttp_report_order_request_available', $data);

                if (isset($data_request[$check->SupplierID])) {

                    $data_request[$check->SupplierID] = $data_request[$check->SupplierID] + 1;
                } else {

                    $data_request[$check->SupplierID] = 1;
                }
            }

            file_put_contents("log/supplier/request_available.txt", json_encode($data_request));
        }
    }

    public function request_available_to_supplier($id = 0) {

        $id = (int) $id;

        $check = $this->db->query("select * from ttp_report_order_request_available where OrderID=$id")->row();

        if (!$check) {

            $products = array();

            $details = $this->db->query("select a.*,b.VariantType,b.SupplierID from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$id")->result();

            if (count($details) > 0) {

                $data_request = file_get_contents("log/supplier/request_available.txt");

                $data_request = $data_request != '' ? json_decode($data_request, true) : array();

                foreach ($details as $row) {

                    if ($row->VariantType == 2) {

                        $details_bundle = $this->db->query("select a.* from ttp_report_products_bundle a where a.Src_ProductsID=$row->ProductsID")->result();

                        if (count($details_bundle) > 0) {

                            foreach ($details_bundle as $item) {

                                if (isset($products[$item->Des_ProductsID])) {

                                    $products[$item->Des_ProductsID] = $products[$item->Des_ProductsID] + ($item->Quantity * $row->Amount);
                                } else {

                                    $products[$item->Des_ProductsID] = $item->Quantity * $row->Amount;
                                }

                                if (isset($data_request[$row->SupplierID])) {

                                    $data_request[$row->SupplierID] = $data_request[$row->SupplierID] + 1;
                                } else {

                                    $data_request[$row->SupplierID] = 1;
                                }
                            }
                        }
                    } else {

                        if (isset($products[$row->ProductsID])) {

                            $products[$row->ProductsID] = $products[$row->ProductsID] + $row->Amount;
                        } else {

                            $products[$row->ProductsID] = $row->Amount;
                        }

                        if (isset($data_request[$row->SupplierID])) {

                            $data_request[$row->SupplierID] = $data_request[$row->SupplierID] + 1;
                        } else {

                            $data_request[$row->SupplierID] = 1;
                        }
                    }
                }
            }

            file_put_contents("log/supplier/request_available.txt", json_encode($data_request));

            $timerequest = date('Y-m-d H:i:s');

            if (count($products) > 0) {

                $sql = array();

                foreach ($products as $key => $row) {

                    $sql[] = "($id," . $this->user->ID . ",$key,$row,'$timerequest')";
                }

                if (count($sql) > 0) {

                    $sql = "insert into ttp_report_order_request_available(OrderID,UserID,ProductsID,Amount,TimeRequest) values" . implode(',', $sql);

                    $this->db->query($sql);
                }
            }
        }

        redirect(ADMINPATH . '/report/import_order/edit/' . $id);
    }

    /*

     * *****************************************

     *   Load custom fillter by field name    *

     *                                        *

     * *****************************************

     */

    public function load_fillter_by_type_and_field() {

        $field = isset($_POST['FieldName']) ? $_POST['FieldName'] : '';

        if ($field == 'vung') {

            $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();

            if (count($result) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($result as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }

                echo "</select>";
            }
        } elseif ($field == 'tinhthanh') {

            $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();

            if (count($result) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($result as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }

                echo "</select>";
            }
        } elseif ($field == 'quanhuyen') {

            $result = $this->db->query("select * from ttp_report_district order by Title ASC")->result();

            if (count($result) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($result as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }

                echo "</select>";
            }
        } elseif ($field == 'nhomnganhhang') {

            $result = $this->db->query("select * from ttp_report_categories where IsLast=1 order by Title ASC")->result();

            if (count($result) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($result as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }

                echo "</select>";
            }
        } elseif ($field == 'thuonghieu') {

            $result = $this->db->query("select ID,Title from ttp_report_trademark order by Title ASC")->result();

            if (count($result) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($result as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }

                echo "</select>";
            }
        } elseif ($field == 'trangthaidonhang') {

            echo "<select name='FieldText[]' class='form-control'>

                    <option value='9'>Đơn hàng chuyển sang nv điều phối</option>

                    <option value='8'>Đơn hàng bị trả về</option>

                    <option value='7'>Chuyển sang bộ phận giao hàng</option>

                    <option value='6'>Đơn hàng bị trả về từ kế toán</option>

                    <option value='5'>Đơn hàng chờ kế toán duyệt</option>

                    <option value='4'>Đơn hàng bị trả về từ kho</option>

                    <option value='3'>Đơn hàng mới chờ kho duyệt</option>

                    <option value='2'>Đơn hàng nháp</option>

                    <option value='0'>Đơn hàng thành công</option>

                    <option value='1'>Đơn hàng hủy</option>

                </select>";
        } elseif ($field == 'user') {

            $userlist = $this->db->query("select ID,UserName from ttp_user where UserType=1")->result();

            if (count($userlist) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($userlist as $row) {

                    echo "<option value='$row->ID'>$row->UserName</option>";
                }

                echo "</select>";
            }
        } elseif ($field == "loaikhachhang") {

            echo "<select name='FieldText[]' class='form-control'>

                <option value='0'>Khách hàng mới</option>

                <option value='1'>Khách hàng cũ</option>

            </select>";
        } elseif ($field == "thutien") {

            echo "<select name='FieldText[]' class='form-control'>

                <option value='0'>Chưa thu tiền</option>

                <option value='1'>Đã thu tiền</option>

            </select>";
        } elseif ($field == "doitacvanchuyen") {

            $transport = $this->db->query("select * from ttp_report_transport")->result();

            if (count($transport) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($transport as $row) {

                    echo "<option value='$row->ID'>$row->Title</option>";
                }

                echo "</select>";
            }
        } elseif ($field == "khoxuathang") {

            $transport = $this->db->query("select * from ttp_report_warehouse")->result();

            if (count($transport) > 0) {

                echo "<select name='FieldText[]' class='form-control'>";

                foreach ($transport as $row) {

                    echo "<option value='$row->ID'>$row->MaKho</option>";
                }

                echo "</select>";
            }
        } else {

            echo '<input type="text" name="FieldText[]" class="form-control" id="textsearch" />';
        }
    }

    /*

     * *****************************************

     *   Check customer phone current day     *

     *                                        *

     * *****************************************

     */

    public function checkphone() {

        $phone = isset($_POST['Phone1']) ? $_POST['Phone1'] : '';

        if ($phone != '') {

            $customer = $this->db->query("select * from ttp_report_customer where Phone1='$phone'")->row();

            if ($customer) {

                $order = $this->db->query("select a.ID,a.AddressOrder,a.Total,a.Chietkhau,b.UserName,a.MaDH,a.SoluongSP,b.ID as CusID from ttp_report_order a,ttp_user b where a.UserID=b.ID and a.CustomerID=$customer->ID")->result();

                if (count($order) > 0) {

                    echo "<div class='row' style='text-align:center'><a class='btn btn-default' onclick='accept_oldcustomer($customer->ID)'>Click vào đây để xác nhận sử dụng thông tin khách hàng cũ</a></div>";

                    echo "<div style='width:100%;padding:5px 10px;text-align:center;margin-bottom:10px;border: 1px solid #EA8686;background: #F9CFD5;'>Vui lòng kiểm tra lại loại khách hàng . Số điện thoại này đã được sử dụng bởi khách hàng cũ .</div>";

                    echo "<h3 style='font-weight: bold;border-bottom: 1px solid #ccc;padding: 5px 0px;text-transform: uppercase;margin-bottom:10px'>Thông tin khách hàng</h3>";

                    echo "<div>

                            <p style='line-height: 25px;font-size:13px !important'><b style='float:left;width:150px;font-size:13px !important''>Tên khách hàng :</b> $customer->Name</p>

                            <p style='line-height: 25px;font-size:13px !important'><b style='float:left;width:150px;font-size:13px !important''>Địa chỉ :</b> $customer->Address</p>

                            <p style='line-height: 25px;font-size:13px !important'><b style='float:left;width:150px;font-size:13px !important''>Phone1 :</b> $customer->Phone1</p>

                            <p style='line-height: 25px;font-size:13px !important'><b style='float:left;width:150px;font-size:13px !important''>Phone2 :</b> $customer->Phone2</p>

                        </div>";

                    echo "<h3 style='font-weight: bold;margin-bottom:10px;margin-top:30px;border-bottom: 1px solid #ccc;padding: 5px 0px;text-transform: uppercase;'>Các đơn hàng gần đây nhất của khách hàng này :</h3>";

                    echo "<table>";

                    echo "<tr><th>Mã đơn hàng</th><th>Số lượng SP</th><th>Tổng tiền hàng</th><th>Chiết khấu</th><th style='width: 100px;'>Nhân viên bán</th></tr>";

                    foreach ($order as $row) {

                        echo "<tr>

                                <td>$row->MaDH</td>

                                <td>$row->SoluongSP</td>

                                <td>" . number_format($row->Total) . "</td>

                                <td style='width:150px'>$row->Chietkhau</td>

                                <td style='width: 150px;'>$row->UserName</td>

                            </tr>";
                    }

                    echo "</table>";

                    return;
                }
            }
        }

        echo "false";
    }

    /*

     * *****************************************

     *   Send cancel order request            *

     *                                        *

     * *****************************************

      10=>'Đơn hàng chờ hủy',

      9=>'Chờ nhân viên điều phối',

      8=>'Đơn hàng bị trả về',

      7=>'Chuyển sang bộ phận giao hàng',

      6=>'Đơn hàng bị trả về từ kế toán',

      5=>'Đơn hàng chờ kế toán duyệt',

      4=>'Đơn hàng bị trả về từ kho',

      3=>'Đơn hàng mới chờ kho duyệt',

      2=>'Đơn hàng nháp',

      0=>'Đơn hàng thành công',

      1=>'Đơn hàng hủy'

     */

    public function cancel_request($id = 0) {

        if ($id != 0) {

            $ghichu = isset($_POST['Note']) ? $_POST['Note'] : '';

            $IDReason = isset($_POST['ReasonID']) ? $_POST['ReasonID'] : 0;

            if ($ghichu == '' && $IDReason == 0) {

                echo 'Vui lòng điền ghi chú hủy đơn hàng !';

                return;
            }

            if ($this->user->UserType == 1) {

                $result = $this->db->query("select ID,Status,KhoID,MaDH,AffiliateID from ttp_report_order where ID=$id and UserID=" . $this->user->ID)->row();
            } else {

                $result = $this->db->query("select ID,Status,KhoID,MaDH,AffiliateID from ttp_report_order where ID=$id")->row();
            }

            if ($result) {

                if ($result->Status == 1) {

                    echo "Đơn hàng đã hủy . Vui lòng không gửi yêu cầu nữa !";

                    return;
                }

                if ($result->Status == 10) {

                    echo "Đơn hàng đang chờ hủy . Vui lòng không gửi yêu cầu nữa !";

                    return;
                }

                if ($result->Status == 0) {

                    echo "Đơn hàng này đã thành công . Vui lòng không gửi yêu cầu nữa !";

                    return;
                }

                $checkreason = $this->db->query("select * from ttp_report_order_reason_details where OrderID=$id")->row();

                if ($checkreason) {

                    if ($this->user->UserType == 4) {

                        $this->db->query("update ttp_report_order_reason_details set PLReason = $IDReason,Another3PL='$ghichu' where OrderID=$id");
                    } else {

                        $ghichu_temp = is_numeric($IDReason) ? $IDReason : 0;

                        if (isset($_POST['Follow'])) {

                            $follow = (int) $_POST['Follow'];

                            $this->db->query("update ttp_report_order_reason_details set SaleReasonID = $ghichu_temp,AnotherSale='$ghichu',Follow=$follow where OrderID=$id");
                        } else {

                            $this->db->query("update ttp_report_order_reason_details set SaleReasonID = $ghichu_temp,AnotherSale='$ghichu' where OrderID=$id");
                        }
                    }
                } else {

                    if ($this->user->UserType == 4) {

                        $this->db->query("insert into ttp_report_order_reason_details(OrderID,PLReason,Source,Another3PL) value($id,$IDReason,1,'$ghichu')");
                    } else {

                        $ghichu_temp = is_numeric($IDReason) ? $IDReason : 0;

                        if (isset($_POST['Follow'])) {

                            $follow = (int) $_POST['Follow'];

                            $this->db->query("insert into ttp_report_order_reason_details(OrderID,SaleReasonID,AnotherSale,Follow) value($id,$ghichu_temp,'$ghichu',$follow)");
                        } else {

                            $this->db->query("insert into ttp_report_order_reason_details(OrderID,SaleReasonID,AnotherSale) value($id,$ghichu_temp,'$ghichu')");
                        }
                    }
                }

                if ($ghichu == '') {

                    $ghichu = $this->db->query("select * from ttp_report_order_reason where ID=$IDReason")->row();

                    $ghichu = $ghichu ? $ghichu->Title : '';
                }

                $export = $this->db->query("select ID from ttp_report_export_warehouse where OrderID=$id")->row();

                if ($export) {

                    $details = $this->db->query("select Amount,ProductsID,ShipmentID,ImportPrice from ttp_report_orderdetails a,ttp_report_products b where a.OrderID=$id and a.ProductsID=b.ID and b.VariantType in(0,1)")->result();

                    if ($result->Status == 5 || $result->Status == 9) {

                        $datahis = array(
                            'OrderID' => $result->ID,
                            'Thoigian' => date('Y-m-d H:i:s', time()),
                            'Status' => 10,
                            "Ghichu" => $ghichu,
                            "UserID" => $this->user->ID
                        );

                        $this->db->insert('ttp_report_orderhistory', $datahis);

                        $this->db->query("update ttp_report_order set Status=10 where ID=$result->ID");

                        $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> gửi yêu cầu hủy đơn hàng <b class='text-primary'>$result->MaDH</b>", 2);

                        $data = array(
                            'OrderID' => $result->ID,
                            'Created' => date('Y-m-d H:i:s'),
                            'LastEdited' => date('Y-m-d H:i:s'),
                            'UserID' => $this->user->ID,
                            'Note' => $ghichu
                        );

                        $this->db->insert('ttp_report_request_cancelorder', $data);

                        echo "true";

                        return;
                    } elseif ($result->Status == 7 || $result->Status == 11) {

                        $arr = array();

                        $slsp = 0;

                        $total = 0;

                        if (count($details) > 0) {

                            foreach ($details as $row) {

                                $slsp = $slsp + $row->Amount;

                                $arr[] = "(ImportIDDV,$row->ProductsID,$row->ShipmentID,'VND',1,0,$row->Amount)";
                            }
                        }

                        $details = $this->db->query("select a.Amount,a.ProductsID,a.ShipmentID from ttp_report_orderdetails_bundle a,ttp_report_products b where a.OrderID=$id and a.ProductsID=b.ID")->result();

                        if (count($details) > 0) {

                            foreach ($details as $row) {

                                $slsp = $slsp + $row->Amount;

                                $arr[] = "(ImportIDDV,$row->ProductsID,$row->ShipmentID,'VND',1,0,$row->Amount)";
                            }
                        }



                        if ($this->user->UserType == 4) {

                            if (count($arr) > 0) {

                                $thismonth = date('m', time());

                                $thisyear = date('Y', time());

                                $max = $this->db->query("select count(1) as max from ttp_report_inventory_import where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and Type=1")->row();

                                $max = $max ? $max->max + 1 : 1;

                                $thisyear = date('y', time());

                                $max = "YCNK1" . '_' . $result->KhoID . '_' . $thisyear . $thismonth . '_' . str_pad($max, 7, '0', STR_PAD_LEFT);



                                $data = array(
                                    'MaNK' => $max,
                                    'ExportID' => $export->ID,
                                    'KhoID' => $result->KhoID,
                                    'UserID' => $this->user->ID,
                                    'NgayNK' => date('Y-m-d'),
                                    'Type' => 1,
                                    'Note' => $ghichu,
                                    'Status' => 0,
                                    'TotalAmount' => $slsp,
                                    'TotalPrice' => $total,
                                    'Created' => date('Y-m-d H:i:s'),
                                    'LastEdited' => date('Y-m-d H:i:s')
                                );

                                $this->db->insert('ttp_report_inventory_import', $data);

                                $ID = $this->db->insert_id();

                                $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Amount) values" . implode(',', $arr);

                                $arr = str_replace('ImportIDDV', $ID, $arr);

                                $this->db->query($arr);

                                $data_his = array(
                                    'ImportID' => $ID,
                                    'Status' => 0,
                                    'Note' => $ghichu,
                                    'UserID' => $this->user->ID,
                                    'Created' => date('Y-m-d H:i:s')
                                );

                                $this->db->insert('ttp_report_inventory_import_history', $data_his);

                                $this->db->query("update ttp_report_order set Status=8,Ghichu='$ghichu' where ID=$result->ID");
                            }
                        } else {

                            $datahis = array(
                                'OrderID' => $result->ID,
                                'Thoigian' => date('Y-m-d H:i:s', time()),
                                'Status' => 10,
                                "Ghichu" => $ghichu,
                                "UserID" => $this->user->ID
                            );

                            $this->db->insert('ttp_report_orderhistory', $datahis);

                            $this->db->query("update ttp_report_order set Status=10 where ID=$result->ID");

                            $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> gửi yêu cầu hủy đơn hàng <b class='text-primary'>$result->MaDH</b>", 2);

                            $data = array(
                                'OrderID' => $result->ID,
                                'Created' => date('Y-m-d H:i:s'),
                                'LastEdited' => date('Y-m-d H:i:s'),
                                'UserID' => $this->user->ID,
                                'Note' => $ghichu
                            );

                            $this->db->insert('ttp_report_request_cancelorder', $data);

                            echo "true";

                            return;
                        }

                        $datahis = array(
                            'OrderID' => $result->ID,
                            'Thoigian' => date('Y-m-d H:i:s', time()),
                            'Status' => 1,
                            "Ghichu" => $ghichu,
                            "UserID" => $this->user->ID
                        );

                        $this->db->insert('ttp_report_orderhistory', $datahis);

                        $this->db->query("update ttp_report_order set Status=1 where ID=$result->ID");

                        $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> hủy đơn hàng <b class='text-primary'>$result->MaDH</b>", 1);

                        if ($result->AffiliateID == 27) {

                            $this->reject_affi_AT($result);
                        }

                        echo "true";

                        return;
                    }
                } else {

                    if ($result->Status == 2 || $result->Status == 3 || $result->Status == 4 || $result->Status == 5 || $result->Status == 6) {

                        $this->db->query("update ttp_report_order set Status=1,Ghichu='$ghichu' where ID=$result->ID");

                        if ($result->AffiliateID == 27) {

                            $this->reject_affi_AT($result);
                        }

                        $this->lib->write_log_data("<b>" . $this->user->UserName . "</b> hủy đơn hàng <b class='text-primary'>$order->MaDH</b>", 1);

                        $datahis = array(
                            'OrderID' => $result->ID,
                            'Thoigian' => date('Y-m-d H:i:s', time()),
                            'Status' => 1,
                            "Ghichu" => $ghichu,
                            "UserID" => $this->user->ID
                        );

                        $this->db->insert('ttp_report_orderhistory', $datahis);

                        $details = $this->db->query("select ID,Amount,ProductsID,ShipmentID from ttp_report_orderdetails where OrderID=$result->ID")->result();

                        if ($result->Status != 2 && $result->Status != 6 && $result->Status != 4)
                            $this->change_inventory($result, $details);

                        echo "true";

                        return;
                    }
                }
            }
        }

        echo "false";
    }

    public function reject_affi_AT($result) {

        $data = array(
            "transaction_id" => $result->MaDH,
            "status" => 2
        );

        $data_string = json_encode($data);

        $ch = curl_init('https://api.accesstrade.vn/v1/postbacks/conversions');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Token 0AC8BWsHsG94A_Ksr2YmGaX6WJjUk8cb'
        ));

        $response = curl_exec($ch);

        $this->write_log_affiliate_AT($result->ID, $data_string, $response);
    }

    public function write_log_affiliate_AT($orderid = 0, $data = '', $response = '') {

        $sql = array(
            'OrderID' => $orderid,
            'Data' => $data,
            'Response' => $response,
            'Created' => date('Y-m-d H:i:s')
        );

        $this->db->insert("ttp_report_affiliate_tracking", $sql);
    }

    public function update_reason($id = 0) {

        if ($id != 0) {

            $ghichu = isset($_POST['Note']) ? $_POST['Note'] : '';

            $IDReason = isset($_POST['ReasonID']) ? $_POST['ReasonID'] : 0;

            if ($ghichu == '' && $IDReason == 0) {

                echo 'Vui lòng điền ghi chú hủy đơn hàng !';

                return;
            }

            if ($this->user->UserType == 1) {

                $result = $this->db->query("select ID,Status,KhoID from ttp_report_order where ID=$id and UserID=" . $this->user->ID)->row();
            } else {

                $result = $this->db->query("select ID,Status,KhoID from ttp_report_order where ID=$id")->row();
            }

            if ($result) {

                if ($result->Status == 1) {

                    $checkreason = $this->db->query("select * from ttp_report_order_reason_details where OrderID=$id")->row();

                    if ($checkreason) {

                        if ($this->user->UserType == 4) {

                            $this->db->query("update ttp_report_order_reason_details set 3PLReason = $IDReason,Another3PL='$ghichu' where OrderID=$id");
                        } else {

                            $ghichu_temp = is_numeric($IDReason) ? $IDReason : 0;

                            if (isset($_POST['Follow'])) {

                                $follow = (int) $_POST['Follow'];

                                $this->db->query("update ttp_report_order_reason_details set SaleReasonID = $ghichu_temp,AnotherSale='$ghichu',Follow=$follow where OrderID=$id");
                            } else {

                                $this->db->query("update ttp_report_order_reason_details set SaleReasonID = $ghichu_temp,AnotherSale='$ghichu' where OrderID=$id");
                            }
                        }
                    } else {

                        if ($this->user->UserType == 4) {

                            $this->db->query("insert into ttp_report_order_reason_details(OrderID,3PLReason,Source,Another3PL) value($id,$IDReason,1,'$ghichu')");
                        } else {

                            $ghichu_temp = is_numeric($IDReason) ? $IDReason : 0;

                            if (isset($_POST['Follow'])) {

                                $follow = (int) $_POST['Follow'];

                                $this->db->query("insert into ttp_report_order_reason_details(OrderID,SaleReasonID,AnotherSale,Follow) value($id,$ghichu_temp,'$ghichu',$follow)");
                            } else {

                                $this->db->query("insert into ttp_report_order_reason_details(OrderID,SaleReasonID,AnotherSale) value($id,$ghichu_temp,'$ghichu')");
                            }
                        }
                    }

                    echo "ok";

                    return;
                }
            }
        }

        echo "false";
    }

    public function get_shipment_default($id = 0, $warehouse = 0, $amount = 1) {

        if ($id > 0 && $warehouse > 0) {

            $object = $this->db->query("select * from ttp_report_products_bundle where Src_ProductsID=$id")->result();

            if (count($object) > 0) {

                echo "COMBO";

                return;
            } else {

                $products = $this->db->query("select sum(a.Available) as Available from ttp_report_inventory a,ttp_report_shipment b where a.ShipmentID=b.ID and a.ProductsID=$id and a.Available>=0 and date(b.DateExpiration) >= '" . date('Y-m-d') . "' and a.WarehouseID=$warehouse and a.LastEdited=1")->row();

                if ($products) {

                    if ($products->Available == 0) {

                        echo "<i style='display:block;text-align:left;font-style:normal'>## Hết hàng ##</i>";

                        return;
                    } else {

                        if ($products->Available >= $amount) {

                            echo "<i style='display:block;text-align:left;font-size:11px;font-family:tahoma;font-style:normal'>Available " . number_format($products->Available) . "</i>";

                            return;
                        } else {

                            echo "<p style='text-align:left;color:#F00'>Không đủ hàng</p><i style='display:block;text-align:left;font-size:11px;font-family:tahoma;font-style:normal'>Available " . number_format($products->Available) . "</i>";

                            return;
                        }
                    }
                }
            }
        }

        echo "<i style='display:block;text-align:left;font-style:normal'>## Hết hàng ##</i>";
    }

    public function get_shipment_arr($ProductsID = 0, $WarehouseID = 0, $Amount = 1) {

        if ($ProductsID > 0 && $WarehouseID > 0) {

            $products = $this->db->query("select a.ShipmentID,a.Available,a.DateInventory,a.OnHand,c.RootPrice from ttp_report_inventory a,ttp_report_shipment b,ttp_report_products c where a.ProductsID=c.ID and a.ShipmentID=b.ID and a.ProductsID=$ProductsID and a.WarehouseID=$WarehouseID and a.Available>0 and a.LastEdited=1 and b.DateExpiration >= '" . date('Y-m-d') . "' order by b.DateExpiration ASC")->result();

            if (count($products) > 0) {

                $arr = array();

                foreach ($products as $row) {

                    if ($row->Available >= $Amount) {

                        $arr[$row->ShipmentID] = array(
                            'OnHand' => $row->OnHand,
                            'Available' => $row->Available - $Amount,
                            'Amount' => $Amount,
                            'DateInventory' => $row->DateInventory,
                            'Price' => $row->RootPrice * $Amount
                        );

                        return $arr;
                    } else {

                        $arr[$row->ShipmentID] = array(
                            'OnHand' => $row->OnHand,
                            'Available' => 0,
                            'Amount' => $row->Available,
                            'DateInventory' => $row->DateInventory,
                            'Price' => $row->RootPrice * $Amount
                        );

                        $Amount = $Amount - $row->Available;
                    }
                }

                return $arr;
            }
        }

        return array();
    }

    public function change_inventory($order, $details, $updatecols = 'Available') {

        if ($order) {

            if (count($details) > 0) {

                foreach ($details as $row) {

                    $bundle = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$row->ID")->result();

                    if (count($bundle) > 0) {

                        foreach ($bundle as $item) {

                            $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                            if ($check_exists) {

                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1");

                                if ($check_exists->DateInventory == date('Y-m-d')) {

                                    if ($updatecols == 'Available') {

                                        $this->db->query("update ttp_report_inventory set Available=Available+$item->Amount,LastEdited=1 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and DateInventory='$check_exists->DateInventory' and WarehouseID=$order->KhoID");

                                        $data_log = array(
                                            'ProductsID' => $item->ProductsID,
                                            'OnHand' => $check_exists->OnHand,
                                            'Available' => $check_exists->Available + $item->Amount,
                                            'WarehouseID' => $order->KhoID,
                                            'ShipmentID' => $item->ShipmentID,
                                            'DateInventory' => $check_exists->DateInventory,
                                            'Type' => "Available",
                                            'Amount' => $item->Amount,
                                            'Action' => 'change_inventory_update 1',
                                            'Method' => 'change order id ' . $order->ID
                                        );

                                        $this->write_log_inventory($data_log);
                                    } elseif ($updatecols == 'OnHand') {

                                        $OnHand = $check_exists->OnHand - $item->Amount;

                                        $OnHand = $OnHand < 0 ? 0 : $OnHand;

                                        $this->db->query("update ttp_report_inventory set OnHand=$OnHand,LastEdited=1 where ID=$check_exists->ID");

                                        $data_log = array(
                                            'ProductsID' => $item->ProductsID,
                                            'OnHand' => $OnHand,
                                            'Available' => $check_exists->Available,
                                            'WarehouseID' => $order->KhoID,
                                            'ShipmentID' => $item->ShipmentID,
                                            'DateInventory' => $check_exists->DateInventory,
                                            'Type' => "OnHand",
                                            'Amount' => $item->Amount,
                                            'Action' => 'change_inventory_update 2',
                                            'Method' => 'change order id ' . $order->ID
                                        );

                                        $this->write_log_inventory($data_log);
                                    }
                                } else {

                                    if ($updatecols == 'Available') {

                                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$check_exists->OnHand,$check_exists->Available+$item->Amount,$order->KhoID,$item->ShipmentID,'" . date('Y-m-d') . "',1)");

                                        $data_log = array(
                                            'ProductsID' => $item->ProductsID,
                                            'OnHand' => $check_exists->OnHand,
                                            'Available' => $check_exists->Available + $item->Amount,
                                            'WarehouseID' => $order->KhoID,
                                            'ShipmentID' => $item->ShipmentID,
                                            'DateInventory' => date('Y-m-d'),
                                            'Type' => "Available",
                                            'Amount' => $item->Amount,
                                            'Action' => 'change_inventory_insert 3',
                                            'Method' => 'change order id ' . $order->ID
                                        );

                                        $this->write_log_inventory($data_log);
                                    } elseif ($updatecols == 'OnHand') {

                                        $OnHand = $check_exists->OnHand - $item->Amount;

                                        $OnHand = $OnHand < 0 ? 0 : $OnHand;

                                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$OnHand,$check_exists->Available,$order->KhoID,$item->ShipmentID,'" . date('Y-m-d') . "',1)");

                                        $data_log = array(
                                            'ProductsID' => $item->ProductsID,
                                            'OnHand' => $OnHand,
                                            'Available' => $check_exists->Available,
                                            'WarehouseID' => $order->KhoID,
                                            'ShipmentID' => $item->ShipmentID,
                                            'DateInventory' => date('Y-m-d'),
                                            'Type' => "OnHand",
                                            'Amount' => $item->Amount,
                                            'Action' => 'change_inventory_insert 4',
                                            'Method' => 'change order id ' . $order->ID
                                        );

                                        $this->write_log_inventory($data_log);
                                    }
                                }
                            }
                        }
                    } else {

                        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                        if ($check_exists) {

                            if ($check_exists->DateInventory == Date('Y-m-d')) {

                                if ($updatecols == 'Available') {

                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");

                                    $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,LastEdited=1 where ID=$check_exists->ID");

                                    $data_log = array(
                                        'ProductsID' => $row->ProductsID,
                                        'OnHand' => $check_exists->OnHand,
                                        'Available' => $check_exists->Available + $row->Amount,
                                        'WarehouseID' => $order->KhoID,
                                        'ShipmentID' => $row->ShipmentID,
                                        'DateInventory' => date('Y-m-d'),
                                        'Type' => "Available",
                                        'Amount' => $row->Amount,
                                        'Action' => 'change_inventory_update 5',
                                        'Method' => 'change order id ' . $order->ID
                                    );

                                    $this->write_log_inventory($data_log);
                                } elseif ($updatecols == 'OnHand') {

                                    $OnHand = $check_exists->OnHand - $row->Amount;

                                    $OnHand = $OnHand < 0 ? 0 : $OnHand;

                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");

                                    $this->db->query("update ttp_report_inventory set OnHand=$OnHand,LastEdited=1 where ID=$check_exists->ID");

                                    $data_log = array(
                                        'ProductsID' => $row->ProductsID,
                                        'OnHand' => $OnHand,
                                        'Available' => $check_exists->Available,
                                        'WarehouseID' => $order->KhoID,
                                        'ShipmentID' => $row->ShipmentID,
                                        'DateInventory' => date('Y-m-d'),
                                        'Type' => "OnHand",
                                        'Amount' => $row->Amount,
                                        'Action' => 'change_inventory_update 6',
                                        'Method' => 'change order id ' . $order->ID
                                    );

                                    $this->write_log_inventory($data_log);
                                }
                            } else {

                                $Available = $check_exists->Available;

                                $OnHand = $check_exists->OnHand;

                                if ($updatecols == 'Available') {

                                    $Available = $Available + $row->Amount;

                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");

                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'" . date('Y-m-d') . "',1)");

                                    $data_log = array(
                                        'ProductsID' => $row->ProductsID,
                                        'OnHand' => $OnHand,
                                        'Available' => $Available,
                                        'WarehouseID' => $order->KhoID,
                                        'ShipmentID' => $row->ShipmentID,
                                        'DateInventory' => date('Y-m-d'),
                                        'Type' => "Available",
                                        'Amount' => $row->Amount,
                                        'Action' => 'change_inventory_insert 7',
                                        'Method' => 'change order id ' . $order->ID
                                    );

                                    $this->write_log_inventory($data_log);
                                } elseif ($updatecols == 'OnHand') {

                                    $OnHand = $check_exists->OnHand - $row->Amount;

                                    $OnHand = $OnHand < 0 ? 0 : $OnHand;

                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");

                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'" . date('Y-m-d') . "',1)");

                                    $data_log = array(
                                        'ProductsID' => $row->ProductsID,
                                        'OnHand' => $OnHand,
                                        'Available' => $Available,
                                        'WarehouseID' => $order->KhoID,
                                        'ShipmentID' => $row->ShipmentID,
                                        'DateInventory' => date('Y-m-d'),
                                        'Type' => "OnHand",
                                        'Amount' => $row->Amount,
                                        'Action' => 'change_inventory_insert 8',
                                        'Method' => 'change order id ' . $order->ID
                                    );

                                    $this->write_log_inventory($data_log);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function get_another_shipment() {

        $WarehouseID = isset($_POST['WarehouseID']) ? (int) $_POST['WarehouseID'] : 0;

        $ShipmentID = isset($_POST['ShipmentID']) ? (int) $_POST['ShipmentID'] : 0;

        $ProductsID = isset($_POST['ProductsID']) ? (int) $_POST['ProductsID'] : 0;

        $Amount = isset($_POST['Amount']) ? (int) $_POST['Amount'] : 0;

        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;

        $check = $this->db->query("select a.ShipmentID,b.ShipmentCode,a.Available from ttp_report_inventory a,ttp_report_shipment b where a.ShipmentID=b.ID and a.WarehouseID=$WarehouseID and a.ShipmentID!=$ShipmentID and a.ProductsID=$ProductsID and a.LastEdited=1 and a.Available>0 order by a.DateInventory ASC")->result();

        if ($check) {

            if (count($check) > 0) {

                foreach ($check as $row) {

                    echo "<li><a title='Thay đổi lô cũ thành lô $row->ShipmentCode' onclick='replace_shipment($ID,$row->ShipmentID)'>$row->ShipmentCode <i>($row->Available)</i></a></li>";
                }
            }
        } else {

            echo "<li><small>Không có lựa chọn khác</small></li>";
        }
    }

    public function replace_shipment() {

        $ShipmentID = isset($_POST['ShipmentID']) ? (int) $_POST['ShipmentID'] : 0;

        $ID = isset($_POST['ID']) ? (int) $_POST['ID'] : 0;

        if ($ID > 0 && $ShipmentID > 0) {

            $details = $this->db->query("select a.*,b.KhoID,b.MaDH,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_order b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=b.ID and a.ID=$ID")->row();

            if ($details) {

                $data = $this->db->query("select a.ID,a.Available,a.OnHand,a.DateInventory,b.ShipmentCode from ttp_report_inventory a,ttp_report_shipment b where a.ShipmentID=b.ID and a.ProductsID=$details->ProductsID and a.ShipmentID=$ShipmentID and a.WarehouseID=$details->KhoID and a.LastEdited=1 order by a.DateInventory DESC limit 0,1")->row();

                if ($data) {

                    if ($data->Available >= $details->Amount) {

                        /* Reput Available */

                        $check_exists = $this->db->query("select ID,Available,OnHand,DateInventory from ttp_report_inventory where ProductsID=$details->ProductsID and ShipmentID=$details->ShipmentID and WarehouseID=$details->KhoID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();

                        if ($check_exists) {

                            if ($check_exists->DateInventory == Date('Y-m-d')) {

                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");

                                $this->db->query("update ttp_report_inventory set Available=Available+$details->Amount,LastEdited=1 where ID=$check_exists->ID");

                                $data_log = array(
                                    'ProductsID' => $details->ProductsID,
                                    'OnHand' => $check_exists->OnHand,
                                    'Available' => $check_exists->Available + $details->Amount,
                                    'WarehouseID' => $details->KhoID,
                                    'ShipmentID' => $details->ShipmentID,
                                    'DateInventory' => date('Y-m-d'),
                                    'Type' => "Available",
                                    'Amount' => $details->Amount,
                                    'Action' => 'update 1',
                                    'Method' => 'replace shipment details id ' . $ID
                                );

                                $this->write_log_inventory($data_log);
                            } else {

                                $Available = $check_exists->Available + $details->Amount;

                                $OnHand = $check_exists->OnHand;

                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$details->ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");

                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($details->ProductsID,$OnHand,$Available,$details->KhoID,$details->ShipmentID,'" . date('Y-m-d') . "',1)");

                                $data_log = array(
                                    'ProductsID' => $details->ProductsID,
                                    'OnHand' => $OnHand,
                                    'Available' => $Available,
                                    'WarehouseID' => $details->KhoID,
                                    'ShipmentID' => $details->ShipmentID,
                                    'DateInventory' => date('Y-m-d'),
                                    'Type' => "Available",
                                    'Amount' => $details->Amount,
                                    'Action' => 'insert 2',
                                    'Method' => 'replace shipment details id ' . $ID
                                );

                                $this->write_log_inventory($data_log);
                            }
                        } else {

                            echo "False";

                            return;
                        }



                        /* Replace Shipment */

                        if ($data->DateInventory == Date('Y-m-d')) {

                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");

                            $this->db->query("update ttp_report_inventory set Available=Available-$details->Amount,LastEdited=1 where ID=$data->ID");

                            $data_log = array(
                                'ProductsID' => $details->ProductsID,
                                'OnHand' => $data->OnHand,
                                'Available' => $data->Available - $details->Amount,
                                'WarehouseID' => $details->KhoID,
                                'ShipmentID' => $ShipmentID,
                                'DateInventory' => date('Y-m-d'),
                                'Type' => "Available",
                                'Amount' => $details->Amount,
                                'Action' => 'update 3',
                                'Method' => 'replace shipment details id ' . $ID
                            );

                            $this->write_log_inventory($data_log);
                        } else {

                            $Available = $data->Available - $details->Amount;

                            $OnHand = $data->OnHand;

                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$details->ProductsID and WarehouseID=$details->KhoID and LastEdited=1");

                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($details->ProductsID,$OnHand,$Available,$details->KhoID,$ShipmentID,'" . date('Y-m-d') . "',1)");

                            $data_log = array(
                                'ProductsID' => $details->ProductsID,
                                'OnHand' => $OnHand,
                                'Available' => $Available,
                                'WarehouseID' => $details->KhoID,
                                'ShipmentID' => $ShipmentID,
                                'DateInventory' => date('Y-m-d'),
                                'Type' => "Available",
                                'Amount' => $details->Amount,
                                'Action' => 'insert 4',
                                'Method' => 'replace shipment details id ' . $ID
                            );

                            $this->write_log_inventory($data_log);
                        }

                        $this->db->query("update ttp_report_orderdetails set ShipmentID=$ShipmentID where ID=$ID");

                        echo "OK";

                        $messagelog = date('H:i:s', time()) . " => User '{$this->user->UserName}' change shipment $details->ShipmentID($details->ShipmentCode) -> $ShipmentID($data->ShipmentCode) Order($details->MaDH) \n";

                        file_put_contents("log/report/" . date('d-m-Y', time()) . ".txt", $messagelog . "\n", FILE_APPEND);

                        return;
                    } else {

                        echo "False";

                        return;
                    }
                }
            }
        }

        echo "False";
    }

    public function change_warehouse() {

        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0;

        $WarehouseID = isset($_POST['WarehouseID']) ? $_POST['WarehouseID'] : 3;

        $order = $this->db->query("select * from ttp_report_order where ID=$ID")->row();

        if ($order) {

            if ($order->KhoID != $WarehouseID && $WarehouseID > 0) {

                $details = $this->db->query("select a.*,b.MaSP as Title from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$ID")->result();

                if (count($details) > 0) {

                    $arr_sum_amount = array();

                    foreach ($details as $row) {

                        $amounttemp = $row->Amount;

                        $productstype = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$row->ID")->result();

                        if (count($productstype) > 0) {

                            foreach ($productstype as $item) {

                                $temp = $item->Amount;

                                if (isset($arr_sum_amount[$item->ProductsID])) {

                                    $arr_sum_amount[$item->ProductsID] += $temp;
                                } else {

                                    $arr_sum_amount[$item->ProductsID] = $temp;
                                }

                                $final = $arr_sum_amount[$item->ProductsID];

                                $check = $this->db->query("select sum(Available) as Available from ttp_report_inventory where ProductsID=$item->ProductsID and WarehouseID=$WarehouseID and LastEdited=1")->row();

                                if ($check) {

                                    if ($check->Available < $final) {

                                        echo "Cảnh báo !!! : Sản phẩm $row->Title không đủ số lượng để chuyển kho . \nMọi thay đổi sẽ được được thực hiện.";

                                        return false;
                                    }
                                } else {

                                    echo "Cảnh báo !!! : Sản phẩm $row->Title không đủ số lượng để chuyển kho . \nMọi thay đổi sẽ được được thực hiện.";

                                    return false;
                                }
                            }
                        } else {

                            if (isset($arr_sum_amount[$row->ProductsID])) {

                                $arr_sum_amount[$row->ProductsID] += $amounttemp;
                            } else {

                                $arr_sum_amount[$row->ProductsID] = $amounttemp;
                            }

                            $final = $arr_sum_amount[$row->ProductsID];

                            $check = $this->db->query("select sum(a.Available) as Available from ttp_report_inventory a,ttp_report_shipment b where a.ShipmentID=b.ID and a.ProductsID=$row->ProductsID and a.WarehouseID=$WarehouseID and a.LastEdited=1 and DateExpiration>=" . date('Y-m-d'))->row();

                            if ($check) {

                                if ($check->Available < $final) {

                                    echo "Cảnh báo !!! : Sản phẩm $row->Title không đủ số lượng để chuyển kho . \nMọi thay đổi sẽ được được thực hiện.";

                                    return;
                                }
                            } else {

                                echo "Cảnh báo !!! : Sản phẩm $row->Title không đủ số lượng để chuyển kho . \nMọi thay đổi sẽ được được thực hiện.";

                                return;
                            }
                        }

                        $row->Bundle = $productstype;

                        $detailstemp[] = $row;
                    }

                    $details = $detailstemp;

                    foreach ($details as $row) {

                        if (count($row->Bundle) > 0) {

                            foreach ($row->Bundle as $item) {

                                $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$item->ShipmentID and ProductsID=$item->ProductsID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                                if ($check_exists) {

                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$item->ShipmentID and ProductsID=$item->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");

                                    if ($check_exists->DateInventory == date('Y-m-d')) {

                                        $this->db->query("update ttp_report_inventory set Available=Available+$item->Amount,LastEdited=1 where ID=$check_exists->ID");

                                        $data_log = array(
                                            'ProductsID' => $item->ProductsID,
                                            'OnHand' => $check_exists->OnHand,
                                            'Available' => $check_exists->Available + $item->Amount,
                                            'WarehouseID' => $order->KhoID,
                                            'ShipmentID' => $item->ShipmentID,
                                            'DateInventory' => date('Y-m-d'),
                                            'Type' => "Available",
                                            'Amount' => $item->Amount,
                                            'Action' => 'update 1',
                                            'Method' => 'change warehouse order id ' . $ID
                                        );

                                        $this->write_log_inventory($data_log);
                                    } else {

                                        $Available = $check_exists->Available;

                                        $OnHand = $check_exists->OnHand;

                                        $Available = $Available + $item->Amount;

                                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$OnHand,$Available,$order->KhoID,$item->ShipmentID,'" . date('Y-m-d') . "',1)");

                                        $data_log = array(
                                            'ProductsID' => $item->ProductsID,
                                            'OnHand' => $OnHand,
                                            'Available' => $Available,
                                            'WarehouseID' => $order->KhoID,
                                            'ShipmentID' => $item->ShipmentID,
                                            'DateInventory' => date('Y-m-d'),
                                            'Type' => "Available",
                                            'Amount' => $item->Amount,
                                            'Action' => 'insert 2',
                                            'Method' => 'change warehouse order id ' . $ID
                                        );

                                        $this->write_log_inventory($data_log);
                                    }
                                }
                            }
                        } else {

                            $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();

                            if ($check_exists) {

                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");

                                if ($check_exists->DateInventory == date('Y-m-d')) {

                                    $this->db->query("update ttp_report_inventory set Available=Available+$row->Amount,LastEdited=1 where ID=$check_exists->ID");

                                    $data_log = array(
                                        'ProductsID' => $row->ProductsID,
                                        'OnHand' => $check_exists->OnHand,
                                        'Available' => $check_exists->Available + $row->Amount,
                                        'WarehouseID' => $order->KhoID,
                                        'ShipmentID' => $row->ShipmentID,
                                        'DateInventory' => date('Y-m-d'),
                                        'Type' => "Available",
                                        'Amount' => $row->Amount,
                                        'Action' => 'update 3',
                                        'Method' => 'change warehouse order id ' . $ID
                                    );

                                    $this->write_log_inventory($data_log);
                                } else {

                                    $Available = $check_exists->Available;

                                    $OnHand = $check_exists->OnHand;

                                    $Available = $Available + $row->Amount;

                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'" . date('Y-m-d') . "',1)");

                                    $data_log = array(
                                        'ProductsID' => $row->ProductsID,
                                        'OnHand' => $check_exists->OnHand,
                                        'Available' => $check_exists->Available + $row->Amount,
                                        'WarehouseID' => $order->KhoID,
                                        'ShipmentID' => $row->ShipmentID,
                                        'DateInventory' => date('Y-m-d'),
                                        'Type' => "Available",
                                        'Amount' => $row->Amount,
                                        'Action' => 'insert 4',
                                        'Method' => 'change warehouse order id ' . $ID
                                    );

                                    $this->write_log_inventory($data_log);
                                }
                            }
                        }
                    }



                    foreach ($details as $row) {

                        $soluong = $row->Amount;

                        if (count($row->Bundle) > 0) {

                            $this->db->query("delete from ttp_report_orderdetails_bundle where OrderID=$ID");

                            foreach ($row->Bundle as $item) {

                                $bundlelist = $item->Amount;

                                $Shipment = $this->get_shipment_arr($item->ProductsID, $WarehouseID, $bundlelist);

                                if (is_array($Shipment) && count($Shipment) > 0) {

                                    foreach ($Shipment as $keyitem => $valueitem) {

                                        if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available'])) {

                                            $data_list = array('OrderID' => $order->ID, 'DetailsID' => $row->ID, 'ProductsID' => $item->ProductsID, 'Amount' => $valueitem['Amount'], 'ShipmentID' => $keyitem);

                                            $this->db->insert('ttp_report_orderdetails_bundle', $data_list);

                                            if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$keyitem and WarehouseID=$WarehouseID and LastEdited=1");

                                                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$WarehouseID,$keyitem,'" . date('Y-m-d') . "',1)");

                                                $data_log = array(
                                                    'ProductsID' => $item->ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $WarehouseID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'insert 5',
                                                    'Method' => 'change warehouse order id ' . $ID
                                                );

                                                $this->write_log_inventory($data_log);
                                            } else {

                                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$keyitem and WarehouseID=$WarehouseID and LastEdited=1");

                                                $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$item->ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$WarehouseID");

                                                $data_log = array(
                                                    'ProductsID' => $item->ProductsID,
                                                    'OnHand' => $valueitem['OnHand'],
                                                    'Available' => $valueitem['Available'],
                                                    'WarehouseID' => $WarehouseID,
                                                    'ShipmentID' => $keyitem,
                                                    'DateInventory' => date('Y-m-d'),
                                                    'Type' => "Available",
                                                    'Amount' => $valueitem['Amount'],
                                                    'Action' => 'update 6',
                                                    'Method' => 'change warehouse order id ' . $ID
                                                );

                                                $this->write_log_inventory($data_log);
                                            }
                                        }
                                    }
                                }
                            }
                        } else {

                            $Shipment = $this->get_shipment_arr($row->ProductsID, $WarehouseID, $soluong);

                            if (is_array($Shipment) && count($Shipment) > 0) {

                                $i = 1;

                                foreach ($Shipment as $keyitem => $valueitem) {

                                    if (isset($valueitem['Amount']) && isset($valueitem['DateInventory']) && isset($valueitem['Available'])) {

                                        if ($i == 1) {

                                            $this->db->query("update ttp_report_orderdetails set ShipmentID=$keyitem where ID=$row->ID");
                                        } else {

                                            $data_list = array('OrderID' => $order->ID, 'ProductsID' => $row->ID, 'Price' => $row->Price, 'PriceDown' => $row->PriceDown, 'Total' => $Price * $valueitem['Amount'], 'ShipmentID' => $keyitem);

                                            $this->db->insert('ttp_report_orderdetails', $data_list);
                                        }

                                        $i++;

                                        if ($valueitem['DateInventory'] != date('Y-m-d')) {

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$keyitem and WarehouseID=$WarehouseID and LastEdited=1");

                                            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID," . $valueitem['OnHand'] . "," . $valueitem['Available'] . ",$WarehouseID,$keyitem,'" . date('Y-m-d') . "',1)");

                                            $data_log = array(
                                                'ProductsID' => $row->ProductsID,
                                                'OnHand' => $valueitem['OnHand'],
                                                'Available' => $valueitem['Available'],
                                                'WarehouseID' => $WarehouseID,
                                                'ShipmentID' => $keyitem,
                                                'DateInventory' => date('Y-m-d'),
                                                'Type' => "Available",
                                                'Amount' => $valueitem['Amount'],
                                                'Action' => 'insert 7',
                                                'Method' => 'change warehouse order id ' . $ID
                                            );

                                            $this->write_log_inventory($data_log);
                                        } else {

                                            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$row->ProductsID and ShipmentID=$keyitem and WarehouseID=$WarehouseID and LastEdited=1");

                                            $this->db->query("update ttp_report_inventory set Available=Available-" . $valueitem['Amount'] . ",LastEdited=1 where ProductsID=$row->ProductsID and ShipmentID=$keyitem and DateInventory='" . $valueitem['DateInventory'] . "' and WarehouseID=$WarehouseID");

                                            $data_log = array(
                                                'ProductsID' => $row->ProductsID,
                                                'OnHand' => $valueitem['OnHand'],
                                                'Available' => $valueitem['Available'],
                                                'WarehouseID' => $WarehouseID,
                                                'ShipmentID' => $keyitem,
                                                'DateInventory' => date('Y-m-d'),
                                                'Type' => "Available",
                                                'Amount' => $valueitem['Amount'],
                                                'Action' => 'update 8',
                                                'Method' => 'change warehouse order id ' . $ID
                                            );

                                            $this->write_log_inventory($data_log);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $this->db->query("update ttp_report_order set KhoID=$WarehouseID where ID=$order->ID");

                    echo "OK";

                    return;
                }
            }
        }

        echo "FALSE";
    }

    public function get_combo_details() {

        $ID = isset($_POST['DetailsID']) ? (int) $_POST['DetailsID'] : 0;

        $details = $this->db->query("select b.*,c.Title,c.MaSP,d.ShipmentCode from ttp_report_orderdetails a, ttp_report_orderdetails_bundle b,ttp_report_products c,ttp_report_shipment d where d.ID=b.ShipmentID and b.ProductsID=c.ID and a.ID=b.DetailsID and a.ID=$ID")->result();

        if (count($details) > 0) {

            echo "<table><tr><th>STT</th><th>Mã SKU</th><th>Tên sản phẩm</th><th>Lô hàng</th><th>Số lượng</th></tr>";

            $i = 1;

            foreach ($details as $row) {

                echo "<tr>";

                echo "<td class='width50'>$i</td>";

                echo "<td class='width150'>$row->MaSP</td>";

                echo "<td>$row->Title</td>";

                echo "<td>$row->ShipmentCode</td>";

                echo "<td>$row->Amount</td>";

                echo "</tr>";

                $i++;
            }

            echo "</table>";
        }
    }

    public function get_order_by_customer() {

        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '';

        echo "<div class='row' style='margin-bottom: 15px;'><div class='col-xs-12'><input type='text' class='form-control' value='$keywords' placeholder='Điền tên , số điện thoại hoặc địa chỉ giao hàng ...' onchange='searchcustomer(this)' /></div></div>";

        if ($keywords != '') {

            $result = $this->db->query("select a.MaDH,a.AddressOrder,b.Name,b.Phone1,b.Phone2,c.UserName from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID=b.ID and (b.Name like '%$keywords%' or b.Phone1 like '%$keywords%' or a.AddressOrder like '%$keywords%') limit 0,200")->result();

            if (count($result) > 0) {

                echo "<table><tr><th>STT</th><th>Mã ĐH</th><th>Tên khách hàng</th><th>Số ĐT</th><th>Địa chỉ giao hàng</th><th>Người khởi tạo</th></tr>";

                $i = 1;

                foreach ($result as $row) {

                    echo "<tr>";

                    echo "<td class='width50'>$i</td>";

                    echo "<td class='width150'>$row->MaDH</td>";

                    echo "<td>$row->Name</td>";

                    echo "<td>$row->Phone1</td>";

                    echo "<td>$row->AddressOrder</td>";

                    echo "<td>$row->UserName</td>";

                    echo "</tr>";

                    $i++;
                }

                echo "</table>";
            } else {

                echo "Không tìm thấy dữ liệu theo yêu cầu . Vui lòng chọn từ khóa khác ...";
            }
        } else {

            echo "Vui lòng điền từ khóa vào khung tìm kiếm khách hàng .";
        }
    }

    public function export_order_template($id = 0) {

        $order = $this->db->query("select a.Chietkhau,a.Ngaydathang,b.Code,b.Name,b.Company,b.Address,b.Phone1,b.Taxcode,b.Email,a.AddressOrder,b.AreaID,b.AreaNote from ttp_report_order a,ttp_report_customer b where a.CustomerID=b.ID and a.ID=$id")->row();

        if ($order) {

            $area = $this->db->query("select Title from ttp_report_area where ID=$order->AreaID")->row();

            $area = $area ? $area->Title : "";

            error_reporting(E_ALL);

            ini_set('display_errors', TRUE);

            ini_set('display_startup_errors', TRUE);

            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');

            require_once 'public/plugin/PHPExcel.php';

            $arr = array();

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                    ->setLastModifiedBy("Maarten Balliauw")
                    ->setTitle("Office 2007 XLSX Test Document")
                    ->setSubject("Office 2007 XLSX Test Document")
                    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("Test result file");

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A9', 'ĐƠN ĐẶT HÀNG')
                    ->setCellValue('F1', 'ID: TTP-FI-F-011-Sales Order')
                    ->setCellValue('F2', date('m/d/Y H:i'))
                    ->setCellValue('B5', 'Số Tài khoản')
                    ->setCellValue('B6', '1020 1000 1849 927')
                    ->setCellValue('B7', "'" . '5500201016038')
                    ->setCellValue('B11', 'Ngày đặt hàng')
                    ->setCellValue('C11', date('d/m/Y', strtotime($order->Ngaydathang)))
                    ->setCellValue('B12', 'Mã đối tác')
                    ->setCellValue('C12', $order->Code)
                    ->setCellValue('B13', 'Tên đối tác')
                    ->setCellValue('C13', $order->Company == '' ? $order->Name : $order->Company )
                    ->setCellValue('B14', 'Địa chỉ')
                    ->setCellValue('C14', $order->Address)
                    ->setCellValue('B15', 'Điện thoại')
                    ->setCellValue('C15', $order->Phone1)
                    ->setCellValue('B16', 'Mã số thuế')
                    ->setCellValue('C16', "'" . $order->Taxcode)
                    ->setCellValue('B17', 'Địa chỉ giao hàng')
                    ->setCellValue('C17', $order->AddressOrder)
                    ->setCellValue('E11', 'Ngày đề nghị giao hàng')
                    ->setCellValue('E12', 'Khu vực')
                    ->setCellValue('D15', 'Số fax : ')
                    ->setCellValue('E15', 'Email:')
                    ->setCellValue('F15', $order->Email)
                    ->setCellValue('F15', $order->AreaNote != '' ? $order->AreaNote : $area )
                    ->setCellValue('C5', 'Ngân Hàng')
                    ->setCellValue('C6', 'Ngân Hàng VIETINBANK - CN TP.HCM')
                    ->setCellValue('C7', 'Ngân Hàng Nông Nghiệp & Phát Triển Nông Thôn tỉnh Bình Dương')
                    ->setCellValue('A20', 'STT')
                    ->setCellValue('B20', 'Mã sản phẩm')
                    ->setCellValue('C20', 'Tên sản phẩm')
                    ->setCellValue('D20', 'Quy cách')
                    ->setCellValue('E20', 'Đơn giá')
                    ->setCellValue('F20', 'Số lượng')
                    ->setCellValue('G20', 'Thành tiền')
                    ->setCellValue('H20', 'Ghi chú')
                    ->setCellValue('A21', 'PHẦN ĐẶT HÀNG CỦA ĐỐI TÁC');

            $objPHPExcel->getActiveSheet()->getStyle("B11:G17")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => "cccccc"), 'borders' => array('bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

            $objPHPExcel->getActiveSheet()->getStyle("A20:H20")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => "deffdc"), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

            $details = $this->db->query("select a.*,b.MaSP,b.Title,b.Donvi from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$id")->result();

            $total = 0;

            if (count($details) > 0) {

                $i = 22;

                $j = 1;

                foreach ($details as $row) {

                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $i, $j)
                            ->setCellValue('B' . $i, $row->MaSP)
                            ->setCellValue('C' . $i, $row->Title)
                            ->setCellValue('D' . $i, $row->Donvi)
                            ->setCellValue('E' . $i, $row->Price)
                            ->setCellValue('F' . $i, $row->Amount)
                            ->setCellValue('G' . $i, $row->Total)
                            ->setCellValue('H' . $i, '');

                    $i++;

                    $j++;

                    $total = $total + $row->Total;
                }
            }

            if ($order->Chietkhau > 0) {

                $total = $total - $order->Chietkhau;

                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $j)
                        ->setCellValue('B' . $i, "")
                        ->setCellValue('C' . $i, "Chiết khấu trực tiếp trên đơn hàng")
                        ->setCellValue('G' . $i, $order->Chietkhau)
                        ->setCellValue('H' . $i, '');

                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C' . $i . ':F' . $i);

                $i++;
            }

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, "Tổng cộng : ")
                    ->setCellValue('F' . $i, "")
                    ->setCellValue('G' . $i, $total)
                    ->setCellValue('H' . $i, "");

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':E' . $i);

            $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => "deffdc"), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

            $reduce = $this->db->query("select a.Title,b.* from ttp_report_reduce a,ttp_report_reduce_order b where a.ID=b.ReduceID and b.OrderID=$id")->result();

            if (count($reduce) > 0) {

                $i++;

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, "PHẦN CẤN TRỪ CÁC KHOẢN HỖ TRỢ: (Điều phối kinh doanh khu vực ghi)");

                $i++;

                $j = 1;

                $reduce_value = 0;

                foreach ($reduce as $row) {

                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $i, $j)
                            ->setCellValue('B' . $i, "")
                            ->setCellValue('C' . $i, $row->Title . ' ' . $row->TimeReduce)
                            ->setCellValue('D' . $i, "")
                            ->setCellValue('E' . $i, "")
                            ->setCellValue('F' . $i, "")
                            ->setCellValue('G' . $i, $row->ValueReduce)
                            ->setCellValue('H' . $i, '');

                    $i++;

                    $j++;

                    $total = $total - $row->ValueReduce;

                    $reduce_value = $reduce_value + $row->ValueReduce;
                }

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, "Tổng cộng : ")
                        ->setCellValue('F' . $i, "")
                        ->setCellValue('G' . $i, $reduce_value)
                        ->setCellValue('H' . $i, "");

                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':E' . $i);

                $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => "deffdc"), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));

                $i++;
            }

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, "Tổng tiền thanh toán: ")
                    ->setCellValue('F' . $i, "")
                    ->setCellValue('G' . $i, $total)
                    ->setCellValue('H' . $i, "");

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':E' . $i);

            $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => "a5d6a2"), 'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN))));



            $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

            $objPHPExcel->getActiveSheet()->getStyle('B5:E7')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->getStyle('A20:H20')->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->getStyle('A22:H' . $i)->applyFromArray($styleArray);

            $objPHPExcel->getActiveSheet()->setTitle('ORDER');

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:E5');

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C6:E6');

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C7:E7');

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:h9');



            header('Content-Type: application/vnd.ms-excel');

            header('Content-Disposition: attachment;filename="' . date('d-m-Y') . '-' . $id . '-ORDER.xls"');

            header('Cache-Control: max-age=0');

            header('Cache-Control: max-age=1');

            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');

            header('Cache-Control: cache, must-revalidate');

            header('Pragma: public');



            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            ob_end_clean();

            $objWriter->save('php://output');

            exit;
        } else {

            echo "This order is not exists !";
        }
    }

    public function assign($OrderID = 0, $UserAssignID = 0) {

        $str = "";

        $str1 = "";

        if (is_numeric($OrderID) && is_numeric($UserAssignID) && $this->user->UserType == 5) {

            $order = $this->db->query("select UserID,Status from ttp_report_order where ID=$OrderID")->row();

            if ($order) {

                if ($order->Status == 2 && $order->UserID == 2) {

                    $user = $this->db->query("select ID,UserName from ttp_user where ID=$UserAssignID and UserType=1 and Channel=0")->row();

                    if ($user) {

                        $this->db->query("update ttp_report_order set UserID=$UserAssignID where ID=$OrderID and Status=2 and UserID=2");

                        $data = array(
                            'OrderID' => $OrderID,
                            'DestinationUserID' => $UserAssignID,
                            'UserID' => $this->user->ID,
                            'Created' => date('Y-m-d H:i:s')
                        );

                        $this->db->insert("ttp_report_history_assign", $data);

                        $str = "Đơn hàng đã được chuyển cho tài khoản $user->UserName thành công !";
                    } else {

                        $str1 = "Tài khoản đích để chuyển đơn hàng không tồn tại . Vui lòng kiểm tra lại !";
                    }
                } else {

                    $str1 = 'Không thể chuyển đơn hàng ở trạng thái hiện tại !';
                }
            } else {

                $str1 = "Đơn hàng không tồn tại . Vui lòng kiểm tra lại !";
            }
        } else {

            $str1 = "Dữ liệu không hợp lệ . Vui lòng kiểm tra lại !";
        }

        echo $str != '' ? "<div class='alert alert-success'><i class='fa fa-check-circle' aria-hidden='true' style='margin-right:10px'></i> $str</div>" : "<div class='alert alert-danger'><i class='fa fa-exclamation' aria-hidden='true' style='margin-right:10px'></i> $str1</div>";
    }

    //-->Lee

    public function get_transport_info() {

        $orderid = isset($_POST['order_id']) ? $_POST['order_id'] : 0;

        $tranid = isset($_POST['transport']) ? $_POST['transport'] : 0;

        $company = $this->db->query("select * from ttp_report_transport where ID=$tranid")->row();



        $info = $this->db->query("select * from ttp_transport where OrderID=$orderid")->row();



        $crr = ($info) ? $info->CurrentStatus : '';

        $CurrentStatus = $this->getStatus($crr);

        $CurrentPayment = ($info) ? $info->Payment : '';

        $CoDAmount = ($info) ? $info->CoDAmount : 0;

        $this->load->model('define_model', 'cfg');

        $status = $this->cfg->get_order_status('payment', 'order');

        foreach ($status as $item) {

            if ($item->code == $CurrentPayment) {

                $CurrentPayment = $item->name;
            }
        }



        if ($info && $company) {

            $data = array(
                "Company" => $company->Title,
                "OrderCode" => $info->OrderCode,
                "ClientOrderCode" => $info->ClientOrderCode,
                "TotalFee" => number_format($info->TotalFee),
                "Payment" => $CurrentPayment,
                "CoDAmount" => ($CoDAmount) ? number_format($CoDAmount) : 0,
                "Crr" => $crr,
                "CurrentStatus" => $CurrentStatus,
                "Created" => $info->Created,
                "Updated" => $info->Updated,
                "Sync" => $info->Sync,
                "SyncBy" => $info->SyncBy,
                "OrderID" => $info->OrderID
            );



            $view = 'admin/get_transport_info';

            $this->load->view($view, $data);
        } else {

            echo "Rất tiếc, Đơn hàng này không tồn tại vận đơn hoặc chưa kết nối vận chuyển.";
        }
    }

    //Test GetInfo

    public function getOrderInfo() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        //157327208952

        $OrderCode = isset($_GET['OrderCode']) ? $_GET['OrderCode'] : '';

        //$OrderCode = '157327208952';

        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "OrderCode" => $OrderCode
        );

        $responseGetOrderInfo = $this->_ghn->GetOrderInfo($shippingOrderRequest);

        echo "<pre>";

        var_dump($responseGetOrderInfo);
        exit();

        echo $this->getStatus($responseGetOrderInfo["CurrentStatus"]);
    }

    //Test Update

    public function updateTransport() {

        $dataUpdate = array();

        $TransID = 26;

        $dataUpdate["CurrentStatus"] = "ReadyToPick";

        $dataUpdate["Sync"] = date("Y-m-d H:i:s");

        $dataUpdate["SyncBy"] = $this->user->ID;

        $this->db->where("ID", $TransID);

        $res = $this->db->update("ttp_transport", $dataUpdate);

        var_dump($res);
        exit();
    }

    public function updateCurrentStatus() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $result = array();

        $OrderCode = isset($_GET['OrderCode']) ? $_GET['OrderCode'] : '';

        $OrderID = isset($_GET['OrderID']) ? $_GET['OrderID'] : '';



        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "OrderCode" => $OrderCode
        );

        $responseGetOrderInfo = $this->_ghn->GetOrderInfo($shippingOrderRequest);

        //Kiểm tra kết nối tới GHN

        $login = $this->_sessionToken;

        $isLogin = "";

        if (!empty($login)) {

            //Kiểm tra trạng thái

            $TransInfo = $this->db->query("select * from ttp_transport where OrderID=$OrderID and OrderCode=$OrderCode")->row();

            $crr = ($TransInfo) ? $TransInfo->CurrentStatus : '';

            if ($responseGetOrderInfo["CurrentStatus"] == NULL) {

                $this->updateCurrentStatus();
            } else {

                $newStatus = $responseGetOrderInfo["CurrentStatus"];

                $dataUpdate = array();

                $dataUpdate["CurrentStatus"] = $newStatus;

                $dataUpdate["Note"] = $responseGetOrderInfo["Note"];

                $dataUpdate["Sync"] = date("Y-m-d H:i:s");

                $dataUpdate["SyncBy"] = $this->user->ID;

                $this->db->where("OrderID", $OrderID);

                $this->db->where("OrderCode", $OrderCode);

                $res = $this->db->update("ttp_transport", $dataUpdate);

                if ($res == true) {

                    $result["updated"] = "True";

                    //Hiển thị trạng thái hủy

                    if ($newStatus == "Cancel") {

                        $result["Note"] = $responseGetOrderInfo["Note"];

                        $result["Cancel"] = "True";
                    }

                    //Hiển thị trạng thái mới

                    $result["CurentStatus"] = $this->getStatus($newStatus);

                    $result["Sync"] = date("Y-m-d H:i:s");
                } else {

                    $result["updated"] = "false";
                }

                echo json_encode($result);



                //$newStatus = "Delivered";
                //Hoàn thành đơn hàng

                if ($newStatus == "Delivered") {

                    $orderUpdate = array();

                    $orderUpdate["Status"] = 0;

                    $orderUpdate["HistoryEdited"] = date("Y-m-d H:i:s");

                    $this->db->where("ID", $OrderID);

                    $res = $this->db->update("ttp_report_order", $orderUpdate);



                    //Update TTP_Report_Orderhistory

                    $datahis = array(
                        'OrderID' => $OrderID,
                        'Thoigian' => date('Y-m-d H:i:s', time()),
                        'Status' => 0,
                        "Ghichu" => "Update khi cập nhật trạng thái Delivered",
                        "UserID" => $this->user->ID
                    );

                    $validateHis = $this->db->query("select ID from ttp_report_orderhistory where OrderID=$OrderID and (Status=0 or Status=1)")->row();

                    if (!$validateHis) {

                        $this->db->insert('ttp_report_orderhistory', $datahis);
                    }
                }
            }
        } else {

            $result["updated"] = "NotConnect";

            echo json_encode($result);
        }
    }

    public function get_transport_form() {

        $district = $this->getProvinceData();

        $pickupHub = $this->getPickupHub();

        $service = $this->getService();

        $totalbill = isset($_POST['total']) ? $_POST['total'] : '';

        $fullname = isset($_POST['fullname']) ? $_POST['fullname'] : '';

        $address = isset($_POST['address']) ? $_POST['address'] : '';

        $phone = isset($_POST['phone']) ? $_POST['phone'] : '';

        $order_id = isset($_POST['order_id']) ? $_POST['order_id'] : '';

        $export_id = isset($_POST['export_id']) ? $_POST['export_id'] : '';

        $pay_method = isset($_POST['pay_method']) ? $_POST['pay_method'] : '';



        $setSO = $this->db->query("select MaXK from ttp_report_export_warehouse where OrderID=$order_id")->row();

        $getSO = $setSO->MaXK;



        $DistrictID = isset($_POST['district']) ? $_POST['district'] : ''; //10

        $getGHN = $this->db->query("select * from ttp_report_district where ID=$DistrictID")->row();

        $dis = $getGHN->GHN;

        $dis_arr = explode("-", $dis);

        $add = trim($dis_arr[0]);



        //Kiểm tra kết nối tới GHN

        $login = $this->_sessionToken;

        $isLogin = "";

        if (!empty($login)) {

            $isLogin = "Connected";
        }



        $data = array(
            'PickHub' => $pickupHub,
            'District' => $district,
            'Service' => $service,
            'TotalBill' => $totalbill,
            'Fullname' => $fullname,
            'Address' => $address,
            'DistrictID' => $add,
            'PayMethod' => $pay_method,
            'Phone' => $phone,
            'SOCode' => $getSO,
            'IDOrder' => $order_id,
            'IDExport' => $export_id,
            'isLogin' => $isLogin
        );



        $view = 'admin/get_transport_form';

        $this->load->view($view, $data);
    }

    public function getProvinceData() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $districtProvinceDataRequest = array("SessionToken" => $this->_sessionToken);

        $responseDistrictProvinceData = $this->_ghn->GetDistrictProvinceData($districtProvinceDataRequest);

        return $responseDistrictProvinceData["Data"];
    }

    private function getPickupHub() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $getPickHubRequest = array("SessionToken" => $this->_sessionToken);

        $responseGetPickHubs = $this->_ghn->GetClientHubs($getPickHubRequest);

        if (empty($responseGetPickHubs['ErrorMessage'])) {

            return $responseGetPickHubs["HubInfo"];
        }
    }

    public function createSo() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $SOCode = isset($_GET['SOCode']) ? $_GET['SOCode'] : '';



        $RecipientName = isset($_GET['RecipientName']) ? $_GET['RecipientName'] : '';

        $DeliveryAddress = isset($_GET['DeliveryAddress']) ? $_GET['DeliveryAddress'] : '';

        $RecipientPhone = isset($_GET['RecipientPhone']) ? $_GET['RecipientPhone'] : '';

        $CODAmount = isset($_GET['CODAmount']) ? $_GET['CODAmount'] : '';

        $ContentNote = isset($_GET['ContentNote']) ? $_GET['ContentNote'] : '';



        $DeliveryDistrictCode = isset($_GET['DeliveryDistrictCode']) ? $_GET['DeliveryDistrictCode'] : '';



        $ServiceID = isset($_GET['ServiceID']) ? $_GET['ServiceID'] : '';

        $PickHubID = isset($_GET['pickupHubID']) ? $_GET['pickupHubID'] : '';



        $Weight = isset($_GET['Weight']) ? $_GET['Weight'] : '';

        $Length = isset($_GET['Length']) ? $_GET['Length'] : '';

        $Width = isset($_GET['Width']) ? $_GET['Width'] : '';

        $Height = isset($_GET['Height']) ? $_GET['Height'] : '';

        $payment = isset($_GET['paymentMethod']) ? $_GET['paymentMethod'] : '';

        $invoice = isset($_GET['invoice']) ? $_GET['invoice'] : '';

        $Fee = isset($_GET['Fee']) ? $_GET['Fee'] : '';

        $isChargeFee = isset($_GET['isChargeFee']) ? $_GET['isChargeFee'] : '';



        $CODAmount = $this->convertNumber($CODAmount, ",");

        $Fee = $this->convertNumber($Fee, ",");



        if ($isChargeFee == 1) {

            $CODAmount = $CODAmount + $Fee;
        }



        $shippingOrderRequest = array(
            "SessionToken" => $this->_sessionToken,
            "RecipientName" => $RecipientName,
            "DeliveryAddress" => $DeliveryAddress,
            "RecipientPhone" => $RecipientPhone,
            "ClientOrderCode" => $SOCode,
            "CODAmount" => $this->convertNumber($CODAmount, ","),
            "ContentNote" => $ContentNote,
            "DeliveryDistrictCode" => $DeliveryDistrictCode,
            "ServiceID" => $ServiceID,
            "PickHubID" => $PickHubID,
            "Weight" => $Weight,
            "Length" => $Length,
            "Width" => $Width,
            "Height" => $Height
        );



        $responseCreateShippingOrder = $this->_ghn->CreateShippingOrder($shippingOrderRequest);

        $onError = ($responseCreateShippingOrder) ? $responseCreateShippingOrder["ErrorMessage"] : "";



        $Ghichu = isset($_GET["Ghichu"]) ? $_GET["Ghichu"] : '';



        $OrderID = isset($_GET['IDOrder']) ? $_GET['IDOrder'] : '';

        $ExportID = isset($_GET['IDExport']) ? $_GET['IDExport'] : '';

        if ($onError == null) {

            $ghnData = array();

            $ghnData["OrderCode"] = $responseCreateShippingOrder["OrderCode"];

            $ghnData["OrderID"] = $OrderID;

            $ghnData["ExportID"] = $ExportID;

            $ghnData["ClientOrderCode"] = $SOCode;

            $ghnData["TotalFee"] = $responseCreateShippingOrder["TotalFee"];

            $ghnData["CoDAmount"] = $CODAmount;

            $ghnData["Payment"] = $payment;

            $ghnData["CurrentStatus"] = "ReadyToPick";

            $ghnData["Created"] = date("Y-m-d H:i:s");

            $ghnData["Updated"] = date("Y-m-d H:i:s");

            $this->db->insert("ttp_transport", $ghnData);



            $dataUpdate = array(
                'Status' => 11,
                'TransportFee' => $responseCreateShippingOrder["TotalFee"],
                'TransportRef' => $responseCreateShippingOrder["OrderCode"]
            );

            $this->db->where("ID", $OrderID);

            $this->db->update("ttp_report_order", $dataUpdate);



            $datahis = array(
                'OrderID' => $OrderID,
                'Thoigian' => date('Y-m-d H:i:s', time()),
                'Status' => 11,
                "Ghichu" => $Ghichu,
                "UserID" => $this->user->ID
            );

            $this->db->insert('ttp_report_orderhistory', $datahis);

            $res["ErrorMessage"] = "";
        } else {

            $res["ErrorMessage"] = $onError;
        }

        echo json_encode($res);
    }

    private function convertNumber($number, $delimiter) {

        return str_replace($delimiter, "", $number);
    }

    public function getCities($ghnProvince) {

        $result = array();

        foreach ($ghnProvince as $item) {

            $pcode = $item["ProvinceCode"];

            $pname = $item["ProvinceName"];

            $result[$pcode] = $pname;
        }

        return $result;
    }

    public function getDistrictsByCityCode($ghnProvince, $cityCode) {

        $result = array();



        foreach ($ghnProvince as $item) {

            if ($item["ProvinceCode"] == $cityCode) {

                $result[$item["DistrictCode"]] = $item["DistrictName"];
            }
        }

        return $result;
    }

    private function genShippingOrderID() {

        $pattern = "TTP-SO-%s-%s-%s-%s";

        $day = date("d");

        $month = date("m");

        $year = date("Y");

        $timestamp = date("His");

        return sprintf($pattern, $day, $month, $year, $timestamp);
    }

    public function calculateFee() {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $Length = isset($_GET['Length']) ? $_GET['Length'] : 0;

        $Weight = isset($_GET['Weight']) ? $_GET['Weight'] : 0;

        $Height = isset($_GET['Height']) ? $_GET['Height'] : 0;

        $Width = isset($_GET['Width']) ? $_GET['Width'] : 0;

        $FromDistrictCode = isset($_GET['PickHubID']) ? $_GET['PickHubID'] : '';

        $ToDistrictCode = isset($_GET['DeliveryDistrictCode']) ? $_GET['DeliveryDistrictCode'] : '';

        $ServiceID = isset($_GET['ServiceID']) ? $_GET['ServiceID'] : '';

        $c = array("FromDistrictCode" => $FromDistrictCode,
            "ServiceID" => $ServiceID,
            "ToDistrictCode" => $ToDistrictCode,
            "Weight" => $Weight,
            "Length" => $Length,
            "Width" => $Width,
            "Height" => $Height
        );

        $items[] = $c;

        $calculateServiceFeeRequest = array("SessionToken" => $this->_sessionToken, "Items" => $items);

        $responseCalculateServiceFee = $this->_ghn->CalculateServiceFee($calculateServiceFeeRequest);

        echo json_encode($responseCalculateServiceFee);
    }

    public function getServiceList() {

        $FromDistrictCode = isset($_GET['FromDistrictCode']) ? $_GET['FromDistrictCode'] : 0;

        $ToDistrictCode = isset($_GET['ToDistrictCode']) ? $_GET['ToDistrictCode'] : 0;

        $ServiceList = $this->getServiceListData($FromDistrictCode, $ToDistrictCode);

        echo json_encode($ServiceList);
    }

    private function getServiceListData($FromDistrictCode, $ToDistrictCode) {

        $this->load->model("Giaohangnhanh");

        $this->_sessionToken = $this->Giaohangnhanh->getSessionToken();

        $this->_ghn = $this->Giaohangnhanh->getGhn();

        $infoServiceRequest = array("SessionToken" => $this->_sessionToken, "FromDistrictCode" => $FromDistrictCode, "ToDistrictCode" => $ToDistrictCode);

        $responseServiceInfo = $this->_ghn->GetServiceList($infoServiceRequest);

        return $responseServiceInfo;
    }

    private function getService() {

        $result = array(
            17 => "Gói siêu tốc (kiện)",
            18 => "Gói tiết kiệm (kiện)",
            53319 => "6 Giờ",
            53320 => "1 Ngày",
            53321 => "2 Ngày",
            53322 => "3 Ngày",
            53323 => "4 Ngày",
            53324 => "5 Ngày",
            53325 => "Prime",
            53326 => "4 Giờ",
            53327 => "6 Ngày",
            53329 => "60 phút",
            53330 => "Chuyển phát cá nhân",
            53339 => "266"
        );

        return $result;
    }

    public function getStatus($id = null) {

        $status = array(
            'ReadyToPick' => "Chờ lấy hàng",
            'Picking' => "Đang lấy hàng",
            'Storing' => "Lưu kho",
            'Delivering' => "Đang giao hàng",
            'Delivered' => "Đã giao hàng",
            'WaitingToFinish' => "Đơn hàng đã được chuyển sang chờ thanh toán",
            'Finish' => "Kết thúc đơn hàng",
            'Return' => "Đơn hàng được trả lại",
            'Cancel' => "Hủy đơn hàng"
        );

        if ($id == null) {

            return $status;
        }

        return $status[$id];
    }

    public function checkvoucher() {

        $Voucher = isset($_POST['Voucher']) ? $_POST['Voucher'] : '';

        $check = $this->db->query("select * from ttp_report_voucher where Code='$Voucher'")->row();

        $data = array('Errorcode' => 0, "Message" => "");

        if ($check) {

            if ($check->UseOne == 0) {

                if ($check->Status == 1) {

                    $data = array('Errorcode' => 1, "Message" => "Mã này đã có người sử dụng");
                }
            }

            if (strtotime($check->Startday) > time() || strtotime($check->Stopday) < time()) {

                $data = array('Errorcode' => 1, "Message" => "Mã này đã hết hạn sử dụng");
            }

            if ($data['Errorcode'] == 0) {

                $data['type'] = $check->PriceReduce > 0 ? 0 : 1;

                $data['data'] = $check->PriceReduce > 0 ? $check->PriceReduce : $check->PercentReduce;

                $data['Message'] = $check->PriceReduce > 0 ? "Mã giảm giá bạn đang sử dụng có giá trị giảm " . number_format($check->PriceReduce) . "đ trên tổng giá trị đơn hàng" : "Mã giảm giá bạn đang sử dụng có giá trị giảm " . $check->PercentReduce . "% trên tổng giá trị đơn hàng";
            }
        } else {

            $data = array('Errorcode' => 1, "Message" => "Không tìm thấy mã trong hệ thống");
        }

        echo json_encode($data);
    }

    public function voucher($Voucher = '') {

        $check = $this->db->query("select * from ttp_report_voucher where Code='$Voucher'")->row();

        if ($check) {

            if ($check->UseOne == 0) {

                if ($check->Status == 1) {

                    return array('state' => 0);
                }
            }

            if (strtotime($check->Startday) > time() || strtotime($check->Stopday) < time()) {

                return array('state' => 0);
            }

            return array('state' => $check->ID, 'UseOne' => $check->UseOne);
        }

        return array('state' => 0);
    }

    public function active_financemoney() {

        $order = isset($_POST['ID']) ? $_POST['ID'] : 0;

        $this->db->query("update ttp_report_order set FinanceMoney=1 where ID=$order");
    }

}
?>

