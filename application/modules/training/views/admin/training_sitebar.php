<?php 
$url = base_url().ADMINPATH.'/training';
$segment_4 = $this->uri->segment(4);
$segment_3 = $this->uri->segment(3);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-graduation-cap fa-fw"></i> Huấn luyện</p>
    <ul>
	<li><a class="<?php echo $segment_3=='learn' ? 'active' : '' ; ?>" href="<?php echo $url."/learn/slist" ?>">Khóa học của tui</a></li>
    <li><a class="<?php echo ($segment_3=='course' && $segment_4=='list_course_user') || $segment_4=='add_course_user' ? 'active' : '' ; ?>" href="<?php echo $url."/course/list_course_user" ?>">Phân bổ khóa học</a></li>
    <li><a class="<?php echo ($segment_3=='course' && $segment_4=='slist') || $segment_4=='add_course' || $segment_4=='add_lesson' ? 'active' : '' ; ?>" href="<?php echo $url."/course/slist" ?>">Quản lý khóa học</a></li>
    <li><a class="<?php echo $segment_3=='group' ? 'active' : '' ; ?>" href="<?php echo $url."/group" ?>">Quản lý nhóm khóa học</a></li>
        <!-- <li><a href="<?php echo $url."/lean/slist" ?>">Khóa học của tui</a></li> -->
        <!-- <li><a>Tìm kiếm khóa học</a></li>
        <li><a>Theo dõi quá trình học</a></li> -->
    </ul>
    <p class="title"><i class="fa fa-clock-o fa-fw"></i> Chấm công</p>
    <ul>
        <li><a>Theo dõi chấm công</a></li>
        <li><a>Nghỉ phép</a></li>
    </ul>
    <p class="title"><i class="fa fa-usd fa-fw"></i> Lương & Thưởng</p>
    <ul>
        <li><a>Lương</a></li>
        <li><a>Thưởng</a></li>
        <li><a>Commission</a></li>
        <li><a>KPI Bonus</a></li>
        <li><a>Thu nhập khác</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>
