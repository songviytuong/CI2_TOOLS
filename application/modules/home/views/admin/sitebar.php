<?php
$segment = $this->uri->segment(3);
$temp_current_group = explode("_", $segment);
$current_group = isset($temp_current_group[0]) ? $temp_current_group[0] : '';
$current_page = isset($temp_current_group[1]) ? $temp_current_group[1] : '';
$permission = json_decode($user->DetailRole, true);
?>
<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="<?php echo base_url() . ADMINPATH ?>" class="site_title"><i class="fa fa-user"></i> <span>Control Panel</span></a>
    </div>
    <div class="clearfix"></div>
    <!-- menu prile quick info -->
    <div class="profile">
        <div class="profile_pic">
            <img src="<?php echo file_exists($user->Thumb) ? base_url() . $user->Thumb : base_url() . "public/admin/images/user.png"; ?>" alt="<?php echo $user->UserName ?>" class="img-circle profile_img">
        </div>
        <div class="profile_info">
            <span>Welcome,</span>
            <h2><?php echo $user->FirstName . " " . $user->LastName ?></h2>
        </div>
    </div>
    <!-- /menu prile quick info -->
    <br />
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
                <li <?php echo $current_group == "system" ? "class='active'" : ''; ?>><a><i class="fa fa-cog"></i> System <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php echo $current_group == "system" ? "display:block" : "display:none"; ?>">
                        <?php
                        if ($user->IsAdmin == 1) {
                            ?>
                            <li <?php echo $current_page == "module" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_module">Permission list</a></li>
                            <li <?php echo $current_page == "role" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_role">Role list</a></li>
                            <li <?php echo $current_page == "user" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_user">User list</a></li>
                            <li <?php echo $current_page == "email" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_email">Email Setting</a></li>
                            <li <?php echo $current_page == "language" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_language">Languages</a></li>
                            <li <?php echo $current_page == "listip" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_listip">List IP</a></li>
                            <?php
                        } else {
                            if (array_key_exists('system_language', $permission)) {
                                ?>
                                <li <?php echo $current_page == "language" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_language">Languages</a></li>
                                <?php
                            }
                            if (array_key_exists('system_module', $permission)) {
                                ?>
                                <li <?php echo $current_page == "module" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_module">Permission list</a></li>
                                <?php
                            }
                            if (array_key_exists('system_role', $permission)) {
                                ?>
                                <li <?php echo $current_page == "role" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_role">Role list</a></li>
                                <?php
                            }
                            if (array_key_exists('system_user', $permission)) {
                                ?>
                                <li <?php echo $current_page == "user" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_user">User list</a></li>
                                <?php
                            }
                            if (array_key_exists('system_email', $permission)) {
                                ?>
                                <li <?php echo $current_page == "email" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/system_email">Email Setting</a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <li <?php echo $current_group == "site" ? "class='active'" : ''; ?>><a><i class="fa fa-home"></i> Content Manager <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php echo $current_group == "site" ? "display:block" : "display:none"; ?>">
                        <?php
                        if ($user->IsAdmin == 1) {
                            ?>
                            <li <?php echo $current_page == "staticconfig" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_staticconfig">Static config</a></li>
                            <li <?php echo $current_page == "pagelinks" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_pagelinks">Page links</a></li>
                            <li <?php echo $current_page == "categories" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_categories">Categories Pages</a></li>
                            <li <?php echo $current_page == "post" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_post">Post Pages</a></li>
                            <li <?php echo $current_page == "banner" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_banner">Banner list</a></li>
                            <li <?php echo $current_page == "promotions" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_promotions">Promotions Manager</a></li>
                            <li <?php echo $current_page == "maps" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_maps">Maps Manager</a></li>
                            <li <?php echo $current_page == "ebrochure" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_ebrochure">E-Brochure Manager</a></li>
                            <li <?php echo $current_page == "recruitment" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_recruitment">Recruitment Manager</a></li>
                            <li <?php echo $current_page == "internallink" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_internallink">Site links footer</a></li>
                            <li <?php echo $current_page == "tags" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_tags">Tags</a></li>
                            <?php
                        } else {
                            if (array_key_exists('site_staticconfig', $permission)) {
                                ?>
                                <li <?php echo $current_page == "staticconfig" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_staticconfig">Static config</a></li>
                                <?php
                            }
                            if (array_key_exists('site_pagelinks', $permission)) {
                                ?>
                                <li <?php echo $current_page == "pagelinks" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_pagelinks">Page links</a></li>
                                <?php
                            }
                            if (array_key_exists('site_categories', $permission)) {
                                ?>
                                <li <?php echo $current_page == "categories" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_categories">Categories Pages</a></li>
                                <?php
                            }
                            if (array_key_exists('site_post', $permission)) {
                                ?>
                                <li <?php echo $current_page == "post" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_post">Post Pages</a></li>
                                <?php
                            }
                            if (array_key_exists('site_banner', $permission)) {
                                ?>
                                <li <?php echo $current_page == "banner" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_banner">Banner list</a></li>
                                <?php
                            }
                            if (array_key_exists('site_promotions', $permission)) {
                                ?>
                                <li <?php echo $current_page == "promotions" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_promotions">Promotions Manager</a></li>
                                <?php
                            }
                            if (array_key_exists('site_maps', $permission)) {
                                ?>
                                <li <?php echo $current_page == "maps" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_maps">Maps Manager</a></li>
                                <?php
                            }
                            if (array_key_exists('site_ebrochure', $permission)) {
                                ?>
                                <li <?php echo $current_page == "ebrochure" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_ebrochure">E-Brochure Manager</a></li>
                                <?php
                            }
                            if (array_key_exists('site_recruitment', $permission)) {
                                ?>
                                <li <?php echo $current_page == "recruitment" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_recruitment">Recruitment Manager</a></li>
                                <?php
                            }
                            if (array_key_exists('site_internallink', $permission)) {
                                ?>
                                <li <?php echo $current_page == "internallink" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_internallink">Site links footer</a></li>
                                <?php
                            }
                            if (array_key_exists('site_tags', $permission)) {
                                ?>
                                <li <?php echo $current_page == "tags" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/site_tags">Tags</a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <li <?php echo $current_group == "client" ? "class='active'" : ''; ?>><a><i class="fa fa-reply-all"></i> Client interaction <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php echo $current_group == "client" ? "display:block" : "display:none"; ?>">
                        <?php
                        if ($user->IsAdmin == 1) {
                            ?>
                            <li <?php echo $current_page == "formrecruitment" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/client_formrecruitment">Recruitment</a></li>
                            <li <?php echo $current_page == "formcontact" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/client_formcontact">Contact Form</a></li>
                            <li <?php echo $current_page == "customeremail" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/client_customeremail">Customer Email</a></li>
                            <?php
                        } else {
                            if (array_key_exists('client_formrecruitment', $permission)) {
                                ?>
                                <li <?php echo $current_page == "formrecruitment" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/client_formrecruitment">Recruitment</a></li>
                                <?php
                            }
                            if (array_key_exists('client_formcontact', $permission)) {
                                ?>
                                <li <?php echo $current_page == "formcontact" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/client_formcontact">Contact Form</a></li>
                                <?php
                            }
                            if (array_key_exists('client_customeremail', $permission)) {
                                ?>
                                <li <?php echo $current_page == "customeremail" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/client_customeremail">Customer Email</a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <li <?php echo $current_group == "gallery" ? "class='active'" : ''; ?>><a><i class="fa fa-picture-o"></i> Image gallery <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php echo $current_group == "gallery" ? "display:block" : "display:none"; ?>">
                        <?php
                        if ($user->IsAdmin == 1) {
                            ?>
                            <li <?php echo $current_page == "album" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/gallery_album">Album</a></li>
                            <li <?php echo $current_page == "images" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/gallery_images">Images</a></li>
                            <?php
                        } else {
                            if (array_key_exists('gallery_album', $permission)) {
                                ?>
                                <li <?php echo $current_page == "album" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/gallery_album">Album</a></li>
                                <?php
                            }
                            if (array_key_exists('gallery_images', $permission)) {
                                ?>
                                <li <?php echo $current_page == "images" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/gallery_images">Images</a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <li <?php echo $current_group == "crawler" ? "class='active'" : ''; ?>><a><i class="fa fa-database"></i> Crawl <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu" style="<?php echo $current_group == "crawler" ? "display:block" : "display:none"; ?>">
                        <?php
                        if ($user->IsAdmin == 1) {
                            ?>
                            <li <?php echo $current_page == "setting" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/crawler_setting">Bot setting</a></li>
                            <li <?php echo $current_page == "data" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/crawler_data">Crawl data</a></li>
                            <li <?php echo $current_page == "site" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/crawler_site">Crawl site</a></li>
                            <?php
                        } else {
                            if (array_key_exists('crawler_setting', $permission)) {
                                ?>
                                <li <?php echo $current_page == "setting" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/crawler_setting">Setting</a></li>
                                <?php
                            }
                            if (array_key_exists('crawler_data', $permission)) {
                                ?>
                                <li <?php echo $current_page == "data" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/crawler_data">Crawl data</a></li>
                                <?php
                            }
                            if (array_key_exists('crawler_site', $permission)) {
                                ?>
                                <li <?php echo $current_page == "site" ? "class='current-page'" : ''; ?>><a href="<?php echo base_url() . ADMINPATH ?>/home/crawler_site">Crawl site</a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url() . ADMINPATH . "/logout"; ?>">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    <!-- /menu footer buttons -->
</div>