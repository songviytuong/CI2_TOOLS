<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>PHIẾU YÊU CẦU NHẬP KHO</h1>
				</div>
				<div class="block2">
					<button class="btn btn-danger" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> Gửi yêu cầu</button>
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="block1">
					<div class="row">
						<label for="" class="col-xs-2 control-label">Chọn kho nhập hàng</label>
						<div class="col-xs-4">
							<select name='KhoID' id="KhoID" class="form-control">
								<?php 
								$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
								if(count($warehouse)>0){
									foreach($warehouse as $row){
										echo "<option value='$row->ID'>$row->MaKho</option>";
									}
								}
								?>
							</select>
						</div>
						<label for="" class="col-xs-2 control-label">Ngày chứng từ nhập kho</label>
						<div class="col-xs-4">
							<input type='text' name='NgayNK' class='form-control' id='NgayNK' value='<?php echo date('Y-m-d') ?>' readonly="true" />
						</div>
					</div>
					<div class="row">
						<label for="" class="col-xs-2 control-label">Hình thức yêu cầu nhập kho</label>
						<div class="col-xs-4">
							<select class="form-control" name="Type">
								<option value="0">Mua hàng từ nhà cung cấp</option>
							</select>
						</div>
						<label for="" class="col-xs-2 control-label">Nhập hàng theo PO số</label>
						<div class="col-xs-4">
							<select name="POID" id="POSelect" class="form-control">
								<option value='0'>-- Chọn PO --</option>
								<?php 
								$PO = $this->db->query("select ID,POCode from ttp_report_perchaseorder where Status not in(0,1,5) order by POCode")->result();
								if(count($PO)>0){
									foreach($PO as $row){
										echo "<option value='$row->ID'>$row->POCode</option>";
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-xs-2 control-label">Nhà cung cấp theo PO</label>
						<div class="col-xs-4">
							<span class="input-group">
								<select name="ProductionID" id="ProductionID" class="form-control">
									<option value='0'>-- Chọn nhà cung cấp --</option>
									<?php 
									$production = $this->db->query("select ID,Title from ttp_report_production where Published=1")->result();
									if(count($production)>0){
										foreach($production as $row){
											echo "<option value='$row->ID'>$row->Title</option>";
										}
									}
									?>
								</select>
								<span class="input-group-btn">
									<a class='btn btn-default' title="Thêm nhà cung cấp mới" onclick="add_production(this)"><i class="fa fa-plus"></i></a>
								</span>
							</span>
						</div>
					</div>
					<div class="row">
						<label for="" class="col-xs-2 control-label">Diễn giải yêu cầu nhập kho</label>
						<div class="col-xs-10">
							<input type='text' name="Note" class="form-control required" required />
						</div>
					</div>
				</div>
				<hr>
				<!-- end block1 -->
		    	<div class="block2">
					<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản Phẩm</a>
					<div class="btn-group">
						<a class="btn btn-default dropdown-toggle" id="show_thaotac" data-toggle="dropdown" aria-expanded="false">
							Thao tác
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
						</ul>
					</div>
		    	</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang data-table" style="background:#FFF">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã SP</th>
		    				<th>Tên sản phẩm</th>
		    				<th>ĐVT</th>
		    				<th>Lô hàng</th>
		    				<th style="width: 150px;">Số lượng <br>của PO</th>
		    				<th style="width: 150px;">Số lượng thực tế <br>đã nhập</th>
		    				<th style="width: 130px;">SL yêu cầu nhập</th>
		    			</tr>
		    			<tr>
		    				<td colspan="6">TỔNG CỘNG</td>
		    				<td><span class='tongcong'>0</span></td>
		    			</tr>
		    		</table>
		    	</div>
			</div>
			<input type="hidden" name="Status" value="0" />
			<input type="hidden" name="Ghichu" value="" />
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	function stopRKey(evt) { 
		var evt = (evt) ? evt : ((event) ? event : null); 
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
		if ((evt.keyCode == 13) && (node.type=="text" || node.type=="number") && node.id!="input_search_products")  {return false;} 
	} 

	document.onkeypress = stopRKey; 

	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function add_production(ob){
		enablescrollsetup();
		$(".over_lay .box_inner").css({"width":"500px"});
		$(".over_lay .box_inner .block1_inner h1").html("Thêm nhà cung cấp mới");
		$(".over_lay .box_inner .block2_inner").html("<div class='row' style='margin: 0px'><p style='margin-bottom:5px'>Tên nhà cung cấp</p><div><input type='text' class='form-control title_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Mã nhà cung cấp</p><div><input type='text' class='form-control code_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Địa chỉ trụ sở</p><div><input type='text' class='form-control address_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Số điện thoại</p><div><input type='text' class='form-control phone_production' style='margin-bottom:5px' /></div><p style='margin-bottom:5px'>Fax</p><div><input type='text' class='form-control fax_production' style='margin-bottom:5px' /></div></div><div class='row'><button class='btn btn-primary' style='float:left;background:#1A82C3;color:#FFF;border: 1px solid #2477CA;margin-left:15px' onclick='save_production(this)'>Lưu dữ liệu</button></div>");
		$(".over_lay").removeClass('in');
    	$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function save_production(ob){
		$(ob).addClass("saving");
		var title = $(".title_production").val();
		var code = $(".code_production").val();
		var address = $(".address_production").val();
		var phone = $(".phone_production").val();
		var fax = $(".fax_production").val();
		if(title!='' && code!=''){
			$.ajax({
            	url: link+"add_production",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "Title="+title+"&code="+code+"&address="+address+"&phone="+phone+"&fax="+fax,
	            success: function(result){
	            	$("#ProductionID").prepend(result);
	            	$(".over_lay").hide();
					disablescrollsetup();
					$(ob).removeClass("saving");
	            }
	        });
		}else{
			alert("Vui lòng điền đầy đủ thông tin !");
		}
	}

	$("#add_products_to_order").click(function(){
		var po = $("#POSelect").val();
		if(po>0){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"850px"});
			$(".over_lay .box_inner .block2_inner").html("");
			$.ajax({
	        	url: link+"get_products",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "POID="+po,
	            success: function(result){
	                if(result!='false'){
	        			$(".over_lay .box_inner").css({'margin-top':'50px'});
				    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
				    	$(".over_lay .box_inner .block2_inner").html(result);
				    	$(".over_lay").removeClass('in');
				    	$(".over_lay").fadeIn('fast');
						$(".over_lay").addClass('in');
	                }else{
	                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
	                	disablescrollsetup();
	                }
	                $(this).removeClass('saving');
	            }
	        });
        }else{
        	alert("Vui lòng chọn PO");
        }
	});

	function input_search_products(ob){
		var data = $(ob).val();
		var po = $("#POSelect").val();
		$.ajax({
        	url: link+"get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "Title="+data+"&POID="+po,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	var celldata = [];
	var sttrow = 1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			var classtr = Math.floor(Date.now() / 1000);
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				var data_donvi = $(this).attr('data-donvi');
				var data_po = parseInt($(this).attr('data-po'));
				var data_products = parseInt($(this).attr('data-products'));
				max = data_po-data_products;
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML=data_donvi;
					row.insertCell(4).innerHTML="<select class='ShipmentDefault select"+classtr+"' name='ShipmentID[]' data-id='"+data_id+"' onchange='add_shipment(this,"+data_id+")'></select>";
					row.insertCell(5).innerHTML="<span class='POAmount'>"+data_po.format('a',3)+"</span>";
					row.insertCell(6).innerHTML=data_products.format('a',3);
					row.insertCell(7).innerHTML="<input type='number' name='Amount[]' class='Amount_input' value='1' onchange='recal()' required />";
					sttrow=sttrow+1;
					loadshipmentdefault(classtr);
			}
		});
		recal();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function loadshipmentdefault(select){
		var warehouse = $('#KhoID').val();
		$(".select"+select).each(function(){
			var data = $(this).attr('data-id');
			$(this).load("<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/' ?>get_shipment_by_productsID/"+data+"/"+warehouse);
		});
	}

	function load_shipment(id,result){
		$(".ShipmentDefault").each(function(){
			if(id==0){
				var data = $(this).attr('data-id');
				$(this).load("<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/' ?>load_shipment_by_products/"+data);
			}else{
				var data = $(this).attr('data-id');
				if(data==id){
					$(this).append('<option value="'+result.ID+'">'+result.Title+'</option>');
				}
			}
		});
	}

	function save_shipment(ob,id){
		$(ob).addClass("saving");
		var ShipmentCode = $(".ShipmentCode").val();
		var DateProduction = $(".DateProduction").val();
		var DateExpiration = $(".DateExpiration").val();
		if(ShipmentCode=="" || DateProduction=="" || DateExpiration=="") {
			alert("Vui lòng điền đầy đủ thông tin !");
			$(ob).removeClass("saving");
			return false;
		}
		$.ajax({
        	url: "<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/' ?>save_shipment",
            dataType: "json",
            type: "POST",
            data: "ID="+id+"&ShipmentCode="+ShipmentCode+"&DateProduction="+DateProduction+"&DateExpiration="+DateExpiration,
            success: function(result){
            	load_shipment(id,result);
            	$(".over_lay").hide();
				disablescrollsetup();
				$(ob).removeClass("saving");
            }
        });
	}

	function add_shipment(ob,id){
		var data = $(ob).val();
		if(data=="add"){
			enablescrollsetup();
			$(".over_lay .box_inner").css({"width":"500px"});
			$(".over_lay .box_inner .block1_inner h1").html("Tạo lô sản phẩm");
			$(".over_lay .box_inner .block2_inner").html("");
			$(".over_lay .box_inner .block2_inner").load("<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/' ?>box_add_shipment/"+id);
			$(".over_lay").removeClass('in');
	    	$(".over_lay").fadeIn('fast');
			$(".over_lay").addClass('in');
		}else{
			if(data!=0){
				var po = $("#POSelect").val();
				$(ob).parent('td').parent('tr').find('span.POAmount').load("<?php echo base_url().ADMINPATH.'/report/warehouse_inventory_perchaseorder/' ?>load_amount_po/"+id+"/"+data+"/"+po);
			}
		}
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				var index = celldata.indexOf("data"+data_id);
				celldata.splice(index, 1);
				sttrow = sttrow-1;
			}
		});
		recal();
	});

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			max = parseInt(parent.find('input.Amount_input').attr('max'));
			if(amount>max){
				parent.find('input.Amount_input').val(max);
				amount = max;
			}
			tongcong = tongcong+amount;
		});
		$(".tongcong").html(tongcong.format('a',3));
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	}

	function change_type(ob){
		var data = parseInt($(ob).val());
		var destination = '';
		if(data==0){
			destination="import_from_production";
		}
		if(data==2){
			destination="import_from_internal";
		}
		window.location=link+destination;
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	$("#POSelect").change(function(){
		var ID = $(this).val();
		$.ajax({
        	url: link+"get_production_from_purchaseorder",
            dataType: "html",
            type: "POST",
            context: this,
            data: "ID="+ID,
            success: function(result){
                $("#ProductionID").val(result);
            }
        });
	});

</script>
<style>
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(5){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(6){text-align:right;}
	.body_content .containner .box_content_warehouse_import .table_donhang .table_data tr td:nth-child(7){text-align:right;}
</style>