<?php
class Training_point_detail_model extends CI_Model
{
    private $_tableName = "ttp_training_point_detail";
    public function getList($groupid="")
    {
        $sql = "select * from $this->_tableName where enabled = 1 ";
        if($groupid !=null){
            $sql .=" and groupid = $groupid";
        }
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
     public function insert($data){
        $this->db->insert($this->_tableName,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function update($id,$data){
        $this->db->where("id", $id);
        return $this->db->update($this->_tableName, $data);
    }
    public function getDetail($id)
    {
        $sql = "select * from $this->_tableName where enabled = 1 and  id = $id ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : '';
    }
    public function getDetailByQuestionId($pointid,$questionid)
    {
        $sql = "select * from $this->_tableName where pointid = $pointid and questionid = $questionid ";
        $result = $this->db->query($sql)->result_array();
        return isset($result[0]) ? $result[0] : '';
    }
    public function getSum($pointid)
    {
        $sql = "SELECT trues,count(*) as total FROM $this->_tableName WHERE pointid = $pointid GROUP BY `trues`;";
        $result = $this->db->query($sql)->result_array();
        return isset($result) ? $result : '';
    }
    public function countQuestionIdByUserId($userid,$questionid){
        $sql = " SELECT questionid,trues,count(*) as total FROM `ttp_training_point_detail` WHERE questionid IN($questionid) and userid !=$userid GROUP BY questionid,trues";
        $result = $this->db->query($sql)->result_array();
        return isset($result) ? $result : 0;
    }


}?>