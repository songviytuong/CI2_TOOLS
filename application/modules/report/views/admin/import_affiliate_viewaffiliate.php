	<?php 
	$data = json_decode($info->Data);
	?>
		<div class="row" style="margin:5px 0px">
			<div class="form-group">
				<div class="col-xs-8">
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Họ và tên : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Fullname' value="<?php echo isset($data->Fullname) ? $data->Fullname : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Ngày sinh : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Birthday' value="<?php echo isset($data->Birthday) ? $data->Birthday : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Số CMND : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='CMND' value="<?php echo isset($data->CMND) ? $data->CMND : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Số tài khoản NH : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Bankcode' value="<?php echo isset($data->Bankcode) ? $data->Bankcode : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Tên ngân hàng : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='Banktitle' value="<?php echo isset($data->Banktitle) ? $data->Banktitle : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Mã số thuế cá nhân : </label>
						<div class="col-xs-6">
							<input type='text' class='form-control' name='MST' value="<?php echo isset($data->MST) ? $data->MST : '' ; ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Cấp độ hiện tại : </label>
						<div class="col-xs-5">
							<select class="form-control" name="Level">
								<?php 
								$arr = $this->lib->get_config_define("affiliate","level",1);
								if(count($arr)>0){
									foreach($arr as $row){
										$selected = $info->Level==$row->code ? "selected='selected'" : '' ;
										echo '<option value="'.$row->code.'" '.$selected.'>'.$row->name.'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Ngày đăng ký </label>
						<label for="" class="col-xs-6 control-label">: <?php echo isset($info->Created) ? $info->Created : '' ; ?></label>
					</div>
					<div class="form-group">
						<label for="" class="col-xs-3 control-label">Ngày được duyệt </label>
						<label for="" class="col-xs-6 control-label">: <?php echo isset($data->Acceptdate) ? $data->Acceptdate : '' ; ?></label>
					</div>
					
				</div>
				<div class="col-xs-1"></div>
				<div class="col-xs-3" style="background: #f3f3f3;position:relative;min-height:100px">
					<div class="dvPreview">
						<img src="<?php echo $info->Thumb ? $info->Thumb : 'public/admin/images/user.png' ; ?>" class="img-responsive" style="width:100%;" />
					</div>
				</div>
			</div>
		</div>
</div>
<style>
	.form-group{margin-bottom:15px !important;overflow: hidden}
</style>