<?php 
class Import_targets extends Admin_Controller { 
 
 	public $user;
 	public $classname="import_targets";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');   
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/import_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Import Targets');
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $result = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
        $json = $result ? json_decode($result->DataDepartment,true) : array() ;
        $data = array(
            'base_link'         =>  base_url().ADMINPATH.'/report/import_targets/',
            'data'              => $json,
            'current_year'      => $year,
            'current_department'=> $department
        );
        $view = $this->user->IsAdmin==1 ? 'admin/import_targets_year' : 'admin/import_targets_year' ;
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function set_fillter(){
        $year = isset($_GET['year']) ? $_GET['year'] : date('Y',time()) ;
        $department = isset($_GET['department']) ? $_GET['department'] : 0 ;
        $team = isset($_GET['team']) ? $_GET['team'] : 0 ;
        $targets = isset($_GET['targets']) ? $_GET['targets'] : 0 ;
        if(isset($_GET['year']))
        $this->session->set_userdata("import_targets_year",$year);
        if(isset($_GET['department']))
        $this->session->set_userdata("import_targets_department",$department);
        if(isset($_GET['team']))
        $this->session->set_userdata("import_targets_team",$team);
        if(isset($_GET['targets']))
        $this->session->set_userdata("import_targets_targets",$targets);
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH."/report/import_targets" ;
        redirect($refer);
    }

    public function add_type_chitieu(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        if($title!=''){
            $check = $this->db->query("select * from ttp_report_targets_type where Title = '$title'")->row();
            if(!$check){
                $data = array("Title"=>$title);
                $this->db->insert("ttp_report_targets_type",$data);
                echo "true";
                return;
            }
        }
        echo "false";
    }

    public function save_chitieu_year(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $data = isset($_POST['year']) ? $_POST['year'] : array() ;
        $check = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
        $json = $check ? json_decode($check->DataDepartment,true) : array() ;
        foreach($data as $key1=>$value1){
            if(is_array($value1) && count($value1)>0){
                foreach ($value1 as $key2 => $value2) {
                    if(is_array($value2) && count($value2)>0){
                        foreach($value2 as $key3=>$value3){
                            if(is_array($value3) && count($value3)>0){
                                foreach($value3 as $key4=>$value4){
                                    $temptotal = isset($json[$key1][$key2][$key3]['Total']) ? $json[$key1][$key2][$key3]['Total'] : 0 ;
                                    $tempremain = isset($json[$key1][$key2][$key3]['Remain']) ? $json[$key1][$key2][$key3]['Remain'] : $temptotal ;
                                    $json[$key1][$key2][$key3]['Remain'] = $value4-$temptotal+$tempremain;
                                    $json[$key1][$key2][$key3][$key4] = $value4;
                                }
                            }
                        }
                    }else{
                        $json[$key1][$key2] = $value2;
                    }
                }
            }
        }
        if($check){
            $sql = array(
                'DataDepartment'=>json_encode($json)
            );
            $this->db->where("ID",$check->ID);
            $this->db->update("ttp_report_targets_year",$sql);
        }else{
            $sql = array(
                'Year'=>$year,
                'DataDepartment'=>json_encode($data)
            );
            $this->db->insert("ttp_report_targets_year",$sql);
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH."/report/import_targets" ;
        redirect($refer);
    }

    public function bymonth(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Import Targets');
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $result = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
        $json = $result ? json_decode($result->DataDepartment,true) : array() ;
        $data = array(
            'base_link'         =>  base_url().ADMINPATH.'/report/import_targets/',
            'data'              => $json,
            'current_year'      => $year,
            'current_department'=> $department
        );
        $view = $this->user->IsAdmin==1 ? 'admin/import_targets_month' : 'admin/import_targets_month' ;
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function save_chitieu_month(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $data = isset($_POST['year']) ? $_POST['year'] : array() ;
        if(count($data)>0){
            $check = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
            if($check){
                $json = json_decode($check->DataDepartment,true);
                foreach($data as $key=>$value){
                    if(isset($value['Department'][$department])){
                        $department_key = $value['Department'][$department];
                        $json[$key]['Department'][$department]['Total'] = $department_key['Total'];
                        $json[$key]['Department'][$department]['Remain'] = $department_key['Remain'];
                        if(is_array($department_key['Month']) && count($department_key['Month'])>0){
                            foreach($department_key['Month'] as $key1=>$value1){
                                $temptotal = isset($json[$key]['Department'][$department]['Month'][$key1]['Total']) ? $json[$key]['Department'][$department]['Month'][$key1]['Total'] : 0 ;
                                $tempremain = isset($json[$key]['Department'][$department]['Month'][$key1]['Remain']) ? $json[$key]['Department'][$department]['Month'][$key1]['Remain'] : $temptotal ;
                                $json[$key]['Department'][$department]['Month'][$key1]['Remain'] = $value1['Total']-$temptotal+$tempremain;
                                $json[$key]['Department'][$department]['Month'][$key1]['Total'] = $value1['Total'];
                            }
                        }
                    }
                }
                $json = json_encode($json);
                $sql = array(
                    'DataDepartment'=>$json
                );
                $this->db->where("ID",$check->ID);
                $this->db->update("ttp_report_targets_year",$sql);
            }
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH."/report/import_targets" ;
        redirect($refer);
    }

    public function byteam(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Import Targets');
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $team = $this->session->userdata("import_targets_team")!='' ? (int)$this->session->userdata("import_targets_team") : 0 ;
        $targets = $this->session->userdata("import_targets_targets")!='' ? (int)$this->session->userdata("import_targets_targets") : 0 ;
        $result = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
        $json = $result ? json_decode($result->DataDepartment,true) : array() ;
        $json1 = $result ? json_decode($result->DataTeam,true) : array() ;
        $data = array(
            'base_link'         => base_url().ADMINPATH.'/report/import_targets/',
            'datadepartment'    => $json,
            'datateam'          => $json1,
            'current_team'      => $team,
            'current_targets'   => $targets,
            'current_year'      => $year,
            'current_department'=> $department
        );
        $view = $this->user->IsAdmin==1 ? 'admin/import_targets_team' : 'admin/import_targets_team' ;
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function save_chitieu_team(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $data = isset($_POST['year']) ? $_POST['year'] : array() ;
        if(count($data)>0){
            $check = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
            if($check){
                $json = json_decode($check->DataDepartment,true);
                foreach($data as $key=>$value){
                    if(isset($value['Department'][$department])){
                        $department_key = $value['Department'][$department];
                        if(isset($department_key['Month'])){
                            for($i=1;$i<13;$i++){
                                if(isset($department_key['Month'][$i]['Remain'])){
                                    $json[$key]['Department'][$department]['Month'][$i]['Remain'] = $department_key['Month'][$i]['Remain'];
                                    if(is_array($department_key['Month'][$i]['Team']) && count($department_key['Month'][$i]['Team'])>0){
                                        foreach($department_key['Month'][$i]['Team'] as $key1=>$value1){
                                            $temptotal = isset($json[$key]['Department'][$department]['Month'][$i]['Team'][$key1]['Total']) ? $json[$key]['Department'][$department]['Month'][$i]['Team'][$key1]['Total'] : 0 ;
                                            $tempremain = isset($json[$key]['Department'][$department]['Month'][$i]['Team'][$key1]['Remain']) ? $json[$key]['Department'][$department]['Month'][$i]['Team'][$key1]['Remain'] : $temptotal ;
                                            $tempapply = $temptotal - $tempremain;
                                            $json[$key]['Department'][$department]['Month'][$i]['Team'][$key1]['Total'] = $value1['Total'];
                                            $json[$key]['Department'][$department]['Month'][$i]['Team'][$key1]['Remain'] = $value1['Total']-$temptotal+$tempremain;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $json = json_encode($json);
                $sql = array(
                    'DataDepartment'=>$json
                );
                $this->db->where("ID",$check->ID);
                $this->db->update("ttp_report_targets_year",$sql);
            }
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH."/report/import_targets" ;
        redirect($refer);
    }

    public function byperson(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Import Targets');
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $team = $this->session->userdata("import_targets_team")!='' ? (int)$this->session->userdata("import_targets_team") : 0 ;
        $targets = $this->session->userdata("import_targets_targets")!='' ? (int)$this->session->userdata("import_targets_targets") : 0 ;
        $result = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
        $json = $result ? json_decode($result->DataDepartment,true) : array() ;
        $json1 = $result ? json_decode($result->DataTeam,true) : array() ;
        $data = array(
            'base_link'         => base_url().ADMINPATH.'/report/import_targets/',
            'datadepartment'    => $json,
            'datateam'          => $json1,
            'current_team'      => $team,
            'current_targets'   => $targets,
            'current_year'      => $year,
            'current_department'=> $department
        );
        $view = $this->user->IsAdmin==1 ? 'admin/import_targets_person' : 'admin/import_targets_person' ;
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function save_chitieu_person(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $year = $this->session->userdata("import_targets_year")!='' ? (int)$this->session->userdata("import_targets_year") : date('Y',time()) ;
        $department = $this->session->userdata("import_targets_department")!='' ? (int)$this->session->userdata("import_targets_department") : 0 ;
        $team = $this->session->userdata("import_targets_team")!='' ? (int)$this->session->userdata("import_targets_team") : 0 ;
        $data = isset($_POST['year']) ? $_POST['year'] : array() ;
        if(count($data)>0){
            $check = $this->db->query("select * from ttp_report_targets_year where Year=$year")->row();
            if($check){
                $json = json_decode($check->DataDepartment,true);
                foreach($data as $key=>$value){
                    if(isset($value['Department'][$department])){
                        $department_key = $value['Department'][$department];
                        if(isset($department_key['Month'])){
                            for($i=1;$i<13;$i++){
                                if(isset($department_key['Month'][$i]['Team'][$team]['Remain']) && isset($department_key['Month'][$i]['Team'][$team]['Person'])){
                                    $json[$key]['Department'][$department]['Month'][$i]['Team'][$team]['Remain'] = $department_key['Month'][$i]['Team'][$team]['Remain'];
                                    $json[$key]['Department'][$department]['Month'][$i]['Team'][$team]['Person'] = $department_key['Month'][$i]['Team'][$team]['Person'];
                                }
                            }
                        }
                    }
                }
                $json = json_encode($json);
                $sql = array(
                    'DataDepartment'=>$json
                );
                $this->db->where("ID",$check->ID);
                $this->db->update("ttp_report_targets_year",$sql);
            }
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH."/report/import_targets" ;
        redirect($refer);
    }

    public function add_team(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $user = isset($_POST['User']) ? $_POST['User'] : '' ;
        $department = isset($_POST['Department']) ? $_POST['Department'] : '' ;
        $listuser = isset($_POST['Listuser']) ? $_POST['Listuser'] : '' ;
        $listuser = explode(",",$listuser);
        $listuser = json_encode($listuser);
        if($title!='' && $user!='' && $department!=''){
            $data = array(
                'Data'      =>$listuser,
                'Title'     =>$title,
                'Department'=>$department,
                'UserID'    =>$user,
                'Published' =>1,
                'Created'   =>date('Y-m-d H:i:s',time())
            );
            $this->db->insert("ttp_report_team",$data);
            echo "true";
            return;
        }
        echo "false";
    }

    public function add_person(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : '' ;
        $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : '' ;
        $username = isset($_POST['username']) ? $_POST['username'] : '' ;
        $password = isset($_POST['password']) ? $_POST['password'] : '' ;
        $email = isset($_POST['email']) ? $_POST['email'] : '' ;
        $TeamID = isset($_POST['TeamID']) ? $_POST['TeamID'] : 0 ;
        if($firstname!='' && $lastname!='' && $username!='' && $password!=''){
            $check = $this->db->query("select ID from ttp_user where UserName='$username'")->row();
            if(!$check){
                $data = array(
                    'UserName'  =>$username,
                    'Password'  =>sha1($password),
                    'FirstName' =>$firstname,
                    'LastName'  =>$lastname,
                    'Email'     =>$email,
                    'RoleID'    =>4,
                    'DetailRole'=>'{"profile":["r","w","m","d"],"import":["r","w","m","d"],"import_order":["r","w","m","d"]}',
                    'UserType'  =>1,
                    'Created'   =>date('Y-m-d H:i:s',time()),
                    'LastEdited'=>date('Y-m-d H:i:s',time())
                );
                $this->db->insert("ttp_user",$data);
                $UserID = $this->db->insert_id();
                $team = $this->db->query("select Data from ttp_report_team where ID=$TeamID")->row();
                if($team){
                    $json = json_decode($team->Data,true);
                    $json[] = $UserID;
                    $json = json_encode($json);
                    $this->db->query("update ttp_report_team set Data='$json' where ID=$TeamID");
                }
                echo "true";
                return;
            }
            echo "false1";
            return;
        }
        echo "false";
    }

    public function add_year(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $year = isset($_POST['Year']) ? $_POST['Year'] : '' ;
        if($year=='ok'){
            $check = $this->db->query("select max(Year) as newyear from ttp_report_targets_year")->row();
            $year = $check ? $check->newyear+1 : date('Y',time()) ;
            $data = array(
                'Year'          =>$year,
                'DataDepartment'=>''
            );
            $this->db->insert("ttp_report_targets_year",$data);
            echo "true";
            return;
        }
        echo "false";
    }
}
?>