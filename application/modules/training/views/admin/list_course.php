<?php 
$url = base_url().ADMINPATH.'/training';
?>
<form action="" id="form_search" method='post'>
<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<select name="groupid" id="groupid" onchange="searchs()" class="form-control">
					<option value="">-- Lọc theo nhóm --</option>
					<?php foreach ($group as $key => $value) {
						$sl='';
						if($groupid ==$value["id"]){
							$sl = 'selected = "selected"';
						}
						?>
						<option <?php echo $sl ?> value="<?php echo $value["id"] ?>"><?php echo $value["title"]; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="block2">
				<a href="<?php echo $url;?>/training/add_course" class="btn btn-danger"><i class="fa fa-plus"></i> Tạo khóa học</a>
			</div>
		</div>

		<div class="table_data">
		    <table>
		    	<tr>
		    		<th>STT</th>
		    		<th>Tên khóa học</th>
		    		<th>Nhóm khóa học</th>
		    		<th>Cấp độ</th>
		    		<th class="th_action">Thao tác</th>
		    	</tr>
		    	<?php
		    	$stt =0;
		    	 foreach ($list as $key => $value) {

		    		?>
		    		<tr>
		    		<td> <?php echo ++$stt; ?></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course/<?php echo $value["id"];?>"><?php echo $value["title"]; ?></a></td>
		    		<td> <?php echo $name_group[$value["groupid"]]; ?></td>
		    		<td> <?php echo $name_level[$value["level"]]; ?></td>
		    		<td> <a href="<?php echo $url;?>/training/add_course/<?php echo $value["id"];?>"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa </a>&nbsp;<a onclick="del(<?php echo $value["id"];?>);return false;"><i class="fa fa-trash-o"> </i> Xóa </a> </td>
		    		</tr>
		    	<?php } ?>
		    </table>
		</div>
	</div>
</div>
<input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
</form>
<script>
function del(id){
	var result = confirm('Bạn có chắc chắn muốn xóa không');
	if(result){
		var link = $('#link').val();
        $.post(link+"/course/delete_course/"+id, $("form").serialize(),function(resp){
        	alert('Xóa thành công');
        	location.reload();
        });
	}
}
function searchs(){
	$('#form_search').submit();
}
    function welcome(){
        $(".welcome_title").addClass("animated");
        $(".welcome_title").addClass("fadeInDown");
    }

    setTimeout(welcome,500);
</script>
<style>
	.body_content .containner{min-height: 569px !important;}
	
</style>