<div class="row">
	<div class="form-group">
		<div class="col-xs-12">
			<label class="control-label col-xs-4">Mã số lô</label>
			<div class="col-xs-7">
				<input type="text" class="ShipmentCode form-control" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<div class="col-xs-12">
			<label class="control-label col-xs-4">Tên sản phẩm</label>
			<div class="col-xs-7">
				<input type="text" class="ProductsName form-control" value='<?php echo $data->Title ?>' readonly="true" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<div class="col-xs-12">
			<label class="control-label col-xs-4">Ngày sản xuất</label>
			<div class="col-xs-7">
				<input type="text" class="DateProduction form-control"  />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<div class="col-xs-12">
			<label class="control-label col-xs-4">Hạn sử dụng</label>
			<div class="col-xs-7">
				<input type="text" class="DateExpiration form-control" />
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="form-group">
		<div class="col-xs-12">
			<label class="control-label col-xs-4"></label>
			<div class="col-xs-7"><a class='btn btn-primary' onclick="save_shipment(this,<?php echo $data->ID ?>)"><i class="fa fa-check-square-o" aria-hidden="true"></i> Lưu dữ liệu</a></div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function () {
        $('.DateProduction,.DateExpiration').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
            showDropdowns: true
        });
    });

</script>