<?php 
class Manager_transport extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="manager_transport";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_transport")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_transport order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_transport/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_transport/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Transport | Manager Report Tools');
		$this->template->write_view('content','admin/manager_transport_home',$data);
		$this->template->render();
	}

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Transport add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_transport/'
        );
        $this->template->write_view('content','admin/manager_transport_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
        $user = isset($_POST['user']) ? $_POST['user'] : array() ;
        if($Title!='' && $Code!=''){
            $data = array(
                'Title' => $Title,
                'Code'  => $Code
            );
            $this->db->insert("ttp_report_transport",$data);
            $id = $this->db->insert_id();
            if(count($user)>0){
                $sql = "";
                foreach($user as $row){
                    $sql[] = "($id,$row)";
                }
                $sql = "insert into ttp_user_transport(TransportID,UserID) values".implode(',',$sql);
                $this->db->query($sql);
            }
        }
        redirect(ADMINPATH.'/report/manager_transport/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_transport where ID=$id");
            $this->db->query("delete from ttp_user_transport where TransportID=$id");
        }
        redirect(ADMINPATH.'/report/manager_transport/');
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_transport where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Transport | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_transport/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_transport_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
        $user = isset($_POST['user']) ? $_POST['user'] : array() ;
        if($Title!=''){
            $data = array(
                'Title' => $Title,
                'Code'  => $Code
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_transport",$data);
            if(count($user)>0){
                $sql = "";
                foreach($user as $row){
                    $sql[] = "($ID,$row)";
                }
                $sql = "insert into ttp_user_transport(TransportID,UserID) values".implode(',',$sql);
                $this->db->query("delete from ttp_user_transport where TransportID=$ID");
                $this->db->query($sql);
            }
        }
        redirect(ADMINPATH.'/report/manager_transport/');
    }
}
?>
