<div class="box_inner" style="width: 1050px;">
	<div class="block1_inner"><h1>Khách hàng với tần suất mua hàng : <b style="color:#F00"><?php echo $num.' lần' ?></b> trong khoản từ <b style="color:#F00"><?php echo date('d/m/Y',strtotime($startday)).'</b> - <b style="color:#F00">'.date('d/m/Y',strtotime($stopday)) ?></b></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
	<?php 
    $arr_buy[$num] = array('SLDH'=>0,'PRICE'=>0,'OLD'=>0,'NEW'=>0,'ORDER'=>0);
    
    $arr_total = array('CUSTOMER'=>array(),'ORDER'=>0,'PRICE'=>0);
    $arr_type_customer = array('new'=>0,'old'=>0);
    $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#EEEEEE","#EEEEEE","#EEEEEE","#EEEEEE");
    $arr_chart = array();
    $arr_source = array();
    $arr_area = array();
    $arr_channel = array();
    $arr_customer_type = array('NEW'=>0,'OLD'=>0,'TOTAL'=>0);
	$totalorder = 0;
    if(count($data)>0){
        foreach($data as $row){
            $totalorder = $totalorder+1;
            $total = $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
			
            $arr_total['ORDER'] = $arr_total['ORDER']+1;
            $arr_total['PRICE'] = $arr_total['PRICE']+$total;
            if(!isset($arr_total['CUSTOMER'][$row->CustomerID])){
                $arr_total['CUSTOMER'][$row->CustomerID]['NAME'] = $row->Name;
                $arr_total['CUSTOMER'][$row->CustomerID]['DH']=1;
                $arr_total['CUSTOMER'][$row->CustomerID]['PRICE']=$total;
                $arr_total['CUSTOMER'][$row->CustomerID]['AreaID']=$row->AreaID;
                $arr_total['CUSTOMER'][$row->CustomerID]['KenhbanhangID']=$row->KenhbanhangID;
                $arr_total['CUSTOMER'][$row->CustomerID]['SourceID']=$row->SourceID;
            }else{
                $arr_total['CUSTOMER'][$row->CustomerID]['DH'] = $arr_total['CUSTOMER'][$row->CustomerID]['DH']+1;
                $arr_total['CUSTOMER'][$row->CustomerID]['PRICE'] = $arr_total['CUSTOMER'][$row->CustomerID]['PRICE']+$total;
            }
            if($row->CustomerType==0){
                $arr_total['CUSTOMER'][$row->CustomerID]['NEW'] = 1;
            }else{
                $arr_total['CUSTOMER'][$row->CustomerID]['OLD'] = 1;
            }
            $arr_total['CUSTOMER'][$row->CustomerID]['Type'] = $row->CustomerType;
        }
    }


    $start = $page==1 ? 0 : ($page*30)+1;
    $totalcustomer = count($arr_total['CUSTOMER']);
    $totalcustomer_num = 0;
    $identity = 0;
    if($totalcustomer>0){
    	$data_customer = array();
        foreach($arr_total['CUSTOMER'] as $key=>$row){
        	if(!isset($row['OLD'])){
                $row['OLD']=0;
            }
            if(!isset($row['NEW'])){
                $row['NEW']=0;
            }
            if($row['DH']==$num){
            	if($num>1){
            		$arr_customer_type['OLD'] = $arr_customer_type['OLD']+$row['OLD'];
            		$arr_customer_type['TOTAL'] = $arr_customer_type['OLD'];
                	$arr_customer_type['NEW'] = 0;
            	}else{
            		$arr_customer_type['OLD'] = $arr_customer_type['OLD']+$row['OLD'];
	                $arr_customer_type['NEW'] = $arr_customer_type['NEW']+$row['NEW'];
	                $arr_customer_type['TOTAL'] = $arr_customer_type['OLD']+$arr_customer_type['NEW'];
            	}

            	/* Source data */
			    if(isset($arr_source[$row['SourceID']])){
			        $arr_source[$row['SourceID']] = $arr_source[$row['SourceID']]+1;
			    }else{
			        $arr_source[$row['SourceID']] = 1;
			    }

			    /* Area data */
			    if(isset($arr_area[$row['AreaID']])){
			        $arr_area[$row['AreaID']] = $arr_area[$row['AreaID']]+1;
			    }else{
			        $arr_area[$row['AreaID']] = 1;
			    }

			    /* Area data */
			    if(isset($arr_channel[$row['KenhbanhangID']])){
			        $arr_channel[$row['KenhbanhangID']] = $arr_channel[$row['KenhbanhangID']]+1;
			    }else{
			        $arr_channel[$row['KenhbanhangID']] = 1;
			    }

			    if($identity>=$start && $identity<=($start+30)){
			    	$data_customer[$key] = $row;
			    }
			    $identity++;
            }
        }
	}
    
    $numpage = ($identity-1)/30;

    ?>
	<div class="block2_inner">
		<div class="row">
			<div class="col-xs-12">
				<h4 style="border-bottom: 1px solid #E1e1e1;padding-bottom: 10px;"><a onclick="showpiepopup(this,3)" class='field_pie current'>Khu vực bán hàng</a> / <a onclick="showpiepopup(this,4)" class='field_pie'>Nguồn khách hàng</a> / <a onclick="showpiepopup(this,6)" class='field_pie'>Kênh bán hàng</a> / <a onclick="showpiepopup(this,7)" class='field_pie'>Loại khách hàng</a><h4>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<canvas id="canvas_pie3"></canvas>
            	<canvas id="canvas_pie4"></canvas>
            	<canvas id="canvas_pie6"></canvas>
            	<canvas id="canvas_pie7"></canvas>
			</div>
			<div class="col-xs-6">
				<table class="table table-hover table3">
	                <tr>
	                    <th></th>
	                    <th style="width: 200px;">Khu vực</th>
	                    <th>Số lượng KH</th>
	                    <th>Tỷ lệ</th>
	                </tr>
	                <?php 
	                $area = $this->db->query("select * from ttp_report_area")->result();
	                $arr_chart1 = array();
	                if(count($area)>0){
	                    $i=0;
	                    $total_area = 0;
	                    foreach($area as $row){
	                        $color = "<span style='width:15px;height:15px;margin-right: 10px;float:left;background:".$array_color[$i]."'></span>";
	                        $data_area = isset($arr_area[$row->ID]) ? $arr_area[$row->ID] : 0 ;
	                        $total_area +=$data_area;
	                        $percent = $identity==0 ? 0 : round($data_area/($identity/100),1);
	                        echo "<tr>";
	                        echo "<td>$color</td>";
	                        echo "<td>$row->Title</td>";
	                        echo "<td>".number_format($data_area)."</td>";
	                        echo "<td>".$percent."%</td>";
	                        echo "</tr>";
	                        $arr_chart1[] = "{
	                                            value: ".$data_area.",
	                                            color: '".$array_color[$i]."',
	                                            highlight: '".$array_color[$i]."',
	                                            label: 'Khu vực $row->Title'
	                                        }";
	                        $i++;
	                    }
	                    $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
	                    $data_area = isset($arr_area[0]) ? $arr_area[0] : 0 ;
	                    $percent_area = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_area/(count($arr_total['CUSTOMER'])/100),1);
	                    echo "<tr>";
	                    echo "<td>$color</td>";
	                    echo "<td>Không xác định</td>";
	                    echo "<td>".number_format($data_area)."</td>";
	                    echo "<td>$percent_area %</td>";
	                    echo "</tr>";
	                    $arr_chart1[] = "{
	                                        value: ".$data_area.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: 'Khu vực $row->Title'
	                                    }";
	                    echo "<tr><th></th><th>TỔNG CỘNG</th><th>".number_format($total_area)."</th><th>100%</th></tr>";
	                }
	                ?>
	            </table>
	    
	            <table class="table table-hover table4">
	                <tr>
	                    <th></th>
	                    <th style="width: 200px;">Nguồn khách hàng</th>
	                    <th>Số lượng KH</th>
	                    <th>Tỷ lệ</th>
	                </tr>
	                <?php 
	                $source = $this->db->query("select * from ttp_report_source")->result();
	                $arr_chart2 = array();
	                if(count($source)>0){
	                    $i=0;
	                    $total_source = 0;
	                    foreach($source as $row){
	                        $color = "<span style='width:15px;height:15px;margin-right: 10px;float:left;background:".$array_color[$i]."'></span>";
	                        $data_source = isset($arr_source[$row->ID]) ? $arr_source[$row->ID] : 0 ;
	                        $total_source +=$data_source; 
	                        $percent = $identity==0 ? 0 : round($data_source/($identity/100),1);
	                        echo "<tr>";
	                        echo "<td>$color</td>";
	                        echo "<td>$row->Title</td>";
	                        echo "<td>".number_format($data_source)."</td>";
	                        echo "<td>".$percent."%</td>";
	                        echo "</tr>";
	                        $arr_chart2[] = "{
	                                            value: ".$data_source.",
	                                            color: '".$array_color[$i]."',
	                                            highlight: '".$array_color[$i]."',
	                                            label: 'Nguồn từ $row->Title'
	                                        }";
	                        $i++;
	                    }
	                    $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
	                    $data_source = isset($arr_source[0]) ? $arr_source[0] : 0 ;
	                    $percent_source = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_source/(count($arr_total['CUSTOMER'])/100),1);
	                    echo "<tr>";
	                    echo "<td>$color</td>";
	                    echo "<td>Không xác định</td>";
	                    echo "<td>".number_format($data_source)."</td>";
	                    echo "<td>$percent_source %</td>";
	                    echo "</tr>";
	                    $arr_chart2[] = "{
	                                        value: ".$data_source.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: 'Nguồn từ $row->Title'
	                                    }";
	                    echo "<tr><th></th><th>TỔNG CỘNG</th><th>".number_format($total_source)."</th><th>100%</th></tr>";
	                }
	                ?>
	            </table>

	            <table class="table table-hover table6">
	                <tr>
	                    <th></th>
	                    <th style="width: 200px;">Kênh bán hàng</th>
	                    <th>Số lượng KH</th>
	                    <th>Tỷ lệ</th>
	                </tr>
	                <?php 
	                $channel = $this->db->query("select * from ttp_report_saleschannel")->result();
	                $arr_chart3 = array();
	                if(count($channel)>0){
	                    $i=0;
	                    $total_channel = 0;
	                    foreach($channel as $row){
	                        $color = "<span style='width:15px;height:15px;margin-right: 10px;float:left;background:".$array_color[$i]."'></span>";
	                        $data_channel = isset($arr_channel[$row->ID]) ? $arr_channel[$row->ID] : 0 ;
	                        $total_channel +=$data_channel; 
	                        $percent = $identity==0 ? 0 : round($data_channel/($identity/100),1);
	                        echo "<tr>";
	                        echo "<td>$color</td>";
	                        echo "<td>$row->Title</td>";
	                        echo "<td>".number_format($data_channel)."</td>";
	                        echo "<td>".$percent."%</td>";
	                        echo "</tr>";
	                        $arr_chart3[] = "{
	                                            value: ".$data_channel.",
	                                            color: '".$array_color[$i]."',
	                                            highlight: '".$array_color[$i]."',
	                                            label: 'Nguồn từ $row->Title'
	                                        }";
	                        $i++;
	                    }
	                    $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
	                    $data_channel = isset($arr_channel[0]) ? $arr_channel[0] : 0 ;
	                    $percent_source = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_channel/(count($arr_total['CUSTOMER'])/100),1);
	                    echo "<tr>";
	                    echo "<td>$color</td>";
	                    echo "<td>Không xác định</td>";
	                    echo "<td>".number_format($data_channel)."</td>";
	                    echo "<td>$percent_source %</td>";
	                    echo "</tr>";
	                    $arr_chart3[] = "{
	                                        value: ".$data_channel.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: 'Kênh bán hàng $row->Title'
	                                    }";
	                    echo "<tr><th></th><th>TỔNG CỘNG</th><th>".number_format($total_channel)."</th><th>100%</th></tr>";
	                }
	                ?>
	            </table>

	            <table class="table table-hover table7">
	                <tr>
	                    <th></th>
	                    <th style="width: 200px;">Loại khách hàng</th>
	                    <th>Số lượng KH</th>
	                    <th>Tỷ lệ</th>
	                </tr>
	                <?php 
	                $arr_chart4 = array();
	                if(count($arr_customer_type)>0){
	                    $i=0;
	                    $arr_name = array("NEW"=>"Khách hàng mới","OLD"=>"Khách hàng cũ");
	                    foreach($arr_customer_type as $key=>$row){
	                        if(array_key_exists($key,$arr_name)){
		                        $color = "<span style='width:15px;height:15px;margin-right: 10px;float:left;background:".$array_color[$i]."'></span>";
		                        $percent = $arr_customer_type['TOTAL']==0 ? 0 : round($row/($arr_customer_type['TOTAL']/100),1);
		                        echo "<tr>";
		                        echo "<td>$color</td>";
		                        echo "<td>".$arr_name[$key]."</td>";
		                        echo "<td>".number_format($row)."</td>";
		                        echo "<td>".$percent."%</td>";
		                        echo "</tr>";
		                        $arr_chart4[] = "{
		                                            value: ".$row.",
		                                            color: '".$array_color[$i]."',
		                                            highlight: '".$array_color[$i]."',
		                                            label: '".$arr_name[$key]."'
		                                        }";
		                        $i++;
	                        }
	                    }
	                    echo "<tr><th></th><th>TỔNG CỘNG</th><th>".number_format($arr_customer_type['TOTAL'])."</th><th>100%</th></tr>";
	                }
	                ?>
	            </table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h4 style="border-bottom: 1px solid #E1e1e1;padding-bottom: 10px;">Danh sách khách hàng<h4>
			</div>
		</div>
		<table class="table table-hover">
            <?php 
			
			if(count($data_customer)>0){
				echo "<div class='row' style='border-bottom:1px solid #eee;margin:0px'><div class='form-group'><div class='col-xs-3'><b>Tên khách hàng</b></div>";
	            echo "<div class='col-xs-3'><b>Tổng giá trị</b></div>";
	            echo "<div class='col-xs-2'><b>Giá trị trung bình</b></div>";
	            echo "<div class='col-xs-2'><b>Loại khách hàng</b></div>";
	            echo "<div class='col-xs-2'><b>Thao tác</b></div></div></div>";
		        foreach($data_customer as $key=>$row){
		            if($row['DH']==$num){
		                $arr_buy[$num]['ORDER'] = $arr_buy[$num]['ORDER']+$row['DH'];
		                $arr_buy[$num]['SLDH'] = $arr_buy[$num]['SLDH']+1;
		                $arr_buy[$num]['PRICE'] = $arr_buy[$num]['PRICE']+$row['PRICE'];
		                $abs = $row['PRICE']/$row['DH'];
		                $type = $row['Type']==0 ? "Mới" : "Cũ" ;
		                echo "<div class='row' style='border-bottom:1px solid #eee;margin:0px'><div class='form-group'>";
						echo "<div class='col-xs-3'><a title='".$row['NAME']."'>".$row['NAME']."</a></div>";
						echo "<div class='col-xs-3'>".number_format($row['PRICE'])."</div>";
						echo "<div class='col-xs-2'>".number_format($abs)."</div>";
						echo "<div class='col-xs-2'>$type</div>";
						echo "<div class='col-xs-2'><a onclick='show_list_order(this,$key)'>Xem đơn hàng <i style='margin-left:5px' class='fa fa-sort-desc' aria-hidden='true'></i> </a></div>";
						echo "</div><div class='form-group showlisthere'></div></div>";
		            }
		        }
		    }

    		?>
		<nav>
			<?php
			$temp = round($numpage) ;
			if($temp>1){
				echo "<ul class='pagination'>";
				$numpage = round($numpage);
				echo ($page-5)>1 ? "<li><a onclick='load_page_nav($num,1)'>« First</a></li>" : "" ;
				for ($i=1; $i < $temp; $i++) { 
					if($i>=($page-5) && $i<=($page+5)){
						$active = $i==$page ? "active" : "" ;
						echo "<li class='$active'><a onclick='load_page_nav($num,$i)'>$i</a></li>";
					}
				}
				echo ($page+5)<$numpage ? "<li><a onclick='load_page_nav($num,$numpage)'>Last »</a></li>" : "" ;
				echo "</ul>";
			}
			?>
		</nav>
	</div>
</div>
<style>
	.body_content .containner .black .box_inner .block2_inner{padding-bottom:0px;}
	.body_content .containner .black .box_inner .block2_inner table tr td{padding:3px 0px;line-height:20px;}
	.body_content .containner .black .box_inner .block2_inner table tr th{padding:5px 0px;text-transform:uppercase;}
	#canvas_pie4{display:none;}
	#canvas_pie6{display:none;}
	#canvas_pie7{display:none;}
	.table4{display:none;}
	.table6{display:none;}
	.table7{display:none;}
	.body_content .containner .black .box_inner .block2_inner table tr:hover td:first-child{border-left:none;}
	.col-xs-3{white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
	.row .showlisthere .col-xs-3,.row .showlisthere .col-xs-2,.row .showlisthere .col-xs-1{
		padding: 5px 2px;
	    border-bottom: 1px solid #b2d6bf;
	    background: #e1ffec;
	    white-space: nowrap;
	    overflow: hidden;
	    text-overflow: ellipsis;
	}
</style>
<script>
	$("#close_overlay").click(function(){
        $(".over_lay").hide();
    });

    var sharePiePolorDoughnutData3 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];

    var sharePiePolorDoughnutData4 = [
        <?php 
        echo implode(',', $arr_chart2);
        ?>
    ];

    var sharePiePolorDoughnutData6 = [
        <?php 
        echo implode(',', $arr_chart3);
        ?>
    ];

    var sharePiePolorDoughnutData7 = [
        <?php 
        echo implode(',', $arr_chart4);
        ?>
    ];

    window.myPie = new Chart(document.getElementById("canvas_pie3").getContext("2d")).Pie(sharePiePolorDoughnutData3, {
        responsive: true,
        tooltipFillColor: "rgba(51, 51, 51, 0.55)"
    });

    window.myPie = new Chart(document.getElementById("canvas_pie4").getContext("2d")).Pie(sharePiePolorDoughnutData4, {
        responsive: true,
        tooltipFillColor: "rgba(51, 51, 51, 0.55)"
    });

    window.myPie = new Chart(document.getElementById("canvas_pie6").getContext("2d")).Pie(sharePiePolorDoughnutData6, {
        responsive: true,
        tooltipFillColor: "rgba(51, 51, 51, 0.55)"
    });

    window.myPie = new Chart(document.getElementById("canvas_pie7").getContext("2d")).Pie(sharePiePolorDoughnutData7, {
        responsive: true,
        tooltipFillColor: "rgba(51, 51, 51, 0.55)"
    });

    function showpiepopup(ob,num){
		$(".field_pie").removeClass("current");
        $(ob).addClass("current");
        $("#canvas_pie3").hide();
        $("#canvas_pie4").hide();
        $("#canvas_pie6").hide();
        $("#canvas_pie7").hide();
        $(".table3").hide();
        $(".table4").hide();
        $(".table6").hide();
        $(".table7").hide();
        $("#canvas_pie"+num).show();
        $(".table"+num).show();
    }

    h = window.innerHeight;
	h = h-100;
	$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
</script>