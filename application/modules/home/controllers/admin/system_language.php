<?php

class System_language extends Admin_Controller
{

    public $user;
    public $classname = "system_language";
    public $limit = 20;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index()
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('Languages | Global');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_languages")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_languages $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/system_language/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/system_language/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/system_language_home', $data);
        $this->template->render();
    }

    public function detail()
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $langid = $this->uri->segment(5);
        $act = end($this->uri->segment_array());
        $act = (isset($act) && trim($act) == 'add') ? trim($act) : '';
        if ($act) {
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/system_language/detail/' . $langid . '/',
                'langid' => $langid
            );
            $this->template->write_view('content', 'admin/system_language_detail_add', $data);
        } else {
            $page = $this->uri->segment(6);
            $start = is_numeric($page) ? $page : 0;
            if (!is_numeric($start))
                $start = 0;
            $limit_str = "limit $start,$this->limit";
            $nav = $this->db->query("select count(*) as nav from ttp_languages_translate WHERE LangID = $langid")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_languages_translate WHERE LangID = $langid ORDER BY ID DESC $limit_str")->result();
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/system_language/detail/' . $langid . '/',
                'langid' => $langid,
                'data' => $object,
                'start' => $start,
                'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/system_language/detail/' . $langid . '/', 6, $nav, $this->limit)
            );
            $this->template->add_title('Languages | Global');
            $this->template->write_view('content', 'admin/system_language_detail', $data);
        }
        $this->template->render();
    }

    public function action()
    {
        $params = $_REQUEST['action'];
        switch ($params) {
            case 'duplicate':
                $this->duplicate($_REQUEST['params']);
                break;
            case 'save':
                $this->save($_REQUEST);
                break;
            case 'add':
                $this->add_tran($_REQUEST);
                break;
        }
    }

    public function add_tran($params)
    {
        $data = array(
            'Keyword' => $params['keyword'],
            'Translate' => $params['translate'],
            'LangID' => $params['langid']
        );
        if ($this->db->insert('ttp_languages_translate', $data)) {
            echo "OK";
        }
    }

    public function save($params)
    {
        $data = array(
            'Translate' => $params['value'],
        );
        $this->db->where('Keyword', $params['keyword']);
        $this->db->where('LangID', $params['id']);
        $this->db->update('ttp_languages_translate', $data);
    }

    public function duplicate($keyword)
    {
        $data = array(
            'Keyword' => $keyword,
            'LangID' => 1
        );
        $this->db->insert('ttp_languages_translate', $data);
    }

    public function search()
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        if (isset($_GET['keyword'])) {
            $keyword = $this->lib->fill_data($_GET['keyword']);
            $str = "select * from ttp_sitetree";
            $str = $keyword != '' ? $str . " where Title like '%" . urldecode($keyword) . "%' or Title like '%" . urldecode($keyword) . "%'" : $str;
            $this->template->add_title('Tìm kiếm dữ liệu');
            $data = array(
                'data' => $this->db->query($str)->result(),
                'nav' => '',
                'start' => 0,
                'base_link' => base_url() . ADMINPATH . '/home/system_module/',
            );
            $this->template->write_view('content', 'admin/system_module_home', $data);
            $this->template->render();
            return;
        }
        redirect(ADMINPATH . '/home/system_module/');
    }

    public function delete($id = 0)
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $this->db->query("delete from ttp_sitetree where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add()
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add Module | Config System');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/system_module/'
        );
        $this->template->write_view('content', 'admin/system_module_add', $data);
        $this->template->render();
    }

    public function addnew()
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Classname = isset($_POST['Classname']) ? $this->lib->fill_data($_POST['Classname']) : '';
        $Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : '';
        if ($Title != '' && $Classname != '' && $Status != '') {
            $data = array(
                'Classname' => $Classname,
                'Published' => $Status,
                'Title' => $Title
            );
            $this->db->insert("ttp_sitetree", $data);
        }
        redirect(ADMINPATH . '/home/system_module/');
    }

    public function edit($id = 0)
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_languages where LangID=$id")->row();
            if (!$result)
                show_error();
            $this->template->add_title('Edit Language | Config System');
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/system_language/',
                'data' => $result
            );
            $this->template->write_view('content', 'admin/system_language_home', $data);
            $this->template->render();
        }
    }

    public function update()
    {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Classname = isset($_POST['Classname']) ? $this->lib->fill_data($_POST['Classname']) : '';
        $Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : '';
        if ($Title != '' && $Classname != '' && $Status != '') {
            $data = array(
                'Classname' => $Classname,
                'Published' => $Status,
                'Title' => $Title
            );
            $this->db->where("ID", $ID);
            $this->db->update("ttp_sitetree", $data);
        }
        redirect(ADMINPATH . '/home/system_module/');
    }
}
