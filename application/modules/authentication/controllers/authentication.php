<?php

class Authentication extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('define_model');
        $app_conf = $this->define_model->getDefine();
        foreach ($app_conf as $row) {
            define("{$row['Keyword']}", $row['Value']);
        }
    }

    public function index() {
        $this->is_login();
        redirect(ADMINPATH . '/home');
    }

    public function is_login() {
        if (!$this->session->userdata('ttp_usercp')) {
            redirect(ADMINPATH . '/login');
        }
    }

    public function logout() {
        $this->session->unset_userdata('ttp_usercp');
        $this->session->sess_destroy();
        redirect('/removeip');
    }

    public function login() {
        $this->load->model('global_model', 'global');
        $yourip = $this->global->getYourIP();
        $referer = $this->global->getReferer();
        $u_agent = $this->global->getUserAgent();
        $type = $this->global->getType();
        $data = array(
            'yourip' => $yourip,
            'referer' => $referer,
            'u_agent' => $u_agent,
            'type' => $type
        );
        $this->load->library('user_agent');
        if ($this->agent->is_mobile()) {
            $this->load->view('m_login', $data);
        } else {
            $this->load->view('login', $data);
        }
    }

    public function send() {
        $this->load->model('global_model', 'global');
        $params = array_merge($_GET, $_POST);
        switch ($params['position']) {
            case 'verify':
                #UPDATE type = 1
                $this->global->addIP(1);
                echo '<span class="btn btn-warning">Wait to be accepted before you start</span>';
                break;
            case 'accept':
                #UPDATE type = 0
                $this->global->addIP(0, $params['ip']);
                echo '<i class="fa fa-check"></i> Done';
                break;
            case 'remove':
                #UPDATE type = 0
                $this->global->removeIP($params['ip']);
                break;
            default:
                break;
        }
    }

    public function login_now() {
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $u = $this->security->xss_clean($_POST['username']);
            $p = $this->security->xss_clean($_POST['password']);
            $this->load->model('verify_model');
            $result = $this->verify_model->verfiy(array($u, $p));
            if ($result) {
                $this->accept_ip($result->IsAdmin, $result->AllowRemote);

                $ip = $this->global_model->getYourIP();
                $tm = date("Y-m-d H:i:s");

                $newdata = array(
                    'ttp_usercp' => $u
                );
                $this->session->set_userdata($newdata);

                $data = array(
                    'session_id' => $this->session->userdata('session_id'),
                    'ttp_usercp' => $this->session->userdata('ttp_usercp'),
                    'ip' => $ip,
                    'tm' => $tm
                );
                $this->db->delete('ttp_plus_login', array('session_id' => $this->session->userdata('session_id')));
                $this->db->insert('ttp_plus_login', $data);

                if ($result->HomePage != '') {
                    redirect(ADMINPATH . "/" . $result->HomePage);
                } else {
                    redirect(ADMINPATH . "/home");
                }
            }
            redirect(ADMINPATH . '/login/');
        }
    }

    public function accept_ip($admin = 0, $AllowRemote = 0) {
        $detect = $this->global_model->troubleshoot();
        if ($detect) {
            if ($detect['accept'] == FALSE) {
                if ($admin == 1 && $AllowRemote == 1) {
                    return;
                }
                redirect(TOOLS_URL);
            }
        }
    }

}
