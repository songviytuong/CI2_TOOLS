<?php 
class Manager_system_menu extends Admin_Controller { 
 
    public $limit = 30;
    public $user;
    public $classname="manager_system_menu";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : "" ;
        $type = isset($_GET['type']) ? $_GET['type'] : 2 ;
        $bonus = " where 1=1 and Title like '%$keywords%'";
        $bonus .= $type==2 ? "" : " and Type=$type" ;
        $nav = $this->db->query("select count(1) as nav from ttp_system_menu $bonus")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_system_menu $bonus order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_system_menu/',
            'keywords'  =>  str_replace('+'," ",$keywords),
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_system_menu/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Menu config | Manager Report Tools');
        $this->template->write_view('content','admin/manager_system_menu_home',$data);
        $this->template->render();
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Add menu | Manager Report Tools');
        $this->template->write_view('content','admin/manager_system_menu_add');
        $this->template->render();
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $this->db->query("delete from ttp_system_menu where ID=$id");
        redirect(base_url().ADMINPATH.'/report/manager_system_menu/');
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_system_menu where ID=$id")->row();
        if($result){
            $data = array(
                'data'=>$result
            );
            $this->template->add_title('Edit menu | Manager Report Tools');
            $this->template->write_view('content','admin/manager_system_menu_edit',$data);
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Type = isset($_POST['Type']) ? $_POST['Type'] : '' ;
        $Published = isset($_POST['Published']) ? $_POST['Published'] : 0 ;
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : '' ;
        $Position = isset($_POST['Position']) ? $_POST['Position'] : 0 ;
        $ParentID = isset($_POST['ParentID']) ? $_POST['ParentID'] : 0 ;
        $ActiveLinks = isset($_POST['ActiveLinks']) ? $_POST['ActiveLinks'] : array() ;
        if($Title!=''){
            if($ParentID>0){
                $path = $this->db->query("select Path from ttp_system_menu where ID=$ParentID")->row();
                $path = $path ? $path->Path : 0 ;
            }else{
                $path = "0";
            }
            $max = $this->db->query("select max(ID) as ID from ttp_system_menu")->row();
            $max = $max ? $max->ID+1 : 1 ;
            $path = $path.'/'.$max;
            $data = array(
                'ID'        =>$max,
                'Path'      =>$path,
                'Title'     =>$Title,
                'Type'      =>$Type,
                'Published' =>$Published,
                'CurrentChecked' => json_encode($ActiveLinks),
                'Alias'     =>$Alias,
                'Position'  =>$Position,
                'ParentID'  =>$ParentID,
                'created'   =>date('Y-m-d H:i:s')
            );
            $this->db->insert('ttp_system_menu',$data);

            if(is_array($UserList)){
                $arr_sql = array();
                foreach($UserList as $row){
                    $arr_sql[] = "($max,$row)";
                }
                if(count($arr_sql)>0){
                    $arr_sql = implode(',',$arr_sql);
                    $sql = "insert into ttp_system_menu_user(MenuID,UserID) values".$arr_sql;
                    $this->db->query($sql);
                }
            }
        }
        redirect(base_url().ADMINPATH.'/report/manager_system_menu/');
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : $ID ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Type = isset($_POST['Type']) ? $_POST['Type'] : '' ;
        $Published = isset($_POST['Published']) ? $_POST['Published'] : 0 ;
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : '' ;
        $Position = isset($_POST['Position']) ? $_POST['Position'] : 0 ;
        $ParentID = isset($_POST['ParentID']) ? $_POST['ParentID'] : 0 ;
        $UserList = isset($_POST['UserList']) ? $_POST['UserList'] : array() ;
        $Ischange = isset($_POST['Ischange']) ? $_POST['Ischange'] : 0 ;
        $ActiveLinks = isset($_POST['ActiveLinks']) ? $_POST['ActiveLinks'] : array() ;
        if($Title!=''){
            if($ParentID>0){
                $path = $this->db->query("select Path from ttp_system_menu where ID=$ParentID")->row();
                $path = $path ? $path->Path : 0 ;
            }else{
                $path = "0";
            }
            $path = $path.'/'.$ID;
            $data = array(
                'Title'     =>$Title,
                'Path'      =>$path,
                'Type'      =>$Type,
                'Published' =>$Published,
                'Alias'     =>$Alias,
                'Position'  =>$Position,
                'CurrentChecked' => json_encode($ActiveLinks),
                'ParentID'  =>$ParentID
            );
            $this->db->where("ID",$ID);
            $this->db->update('ttp_system_menu',$data);
            if(is_array($UserList)){
                if($Ischange==1){
                    $this->db->query("delete from ttp_system_menu_user where MenuID=$ID");
                    $arr_sql = array();
                    foreach($UserList as $row){
                        $arr_sql[] = "($ID,$row)";
                    }
                    if(count($arr_sql)>0){
                        $arr_sql = implode(',',$arr_sql);
                        $sql = "insert into ttp_system_menu_user(MenuID,UserID) values".$arr_sql;
                        $this->db->query($sql);
                    }
                }
            }
        }
        redirect(base_url().ADMINPATH.'/report/manager_system_menu/');
    }

}
?>