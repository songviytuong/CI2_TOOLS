<?php
class Import_customer extends Admin_Controller {

    public $limit = 30;
    public $user;
    public $classname="import_customer";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/import_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public function change_state_prize(){
        $state = isset($_POST['state']) ? $_POST['state'] : 0 ;
        $id = isset($_POST['id']) ? $_POST['id'] : 0 ;
        $arr = array('error'=>true,'message'=>'Không thể thay đổi trạng thái này !');
        if($id){
            $result = $this->db->query("select * from ttp_result_luckydraw where ID=$id")->row();
            if($result){
                if($result->Recive==0){
                    $this->db->query("update ttp_result_luckydraw set Recive=1 where ID=$id");
                    $arr = array('error'=>false,'message'=>'');
                }
            }
        }
        echo json_encode($arr);
    }

    public function add_note_luckynumber(){
        $data = isset($_POST['Data']) ? json_decode($_POST['Data']) : (object)array() ;
        $Note = isset($data->Note) ? $data->Note : '' ;
        $ID = isset($data->ID) ? (int)$data->ID : 0 ;
        $respone = array('error'=>0,'message'=>'');
        $sql = array(
            "Note"  => $Note
        );
        $this->db->where("ID",$ID);
        $this->db->update("ttp_event_lucky_number",$sql);
        echo json_encode($respone);
    }

    public function add_note_callme(){
        $data = isset($_POST['Data']) ? json_decode($_POST['Data']) : (object)array() ;
        $Note = isset($data->Note) ? $data->Note : '' ;
        $ID = isset($data->ID) ? (int)$data->ID : 0 ;
        $respone = array('error'=>0,'message'=>'');
        $sql = array(
            "Response"  => $Note
        );
        $this->db->where("ID",$ID);
        $this->db->update("ttp_report_customer_callme",$sql);
        echo json_encode($respone);
    }

    public function add_note_comments(){
        $data = isset($_POST['Data']) ? json_decode($_POST['Data']) : (object)array() ;
        $Note = isset($data->Note) ? $data->Note : '' ;
        $ID = isset($data->ID) ? (int)$data->ID : 0 ;
        $respone = array('error'=>0,'message'=>'');
        $sql = array(
            "Note"  => $Note
        );
        $this->db->where("ID",$ID);
        $this->db->update("ttp_funels_comments",$sql);
        echo json_encode($respone);
    }

    public function edit_question($id=0){
        $result = $this->db->query("select * from ttp_event_quiz_question where ID=$id")->row();
        $data = array(
            'data'  => $result
        );
        $this->load->view('import_customer_quiz_question_edit',$data);
    }

    public function new_question($id=0){
        $this->load->view('import_customer_quiz_question_add',array('parent'=>$id));
    }

    public function save_question(){
        $data = isset($_POST['Data']) ? json_decode($_POST['Data']) : (object)array() ;
        $Title = isset($data->Title) ? $data->Title : '' ;
        $Level = isset($data->Level) ? $data->Level : 1 ;
        $Point = isset($data->Point) ? $data->Point : 0 ;
        $Parent = isset($data->ParentID) ? $data->ParentID : 0 ;
        $IsTrue = isset($data->IsTrue) ? $data->IsTrue : 0 ;
        $ID = isset($data->ID) ? $data->ID : 0 ;
        $respone = array('error'=>1,'message'=>'');
        if($Title!='' && $ID>0){
            $data = array(
                'Title' => $Title,
                'Level' => $Level,
                'Point' => $Point,
                'ParentID'=> $Parent,
                'IsTrue'  => $IsTrue
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_event_quiz_question",$data);
            if($IsTrue==1){
                $this->db->query("update ttp_event_quiz_question set IsTrue=0 where ParentID=$Parent and ID!=$ID");
            }
            $respone['error'] = 0;
        }
        echo json_encode($respone);
    }

     public function add_question(){
        $data = isset($_POST['Data']) ? json_decode($_POST['Data']) : (object)array() ;
        $Title = isset($data->Title) ? $data->Title : '' ;
        $Level = isset($data->Level) ? $data->Level : 1 ;
        $Point = isset($data->Point) ? $data->Point : 0 ;
        $Parent = isset($data->ParentID) ? $data->ParentID : 0 ;
        $IsTrue = isset($data->IsTrue) ? $data->IsTrue : 0 ;
        $respone = array('error'=>1,'message'=>'');
        if($Title!=''){
            $data = array(
                'Title' => $Title,
                'Level' => $Level,
                'Point' => $Point,
                'ParentID'=> $Parent,
                'IsTrue'  => $IsTrue
            );
            $this->db->insert("ttp_event_quiz_question",$data);
            $id = $this->db->insert_id();
            if($IsTrue==1){
                $this->db->query("update ttp_event_quiz_question set IsTrue=0 where ParentID=$Parent and ID!=$id");
            }
            $respone['error'] = 0;
            $respone['id'] = $id;
        }
        echo json_encode($respone);
    }

    public function changeimagerow(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        if(isset($_FILES['file'])){
            if($_FILES['file']['error']==0){
                $data = $this->upload_image_single_report("aa",array(),false,IMAGECROPS);
                if($data!=''){
                    $this->db->query("update ttp_event_quiz_question set PrimaryImage='$data' where ID=$ID");
                    echo file_exists($this->lib->get_thumb($data)) ? $this->lib->get_thumb($data) : '' ;
                    return;
                }
            }
        }
        echo "False";
    }

    public function remove_answer(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $this->db->query("delete from ttp_event_quiz_question where ID=$ID");
    }

    public function remove_question(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $this->db->query("delete from ttp_event_quiz_question where ID=$ID or ParentID=$ID");
    }

    public function quiz_question(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1 || $this->user->UserType==5){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $nav = $this->db->query("select count(1) as nav from ttp_event_quiz_question where ParentID=0")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_event_quiz_question where ParentID=0 order by ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/quiz_question',5,$nav,$this->limit)
            );
            $this->template->add_title('Quiz question | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_quiz_question',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function comments(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1 || $this->user->UserType==5 || $this->user->UserType==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $nav = $this->db->query("select count(1) as nav from ttp_report_customer_callme")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_funels_comments order by ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/comments',5,$nav,$this->limit)
            );
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_comments',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function callme(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1 || $this->user->UserType==5 || $this->user->UserType==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $nav = $this->db->query("select count(1) as nav from ttp_report_customer_callme")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_report_customer_callme order by ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/callme',5,$nav,$this->limit)
            );
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_callme',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function sms(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1 || $this->user->UserType==5 || $this->user->UserType==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $search = '';
            if(isset($_GET['search'])){
                $search = mysql_real_escape_string($_GET['search']);
                $nav = $this->db->query("select count(1) as nav from ttp_report_sms where Message like '%$search%'")->row();
                $nav = $nav ? $nav->nav : 0;
                $object = $this->db->query("select * from ttp_report_sms where Message like '%$search%' order by ID DESC $limit_str")->result();
            }else{
                $nav = $this->db->query("select count(1) as nav from ttp_report_sms")->row();
                $nav = $nav ? $nav->nav : 0;
                $object = $this->db->query("select * from ttp_report_sms order by ID DESC $limit_str")->result();
            }
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'search'    =>  $search,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/sms',5,$nav,$this->limit)
            );
            $this->template->add_title('SMS voucher | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_sms_voucher',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function lucky_number(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1 || $this->user->UserType==5){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            if(isset($_GET['dis'])){
                $district = (int)$_GET['dis'];
                $nav = $this->db->query("select count(1) as nav from ttp_event_lucky_number where DistrictID=$district")->row();
                $nav = $nav ? $nav->nav : 0;
                $object = $this->db->query("select * from ttp_event_lucky_number where DistrictID=$district order by ID DESC $limit_str")->result();
            }else{
                $nav = $this->db->query("select count(1) as nav from ttp_event_lucky_number")->row();
                $nav = $nav ? $nav->nav : 0;
                $object = $this->db->query("select * from ttp_event_lucky_number order by ID DESC $limit_str")->result();
            }
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/lucky_number',5,$nav,$this->limit)
            );
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_lucky_number',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function download_lucky_number(){
        $result = $this->db->query("select * from ttp_event_lucky_number")->result();
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Tên khách hàng')
                        ->setCellValue('B1', 'Email')
                        ->setCellValue('C1', 'Số điện thoại')
                        ->setCellValue('D1', 'Địa chỉ');
            $i=2;
            foreach($result as $row){
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row->Name)
                        ->setCellValue('B'.$i, $row->Email)
                        ->setCellValue('C'.$i, $row->Phone)
                        ->setCellValue('D'.$i, $row->Address);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('LUCKY_NUMBER_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="LUCKY_NUMBER_DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "WARNING : Data is empty !!!!";
        }
    }

    public function prize(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->IsAdmin==1 || $this->user->UserType==5 || $this->user->UserType==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $search = $this->session->userdata("report_filter_customer_Search");
            $bonus = $search!='' ? " and (a.Phone1 like '$search' or a.Name like '%$search%')" : '' ;
            $nav = $this->db->query("select count(1) as nav from ttp_report_customer a,ttp_result_luckydraw b where b.ResultCode>0 and a.ID=b.CustomerID $bonus")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select a.Phone1,a.Name,b.ResultCode,b.Recive,b.Created,b.ID from ttp_report_customer a,ttp_result_luckydraw b where b.ResultCode>0 and a.ID=b.CustomerID $bonus order by b.ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'keywords'  => $search,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/prize',5,$nav,$this->limit)
            );
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_prize',$data);
            $this->template->render();
        }
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $bonus = " where Type=0";
            $search = $this->session->userdata("report_filter_customer_Search");
            $bonus = $search!='' ? " where (Phone1 like '%$search%' or Name like '%$search%')" : ' where 1=1' ;
            if($this->user->IsAdmin==0 && $this->user->UserType!=5){
                $bonus = $bonus!='' ? $bonus." and UserID=".$this->user->ID : " where UserID=".$this->user->ID ;
            }
            $nav = $this->db->query("select count(1) as nav from ttp_report_customer $bonus")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_report_customer $bonus order by ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'keywords'  => $search,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/index',5,$nav,$this->limit)
            );
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/import_customer_home',$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer/sales_customers");
        }
    }

    public function sales_customers(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $page = $this->uri->segment(5);
            $start = is_numeric($page) ? $page : 0;
            if(!is_numeric($start)) $start=0;
            $limit_str = "limit $start,$this->limit";
            $bonus = " where 1=1";
            $search = $this->session->userdata("report_filter_customer_Search");
            $bonus = $search!='' ? $bonus." and Name like '%$search%'" : $bonus ;
            $TypeChanel = $this->session->userdata("report_filter_customer_TypeChanel");
            $bonus = $TypeChanel=='' || $TypeChanel==0 ? $bonus." and Type=1" : $bonus ;
            $System = $this->session->userdata("report_filter_customer_SystemID");
            $bonus = $System=='' || $System==0 ? $bonus : $bonus." and SystemID=$System" ;
            $bonus = $TypeChanel==1 ? $bonus." and Type=2" : $bonus ;
            $bonus = $TypeChanel==4 ? $bonus." and Type=4" : $bonus ;
            $bonus = $TypeChanel==5 ? $bonus." and Type=5" : $bonus ;
            $nav = $this->db->query("select count(1) as nav from ttp_report_customer $bonus")->row();
            $nav = $nav ? $nav->nav : 0;
            $object = $this->db->query("select * from ttp_report_customer $bonus order by ID DESC $limit_str")->result();
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                'data'      =>  $object,
                'start'     =>  $start,
                'find'      =>  $nav,
                'keywords'  => $search,
                'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/import_customer/sales_customers',5,$nav,$this->limit)
            );
            $view = $TypeChanel=='' || $TypeChanel==0 ? 'import_customer_home_sales' : '' ;
            $view = $TypeChanel==1 ? 'import_customer_home_salesmt' : $view ;
            $view = $TypeChanel==4 ? 'import_customer_home_salestd' : $view ;
            $view = $TypeChanel==5 ? 'import_customer_home_salesnb' : $view ;
            $this->template->add_title('Customer | Manager Report Tools');
            $this->template->write_view('content','admin/'.$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/import_customer");
        }
    }

    public function setsessionsearch(){
        if(isset($_POST['Search'])){
            $Search = mysql_real_escape_string($_POST['Search']);
            $this->session->set_userdata("report_filter_customer_Search",$Search);
        }
        if(isset($_GET['TypeChanel'])){
            $this->session->set_userdata("report_filter_customer_TypeChanel",(int)$_GET['TypeChanel']);
        }
        if(isset($_GET['SystemID'])){
            $this->session->set_userdata("report_filter_customer_SystemID",(int)$_GET['SystemID']);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_customer_Search");
        $this->session->unset_userdata("report_filter_customer_TypeChanel");
        $this->session->unset_userdata("report_filter_customer_SystemID");
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function add(){
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add',$data);
            $this->template->render();
        }
    }

    public function add_sales(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_gt',$data);
            $this->template->render();
        }
    }

    public function add_sales_mt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_mt',$data);
            $this->template->render();
        }
    }

    public function add_sales_td(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_td',$data);
            $this->template->render();
        }
    }

    public function add_sales_nb(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            $this->template->add_title('Customer add | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/import_customer/'
            );
            $this->template->write_view('content','admin/import_customer_add_nb',$data);
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        $Type = isset($_POST['Type']) ? $_POST['Type'] : 0 ;
        $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
        $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
        $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $Age = isset($_POST['Age']) ? $_POST['Age'] : '' ;
        $Birthday = isset($_POST['NTNS']) ? $_POST['NTNS'] : '' ;
        $Sex = isset($_POST['Sex']) ? $_POST['Sex'] : '' ;
        $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
        $Job = isset($_POST['Job']) ? $_POST['Job'] : '' ;
        $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
        $Testimonial = isset($_POST['Testimonial']) ? 1 : 0 ;
        $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
        $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
        $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
        $BeforeAd = isset($_POST['BeforeAd']) ? $_POST['BeforeAd'] : '' ;
        $AfterAd = isset($_POST['AfterAd']) ? $_POST['AfterAd'] : '' ;
        $Solution = isset($_POST['Solution']) ? $_POST['Solution'] : '' ;
        $GroupID = isset($_POST['GroupID']) ? $_POST['GroupID'] : 0 ;
        if($Name!='' && $Phone1!='' && $Address!=''){
            $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
            if(!$check){
                $max = $this->db->query("select max(ID) as ID from ttp_report_customer")->row();
                $Code = "KH".str_pad($max->ID+1, 8, '0', STR_PAD_LEFT);
                $data = array(
                    'ID'            => $max->ID+1,
                    'Type'          => $Type,
                    'GroupID'       => $GroupID,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Email'         => $Email,
                    'Phone1'        => $Phone1,
                    'Phone2'        => $Phone2,
                    'Address'       => $Address,
                    'AddressOrder'  => $Address,
                    'Age'           => $Age,
                    'Birthday'      => $Birthday,
                    'Sex'           => $Sex,
                    'Job'           => $Job,
                    'Note'          => $Note,
                    'Testimonial'   => $Testimonial,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'UserID'        => $this->user->ID
                );
                $this->db->insert("ttp_report_customer",$data);
                $data = array(
                    'CustomerID'=> $max->ID+1,
                    'BeforeAd'  => $BeforeAd,
                    'AfterAd'   => $AfterAd,
                    'Solution'  => $Solution,
                    'UserID'    => $this->user->ID,
                    'Created'   => date('Y-m-d H:i:s')
                );
                $this->db->insert("ttp_report_advisory_history",$data);
            }else{
                echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                return;
            }
        }
        redirect(ADMINPATH.'/report/import_customer/');
    }

    public function add_new_gt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 1 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
            $AreaNote = isset($_POST['AreaNote']) ? $_POST['AreaNote'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'AddressOrder'  => $Address,
                        'Note'          => $Note,
                        'AreaNote'      => $AreaNote,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'Surrogate'     => $Surrogate,
                        'SystemID'      => $SystemID,
                        'AddressOrder' => $AddressOrder,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->insert("ttp_report_customer",$data);
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;
                }
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function add_new_mt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 2 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $Dept = isset($_POST['Dept']) ? (int)$_POST['Dept'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            $Company = isset($_POST['Company']) ? $_POST['Company'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Surrogate'     => $Surrogate,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AddressOrder'  => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'SystemID'      => $SystemID,
                    'Dept'          => $Dept,
                    'AddressOrder' => $AddressOrder,
                    'UserID'        => $this->user->ID,
                    'Company'       => $Company
                );
                $this->db->insert("ttp_report_customer",$data);
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function add_new_td(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 4 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'AddressOrder'  => $Address,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'AddressOrder' => $AddressOrder,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->insert("ttp_report_customer",$data);
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;
                }
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function add_new_nb(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
            echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 5 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'AddressOrder'  => $Address,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'AddressOrder' => $AddressOrder,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->insert("ttp_report_customer",$data);
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;
                }
            }
            redirect(ADMINPATH.'/report/import_customer/sales_customers');
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function edit($id=0){
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            if(is_numeric($id) && $id>0){
                $bonus='';
                if($this->user->IsAdmin==0){
                    //$bonus = " and UserID=".$this->user->ID ;
                }
                $result = $this->db->query("select * from ttp_report_customer where ID=$id $bonus")->row();
                if(!$result) return;
                $this->template->add_title('Edit Customer | Manager report Tools');
                $data = array(
                    'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                    'data'      =>  $result
                );
                $this->template->write_view('content','admin/import_customer_edit',$data);
                $this->template->render();
            }
        }
    }

    public function edit_sales($id=0){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            if(is_numeric($id) && $id>0){
                $bonus='';
                $result = $this->db->query("select * from ttp_report_customer where ID=$id $bonus")->row();
                if(!$result) return;
                $this->template->add_title('Edit Customer | Manager report Tools');
                $data = array(
                    'base_link' =>  base_url().ADMINPATH.'/report/import_customer/',
                    'data'      =>  $result
                );
                $view = $result->Type==1 ? "import_customer_edit_gt" : "" ;
                $view = $result->Type==2 ? "import_customer_edit_mt" : $view ;
                $view = $result->Type==4 ? "import_customer_edit_td" : $view ;
                $view = $result->Type==5 ? "import_customer_edit_nb" : $view ;
                $this->template->write_view('content','admin/'.$view,$data);
                $this->template->render();
            }
        }
    }

    public function update(){
        if($this->user->UserType!=10 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Type = isset($_POST['Type']) ? $_POST['Type'] : 0 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Age = isset($_POST['Age']) ? $_POST['Age'] : '' ;
            $Birthday = isset($_POST['NTNS']) ? $_POST['NTNS'] : '' ;
            $Sex = isset($_POST['Sex']) ? $_POST['Sex'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Job = isset($_POST['Job']) ? $_POST['Job'] : '' ;
            $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
            $Testimonial = isset($_POST['Testimonial']) ? 1 : 0 ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $BeforeAd = isset($_POST['BeforeAd']) ? $_POST['BeforeAd'] : '' ;
            $AfterAd = isset($_POST['AfterAd']) ? $_POST['AfterAd'] : '' ;
            $Solution = isset($_POST['Solution']) ? $_POST['Solution'] : '' ;
            $GroupID = isset($_POST['GroupID']) ? $_POST['GroupID'] : 0 ;
            if($Name!='' && $Address!=''){
                if($this->user->ID==1){
                    $owner = $this->db->query("select * from ttp_report_customer where ID=$ID")->row();
                }else{
                    $owner = $this->db->query("select * from ttp_report_customer where ID=$ID and UserID=".$this->user->ID)->row();
                }
                if($owner){
                    $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1' and Phone1!='' and ID!=$ID and 1=2")->row();
                    if(!$check){
                        //$Code = "KH".str_pad($ID, 8, '0', STR_PAD_LEFT);
                        $data = array(
                            'ID'            => $ID,
                            'Type'          => $Type,
                            'GroupID'       => $GroupID,
                            //'Code'          => $Code,
                            'Name'          => $Name,
                            'Email'         => $Email,
                            'Phone2'        => $Phone2,
                            'Address'       => $Address,
                            'AddressOrder'  => $Address,
                            'Age'           => $Age,
                            'Birthday'      => $Birthday,
                            'Sex'           => $Sex,
                            'Job'           => $Job,
                            'Note'          => $Note,
                            'Testimonial'   => $Testimonial,
                            'AreaID'        => $AreaID,
                            'CityID'        => $CityID,
                            'DistrictID'    => $DistrictID,
                            'UserID'        => $this->user->ID
                        );
                        $this->db->where("ID",$ID);
                        $this->db->update("ttp_report_customer",$data);
                        if($BeforeAd!='' || $AfterAd !='' || $Solution !=''){
                            $data = array(
                                'CustomerID'=> $ID,
                                'BeforeAd'  => $BeforeAd,
                                'AfterAd'   => $AfterAd,
                                'Solution'  => $Solution,
                                'UserID'    => $this->user->ID,
                                'Created'   => date('Y-m-d H:i:s')
                            );
                            $this->db->insert("ttp_report_advisory_history",$data);
                        }
                        redirect(ADMINPATH.'/report/import_customer/');
                    }else{
                        echo "<script>alert('This phone number is exists !')</script>";
                        echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                        return;
                    }
                }
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_gt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 1 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Note = isset($_POST['Note']) ? $_POST['Note'] : '' ;
            $AreaNote = isset($_POST['AreaNote']) ? $_POST['AreaNote'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Name!='' && $Phone1!='' && $Address!=''){
                $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1' and ID!=$ID")->row();
                if(!$check){
                    $data = array(
                        'Type'          => $Type,
                        'Code'          => $Code,
                        'Name'          => $Name,
                        'Email'         => $Email,
                        'Phone1'        => $Phone1,
                        'Address'       => $Address,
                        'AddressOrder'  => $Address,
                        'Note'          => $Note,
                        'AreaNote'      => $AreaNote,
                        'AreaID'        => $AreaID,
                        'CityID'        => $CityID,
                        'DistrictID'    => $DistrictID,
                        'Surrogate'     => $Surrogate,
                        'AddressOrder'  => $AddressOrder,
                        'SystemID'      => $SystemID,
                        'UserID'        => $this->user->ID
                    );
                    $this->db->where("ID",$ID);
                    $this->db->update("ttp_report_customer",$data);
                    redirect(ADMINPATH.'/report/import_customer/sales_customers');
                }else{
                    echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                    echo "<script>window.location='".base_url().ADMINPATH."/report/import_customer/add'</script>";
                    return;
                }
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_mt(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 2 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Surrogate = isset($_POST['Surrogate']) ? $_POST['Surrogate'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $SystemID = isset($_POST['SystemID']) ? $_POST['SystemID'] : 0 ;
            $Dept = isset($_POST['Dept']) ? (int)$_POST['Dept'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            $Company = isset($_POST['Company']) ? $_POST['Company'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Surrogate'     => $Surrogate,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AddressOrder'  => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'SystemID'      => $SystemID,
                    'Dept'          => $Dept,
                    'AddressOrder'  => $AddressOrder,
                    'UserID'        => $this->user->ID,
                    'Company'       => $Company
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_customer",$data);
                redirect(ADMINPATH.'/report/import_customer/sales_customers');
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_td(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 4 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AddressOrder'  => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'AddressOrder'  => $AddressOrder,
                    'UserID'        => $this->user->ID
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_customer",$data);
                redirect(ADMINPATH.'/report/import_customer/sales_customers');
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function update_nb(){
        if($this->user->UserType!=1 || $this->user->IsAdmin==1){
            $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
            $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
            $Code = isset($_POST['Code']) ? $_POST['Code'] : '' ;
            $Type = 5 ;
            $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
            $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
            $Email = isset($_POST['Email']) ? $_POST['Email'] : '' ;
            $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
            $AreaID = isset($_POST['AreaID']) ? $_POST['AreaID'] : 0 ;
            $CityID = isset($_POST['CityID']) ? $_POST['CityID'] : 0 ;
            $DistrictID = isset($_POST['DistrictID']) ? $_POST['DistrictID'] : 0 ;
            $AddressOrder = isset($_POST['AddressOrder']) ? $_POST['AddressOrder'] : '' ;
            if($Code!='' && $Name!='' && $Address!=''){
                $data = array(
                    'Type'          => $Type,
                    'Email'         => $Email,
                    'Phone1'        => $Phone1,
                    'Code'          => $Code,
                    'Name'          => $Name,
                    'Address'       => $Address,
                    'AddressOrder'  => $Address,
                    'AreaID'        => $AreaID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'AddressOrder'  => $AddressOrder,
                    'UserID'        => $this->user->ID
                );
                $this->db->where("ID",$ID);
                $this->db->update("ttp_report_customer",$data);
                redirect(ADMINPATH.'/report/import_customer/sales_customers');
            }
        }else{
            redirect(ADMINPATH.'/report/import_customer/');
        }
    }

    public function get_history(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $result = $this->db->query("select a.MaDH,a.Ngaydathang,a.Status,a.Total,c.UserName from ttp_report_order a,ttp_report_customer b,ttp_user c where a.UserID=c.ID and a.CustomerID =b.ID and b.ID=$ID")->result();
        echo "<table><tr><th style='width:80px'>STT</th><th>Ngày mua</th><th>Mã ĐH</th><th>Giá trị ĐH</th><th>Nhân viên bán hàng</th><th>Tình trạng ĐH</th></tr>";
        if(count($result)>0){
            $array_status = array(
                9=>'Chờ nhân viên điều phối',
                8=>'Đơn hàng bị trả về',
                7=>'Chuyển sang bộ phận giao hàng',
                6=>'Đơn hàng bị trả về từ kế toán',
                5=>'Đơn hàng chờ kế toán duyệt',
                4=>'Đơn hàng bị trả về từ kho',
                3=>'Đơn hàng mới chờ kho duyệt',
                2=>'Đơn hàng nháp',
                0=>'Đơn hàng thành công',
                1=>'Đơn hàng hủy'
            );
            $i=1;
            foreach($result as $row){
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>";
                echo "<td>$row->MaDH</td>";
                echo "<td>".number_format($row->Total)."</td>";
                echo "<td>$row->UserName</td>";
                echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>";
                echo "</tr>";
                $i++;
            }
        }else{
            echo '<tr><td colspan="6">Chưa có dữ liệu lịch sử giao dịch</td></tr>';
        }
        echo "</table>";
    }

    public function get_history_advisory(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $result = $this->db->query("select a.*,b.UserName from ttp_report_advisory_history a,ttp_user b where a.UserID=b.ID and a.CustomerID=$ID")->result();
        echo "<table><tr><th style='width:80px'>STT</th><th>Vấn đề trước khi sử dụng</th><th>Giải pháp của nv tư vấn</th><th>Hiệu quả khi sử dụng</th><th>Người tạo</th><th>Ngày cập nhật</th></tr>";
        if(count($result)>0){
            $i=1;
            foreach($result as $row){
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".$row->BeforeAd."</td>";
                echo "<td>".$row->Solution."</td>";
                echo "<td>".$row->AfterAd."</td>";
                echo "<td>$row->UserName</td>";
                echo "<td>".date('d/m/Y',strtotime($row->Created))."</td>";
                echo "</tr>";
                $i++;
            }
        }else{
            echo '<tr><td colspan="5">Chưa có dữ liệu lịch sử tư vấn</td></tr>';
        }
        echo "</table>";
    }

    public function checkphone(){
        $data = isset($_POST['data']) ? $_POST['data'] : '' ;
        if($data!=''){
            $check = $this->db->query("select ID from ttp_report_customer where Phone1 ='$data'")->row();
            echo $check ? "false" : "ok";
        }
    }

    public function get_city_by_area(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        echo "<option value=''>-- Chọn tỉnh thành --</option>";
        if(is_numeric($ID)){
            $result = $this->db->query("select * from ttp_report_city where AreaID=$ID order by Title ASC")->result();
            if(count($result)>0){
                foreach($result as $row){
                    echo "<option value='$row->ID'>$row->Title</option>";
                }
            }
        }
    }

    public function get_district_by_city(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
        $data = array('DistrictHtml'=>"<option value='0'>-- Chọn quận huyện --</option>",'WarehouseHtml'=>'<option value="">Không có kho hàng</option>');
        if(is_numeric($ID)){
            $result = $this->db->query("select * from ttp_report_district where CityID=$ID order by Title ASC")->result();
            if(count($result)>0){
                foreach($result as $row){
                    $data['DistrictHtml'].= "<option value='$row->ID'>$row->Title</option>";
                }
            }
            $warehouse = $this->db->query("select b.MaKho,b.ID from ttp_report_city a,ttp_report_warehouse b where a.DefaultWarehouse=b.ID and a.ID=$ID")->row();
            if($warehouse){
                $data['WarehouseHtml'] = "<option value='$warehouse->ID'>$warehouse->MaKho</option>";
            }
        }
        echo json_encode($data);
    }

    public function add_system_mt(){
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '';
        $check = $this->db->query("select * from ttp_report_system where Title = '$Title'")->row();
        if(!$check){
            $this->db->query("insert into ttp_report_system(Title) value('$Title')");
            $id = $this->db->insert_id();
            echo $id;
        }else{
            echo "false";
        }
    }

    public function has_order_luckynumber($id=0,$status=0){
        $this->db->query("update ttp_event_lucky_number set Status=$status where ID=$id");
        $icon = $status==0 ? "<i class='fa fa-check-square' style='color:#ddd'></i>" : "<i class='fa fa-check-square' style='color:#090'></i>" ;
        echo $icon;
    }

    public function has_success_callme($id=0,$status=0){
        $this->db->query("update ttp_report_customer_callme set Success=$status where ID=$id");
        $icon = $status==0 ? "<i class='fa fa-check-square' style='color:#ddd'></i>" : "<i class='fa fa-check-square' style='color:#090'></i>" ;
        echo $icon;
    }

    public function has_success_comments($id=0,$status=0){
        $this->db->query("update ttp_funels_comments set Status=$status where ID=$id");
        $icon = $status==0 ? "<i class='fa fa-check-square' style='color:#ddd'></i>" : "<i class='fa fa-check-square' style='color:#090'></i>" ;
        echo $icon;
    }
}
?>
