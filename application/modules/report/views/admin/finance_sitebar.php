<?php 
$url = base_url().ADMINPATH.'/report';
$segment = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-arrow-circle-up fa-fw" aria-hidden="true"></i> Quản lý thu</p>
    <ul>
        <li><a href="<?php echo $url.'/finance_import/' ?>" <?php echo $segment=="finance_import" && ($segment_current_bonus=="index" || $segment_current_bonus=="add" || $segment_current_bonus=="" || $segment_current_bonus=="edit_import") ? "class='active'" : '' ; ?>>Phiếu thu</a></li>
        <li><a href="<?php echo $url.'/finance_import/due' ?>" <?php echo $segment=="finance_import" && $segment_current_bonus=="due" ? "class='active'" : '' ; ?>>Nợ phải thu</a></li>
    </ul>
    <p class="title"><i class="fa fa-arrow-circle-down fa-fw" aria-hidden="true"></i> Quản lý chi</p>
    <ul>
        <li><a href="<?php echo $url.'/finance_export/' ?>" <?php echo $segment=="finance_export" && ($segment_current_bonus=="index" || $segment_current_bonus=="add" || $segment_current_bonus=="" || $segment_current_bonus=="edit_export") ? "class='active'" : '' ; ?>>Phiếu chi</a></li>
        <li><a href="<?php echo $url.'/finance_export/due' ?>" <?php echo $segment=="finance_export" && $segment_current_bonus=="due" ? "class='active'" : '' ; ?>>Nợ phải trả</a></li>
    </ul>
    <p class="title"><i class="fa fa-university fa-fw" aria-hidden="true"></i> Quản lý ngân sách</p>
    <ul>
        <li><a href="<?php echo $url.'/finance_budget/cash' ?>" <?php echo $segment=="finance_budget" && ($segment_current_bonus=="cash") ? "class='active'" : '' ; ?>>Quỹ tiền mặt</a></li>
        <li><a href="<?php echo $url.'/finance_budget/bank' ?>" <?php echo $segment=="finance_budget" && ($segment_current_bonus=="bank") ? "class='active'" : '' ; ?>>Quỹ ngân hàng</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>

<script>
    $(".sitebar ul li p i").click(function(){
        $(".sitebar ul li ul").slideUp();
        $(".sitebar ul li p i.fa").removeClass('fa-caret-up').addClass('fa-caret-down');
        $(this).removeClass('fa-caret-down').addClass('fa-caret-up');
        $(this).parent('p').parent('li').find('ul').slideDown();
    });
</script>
