	<table class="table">
	<tr>
		<th>HÌNH ẢNH</th>
		<th>TÊN SẢN PHẨM</th>
		<th>ĐƠN VỊ</th>
		<th>SỐ LƯỢNG</th>
		<th>ĐƠN GIÁ</th>
		<th>THÀNH TIỀN</th>
		<th class="text-center">HỦY</th>
	</tr>
	<?php 
	$districtid = isset($data['customer']['district']) ? $data['customer']['district'] : 0 ;
	$transport_type = isset($data['transport']) ? $data['transport'] : 0 ;
	$total_order = 0;
	$total_products = 0;
	$total_reduce = 0;
	$total_chiphi = 0;
	if(isset($data['chiphi'])){
		$total_chiphi = $data['chiphi'];
	}
	if(isset($data['reduce'])){
		$total_reduce = $data['reduce'];
	}
	if(isset($data['products']) && count($data['products'])>0){
		$key = array_keys($data['products']);
		$arr_inventory = array();
		$inventory = $this->db->query("select * from ttp_report_inventory where ProductsID in(".implode(',',$key).") and LastEdited=1 and WarehouseID=$this->WarehouseID")->result();
		if(count($inventory)>0){
			foreach($inventory as $row){
				if(isset($arr_inventory[$row->ProductsID])){
					$arr_inventory[$row->ProductsID] = $arr_inventory[$row->ProductsID]+$row->OnHand;
				}else{
					$arr_inventory[$row->ProductsID] = $row->OnHand;
				}
			}
		}
		$total_products = count($key);
		$products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
		if(count($products)>0){
			foreach ($products as $row) {
				$invent = isset($arr_inventory[$row->ID]) ? $arr_inventory[$row->ID] : 0 ;
				$amount = $data['products'][$row->ID];
				$price = date('Y-m-d')>=date('Y-m-d',strtotime($row->SpecialStartday)) && date('Y-m-d')<=date('Y-m-d',strtotime($row->SpecialStopday)) ? $row->SpecialPrice : $row->Price ;
				$diff = '';
				if(isset($data['price'][$row->ID])){
					if($data['price'][$row->ID]<$price){
						$diff = $price-$data['price'][$row->ID];
						$diff = '<span style="color:#999;display:block;margin-top:8px">Đã giảm : '.number_format($diff).'</span>';
					}
					$price = $data['price'][$row->ID];
				}
				$total = $amount*$price;
				$total_order += $total;
				echo '<tr>
				<td><img src="'.$this->lib->get_thumb($row->PrimaryImage).'" /></td>
				<td style="width:350px">
					<p>'.$row->Title.'</p>
					<span style="color:#999">Mã sản phẩm : '.$row->MaSP.'</span>
				</td>
				<td>'.$row->Donvi.'</td>
				<td>
					<input type="number" value="'.$data['products'][$row->ID].'" onchange="sales_change_products(this,'.$row->ID.')" style="width:100px" />
					<span style="color:#999;display: block;margin-top:8px">Hiện còn : '.$invent.'</span>
				</td>
				<td><input type="number" value="'.$price.'" onchange="sales_change_price(this,'.$row->ID.')" />'.$diff.'</td>
				<td>'.number_format($total).'</td>
				<td class="text-center"><a onclick="sales_remove_products('.$row->ID.')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
			}
		}
	}else{
		echo "<tr><td colspan='8'>Vui lòng chọn sản phẩm</td></tr>"; 
	}
	?>
	<tr>
		<th colspan="5" class="text-right">TỔNG SỐ SẢN PHẨM</th>
		<th colspan="3">: <?php echo number_format($total_products) ?></th>
	</tr>
	<tr>
		<th colspan="5" class="text-right">GIÁ TRỊ ĐƠN HÀNG</th>
		<th colspan="3">: <?php echo number_format($total_order) ?></th>
	</tr>
	<tr>
		<th colspan="5" class="text-right">GIẢM GIÁ TRÊN ĐƠN HÀNG</th>
		<th colspan="3">: <input name="Reduce" onchange="sales_add_reduce(this)" type="number" value="<?php echo $total_reduce ?>" /></th>
	</tr>
	<?php 
	$cal_fee = $total_order-$total_reduce;
	if($transport_type==1){
		$fee = $this->lib->getfee($districtid,$cal_fee);
		$temp = $total_chiphi>=0 ? $total_chiphi : $fee ;
	}else{
		$temp = $total_chiphi;
	}
	?>
	<tr>
		<th colspan="5" class="text-right">CHI PHÍ GIAO HÀNG</th>
		<th colspan="3">: <input onchange="sales_add_chiphi(this)" type="number" id="feeship" value="<?php echo $temp; ?>" /></th>
	</tr>
	<tr style="font-size: 17px">
		<th colspan="5" class="text-right">TỔNG TIỀN PHẢI TRẢ</th>
		<th colspan="3">: <?php echo number_format($total_order-$total_reduce+$temp) ?>đ</th>
	</tr>
	</table>