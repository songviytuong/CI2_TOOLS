<div class="containner">
	<div id="alertmessage"></div>
	<div class="disableedit" <?php echo $data->Status==1 && $this->user->UserType==1 ? 'style="height:1200px"' : 'style="height:0px"' ; ?>></div>
	<div class="status_progress">
    	<?php
    	$status1 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 || $data->Status==2 ? 'class="active"' : "";
    	$point1 = $status1=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point1 = $data->Status==3 || $data->Status==5 || $data->Status==7  || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point1 ;

		$status2 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point2 = $status2=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point2 =  $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point2 ;

    	$status3 = $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point3 = $status3=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point3 =  $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point3 ;

    	$status4 = $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point4 = $status4=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point4 =  $data->Status==7 || $data->Status==8 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point4 ;

		$status5 = $data->Status==8 || $data->Status==7 || $data->Status==0 ? 'class="active"' : "";
    	$point5 = $status5=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point5 =  $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point5 ;

    	$width = $data->Status==2 || $data->Status==4 || $data->Status==6 ? 20 : 0 ;
    	$width = $data->Status==3 ? 40 : $width ;
    	$width = $data->Status==5 ? 60 : $width ;
    	$width = $data->Status==9 ? 80 : $width ;
    	$width = $data->Status==7 || $data->Status==0 || $data->Status==8 ? 100 : $width ;

    	if($data->Status==1 || $data->Status==8){
    		$title_drop = $data->Status==8 ? "Bị trả về" : "Bị hủy" ;
    		echo "<div class='orderisdrop'><a>$title_drop</a><span><i class='fa fa-times'></i></span></div>";
    		echo '<div class="transfer_progress" style="width:100%;background:#000"></div>';
    	}else{
    	?>
    	<ul>
            <li class="active"><a>ĐH nháp</a><span class="ok"><i class="fa fa-check-circle"></i></span></li>
            <li <?php echo $status1 ?>><a>Chờ chuyển kho xử lý</a><?php echo $point1 ?></li>
            <li <?php echo $status2 ?>><a>Kho xác nhận</a><?php echo $point2 ?></li>
            <li <?php echo $status3 ?>><a>Kế toán xác nhận</a><?php echo $point3 ?></li>
            <li <?php echo $status4 ?>><a>Đã giao vận chuyển</a><?php echo $point4 ?></li>
            <li <?php echo $status5 ?>><a>Hoàn tất</a><?php echo $point5 ?></li>
        </ul>
        <div class="transfer_progress" style="width:<?php echo $width ?>%"></div>
        <?php
    	}
        ?>
    </div>
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href="<?php echo $base_link ?>">THÔNG TIN ĐƠN HÀNG</a> - <a style="color:#DE5252"><?php echo $data->MaDH ?></a></h1>
	    </div>
	    <div class="block2">
	    	<div class="btn_group">
	    		<?php
	    		if($this->user->UserType==5){
	    			if($data->Status==2 && $data->UserID==2){
	    				echo "<select onchange='assign(this)' style='margin-left:10px;padding:6px;margin-right:10px;'><option value='0'>-- Chuyển đơn hàng --</option>";
	    				$userlist = $this->db->query("select UserName,ID from ttp_user where Published=1 and UserType=1 and Channel=0")->result();
	    				if(count($userlist)>0){
	    					foreach($userlist as $row){
	    						echo "<option value='$row->ID'>$row->UserName</option>";
	    					}
	    				}
	    				echo "</select>";
	    				echo '<a class="btn btn-success btn-update" type="submit" onclick="submitform(this,'.$data->Status.')"><i class="fa fa-pencil"></i> Lưu nháp</a>';
	    				echo '<button class="btn btn-danger btn-accept" type="submit" onclick="submitform(this,3)"><i class="fa fa-check-square"></i> Duyệt</button>';
	    			}
	    		}else{
	    		?>
		    		<a class="btn btn-default btn-drop pull-right" onclick="show_cancelbox()" style="margin-right: 0px;">Hủy <i class="fa fa-caret-down"></i></a>
					<?php
					$checkleader = $this->db->query("select Data from ttp_report_team where UserID=".$this->user->ID)->row();
		            if($data->Status==2 || $data->Status==4 || $data->Status==6){
						echo '<a class="btn btn-danger btn-accept" type="submit" onclick="submitform(this,3)" erp-url-excute="'.$base_link.'update_order" erp-object-excute="#submit_order" erp-object-message="#box-warning" erp-url-done="'.$base_link.'"><i class="fa fa-check-square"></i> Duyệt</button>';
						echo '<a class="btn btn-primary btn-update" type="submit" onclick="submitform(this,'.$data->Status.')" erp-url-excute="'.$base_link.'update_order" erp-object-excute="#submit_order" erp-object-message="#box-warning" erp-url-done="'.$base_link.'"><i class="fa fa-pencil-square-o"></i> Lưu</a>';
						echo $this->user->UserType==5 || $checkleader ? "<a class='btn btn-primary btn-print' href='".base_url().ADMINPATH."/report/import/printorder/$data->ID'><i class='fa fa-print'></i> In đơn hàng</a>" : "" ;
					}

                    $refExits = $this->db->query("select TransportID,TransportRef from ttp_report_order where ID=$data->ID")->row();
                    $dis = "";
                    $style = "";
                    if(in_array($refExits->TransportID,$arr)){
                        $dis = " disabled ";
                        $style = ' style="display:none"';
                    }
                    if(in_array($refExits->TransportID,$arr)){
                        echo '<a class="btn btn-success" type="button" onclick="transportInfo(this,'.$refExits->TransportID.')"><i class="fa fa-truck"></i> Xem thông tin</a>';
                    }
					
					?>
					<div class="cancel_box">
						<?php
						$reasonchecked = $this->db->query("select * from ttp_report_order_reason_details where OrderID=$data->ID")->row();
						if($reasonchecked){
							if($reasonchecked->PLReason!=0 || $reasonchecked->Another3PL!=''){
								if($reasonchecked->Another3PL!=''){
									echo "<div class='PLReason'><b>Lý do hủy từ 3PL </b> : ".$reasonchecked->Another3PL."</div>";
								}else{
									$check_reason = $this->db->query("select * from ttp_report_order_reason where ID=$reasonchecked->PLReason")->row();
									echo $check_reason ? "<div class='PLReason'><b>Lý do hủy từ 3PL </b>: ".$check_reason->Title."</div>" : '' ;
								}
							}

							if($reasonchecked->SaleReasonID!=0 || $reasonchecked->AnotherSale!=''){
								if($reasonchecked->AnotherSale!=''){
									echo "<div class='PLReason'><b>Lý do hủy từ người bán hàng </b> : ".$reasonchecked->AnotherSale."</div>";
								}else{
									$check_reason = $this->db->query("select * from ttp_report_order_reason where ID=$reasonchecked->SaleReasonID")->row();
									echo $check_reason ? "<div class='PLReason'><b>Lý do hủy từ người bán hàng </b>: ".$check_reason->Title."</div>" : '' ;
								}
							}
						}
		    			$another = $reasonchecked ? $reasonchecked->AnotherSale : '' ;
		    			$Follow = $reasonchecked ? $reasonchecked->Follow : 0 ;
		    			$Follow = $Follow==1 ? "checked='checked'" : '' ;
		    			$reasonchecked = $reasonchecked ? $reasonchecked->SaleReasonID : 0 ;
						?>
						<p>Chọn lý do hủy đơn hàng</p>
						<select id="Lydohuydh" onchange="checkshowlydokhac(this)" class="form-control">
			    			<option value=''>-- Lý do hủy đơn hàng --</option>
			    			<?php
			    			$reason = $this->db->query("select * from ttp_report_order_reason")->result();
			    			if(count($reason)>0){
			    				foreach($reason as $row){
			    					$checked = $row->ID==$reasonchecked ? "selected='selected'" : '' ;
			    					echo "<option value='$row->ID' $checked>$row->Title</option>";
			    				}
			    			}

			    			?>
			    			<option value='0'>LÝ DO KHÁC</option>
			    		</select>
			    		<p class='lydokhac'><span>Lý do hủy khác : </span><input type='text' id='Lydokhac' placeholder="Điền lý do hủy khác ..." class="form-control" /></p>
			    		<p><input type='checkbox' name='Follow' id='Follow' style='float:left;margin-right:5px' <?php echo $Follow ?> /> Đánh dấu đơn hàng hủy cần theo dõi về sau .</p>
			    		<a class='btn btn-danger btn-acceptcancel' onclick='send_cancel_request(this)' data='<?php echo $data->Status==1 ? base_url().ADMINPATH."/report/import_order/update_reason/$data->ID" : base_url().ADMINPATH."/report/import_order/cancel_request/$data->ID"; ?>'><i class='fa fa-minus-circle'></i> <?php echo $data->Status==1 ? 'Cập nhật lý do' : 'Xác nhận hủy' ; ?></a>
					</div>
				<?php
				}
				?>
			</div>
	    </div>
    </div>
    <div class="alert alert-danger hidden" id="box-warning"></div>
    <div class="import_order_info">
    	<form id="submit_order" method="POST" action="<?php echo $base_link.'update_order' ?>" enctype='multipart/form-data'>
    		<?php
			if(file_exists($data->ImageCMND)){
				echo "<a target='_blank' style='position: absolute;top: 249px;right: 21px;border-radius: 3px;overflow: hidden;z-index:9999;' href='".base_url().$data->ImageCMND."'><img style='height:36px' src='".$data->ImageCMND."' /></a>";
			}
			?>
    		<a title="<?php echo file_exists($data->ImageCMND) ? 'Thay đổi hình CMND' : 'Đính kèm hình CMND' ; ?>" class="btn btn-default hidden" style="position: absolute;top: 50px;right: 1px;width: 32px;height: 32px;overflow: hidden;z-index: 9999;border: none;border-left: 1px solid #E1e1e1;border-radius: 0px 3px 3px 0px;">
				<i class="fa fa-paperclip" aria-hidden="true"></i>
				<input type="file" name="Image_upload" style="position: absolute;left: 0px;top: 0px;height: 36px;padding-left: 138px;outline: none;" onchange="$('#submit_order').submit()" />
			</a>
    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
    		<input type="hidden" name="IDOrder" value="<?php echo $data->ID ?>" />
	    	<div class="block1">
	    		<div class="row">
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Nhân viên bán hàng </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select name="UserID" id="SalesmanID" class="form-control" >
				    				<?php
				    				$saleman = $this->user->UserType==5 ? $data->UserID : $this->user->ID ;
				    				echo "<option value='".$saleman."'>".$data->UserName."</option>";
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Nguồn đơn hàng </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select name="SourceID" id="SourceID" class="form-control">
				    				<?php
				    				$source = $this->db->query("select * from ttp_report_source")->result();
				    				if(count($source)>0){
				    					foreach ($source as $row) {
				    						$selected = $row->ID==$data->SourceID ? "selected='selected'" : '' ;
	    									echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Kênh bán hàng </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select name="KenhbanhangID" id="KenhbanhangID" class="form-control">
				    				<option value='0'>-- Chọn kênh bán hàng --</option>
				    				<?php
				    				$kenhbanhang = $this->db->query("select * from ttp_report_saleschannel")->result();
				    				if(count($kenhbanhang)>0){
				    					foreach($kenhbanhang as $row){
				    						$selected = $row->ID==$data->KenhbanhangID ? "selected='selected'" : '' ;
	    									echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<input type='hidden' name="Ngayban" class="form-control date-picker" id="Ngayban" value="<?php echo date('m/d/Y',strtotime($data->Ngaydathang)); ?>" readonly="true" required />
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Nhóm khách hàng </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select name="GroupID" id="GroupID" class="form-control">
		    					<?php 
		    					$group = $this->db->query("select * from ttp_report_customer_group")->result();
		    					if(count($group)>0){
		    						foreach($group as $row){
		    							$selected = $row->ID==$data->GroupID ? 'selected="selected"' : '' ;
		    							echo "<option value='$row->ID' $selected>$row->Title</option>";
		    						}
		    					}
		    					?>
		    					</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Tên khách hàng </label>
    						<div class="col-xs-12 col-sm-7">
		    					<input name="CustomerID" id="CustomerID" value="<?php echo $data->CustomerID ?>" type="hidden" />
		    					<input type='text' name="Tenkhachhang" class="form-control" id="Tenkhachhang" value="<?php echo $data->Name ?>" required />
		    				</div>
	    				</div>
	    			</div>
					<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Ngày sinh </label>
    						<div class="col-xs-12 col-sm-7">
		    					<input type='text' name="NTNS" id="NTNS" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($data->Birthday)) ?>" class="form-control NTNS" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4 hidden">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Số CMND (nếu có) </label>
    						<div class="col-xs-12 col-sm-7">
		    					<input type='text' name="CMND" class="form-control Age" value="<?php echo $data->CMND ?>" id="Age" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-12 hidden-xs" style="margin-top:10px">
	    				<div class="form-group">
	    					<div class='lichsugiaodich' style="position: relative;z-index: 999;"><div class="history_cus"></div><a id='xemlichsugiaodich' data-status="down">Xem lịch sử giao dịch <b class='caret'></b></a></div>
	    				</div>
	    			</div>

	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Số di động 1: </label>
    						<div class="col-xs-12 col-sm-7">
		    					<input type='text' name="Phone1" id="Phone1" class="form-control" value='<?php echo $data->Phone1 ?>' required />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Số di động 2: </label>
    						<div class="col-xs-12 col-sm-7">
		    					<input type='text' name="Phone2" id="Phone2" class="form-control" value='<?php echo $data->Phone2 ?>' />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">PT thanh toán</label>
    						<div class="col-xs-12 col-sm-7">
		    					<select name="PTthanhtoan" class="form-control">
		    						<?php
		    						$pay_status = $this->define_model->get_order_status('payment','order');
					                $arr_payment = array();
					                foreach($pay_status as $key=>$ite){
					                    $code = (int)$ite->code;
					                    $selected = $data->Payment==$code ? 'selected="selected"' : '' ;
					                    echo "<option value='".$code."' $selected>".$ite->name."</option>";
					                }
					                ?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<?php
	    			$tinhthanh = $this->db->query("select a.Title,a.ID,b.Title as AreaTitle,a.AreaID from ttp_report_city a,ttp_report_area b where a.AreaID=b.ID and a.ID=$data->CityID")->row();
	    			?>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Khu vực </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select id="Khuvuc" class="form-control">
				    				<option value="0">-- Chọn khu vực --</option>
				    				<?php
				    				$area = $this->db->query("select * from ttp_report_area")->result();
				    				if(count($area)>0){
				    					foreach($area as $row){
				    						$selected= '';
				    						if($tinhthanh){
				    							$selected = $row->ID==$tinhthanh->AreaID ? "selected='selected'" : '' ;
				    						}
				    						echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Tỉnh / Thành </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select id="Tinhthanh" class="form-control" name="CityID">
				    				<?php
				    					$selected= '';
			    						if($tinhthanh){
			    							$selected = $row->ID==$tinhthanh->AreaID ? "selected='selected'" : '' ;
			    							echo "<option value='$tinhthanh->ID'>$tinhthanh->Title</option>";
			    						}else{
			    							echo '<option value="">-- Chọn tỉnh thành --</option>';
			    						}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Quận / Huyện </label>
    						<div class="col-xs-12 col-sm-7">
		    					<select id="Quanhuyen" class="form-control" name="DistrictID" onchange="recal()">
				    				<?php
				    					$selected= '';
				    					$quanhuyen = $this->db->query("select * from ttp_report_district where CityID=$data->CityID")->result();
			    						if(count($quanhuyen)>0){
			    							foreach($quanhuyen as $row){
				    							$selected = $row->ID==$data->DistrictID ? "selected='selected'" : '' ;
				    							echo "<option value='$row->ID' $selected>$row->Title</option>";
			    							}
			    						}else{
			    							echo '<option value="">-- Chọn quận huyện --</option>';
			    						}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
					<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Ngày hẹn giao hàng </label>
    						<div class="col-xs-12 col-sm-7">
		    					<input type="text" name="DayDeliveryTime" class="form-control date-picker" id="Export_date" value="<?php echo date('Y-m-d',strtotime($data->DeliveryTime))!="-0001-11-30" ? date('Y-m-d',strtotime($data->DeliveryTime)) : date('Y-m-d',strtotime($data->Ngaydathang)) ; ?>" placeholder="yyyy-mm-dd" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-6 col-sm-4">
	    				<div class="form-group">
    						<label class="col-xs-12 col-sm-5 control-label">Thời gian chính xác</label>
    						<div class="col-xs-12 col-sm-7">
		    					<select class="form-control" name="HourDeliveryTime">
		    						<?php
		    						$hour = date("H",strtotime($data->DeliveryTime));
		    						for ($i=1; $i < 25; $i++) {
		    							$selected = $hour==$i ? "selected='selected'" : "" ;
		    							$i=$i<10 ? '0'.$i : $i ;
		    							echo "<option value='$i' $selected>$i giờ</option>";
		    						}
		    						?>
		    					</select>
		    				</div>
	    				</div>
	    			</div>

	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Địa chỉ giao hàng </label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Address" id="Address_order" class="form-control" value="<?php echo $data->AddressOrder ?>" required />
			    			</div>
		    			</div>
	    			</div>
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Ghi chú giao hàng</label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Note" class="form-control" value='<?php echo $data->Note ?>' />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    	</div>

	    	<!-- end block1 -->
	    	<div class="block2 row">
					<div class="col-xs-8">
	    			<div class="form-group">
		    			<div class="col-xs-5">
		    				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Sản phẩm</a>
								<ul>
									<li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
					    				<ul>
						    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
						    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
					    				</ul>
									</li>
								</ul>
							</div>
							<div class="col-xs-2">
								<input type='text' name="Voucher" id='voucher' placeholder='Mã giảm giá' class="form-control pull-left" />
							</div>
							<div class="col-xs-5">
								<a class="btn btn-default" onclick="checkvoucher(this)" style="margin-left:10px"><i class="fa fa-check-square"></i> Áp dụng</a>
								<span style="padding:8px 0px;float:left;">Mã hiện tại : 
									<?php
									$voucher = $this->db->query("select * from ttp_report_voucher where ID=$data->VoucherID")->row();
									if($voucher){
										echo $voucher->Code;
									}
									?>
								</span>
							</div>
		    		</div>
		    	</div>
		    	<div class="col-xs-4">
		    		<div class="form-group">
		    			<label class="col-xs-4 control-label text-right">Kho lấy hàng </label>
		    			<div class="col-xs-7 pull-right">
		    				<select name="KhoID" id="KhoID" class="form-control" onchange="loadshipmentdefault()">
			    				<?php
			    				$khoresult = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID=$data->KhoID")->row();
			    				if($khoresult){
			    					echo "<option value='$khoresult->ID'>$khoresult->MaKho</option>";
			    				}else{
			    					echo '<option value="">-- Chọn Kho --</option>';
			    				}
			    				?>
			    			</select>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
	    	<div class="table_donhang table_data">
	    		<table class="table_data" id="table_data">
	    			<tr>
	    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
	    				<th>Mã sản phẩm</th>
	    				<th>Tên sản phẩm</th>
	    				<th>Lô</th>
	    				<th>Đơn vị</th>
	    				<th>Số lượng</th>
	    				<th>Giá bán</th>
	    				<th>% CK</th>
	    				<th>Giá trị CK</th>
	    				<th>Giá sau CK</th>
	    				<th>Thành tiền</th>
	    			</tr>
	    			<?php

	    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
	    			$arrproducts = array();
	    			$details_products = array();
	    			if(count($details)>0){
	    				foreach($details as $row){
	    					$details_products[] = $row->Title . " x ".$row->Amount." ".$row->Donvi;
	    					echo "<tr>";
	    					echo "<td><input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='$row->ProductsID'>
	    							  <input type='hidden' name='ProductsID[]' value='$row->ProductsID'>
	    					</td>";
	    					echo "<td>$row->MaSP</td>";
	    					echo "<td>$row->Title</td>";
	    					echo "<td style='width:180px;text-align:left'><span class='ShipmentDefault' data-id='$row->ProductsID'>--</span></td>";
	    					echo "<td>$row->Donvi</td>";
	    					$giaban = $row->Price+$row->PriceDown;
	    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
	    					echo "<td><input type='number' name='Amount[]' class='soluong' value='$row->Amount' min='1' onchange='recal()' readonly='true' /></td>";
	    					echo "<td><input type='number' onchange='recal()' class='dongia' value='$giaban' min='0' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>".number_format($giaban)."</span></td>";
	    					
	    					echo "<td><input type='number' name='PercentCK[]' onchange='calrowchietkhau(this)' class='percentck' value='$phantramck' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>$phantramck</span></td>";
							echo "<td><input type='number' name='GiaCK[]' onchange='calrowchietkhau(this)' class='giack' value='$row->PriceDown' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->PriceDown)."</span></td>";
							echo "<td><input type='number' name='Price[]' class='giasauCK' value='$row->Price' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Price)."</span></td>";
							echo "<td><input type='number' class='thanhtien' value='$row->Total' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Total)."</span></td>";
	    					echo "</tr>";
	    					$arrproducts[] = '"data'.$row->ProductsID.'"';
	    				}
	    			}

	    			$phantramchietkhau = ($data->Total==0) ? 0 : round($data->Chietkhau/($data->Total/100));
	    			$tongtienhang = $data->Total - $data->Chietkhau;
	    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
	    			?>
	    			<tr class="last">
	    				<td colspan="10">Tổng cộng</td>
	    				<td><input type="number" class="tongcong" readonly="true" value="<?php echo $data->Total ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong"><?php echo number_format($data->Total) ?></span></td>

	    			</tr>
	    			<tr class="last">
	    				<td colspan="10">% chiết khấu</td>
	    				<td><input type='number' name="phantramchietkhau" class="phantramchietkhau" min="0" max="100" value="<?php echo $phantramchietkhau ?>" onchange="calchietkhau(this)" /></td>

	    			</tr>
	    			<tr class="last">
	    				<td colspan="10">Giá chiết khấu</td>
	    				<td><input type='number' name="giachietkhau" class="giachietkhau" value="<?php echo $data->Chietkhau ?>" onchange="calchietkhau(this)" /></td>

	    			</tr>
	    			<tr class="last">
	    				<td colspan="10">Tổng tiền hàng</td>
	    				<td><input type="number" class="tongtienhang" readonly="true" value="<?php echo $tongtienhang ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang"><?php echo number_format($tongtienhang) ?></span></td>

	    			</tr>
	    			<tr class="last">
	    				<td colspan="10">Chi phí vận chuyển</td>
	    				<td><input type='number' name="chiphivanchuyen" class="chiphivanchuyen" value="<?php echo $data->Chiphi ?>" onchange="recal_total()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()"><?php echo number_format($data->Chiphi) ?></span></td>

	    			</tr>
	    			<tr class="last">
	    				<td colspan="10">Tổng giá trị thanh toán</td>
	    				<td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="<?php echo $tonggiatrithanhtoan ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan"><?php echo number_format($tonggiatrithanhtoan) ?></span></td>

	    			</tr>
	    		</table>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
	    			$arr_status = $this->define_model->get_order_status('status','order');
                                $array_status = array();
                                foreach($arr_status as $key=>$ite){
                                    $code = (int)$ite->code;
                                    $array_status[$code] = $ite->name;
                                }
	    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
	    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
    		<?php 
    		$prize_gift = $this->db->query("select * from ttp_define where `group`='prize' and `type`='lucky_draw' and position=0")->result();
    		$arr_gift = array();
            if(count($prize_gift)>0){
                foreach($prize_gift as $row){
                	$arr_gift[$row->code] = $row->name;
                }
            }

            if(count($prize)>0){
            	echo '<div class="history_status"><h2>Danh sách khách đã trúng thưởng</h2>';
            	echo "<table><tr><th>Phần quà</th><th>Ngày / giờ</th><th>Trạng thái nhận quà</th></tr>";
            	foreach($prize as $item){
            		$item_prize = isset($arr_gift[$item->ResultCode]) ? $arr_gift[$item->ResultCode] : '--' ;
            		$item->Recive = $item->Recive==0 ? 'Chưa nhận' : 'Đã nhận' ;
            		echo "<tr>";
            		echo "<td>".$item_prize."</td>";
            		echo "<td>".$item->Created."</td>";
            		echo "<td>".$item->Recive."</td>";
            		echo "</tr>";
            	}
            	echo "</table>";
            	echo '</div>';
            }
    		?>
	    	<div class="alert alert-info">
	    		<p><b>Xác nhận đơn hàng</b></p>
	    		<p>Xác nhận đơn hàng <?php echo $data->MaDH ?> của Quý Khách gồm (<?php echo implode(', ',$details_products) ?>) tổng giá trị <?php echo number_format($tonggiatrithanhtoan) ?>đ giao hàng tại <?php echo $data->AddressOrder ?>, thời gian giao hàng (8h làm việc nếu nội thành TPHCM, 2-3 ngày nếu là ngoại thành hoặc các tỉnh). Chúc Quý Khách một ngày vui vẻ. Trân trọng.</p>
	    	</div>	
	    	<div class="block1">
	    		<input type="hidden" name="Ghichu" id="Ghichu" value="" />
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    	</div>
    	</form>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
    <input type="hidden" name="IDOrder" id="IDOrder" value="<?php echo $data->ID ?>" />
</div>
<div class="notify_current" style="display:none">
    <a class="numbernoti">1</a>
    <ul></ul>
</div>
<style>
    .daterangepicker{width: auto;}
</style>


<?php
    $max = $this->db->query("select Status,count(ID) as SL from ttp_report_order where Status in(4,6,1) and UserID=".$this->user->ID." group by Status")->result();
    $status4 = 0;
    $status6 = 0;
    $status1 = 0;
    if(count($max)>0){
        foreach($max as $row){
            $status4 = $row->Status==4 ? $row->SL : $status4 ;
            $status6 = $row->Status==6 ? $row->SL : $status6 ;
            $status1 = $row->Status==1 ? $row->SL : $status1 ;
        }
    }
?>
<script type="text/javascript">
	$(document).ready(function () {
        $('#Export_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

    var baselink = $("#baselink_report").val();
    var resultmax = <?php echo $status4 ?>;
    var resultmax1 = <?php echo $status6 ?>;
    var resultmax2 = <?php echo $status1 ?>;
    var getcurrentorder = function(){
        $.ajax({
            url: baselink+"import/get_current_order_department",
            dataType: "html",
            type: "POST",
            data: "department=1&max="+resultmax+"&max1="+resultmax1+"&max2="+resultmax2,
            success: function(result){
                if(result!="false"){
                    var ob = jQuery.parseJSON(result);
                    content = ob.content;
                    resultmax = ob.max1;
                    resultmax1 = ob.max2;
                    resultmax2 = ob.max3;
                    for (var key in content) {
                        if (content.hasOwnProperty(key)) {
                            notifyNewOrder(content[key].user,content[key].image,content[key].title,content[key].id);
                        }
                    }
                }
            }
        });
    }
    //setInterval(getcurrentorder,10000);
</script>

<script type="text/javascript">
    $(".numbernoti").click(function(){
        $(this).parent().find('ul').toggle();
    });

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	var link = $("#baselink_report").val();
	$("#find_customers").click(function(){
		var salesman = $("#SalesmanID").val();
		var sdt_cus  = $("#sdt_cus").val();
		var name_cus = $("#name_cus").val();
		if(salesman!='' && (sdt_cus!='' || name_cus!='')){
			enablescrollsetup();
			$(this).addClass('saving');
			$.ajax({
            	url: link+"import_order/getcus",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "CustomerID="+salesman+'&sdt_cus='+sdt_cus+"&name_cus="+name_cus,
	            success: function(result){
	                if(result!='false'){
	                	$(".over_lay .box_inner").css({'margin-top':'50px'});
	                	$(".over_lay .box_inner .block1_inner h1").html("Danh sách khách hàng");
	                	$(".over_lay .box_inner .block2_inner").html(result);
	                	$(".over_lay").removeClass('in');
	                	$(".over_lay").fadeIn('fast');
	                	$(".over_lay").addClass('in');
	                }else{
	                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
	                }
	                $(this).removeClass('saving');
	            }
	        });
		}else{
			if(salesman==''){
				alert("Vui lòng chọn tên nhân viên để thực hiện quá trình tìm kiếm");
			}else{
				alert("Vui lòng điền tên hoặc số dt của khách hàng để thực hiện quá trình tìm kiếm");
			}
		}
	});

	$("#add_products_to_order").click(function(){
		var KhoID = $('#KhoID').val();
		if(KhoID=='' || KhoID==0){
			alert('Vui lòng chọn kho lấy hàng');
			$('#KhoID').focus();
			return false;
		}
		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'50px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);
			    	$(".over_lay").removeClass('in');
                	$(".over_lay").fadeIn('fast');
                	$(".over_lay").addClass('in');
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		var KhoID = $('#KhoID').val();
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID+"&Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });
	}

	$.ajax({
    	url: link+"import_order/get_targets_user",
        dataType: "html",
        type: "POST",
        context: this,
        data: "IDOrder=<?php echo $data->ID ?>&UserID=<?php echo $data->UserID ?>",
        success: function(result){
        	if(result!='fasle')
        	$(".table_absolute").html(result);
        }
    });

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_city_by_area",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            	$("#KhoID").html('<option value="">Không có kho hàng</option>');
	            	$("#KhoID").change();
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: link+"import_order/get_district_by_city",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	                var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            	$("#KhoID").html(jsonobj.WarehouseHtml);
	            	$("#KhoID").change();
	            }
	        });
		}
	});

	var statusloadhis = 0;
	$("#xemlichsugiaodich").click(function(){
		var datastatus = $("#xemlichsugiaodich").attr("data-status");
		if(datastatus=="down"){
			$("#xemlichsugiaodich").attr("data-status","up");
			$(this).html("Ẩn lịch sử giao dịch <i class='fa fa-caret-up'></i>");
		}else{
			$("#xemlichsugiaodich").attr("data-status","down");
			$(this).html("Xem lịch sử giao dịch <b class='caret'></b>");
		}
		var salesman = $("#SalesmanID").val();
		var ID = $("#CustomerID").val();
		if(ID!='' && ID>0){
			if(statusloadhis==0){
				$.ajax({
	            	url: link+"import_order/get_history",
		            dataType: "html",
		            type: "POST",
		            context: this,
		            data: "ID="+ID+"&UserID="+salesman,
		            success: function(result){
		            	statusloadhis=1;
		                if(result!='false'){
			                $(".lichsugiaodich .history_cus").html(result);
		                }else{
		                	$(".lichsugiaodich .history_cus").html("<p>Không có lịch sử giao dịch của khách hàng này.</p>");
		                }
		            }
		        });
	        }
		}
	});

	$("#edit_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var parent = $(this).parent('td').parent('tr');
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
			}
		});
	});

	function calchietkhau(ob){
		tongcong = parseFloat($(".tongcong").val());
		var classname = $(ob).attr('class');
		data = parseFloat($(ob).val());
		if(classname=="phantramchietkhau"){
			giachietkhau = data*(tongcong/100);
			$(".giachietkhau").val(giachietkhau);
			$("#showgiachietkhau").html(giachietkhau.format('a',3));
		}else{
			phantramchietkhau = data/(tongcong/100);
			$(".phantramchietkhau").val(phantramchietkhau.toFixed(0));
		}
		recal();
	}

	function calrowchietkhau(ob){
		var parent = $(ob).parent('td').parent('tr');
		tongcong = parseFloat(parent.find('input.dongia').val());
		var classname = $(ob).attr('class');
		data = parseFloat($(ob).val());
		if(classname=="percentck border"){
			giachietkhau = data*(tongcong/100);
			var giasauchietkhau = tongcong-giachietkhau;
			if(giasauchietkhau>=0){
				parent.find('input.giack').val(giachietkhau);
				parent.find('span.showgiaCK').html(giachietkhau.format('a',3));
				parent.find('span.showpercentCK').html(data.format('a',0));
			}
		}else{
			var giasauchietkhau = tongcong-data;
			if(giasauchietkhau>=0){
				phantramchietkhau = Math.round((data/(tongcong/100))*10)/10;
				parent.find('input.percentck').val(phantramchietkhau);
				parent.find('span.showpercentCK').html(phantramchietkhau);
				parent.find('span.showgiaCK').html(data.format('a',0));
			}
		}
		if(giasauchietkhau>=0){
			parent.find('span.showgiasauCK').html(giachietkhau.format('a',3));
			parent.find('input.giasauCK').val(giachietkhau);
			recal();
		}
	}

	function recal_total(){
		$(".chiphivanchuyen").hide();
		var tongcong = 0;
		var phantramchietkhau = parseFloat($(".phantramchietkhau").val());
		$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				soluong = parseFloat(parent.find('input.soluong').val());
				dongia = parseFloat(parent.find('input.dongia').val());
				percentck = parseFloat(parent.find('input.percentck').val());
				giack = parseFloat(parent.find('input.giack').val());
				giasauck = dongia-giack;
				thanhtien = soluong*giasauck;
				parent.find('input.thanhtien').val(thanhtien);
				parent.find('span.showthanhtien').html(thanhtien.format('a',3));
				parent.find('input.giasauCK').val(giasauck);
				parent.find('span.showgiasauCK').html(giasauck.format('a',3));
				tongcong = tongcong+thanhtien;
		});
		giachietkhau = parseFloat($(".giachietkhau").val());
		tongtienhang = tongcong-giachietkhau;
		$(".tongcong").val(tongcong);
		$(".tongtienhang").val(tongcong-giachietkhau);
		var chiphivanchuyen = parseFloat($(".chiphivanchuyen").val());
		$("#showchiphivanchuyen").html(chiphivanchuyen.format('a',3)).show();
		$(".tonggiatrithanhtoan").val(tongcong-giachietkhau+chiphivanchuyen);
		$("#showtongcong").html(tongcong.format('a',3));
		$("#showtongtienhang").html((tongcong-giachietkhau).format('a',3));
		$("#showgiatrithanhtoan").html((tongcong-giachietkhau+chiphivanchuyen).format('a',3));
	}

	function recal(){
		$(".chiphivanchuyen").hide();
		var tongcong = 0;
		var phantramchietkhau = parseFloat($(".phantramchietkhau").val());
		$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				soluong = parseFloat(parent.find('input.soluong').val());
				dongia = parseFloat(parent.find('input.dongia').val());
				percentck = parseFloat(parent.find('input.percentck').val());
				giack = parseFloat(parent.find('input.giack').val());
				giasauck = dongia-giack;
				thanhtien = soluong*giasauck;
				parent.find('input.thanhtien').val(thanhtien);
				parent.find('span.showthanhtien').html(thanhtien.format('a',3));
				parent.find('input.giasauCK').val(giasauck);
				parent.find('span.showgiasauCK').html(giasauck.format('a',3));
				tongcong = tongcong+thanhtien;
		});
		giachietkhau = parseFloat($(".giachietkhau").val());
		tongtienhang = tongcong-giachietkhau;
		$(".tongcong").val(tongcong);
		$(".tongtienhang").val(tongcong-giachietkhau);
		getfee(tongtienhang, function() {
				var chiphivanchuyen = parseFloat($(".chiphivanchuyen").val());
				$("#showchiphivanchuyen").html(chiphivanchuyen.format('a',3)).show();
				$(".tonggiatrithanhtoan").val(tongcong-giachietkhau+chiphivanchuyen);
				$("#showtongcong").html(tongcong.format('a',3));
				$("#showtongtienhang").html((tongcong-giachietkhau).format('a',3));
				$("#showgiatrithanhtoan").html((tongcong-giachietkhau+chiphivanchuyen).format('a',3));
    });
		changestatus();
	}

	function changestatus(){
		$("#IsChangeOrder").val("1");
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').hide();
				parent.find('span.showdongia').hide();
				parent.find('span.showpercentCK').hide();
				parent.find('span.showgiaCK').hide();
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input.dongia').prop('readonly', true).show();
				parent.find('input.percentck').prop('readonly', false).show();
				parent.find('input.giack').prop('readonly', false).show();
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
				parent.find('input.dongia').addClass('border');
				parent.find('input.percentck').addClass('border');
				parent.find('input.giack').addClass('border');
				parent.find('input.giasauck').addClass('border');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').show();
				parent.find('span.showdongia').show();
				parent.find('span.showpercentCK').show();
				parent.find('span.showgiaCK').show();
				parent.find('input[type="text"]').prop('readonly', true);
				parent.find('input.soluong').prop('readonly', true);
				parent.find('input.dongia').prop('readonly', true).hide();
				parent.find('input.percentck').prop('readonly', true).hide();
				parent.find('input.giack').prop('readonly', true).hide();
				parent.find('input[type="text"]').removeClass('border');
				parent.find('input.soluong').removeClass('border');
				parent.find('input.dongia').removeClass('border');
				parent.find('input.percentck').removeClass('border');
				parent.find('input.giack').removeClass('border');
				parent.find('input.giasauck').removeClass('border');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		if(ob.checked===true){
			parent.find('span.showsoluong').hide();
			parent.find('span.showdongia').hide();
			parent.find('span.showpercentCK').hide();
			parent.find('span.showgiaCK').hide();
			parent.find('input[type="text"]').prop('readonly', false);
			parent.find('input.soluong').prop('readonly', false);
			parent.find('input.dongia').prop('readonly', true).show();
			parent.find('input.percentck').prop('readonly', false).show();
			parent.find('input.giack').prop('readonly', false).show();
			parent.find('input[type="text"]').addClass('border');
			parent.find('input.soluong').addClass('border');
			parent.find('input.dongia').addClass('border');
			parent.find('input.percentck').addClass('border');
			parent.find('input.giack').addClass('border');
			parent.find('input.giasauck').addClass('border');
		}else{
			parent.find('span.showsoluong').show();
			parent.find('span.showdongia').show();
			parent.find('span.showpercentCK').show();
			parent.find('span.showgiaCK').show();
			parent.find('input[type="text"]').prop('readonly', true);
			parent.find('input.soluong').prop('readonly', true);
			parent.find('input.dongia').prop('readonly', true).hide();
			parent.find('input.percentck').prop('readonly', true).hide();
			parent.find('input.giack').prop('readonly', true).hide();
			parent.find('input[type="text"]').removeClass('border');
			parent.find('input.soluong').removeClass('border');
			parent.find('input.dongia').removeClass('border');
			parent.find('input.percentck').removeClass('border');
			parent.find('input.giack').removeClass('border');
			parent.find('input.giasauck').removeClass('border');
		}
	}

	function select_this_custom(ob,id){
		$(".over_lay").hide();
		disablescrollsetup();
		var name = $(ob).parent('td').parent('tr').find('td.name_td a').html();
		$("#CustomerID").val(id);
		$("#Tenkhachhang").val(name);
		$(".lichsugiaodich").show();
	}

	var celldata = [<?php echo implode(',',$arrproducts) ?>];
	var sttrow = <?php echo count($arrproducts) ?>+1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML="<span class='ShipmentDefault' data-id='"+data_id+"'></span>";
					data_price = parseFloat(data_price);
					row.insertCell(4).innerHTML="";
					row.insertCell(5).innerHTML="<input type='number' name='Amount[]' class='soluong' value='1' min='1' onchange='recal()' readonly='true' />";
					row.insertCell(6).innerHTML="<input type='number' class='dongia' value='"+data_price+"' min='1' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(7).innerHTML="<input type='number' name='PercentCK[]' onchange='calrowchietkhau(this)' class='percentck' value='0' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(8).innerHTML="<input type='number' name='GiaCK[]' onchange='calrowchietkhau(this)' class='giack' value='0' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(9).innerHTML="<input type='number' name='Price[]' class='giasauCK' value='"+data_price+"' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(10).innerHTML="<input type='number' class='thanhtien' value='"+data_price+"' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
			}
		});
		loadshipmentdefault();
		$(".over_lay").hide();
		disablescrollsetup();
		recal();
	}

	function checkshipment(ob){
		var amount = $(ob).val();
		var warehouse = $('#KhoID').val();
		var id = $(ob).parent('td').parent('tr').find('.ShipmentDefault').attr('data-id');
		$(ob).parent('td').parent('tr').find('.ShipmentDefault').load(link+"import_order/get_shipment_default/"+id+"/"+warehouse+"/"+amount);
		recal();
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoID').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			var amount = $(this).parent('td').parent('tr').find('.soluong').val();
			$(this).load(link+"import_order/get_shipment_default/"+data+"/"+warehouse+"/"+amount);
		});
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				var index = celldata.indexOf("data"+data_id);
				celldata.splice(index, 1);
				sttrow = sttrow-1;
			}
		});
		recal();
	});

	function submitform(ob,status){
		$("#Status").val(status);
		if(status!=<?php echo $data->Status ?>){
			var getValue = prompt("Điền ghi chú thay đổi (nếu có) : ", "");
			if(getValue!=null){
				$("#Ghichu").val(getValue);
			}
		}
		new ERPPlugin(ob).submitform();
	}

	function showchiphi(){
		$("#showchiphivanchuyen").hide();
		$(".chiphivanchuyen").show();
		$(".chiphivanchuyen").focus();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function send_cancel_request(ob){
		if($("#Follow").prop('checked')==true){
			var checked_follow=1;
		}else{
			var checked_follow=0;
		}
		var note = $("#Lydohuydh").val();
		var another = $("#Lydokhac").val();
		var links_request = $(ob).attr('data');
		if(note=='' || (note==0 && another=='')){
			alert("Vui lòng chọn lý do hủy đơn hàng !");
			$("#Lydohuydh").focus();
		}else{
			$.ajax({
            	url: links_request,
	            dataType: "html",
	            type: "POST",
	            data: "ReasonID="+note+"&Note="+another+"&Follow="+checked_follow,
	            success: function(result){
	            	window.location=link+"import_order";
	            }
	        });
		}
	}

	function checkshowlydokhac(ob){
		var data = $(ob).val();
		if(data==0){
			$(".lydokhac").find('input').focus();
			$(".lydokhac").show();
		}else{
			$(".lydokhac").hide();
		}
	}

	function show_cancelbox(){
		$(".cancel_box").toggle();
	}

    function transportInfo(ob){
        var order_id = $('#IDOrder').val();
        $.ajax({
            url: link+"import_order/get_transport_info",
            dataType: "html",
            type: "POST",
            data: "transport=<?=$data->TransportID;?>&order_id="+order_id,
            success: function(result){
            	$(".over_lay .box_inner").css({'margin-top':'20px','width':'30%'});
                $(".over_lay .box_inner .block1_inner h1").html("Thông tin vận chuyển");
                $(".over_lay .box_inner .block2_inner").html(result);
                $(".over_lay").removeClass('in');
                $(".over_lay").fadeIn('fast');
                $(".over_lay").addClass('in');
            }
        });
	}

	function notifyNewOrder(title,image,message,clicknoti){
        notify(title, {
            body: message,
            icon: image,
            onclick: function(e) {
                window.location=baselink+"import_order/edit/"+clicknoti;
            },
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }

    function assign(ob){
    	var userassign = $(ob).val();
    	if(userassign>0){
    		var r = confirm("Bạn có chắc muốn chuyển quyền sở hữu đơn hàng này ?");
	        if (r == false) {
	            return false;
	        }else{
	    		$("#alertmessage").html("<p class='alert alert-warning'>Hệ thống đang xử lý ...</p>");
	    		$("#alertmessage").load(baselink+"import_order/assign/<?php echo $data->ID ?>/"+userassign);
	    	}
        }
    }

    function send_to_supplier(ob,detailsid){
    	var sendstate = $(ob).attr('data-state');
    	if(sendstate==1){
    		alert('Bạn đã gửi yêu cầu rồi !');
    		return;
    	}
    	$(ob).addClass("saving");
    	$.ajax({
            url: link+"import_order/request_single_available_to_supplier",
            dataType: "html",
            type: "POST",
            data: "id="+detailsid,
            success: function(result){
            	console.log(result);
            	$(ob).removeClass('saving');
            	$(ob).html("Đã gửi");
                $(ob).attr('data-state',1);
            }
        });
    }

		function checkvoucher(ob){
			var voucher = $("#voucher").val();
			if(voucher==''){
					alert("Vui lòng nhập mã giảm giá");
					$("#voucher").focus();
					return;
			}
			$.ajax({
					url: link+"import_order/checkvoucher",
					dataType: "json",
					type: "POST",
					context: this,
					data: "Voucher="+voucher,
					success: function(result){
							alert(result.Message);
							if(result.Errorcode==0){
									tongcong = parseFloat($(".tongcong").val());
									if(result.type==1){
											$(".phantramchietkhau").val(result.data);
											$(".phantramchietkhau").attr("value",result.data);
											$(".phantramchietkhau").change();
									}
									if(result.type==0){
											giachietkhau = parseFloat(result.data);
											$(".giachietkhau").val(giachietkhau);
											$(".giachietkhau").attr("value",giachietkhau);
											$("#showgiachietkhau").html(giachietkhau.format('a',3));
											$(".giachietkhau").change();
									}
							}
					}
			});
		}

		function getfee(total,callback){
				var district = $("#Quanhuyen").val();
				$.ajax({
						url: link+"import_order/getfee",
						dataType: "json",
						type: "POST",
						data: "district="+district+"&total="+total,
						success: function(result){
								$(".chiphivanchuyen").val(result);
								$(".chiphivanchuyen").attr("value",result);
								$("#showchiphivanchuyen").html(result.format('a',3)).show();
								callback();
						}
				});
		}
</script>
