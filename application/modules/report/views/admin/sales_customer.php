<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$baselink = ADMINPATH.'/report/import_order/';
$numday = $this->lib->get_nume_day($startday,$stopday);
?>
<div class="containner">
    <div class="import_select_progress row">
        <div class="block1 col-xs-12 col-sm-12 col-md-4">
            <h1>Báo cáo khách hàng</h1>
        </div>
        <div class="block2 col-xs-12 col-sm-12 col-md-8">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="btn-group">
                        <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Lọc khách hàng</a>
                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
    						<?php 
    						$type_color1 = !isset($_GET['cus_type']) ? "color:#090" : "color:#ccc" ;
    						$type_color2 = isset($_GET['cus_type']) && $_GET['cus_type']==0 ? "color:#090" : "color:#ccc" ;
    						$type_color3 = isset($_GET['cus_type']) && $_GET['cus_type']==1 ? "color:#090" : "color:#ccc" ;
    						?>
                            <li><a href="<?php echo $base_link.'report_customer' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type_color1 ?>" aria-hidden="true"></i> Tất cả khách hàng</a></li>
                            <li><a href="<?php echo $base_link.'report_customer?cus_type=0' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type_color2 ?>" aria-hidden="true"></i> Chỉ khách hàng mới</a></li>
                            <li><a href="<?php echo $base_link.'report_customer?cus_type=1' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type_color3 ?>" aria-hidden="true"></i> Chỉ khách hàng cũ</a></li>
                        </ul>
                    </div>
                </div>
                <div id="reportrange" class="list_div col-xs-12 col-sm-6 text-center">
                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                    <span></span> <b class="caret"></b>
                </div>
            </div>
        </div>
        
    </div>
    <?php 
    $arr = array();
    for($temp=1;$temp<11;$temp++){
        $arr_buy[$temp] = array('SLDH'=>0,'PRICE'=>0,'OLD'=>0,'NEW'=>0,'ORDER'=>0);
    }
    
    $arr_total = array('CUSTOMER'=>array(),'ORDER'=>0,'PRICE'=>0);
    $arr_type_customer = array('new'=>0,'old'=>0);
    $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#EEEEEE");
    $arr_chart = array();
    $arr_source = array();
    $arr_area = array();
    $arr_channel = array();
	$totalorder = 0;
    if(count($data)>0){
        foreach($data as $row){
            $totalorder = $totalorder+1;
            $total = $row->Total-$row->Chietkhau-$row->Reduce+$row->Chiphi;
			
            $arr_total['ORDER'] = $arr_total['ORDER']+1;
            $arr_total['PRICE'] = $arr_total['PRICE']+$total;
            if(!isset($arr_total['CUSTOMER'][$row->CustomerID])){
                $arr_total['CUSTOMER'][$row->CustomerID]['NAME'] = $row->Name;
                $arr_total['CUSTOMER'][$row->CustomerID]['DH']=1;
                $arr_total['CUSTOMER'][$row->CustomerID]['PRICE']=$total;

                /* Source data */
                if(isset($arr_source[$row->SourceID])){
                    $arr_source[$row->SourceID] = $arr_source[$row->SourceID]+1;
                }else{
                    $arr_source[$row->SourceID] = 1;
                }

                /* Area data */
                if(isset($arr_area[$row->AreaID])){
                    $arr_area[$row->AreaID] = $arr_area[$row->AreaID]+1;
                }else{
                    $arr_area[$row->AreaID] = 1;
                }

                /* Channel data */
                if(isset($arr_channel[$row->KenhbanhangID])){
                    $arr_channel[$row->KenhbanhangID] = $arr_channel[$row->KenhbanhangID]+1;
                }else{
                    $arr_channel[$row->KenhbanhangID] = 1;
                }                

            }else{
                $arr_total['CUSTOMER'][$row->CustomerID]['DH'] = $arr_total['CUSTOMER'][$row->CustomerID]['DH']+1;
                $arr_total['CUSTOMER'][$row->CustomerID]['PRICE'] = $arr_total['CUSTOMER'][$row->CustomerID]['PRICE']+$total;
            }
            if($row->CustomerType==0){
                $arr_type_customer['new'] = $arr_type_customer['new']+1;
                $arr_total['CUSTOMER'][$row->CustomerID]['NEW'] = 1;
            }else{
                $arr_type_customer['old'] = $arr_type_customer['old']+1;
                $arr_total['CUSTOMER'][$row->CustomerID]['OLD'] = 1;
            }
        }
    }
    if(count($arr_total['CUSTOMER'])>0){
        foreach($arr_total['CUSTOMER'] as $key=>$row){
            if(!isset($row['OLD'])){
                $row['OLD']=0;
            }
            if(!isset($row['NEW'])){
                $row['NEW']=0;
            }
            if($row['DH']>9){
                $arr_buy[10]['ORDER'] = $arr_buy[10]['ORDER']+$row['DH'];
                $arr_buy[10]['SLDH'] = $arr_buy[10]['SLDH']+1;
                $arr_buy[10]['PRICE'] = $arr_buy[10]['PRICE']+$row['PRICE'];
                $arr_buy[10]['OLD'] = $arr_buy[10]['OLD']+$row['OLD'];
                $arr_buy[10]['NEW'] = 0;
            }elseif($row['DH']>1){
                $arr_buy[$row['DH']]['ORDER'] = $arr_buy[$row['DH']]['ORDER']+$row['DH'];
                $arr_buy[$row['DH']]['SLDH'] = $arr_buy[$row['DH']]['SLDH']+1;
                $arr_buy[$row['DH']]['PRICE'] = $arr_buy[$row['DH']]['PRICE']+$row['PRICE'];
                $arr_buy[$row['DH']]['OLD'] = $arr_buy[$row['DH']]['OLD']+$row['OLD'];
                $arr_buy[$row['DH']]['NEW'] = 0;
            }elseif($row['DH']==1){
                $arr_buy[$row['DH']]['ORDER'] = $arr_buy[$row['DH']]['ORDER']+$row['DH'];
                $arr_buy[$row['DH']]['SLDH'] = $arr_buy[$row['DH']]['SLDH']+1;
                $arr_buy[$row['DH']]['PRICE'] = $arr_buy[$row['DH']]['PRICE']+$row['PRICE'];
                $arr_buy[$row['DH']]['OLD'] = $arr_buy[$row['DH']]['OLD']+$row['OLD'];
                $arr_buy[$row['DH']]['NEW'] = $arr_buy[$row['DH']]['NEW']+$row['NEW'];
            }
        }
    }

    $abs_order = $totalorder==0 ? 0 : $arr_total['PRICE']/$totalorder;

    ?>
    <div class="row" style="margin-top: 30px;">
        <div class="col-xs-12 col-sm-12 col-md-7">
            <table class='table' style="margin-bottom:20px">
                <tr>
                    <td>TỔNG SỐ KHÁCH HÀNG</td>
                    <td>: <b><?php echo number_format(count($arr_total['CUSTOMER'])); ?></b></td>
                </tr>
                <tr>
                    <td>TỔNG SỐ LƯỢNG ĐƠN HÀNG</td>
                    <td>: <b><?php echo number_format($arr_total['ORDER']) ?></b></td>
                </tr>
                <tr>
                    <td>TỔNG GIÁ TRỊ ĐƠN HÀNG</td>
                    <td>: <b><?php echo number_format($arr_total['PRICE']) ?></b></td>
                </tr>
                <tr>
                    <td>GIÁ TRỊ ĐƠN HÀNG TB</td>
                    <td>: <b><?php echo number_format($abs_order) ?></b></td>
                </tr>
            </table>
			<div class="col-xs-4 block_customerlist">
				<p>Tỷ lệ KH quay lại</p>
				<h3 class="tylekhachhangquaylai"></h3>
			</div>
			<div class="col-xs-4 block_customerlist"> 
				<p>Tần suất mua hàng</p>
				<?php 
				$tansuatmuahang = count($arr_total['CUSTOMER'])==0 ? 0 : round($totalorder/count($arr_total['CUSTOMER']),2);
				?>
				<h3 class="tansuatmuahang"><?php echo $tansuatmuahang ?></h3>
			</div>
			<div class="col-xs-4 block_customerlist" style="border-right:0px">
				<p>TG mua hàng TB</p>
				<?php 
				$thoigianmuahang = $tansuatmuahang==0 ? 0 : round($numday/$tansuatmuahang);
				?>
				<h3 class="thoigianmuahangtrungbinh"><?php echo $thoigianmuahang ?> ngày</h3>
			</div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-5">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-12">
                    <canvas id="canvas_pie"></canvas>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-12">
                    <?php 
                    $totalpercent = $totalorder;
                    if(count($arr_type_customer)>0){
                        echo "<table class='table'><tr><th></th><th>Loại khách hàng</th><th>Tỷ lệ</th></tr>";
                        $i=0;
                        $arrname = array("new"=>"ĐH KH mới","old"=>"ĐH KH cũ");
                        foreach($arr_type_customer as $key=>$value){
                            $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                            $percent = $totalpercent==0 ? 0 : round($value/($totalpercent/100),1);
                            echo "<tr><td>$color</td><td>".$arrname[$key]."</td><td>$percent %</td></tr>";
                            $arr_chart[] = "{
                                                value: ".$value.",
                                                color: '".$array_color[$i]."',
                                                highlight: '".$array_color[$i]."',
                                                label: '".$arrname[$key]."'
                                            }";
                            $i++;
                        }
                        echo "</table>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12"><h1 style="font-size:20px;font-weight:bold;margin-top:30px;text-transform:uppercase;border-bottom: 1px solid #E1e1e1;padding-bottom: 10px;">Phân loại khách hàng theo lần mua hàng (tần suất)</h1></div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table_data">
            <table class="table table-hover">
                <tr>
                    <th>Số lần mua</th>
                    <th>Số lượng KH</th>
                    <th>Tỷ lệ</th>
                    <th>Giá trị đơn hàng trung bình</th>
                    <th>Khách hàng cũ</th>
                    <th>Khách hàng mới</th>
                    <th>Thao tác</th>
                </tr>
				<?php 
				$total_SLDH = 0;
				$total_OLD = 0;
				$total_NEW = 0;
				$total_ABS = 0;
                foreach($arr_buy as $key=>$row){
                    $temp = $key;
                    if($key>=10){
                        $key = ">".$key;
                    }
                    $abs = $row['ORDER']==0 ? 0 : $row['PRICE']/$row['ORDER'];
					$total_SLDH = $total_SLDH+$row['SLDH'];
					$total_OLD = $total_OLD+$row['OLD'];
					$total_NEW = $total_NEW+$row['NEW'];
					$total_ABS = $total_ABS+$abs;
                    $percent_customer = count($arr_total['CUSTOMER'])==0 ? 0 : round($row['SLDH']/(count($arr_total['CUSTOMER'])/100),1);
                    echo "<tr>";
                    echo "<td>$key</td>";
                    echo "<td>".number_format($row['SLDH'])."</td>";
                    echo "<td>".$percent_customer."%</td>";
                    echo "<td>".number_format($abs)."</td>";
                    echo "<td>".number_format($row['OLD'])."</td>";
                    echo "<td>".number_format($row['NEW'])."</td>";
                    echo "<td><a onclick='loaddetails_customer(this,$temp)'><i class='fa fa-bar-chart' aria-hidden='true'></i> Xem chi tiết</a></td>";
                    echo "</tr>";
                }
				$first = isset($arr_buy[1]['SLDH']) ? $arr_buy[1]['SLDH'] : 0 ;
				$tylekhachhangquaylai = $total_SLDH ==0 ? 0 : ($total_SLDH-$first)/($total_SLDH/100) ;
                ?>
				<tr>
                    <th>TỔNG CỘNG</th>
                    <th><?php echo number_format($total_SLDH) ?></th>
                    <th>100%</th>
                    <th><?php echo number_format($abs_order) ?></th>
                    <th><?php echo number_format($total_OLD) ?></th>
                    <th><?php echo number_format($total_NEW) ?></th>
                    <th></th>
                </tr>
            </table>
            </div>
        </div>  
    </div>
    <div class="row">
        <div class="col-xs-12"><h1 style="font-size:20px;font-weight:bold;margin-top:30px;text-transform:uppercase;border-bottom: 1px solid #E1e1e1;padding-bottom: 10px;"><a onclick="showpie(this,2)" class='field_pie current'>Nguồn đặt hàng</a> / <a onclick="showpie(this,1)" class='field_pie'>Khu vực</a> / <a onclick="showpie(this,5)" class='field_pie'>Kênh bán hàng</a></h1></div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <canvas id="canvas_pie1"></canvas>
            <canvas id="canvas_pie2"></canvas>
            <canvas id="canvas_pie5"></canvas>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <table class="table table-hover table1">
                <tr>
                    <th></th>
                    <th>Khu vực</th>
                    <th>Số lượng KH</th>
                    <th>Tỷ lệ</th>
                </tr>
                <?php 
                $area = $this->db->query("select * from ttp_report_area")->result();
                $arr_chart1 = array();
                if(count($area)>0){
                    $i=0;
                    foreach($area as $row){
                        $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                        $data_area = isset($arr_area[$row->ID]) ? $arr_area[$row->ID] : 0 ;
                        $percent_area = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_area/(count($arr_total['CUSTOMER'])/100),1);
                        echo "<tr>";
                        echo "<td>$color</td>";
                        echo "<td>$row->Title</td>";
                        echo "<td>".number_format($data_area)."</td>";
                        echo "<td>$percent_area %</td>";
                        echo "</tr>";
                        $arr_chart1[] = "{
                                            value: ".$data_area.",
                                            color: '".$array_color[$i]."',
                                            highlight: '".$array_color[$i]."',
                                            label: 'Khu vực $row->Title'
                                        }";
                        $i++;
                    }
                    $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                    $data_area = isset($arr_area[0]) ? $arr_area[0] : 0 ;
                    $percent_area = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_area/(count($arr_total['CUSTOMER'])/100),1);
                    echo "<tr>";
                    echo "<td>$color</td>";
                    echo "<td>Không xác định</td>";
                    echo "<td>".number_format($data_area)."</td>";
                    echo "<td>$percent_area %</td>";
                    echo "</tr>";
                    $arr_chart1[] = "{
                                        value: ".$data_area.",
                                        color: '".$array_color[$i]."',
                                        highlight: '".$array_color[$i]."',
                                        label: 'Khu vực $row->Title'
                                    }";
                }
                ?>
                <tr>
                    <th></th>
                    <th>TỔNG CỘNG</th>
                    <th><?php echo number_format(count($arr_total['CUSTOMER'])) ?></th>
                    <th>100%</th>
                </tr>
            </table>

            <table class="table table-hover table2">
                <tr>
                    <th></th>
                    <th>Nguồn khách hàng</th>
                    <th>Số lượng KH</th>
                    <th>Tỷ lệ</th>
                </tr>
                <?php 
                $source = $this->db->query("select * from ttp_report_source")->result();
                $arr_chart2 = array();
                if(count($source)>0){
                    $i=0;
                    foreach($source as $row){
                        $array_color[$i] = isset($array_color[$i]) ? $array_color[$i] : '#ccc' ;
                        $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                        $data_source = isset($arr_source[$row->ID]) ? $arr_source[$row->ID] : 0 ;
                        $percent_source = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_source/(count($arr_total['CUSTOMER'])/100),1);
                        echo "<tr>";
                        echo "<td>$color</td>";
                        echo "<td>$row->Title</td>";
                        echo "<td>".number_format($data_source)."</td>";
                        echo "<td>$percent_source %</td>";
                        echo "</tr>";
                        $arr_chart2[] = "{
                                            value: ".$data_source.",
                                            color: '".$array_color[$i]."',
                                            highlight: '".$array_color[$i]."',
                                            label: 'Nguồn từ $row->Title'
                                        }";
                        $i++;
                    }
                    $array_color[$i] = isset($array_color[$i]) ? $array_color[$i] : '#ccc' ;
                    $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                    $data_source = isset($arr_source[0]) ? $arr_source[0] : 0 ;
                    $percent_source = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_source/(count($arr_total['CUSTOMER'])/100),1);
                    echo "<tr>";
                    echo "<td>$color</td>";
                    echo "<td>Không xác định</td>";
                    echo "<td>".number_format($data_source)."</td>";
                    echo "<td>$percent_source %</td>";
                    echo "</tr>";
                    $arr_chart2[] = "{
                                        value: ".$data_source.",
                                        color: '".$array_color[$i]."',
                                        highlight: '".$array_color[$i]."',
                                        label: 'Nguồn từ $row->Title'
                                    }";
                }
                ?>
                <tr>
                    <th></th>
                    <th>TỔNG CỘNG</th>
                    <th><?php echo number_format(count($arr_total['CUSTOMER'])) ?></th>
                    <th>100%</th>
                </tr>
            </table>

            <table class="table table-hover table5">
                <tr>
                    <th></th>
                    <th>Kênh khách hàng</th>
                    <th>Số lượng KH</th>
                    <th>Tỷ lệ</th>
                </tr>
                <?php 
                $salechannel = $this->db->query("select * from ttp_report_saleschannel")->result();
                $arr_chart3 = array();
                if(count($salechannel)>0){
                    $i=0;
                    foreach($salechannel as $row){
                        $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                        $data_channel = isset($arr_channel[$row->ID]) ? $arr_channel[$row->ID] : 0 ;
                        $percent_source = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_channel/(count($arr_total['CUSTOMER'])/100),1);
                        echo "<tr>";
                        echo "<td>$color</td>";
                        echo "<td>$row->Title</td>";
                        echo "<td>".number_format($data_channel)."</td>";
                        echo "<td>$percent_source %</td>";
                        echo "</tr>";
                        $arr_chart3[] = "{
                                            value: ".$data_channel.",
                                            color: '".$array_color[$i]."',
                                            highlight: '".$array_color[$i]."',
                                            label: 'Kênh bán hàng $row->Title'
                                        }";
                        $i++;
                    }
                    $color = "<span style='width:15px;height:15px;float:left;background:".$array_color[$i]."'></span>";
                    $data_channel = isset($arr_channel[0]) ? $arr_channel[0] : 0 ;
                    $percent_source = count($arr_total['CUSTOMER'])==0 ? 0 : round($data_channel/(count($arr_total['CUSTOMER'])/100),1);
                    echo "<tr>";
                    echo "<td>$color</td>";
                    echo "<td>Không xác định</td>";
                    echo "<td>".number_format($data_channel)."</td>";
                    echo "<td>$percent_source %</td>";
                    echo "</tr>";
                    $arr_chart3[] = "{
                                        value: ".$data_channel.",
                                        color: '".$array_color[$i]."',
                                        highlight: '".$array_color[$i]."',
                                        label: 'Kênh bán hàng $row->Title'
                                    }";
                }
                ?>
                <tr>
                    <th></th>
                    <th>TỔNG CỘNG</th>
                    <th><?php echo number_format(count($arr_total['CUSTOMER'])) ?></th>
                    <th>100%</th>
                </tr>
            </table>

        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">
	$(".tylekhachhangquaylai").html('<?php echo round($tylekhachhangquaylai,1) ?> %'); 
	
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var sharePiePolorDoughnutData = [
        <?php 
        echo implode(',', $arr_chart);
        ?>
    ];

    var sharePiePolorDoughnutData1 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];

    var sharePiePolorDoughnutData2 = [
        <?php 
        echo implode(',', $arr_chart2);
        ?>
    ];

    var sharePiePolorDoughnutData5 = [
        <?php 
        echo implode(',', $arr_chart3);
        ?>
    ];


    $(document).ready(function () {
        window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_pie1").getContext("2d")).Pie(sharePiePolorDoughnutData1, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_pie2").getContext("2d")).Pie(sharePiePolorDoughnutData2, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_pie5").getContext("2d")).Pie(sharePiePolorDoughnutData5, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

    });

    function showpie(ob,num){
		$(".field_pie").removeClass("current");
        $(ob).addClass("current");
        $("#canvas_pie1").hide();
        $("#canvas_pie2").hide();
        $("#canvas_pie5").hide();
        $(".table1").hide();
        $(".table2").hide();
        $(".table5").hide();
        $("#canvas_pie"+num).show();
        $(".table"+num).show();
    }

    function loaddetails_customer(ob,num){
        $(".over_lay").html('');
        $(window).scrollTop(70);
        $(".over_lay").addClass("black");
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
        $(".over_lay").append("<div class='loader'></div>");
        $(".over_lay").load("<?php echo $base_link ?>load_data_customer_repeat/"+num+"/1");
    }

    function load_page_nav(num,page){
        $(window).scrollTop(70);
        $(".over_lay").html('');
        $(".over_lay").append("<div class='loader'></div>");
        $(".over_lay").load("<?php echo $base_link ?>load_data_customer_repeat/"+num+"/"+page);
    }

    function show_list_order(ob,customer){
        $(ob).parent(".col-xs-2").parent(".form-group").find("div").css({'background':'#EEE','padding':'5px 0px'});
        var object = $(ob).parent(".col-xs-2").parent(".form-group").parent(".row").find(".showlisthere");
        object.load("<?php echo $base_link ?>load_order_by_customer_repeat/"+customer);
    }
   
</script>
<style>
    .version{display:none;}
    .copyright{display:none;}
    .table tr:first-child th{border-top:none;}
	.table tr:last-child th{font-size:17px;}
	.table tr:first-child td{border-top:none;}
    .table tr td b{font-size:20px;font-weight:normal;}
    .table tr td:first-child{line-height:30px;}
    #canvas_pie1{display:none;}
    #canvas_pie5{display:none;}
    .table1{display:none;}
    .table5{display:none;}
	.field_pie.current{color:#F00;}
	.block_customerlist{border-right: 1px solid #dddddd;background: #ddfbe5;padding: 10px;}
	.block_customerlist p{text-transform:uppercase;}
    .over_lay .loader{margin-top:200px;}
</style>