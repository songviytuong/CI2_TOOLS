<div class="containner">
	<div class="bill_print export_internal">
		<div class="block1">
			<table id="table1">
				<tr>
					<td rowspan="3"><img src='public/admin/images/logo.png' /></td>
					<td rowspan="3"><h1>PHIẾU XUẤT KHO</h1></td>
					<td>Mã: TP.FA.DI.15.FM07</td>
				</tr>
				<tr>
					<td>PB: 00</td>
				</tr>
				<tr>
					<td>NBH: 01/12/2015</td>
				</tr>
			</table>
			<div class="block1_2">
				<?php 
				$export = $this->db->query("select * from ttp_report_export_warehouse where OrderID=$data->ID")->row();
				?>
				<a id="back_page" style='left:5px;top:40px' href='<?php echo base_url().ADMINPATH."/report/warehouse_scancode/export_another" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<?php 
				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=2 and TypeExport=0")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('Y',time());
                if($thisyear=='2016' && $thismonth=='03'){
                    $max = $max+46;
                }
                $max = "XKLO".$thisyear.$thismonth.'.'.str_pad($max, 5, '0', STR_PAD_LEFT);
				?>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaXK : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $export ? $export->TKNO : "................." ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $export ? $export->TKCO : "................." ; ?>" /></td>
					</tr>
				</table>
				<input type="hidden" id="KPP" value="LO" />
			</div>
		</div>

		<div class="block2">
			<?php 
    		$data_note = json_decode($data->Note,true);
    		$data_note = is_array($data_note) ? $data_note : array() ;
    		?>
			<div class="row">
				<li>Họ tên người nhận</li>
				<li>: <?php echo isset($data_note['Nguoinhanhang']) ? $data_note['Nguoinhanhang'] : '' ; ?> </li>
				<li>Phòng ban / bộ phận</li>
				<li> : <?php echo isset($data_note['Phongban']) ? $data_note['Phongban'] : '' ; ?></li>
			</div>
			<div class="row">
				<li>Địa chỉ nhận hàng</li>
				<li class="special">: <?php echo $data->AddressOrder ?></li>
			</div>
			<div class="row">
				<li>Lý do xuất hàng</li>
				<li class="special">: <?php echo isset($data_note['Note']) ? $data_note['Note'] : '' ; ?></li>
			</div>
			<div class="row">
				<li>Xuất tại kho</li>
				<li>: 
					<?php echo $data->KhoTitle ?>
					<input type="hidden" id="KhoID" value="<?php echo $data->KhoID ?>" />
					<input type="hidden" name="hinhthucxuatkho" id="hinhthucxuatkho" value="2" />
				</li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th rowspan='2'>Số TT</th>
					<th rowspan='2'>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th rowspan='2'>Mã số</th>
					<th rowspan='2'>Số lô</th>
					<th rowspan='2'>Đơn vị tính</th>
					<th colspan='2'>Số lượng</th>
					<th rowspan='2'>Đơn giá</th>
					<th rowspan='2'>Thành tiền (VNĐ)</th>
					<th rowspan='2'>Ghi chú</th>
				</tr>
				<tr>
					<th style="width:100px">Theo chứng từ</th>
					<th>Thực xuất</th>
				</tr>
				<?php 
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,b.*,c.ShipmentCode from ttp_report_products a,ttp_report_orderdetails b,ttp_report_shipment c where b.ShipmentID=c.ID and a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						$giaban = $row->Price+$row->PriceDown;
    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->ShipmentCode</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>$row->Amount</td>";
						echo "<td>".number_format($row->Price)."</td>";
						echo "<td>".number_format($row->Total)."</td>";
						echo "<td>$row->Khuyenmai</td>";
						echo "</tr>";
						$i++;
					}
				}
				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
				$tongtienhang = $data->Total - $data->Chietkhau;
		    	$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
				?>
				
				<tr>
					<td></td>
					<td>TỔNG CỘNG</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>
					<?php echo number_format($data->Total) ?>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="block5">
			<p class="title">- Tổng số tiền viết bằng chữ : </p>
			<p class="title">- Số chứng từ gốc kèm theo : </p>
			<div>
				<p><span>1. Phiếu yêu cầu xuất kho số </span>: .............................................................................................</p>
				<p><span>2. Lệnh sản xuất số </span>: .............................................................................................</p>
				<p><span>3. Chứng từ khác số </span>: .............................................................................................</p>
			</div>
		</div>
		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
				<div></div>
				<p><?php echo date("H:i A d/m/Y",strtotime($data->Ngaydathang)) ?></p>
				<p>In từ tools.trantoanphat.com <br><?php echo date("H:i A d/m/Y",time()) ?></p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Trương phòng Logistic</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Kế toán trưởng</p>
				<p>(Ký, họ tên)</p>
			</div>
		</div>
	</div>
</div>
