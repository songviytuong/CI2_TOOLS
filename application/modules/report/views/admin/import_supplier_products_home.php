<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<form action="<?php echo $base_link ?>setsessionsearch" method="post">
					<a class="pull-left btn"><i class="fa fa-search" style="font-size:16px"></i></a><input type="text" value='<?php echo $keywords ?>' class="form-control" name="keywords" placeholder="Tìm nhà cung cấp / tên sản phẩm / mã sản phẩm" style="width:350px;border:none;border-bottom:1px solid #e1e1e1;box-shadow:none;padding-left:0px;padding-right:0px;" />
				</form>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'export_products_supplier'; ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Mã sản phẩm NCC</th>
					<th>Tên sản phẩm</th>
					<th>Đơn vị</th>
					<th>Giá bán cho UCC</th>
					<th>Giá bán lẻ đề nghị</th>
					<th>Nhà cung cấp</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->BarcodeClient</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td class='text-right'>".number_format($row->RootPrice)."</td>";
						echo "<td class='text-right'>".number_format($row->PriceSuggest)."</td>";
						echo "<td>$row->SupplierTitle</td>";
						echo "</tr>";
					}
				}else{
					echo "<tr><td colspan='7'>Không tìm thấy dữ liệu .</td></tr>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>