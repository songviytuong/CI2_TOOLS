<div class="containner">
		<div class="import_orderlist">
			<div class="block2">
	    		<div class="block_2_1">
	    			<a class="btn btn-danger" href="<?php echo $base_link.'add'; ?>"><i class="fa fa-plus"></i> Tạo tài khoản</a>
	    		</div>
	    		<div class="block_2_2">
	    			
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>UserName</th>
						<th>Name</th>
						<th>Hệ thống website</th>
						<th>Created</th>
						<th>Trạng thái</th>
						<th></th>
					</tr>
					<?php 
					if(count($data)>0){
						$i=$start;
						foreach($data as $row){
							$i++;
							$status = $row->Published==1 ? 'Enable' : 'Disable' ;
							echo "<tr>";
							echo "<td style='width:30px;text-align:center;background:#F7F7F7'><a href='{$base_link}edit/$row->ID'>$i</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$row->UserName</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$row->Name</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$row->Domain</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>".date('d/m/Y H:i:s',strtotime($row->Created))."</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$status</a></td>";
							echo "<td><a href='{$base_link}delete/$row->ID' title='Xóa dữ liệu này'><i class='fa fa-times'></i></a></td>";
							echo "</tr>";
						}
					}else{
						echo "<tr><td colspan='7'>Không tìm thấy dữ liệu .</td></tr>";
					}
					?>
				</table>
				<?php if(count($data)>0) echo $nav; ?>
			</div>
		</div>
</div>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;}
	.body_content .containner table tr td:nth-child(5){max-width: 300px}
</style>
