<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH MỤC QR CODE SẢN PHẨM</h1>
	    </div>
	    <div class="block2">
            
	    </div>
    </div>
    <div class="import_orderlist">
    	<div class="block2">
    		<div class="row">
                <div class="col-xs-6">
                    <a class="btn btn-danger" role="button" onclick="showimport()"><i class="fa fa-upload"></i> Import từ file Excel</a>
                    <a class="btn btn-primary" role="button" href="<?php echo base_url().'assets/template/template_import_qrcode.xlsx' ?>"><i class="fa fa-download"></i> Mẫu nhập liệu</a>
                </div>
                <div class="col-xs-3">
                    <div class="btn-group pull-right" style="margin-right:8px">
                        <a class="btn btn-default"><i class="fa fa-sort" aria-hidden="true"></i> Lọc trạng thái</a>
                        <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php 
                            $url = base_url().ADMINPATH."/report/factory/qrcode/";
                            ?>
                            <li><a href="<?php echo $url.'?type=-1' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type==-1 ? "color:#090" : "color:#ccc" ; ?>" aria-hidden="true"></i> Tất cả mã</a></li>
                            <li><a href="<?php echo $url.'?type=0' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type==0 ? "color:#090" : "color:#ccc" ; ?>" aria-hidden="true"></i> Mã chưa sử dụng</a></li>
                            <li><a href="<?php echo $url.'?type=2' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type==2 ? "color:#090" : "color:#ccc" ; ?>" aria-hidden="true"></i> Mã đang lưu thông</a></li>
                            <li><a href="<?php echo $url.'?type=3' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type==3 ? "color:#090" : "color:#ccc" ; ?>" aria-hidden="true"></i> Mã hàng trả về</a></li>
                            <li><a href="<?php echo $url.'?type=1' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type==1 ? "color:#090" : "color:#ccc" ; ?>" aria-hidden="true"></i> Mã đã gán thùng</a></li>
                            <li><a href="<?php echo $url.'?type=4' ?>"><i class="fa fa-check-square" style="margin-right:10px;<?php echo $type==4 ? "color:#090" : "color:#ccc" ; ?>" aria-hidden="true"></i> Mã quét lẻ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-3">
                    <form action="<?php echo $base_link ?>" method="get">
                        <input type="text" name="keywords" class="form-control" placeholder="Điền mã QR code cần tìm kiếm ..." />
                    </form>
                </div>
            </div>
    	</div>
        
    	<div class="block3 table_data">
            <?php if(count($data)>0){ ?>
                <table>
                    <tr>
                        <th>STT</th>
                        <th>Mã QR Code</th>
                        <th>Mã SKU</th>
                        <th>Số chứng từ</th>
                        <th>Lô sản xuất</th>
                        <th>Tình trạng</th>
                        <th>Lịch sử QR CODE</th>
                    </tr>
                    <?php 
                        $mobilization = $this->db->query("select ID,Code from ttp_report_transferorder_mobilization")->result();
                        $arr_mobilization = array();
                        if(count($mobilization)>0){
                            foreach($mobilization as $row){
                                $arr_mobilization[$row->ID] = $row->Code;
                            }
                        }

                        $products = $this->db->query("select ID,MaSP from ttp_report_products where CodeManage=1")->result();
                        $products_arr = array();
                        if(count($products)>0){
                            foreach($products as $row){
                                $products_arr[$row->ID] = array('MaSP'=>$row->MaSP);
                            }
                        }

                        $shipment = $this->db->query("select a.ID,a.ShipmentCode,b.MaSP from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and b.CodeManage=1 order by b.ID DESC")->result();
                        $shipment_arr = array();
                        if(count($shipment)>0){
                            foreach($shipment as $row){
                                $shipment_arr[$row->ID] = $row->ShipmentCode;
                            }
                        }

                        $arr = array(0=>'Chưa sử dụng',1=>'<span class="text-primary">Đã gán thùng</span>',2=>'<span class="text-warning">Đang lưu thông</span>',3=>'<span class="text-danger">Hàng trả về</span>',4=>'<span class="text-success">Hàng quét lẻ</span>');
                        $i = $start+1;
                        foreach($data as $row){
                            $row->Code = str_pad($row->Code, 7, '0', STR_PAD_LEFT);
                            $code = "http://www.adiva.com.vn/qrcode?serial=".$row->Code;
                            echo "<tr>";
                            echo "<td>$i</td>";
                            echo "<td>$code</td>";
                            echo isset($products_arr[$row->ProductsID]['MaSP']) ? "<td>".$products_arr[$row->ProductsID]['MaSP']."</td>" : "<td>--</td>" ;
                            echo isset($arr_mobilization[$row->MobilizationID]) ? "<td>".$arr_mobilization[$row->MobilizationID]."</td>" : "<td>--</td>" ;
                            echo isset($shipment_arr[$row->ShipmentID]) ? "<td>".$shipment_arr[$row->ShipmentID]."</td>" : "<td>--</td>" ;
                            echo isset($arr[$row->Status]) ? "<td>".$arr[$row->Status]."</td>" : "<td>--</td>";
                            echo $row->Status==0 ? "<td>--</td>" : '<td><a class="text-success" onclick="preview_trace('.$row->ID.')"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Xem thông tin</a></td>';
                            echo "</tr>";
                            $i++;
                        }
                    
                    ?>
                    <tr>
                        <td colspan='7'>
                            <?php 
                            $status0 = 0;
                            $status1 = 0;
                            $status2 = 0;
                            $status4 = 0;

                            if(count($status)>0){
                                foreach($status as $row){
                                    $status0 = $row->Status==0 ? $row->total : $status0 ;
                                    $status1 = $row->Status==1 ? $row->total : $status1 ;
                                    $status2 = $row->Status==2 ? $row->total : $status2 ;
                                    $status4 = $row->Status==4 ? $row->total : $status4 ;
                                }
                            }
                            ?>
                            <a class='btn btn-default'>Tổng cộng : <b><?php echo number_format($find) ?></b> mã đã tạo</a>
                            <a class='btn btn-default'><span><b><?php echo number_format($status0) ?></b> mã chưa sử dụng</span></a>
                            <a class='btn btn-default'><span class='text-primary'><b><?php echo number_format($status1) ?></b> mã đã gán thùng</span></a>
                            <a class='btn btn-default'><span class='text-success'><b><?php echo number_format($status4) ?></b> mã quét lẻ</span></a>
                            <a class='btn btn-default'><span class='text-danger'><b><?php echo number_format($status2) ?></b> mã đang lưu thông</span></a>
                        </td>
                    </tr>
                </table>

                <?php 
                echo $nav;
            }else{
                echo count($data)==0 ? "<p><img src='public/admin/images/view_empty_arrow.png' /> Bấm để nhập danh sách mới .</p>" : ""; 
            }
            ?>
    	</div>
    </div>
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/factory/" ?>" />
</div>
<script>
    var baselink = $("#baselink_report").val();

    $("#close_overlay").click(function(){
        $(".over_lay").hide();
    });

    function showimport(){
        $(".over_lay .box_inner").css({'margin-top':'10px'});
        $(".over_lay .box_inner .block1_inner h1").html("Import QR CODE");
        $(".over_lay .box_inner .block2_inner").load(baselink+"load_import_qrcode_form");
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    function activeimport(){
        var Fileinput = document.getElementById("file_import");
        var file = Fileinput.files[0];
        var fd = new FormData();
        fd.append('file', file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', baselink+'upload_import', true);
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                $(".over_lay .box_inner .block2_inner").css({'overflow-x':'hidden'});
                $(".over_lay .box_inner .block2_inner #form-file p:first-child").html('<div class="loader">Loading...</div>');
                $(".over_lay .box_inner .block2_inner #form-file h4").html("Đang tải file lên hệ thống ...");
            }
        };
        xhr.onload = function() {
            if (this.status == 200) {
                $(".over_lay .box_inner .block2_inner").html(xhr.responseText);
            }else{
                console.log(xhr.responseText);
            }
        };
        xhr.send(fd);
    }

    function rmtr(ob){
        $(ob).parent('td').parent('tr').remove();
    }

    function active_import(ob){
        $(ob).addClass("saving");
        $(ob).html('Loading...');
        var arr = new Array();
        $(".over_lay .box_inner .block2_inner table tr.treach").each(function(){
            var qrcode = $(this).find('.td0').html();
            if(qrcode!=''){
                var jsonArg1 = new Object();
                jsonArg1.qrcode = qrcode;
                arr.push(jsonArg1);
            }
        });
        var billcode = $("#Billcode").val();
        $.ajax({
            dataType: "json",
            data: "bill="+billcode+"&data="+JSON.stringify(arr),
            url: baselink+'active_upload_import',
            cache: false,
            method: 'POST',
            success: function(result) {
                if(result.Error==null){
                    location.reload();
                }else{
                    $(".over_lay .box_inner .block2_inner").load(baselink+"load_import_qrcode_form");
                    alert(result.Error);
                }
            },
            error:function(result){
                if(result.status==500){
                    alert("Dữ liệu QRCode bị trùng ! Vui lòng kiểm tra lại file!");
                }
                console.log(result.responseText);
                $(ob).removeClass("saving");
                $(ob).html("<i class='fa fa-upload'></i> Nhập vào hệ thống");
                $(".over_lay .box_inner .block2_inner").load(baselink+"load_import_qrcode_form");
            }
        });
    }

    function preview_trace(id){
        $(window).scrollTop(70);
        $(".over_lay .box_inner").css({'margin-top':'10px'});
        $(".over_lay .box_inner .block1_inner h1").html("QR CODE TRACE");
        $(".over_lay .box_inner .block2_inner").load(baselink+"load_preview_trace/"+id);
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    function getbill(ob){
        var factory = $(ob).val();
        $("#Billcode").load(baselink+"get_bill_by_warehouse/"+factory);
    }
</script>