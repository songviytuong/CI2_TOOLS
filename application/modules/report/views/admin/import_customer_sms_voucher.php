<div class="containner">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-7">
			<h3 style="margin-top:0px;padding-top:5px;">Lịch sử tin nhắn hệ thống SMS</h3>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-5">
			<div class="input-group">
			  	<input type="text" class="form-control" placeholder="Tìm kiếm theo nội dung" aria-describedby="basic-addon1" id="form-search" onchange="submit()" value="<?php echo strip_tags($search) ?>">
			  	<span class="input-group-addon" onclick="submit()">Tìm</span>
			</div>
		</div>
		<div class="col-xs-12">
			<hr style="margin-top: 10px;">
			<table class="table table-bordered">
				<tr style="background:#EEE">
					<th>STT</th>
					<th>Nội dung tin nhắn</th>
					<th style="width:120px">Số điện thoại</th>
					<th style="width:120px">Thời gian gửi</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start+1;
					foreach($data as $row){
						echo "<tr>
							<td>$i</td>
							<td>$row->Message</td>
							<td>$row->Phone</td>
							<td>$row->Created</td>
						</tr>";
						$i++;
					}
				}else{
					echo "<tr><td colspan='4'>Không tìm thấy dữ liệu theo yêu cầu !</td></tr>";
				}
				?>
			</table>
		</div>
		<div class="col-xs-12">
			<?php echo $nav ?>
		</div>
	</div>
</div>
<script>
	function submit(){
		var data = $("#form-search").val();
		window.location = "<?php echo current_url() ?>?search="+data;
	}
</script>