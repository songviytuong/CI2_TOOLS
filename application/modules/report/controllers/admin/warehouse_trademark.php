<?php

class Warehouse_trademark extends Admin_Controller {

    public $limit = 30;
    public $user;
    public $classname = "warehouse_trademark";

    public function __construct() {

        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar', 'admin/warehouse_trademark_sitebar', array('user' => $this->user));
        $this->template->write_view('header', 'admin/header', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype();
    }

    public function index() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '';
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_trademark where Title like '%$keywords%'")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_trademark where Title like '%$keywords%' order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/report/warehouse_trademark/',
            'data' => $object,
            'start' => $start,
            'find' => $nav,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/report/warehouse_trademark/index', 5, $nav, $this->limit)
        );
        $this->template->add_title('Trademark | Manager Report Tools');
        $this->template->write_view('content', 'admin/warehouse_trademark_home', $data);
        $this->template->render();
    }

    public function add() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Trademark add | Manager report Tools');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/report/warehouse_trademark/'
        );
        $this->template->write_view('content', 'admin/warehouse_trademark_add', $data);
        $this->template->render();
    }

    public function add_new() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? mysql_real_escape_string($_POST['Title']) : '';
        $Content = isset($_POST['Content']) ? $_POST['Content'] : '';
        $Description = isset($_POST['Description']) ? $_POST['Description'] : '';
        $Published = isset($_POST['Published']) ? mysql_real_escape_string($_POST['Published']) : '';
        if ($Title != '') {
            $max = $this->db->query("select Max(ID) as ID from ttp_report_trademark")->row();
            $max = $max ? $max->ID + 1 : 1;
            $data = array(
                'ID' => $max,
                'Title' => $Title,
                'Published' => $Published,
                'Content' => $Content,
                'Description' => $Description,
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s'),
                'Owner' => $this->user->ID
            );
            $data['Alias'] = $this->lib->alias($Title);
            $check = $this->db->query("select ID from ttp_report_trademark where Alias='" . $data['Alias'] . "'")->row();

            if (isset($_FILES['Image_upload'])) {
                if ($_FILES['Image_upload']['error'] == 0) {
                    $this->upload_to = "brand_thumb";
                    $data['Banner_tmp'] = $this->upload_attach_file('Image_upload');
                }
            }
            if (!$check) {
                $this->db->insert("ttp_report_trademark", $data);
//                $data['Alias'] .= $max;
            }
        }
        redirect(ADMINPATH . '/report/warehouse_trademark/');
    }

    public function delete($id = 0) {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $this->db->query("delete from ttp_report_trademark where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function edit($id = 0) {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_report_trademark where ID=$id")->row();
            if (!$result)
                return;
            $this->template->add_title('Edit Trademark | Manager report Tools');
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/report/warehouse_trademark/',
                'data' => $result
            );
            $this->template->write_view('content', 'admin/warehouse_trademark_edit', $data);
            $this->template->render();
        }
    }

    public function update() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '';
        $Content = isset($_POST['Content']) ? $_POST['Content'] : '';
        $Description = isset($_POST['Description']) ? $_POST['Description'] : '';
        $Published = isset($_POST['Published']) ? $_POST['Published'] : '';
        if ($Title != '') {
            $data = array(
                'Title' => $Title,
                'Published' => $Published,
                'Content' => $Content,
                'Description' => $Description,
                'LastEdited' => date('Y-m-d H:i:s'),
                'Owner' => $this->user->ID,
            );

            $data['Alias'] = $this->lib->alias($Title);
            $check = $this->db->query("select ID from ttp_report_trademark where Alias='" . $data['Alias'] . "' and ID!=$ID")->row();

            if (isset($_FILES['Image_upload'])) {
                if ($_FILES['Image_upload']['error'] == 0) {
                    $this->upload_to = "brand_thumb";
                    $data['Banner_tmp'] = $this->upload_attach_file('Image_upload');
                }
            }
            if ($check) {
                $this->db->where("ID", $ID);
                $this->db->update("ttp_report_trademark", $data);
//                $data['Alias'] .= $ID;
            }
        }
        redirect(ADMINPATH . '/report/warehouse_trademark/');
    }

}
?>

