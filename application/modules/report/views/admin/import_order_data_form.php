<?php 
	if(!isset($data)){
		if(isset($error) && $error!=''){
			echo "<div class='alert alert-danger text-center'>$error</div>";
		}
		echo "
			<form id='form-file' method=l'post' enctype='multipart/form-data'>
			<p class='text-center' style='color:#ccc;font-size:150px'><i class='fa fa-cloud-upload' aria-hidden='true'></i></p>
			<h4 class='text-center'>Vui lòng chọn file import từ máy tính</h4>
			<p class='text-center'><button class='btn btn-primary' style='position:relative;overflow:hidden;margin-top:20px'><input type='file' name='file' id='file_import' onchange='activeimport()' style='position:absolute;left:0px;top:0px;padding-left: 200px;height: 36px;cursor: pointer;' /><i class='fa fa-upload'></i> Brown file</button></p>
			<p class='text-center' style='margin-top:20px'>(*) chỉ chấp nhận file excel (xls,xlsx)</p></form>
		";
	}else{
		if(count($data)>0){
			echo "<div class='alert alert-success text-center'>Nội dung chi tiết file <b>\"$filename\"</b> bạn vừa tải lên hệ thống . Vui lòng kiểm tra lại thật kỹ trước khi nhấn lệnh nhập dữ liệu . <br>
				<p class='text-center' style='margin-top: 10px;'>
					<a class='btn btn-success' onclick='active_import(this)'><i class='fa fa-upload'></i> Nhập vào hệ thống</a>
				</p>
			</div>";
			echo "<table>";
			$i=1;
			foreach($data as $row){
				if(isset($row[1])){
					$temp = explode('-',$row[1]);
					$color = '';
					if(count($temp)!=3){
						$color = 'color:#F00';
					}else{
						if($temp[0]<999 || $temp[0]>9999 || $temp[1]>12 || $temp[2]>31){
							$color = 'color:#F00';
						}
					}
				}
				$td = $i==1 ? "th" : "td" ;
				$first = $i==1 ? "" : "treach" ;
				if($color!=''){
					$first = "trfalse";
				}
				echo "<tr class='$first'>";
				if(count($row)>0){
					for($k=0;$k<count($row);$k++){
						echo "<$td class='td$k' style='width:auto;$color'>".$row[$k]."</$td>";
					}
					echo $i==1 ? "<th></th>" : "<$td style='width:30px;$color'><a title='Xóa dữ liệu này ?' onclick='rmtr(this)'><i class='fa fa-times' aria-hidden='true'></i></a></$td>";
				}
				echo "</tr>";
				$i++;
			}
			echo "</table>";
		}else{
			echo "
				<form id='form-file' method=l'post' enctype='multipart/form-data'>
				<p class='text-center' style='color:#ccc;font-size:150px'><i class='fa fa-cloud-upload' aria-hidden='true'></i></p>
				<h4 class='text-center'>Vui lòng chọn file import từ máy tính</h4>
				<p class='text-center'><button class='btn btn-primary' style='position:relative;overflow:hidden;margin-top:20px'><input type='file' name='file' id='file_import' onchange='activeimport()' style='position:absolute;left:0px;top:0px;padding-left: 200px;height: 36px;cursor: pointer;' /><i class='fa fa-upload'></i> Brown file</button></p>
				<p class='text-center' style='margin-top:20px'>(*) chỉ chấp nhận file excel (xls,xlsx)</p></form>
			";
		}
	}
?>