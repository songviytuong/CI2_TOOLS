<?php 
class System_role extends Admin_Controller { 
	public $user;
	public $classname="system_role";
	public $limit = 10;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }
	
	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Role list | Config System');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_role")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select * from ttp_role $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/system_role/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/system_role/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/system_role_home',$data); 
		$this->template->render();
	}

	public function search(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		if(isset($_GET['keyword'])){
			$keyword = $this->lib->fill_data($_GET['keyword']);
			$str = "select * from ttp_role";
			$str = $keyword != '' ? $str." where Title like '%".urldecode($keyword)."%' or Title like '%".urldecode($keyword)."%'" : $str ;
			$this->template->add_title('Tìm kiếm dữ liệu');
			$data=array(
				'data'	=> $this->db->query($str)->result(),
				'nav'	=> '',
				'start'	=> 0,
				'base_link' =>	base_url().ADMINPATH.'/home/system_role/',
			);
			$this->template->write_view('content','admin/system_role_home',$data);
			$this->template->render();
			return;
		}
		redirect(ADMINPATH.'/home/system_role/');
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>1){
			$this->db->query("delete from ttp_role where ID=$id");
			$this->db->query("update ttp_user set DetailRole='' where RoleID=$id");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	
	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Role | Config System');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/system_role/'
		);
		$this->template->write_view('content','admin/system_role_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : '' ;
		if($Title!='' && $Status!=''){
			$data = array(
				'Published'	=> $Status,
				'Title'		=> $Title,
				'Created'	=> date('Y-m-d H:i:s'),
				'LastEdited'=> date('Y-m-d H:i:s')
			);
			if(count($_POST)>0){
				$arr = array();
				foreach($_POST as $key=>$value){
					if($key!='Title' && $key!='Status'){
						$arr[$key] = $value;
					}
				}
				$arr = json_encode($arr);
				$data['DetailRole'] = $arr;
	 		}
			$this->db->insert("ttp_role",$data);
		}
		redirect(ADMINPATH.'/home/system_role/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_role where ID=$id")->row();
			if(!$result) show_error("Data is empty !");
			$this->template->add_title('Edit Role | Config System');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/system_role/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/system_role_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '' ;
		$Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : '' ;
		if($Title!='' && $Status!=''){
			$data = array(
				'LastEdited'=> date('Y-m-d H:i:s'),
				'Published'	=> $Status,
				'Title'	=> $Title
			);
			if(count($_POST)>0){
				$arr = array();
				foreach($_POST as $key=>$value){
					if($key!='Title' && $key!='Status' && $key!='ID'){
						$arr[$key] = $value;
					}
				}
				$arr = json_encode($arr);
				$data['DetailRole'] = $arr;
	 		}
	 		$this->db->where("ID",$ID);
			$this->db->update("ttp_role",$data);
			$this->db->query("update ttp_user set DetailRole='".$data['DetailRole']."' where RoleID=$ID");
		}
		redirect(ADMINPATH.'/home/system_role/');
	}

}
?>