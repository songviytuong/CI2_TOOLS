<?php
class Transporter extends Admin_Controller {

    public $user;
    public $classname="transporter";

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('transporter');
        $this->template->write_view('sitebar','admin/transporter_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/transporter_header',array('user'=>$this->user));
        $this->template->add_doctype();
    }

    public $limit = 30;

    public function index(){
        $this->template->add_title('Lấy hàng | DOHATO');
        $type = isset($_GET['type']) ? (int)$_GET['type'] : 0 ;
        $place = isset($_GET['place']) ? (int)$_GET['place'] : 0 ;
        $bonus = "";
        if($place>0){
            $bonus = " and a.BranchID=$place";
        }
        $result = $this->db->query("select a.ID as IDDetails,a.BranchID,a.WarehouseID,a.Amount,a.Status,b.MaDH,c.Title,c.Donvi from ttp_report_order_send_supplier a,ttp_report_order b,ttp_report_products c where a.OrderID=b.ID and a.ProductsID=c.ID and a.UserReciver=".$this->user->ID." and a.Status=1 and b.Status!=1 $bonus")->result();
		$data = array(
            'data'      => $result,
            'type'      => $type,
            'place'     => $place,
            'base_link' =>  base_url().ADMINPATH.'/report/transporter/'
        );
        $view = "admin/transporter_home";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function order_has_ship(){
        $this->template->add_title('Đơn hàng đã giao | DOHATO');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";    
        $result = $this->db->query("select DISTINCT a.MaDH,a.AddressOrder,a.Total,a.Chiphi,a.Chietkhau,a.Name,a.Phone,a.FinanceMoney from ttp_report_order a,ttp_report_order_send_supplier b where b.OrderID=a.ID and b.UserReciver=".$this->user->ID." order by a.ID DESC $limit_str")->result();
        $nav = $this->db->query("select DISTINCT a.MaDH,count(a.ID) as nav from ttp_report_order a,ttp_report_order_send_supplier b where b.OrderID=a.ID and b.UserReciver=".$this->user->ID)->row();
        $order = $this->db->query("select DISTINCT a.ID,count(a.ID) as nav from ttp_report_order a,ttp_report_order_send_supplier b where b.OrderID=a.ID and a.Status=0 and b.UserReciver=".$this->user->ID)->row();
        $success = $order ? $order->nav : 0 ;
        $order = $this->db->query("select DISTINCT a.ID,count(a.ID) as nav from ttp_report_order a,ttp_report_order_send_supplier b where b.OrderID=a.ID and a.Status=1 and b.UserReciver=".$this->user->ID)->row();
        $false = $order ? $order->nav : 0 ;
        $nav = $this->db->query("select DISTINCT a.ID,count(a.ID) as nav from ttp_report_order a,ttp_report_order_send_supplier b where b.OrderID=a.ID and b.UserReciver=".$this->user->ID)->row();
        $total = $nav ? $nav->nav : 0;
        $nav = $nav ? $nav->nav : 0;
        $data = array(
            'data'      =>  $result,
            'start'     =>  $start,
            'total'     =>  $total,
            'success'   =>  $success,
            'false'     =>  $false,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/transporter/order_has_ship',5,$nav,$this->limit)
        );
        $view = "transporter_order";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function shipping(){
        $this->template->add_title('Giao hàng | DOHATO');
        $result = $this->db->query("select DISTINCT b.ID,b.MaDH,b.Name,b.Phone,b.AddressOrder,b.Total,b.Chiphi,b.Chietkhau,b.Reduce,b.Note,b.DistrictID,b.CityID from ttp_report_order_send_supplier a,ttp_report_order b where a.OrderID=b.ID and a.UserReciver=".$this->user->ID." and a.Status in(1,2) and b.Status=7")->result();
        $data = array(
            'data'      => $result,
            'base_link' =>  base_url().ADMINPATH.'/report/transporter/'
        );
        $view = "admin/transporter_shipping";
        $this->template->write_view('content',$view,$data);
        $this->template->render();
    }

    public function apply_get_products_from_branch(){
        $data = isset($_POST['data']) ? json_decode($_POST['data']) : array() ;
        if(count($data)>0){
            $data = implode(',',$data);
            $details = $this->db->query("select a.BranchID,a.ProductsID,a.ShipmentID,a.Amount,b.KhoID,b.ID from ttp_report_order_send_supplier a,ttp_report_order b where a.OrderID=b.ID and a.ID in($data)")->result();
            if(count($details)>0){
                foreach($details as $row){
                    if($row->BranchID>0){
                        $this->change_inventory($row->Amount,$row->KhoID,$row->ShipmentID,$row->ProductsID,$row->ID);
                    }
                }
                $this->db->query("update ttp_report_order_send_supplier set TimeReciver='".date('Y-m-d H:i:s')."',Status=2,UCC=1 where ID in($data)");
                echo "true";
                return;
            }
        }
        echo "false";
    }


    public function success_order(){
        $ID = isset($_GET['data']) ? (int)$_GET['data'] : 0 ;
        $reciver = isset($_GET['reciver']) ? $_GET['reciver'] : 0 ;
        $reciver = str_replace(',','',$reciver);
        $reciver = (int)$reciver ;
        $details = $this->db->query("select a.BranchID,a.ProductsID,a.ShipmentID,a.Amount,b.KhoID,b.ID,b.MaDH,a.CustomerRecive,a.Status from ttp_report_order_send_supplier a,ttp_report_order b where a.OrderID=b.ID and b.ID=$ID and a.Status=2 and a.UserReciver=".$this->user->ID)->result();
        if(count($details)>0){
			foreach($details as $row){
				if($row->CustomerRecive==0 && $row->Status!=5){
					$this->change_inventory_success($row->Amount,$row->KhoID,$row->ShipmentID,$row->ProductsID,$row->ID);
				}else{
					$this->change_inventory_false($row->Amount,$row->KhoID,$row->ShipmentID,$row->ProductsID,$row->ID);
				}
            }
            $this->db->query("update ttp_report_order_send_supplier set TimeSuccess='".date('Y-m-d H:i:s')."',Status=3 where OrderID=$ID and CustomerRecive=0");
            $this->db->query("update ttp_report_order_send_supplier set TimeSuccess='".date('Y-m-d H:i:s')."',Status=4 where OrderID=$ID and CustomerRecive=1");
            $this->db->query("update ttp_report_order set TransportMoney=1,Status=0,DateSuccess='".date('Y-m-d H:i:s')."',HistoryEdited='".date('Y-m-d H:i:s')."' where ID=$ID and Status=7");
            $export = $this->db->query("select ID from ttp_report_export_warehouse where OrderID=$ID")->row();
            if($export){
                $this->db->query("update ttp_report_export_warehouse set TransferMoney=1,RealTransferMoney='$reciver',DateTransferMoney='".date('Y-m-d')."' where ID=$export->ID");
            }
            $datahis = array(
                'OrderID'   =>$ID,
                'Thoigian'  =>date('Y-m-d H:i:s',time()),
                'Status'    =>0,
                "UserID"    =>$this->user->ID
            );
            $this->db->insert('ttp_report_orderhistory',$datahis);
            $customer = $this->db->query("select a.Phone1,b.Name,b.MaDH,b.Total,b.Chietkhau,b.Reduce,b.Chiphi,a.ID,b.AffiliateID from ttp_report_customer a,ttp_report_order b where a.ID=b.CustomerID and b.ID=$ID")->row();
            if($customer){
                $total = $customer->Total-$customer->Chietkhau;
                $point = $total==0 ? 0 : round($total/1000);
                $this->db->query("update ttp_report_customer set Point=Point+$point where ID=$customer->ID");
                $sql = array(
                    'CustomerID'=> $customer->ID,
                    'Point'     => $point,
                    'Created'   => date('Y-m-d H:i:s'),
                    'Note'      => 'Nhận UPOINT từ đơn hàng #'.$customer->MaDH
                );
                $this->db->insert('ttp_report_customer_point',$sql);
                
                $reciver = $reciver==0 ? "" : ". Ban da thanh toan so tien ".number_format($reciver)."d" ;
                $MESSAGE = "Ucancook.vn da giao thanh cong don hang $customer->MaDH cho KH ".$this->lib->asciiCharacter($customer->Name)." luc ".date('H:i (d/m/Y)')."$reciver . Tran trong cam on.";
                //$this->sendsms($customer->Phone1,$MESSAGE);
				$this->lib->write_log_data("<b>".$this->user->UserName."</b> giao thành công đơn hàng <b class='text-primary'>$customer->MaDH</b> cho KH <b>".$customer->Name."</b>",0);
                if($customer->AffiliateID==27){
                    $this->send_affi_AT($ID);
                }
            }
            redirect(base_url().ADMINPATH.'/report/transporter/shipping');
            return;
        }
        echo "false";
    }

    public function reject_affi_AT($result){
        $data = array(
            "transaction_id"=>$result->MaDH,
            "status"=>2
        );
        $data_string = json_encode($data);
        $ch = curl_init('https://api.accesstrade.vn/v1/postbacks/conversions');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Token 0AC8BWsHsG94A_Ksr2YmGaX6WJjUk8cb'
        ));
        $response = curl_exec($ch);
        $this->write_log_affiliate_AT($result->ID,$data_string,$response);
    }

    public function get_categories_affiliate(){
        $categories = $this->db->query("select * from ttp_report_products_categories where CategoriesID=3")->result();
        $arr_cat = array();
        if(count($categories)>0){
            foreach ($categories as $row) {
                $arr_cat[$row->ProductsID] = 1;
            }
        }
        return $arr_cat;
    }

    public function send_affi_AT($orderid=0){
        $result = $this->db->query("select * from ttp_report_order where ID=$orderid")->row();
        if($result){
            $customer = $this->db->query("select count(1) as total from ttp_report_order where CustomerID=$result->CustomerID and Status=0 and ID!=$orderid")->row();
            $is_new = $customer ? $customer->total : 0 ;
            $is_new = $is_new>0 ? 0 : 1 ;
            $data = array(
                "conversion_id"         => $result->ID,
                "conversion_result_id"  => "30",
                "tracking_id"           => $result->TrackingID,
                "transaction_id"        => $result->MaDH,
                "transaction_time"      => date('Y-m-d H:i:s'),
                "transaction_value"     => $result->Total+$result->Chiphi-$result->Chietkhau,
                "transaction_discount"  => $result->Chietkhau,
                "status"                => 1,
                "items" => array ()
            );
            $total_commission = 0;
            $arr_pork = $this->get_categories_affiliate();
            $details = $this->db->query("select a.*,b.MaSP,b.Title from ttp_report_orderdetails a,ttp_report_products b where a.OrderID=$result->ID and a.ProductsID=b.ID")->result();
            if(count($details)>0){
                foreach ($details as $row) {
                    $item = array(
                        "id"    => $row->ProductsID,
                        "sku"   => $row->MaSP,
                        "name"  => $row->Title,
                        "price" => $row->Price,
                        "quantity" => $row->Amount
                    );
                    if($is_new==1){
                        if(isset($arr_pork[$row->ProductsID])){
                            $item["category"] = "First order Pork";
                            $item["category_id"] = "first_order_port";
                            $percent_commission = 5;
                        }else{
                            $item["category"] = "First order remaining products";
                            $item["category_id"] = "first_order_remaining";
                            $percent_commission = 8;
                        }
                    }else{
                        if(isset($arr_pork[$row->ProductsID])){
                            $item["category"] = "Next order Pork";
                            $item["category_id"] = "next_order_port";
                            $percent_commission = 2;
                        }else{
                            $item["category"] = "Next order remaining products";
                            $item["category_id"] = "next_order_remaining";
                            $percent_commission = 3;
                        }
                    }
                    $total_commission += (($row->Total/100)*$percent_commission);
                    $data['items'][] = $item;
                }
            }
            $total_commission = $total_commission-$result->Chietkhau;
            $this->db->query("update ttp_report_order set AffiliateCost=$total_commission where ID=$orderid");
            $this->reject_affi_AT($result);
            $data_string = json_encode($data);
            $ch = curl_init('https://api.accesstrade.vn/v1/postbacks/conversions');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Token 0AC8BWsHsG94A_Ksr2YmGaX6WJjUk8cb'
            ));
            $result = curl_exec($ch);
            $json_result = json_decode($result);
            $this->write_log_affiliate_AT($orderid,$data_string,$result);
        }
    }

    public function write_log_affiliate_AT($orderid=0,$data='',$result=''){
        $sql = array(
            'OrderID'   => $orderid,
            'Data'      => $data,
            'Created'   => date('Y-m-d H:i:s'),
            'Response'  => $result
        );
        $this->db->insert("ttp_report_affiliate_tracking",$sql);
    }

    public function cancel_order(){
        $ID = isset($_GET['data']) ? (int)$_GET['data'] : 0 ;
        $details = $this->db->query("select a.BranchID,a.ProductsID,a.ShipmentID,a.Amount,b.KhoID,b.ID from ttp_report_order_send_supplier a,ttp_report_order b where a.OrderID=b.ID and b.ID=$ID and a.UserReciver=".$this->user->ID)->result();
        if(count($details)>0){
            foreach($details as $row){
                $this->change_inventory_false($row->Amount,$row->KhoID,$row->ShipmentID,$row->ProductsID,$row->ID);
            }
            $this->db->query("update ttp_report_order set Status=1,HistoryEdited='".date('Y-m-d H:i:s')."' where ID=$ID and Status=7");
            $datahis = array(
                'OrderID'   =>$ID,
                'Thoigian'  =>date('Y-m-d H:i:s',time()),
                'Status'    =>1,
                "UserID"    =>$this->user->ID
            );
            $this->db->insert('ttp_report_orderhistory',$datahis);
            redirect(base_url().ADMINPATH.'/report/transporter/shipping');
            return;
        }
        echo "false";
    }

    public function details_order($id=0){
        $state = $this->lib->get_config_define("status","picking_status",1);
        $arr = array();
        if(count($state)>0){
            foreach ($state as $row) {
                $row->code = (int)$row->code;
                $arr[$row->code] = "<b style='background:#$row->color'>$row->name</b>";
            }
        }
        $result = $this->db->query("select a.Amount,a.Status,c.Title,c.Donvi,a.ID,a.CustomerRecive,a.OrderID from ttp_report_order_send_supplier a,ttp_report_products c where a.ProductsID=c.ID and a.OrderID=$id and a.UserReciver=".$this->user->ID)->result();
        if(count($result)>0){
            echo "<p class='list_products_details active'>CHI TIẾT ĐƠN HÀNG</p>";
            foreach ($result as $row) {
                $checked = $row->CustomerRecive==0 ? "active" : "" ;
                $function = $row->CustomerRecive==0 ? "not_reciver" : "yes_reciver" ;
                $haveclick = $row->Status!=5 ? "onclick='$function(this,$row->ID,$row->OrderID)'" : "";
                echo "<p class='list_products_details'><a class='check_reciver $checked' $haveclick><i class='fa fa-check'></i></a> $row->Title <br>x $row->Amount / $row->Donvi ".$arr[$row->Status]."</p>";
            }
            echo "<p class='list_products_details active'><a onclick='minimumdetails(this)'>Thu gọn <i class='fa fa-chevron-up' aria-hidden='true'></i></a></p>";
        }
    }

    public function change_inventory_success($Amount=0,$KhoID=0,$ShipmentID=0,$ProductsID=0,$OrderID=0){
        $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$KhoID and LastEdited=1 order by DateInventory DESC")->row();
        if($check){
            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$KhoID and LastEdited=1");
            if($check->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set OnHand=OnHand-$Amount,LastEdited=1 where ID=$check->ID");
                $data_log = array(
                    'ProductsID'    =>$ProductsID,
                    'OnHand'        =>$check->OnHand-$Amount,
                    'Available'     =>$check->Available,
                    'WarehouseID'   =>$KhoID,
                    'ShipmentID'    =>$ShipmentID,
                    'DateInventory' =>date('Y-m-d'),
                    'Type'          =>"OnHand",
                    'Amount'        =>$Amount,
                    'Action'        =>'update',
                    'Method'        =>'shipping success order id '.$OrderID
                );
                $this->write_log_inventory($data_log);
            }else{
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$check->OnHand-$Amount,$check->Available,$KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                $data_log = array(
                    'ProductsID'    =>$ProductsID,
                    'OnHand'        =>$check->OnHand-$Amount,
                    'Available'     =>$check->Available,
                    'WarehouseID'   =>$KhoID,
                    'ShipmentID'    =>$ShipmentID,
                    'DateInventory' =>date('Y-m-d'),
                    'Type'          =>"OnHand",
                    'Amount'        =>$Amount,
                    'Action'        =>'insert',
                    'Method'        =>'shipping success order id '.$OrderID
                );
                $this->write_log_inventory($data_log);
            }
        }
    }

    public function change_inventory_false($Amount=0,$KhoID=0,$ShipmentID=0,$ProductsID=0,$OrderID=0){
        $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$KhoID and LastEdited=1 order by DateInventory DESC")->row();
        if($check){
            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$KhoID and LastEdited=1");
            if($check->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=Available-$Amount,LastEdited=1 where ID=$check->ID");
                $data_log = array(
                    'ProductsID'    =>$ProductsID,
                    'OnHand'        =>$check->OnHand,
                    'Available'     =>$check->Available-$Amount,
                    'WarehouseID'   =>$KhoID,
                    'ShipmentID'    =>$ShipmentID,
                    'DateInventory' =>date('Y-m-d'),
                    'Type'          =>"Available",
                    'Amount'        =>$Amount,
                    'Action'        =>'update',
                    'Method'        =>'shipping success order id '.$OrderID
                );
                $this->write_log_inventory($data_log);
            }else{
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$check->OnHand,$check->Available-$Amount,$KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                $data_log = array(
                    'ProductsID'    =>$ProductsID,
                    'OnHand'        =>$check->OnHand,
                    'Available'     =>$check->Available-$Amount,
                    'WarehouseID'   =>$KhoID,
                    'ShipmentID'    =>$ShipmentID,
                    'DateInventory' =>date('Y-m-d'),
                    'Type'          =>"Available",
                    'Amount'        =>$Amount,
                    'Action'        =>'insert',
                    'Method'        =>'shipping success order id '.$OrderID
                );
                $this->write_log_inventory($data_log);
            }
        }
    }

    public function change_inventory($Amount=0,$KhoID=0,$ShipmentID=0,$ProductsID=0,$OrderID=0){
        $check = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$KhoID and LastEdited=1 order by DateInventory DESC")->row();
        if($check){
            $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$KhoID and LastEdited=1");
            if($check->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set OnHand=OnHand+$Amount,LastEdited=1 where ID=$check->ID");
                $data_log = array(
                    'ProductsID'    =>$ProductsID,
                    'OnHand'        =>$check->OnHand+$Amount,
                    'Available'     =>$check->Available,
                    'WarehouseID'   =>$KhoID,
                    'ShipmentID'    =>$ShipmentID,
                    'DateInventory' =>date('Y-m-d'),
                    'Type'          =>"OnHand",
                    'Amount'        =>$Amount,
                    'Action'        =>'update',
                    'Method'        =>'accept onhand from supplier '.$OrderID
                );
                $this->write_log_inventory($data_log);
            }else{
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$check->OnHand+$Amount,$check->Available,$KhoID,$ShipmentID,'".date('Y-m-d')."',1)");
                $data_log = array(
                    'ProductsID'    =>$ProductsID,
                    'OnHand'        =>$check->OnHand+$Amount,
                    'Available'     =>$check->Available,
                    'WarehouseID'   =>$KhoID,
                    'ShipmentID'    =>$ShipmentID,
                    'DateInventory' =>date('Y-m-d'),
                    'Type'          =>"OnHand",
                    'Amount'        =>$Amount,
                    'Action'        =>'insert',
                    'Method'        =>'accept onhand from supplier '.$OrderID
                );
                $this->write_log_inventory($data_log);
            }
        }
    }

    public function not_reciver(){
      $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
      $Amount = isset($_POST['Amount']) ? (int)$_POST['Amount'] : 0 ;
      $this->db->query("update ttp_report_order_send_supplier set CustomerRecive=1 where ID=$ID and UserReciver=".$this->user->ID);
      echo "OK";
    }

    public function not_reciver_order(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $this->db->query("update ttp_report_order_send_supplier set CustomerRecive=1 where OrderID=$ID and UserReciver=".$this->user->ID);
        $this->db->query("update ttp_report_order set Status=0,DateSuccess='".date('Y-m-d H:i:s')."',HistoryEdited='".date('Y-m-d H:i:s')."' where ID=$ID and Status=7");
        echo "OK";
    }

    public function yes_reciver(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $this->db->query("update ttp_report_order_send_supplier set CustomerRecive=0 where ID=$ID and UserReciver=".$this->user->ID);
        echo "OK";
    }

    public function cancel_picking_products($id=0){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $this->db->query("update ttp_report_order_send_supplier set Status=4,CustomerRecive=1 where ID=$id and UserReciver=".$this->user->ID);
        redirect(ADMINPATH."/report/transporter");
    }

    public function sendsms($PHONE='',$MESSAGE='',$SMSTYPE=0){
      $TYPE  = 1;
      $IDREQ  = time();
      $client  = new SoapClient("http://210.211.109.118/apibrandname/send?wsdl");
      $result  = $client->send(array("USERNAME" => SMS_USERNAME, "PASSWORD" => SMS_PASSWORD, "BRANDNAME" => SMS_BRANDNAME, "MESSAGE" => $MESSAGE, "TYPE" => $TYPE, "PHONE" => $PHONE, "IDREQ" => $IDREQ));
      $data = array(
          'Message' => $MESSAGE,
          'Phone'   => $PHONE,
          'Created' => date('Y-m-d H:i:s'),
          'Type'    => $SMSTYPE,
          'CodeStatus'=> $result->return->result
      );
      $this->db->insert('ttp_report_sms',$data);
    }
}
?>
