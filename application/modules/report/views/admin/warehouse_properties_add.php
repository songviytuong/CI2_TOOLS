<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Thêm loại biến thể</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên biến thể</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Thuộc loại biến thể</span></div>
					<div class='block2'>
						<select name="VariantID" class="form-control">
							<option value="0">-- Chọn nhóm biến thể --</option>
							<?php 
							$categories = $this->db->query("select * from ttp_report_properties where ParentID=0")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									echo "<option value='$row->ID'>$row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Trạng thái sử dụng</span></div>
					<div class='block2'>
						<input type='radio' name="Published" value="1" checked="true" /> Enable 
						<input type='radio' name="Published" value="0" /> Disable 
					</div>
				</div>
			</div>
		</form>
	</div>
</div>