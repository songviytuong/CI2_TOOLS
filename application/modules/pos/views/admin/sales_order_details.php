<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã SKU</th>
				<th>Tên sản phẩm</th>
				<th>Đơn vị</th>
				<th>Số lượng</th>
				<th>Giá bán</th>
				<th>Đã giảm</th>
				<th>Thành tiền</th>
			</tr>
			<tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
				$total = 0;
				if(count($data)>0){
					$i=1;
					foreach ($details as $row) {
						$total = $total + ($row->Price*$row->Amount);
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td>'.$row->MaSP.'</td>
						<td>'.$row->Title.'</td>
						<td>'.$row->Donvi.'</td>
						<td>'.$row->Amount.'<a class="pull-right" onclick="sales_save_change_amount(this,'.$row->Amount.','.$row->ID.','.$data->ID.')"><i class="fa fa-pencil"></i></a></td>
						<td>'.number_format($row->Price).'<a class="pull-right" onclick="sales_save_change_price(this,'.$row->Price.','.$row->ID.','.$data->ID.')"><i class="fa fa-pencil"></i></a></td>
						<td>'.number_format($row->PriceDown).'</td>
						<td>'.number_format($row->Price*$row->Amount).'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>TỔNG CỘNG : </b></td>
				<td><b><?php echo number_format($total) ?></b></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>GIẢM GIÁ : </b></td>
				<td><b><?php echo number_format($data->Chietkhau) ?></b><a class="pull-right" onclick="sales_save_change_discount(this,<?php echo $data->Chietkhau.",".$data->ID ?>)"><i class="fa fa-pencil"></i></a></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>CHI PHÍ KHÁC : </b></td>
				<td><b><?php echo number_format($data->Chiphi) ?></b> <a onclick="sales_save_change_chiphi(this,<?php echo $data->Chiphi.",".$data->ID ?>)" class="pull-right"><i class="fa fa-pencil"></i></a></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>THÀNH TIỀN PHẢI TRẢ : </b></td>
				<td><b><?php echo number_format($data->Total - $data->Chietkhau + $data->Chiphi) ?></b></td>
			</tr>
		</table>