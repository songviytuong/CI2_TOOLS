<?php

class City_model extends CI_Model {

    var $tablename = 'ttp_report_city';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getBy($name, $where, $value) {
        $this->db->select($name);
        $this->db->where($where, $value);
        $res = $this->db->get($this->tablename)->row()->$name;
        return $res;
    }

}
