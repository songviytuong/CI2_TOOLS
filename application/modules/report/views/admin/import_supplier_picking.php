<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Danh sách yêu cầu soạn hàng</h1>
	    </div>
	    <div class="block2">
	    	<div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
	    </div>
    </div>
    <div class="row" style="margin-bottom:10px">
    	<div class="btn-group" style="margin-left:15px;margin-right:8px">
        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Nhà cung cấp / đối tác</a>
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
            <ul class="dropdown-menu">
            	<li <?php echo $SupplierID==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,0,<?php echo $BranchID ?>)'>Tất cả nhà cung cấp</a></li>
				<?php 
				if($this->user->IsAdmin==1){
					$supplier_list = $this->db->query("select b.* from ttp_report_production b")->result();
				}else{
					$supplier_list = $this->db->query("select b.* from ttp_user_supplier a,ttp_report_production b where a.SupplierID=b.ID and a.UserID=".$this->user->ID)->result();
				}
				if(count($supplier_list)>0){
					foreach($supplier_list as $row){
						$style = $row->ID==$SupplierID ? "style='background:#c3e2ff;'" : '' ;
						echo "<li $style><a onclick='changefillter(this,$row->ID,$BranchID)'>$row->Title</a></li>";
					}
				}
				?>
	        </ul>
        </div>
        <div class="btn-group" style="margin-left:15px;margin-right:8px">
        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Chi nhánh / cửa hàng</a>
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
            <ul class="dropdown-menu">
            	<li <?php echo $BranchID==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,<?php echo $SupplierID ?>,0)'>Tất cả chi nhánh</a></li>
				<?php 
				if($this->user->IsAdmin==1){
					$branch_list = $this->db->query("select c.* from ttp_report_branch c")->result();
				}else{
					$branch_list = $this->db->query("select c.* from ttp_user_supplier a,ttp_report_production b,ttp_report_branch c where b.ID=c.SupplierID and a.SupplierID=b.ID and a.UserID=".$this->user->ID)->result();
				}
				if(count($branch_list)>0){
					foreach($branch_list as $row){
						$style = $row->ID==$BranchID ? "style='background:#c3e2ff;'" : '' ;
						echo "<li $style><a onclick='changefillter(this,$row->SupplierID,$row->ID)'>$row->Title</a></li>";
					}
				}
				?>
	        </ul>
        </div>
    </div>
	<table class="table table-bordered">
		<tr style='background:#f5f5f5;'>
			<th>STT</th>
			<th>MÃ ĐƠN HÀNG</th>
			<th>TÊN SẢN PHẨM</th>
			<th>DV</th>
			<th>SỐ LƯỢNG</th>
			<th>ĐƠN GIÁ</th>
			<th>CHI NHÁNH</th>
    		<th class='text-center'>XÁC NHẬN</th>
		</tr>
		<?php 
		if(count($data)>0){
			$i=0;
			$group_order = array();
			foreach($data as $row){
				if(isset($group_order[$row->ID])){
					$group_order[$row->ID] = $group_order[$row->ID]+1;
				}else{
					$group_order[$row->ID] = 1;
				}
			}
			$temp=1;
			foreach($data as $row){
				$rowspan = $group_order[$row->ID];
				$group = $temp==1 ? "<td rowspan='$rowspan' style='vertical-align:middle'>$row->MaDH</td>" : "" ;
				$i = $temp==1 ? $i+1 : $i ;
				$stt = $temp==1 ? "<td rowspan='$rowspan' style='vertical-align:middle'>$i</td>" : "" ;
				if($temp<$rowspan){
					$temp++;
				}else{
					$temp=1;
				}
				echo "<tr>
					$stt
					$group
					<td>$row->Title</td>
					<td>$row->Donvi</td>
					<td class='text-right'>".number_format($row->Amount,2)."</td>
					<td class='text-right'>".number_format($row->Price)."</td>
					<td>$row->BranchTitle</td>
					<td class='text-center' style='vertical-align:middle'><a class='btn-sm btn-danger' data-state='0' onclick='readytopick(this,$row->IDDetails)'><i class='fa fa-check-square-o' aria-hidden='true'></i> Đã soạn đủ</a></td>
				</tr>";
				
			}
		}else{
			echo '<tr><td colspan="8">Không tìm thấy dữ liệu theo yêu cầu .</td></tr>';
		}
		?>
	</table>
	<div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

	var link = "<?php echo $base_link ?>";

	function changefillter(ob,SupplierID,BranchID){
		window.location = link+"request_picking?supplier="+SupplierID+"&branch="+BranchID;
	}

	function readytopick(ob,detailis){
		var data = $(ob).attr('data-state');
		if(data==0){
			$(ob).addClass('saving');
			$.ajax({
	            url: link+"readytopick",
	            dataType: "html",
	            type: "POST",
	            data: "ID="+detailis,
	            success: function(result){
	                $(ob).removeClass('saving');
	                if(result=='true'){
	                	$(ob).removeClass("btn-danger");
	                    $(ob).addClass("text-success");
	                    $(ob).html("<i class='fa fa-check-square-o' aria-hidden='true'></i> Đã xác nhận");
	                }else{
	                    alert("Không thực thi được yêu cầu .");
	                }
	            },error:function(result){
	                console.log(result.responseText);
	            }
	        });
        }
	}

</script>
