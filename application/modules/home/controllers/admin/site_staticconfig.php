<?php

class Site_staticconfig extends Admin_Controller {

    public $user;
    public $classname = "site_staticconfig";
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $this->template->add_title('Static config | Config Site');
        $object = $this->db->query("select * from ttp_static_config where ID=1")->row();
        $json = $object ? json_decode($object->Data, true) : array();
        $json_en = $object ? json_decode($object->Data_en, true) : array();

        #DEFAULT SEO
        $this->load->model('seo_model', 'seo');
        $SEO_DEFAULT = $this->seo->defineSEODefault();

        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_staticconfig/',
            'data' => $json,
            'data_en' => $json_en,
            'seo' => $SEO_DEFAULT
        );
        $this->template->write_view('content', 'admin/site_static_config', $data);
        $this->template->render();
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Address = isset($_POST['Address']) ? $this->lib->fill_data($_POST['Address']) : '';
        $FullAddress = isset($_POST['FullAddress']) ? $this->lib->fill_data($_POST['FullAddress']) : '';
        $Certificate = isset($_POST['Certificate']) ? $this->lib->fill_data($_POST['Certificate']) : '';
        $Hotline = isset($_POST['Hotline']) ? $this->lib->fill_data($_POST['Hotline']) : '';
        $Ext = isset($_POST['Ext']) ? $this->lib->fill_data($_POST['Ext']) : '';
        $Mle = isset($_POST['Mle']) ? $this->lib->fill_data($_POST['Mle']) : 0;
        $Popup = isset($_POST['Popup']) ? $this->lib->fill_data($_POST['Popup']) : 0;
        $Email = isset($_POST['Email']) ? $this->lib->fill_data($_POST['Email']) : '';
        $Copyright = isset($_POST['Copyright']) ? $this->lib->fill_data($_POST['Copyright']) : '';
        $Analytics = isset($_POST['Analytics']) ? $this->lib->fill_data($_POST['Analytics']) : '';
        #SEO
        $MetaTitle = isset($_POST['MetaTitle']) ? $this->lib->fill_data($_POST['MetaTitle']) : '';
        $MetaKeywords = isset($_POST['MetaKeywords']) ? $this->lib->fill_data($_POST['MetaKeywords']) : '';
        $MetaDescription = isset($_POST['MetaDescription']) ? $this->lib->fill_data($_POST['MetaDescription']) : '';

        $data_seo = array(
            'Title' => $MetaTitle,
            'Keywords' => $MetaKeywords,
            'Description' => $MetaDescription
        );
        
        
        if (isset($_FILES['Image_upload'])) {
            if ($_FILES['Image_upload']['tmp_name'] != '') {
                $this->upload_to = "banner_thumb/$MetaTitle";
                $data_seo['Thumb'] = $this->upload_image_single();
                $this->delete_file_path($user->Thumb);
            }
        }
        
        $this->db->where("ID", 1);
        $this->db->update("auc_seo", $data_seo);

        $data = array(
            'Address' => $Address,
            'FullAddress' => $FullAddress,
            'Title' => $Title,
            'Certificate' => $Certificate,
            'Hotline' => $Hotline,
            'Ext' => $Ext,
            'Copyright' => $Copyright,
            'Analytics' => $Analytics,
            'Email' => $Email,
            'Mle' => ($Mle) ? 1 : 0,
            'Popup' => ($Popup) ? 1 : 0
        );

        
        $this->db->where("ID", 1);
        $this->db->update("ttp_static_config", array("Data" => json_encode($data)));
        redirect(ADMINPATH . '/home/site_staticconfig/');
    }

    public function update_en() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Address = isset($_POST['Address']) ? $this->lib->fill_data($_POST['Address']) : '';
        $Certificate = isset($_POST['Certificate']) ? $this->lib->fill_data($_POST['Certificate']) : '';
        $Hotline = isset($_POST['Hotline']) ? $this->lib->fill_data($_POST['Hotline']) : '';
        $Copyright = isset($_POST['Copyright']) ? $this->lib->fill_data($_POST['Copyright']) : '';
        $data = array(
            'Address' => $Address,
            'Title' => $Title,
            'Certificate' => $Certificate,
            'Hotline' => $Hotline,
            'Copyright' => $Copyright
        );
        $this->db->where("ID", 1);
        $this->db->update("ttp_static_config", array("Data_en" => json_encode($data)));
        redirect(ADMINPATH . '/home/site_staticconfig/');
    }

}

?>