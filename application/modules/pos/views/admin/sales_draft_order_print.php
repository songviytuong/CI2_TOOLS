<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>In hóa đơn</title>
<div class="print_page">
	<h1>UCANCOOK.VN</h1>
	<p class="head_info_bill"><small>21 Nguyễn Văn Tráng, phường Bến Thành, Quận 1, TPHCM</small>
	<small>Hotline : 0898.101.189</small></p>
	<h2>CASH BILL</h2>
	<div class="bill_info">BILL NO : <?php echo $data->MaDH ?> <span>DATE: <?php echo date('d/m/Y',strtotime($data->Ngaydathang)) ?></span></div>
	<div style="margin:10px 0px;">Name : <?php echo $data->Name ?></div>
	<div style="margin:10px 0px;">Phone : <?php echo $data->Phone ?></div>
	<div style="margin:10px 0px;">Address : <?php echo $data->AddressOrder.' '.$data->DistrictTitle.' - '.$data->CityTitle ?></div>
	<div style="margin:10px 0px;">Note : <?php echo $data->Note ?></div>
	<table>
		<tr>
			<th>PRODUCT</th>
			<th>QTY</th>
			<th>PRICE</th>
			<th>AMOUNT</th>
		</tr>
		<?php 
		$details = $this->db->query("select a.Title,a.MaSP,a.ShortName,b.* from ttp_report_products a,ttp_report_orderdetails b where a.ID=b.ProductsID and b.OrderID=$data->ID")->result();
		if(count($details)>0){
			foreach ($details as $row) {
				echo "<tr>";
				echo "<td style='text-transform:uppercase'>".$this->lib->asciiCharacter($row->ShortName)."</td>";
				echo "<td>".$row->Amount."</td>";
				echo "<td>".$row->Price."</td>";
				echo "<td>".number_format($row->Price*$row->Amount)."</td>";
				echo "</tr>";
			}
		}
		?>
	</table>
	<div class="last">
		<b>TOTAL:</b>
		<span><?php echo number_format($data->Total) ?></span>
	</div>
	<div class="last">
		<b>DISCOUNT:</b>
		<span><?php echo number_format($data->Chietkhau) ?></span>
	</div>
	<div class="last">
		<b>SHIP FEE:</b>
		<span><?php echo number_format($data->Chiphi) ?></span>
	</div>
	<div class="last">
		<b>TOTAL PAY:</b>
		<span><?php echo number_format($data->Total-$data->Chietkhau+$data->Chiphi) ?></span>
	</div>
	
	<div class="thanks_bill">CẢM ƠN QUÝ KHÁCH & HẸN GẶP LẠI</div>
	<p class="copyright_bill">Chúng tôi biết bạn có nhiều lựa chọn, cảm ơn bạn đã chọn chúng tôi ngày hôm nay !</p>
	<a onclick="window.print()" id="print_button">In hóa đơn</a>
</div>
<style>
	body{background: #EEE}
	.print_page{width:452px;font-size: 19px;font-family: tahoma;padding:20px;background: #FFF;margin:auto;border: 1px solid #555;position: relative;}
	.print_page .head_info_bill{margin-top:5px;margin-bottom:20px;text-align: center;}
	.print_page h1{font-size: 35px;margin:0px;padding:0px;font-family: fantasy;text-align: center;}
	.print_page h2{text-align: center;font-size: 23px;}
	.print_page p small{display: block;margin-bottom:5px;font-size: 16px;}
	.print_page .bill_info span{float:right;font-size: 19px;margin-right:10px;}
	.print_page table{border-collapse: collapse;font-size: 19px;border-top: 1px dotted #000;border-bottom: 1px dotted #000;margin:6px 0px;width:100%;}
	.print_page table tr th{border-bottom: 1px dotted #000;}
	.print_page table tr th,.print_page table tr td{padding:6px 19px;font-size:17px;text-align: left;}
	.print_page table tr th:first-child,.print_page table tr td:first-child{padding-left:0px;}
	.print_page .last{text-align: right;padding:3px 0px;font-size: 17px;}
	.print_page .last span{float:right;min-width: 80px;margin-right:30px;}
	.print_page .thanks_bill{text-align: center;font-size: 20px;font-weight: bold;border-top:1px solid #000;margin-top:10px;padding-top:10px;}
	.print_page .copyright_bill{margin:0px;padding:0px;text-align: center;font-size: 17px;margin-top:5px;}
	a#print_button{position: absolute;
    top: 10px;right: 10px;font-size: 12px;
    background-image: -webkit-linear-gradient(top,#337ab7 0,#265a88 100%);
    background-image: -o-linear-gradient(top,#337ab7 0,#265a88 100%);
    background-image: -webkit-gradient(linear,left top,left bottom,from(#337ab7),to(#265a88));
    background-image: linear-gradient(to bottom,#337ab7 0,#265a88 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff337ab7', endColorstr='#ff265a88', GradientType=0);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
    background-repeat: repeat-x;border-color: #245580;color:#FFF;padding: 6px 10px;
    border-radius: 4px;cursor: pointer;text-shadow: 0 -1px 0 rgba(0,0,0,.2);
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.15), 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 0 rgba(255,255,255,.15), 0 1px 1px rgba(0,0,0,.075);
    }
	@media print {
		.print_page{border:none;padding:0px;width:400px;}
		a#print_button{display: none;}
	}
</style>