	<table class="table">
	<tr>
		<th>HÌNH ẢNH</th>
		<th>TÊN SẢN PHẨM</th>
		<th>ĐƠN VỊ</th>
		<th>SỐ LƯỢNG</th>
		<th>ĐƠN GIÁ NHẬP</th>
		<th>THÀNH TIỀN</th>
		<th class="text-center">HỦY</th>
	</tr>
	<?php 
	$total_order = 0;
	$total_products = 0;
	$total_reduce = 0;
	$WarehouseID = $this->WarehouseID;
	if(isset($data['reduce'])){
		$total_reduce = $data['reduce'];
	}
	if(isset($data['partner']['WarehouseSender'])){
		$WarehouseID = $data['partner']['WarehouseSender'];
	}
	if(isset($data['products']) && count($data['products'])>0){
		$key = array_keys($data['products']);
		$arr_inventory = array();
		$inventory = $this->db->query("select * from ttp_report_inventory where ProductsID in(".implode(',',$key).") and LastEdited=1 and WarehouseID=$WarehouseID")->result();
		if(count($inventory)>0){
			foreach($inventory as $row){
				if(isset($arr_inventory[$row->ProductsID])){
					$arr_inventory[$row->ProductsID] = $arr_inventory[$row->ProductsID]+$row->OnHand;
				}else{
					$arr_inventory[$row->ProductsID] = $row->OnHand;
				}
			}
		}
		$total_products = count($key);
		$products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
		if(count($products)>0){
			foreach ($products as $row) {
				$invent = isset($arr_inventory[$row->ID]) ? $arr_inventory[$row->ID] : 0 ;
				$amount = $data['products'][$row->ID];
				$price = $row->RootPrice;
				$diff = '';
				if(isset($data['price'][$row->ID])){
					if($data['price'][$row->ID]<$price){
						$diff = $price-$data['price'][$row->ID];
						$diff = '<span style="color:#999;display:block;margin-top:8px">Đã giảm : '.number_format($diff).'</span>';
					}elseif($data['price'][$row->ID]>$price){
						$diff = $data['price'][$row->ID]-$price;
						$diff = '<span style="color:#999;display:block;margin-top:8px">Đã tăng : '.number_format($diff).'</span>';
					}
					$price = $data['price'][$row->ID];
				}
				$total = $amount*$price;
				$total_order += $total;
				echo '<tr>
				<td>'.$row->MaSP.'</td>
				<td style="width:350px">
					<p>'.$row->Title.'</p>
				</td>
				<td>'.$row->Donvi.'</td>
				<td>
					<input type="number" value="'.$data['products'][$row->ID].'" style="width:100px" onchange="transfer_warehouse_change_products(this,'.$row->ID.')" />
					<span style="color:#999;display: block;margin-top:8px">Hiện còn : '.$invent.'</span>
				</td>
				<td><input readonly="true" type="number" value="'.$price.'" onchange="purchase_order_change_price(this,'.$row->ID.')" />'.$diff.'
				</td>
				<td>'.number_format($total).'</td>
				<td class="text-center"><a onclick="purchase_order_remove_products('.$row->ID.')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
			}
		}
	}else{
		echo "<tr><td colspan='8'>Vui lòng chọn sản phẩm</td></tr>"; 
	}
	?>
	<tr>
		<th colspan="5" class="text-right">TỔNG SỐ SẢN PHẨM</th>
		<th colspan="3">: <?php echo number_format($total_products) ?></th>
	</tr>
	<tr>
		<th colspan="5" class="text-right">GIÁ TRỊ PHIẾU CHUYỂN</th>
		<th colspan="3">: <?php echo number_format($total_order) ?></th>
	</tr>
	</table>