<div class="containner">
	<div class='accept_cancel_box <?php echo $data->AcceptStatus ==1 ? 'accept_cancel_box_success' : '' ; ?>'>
		<table>
			<tr>
				<td>Người gửi yêu cầu hủy  </td>
				<td>: <b><?php echo $data->Sender ?></b></td>
				<td rowspan="2"><a class='btn btn-default' <?php echo $data->AcceptStatus ==1 ? '' : "href='".base_url().ADMINPATH.'/report/request_cancel/accept_cancel/'.$data->ID."'" ; ?>> <i class="fa fa-check-circle"></i> <?php echo $data->AcceptStatus ==1 ? 'Đã xử lý' : 'Xác nhận hủy' ; ?></a></td>
			</tr>
			<tr>
				<td>Lý do gửi yêu cầu hủy ĐH</td>
				<td>: <b><?php echo $data->Lydohuy ?></b></td>
			</tr>
		</table>
	</div>
	<div class="print_order">
		<h1>THÔNG TIN ĐƠN HÀNG <?php echo $data->MaDH ?></h1>
		<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/request_cancel/" ?>'><i class="fa fa-undo"></i> Quay lại</a>
		<div class="row row1">
			<li>Tên NV bán hàng</li>
			<li>: <?php echo $data->FirstName." ".$data->LastName ?></li>
			<li>Ngày bán</li>
			<li>: <?php echo date("d/m/Y",strtotime($data->Ngaydathang)) ?></li>
		</div>
		<div class="row row2">
			<li>Tên khách hàng</li>
			<li>: <?php echo $data->Name ?></li>
			<li>Số điện thoại 1</li>
			<li>: <?php echo $data->Phone1 ?></li>
			<li>Số điện thoại 2</li>
			<li>: <?php echo $data->Phone2 ?></li>
		</div>
		<div class="row row5">
			<li>Khu vực</li>
			<li>: <?php echo $data->Area ?></li>
			<li>Tỉnh thành</li>
			<li>: <?php echo $data->Thanhpho ?></li>
			<li>Quận huyện</li>
			<li>: <?php echo $data->Quanhuyen ?></li>
		</div>
		<div class="row row3">
			<li>Địa chỉ giao hàng</li>
			<li>: <?php echo $data->AddressOrder ?></li>
		</div>
		<div class="row row6">
			<?php 
			$payment = array(0=>"COD",1=>"Chuyển khoản");
			?>
			<li>Phương thức thanh toán:</li>
			<li><?php echo isset($payment[$data->Payment]) ? $payment[$data->Payment] : ''; ?></li>
			<li>Ghi chú:</li>
			<li><?php echo $data->Ghichu ?></li>
		</div>

		<div class="table_donhang">
    			<table class="table_data" id="table_data">
	    			<tr>
	    				<th>Mã sản phẩm</th>
	    				<th>Tên sản phẩm</th>
	    				<th>Giá bán</th>
	    				<th>Số lượng</th>
	    				<th>% CK</th>
	    				<th>Giá trị CK</th>
	    				<th>Giá sau CK</th>
	    				<th>Thành tiền</th>
	    			</tr>
	    			<?php 
	    			$details = $this->db->query("select a.*,b.Title,b.MaSP from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
	    			$arrproducts = array();
	    			$temp = 0;
	    			if(count($details)>0){
	    				foreach($details as $row){
	    					echo "<tr>";
	    					echo "<td>$row->MaSP</td>";
	    					echo "<td>$row->Title</td>";
		    				$giaban = $row->Price+$row->PriceDown;
	    					$phantramck = $giaban==0 ? 0 : round($row->PriceDown/($giaban/100),1);
	    					echo "<td><span>".number_format($giaban)."</span></td>";
	    					echo "<td>$row->Amount</td>";
	    					echo "<td><span>$phantramck</span></td>";
							echo "<td><span>".number_format($row->PriceDown)."</span></td>";
							echo "<td><span>".number_format($row->Price)."</span></td>";
							echo "<td><span>".number_format($row->Total)."</span></td>";
	    					echo "</tr>";
	    					$arrproducts[] = '"data'.$row->ProductsID.'"';
	    				}
	    			}
	    			
	    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
	    			$tongtienhang = $data->Total - $data->Chietkhau;
	    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
	    			?>
	    			<tr class="last">
	    				<td colspan="7">Tổng cộng</td>
	    				<td><span><?php echo number_format($data->Total) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="7">% chiết khấu</td>
	    				<td><?php echo $phantramchietkhau ?></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="7">Giá chiết khấu</td>
	    				<td><span><?php echo number_format($data->Chietkhau) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="7">Tổng tiền hàng</td>
	    				<td><span><?php echo number_format($tongtienhang) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="7">Chi phí vận chuyển</td>
	    				<td><span><?php echo number_format($data->Chiphi) ?></span></td>
	    				
	    			</tr>
	    			<tr class="last">
	    				<td colspan="7">Tổng giá trị thanh toán</td>
	    				<td><span><?php echo number_format($tonggiatrithanhtoan) ?></span></td>
	    				
	    			</tr>
	    		</table>
	    	</div>
	</div>
</div>

<style>
	.print_order{	
	    background: #FFF;
	    padding: 50px 20px;
	    border: 1px solid #E1E1E1;
	    border-radius: 3px;
	}

	.body_content .containner .print_order .row {
		overflow:hidden;
		padding: 8px 0;
		margin: 0px;
	}

	.body_content .containner .print_order #back_page {
		position:absolute;
		right: 20px;
		top:30px;
	}

	.body_content .containner .print_order table tr th{padding:8px 5px;}
	.body_content .containner .print_order table tr td{padding:8px 5px;}
</style>