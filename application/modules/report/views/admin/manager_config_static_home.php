<?php 
	if(!isset($_GET['group'])){
		$_GET['group'] = '';
	}
	if(!isset($_GET['type'])){
		$_GET['type'] = '';
	}
?>
<style>table tr th{background:#EEE;}</style>
<div class="containner">
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-6">
				<h3 style="margin:0px">Cấu hình dữ liệu tĩnh trên tools</h3>
			</div>
			<div class="col-xs-6 text-right">
				<a class="btn btn-primary" href="<?php echo $base_link.'add'; ?>"> <i class="fa fa-plus"></i> Tạo mới</a>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<div class="form-group">
			<div class="col-xs-12">
				<label class="col-xs-6 control-label">
					Tìm thấy <?php echo number_format($find) ?> dữ liệu theo yêu cầu .
				</label>
				<div class="col-xs-6">
					<div class="btn-group pull-right">
	                    <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> <?php echo $_GET['group']=='' ? "Lọc nhóm" : $_GET['group'] ; ?></a>
	                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	                    <ul class="dropdown-menu">
							<?php 
							$select = $_GET['group']=='' ? "style='background:#beddfb'" : "" ; 
							echo "<li $select><a href='".$base_link."?group=&type=".$_GET['type']."'>Tất cả</a></li>";
							$group = $this->db->query("select DISTINCT `group` from ttp_define")->result();
							if(count($group)>0){
								foreach($group as $row){
									$select = $row->group==$_GET['group'] ? "style='background:#beddfb'" : "" ;
									echo "<li $select><a href='".$base_link."?group=$row->group&type=".$_GET['type']."'>$row->group</a></li>";
								}
							}
							?>
	                    </ul>
	                </div>

	                <div class="btn-group pull-right" style="margin-right:10px">
	                    <a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> <?php echo $_GET['type']=='' ? "Lọc loại" : $_GET['type'] ; ?></a>
	                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
	                    <ul class="dropdown-menu">
	                    	<?php
	                    	$select = $_GET['type']=='' ? "style='background:#beddfb'" : "" ; 
	                    	echo "<li $select><a href='".$base_link."?group=".$_GET['group']."&type='>Tất cả</a></li>";
							$group = $this->db->query("select DISTINCT `type` from ttp_define")->result();
							if(count($group)>0){
								foreach($group as $row){
									$select = $row->type==$_GET['type'] ? "style='background:#beddfb'" : "" ;
									echo "<li $select><a href='".$base_link."?group=".$_GET['group']."&type=".$row->type."'>$row->type</a></li>";
								}
							}
							?>
	                    </ul>
	                </div>
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="margin:0px">
		<table class="table table-bordered">
			<tr>
				<th>STT</th>
				<th>Nhóm</th>
				<th>Loại</th>
				<th>Mã</th>
				<th>Tên dữ liệu</th>
				<th>Position</th>
				<th>Language</th>
				<th>Action</th>
			</tr>
			<?php 
			if(count($data)>0){
				$i=$start+1;
				foreach($data as $row){
					$row->position = $row->position =='' ? "--" : $row->position ;
					echo "<tr>";
					echo "<td>$i</td>";
					echo "<td>$row->group</td>";
					echo "<td>$row->type</td>";
					echo "<td>$row->code</td>";
					echo "<td>$row->name</td>";
					echo "<td>$row->position</td>";
					echo "<td>$row->lang</td>";
					echo "<td><a href='".$base_link."edit/$row->id'>Edit</a> | <a class='delete' href='".$base_link."delete/$row->id'>Delete</a></td>";
					echo "</tr>";
					$i++;
				}
			}
			?>
		</table>
		<?php echo str_replace("href=","onclick='changelinks(this)' data=",$nav); ?>
	</div>
</div>
<script>
	function changelinks(ob){
		var data = $(ob).attr('data');
		var query = "?<?php echo http_build_query($_GET); ?>";
		window.location = data+query;
	}
</script>