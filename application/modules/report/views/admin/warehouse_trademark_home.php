<div class="containner">
    <div class="manager">
        <div class="fillter_bar">
            <div class="block1">
                <h1>Danh sách thương hiệu</h1>
            </div>
            <div class="block2">
                <form action="" style='float:left;'>
                    <input name="keywords" type='text' placeholder="Tìm thương hiệu ..." style="padding: 7px 10px;border: 1px solid #ccc;margin-right: 10px;border-radius: 4px;width: 200px;outline: none;" />
                </form>
                <a href="<?php echo $base_link . 'add'; ?>" class="btn btn-danger"><i class='fa fa-plus'></i> Add new</a>
            </div>
        </div>
        <div class="table_data">
            <table>
                <tr>
                    <th>ID</th>
                    <th>Brand Name</th>
                    <th>Status</th>
                    <th>Last Edited</th>
                    <th>Owner</th>
                    <th class="text-right">Action</th>
                </tr>
                <?php
                if (count($data) > 0) {
                    $i = $start;
                    foreach ($data as $row) {
                        $this->load->model('users_model', 'users');
                        $Users = $this->users->defineUsers(array('userid' => ($row->Owner) ? $row->Owner : 1));
                        $i++;
                        $status = $row->Published == 1 ? "Enable" : "Disable";
                        echo "<tr>";
                        echo "<td>$i</td>";
                        echo "<td><a href='{$base_link}edit/$row->ID'>$row->Title</a></td>";
                        echo "<td>$status</td>";
                        echo "<td>$row->LastEdited</td>";
                        echo "<td>" . $Users['FirstName'] . "</td>";
                        echo "<td class='text-right'>"
                        . "<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>&nbsp;&nbsp;&nbsp;"
                        . "<a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<td colspan='4'>Không tìm thấy dữ liệu</td>";
                }
                ?>
            </table>
            <?php if (count($data) > 0) echo $nav; ?>
        </div>
    </div>
</div>