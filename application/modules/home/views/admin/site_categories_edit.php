<div class="">
    <div class="tab_control col-md-12 col-sm-12 col-xs-12">
        <a class="col-md-1 col-sm-1 col-xs-4 current" data="tab1"><small>VIETNAM</small></a>
        <a class="col-md-1 col-sm-1 col-xs-4" data="tab2"><small>ENGLISH</small></a>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
            <div class="tab1 tabcontent">
                <div class="x_title">
                    <h2>CATEGORIES <small> / Edit categories</small></h2>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                        <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="CreateLink" required="required" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo $data->Title ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Post link (<span class="required">*</span>) </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="AliasLink" required="required" class="form-control col-md-12 col-xs-12" name="Alias" value="<?php echo $data->Alias ?>">
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-12" id='loadingalias' style="padding-top:7px"><i class="fa fa-cog fa-spin"></i> Loading ...</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo $data->Description ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Published" value="1" <?php echo $data->Published==1 ? "checked='checked'" : '' ; ?>> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Published" value="0" <?php echo $data->Published==0 ? "checked='checked'" : '' ; ?>> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Page <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="PagelinksID" class="form-control">
                                    <option value='0'>-- Not Connect Page --</option>
                                    <?php 
                                        $result = $this->db->query("select ID,Title from ttp_pagelinks")->result();
                                        if(count($result)>0){
                                            foreach($result as $row){
                                                $select = $data->PagelinksID==$row->ID ? "selected='selected'" : '' ;
                                                echo "<option value='$row->ID' $select>$row->Title</option>";
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                    </div>
            </div>
            <!-- end VIETNAM -->
            <div class="tab2 tabcontent">
                <div class="x_title">
                    <h2>CATEGORIES <small> / Edit categories</small></h2>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Title <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="CreateLink_en" class="form-control col-md-7 col-xs-12" name="Title_en" value="<?php echo $data->Title_en ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Post link (<span class="required">*</span>) </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="AliasLink_en" class="form-control col-md-12 col-xs-12" name="Alias_en" value="<?php echo $data->Alias_en ?>">
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-12" id='loadingalias_en' style="padding-top:7px"><i class="fa fa-cog fa-spin"></i> Loading ...</label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="Description_en" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;"><?php echo $data->Description_en ?></textarea>
                            </div>
                        </div>
                    </div>
            </div>
                    <div class="x_title" id="title_ext">
                        <h2>OPTION EXTENTSION <small> / Add new option</small></h2>
                        
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content"  id="recive_ajax">
                    <?php 
                    $jsondata = array();
                    $json = json_decode($data->Data,true);
                    if(count($json)>0){
                        foreach($json as $key=>$value){
                            $jsondata[]='"'.$key.'"';
                            $form = '<textarea class="resizable_textarea form-control" name="'.$key.'" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 87px;">'.$value.'</textarea>';
                            if($key=='album'){
                                $form = '<select name="album" class="form-control">';
                                $result = $this->db->query("select * from ttp_album")->result();
                                if(count($result)>0){
                                    foreach ($result as $row) {
                                        $selected = $row->ID==$value ? "selected='selected'" : '' ;
                                        $form.="<option value='$row->ID' $selected>$row->Title</option>";
                                    }
                                }
                                $form .='</select>';
                            }
                            if($key=='hot' || $key=='useslide' || $key=='showhome'){
                                $checkedtrue = $value=='true' ? "checked='checked'" : "" ;
                                $checkedfalse = $value!='true' ? "checked='checked'" : "" ;
                                $form = '<div>
                                            <label style="padding-top:7px">
                                                <input type="radio" name="'.$key.'" value="true" '.$checkedtrue.'> &nbsp; Enable &nbsp;
                                                <input type="radio" name="'.$key.'" value="false '.$checkedfalse.'"> Disable
                                            </label>
                                        </div>';
                            }
                            if($key=='position' || $key=='worktime' || $key=='year' || $key=='timeline' || $key=='number' || $key=='block'){
                                $form = '<input type="text" required="required" class="form-control col-md-7 col-xs-12" name="'.$key.'" value="'.$value.'">';
                            }
                            echo '<div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12"><a class="remove_option" onclick="removeoption(this)" title="Remove this field">[x]</a> '.$key.'</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        '.$form.'
                                    </div>
                                </div>
                            </div>';
                        }        
                    }
                    ?>
                    </div>
                    <div class="x_content">
                        <div class="form-group" id="control_addfield">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Add new field</label>
                            <!-- <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="optionname" class="form-control col-md-6 col-xs-12">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <select class="form-control" id="optionvalue">
                                    <option value="inputtext">Text Field</option>
                                    <option value="textarea">Text Area Field </option>
                                </select>
                            </div>
                            -->
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <select class="form-control" id="optionvalue">
                                    <option value="block">Block - Text box </option>
                                    <option value="changelink">Changelink - Text box</option>
                                    <option value="showhome">Showhome - Check box </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                                <button type="button" class="btn btn-primary" id="addfield">Add Field</button>
                                <button type="button" class="btn btn-primary" id="addoption">Add New Field</button>
                                <div class="description_code"><a class="question">Description field code <i class="fa fa-question-circle"></i></a>
                                    <div class='list_code'>
                                        <table>
                                            <tr>
                                                <th>Code Field</th>
                                                <th>Type Field</th>
                                                <th>Description Field</th>
                                            </tr>
                                            <tr>
                                                <td>block</td>
                                                <td>Text</td>
                                                <td>Vị trí hiển thị danh mục tùy theo giao diện</td>
                                            </tr>
                                            <tr>
                                                <td>changelink</td>
                                                <td>Text</td>
                                                <td>Thay đổi link đích cho danh mục</td>
                                            </tr>
                                            <tr>
                                                <td>showhome</td>
                                                <td>Radio check</td>
                                                <td>Đưa danh mục ra ngoài trang chủ</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
<style>
    <?php 
    if(count($json)>0){
        echo "#title_ext{display:block;}";
    }else{
        echo "#title_ext{display:none;}";
    }
    ?>
    #addfield{display:none;}
    #control_addfield{display:none;}
    a{cursor: pointer}
    #loadingalias{display:none;}
    #loadingalias_en{display:none;}
    .question i{font-size:20px;}
    .question:hover{cursor: pointer;text-decoration: none}
    .description_code{display: inline-block;position: relative;}
    .description_code .list_code:after{content:'';width:30px;height:30px;bottom:-15px;background: #F4F4F4;display:block;position: absolute;left: 221px;transform:rotate(45deg);-moz-transform:rotate(45deg);-webkit-transform:rotate(45deg);border-bottom: 1px solid #E1e1e1;border-right: 1px solid #E1e1e1;}
    .description_code .list_code{position: absolute;bottom:45px;left:-100px;width:500px;background: #F4F4F4;border:1px solid #E1e1e1;padding:20px;display:none;}
    .description_code .list_code table{width: 100%;border-collapse: collapse;}
    .description_code .list_code table tr td{border:1px solid #E1e1e1;padding:5px 10px;background: #FFF}
    .description_code .list_code table tr th{border:1px solid #E1e1e1;padding:5px 10px;background: #FFF;width:55px}
</style>
<script>
    $(document).ready(function(){
        var array = [];

        $(".question").click(function(){
            $(".description_code .list_code").toggle();
        });

        $("#addoption").click(function(){
            $("#title_ext").show();
            $("#addfield").show();
            $("#control_addfield").show();
            $(this).hide();
        });

        $("#addfield").click(function(){
            var optionvalue = $("#optionvalue").val();
            if(array.indexOf(optionvalue)==-1){
                array.push(optionvalue);
                if(optionvalue!=''){
                    $.ajax({
                        url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_option_input/' ?>"+optionvalue+"/0", 
                        success: function(result){
                            if(result!='fasle'){
                                $("#recive_ajax").append(result);
                                $("#optionname").val('');
                            }
                        }
                    });
                }
            }
        });

        $("#CreateLink").change(function(){
            var data = $(this).val();
            getalias(data);
        });

        $("#AliasLink").change(function(){
            var data = $(this).val();
            var title =  $("#CreateLink").val();
            data = data!='' ? data : title ;
            getalias(data);
        });

        $("#CreateLink_en").change(function(){
            var data = $(this).val();
            getalias_en(data);
        });

        $("#AliasLink_en").change(function(){
            var data = $(this).val();
            var title =  $("#CreateLink_en").val();
            data = data!='' ? data : title ;
            getalias_en(data);
        });
    });

    function getalias(path){
        if(path!=''){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_alias/' ?>",
                dataType: "html",
                type: "POST",
                data: "ob=cat&data="+path+"&ID=<?php echo $data->ID ?>",
                beforeSend: function(){
                    $("#loadingalias").show();
                },
                success: function(result){
                    if(result!=false){
                        $("#AliasLink").val(result);
                    }
                    $("#loadingalias").hide();
                }
            });
        }else{
            $("#AliasLink").val('');
        }
    }

    function getalias_en(path){
        if(path!=''){
            $.ajax({
                url: "<?php echo base_url().ADMINPATH.'/home/system_ajax/site_load_alias/' ?>",
                dataType: "html",
                type: "POST",
                data: "ob=cat&data="+path+"&ID=<?php echo $data->ID ?>",
                beforeSend: function(){
                    $("#loadingalias_en").show();
                },
                success: function(result){
                    if(result!=false){
                        $("#AliasLink_en").val(result);
                    }
                    $("#loadingalias_en").hide();
                }
            });
        }else{
            $("#AliasLink_en").val('');
        }
    }

    function removeoption(ob){
        var formgroup = $(ob).parent("label").parent("div");
        formgroup.remove();
    }
</script>
