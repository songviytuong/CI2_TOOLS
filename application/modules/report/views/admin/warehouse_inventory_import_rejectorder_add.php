<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<div class="fillter_bar">
				<div class="block1">
					<h1>PHIẾU NHẬP KHO HÀNG BÁN BỊ TRẢ LẠI</h1>
				</div>
				<div class="block2">
					
				</div>
			</div>
			<div class="box_content_warehouse_import">
				<div class="block1">
					<table class="table1">
						<tr>
							<td>Tại kho:</td>
							<td>
								<select name='KhoID'>
									<?php 
									$warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse order by MaKho ASC")->result();
									if(count($warehouse)>0){
										foreach($warehouse as $row){
											echo "<option value='$row->ID'>$row->MaKho</option>";
										}
									}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td>Ngày chứng từ:</td>
							<td><input type='text' name='NgayNK' id='NgayNK' value='<?php echo date('Y-m-d') ?>' readonly="true" /></td>
						</tr>
						<tr>
							<td>Hình thức:</td>
							<td>
								<input type='radio' name="Type" value="0" onclick='change_type(this)' />
								<span>Mua hàng</span>
								<input type='radio' name="Type" value="1" onclick='change_type(this)' checked="checked" />
								<span>Hàng trả về</span>
								<input type='radio' name="Type" value="2" onclick='change_type(this)' />
								<span>Thành phẩm / Trả kho nội bộ</span>
							</td>
						</tr>
						<tr>
							<td>Diễn giải:</td>
							<td><input type='text' name="Note" required style="width:100%" /></td>
						</tr>
						<tr class='findphieu'>
							<td>Tìm phiếu XK:</td>
							<td><input type='text' id='input_pxk' /> <a class='btn btn-default' onclick='find_pxk(this)'><i class="fa fa-search"></i> Tìm phiếu</a>
								<input type='hidden' value='0' name='ExportID' id='ExportID' />
							</td>
						</tr>
						<tr class='phieuinfo'>
							<td>Mã phiếu XK:</td>
							<td><input type='text' id="MaPXK" readonly="true" /> <span>Tên người nhận:</span> <input type='text' id="ReciverName" readonly="true" /> <span>Ngày xuất kho:</span> <input type='text' id="DayExport" readonly="true" /></td>
						</tr>

					</table>
				</div>
				<!-- end block1 -->
		    	<div class="block2">
					
		    	</div>
		    	<div class="clear"></div>
		    	<div class="table_donhang">
		    		<div id="inner_table_pxk"></div>
		    		<div class='last'>
		    			<div>
		    				<li><b>Tình trạng chứng từ:</b></li>
		    				<li>
		    					<select name="Status">
		    						<option value='0'>Yêu cầu nhập kho</option>
		    					</select>
		    				</li>
		    			</div>
		    			<div>
		    				<li><b>Ghi chú tình trạng:</b></li>
		    				<li><textarea name="Ghichu"></textarea></li>
		    			</div>
		    			<button class='btn btn_default'><i class="fa fa-refresh"></i> Cập nhật</button>
		    		</div>
		    	</div>
			</div>
		</form>
		<input type='hidden' id='baselink' value='<?php echo $base_link ?>' />
	</div>
	<div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#NgayNK,.DateProduction').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });

	var link = $("#baselink").val();

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function find_pxk(ob){
		var mapxk = $("#input_pxk").val();
		if(mapxk!=''){
			$.ajax({
	            url: link+"find_PXK",
	            dataType: "html",
	            type: "POST",
	            data: "Getdata=0&MaPXK="+mapxk,
	            success: function(result){
	            	data = result.split("||",6);
	            	var status = data[0];
	            	if(status==1){
	            		alert(data[1]);
	            	}else{
	            		$("#MaPXK").val(data[1]);
	            		$("#ReciverName").val(data[2]);
	            		$("#DayExport").val(data[3]);
	            		$("#ExportID").val(data[4]);
	            		$("#inner_table_pxk").html(data[5]);
	            	}
	            }
	        });
        }else{
        	$("#input_pxk").focus();
        }
	}

	function change_type(ob){
		var data = parseInt($(ob).val());
		var destination = '';
		if(data==0){
			destination="import_from_production";
		}
		if(data==1){
			destination="import_from_rejectorder";
		}
		if(data==2){
			destination="import_from_internal";
		}
		window.location=link+destination;
	}

	function recal(){
		var tongcong = 0;
		$("#table_data .selected_products").each(function(){
			var parent = $(this).parent('td').parent('tr');
			amount = parseInt(parent.find('input.Amount_input').val());
			valueCurrency = parent.find('input.ValueCurrency_input').val();
			pricecurrentcy = parent.find('input.PriceCurrency_input').val();
			total = (amount*pricecurrentcy)*valueCurrency;
			tongcong = tongcong+total;
			parent.find('input.Total_input').val(total);
			parent.find('.TotalVND').html(total.format('a',3));
		});
		$(".tongcong").html(tongcong.format('a',3));
	}

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		var cls = $(ob).attr('class');
		Amount_input = parseInt(parent.find('td input.Amount_input').val());
		PriceCurrency_input = parent.find('input.PriceCurrency_input').val();
		ValueCurrency = parent.find('input.ValueCurrency_input').val();
		value = $(ob).val();
		if(cls=="PriceCurrency_input"){
			TotalCurrency = value*Amount_input;
			parent.find('input.TotalCurrency_input').val(TotalCurrency);
		}
		if(cls=="TotalCurrency_input"){
			PriceCurrency = value/Amount_input;
			parent.find('input.PriceCurrency_input').val(PriceCurrency);
		}
		if(cls=="Amount_input"){
			TotalCurrency = value*PriceCurrency_input;
			parent.find('input.TotalCurrency_input').val(TotalCurrency);
		}
		recal();
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function deleterow(ob){
		$(ob).parent('td').parent('tr').remove();
		recal();
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

</script>