<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa sản phẩm</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class='block1'><span class="title">Tên sản phẩm</span></div>
					<div class='block2'><input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Mã sản phẩm</span></div>
					<div class='block2'><input type='text' class="form-control" name="MaSP" value="<?php echo isset($data->MaSP) ? $data->MaSP : '' ; ?>" required /></div>
				</div>
				
				<div class="row">
					<div class='block1'><span class="title">Giá bán sản phẩm</span></div>
					<div class='block2'><input type='number' class="form-control" name="Price" value="<?php echo isset($data->Price) ? $data->Price : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Thuộc nhóm ngành hàng</span></div>
					<div class='block2'>
						<select name="CategoriesID" class="form-control">
							<option value="0">-- Chọn nhóm ngành hàng --</option>
							<?php 
							$categories = $this->db->query("select * from ttp_report_categories")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									$selected = $row->ID==$data->CategoriesID ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $selected>$row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class='block1'><span class="title">Đơn vị tính</span></div>
					<div class='block2'>
						<select name="Donvi" class="form-control">
							<option value="">-- Chọn đơn vị tính --</option>
							<option value="Hộp" <?php echo $data->Donvi=='Hộp' ? "selected='selected'" : '' ; ?>>Hộp</option>
							<option value="Cái" <?php echo $data->Donvi=='Cái' ? "selected='selected'" : '' ; ?>>Cái</option>
							<option value="Bộ" <?php echo $data->Donvi=='Bộ' ? "selected='selected'" : '' ; ?>>Bộ</option>
							<option value="Thùng" <?php echo $data->Donvi=='Thùng' ? "selected='selected'" : '' ; ?>>Thùng</option>
						</select>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>