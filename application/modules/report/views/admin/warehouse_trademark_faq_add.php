<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST" enctype="multipart/form-data">
			<div class="fillter_bar">
				<div class="block1">
					<h1>Tạo mới câu hỏi của thương hiệu</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-danger"><i class="fa fa-check-square"></i> Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Trạng thái hoạt động</label>
							<div class='col-xs-8'>
								<input type='radio' name="Published" value="1" checked="true" /> Enable 
								<input type='radio' name="Published" value="0" /> Disable 
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Thuộc thương hiệu</label>							
							<div class='col-xs-8'>
								<select class="form-control" name="TrademarkID">
									<option>Chọn thương hiệu</option>
									<?php foreach ($list_brand as $row) :?>
										<option value="<?php echo $row->ID ?>"><?php echo $row->Title ?></option>
									<?php endforeach ?>
								</select>								
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Câu hỏi</label>
							<div class='col-xs-8'>								
								<textarea rows="5" class="form-control" name="Question" required ><?php echo isset($data->Question) ? $data->Question : '' ; ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
							<label for="" class="col-xs-4 control-label">Câu trả lời</label>
							<div class='col-xs-8'>
								<textarea class="form-control" name="Answer" rows="10" required><?php echo isset($data->Answer)? $data->Answer : '' ?> </textarea>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</form>
	</div>
</div>