<?php

class Verify_model extends CI_Model {

    var $tbl_user = 'ttp_user';
    var $ttp_role = 'ttp_role';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function verfiy($params) {
        $token = isset($_POST['ttp']) ? $_POST['ttp'] : '';
        $_user = mysql_real_escape_string($params[0]);
        $_pwd = mysql_real_escape_string($params[1]);
        $_pwd = sha1($_pwd);
        $this->db->select('a.ID as ID,a.UserType as UserType,a.IsAdmin as IsAdmin,a.AllowRemote as AllowRemote,a.HomePage as HomePage');
        $this->db->from('ttp_user a');
        $this->db->join('ttp_role b', 'a.RoleID = b.ID', 'INNER');
        $this->db->where('a.UserName', $_user);
        $this->db->where('a.Password', $_pwd);
        $this->db->where('a.Published', '1');
        $this->db->where('b.Published', '1');
        return $this->db->get()->row();
    }

}
