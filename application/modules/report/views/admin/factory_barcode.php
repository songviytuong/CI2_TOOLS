<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>DANH MỤC BARCODE THÙNG HÀNG</h1>
	    </div>
	    <div class="block2">
            
	    </div>
    </div>
    <div class="import_orderlist">
    	<div class="block2">
    		<div class="row">
                <div class="col-xs-8">
                    <a class="btn btn-danger" role="button" onclick="add_barcode(this)"><i class="fa fa-upload"></i> Tạo mã Barcode</a>
                    <a class="btn btn-primary" onclick="export_barcode(this)" role="button" data="<?php echo base_url().ADMINPATH."/report/factory/export_barcode" ?>"><i class="fa fa-download"></i> Export mã Barcode</a>
                </div>
                
                <div class="col-xs-4">
                    <form action="<?php echo $base_link ?>" method="get">
                        <input type="text" name="keywords" class="form-control" placeholder="Tìm kiếm : Điền mã Barcode cần tìm kiếm ..." />
                    </form>
                </div>
            </div>
    	</div>
        
    	<div class="block3 table_data">
            <?php if(count($data)>0){ ?>
                <table>
                    <tr>
                        <th>STT</th>
                        <th>Mã BARCODE</th>
                        <th>Tình trạng</th>
                        <th>Sức chứa thùng hàng</th>
                        <th>Loại thùng</th>
                    </tr>
                    <?php 
                        $result = $this->db->query("select Title,ID,Tankage from ttp_report_products where CategoriesID like '%\"62\"%'")->result();
                        $products = array();
                        if(count($result)>0){
                            foreach($result as $row){
                                $products[$row->ID] = array("Tankage"=>$row->Tankage,'Title'=>$row->Title);
                            }
                        }

                        $arr = array(0=>'<span class="text-warning"><i class="fa fa-exclamation-triangle"></i> Chưa nhập sản phẩm</span>',1=>'<span class="text-success"><i class="fa fa-exclamation-triangle"></i> Đã nhập sản phẩm</span>',2=>'<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Đã xé thùng quét lẻ</span>');
                        $i = $start+1;
                        foreach($data as $row){
                            echo "<tr>";
                            echo "<td>$i</td>";
                            echo $row->Code=='' ? "<td>--</td>" : "<td>$row->Code</td>";
                            echo isset($arr[$row->Status]) ? "<td>".$arr[$row->Status]."</td>" : "<td>--</td>";
                            echo isset($products[$row->ProductsID]['Tankage']) ? "<td>".str_pad($products[$row->ProductsID]['Tankage'],  2, '0', STR_PAD_LEFT)." sản phẩm / thùng</td>" : "<td>--</td>";
                            echo isset($products[$row->ProductsID]['Title']) ? "<td>".$products[$row->ProductsID]['Title']."</td>" : "<td>--</td>";
                            echo "</tr>";
                            $i++;
                        }
                    
                    ?>
                    <tr>
                        <td colspan='8'>
                            <?php 
                            $status1 = 0;
                            $status2 = 0;
                            if(count($status)>0){
                                foreach($status as $row){
                                    $status1 = $row->Status==0 ? $row->total : $status1 ;
                                    $status2 = $row->Status==1 ? $row->total : $status2 ;
                                }
                            }
                            ?>
                            <a class='btn btn-default'>Tổng cộng : <b><?php echo number_format($find) ?></b> mã đã tạo</a>
                            <a class='btn btn-default'><span class='text-warning'><b><?php echo number_format($status1) ?></b> chưa nhập sản phẩm</span></a>
                            <a class='btn btn-default'><span class='text-success'><b><?php echo number_format($status2) ?></b> đã nhập sản phẩm</span></a>
                        </td>
                    </tr>
                </table>

                <?php 
                echo $nav;
            }else{
                echo count($data)==0 ? "<p><img src='public/admin/images/view_empty_arrow.png' /> Bấm để nhập danh sách mới .</p>" : ""; 
            }
            ?>
    	</div>
    </div>
    <div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/factory/" ?>" />
</div>
<script>
    var baselink = $("#baselink_report").val();

    $("#close_overlay").click(function(){
        $(".over_lay").hide();
    });

    function showimport(){
        $(".over_lay .box_inner").css({'margin-top':'50px'});
        $(".over_lay .box_inner .block1_inner h1").html("Import BARCODE FROM FILE");
        $(".over_lay .box_inner .block2_inner").load(baselink+"load_import_barcode_form");
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    function activeimport(){
        var Fileinput = document.getElementById("file_import");
        var file = Fileinput.files[0];
        var fd = new FormData();
        fd.append('file', file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', baselink+'upload_import', true);
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                $(".over_lay .box_inner .block2_inner").css({'overflow-x':'hidden'});
                $(".over_lay .box_inner .block2_inner #form-file p:first-child").html('<div class="loader">Loading...</div>');
                $(".over_lay .box_inner .block2_inner #form-file h4").html("Đang tải file lên hệ thống ...");
            }
        };
        xhr.onload = function() {
            if (this.status == 200) {
                $(".over_lay .box_inner .block2_inner").html(xhr.responseText);
            }else{
                console.log(xhr.responseText);
            }
        };
        xhr.send(fd);
    }

    function rmtr(ob){
        $(ob).parent('td').parent('tr').remove();
    }

    function active_import(ob){
        $(ob).addClass("saving");
        $(ob).html('Loading...');
        var arr = new Array();
        $(".over_lay .box_inner .block2_inner table tr.treach").each(function(){
            var id = $(this).find('.td0').html();
            var soluong = $(this).find('.td3').html();
            if(id!='' && id!=0 && soluong!=0){
                var jsonArg1 = new Object();
                jsonArg1.id = id;
                jsonArg1.soluong = soluong;
                arr.push(jsonArg1);
            }
        });
        $.ajax({
            dataType: "json",
            data: "data="+JSON.stringify(arr),
            url: baselink+'active_upload_import_barcode',
            cache: false,
            method: 'POST',
            success: function(result) {
                if(result.Error==null){
                    location.reload();
                }else{
                    $(".over_lay .box_inner .block2_inner").load(baselink+"load_import_barcode_form");
                    alert(result.Error);
                }
            },
            error:function(result){
                if(result.status==500){
                    alert(result.statusText);
                }
                console.log(result.responseText);
                $(ob).removeClass("saving");
                $(ob).html("<i class='fa fa-upload'></i> Nhập vào hệ thống");
                $(".over_lay .box_inner .block2_inner").load(baselink+"load_import_barcode_form");
            }
        });
    }

    function preview_trace(id){
        $(window).scrollTop(70);
        $(".over_lay .box_inner").css({'margin-top':'50px'});
        $(".over_lay .box_inner .block1_inner h1").html("QR CODE TRACE");
        $(".over_lay .box_inner .block2_inner").load(baselink+"load_preview_trace/"+id);
        $(".over_lay").removeClass('in');
        $(".over_lay").fadeIn('fast');
        $(".over_lay").addClass('in');
    }

    function add_barcode(ob){
        var getValue = prompt("Điền số lượng barcode muốn tạo mới ", "");
        if(getValue!=null){
            if(getValue<=1000){
                $(ob).html("Đang tạo mã ...");
                $(ob).addClass('saving');
                $.ajax({
                    dataType: "text",
                    data: "Number="+getValue,
                    url: baselink+'created_barcode',
                    cache: false,
                    method: 'POST',
                    success: function(result) {
                        location.reload();
                    }
                });
            }else{
                alert("Bạn chỉ có thể khởi tạo tối 1.000 mã Barcode 1 lần .");
                location.reload();
            }
        }
    }

    function export_barcode(ob){
        var getValue = prompt("Điền số lượng barcode cần xuất để in", "");
        var datalink = $(ob).attr('data');
        if(getValue!=null){
            if(getValue>0){
               window.location = datalink+"/"+getValue;
            }else{
                alert("Bạn cần phải nhập số lượng chính xác !");
                location.reload();
            }
        }
    }
</script>