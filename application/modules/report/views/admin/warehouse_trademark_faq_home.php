<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<h1>Danh sách câu hỏi cho thương hiệu</h1>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary">Add new</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th class="col-question">Câu hỏi</th>
					<th class="col-answer">Câu trả lời</th>
					<th class="col-brand">Thương hiệu</th>
					<th class="col-status">Trạng thái sử dụng</th>
					<th class="col-action">Action</th>
				</tr>
				<?php 
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						$status = $row->Status==1 ? "Enable" : "Disable" ;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Question</td>";
						echo "<td>".$this->lib->splitString($row->Answer, 100)."</td>";
						echo "<td>$row->t_Title</td>";
						echo "<td>$status</td>";
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}else{
					echo "<td colspan='6'>Không tìm thấy dữ liệu</td>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>

<style type="text/css">
	.col-question{
		min-width: 200px;
	}
	.col-action, .col-status{
		min-width: 140px;
	}
	.col-brand{
		min-width: 105px;
	}
	.table_data tr td{
		vertical-align: top;
	}
</style>