<div class="sub_menu">
	<div class="list-open-bill pull-left">
		<?php 
		$files = scandir('log/pos/purchase_order');
		$first = '';
		if(count($files)>0){
			$i=1;
			foreach($files as $row){
				if($row!='.' && $row!='..'){
					$name = explode('_',$row);
					$name = str_replace('.txt','',$name[2]);
					if($i==1){
						$first = $name;
					}
					$current = $i==1 ? 'current' : '' ;
					echo '<a class="btn control-bill-'.$name.' '.$current.'"><b onclick="purchase_order_open_order('.$name.')">Phiếu nhập hàng '.$i.'</b> <span onclick="purchase_order_closebill('.$name.')"><i class="fa fa-times"></i></span></a>';
					$i++;
				}
			}
			echo '<script>
					var stt = '.number_format($i-1).';
					var order = '.number_format($i-1).';
					var ordercurrent = '.$first.';
				</script>';
		}
		
		?>
	</div>
	<a onclick="purchase_order_openbill(this)" class="btn"><b><i class="fa fa-plus"></i></b></a>
</div>
<div class="containner">
	<div class="block1">
		<div class="data_table">
			<div class="tools">
				<div class="row">
					<div class="col-xs-4" style="position:relative;">
						<input type="text" class="form-control" placeholder="Nhập tên sản phẩm cần tìm ..." onchange="purchase_order_get_products_list(this)" id="keywords" autocomplete="off" />
						<div id="auto_complete" class='hidden auto_complete'></div>
					</div>
					<div class="col-xs-4">
						<div class="dropdown">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						    <i class="fa fa-search" aria-hidden="true"></i> Tìm tự động
						    <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						    <li><a onclick="purchase_order_change_search_type(0)"><i class="fa fa-search" aria-hidden="true"></i> Tìm tự động</a></li>
						    <li><a onclick="purchase_order_change_search_type(1)"><i class="fa fa-barcode" aria-hidden="true"></i> Quét Barcode</a></li>
						  </ul>
						</div>
					</div>
					<div class="col-xs-4 text-right">
						<a onclick="purchase_order_reset_bill_draft()" class="btn btn-default"><i class="fa fa-reply-all" aria-hidden="true"></i> Làm lại</a>
						<a onclick="purchase_order_drop_bill_draft()" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hủy đơn hàng</a>
					</div>
				</div>
			</div>
			<div id="table_content" style="position:relative">
				<table class="table">
					<tr>
						<th>MÃ SKU</th>
						<th>TÊN SẢN PHẨM</th>
						<th>ĐƠN VỊ</th>
						<th>SỐ LƯỢNG</th>
						<th>ĐƠN GIÁ NHẬP</th>
						<th>THÀNH TIỀN</th>
						<th class="text-center">HỦY</th>
					</tr>
					<?php 
					$total_order = 0;
					$total_products = 0;
					$total_reduce = 0;
					if($first!=''){
						$data = file_get_contents('log/pos/purchase_order/draft_order_'.$first.'.txt');
						$data = json_decode($data,true);
						if(isset($data['reduce'])){
							$total_reduce = $data['reduce'];
						}
						$WarehouseID = isset($data['partner']['WarehouseID']) ? $data['partner']['WarehouseID'] : 0 ;
						if(isset($data['products']) && count($data['products'])>0){
							$key = array_keys($data['products']);
							$arr_inventory = array();
							$inventory = $this->db->query("select * from ttp_report_inventory where ProductsID in(".implode(',',$key).") and LastEdited=1 and WarehouseID=$WarehouseID")->result();
							if(count($inventory)>0){
								foreach($inventory as $row){
									if(isset($arr_inventory[$row->ProductsID])){
										$arr_inventory[$row->ProductsID] = $arr_inventory[$row->ProductsID]+$row->OnHand;
									}else{
										$arr_inventory[$row->ProductsID] = $row->OnHand;
									}
								}
							}
							$total_products = count($key);
							$products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
							if(count($products)>0){
								foreach ($products as $row) {
									$invent = isset($arr_inventory[$row->ID]) ? $arr_inventory[$row->ID] : 0 ;
									$amount = $data['products'][$row->ID];
									$price = $row->RootPrice;
									$diff = '';
									if(isset($data['price'][$row->ID])){
										if($data['price'][$row->ID]<$price){
											$diff = $price-$data['price'][$row->ID];
											$diff = '<span style="color:#999;display:block;margin-top:8px">Đã giảm : '.number_format($diff).'</span>';
										}elseif($data['price'][$row->ID]>$price){
											$diff = $data['price'][$row->ID]-$price;
											$diff = '<span style="color:#999;display:block;margin-top:8px">Đã tăng : '.number_format($diff).'</span>';
										}
										$price = $data['price'][$row->ID];
									}
									$total = $amount*$price;
									$total_order += $total;
									echo '<tr>
									<td>'.$row->MaSP.'</td>
									<td style="width:350px">
										<p>'.$row->Title.'</p>
									</td>
									<td>'.$row->Donvi.'</td>
									<td>
										<input type="number" value="'.$data['products'][$row->ID].'" style="width:100px" onchange="purchase_order_change_products(this,'.$row->ID.')" />
										<span style="color:#999;display: block;margin-top:8px">Hiện còn : '.$invent.'</span>
									</td>
									<td><input type="number" value="'.$price.'" onchange="purchase_order_change_price(this,'.$row->ID.')" />'.$diff.'
									</td>
									<td>'.number_format($total).'</td>
									<td class="text-center"><a onclick="purchase_order_remove_products('.$row->ID.')"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
								}
							}
						}else{
							echo "<tr><td colspan='8'>Vui lòng chọn sản phẩm</td></tr>"; 
						}
					}
					?>
					<tr>
						<th colspan="5" class="text-right">TỔNG SỐ SẢN PHẨM</th>
						<th colspan="3">: <?php echo number_format($total_products) ?></th>
					</tr>
					<tr>
						<th colspan="5" class="text-right">GIÁ TRỊ ĐƠN HÀNG</th>
						<th colspan="3">: <?php echo number_format($total_order) ?></th>
					</tr>
					<tr>
						<th colspan="5" class="text-right">CHIẾT KHẤU</th>
						<th colspan="3">: <?php echo number_format($total_reduce) ?></th>
					</tr>
					<tr style="font-size: 17px">
						<th colspan="5" class="text-right">TỔNG TIỀN PHẢI TRẢ</th>
						<th colspan="3">: <?php echo number_format($total_order-$total_reduce) ?>đ</th>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div class="block2">
		<div class="order_info">
			<h3>Thông tin nhập hàng</h3>
			<hr>
			<?php 
			$ProductionID = isset($data['partner']['ID']) ? $data['partner']['ID'] : 0 ;
			$Number = isset($data['partner']['Number']) ? $data['partner']['Number'] : '' ;
			$note = isset($data['partner']['Note']) ? $data['partner']['Note'] : '' ;
			$Day = isset($data['partner']['Day']) ? $data['partner']['Day'] : '' ;
			?>
			<div class="row">
				<label class="control-label col-xs-6">
					Ngày nhập hàng
				</label>
				<label class="control-label col-xs-6">
					Số chứng từ (nếu có)
				</label>
				
				<div class="col-xs-6">
					<div class="input-group"> 
						<span class="input-group-addon" style="background: none;border: none;border-bottom: 1px solid #E1E1E1;padding-left: 0px;">
							<i class="glyphicon glyphicon-calendar"></i>
						</span> 
						<input type="text" id="purchase_day" class="form-control date-picker" value="<?php echo $Day ?>" />
					</div>
				</div>
				<div class="col-xs-6">
					<input type="text" class="form-control" placeholder="Nhập số chứng từ ..." id="purchase_order_bill_Code" onchange="purchase_order_apply_code_bill(this)" value="<?php  echo $Number ?>" />
				</div>
				
			</div>
			<div class="row">
				<label class="control-label col-xs-6">
					Nhập hàng tại kho
				</label>
				<label class="control-label col-xs-6">
					Nhà cung cấp
				</label>
				<div class="col-xs-6">
					<select name="Payment" id="purchase_order_bill_WarehouseID" onchange="purchase_order_apply_warehouse(this)" class="form-control">
						<?php 
						$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
						if(count($warehouse)>0){
							foreach($warehouse as $row){
								$select = $row->ID==$WarehouseID ? 'selected="selected"' : '' ;
								echo "<option value='$row->ID' $select>$row->MaKho</option>";
							}
						}
						?>
					</select>
				</div>
				<div class="col-xs-6">
					<select class="form-control" id="ProductionID" onchange="purchase_order_apply_production_bill(this)">
						<?php 
						$production = $this->db->query("select * from ttp_report_production")->result();
						if(count($production)>0){
							foreach ($production as $row) {
								$select = $row->ID==$ProductionID ? 'selected="selected"' : '' ;
								echo "<option value='$row->ID' $select>$row->Title</option>";
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="row">
				<label class="control-label col-xs-12">
					Ghi chú thêm
				</label>
				<div class="col-xs-12">
					<textarea name="Note" id="purchase_order_bill_CustomerNote" onchange="purchase_order_apply_note_bill(this)" rows="3" class="form-control" placeholder="Nhập ghi chú trên hóa đơn (nếu có) ..."><?php  echo $note ?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php 
					$active = isset($data['partner']['Active']) && $data['partner']['Active']>0 ? 'hidden' : '' ;
					$inactive = isset($data['partner']['Active']) && $data['partner']['Active']==0 ? 'hidden' : '' ;
					$href = isset($data['partner']['Active']) && $data['partner']['Active']>0 ? ADMINPATH.'/pos/pos_inventory_import/print_order/'.$data['partner']['Active'] : '#' ;
					?>
					<a id="btn_createorder" onclick="purchase_order_create_order(this)" class="btn btn-success <?php echo $active ?>"><i class="fa fa-check-square" aria-hidden="true"></i> Hoàn tất</a>
					<a href="<?php echo $href ?>" id="btn_printorder" target="_blank" class="btn btn-primary <?php echo $inactive ?> print_button"><i class="fa fa-print" aria-hidden="true"></i> In hóa đơn</a>
					<a onclick="purchase_order_closebill_button(this)" id="btn_closeorder" class="btn btn-default <?php echo $inactive ?>"><i class="fa fa-times" aria-hidden="true"></i> Đóng hóa đơn</a>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>

<!-- daterangepicker -->
<script type="text/javascript" src="public/admin/js/moment.min2.js"></script>
<script type="text/javascript" src="public/admin/js/datepicker/daterangepicker.js"></script>
<script>
	$(document).ready(function () {
        $('#purchase_day').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });
</script>