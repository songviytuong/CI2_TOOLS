<div class="containner">
	<div class="bill_print">
		<div class="block1">
			<div class="block1_1">
				<p>CÔNG TY TNHH TM DV TRẦN TOÀN PHÁT</p>
				<p>246/9 Bình Quới, P.28, Q.Bình Thạnh, TPHCM</p>
				<p>Mã số thuế: 0310717887</p>
			</div>
			<div class="block1_2">
				<?php 
				$bill = $this->db->query("select * from ttp_report_transferorder_bill where OrderID=$data->ID")->row();
				if($bill){
					echo '<a id="print_page"><i class="fa fa-print"></i> In Phiếu</a>';
				}
				?>
				<a id="save_page"><i class="fa fa-check-circle"></i> Lưu</a>
				<a id="back_page" href='<?php echo base_url().ADMINPATH."/report/warehouse_inventory_transferproducts/edit/$data->ID" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<?php 
				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_transferorder_bill where MONTH(DateCreated)=$thismonth and YEAR(DateCreated)=$thisyear")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                $max = "LCNB".$thisyear.$thismonth.'.'.str_pad($max, 5, '0', STR_PAD_LEFT);
				?>
				<h1>Phiếu lưu chuyển nội bộ</h1>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $bill ? $bill->Code : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<p>Mẫu số : 02-VT</p>
				<p>(Ban hành theo TT200/2014/TT-BTC ngày 22/12/2014 của bộ trưởng BTC)</p>
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $bill ? $bill->TKNO : "1311" ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $bill ? $bill->TKCO : "5115" ; ?>" /></td>
					</tr>
					<tr>
						<td>KPP</td>
						<td><input type="text" id="KPP" value="<?php echo $bill ? $bill->KPP : "OL" ; ?>" /></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="block2">
			<div class="row">
				<li>Theo yêu cầu số:</li>
				<li><?php echo $data->OrderCode ?></li>
				<li>Ngày tạo yêu cầu:</li>
				<li><?php echo date('d/m/Y',strtotime($data->DateCreated)) ?> </li>
			</div>
			<div class="row">
				<li>Xuất tại kho:</li>
				<li><?php echo $data->SenderTitle ?></li>
				<li>Ngày xuất kho:</li>
				<li><?php echo date('d/m/Y',strtotime($data->ExportDate)) ?> </li>
			</div>
			<div class="row">
				<li>Địa chỉ kho xuất:</li>
				<li class="special"><?php echo $data->SenderAddress ?></li>
			</div>
			<div class="row"></div>
			<div class="row">
				<li>Nhận tại kho:</li>
				<li><?php echo $data->ReciverTitle ?></li>
				<li>Ngày nhập kho:</li>
				<li><?php echo date('d/m/Y',strtotime($data->ImportDate)) ?> </li>
			</div>
			<div class="row">
				<li>Địa chỉ kho nhập:</li>
				<li class="special"><?php echo $data->ReciverAddress ?></li>
			</div>
			<div class="row">
				<li>Lý do lưu chuyển:</li>
				<li><input type="text" id="lydoxuatkho" value="<?php echo $bill ? $bill->Note : "Chuyển kho bán hàng" ; ?>" placeholder="Điền lý do lưu chuyển" /></li>
			</div>
			<div class="row">
				<li>Ghi chú yêu cầu:</li>
				<li class="special"><?php echo $data->Note ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th rowspan='2'>Số TT</th>
					<th rowspan='2'>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th rowspan='2'>Mã số</th>
					<th rowspan='2'>Đơn vị tính</th>
					<th colspan='3'>Số lượng</th>
				</tr>
				<tr>
					<th style="width:100px">Yêu cầu xuất</th>
					<th>Thực xuất</th>
					<th>Thực nhập</th>
				</tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi,c.ShipmentCode,d.ShipmentCode as ShipmentCodeImport from ttp_report_transferorder_details a,ttp_report_products b,ttp_report_shipment c,ttp_report_shipment d where a.ShipmentIDImport=d.ID and a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->Donvi</td>";
						echo "<td>".number_format($row->Request,3)."</td>";
						echo "<td>".number_format($row->TotalExport,3)."</td>";
						echo "<td>".number_format($row->TotalImport,3)."</td>";
						echo "</tr>";
						$i++;
					}
				}
				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				?>
				
				<tr>
					<td></td>
					<td colspan="4">TỔNG CỘNG </td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>

		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
				<div></div>
				<p>In từ tools.trantoanphat.com <br><?php echo date("H:i A d/m/Y",time()) ?></p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Thủ kho</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Kế toán trưởng</p>
				<p>(Ký, họ tên)</p>
				<div>Đ.N. Nhật Thảo</div>
			</div>
			<div>
				<p>Giám đốc</p>
				<p>(Ký, họ tên)</p>
				<div>Trần Quốc Dũng</div>
			</div>
		</div>
	</div>
</div>
<script>
	$("#print_page").click(function(){
		window.print();
	});

	$("#save_page").click(function(){
		$(this).addClass("saving");
		var TKNO 	= $("#TKNO").val();
		var TKCO 	= $("#TKCO").val();
		var KPP 	= $("#KPP").val();
		var Lydo 	= $("#lydoxuatkho").val();
		if(TKNO=="" || TKCO=="" || KPP=="" || Lydo==''){
			$(this).removeClass("saving");
			alert("Vui lòng điền đầy đủ các thông tin cần thiết trước khi lưu và in phiếu !");
			return false;
		}else{
			$.ajax({
	        	url: "<?php echo $base_link ?>print_bill",
	            dataType: "html",
	            type: "POST",
	            context:this,
	            data: "OrderID=<?php echo $data->ID ?>&TKNO="+TKNO+"&TKCO="+TKCO+"&KPP="+KPP+"&Lydo="+Lydo,
	            success: function(result){
	            	$(this).removeClass("saving");
	            	location.reload();
	            }
	        });
		}
	});

	$(document).on('keydown', function(e) {
	    if(e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80) ){
	        e.cancelBubble = true;
	        e.preventDefault();
	        e.stopImmediatePropagation();
	    }  
	});
</script>