<?php 
$url = base_url().ADMINPATH.'/training';
?>
<input type='hidden' id="link" value="<?php echo base_url().ADMINPATH."/training" ?>" />
<div class="containner">
	<div class="study_box training-box">
		<div class="blockstudy_1">
			<div class="blockstudy_1_1">
				<b><i class="fa fa-book"></i> Khóa học : <span>kiến thức về da và collagen</span></b>
				<h1> <span id='title'></span></h1>
			</div>
			<div class="blockstudy_1_2">
				<div class="scroll_content">
					<form class="form-box">
						<div class="tab_result" style="display:none">
							<div class="col-xs-5">
								<div align="center" class="title_point"><h2></h2></div>
								<div align="center" class="img bg_point"><div class="v_point"></div></div>
								<div align="center" class="show">
									<div class="trues text-success">Đúng</div>
									<div class="falses text-danger">Sai</div>
								</div>
								<h6>Điểm trung bình : <span class="abs"></span></h6>
							</div>
							<div class="col-xs-7">
								<div class="table_data">
									<table class="table table-bordered table_kq">
										<tr>
											<th>Câu</th>
											<th><i class="fa fa-user"></i> Kết quả của bạn</th>
											<th><i class="fa fa-users"></i> Các học viên khác</th>
										</tr>
									</table>
								</div>
							</div>
						</div>

						<input type="hidden" id="courseid" name="courseid" value=""></input>
						<input type="hidden" id="partid" name="partid" value=""></input>
						<input type="hidden" id="chapterid" name="chapterid" value=""></input>
						<input type="hidden" id="lessonid" name="lessonid" value=""></input>
						<input type="hidden" id="questionid" name="questionid" value=""></input>
						<input type="hidden" id="lms" name="lms" value=""></input>
						<div id="show_video"></div>
						<div class="content_full hidden" id="des_full"></div>
						<div class="showanswer hidden" id="showanswer"></div>
						<div id="result" class="hidden"></div>
						<div class="ttitle hidden" id="title2"></div>
						<div class="sz20" id="des_full2"></div>
						<div ><em id="des_soft"></em></div>
						<div id="start"></div>
					</form>
				</div>
			</div>
			<div class="blockstudy_1_3">
				<div id="next">Bài tiếp theo</div>
				<div id="pre"></div>
				<div id="finsh"></div>
			</div>
		</div>
		<div class="blockstudy_2">
			<div class="scroll_content">
				<?php 
				$stt =0;
				$checked=0;
				$default = 0;
				$certificate_status = 0;
				foreach ($list as $key => $value) {
					if(isset($value["title"])){
						echo  "<h2>  ".$value["title"]."</h2>"; 
				?>
				<ul class="uls list-unstyled">
					<li>
					<?php if($value["arr_chapter"] !=null) {?>
				    <?php foreach ($value["arr_chapter"] as $key2 => $value2) {
				    		echo "<h3 class='title'><i class='fa fa-info-circle'></i> ".$value2["title"]."</h3>";
				    		$k = isset($value["lesson"][$value2["id"]][$value["id"]]) ? $value["lesson"][$value2["id"]][$value["id"]] : '';
				    		if(!empty($k)){
				    			foreach ($k as $key3 => $value3) {
				    				$bh=$key3+1;
				    				$stt++;
				    				$oncheck =0;
				    				if(in_array($value3["id"], $history1)){
				    					$oncheck =1;
				    					$checked++;
				    				}else{
				    					$certificate_status=1;
				    					$default= $default==0 ? $stt : $default;
				    				}
				    				if($value3["type"]==1){
				    					$icon = '<i class="fa fa-file-text-o"></i>';
				    				}
				    				if($value3["type"]==2){
				    					$icon = '<i class="fa fa-file-video-o"></i>';
				    				}
				    				if($value3["type"]==3){
				    					$icon = '<i class="fa fa-pencil"></i>';
				    				}
				    				if($testid==$value3["id"]){
				    					$testid = $stt;
				    				}
				    				?>
				    			<a onclick="lean(<?php echo $stt; ?>);return false;">
					    			<li class=" next_<?php echo $stt; ?>">
						    			<p class="click click_<?php echo $stt ?>"> 
							    			<?php 
							    			echo $icon." Bài ".$bh." : ".$value3["title"];
							    			$hour = isset($value3["hour"]) ? round($value3["hour"]/60,0) : 0;
							    			$hour = $hour<10 ? '0'.$hour : $hour ;
							    			$minus = isset($value3["hour"]) ? $value3["hour"]%60 : 0;
							    			$minus = $minus<10 ? '0'.$minus : $minus ;
							    			echo " <span>($hour:$minus)</span>";
							    			echo $oncheck==1 ? '<i class="fa fa-check-circle text-success"></i>' : '' ; 
							    			?>
						    			</p>
					    			</li>
				    			</a>
				    			<div style="display: none" id="lessonid_<?php echo $stt; ?>"><?php echo $value3["id"] ?></div>
				    			<div style="display: none" id="chapterid_<?php echo $stt; ?>"><?php echo $value2["id"] ?></div>
				    			<div style="display: none" id="partid_<?php echo $stt; ?>"><?php echo $value["id"] ?></div>
				    			<div style="display: none" id="courseid_<?php echo $stt; ?>"><?php echo $value["scienceid"]; ?></div>
				    			<div style="display: none" id="type_<?php echo $stt; ?>"><?php echo $value3["type"] ?></div>
				    			<div style="display: none" id="oncheck_<?php echo $stt; ?>"><?php echo $oncheck ?></div>
								<div style="display: none" id="lms_<?php echo $stt; ?>"><?php echo $stt ?></div>
					    		 <?php 
					    		}
					    	}
					    } 
					}
					?>
			    </li>
				</ul>
				<?php }
				} 
				?>

			</div>
		</div>
	</div>
	<div class="over_lay black">
        <div class="box_inner">
            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
            <div class="block2_inner"></div>
        </div>
    </div>
</div>

<input type="hidden" name="stt" id="stt" value="<?php echo $stt; ?>"> </input>
<input type="hidden" id="checked" value="<?php echo $checked; ?>"> </input>
<input type="hidden" name="tid" id="tid" value="<?php echo $testid==0 ? $default : $testid; ?>"> </input>
<script>
	$("#close_overlay").click(function(){
        $(".over_lay").hide();
        disablescrollsetup();
        $(".over_lay .block2_inner").html('');
    });

    function enablescrollsetup(){
        $("body").css({'height':'100%','overflow-y':'hidden'});
        h = window.innerHeight;
        h = h-200;
        $(window).scrollTop(70);
        $(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
    }

    function disablescrollsetup(){
        $("body").css({'height':'auto','overflow-y':'scroll'});
    }

	function view_lesson_popup(lesson,title){
		$(".over_lay").show();
		$(".over_lay").removeClass('in');
		$(".over_lay").fadeIn('fast');
		$(".over_lay").addClass('in');
		$(".over_lay .block1_inner h1").html(title);
		$(".over_lay .block2_inner").load("<?php echo base_url().ADMINPATH.'/training/training/get_introtext_lesson_by_id/' ?>"+lesson);
		$(window).scrollTop(70);
	}
	
	function update_history(limit,status){
		var lessonid = $('#lessonid_'+limit).html();
		var chapterid = $('#chapterid_'+limit).html();
		var partid = $('#partid_'+limit).html();
		var courseid = $('#courseid_'+limit).html();
		var type = $('#type_'+limit).html();

		var link = $('#link').val();
		var url = link+"/learn/update_history/"+lessonid+"/"+chapterid+"/"+partid+"/"+courseid+"/"+status;
		if(type !=3){
			if(status==1){
				var html_check = "<i class='fa fa-check-circle'></i>";
			}
			$('#checks_'+limit).html(html_check);
			$.post(url, function(data) {
			  console.log(data);
			});
		}
		
	}

	function nexts(limit){
		$(".tab_result").hide();
		var lm = limit-1;
		update_history(lm,1);
		var html_check = "<i class='fa fa-check-circle'></i>";
		$('#checks_'+lm).html(html_check);

		var html_check2 = "<i class='fa fa-times'></i>";
		$('#checks_'+limit).html(html_check2);
		lean(limit);
	}

	function pres(limit){
		lean(limit);
	}

	function reset_html(){
		$('#title').html('');
		$('#show_video').html('');
		$('#des_full').html('').addClass('hidden');
		$('#start').html('');
		$('#des_full2').html('').addClass('hidden');
		$('#title2').html('').addClass('hidden');
		$('#des_soft').html('').addClass('hidden');
		$('#showanswer').html('').addClass('hidden');
		$('#result').html('').addClass('hidden');
		$('.row_result').html('');
		$('.table_kq').hide();
		$('.table_point').hide();
	}

	$( window ).load(function() {
		var testid = $('#tid').val();
		if(testid !=0){
			lean(testid);	
		}else{
			lean(1);
		}
	});

	function lean(limit){
		limit = parseInt(limit);
		$("#next").show();
  		$("#pre").show();
  		$("#finsh").show();
		var lessonid = $('#lessonid_'+limit).html();
		var chapterid = $('#chapterid_'+limit).html();
		var partid = $('#partid_'+limit).html();
		var courseid = $('#courseid_'+limit).html();
		var type = $('#type_'+limit).html();
		var link = $('#link').val();
		var lms = $('#lms_'+limit).html();
		var html_document = "";
		
		var html_test = "";

		$("body,html").animate({
            scrollTop: 0
        }, 500);

		$( ".click" ).removeClass( "activeds" );
		$( ".click_"+limit ).addClass( "activeds");
		
		$('#courseid').val(courseid);
		$('#partid').val(partid);
		$('#chapterid').val(chapterid);
		$('#lessonid').val(lessonid);
		
		var lm = limit +1;
		var stt = parseInt($('#stt').val());
		var checked = parseInt($('#checked').val());
		if(stt==limit){
			if(checked!=stt){
				var html_next = "<a class='btn btn-danger' href='"+link+"/learn/get_certificate/"+courseid+"/"+lessonid+"/"+chapterid+"/"+partid+"/"+courseid+"'><i class='fa fa-graduation-cap'></i> Kết thúc khóa học</a>";
				$("#next").show();
			}else{
				$("#next").hide();
			}
		}else{
			var html_next = "<a class='btn btn-danger' onclick='nexts("+lm+");return false;'><i class='fa fa-step-forward'></i> Bài tiếp theo</a>";
			$("#next").show();
		}
		if(limit==1){
			$("#pre").hide();
		}else{
			$("#pre").show();
		}

		$('#next').html(html_next);
		var lm2 = limit -1;
		if(limit>1){
			var html_pre ="<a class='btn btn-default' onclick='pres("+lm2+");return false;'><i class='fa fa-step-backward'></i> Bài học trước</a>";
			$('#pre').html(html_pre);
		}
		$(".tab_result").hide();
		var url = link+"/learn/getlesson/"+lessonid;
		
		$.ajax({
		  dataType: "json",
		  type: "GET",
		  url: url,
		  data: "",
		  success: function(resp){
		  	reset_html();
		  	$('#start').html('');
		  	console.log(resp);
		  	var title = resp.title;
		  	var des_full = resp.des_full;
		  	var des_soft = resp.des_soft
		  	var type = resp.type;
		  	if(type ==2){
				var html_video = "<iframe width='100%' height='500' src='"+resp.video+"' frameborder='0' allowfullscreen></iframe>";
				$('#show_video').html(html_video);
				$('#show_video').addClass("videoWrapper");
			}

		  	var html = "<a class='btn btn-primary btn-lg btn_send btn_result' onclick='get_question("+limit+");return false;'><i class='fa fa-play-circle'></i> Bắt đầu</a>";
		  	var html_result = "<a class='btn btn-success btn_send btn_result' onclick='get_result("+limit+");return false;'> Xem kết quả</a>";
		  	var oncheck = $('#oncheck_'+limit).html();
		  	if(type==3){
		  		$('#show_video').removeClass("videoWrapper");
		  		$('#start').show();
		  		$('#start').html(html);
		  		$('#title2').html(title).removeClass('hidden');
		  		$('#des_full2').html(des_full).removeClass('hidden');
		  		$('#des_soft').html(des_soft).removeClass('hidden');
		  		if(oncheck == 1){
		  			get_result(limit);
		  		}
		  	}
		  	if(type == 1){
		  		$('#des_full').html(des_full).removeClass('hidden');
		  		$('#show_video').removeClass('hidden').removeClass("videoWrapper");
		  	}
		  	$('#title').html(title);
		  }
		});
	}

	function view_question(lessonid,limit){
		var lms = limit +1;
		var link = $('#link').val();
			url = link+"/learn/next_question/"+lessonid+"/"+limit;
			$.ajax({
			  dataType: "json",
			  type: "GET",
			  url: url,
			  data: "",
			  success: function(resp){
			  	console.log(resp);
			  	$('#result').html('').removeClass('hidden');
			  	$('#des_full').html('').removeClass('hidden');
			  	$('#showanswer').html('').removeClass('hidden');
			  	$('#start').html('');
			  	var cauhoi = "Câu "+lms+": "+resp[0].cauhoi;
			  	$('#title').html(cauhoi);
			  	var msg = resp[0].id;
			  	var suggestid = resp[0].suggestid;
			  	var image = resp[0].image;
			  	var lesson = resp[0].lesson;
			  	if(msg !="ok"){
			  		$('#showanswer').append(image);
			  		$('#questionid').val(resp[0].questionid);
				  	$.each(resp,function(i,obj){
				  		var html = "<p onclick='checked_row(this)'><input name='answerid' id='answerid' value='"+resp[i].id+"' type='radio'>  "+resp[i].title+"</input></p>";
				  		$('#showanswer').append(html);

				  	});
				  	var send_result = "<a class='btn btn-primary btn_send btn_start' onclick='send_question("+lessonid+","+limit+");return false;'><i class='fa fa-paper-plane'></i> Gửi đáp án</a>";
				  	if(suggestid!=0 && suggestid!='0'){
				  		send_result = send_result+" <b style='margin-left:10px'>Gợi ý: </b><a onclick='view_lesson_popup("+suggestid+",\""+lesson+"\")'><i class='fa fa-file-text-o'></i> "+lesson+"</a>";
				  	}
				  	$('#result').append(send_result);
				  	return false;
			  	}else{
			  		var pass = resp[0].pass;
			  		$('#title').html('');
			  		var html_check = "<i class='fa fa-check-circle'></i>";
			  		var lm = $('#lms').val();
					$('#checks_'+lm).html(html_check);
					$('#oncheck_'+lm).html(1);
			  		$('#title').html("Kết quả kiểm tra");
			  		get_result(limit,pass);
			  		$(".tab_result").show();
			  		$("#pre").hide();
			  		return false;
			  	}
			  }
			});
	}

	function get_question(lm){
		$("#next").hide();
  		$("#pre").hide();
  		$("#finsh").hide();
		reset_html();
		var link = $('#link').val();
		var oncheck = $('#oncheck_'+lm).html();
		if(oncheck ==1){
			var result = confirm('Bạn đã hoàn thành kiểm tra này rồi.Bạn có muốn thi lại không');
			if(result){
				$(".tab_result").hide();
				$.post(link+"/learn/reset_lesson/", $("form").serialize(),function(resp){

				});
				var lessonid = $('#lessonid_'+lm).html();
		    	update_history(lm,0);
				var limit =0;
				view_question(lessonid,limit);
			}else{
				lean(lm);
			}
		}else{
			var lessonid = $('#lessonid_'+lm).html();
	    	update_history(lm,0);
			var limit =0;
			view_question(lessonid,limit);
		}
	}

	function send_question(lessonid,limit){
		$("body,html").animate({
            scrollTop: 0
        }, 500);
		$(this).addClass("saving");
		limit++;
		var link = $('#link').val();
		$.post(link+"/learn/send_question/", $("form").serialize(),function(resp){
			$.each(resp, function (i, obj) {
				var id = obj.id;
	            var msg = obj.msg;
	            if(id != "ok"){
	            	alert(msg);
	            	$('#' + id).focus();
	            	return false;
	            }else{
	            	view_question(lessonid,limit);
	            }

			});

		}, 'json');
		
	}


	function get_result(limit,pass){
		var link = $('#link').val();
		var lessonid = $('#lessonid').val();
		var url = link+"/learn/get_result/"+lessonid;
		$.ajax({
		  	dataType: "json",
		  	type: "GET",
		  	url: url,
		  	data: "",
		  	success: function(resp){
			  	$('.row_result').html('');
			  	$('.table_kq').show();
			  	$('.table_point').show();
			  	var count=0;
			  	var sum_true = 0;
			  	var sum_false = 0;
			  	var total = 0;
			  	var abs=0;
			  	var t_true =0;
			  	var t_false =0;
			  	var t_abs =0;
			  	$.each(resp,function(i,obj){
			  		var trues = '';
			  		if(resp[i].trues ==1){
			  			trues ='<span class="text-success"><i class="fa fa-check"></i> Đúng</span>';
			  			sum_true++;
			  		}else{
			  			trues ='<span class="text-danger"><i class="fa fa-times"></i> Sai</span>';
			  			sum_false++;
			  		}
			  		count++;
			  		var html_res = "<tr class='row_result'><td>"+count+"</td><td>"+trues+"</td><td><span class='text-success'><i class='fa fa-check'></i> "+resp[i].total_true+"</span> <span class='text-danger' style='margin-left:10px'><i class='fa fa-times'></i> "+resp[i].total_false+"</span></td></tr>";
			  		$('.table_kq').append(html_res);
			  		t_true +=parseInt(resp[i].total_true);
			  		t_false +=parseInt(resp[i].total_false);
			  	});
			  	t_abs = t_true + t_false;
			  	abs = t_true>0 && t_abs>0 ? Math.round(t_true/t_abs*100) : 0;

			  	total = sum_true + sum_false;
			  	var point =0;
			  	point = Math.round(sum_true/total*100);
			  	var html_sum_true = "<div class='text-success'><i class='fa fa-check'></i> Đúng: "+sum_true+"</div>";
		  		var html_sum_false = "<div class='text-danger'><i class='fa fa-times'></i> Sai: "+sum_false+"</div>";
		  		$('.trues').html(html_sum_true);
		  		$('.falses').html(html_sum_false);
		  		$('.v_point').html(point);
		  		$('.abs').html(abs);
		  		if(pass=='notpass'){
		  			limit = limit-2;
		  			$(".result_test").remove();
		  			$(".tab_result").append("<div class='alert alert-danger text-center result_test' style='clear:both'><p>Kết quả bài kiểm tra của bạn không đủ để hoàn thành khóa học này .</p> <a class='btn btn-danger' style='margin-top: 15px;' onclick='lean("+limit+")'>Kiểm tra lại</a></div>");
		  		}
		  		if(pass=='passed_one' || pass=='passed'){
		  			$(".result_test").remove();
		  			$(".tab_result").append("<div class='alert alert-success result_test' style='clear:both'><p>Chúc mừng bạn <b><?php echo $this->user->FirstName.' '.$this->user->LastName ?></b> đã hoàn thành bài kiểm tra với kết quả cực kỳ xuất sắc !</p></div>");
		  			var html_next = "<a class='btn btn-danger' href='"+link+"/learn/get_certificate/<?php echo $scienceid ?>'><i class='fa fa-graduation-cap'></i> Xem chứng chỉ</a>";
		  			$("#next").html(html_next).show();
		  		}
		  	}
		});
	}

	function checked_row(ob){
		$(ob).find('input').prop("checked", true);
	}

	<?php 
	if($certificate_status==0){
		echo '$(".blockstudy_1_3 #finsh").html("<a class=\'btn btn-default view_certificate\' href=\''.$url.'/learn/get_certificate/'.$scienceid.'\'><i class=\'fa fa-graduation-cap\'></i> Xem chứng chỉ</a>");';
	}
	?>
</script>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>