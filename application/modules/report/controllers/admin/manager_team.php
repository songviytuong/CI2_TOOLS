<?php 
class Manager_team extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="manager_team";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_team a,ttp_report_targets_department b where a.Department=b.ID")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Title as DepartmentTitle,c.UserName from ttp_report_team a,ttp_report_targets_department b,ttp_user c where a.UserID=c.ID and a.Department=b.ID order by a.ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_team/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_team/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Team | Manager Report Tools');
		$this->template->write_view('content','admin/manager_team_home',$data);
		$this->template->render();
	}

    public function search($link='search'){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $CategoriesID = $this->session->userdata("report_filter_Department");
        $str_nav = "select count(1) as nav from ttp_report_team a,ttp_report_targets_department b,ttp_user c where a.UserID=c.ID and a.Department=b.ID";
        $str = "select a.*,b.Title as DepartmentTitle,c.UserName from ttp_report_team a,ttp_report_targets_department b,ttp_user c where a.UserID=c.ID and a.Department=b.ID";
        if($CategoriesID>0){
            $str.=" and a.Department=$CategoriesID";
            $str_nav.=" and a.Department=$CategoriesID";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data=array(
            'data'  => $this->db->query($str." order by ID DESC $limit_str")->result(),
            'nav'   => $this->lib->nav(base_url().ADMINPATH.'/report/manager_team/'.$link,5,$nav,$this->limit),
            'start' => $start,
            'find'      =>  $nav,
            'base_link' =>  base_url().ADMINPATH.'/report/manager_team/',
        );
        $this->template->write_view('content','admin/manager_team_home',$data);
        $this->template->render();
    }

    public function setsessionsearch(){
        if(isset($_POST['Department'])){
            $CategoriesID = mysql_real_escape_string($_POST['Department']);
            $this->session->set_userdata("report_filter_Department",$CategoriesID);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_Department");
        $this->search('setsessionsearch');
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Team add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_team/'
        );
        $this->template->write_view('content','admin/manager_team_add',$data);
        $this->template->render();
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Published = isset($_POST['Published']) ? $_POST['Published'] : 0 ;
        $Department = isset($_POST['Department']) ? $_POST['Department'] : 0 ;
        $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0 ;
        $userlist = isset($_POST['userlist']) ? $_POST['userlist'] : array() ;
        if($Title!=''){
            $data = array(
                'Title'     => $Title,
                'Published' => $Published,
                'Department'=> $Department,
                'UserID'    => $UserID,
                'Data'      => json_encode($userlist),
                'Created'   =>date('Y-m-d H:i:s',time())
            );
            $this->db->insert("ttp_report_team",$data);
        }
        redirect(ADMINPATH.'/report/manager_team/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_team where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_team where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Team | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_team/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_team_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        $Published = isset($_POST['Published']) ? $_POST['Published'] : 0 ;
        $Department = isset($_POST['Department']) ? $_POST['Department'] : 0 ;
        $UserID = isset($_POST['UserID']) ? $_POST['UserID'] : 0 ;
        $userlist = isset($_POST['userlist']) ? $_POST['userlist'] : array() ;
        if($Title!=''){
            $data = array(
                'Title'     => $Title,
                'Published' => $Published,
                'Department'=> $Department,
                'UserID'    => $UserID,
                'Data'      => json_encode($userlist)
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_team",$data);
        
        }
        redirect(ADMINPATH.'/report/manager_team/');
    }
}
?>
