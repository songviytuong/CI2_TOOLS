<style>.row{margin:0px !important;}</style>
<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type='hidden' name="ID" value="<?php echo $data->ID ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>THÔNG TIN KHÁCH HÀNG</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i> Save</button>
				</div>
			</div>
			<div class="row">
				<table class="customer_table">
					<tr>
						<td>Nhóm khách hàng</td>
						<td>
							<select name="GroupID">
								<?php 
								$group = $this->db->query("select * from ttp_report_customer_group")->result();
								if(count($group)>0){
									foreach ($group as $row) {
										$selected = $row->ID==$data->GroupID ? 'selected="selected"' : '' ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Mã khách hàng</td>
						<td><input type='text' name='Code' value='<?php echo $data->Code ?>' readonly="true" /></td>
						<td style="text-align:right;padding-right:10px">Tên</td>
						<td colspan="6"><input type='text' name='Name' value='<?php echo $data->Name ?>' /></td>
					</tr>
					<tr>
						<td>Địa chỉ đăng ký</td>
						<td colspan="8">
							<input type='text' name='Address' value='<?php echo $data->Address ?>' />
						</td>
					</tr>
					<tr>
						<td>NTNS</td>
						<td><input type='text' name='NTNS' value='<?php echo $data->Birthday ?>' /></td>
						<td style="text-align:right;padding-right:10px">Tuổi</td>
						<td><input type='text' name='Age' value='<?php echo $data->Age ?>' /></td>
						<td style="text-align:right;padding-right:10px">Giới tính</td>
						<td>
							<select name="Sex">
								<option value='0'>Nam</option>
								<option value='1'>Nữ</option>
								<option value='2'>Khác</option>
							</select>
						</td>
						<td colspan="3"></td>
					</tr>
					<tr>
						<td>Khu vực</td>
						<td>
							<select name="AreaID" id="Khuvuc">
								<option value='0'>-- Chọn khu vực --</option>
								<?php 
								$area = $this->db->query("select * from ttp_report_area")->result();
								if(count($area)>0){
									foreach($area as $row){
										$selected = $row->ID==$data->AreaID ? "selected='selected'" : "" ;
										echo "<option value='$row->ID' $selected>$row->Title</option>";
									}
								}
								?>
							</select>
						</td>
						<td style="text-align:right;padding-right:10px">Tỉnh thành</td>
						<td>
							<select name="CityID" id="Tinhthanh">
								<?php 
								$city = $this->db->query("select ID,Title from ttp_report_city where ID=$data->CityID")->row();
								echo $city ? "<option value='$data->CityID'>$city->Title</option>" : "<option value='0'>-- Chọn tỉnh thành --</option>" ;
								?>
							</select>
						</td>
						<td style="text-align:right;padding-right:10px">Quận huyện</td>
						<td>
							<select name="DistrictID" id="Quanhuyen">
								<?php 
								$district = $this->db->query("select ID,Title from ttp_report_district where ID=$data->DistrictID")->row();
								echo $city ? "<option value='$data->DistrictID'>$district->Title</option>" : "<option value='0'>-- Chọn quận huyện --</option>" ;
								?>
							</select>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>Nghề nghiệp</td>
						<td colspan="8">
							<select name="Job">
								<option value="1" <?php echo $data->Job==1 ? "selected='selected'" : '' ; ?>>Văn phòng</option>
								<option value="2" <?php echo $data->Job==2 ? "selected='selected'" : '' ; ?>>Kinh doanh-buôn bán</option>
								<option value="3" <?php echo $data->Job==3 ? "selected='selected'" : '' ; ?>>Nội trợ</option>
								<option value="4" <?php echo $data->Job==4 ? "selected='selected'" : '' ; ?>>Về hưu</option>
								<option value="5" <?php echo $data->Job==5 ? "selected='selected'" : '' ; ?>>Sinh viên/học sinh</option>
								<option value="6" <?php echo $data->Job==6 ? "selected='selected'" : '' ; ?>>Lao động phổ thông</option>
								<option value="0" <?php echo $data->Job==0 ? "selected='selected'" : '' ; ?>>Khác</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Ghi chú</td>
						<td colspan="8"><input type='text' name="Note" value='<?php echo $data->Note ?>' /></td>
					</tr>
					<tr>
						<td>Số di động 1</td>
						<td><input type='text' name="Phone1" readonly="true" value='<?php echo $data->Phone1 ?>' /></td>
						<td style="text-align:right;padding-right:10px">Số di động 2</td>
						<td><input type='text' name="Phone2" value='<?php echo $data->Phone2 ?>' /></td>
						<td style="text-align:right;padding-right:10px">Email</td>
						<td><input type='text' name="Email" value='<?php echo $data->Email ?>' /></td>
					</tr>
				</table>
				<hr>
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#home">Đơn hàng đã mua</a></li>
					<li><a data-toggle="tab" href="#menu1">Chi tiết SP đã mua</a></li>
					<li><a data-toggle="tab" href="#menu2">Lịch sử tư vấn</a></li>
				</ul>

				<div class="tab-content showbox_history">
				  	<div id="home" class="tab-pane fade in active">
				    	<?php 
				    	$type = $this->lib->get_config_define('status','order');
				    	$order = $this->db->query("select a.ID,a.MaDH,a.Ngaydathang,a.Total,a.Chietkhau,a.Chiphi,b.UserName,a.Status from ttp_report_order a,ttp_user b where a.UserID=b.ID and a.CustomerID=$data->ID order by a.Ngaydathang DESC")->result();
				    	if(count($order)>0){
				    		echo "<table><tr><th style='width:80px'>STT</th><th>Ngày mua</th><th>Mã ĐH</th><th>Giá trị ĐH</th><th>Nhân viên bán hàng</th><th>Trạng thái</th>
				    			</tr>";
				    		$i=1;
				    		foreach($order as $row){
				    			echo "<tr>";
				    			echo "<td>$i</td>";
				    			echo "<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>";
				    			echo "<td>$row->MaDH</td>";
				    			echo "<td>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</td>";
				    			echo "<td>$row->UserName</td>";
				    			echo "<td>".$type[$row->Status]."</td>";
				    			echo "</tr>";
				    			$i++;
				    		}
				    		echo "</table>";
				    	}
				    	?>
				  	</div>
				  	<div id="menu1" class="tab-pane fade">
				    	<?php 
				    	$details = $this->db->query("select a.MaSP,a.Title,b.Amount,c.MaDH,c.Ngaydathang,a.Donvi,b.Price from ttp_report_products a,ttp_report_orderdetails b,ttp_report_order c where a.ID=b.ProductsID and b.OrderID=c.ID and c.CustomerID=$data->ID order by c.Ngaydathang DESC")->result();
				    	if(count($details)>0){
				    		echo "<table><tr><th style='width:80px'>STT</th><th>Ngày mua</th><th>Mã ĐH</th><th>Tên sản phẩm</th><th>Số lượng</th><th>Đơn vị</th><th>Giá bán</th>
				    			</tr>";
				    		$i=1;
				    		foreach($details as $row){
				    			echo "<tr>
				    				<td>$i</td>
									<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>
									<td>$row->MaDH</td>
									<td>$row->Title</td>
									<td>$row->Amount</td>
									<td>$row->Donvi</td>
									<td>".number_format($row->Price)."</td>
				    			</tr>";
				    			$i++;
				    		}
				    		echo "</table>";
				    	}
				    	?>
				  	</div>
				  	<div id="menu2" class="tab-pane fade">
				  		<div class="row view_history">
							<a onclick="viewhistoryad(<?php echo $data->ID ?>)">Xem lịch sử tư vấn <i class="fa fa-caret-down"></i></a>
						</div>
						<div class="showbox_history">
							<div class="box_historyadvisory"></div>
						</div>
				  	</div>
				</div>
				
				<div class="add_advisory">
					<h1>Nội dung tư vấn khách hàng</h1>
					<p>Vấn đề của khách hàng trước khi sử dụng</p>
					<textarea name="BeforeAd"></textarea>
					<p>Giải pháp của nhân viên tư vấn</p>
					<textarea name="Solution"></textarea>
					<p>Hiệu quả sau khi sử dụng</p>
					<textarea name="AfterAd" style="margin-bottom:10px"></textarea>
					<p style='margin-top: 10px;'><input type='checkbox' name='Testimonial' style='margin-right:5px' <?php echo $data->Testimonial==1 ? "checked='checked'" : "" ; ?> /> Đồng ý tham gia Testimonial case</p>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 780px !important;}
	.body_content .customer_table{width:100%;border-collapse: collapse;background:none !important;margin-top:10px}
	.body_content .customer_table tr td{padding:7px 0px;}
	.body_content .customer_table tr td:first-child{width:120px;}
	.body_content .customer_table tr td:nth-child(2){width:200px;}
	.body_content .customer_table tr td:nth-child(3){width:100px;}
	.body_content .customer_table tr td input[type="text"]{width:100%; border:1px solid #c1c1c1;padding:4px 5px;height:32px;}
	.body_content .customer_table tr td select{height:30px;width:100%; border:1px solid #c1c1c1;padding:4px 5px;}
	.body_content .showbox_history table{width:100%;border-collapse: collapse;border:1px solid #E1e1e1;margin-top:10px;}
	.body_content .showbox_history table tr th{padding:5px;text-align:left;border-bottom:1px solid #E1e1e1;}
	.body_content .showbox_history table tr td{padding: 5px;text-align: left;border-bottom: 1px solid #E1e1e1;white-space: nowrap;max-width: 300px;overflow: hidden;text-overflow: ellipsis;}
	.view_history{margin-top:10px;}
	.view_history a{color:#1A82C3;text-decoration: underline;border:none !important;padding:0px !important;margin-right:20px;}
	.view_history a i{font-size: 12px;margin-left:6px;}
	.add_advisory{clear:both;margin-top:10px;}
	.add_advisory h1{font-size: 22px;padding: 10px 0px;text-transform: uppercase;font-weight: bold;margin-bottom: 10px}
	.add_advisory textarea{width:100%;border:1px solid #c1c1c1;height:80px;padding:5px;margin-top:5px;}
	.daterangepicker{width: auto;}
</style>
<script>
	statusloadhisorder=0;
	statusloadhisad=0;
	function viewhistoryorder(ID){
		if(ID!='' && ID>0){
			if(statusloadhisorder==0){
				$.ajax({
	            	url: "<?php echo $base_link.'get_history' ?>",
		            dataType: "html",
		            type: "POST",
		            context: this,
		            data: "ID="+ID,
		            success: function(result){
		            	statusloadhisorder=1;
		                if(result!='false'){
			                $(".box_historyorder").html(result);
		                }else{
		                	$(".box_historyorder").html("<p>Không có lịch sử giao dịch của khách hàng này.</p>");
		                }
		            }
		        });
	        }else{
	        	$(".box_historyorder").toggle();
	        }
	    }
	}


	function viewhistoryad(ID){
		if(ID!='' && ID>0){
			if(statusloadhisad==0){
				$.ajax({
	            	url: "<?php echo $base_link.'get_history_advisory' ?>",
		            dataType: "html",
		            type: "POST",
		            context: this,
		            data: "ID="+ID,
		            success: function(result){
		            	statusloadhisad=1;
		                if(result!='false'){
			                $(".box_historyadvisory").html(result);
		                }else{
		                	$(".box_historyadvisory").html("<p>Không có lịch sử tư vấn của khách hàng này.</p>");
		                }
		            }
		        });
	        }else{
	        	$(".box_historyadvisory").toggle();
	        }
	    }
	}

	$("#Khuvuc").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_city_by_area' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	$("#Tinhthanh").html(result);
	            }
	        });
		}
	});

	$("#Tinhthanh").change(function(){
		var ID= $(this).val();
		if(ID!=''){
			$.ajax({
            	url: "<?php echo base_url().ADMINPATH.'/report/import_order/get_district_by_city' ?>",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "ID="+ID,
	            success: function(result){
	            	var jsonobj = JSON.parse(result);
	            	$("#Quanhuyen").html(jsonobj.DistrictHtml);
	            }
	        });
		}
	});

</script>