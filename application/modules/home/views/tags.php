<div class="content blog-category-page search">
      <div class="container">
        <div class="row">
          <div class="main-content col-xs-12 col-md-8">
            <div class="cate-box content-box row search-content-box">
              <h3 class="well"><i class="fa fa-search"></i><?php echo $lang=="en" ? "Search with " : "Tìm kiếm với " ; ?> tags: <strong>"<?php echo $categories->Title ?>"</strong>. <span> <?php echo $lang=="en" ? "Find" : "Tìm thấy" ; ?> <?php echo number_format($result) ?> <?php echo $lang=="en" ? "result" : "kết quả" ; ?>.</span></h3>
              <div class="row">
                <?php 
                if(count($data)>0){
                  foreach($data as $row){ 
                    $image = $this->lib->getfilesize($row->Thumb,380,212);
                    echo '<div class="post-item col-xs-6 col-sm-12"><a href="'.$row->Alias.'" class="post-img col-sm-4"><img src="'.$image.'" alt="'.$row->Title.'" class="img-responsive"></a>
                      <div class="meta col-sm-8">
                        <h4><a href="'.$row->Alias.'" class="text">'.$row->Title.'</a></h4>
                        <p class="post-date">'.$row->Created.'</p>
                        <p class="hidden-xs">'.$this->lib->splitString($row->Description,180).'</p>
                      </div>
                    </div>';
                  }
                }
                ?>
              </div>
              <?php echo $result>count($data) ? $nav : ""; ?>
            </div>
          </div>
          <div class="sidebar col-xs-12 col-md-4">
            <div class="search-form sidebar-box">
              <div class="form-group no-gutters">
                <form method="post" action="<?php echo $lang=="en" ? base_url()."en/" : base_url(); ?>search">
                    <label for="" class="control-label"><?php echo $lang=="en" ? SEARCH_EN : SEARCH_VI ; ?>:                </label>
                    <input type="text" placeholder="<?php echo $lang=="en" ? SEARCH_EN : SEARCH_VI ; ?>" class="search-input form-control" name="keywords" required>
                    <input type="hidden" name="lang" value="<?php echo $lang ?>" />
                    <input type="submit" value="" class="search-button fa">
                </form>
              </div>
              <!-- End search-form-->
            </div>
            <?php 
            if($lang=="en"){
                $hot = $this->db->query("select a.Title_en as Title,a.Alias_en as Alias,a.Thumb,a.Data,a.ID from ttp_post a where a.Published=1 and a.Data like '%\"hot\":\"true\"%' order by a.LastEdited DESC")->row();
            }else{
                $hot = $this->db->query("select a.Title,a.Alias,a.Thumb,a.Data,a.Alias,a.ID from ttp_post a where a.Published=1 and a.Data like '%\"hot\":\"true\"%' order by a.LastEdited DESC")->row();
            }
            if($hot){
                $json = json_decode($hot->Data);
                $link = isset($json->changelink) ? $json->changelink : $hot->Alias ;
                $link = $lang=="en" ? "en/".$link : $link ;
                $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'"></iframe></div>' : "" ;
                if($video==''){
                    $video = file_exists($row->Thumb) ? '<img src="'.$row->Thumb.'" alt="'.$row->Title.'" class="img-responsive">' : "" ;
                }
                $tieudiem = $lang=="en" ? FOCAL_EN : FOCAL_VI ;
                echo '<div class="sidebar-box">
                      <h3 class="sidebar-title">'.$tieudiem.'</h3>
                      '.$video.'
                      <h4><a href="'.$link.'">'.$hot->Title.'</a></h4>
                    </div>';
            }
            ?>
            <div class="sidebar-box">
              <h3 class="sidebar-title"><?php echo $lang=="en" ? FEATURE_EN : FEATURE_VI ; ?></h3>
              <div class="row">
                <?php 
                if($lang=="en"){
                    $featured = $this->db->query("select Title_en as Title,Thumb,Description_en as Description,ID,Alias_en as Alias,Data from ttp_post where Thumb!='' and Published=1 and Data like '%\"featured\":\"true\"%' order by ID DESC limit 0,5")->result();
                    if(count($featured)==0){
                        $featured = $this->db->query("select Title_en as Title,Thumb,Description_en as Description,ID,Alias_en as Alias,Data from ttp_post where Thumb!='' and Published=1 order by View DESC limit 0,5")->result();
                    }
                }else{
                    $featured = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and Data like '%\"featured\":\"true\"%' order by ID DESC limit 0,5")->result();
                    if(count($featured)==0){
                        $featured = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 order by View DESC limit 0,5")->result();
                    }
                }
                if(count($featured)>0){
                    foreach ($featured as $row) {
                        $json = json_decode($row->Data);
                        $link = isset($json->changelink) ? $json->changelink : $row->Alias ;
                        $link = $lang=="en" ? "en/".$link : $link ;
                        $image = $this->lib->getfilesize($row->Thumb,113,63);
                        echo '<div class="item col-xs-6 col-md-12 post-item"><a href="'.$link.'" class="post-img"><img alt="'.$row->Title.'" src="'.$image.'" class="img-responsive"></a>
                              <h5><a href="'.$link.'">'.$row->Title.'</a></h5>
                            </div>';
                    }
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>