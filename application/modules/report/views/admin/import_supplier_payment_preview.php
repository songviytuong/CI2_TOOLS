<?php 
	if(count($data)>0){
			$action = $ob==0 || $ob==2 ? "active_import" : "active_import_ucc" ;
			echo "<div class='alert alert-success text-center'>Nội dung chi tiết file <b>\"$filename\"</b> bạn vừa tải lên hệ thống . Vui lòng kiểm tra lại thật kỹ trước khi nhấn lệnh nhập dữ liệu . <br>
				<p class='text-center' style='margin-top: 10px;'><a class='btn btn-success' onclick='$action(this)'><i class='fa fa-upload'></i> Nhập vào hệ thống</a></p>
			</div>";
			echo "<table>";
			$i=1;
			if($ob==0 || $ob==2){
				$arr_status = $this->lib->get_config_define("payment","ncc",0,"code");
			}else{
				$arr_status = $this->lib->get_config_define("payment","ucc",0,"code");
			}
			foreach($data as $row){
				$td = $i==1 ? "th" : "td" ;
				$first = $i==1 ? "" : "treach" ;
				echo "<tr class='$first'>";
				if(count($row)>0){
					for($k=0;$k<count($row);$k++){
						if($k==7 && $i!=1){
							if(!array_key_exists($row[$k],$arr_status)){
								echo "<$td class='td$k' style='width:auto'>No</$td>";
							}else{
								echo "<$td class='td$k' style='width:auto'>".$arr_status[$row[$k]]."</$td>";	
							}
						}else{
							echo "<$td class='td$k' style='width:auto'>".$row[$k]."</$td>";
						}
					}
					echo $i==1 ? "<th></th>" : "<$td style='width:30px'><a title='Xóa dữ liệu này ?' onclick='rmtr(this)'><i class='fa fa-times' aria-hidden='true'></i></a></$td>";
				}
				echo "</tr>";
				$i++;
			}
			echo "</table>";
		}
?>