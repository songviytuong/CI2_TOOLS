<?php

class Recruitment_model extends CI_Model {

    var $tablename = prefix . 'recruitment';
    var $tablename_location = prefix . 'recruitment_location';
    var $tablename_timer = prefix . 'recruitment_timer';

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getRecruitment($params = array(), $orderby = 'Position', $sort = 'ASC') {
        $this->db->select("r.*, r.ID as ID, r.Name as Name,r.City as City, l.Name as Location, t.Name as Timer,d.Name as Department,e.Name as Experience,r.Number as Number");
        $this->db->from('auc_recruitment r');
        $this->db->join('auc_recruitment_location l', 'l.ID = r.Location');
        $this->db->join('auc_recruitment_timer t', 't.ID = r.Timer');
        $this->db->join('auc_recruitment_department d', 'd.ID = r.Department');
        $this->db->join('auc_recruitment_experience e', 'e.ID = r.Experience');
        $this->db->where('r.Published', 1);
        $this->db->order_by($orderby, $sort);
        $result = $this->db->get()->result();
        return $result;
    }

    function getCountSearch($params) {
        $this->db->from($this->tablename);
        if (!empty($params['keyword'])) {
            $this->db->like('Title', $params['keyword']);
            $this->db->or_like('Introtext', $params['keyword']);
        }
        $this->db->where('Published', 1);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }
    
    function getCount() {
        $this->db->from($this->tablename);
        $this->db->where('Published', 1);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    function defineRecruitment($params = array()) {
        $recruitment_data = $this->getRecruitment($params);
        $res = array();
        foreach ($recruitment_data as $key => $row) {
            $res[$key]['ID'] = isset($row->ID) ? $row->ID : '';
            $res[$key]['Name'] = isset($row->Name) ? $row->Name : '';
            $res[$key]['Description'] = isset($row->Description) ? $row->Description : '';
            $res[$key]['Location'] = isset($row->Location) ? $row->Location : '';
            $res[$key]['Timer'] = isset($row->Timer) ? $row->Timer : '';
            $res[$key]['City'] = isset($row->City) ? $row->City : '';
            $res[$key]['Department'] = isset($row->Department) ? $row->Department : '';
            $res[$key]['Experience'] = isset($row->Experience) ? $row->Experience : '';
            $res[$key]['Number'] = isset($row->Number) ? $row->Number : 1;
            $res[$key]['Gender'] = isset($row->Gender) ? $row->Gender : 'Nam';
            $res[$key]['End'] = isset($row->End) ? date_format(date_create($row->End), 'd/m/Y') : '';
        }
        return $res;
    }

}