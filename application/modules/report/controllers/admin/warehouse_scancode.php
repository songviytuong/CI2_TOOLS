<?php 
class Warehouse_scancode extends Admin_Controller { 
 	public $user;
 	public $classname="warehouse_scancode";

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public $limit = 40;
	
    public function getwarehouselist(){
        $warehouselist = $this->db->query("select WarehouseID from ttp_report_qrcode_warehouse where UserID=".$this->user->ID)->result();
        $arr_warehouse = array(0);
        if(count($warehouselist)>0){
            foreach($warehouselist as $row){
                $arr_warehouse[] = $row->WarehouseID;
            }
        }
        return "(".implode(',',$arr_warehouse).")";
    }

	public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
        $transportid = isset($_GET['transport']) ? (int)$_GET['transport'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " a.MaXK ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $warehouse_accept = $this->getwarehouselist();
        $bonus = $warehouseid==0 ? "" : " and c.KhoID=$warehouseid and c.KhoID in$warehouse_accept" ;
        $bonus = $transportid==0 ? $bonus : $bonus." and d.ID=$transportid" ;
        $orderby = $orderby!='' ? $orderby : "a.MaXK ASC" ;
        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in$warehouse_accept")->result();
        $transport = $this->db->query("select ID,Title from ttp_report_transport")->result();
		$result = $this->db->query("select c.ID,a.MaXK,b.Name,a.Amount,d.Title,e.MaKho from ttp_report_export_warehouse a,ttp_report_customer b,ttp_report_order c,ttp_report_transport d,ttp_report_warehouse e where c.KhoID=e.ID and c.TransportID=d.ID and a.OrderID=c.ID and c.CustomerID=b.ID and a.Scan=1 and IsScan=0 and a.Hinhthucxuatkho<2 and c.CustomerID!=9996 and (b.Name like '%$keywords%' or a.MaXK like '%$keywords%') $bonus order by $orderby $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_export_warehouse a,ttp_report_customer b,ttp_report_order c,ttp_report_transport d,ttp_report_warehouse e where c.KhoID=e.ID and c.TransportID=d.ID and a.OrderID=c.ID and c.CustomerID=b.ID and a.Scan=1 and IsScan=0 and a.Hinhthucxuatkho<2 and c.CustomerID!=9996 and (b.Name like '%$keywords%' or a.MaXK like '%$keywords%') $bonus")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'transport' => $transport,
            'keywords'  => $keywords,
            'base_link' => base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     => $start,
            'find'      => $nav,
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/sale',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
		$this->template->write_view('content','admin/warehouse_scancontent',$data);
		$this->template->render();
	}

    public function sale(){
        $this->index();
    }

    /*
    *   Export sale order
    */

    public function preview_order($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($id>0){
            $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,c.Company,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
            $data = array(
                'data'=>$result
            );
            $this->template->add_title('Factory | Report Tools');
            $this->template->write_view('content','admin/warehouse_scan_preview',$data);
            $this->template->render();
        }
    }

    public function scan_from_export_order($id=0){
        $arr_scan = array();
        $result = $this->db->query("select a.*,b.ProductsID,b.ShipmentID from ttp_report_factory_qrcode_trace a,ttp_report_factory_qrcode b where a.ActionType=2 and a.ActionID=$id and a.QRID=b.ID")->result();
        $import_code = 0;
        if(count($result)>0){
            foreach($result as $row){
                if(isset($arr_scan[$row->ProductsID][$row->ShipmentID])){
                    $arr_scan[$row->ProductsID][$row->ShipmentID]++;
                }else{
                    $arr_scan[$row->ProductsID][$row->ShipmentID] = 1;
                }
                $import_code++;
            }
        }

        $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->result();
        $sl = 0 ;
        $str = "<table><tr>
                    <th>STT</th>
                    <th>Mã SKU</th>
                    <th>Tên sản phẩm</th>
                    <th>Lô hàng</th>
                    <th>Số lượng cần quét</th>
                    <th>Số lượng đã quét</th>
                </tr>";
        $k=1;
        if(count($result)>0){
            foreach($result as $row){
                $sl = $sl+$row->Amount;
                $scan = isset($arr_scan[$row->ProductsID][$row->ShipmentID]) ? $arr_scan[$row->ProductsID][$row->ShipmentID] : 0 ;
                $str.="<tr>
                    <td>$k</td>
                    <td>$row->MaSP</td>
                    <td>$row->Title</td>
                    <td>$row->ShipmentCode</td>
                    <td>$row->Amount</td>
                    <td>$scan</td>
                </tr>";
                $k++;
            }
        }
        $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode from ttp_report_orderdetails_bundle a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->result();
        if(count($result)>0){
            foreach($result as $row){
                $sl = $sl+$row->Amount;
                $scan = isset($arr_scan[$row->ProductsID][$row->ShipmentID]) ? $arr_scan[$row->ProductsID][$row->ShipmentID] : 0 ;
                $str.="<tr>
                    <td>$k</td>
                    <td>$row->MaSP</td>
                    <td>$row->Title</td>
                    <td>$row->ShipmentCode</td>
                    <td>$row->Amount</td>
                    <td>$scan</td>
                </tr>";
                $k++;
            }
        }
        $str.="</table>";
        $remain = $sl-$import_code;
        if($sl>0){
            $pxk = $this->db->query("select MaXK from ttp_report_export_warehouse where OrderID=$id and Scan=1 and IsScan=0")->row();
            if($pxk){
                echo "
                    <div class='row'>
                    <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                    <div class='col-xs-8'>
                        <h1>QR Scaner</h1>
                        <p>Tổng cộng <b style='font-size:17px'>$import_code/$sl</b> sản phẩm đã được quét cho phiếu <b class='text-danger' style='font-size:17px'>$pxk->MaXK</b></p>
                        <input type='text' class='form-control' onchange='enterqrcode(this,$id)' remain='$remain' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                        <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>
                        <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                        <div class='alert alert-danger hidden'></div>
                    </div>
                    </div>
                    <div class='row'><div class='col-xs-12'>$str</div></div>";
                echo '<script>$("#autofocus").focus();</script>';
            }
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function check_export_order_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];            
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $order = $this->db->query("select a.ID,a.MaDH,b.MaKho,a.KhoID,c.Name,d.MaXK,d.ID as ExportID from ttp_report_order a,ttp_report_warehouse b,ttp_report_customer c,ttp_report_export_warehouse d where a.ID=d.OrderID and a.CustomerID=c.ID and a.KhoID=b.ID and a.ID=$ID")->row();
        if($order){
            $warehouse_accept = $this->getwarehouselist();
            $warehouse = " and a.WarehouseID=$order->KhoID and a.WarehouseID in$warehouse_accept" ;
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code xuất từ kho $order->MaKho bán cho khách hàng $order->Name theo phiếu xuất $order->MaXK(Xuất bán hàng)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select * from ttp_report_factory_qrcode a where a.Code='$code' and a.OrderID=0 $warehouse and a.Published=1")->row();
            if($result){
                $checkproducts = $this->db->query("select ID from ttp_report_orderdetails where ShipmentID=$result->ShipmentID and ProductsID=$result->ProductsID and OrderID=$ID")->row();
                if($checkproducts){
                    $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=2 and QRID=$result->ID")->row();
                    if(!$check){
                        $this->db->query("update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2,BarcodeID=0 where ID=$result->ID");
                        $this->db->query("update ttp_report_factory_barcode set Status=2 where ID=$result->BarcodeID");
                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,2)");
                        $data['error']  = false;
                        if($remain==1){
                            $data['ready']=$order->ID;
                            $this->readyclose($order->ExportID);
                        }
                    }else{
                        $data['error']="Mã QRCode này đã được quét rồi !";
                    }
                }else{
                    $checkproducts = $this->db->query("select ID from ttp_report_orderdetails_bundle where ShipmentID=$result->ShipmentID and ProductsID=$result->ProductsID and OrderID=$ID")->row();
                    if($checkproducts){
                        $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=2 and QRID=$result->ID")->row();
                        if(!$check){
                            $this->db->query("update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2,BarcodeID=0 where ID=$result->ID");
                            $this->db->query("update ttp_report_factory_barcode set Status=2 where ID=$result->BarcodeID");
                            $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,2)");
                            $data['error']  = false;
                            if($remain==1){
                                $data['ready']=$order->ID;
                                $this->readyclose($order->ExportID);
                            }
                        }else{
                            $data['error']="Mã QRCode này đã được quét rồi !";
                        }
                    }else{
                        $data['error']="Mã QRCode không khớp sản phẩm hoặc lô hàng!.";
                    }
                }
            }else{
                $result = $this->db->query("select a.*,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status in(1,2) and a.Published=1 and b.Published=1 and a.OrderID=0 $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        $arr_query = array();
                        $arr_update = array();
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=2 and QRID=$row->ID")->row();
                                if(!$check){
                                    $checkproducts = $this->db->query("select ID from ttp_report_orderdetails where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and OrderID=$ID")->row();
                                    if($checkproducts){
                                        $arr_update[] = "update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2 where ID=$row->ID";
                                        $arr_query[] = "('$Note','$Created',$row->ID,$UserID,$ID,2)";
                                        if($remain==count($result) && $temp==1){
                                            $data['ready']=$order->ID;
                                            $this->readyclose($order->ExportID);
                                            $temp++;
                                        }
                                    }else{
                                        $checkproducts = $this->db->query("select ID from ttp_report_orderdetails_bundle where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and OrderID=$ID")->row();
                                        if($checkproducts){
                                            $arr_update[] = "update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2 where ID=$row->ID";
                                            $arr_query[] = "('$Note','$Created',$row->ID,$UserID,$ID,2)";
                                            if($remain==count($result) && $temp==1){
                                                $data['ready']=$order->ID;
                                                $this->readyclose($order->ExportID);
                                                $temp++;
                                            }
                                        }else{
                                            $data['error']="Mã QRCode không khớp sản phẩm hoặc lô hàng!.";
                                        }
                                    }
                                }else{
                                    $data['error']="Mã QRCode này đã được quét rồi !";
                                }
                            }else{
                                $data['error']="Số lượng sản phẩm trong thùng không khớp với sức chứa của thùng ! Kiểm tra lại nhé .";
                            }
                        }
                        if(count($arr_query)>0 && count($result)==count($arr_query)){
                            $arr_query = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values".implode(",",$arr_query);
                            $this->db->query($arr_query);
                            if(count($arr_update)>0){
                                foreach($arr_update as $row){
                                    $this->db->query($row);
                                }
                            }
                        }
                    }else{
                        $data['error']="Số lượng sản phẩm trong thùng vượt quá số lượng cần quét ! Kiểm tra lại nhé .";
                    }
                }
            }
        }
        echo json_encode($data);
    }

    /*
    *   Export another
    */
    public function preview_export_bill($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($id>0){
            $result = $this->db->query("select a.*,b.FirstName,b.LastName,c.Name,c.Phone1,c.Phone2,c.Company,d.MaKho as KhoTitle from ttp_report_order a,ttp_user b,ttp_report_customer c,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.CustomerID=c.ID and a.UserID=b.ID")->row();
            $data = array(
                'data'=>$result
            );
            $this->template->add_title('Factory | Report Tools');
            $this->template->write_view('content','admin/warehouse_scan_preview_export',$data);
            $this->template->render();
        }
    }

    public function export_another(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
        $transportid = isset($_GET['transport']) ? (int)$_GET['transport'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " a.MaXK ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $warehouse_accept = $this->getwarehouselist();
        $bonus = $warehouseid==0 ? "" : " and c.KhoID=$warehouseid and c.KhoID in$warehouse_accept" ;
        $bonus = $transportid==0 ? $bonus : $bonus." and d.ID=$transportid" ;
        $orderby = $orderby!='' ? $orderby : "a.MaXK ASC" ;
        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in$warehouse_accept")->result();
        $result = $this->db->query("select c.ID,c.Note,a.MaXK,b.Name,a.Amount,e.MaKho from ttp_report_export_warehouse a,ttp_report_customer b,ttp_report_order c,ttp_report_warehouse e where c.KhoID=e.ID and a.OrderID=c.ID and c.CustomerID=b.ID and a.Scan=1 and a.IsScan=0 and a.Hinhthucxuatkho=2 and c.CustomerID=9996 and (a.MaXK like '%$keywords%') $bonus order by $orderby $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_export_warehouse a,ttp_report_customer b,ttp_report_order c,ttp_report_warehouse e where c.KhoID=e.ID and a.OrderID=c.ID and c.CustomerID=b.ID and a.Scan=1 and a.IsScan=0 and a.Hinhthucxuatkho=2 and c.CustomerID=9996 and (a.MaXK like '%$keywords%') $bonus")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'keywords'  => $keywords,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/export_another',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan_export_another',$data);
        $this->template->render();
    }

    public function check_export_another_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];            
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $order = $this->db->query("select a.ID,a.MaDH,a.Note,b.MaKho,a.KhoID,c.Name,d.MaXK,d.ID as ExportID from ttp_report_order a,ttp_report_warehouse b,ttp_report_customer c,ttp_report_export_warehouse d where a.ID=d.OrderID and a.CustomerID=c.ID and a.KhoID=b.ID and a.ID=$ID")->row();
        if($order){
            $warehouse_accept = $this->getwarehouselist();
            $warehouse = " and a.WarehouseID=$order->KhoID and a.WarehouseID in$warehouse_accept" ;
            $Created = date('Y-m-d H:i:s');
            $Note_object = json_decode($order->Note);
            $Phongban = isset($Note_object->Phongban) ? "từ phòng ban $Note_object->Phongban" : "" ;
            $Lydo = isset($Note_object->Note) ? "lý do $Note_object->Note" : "" ;
            $Note = "Quét QR code xuất từ kho $order->MaKho theo phiếu xuất $order->MaXK $Phongban $Lydo (Xuất cho tặng hủy)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select * from ttp_report_factory_qrcode a where a.Code='$code' and a.OrderID=0 $warehouse and a.Published=1")->row();
            if($result){
                $checkshipment = $this->db->query("select ID from ttp_report_orderdetails where ShipmentID=$result->ShipmentID and ProductsID=$result->ProductsID and OrderID=$ID")->row();
                if($checkshipment){
                    $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=2 and QRID=$result->ID")->row();
                    if(!$check){
                        $this->db->query("update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2 where ID=$result->ID");
                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,2)");
                        $this->db->query("update ttp_report_factory_barcode set Status=2 where ID=$result->BarcodeID");
                        $data['error']  = false;
                        if($remain==1){
                            $data['ready']=$order->ID;
                            $this->readyclose($order->ExportID);
                        }
                    }else{
                        $data['error']="Mã QRCode này đã được quét rồi !";
                    }
                }else{
                    $checkshipment = $this->db->query("select ID from ttp_report_orderdetails_bundle where ShipmentID=$result->ShipmentID and ProductsID=$result->ProductsID and OrderID=$ID")->row();
                    if($checkshipment){
                        $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=2 and QRID=$result->ID")->row();
                        if(!$check){
                            $this->db->query("update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2 where ID=$result->ID");
                            $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,2)");
                            $this->db->query("update ttp_report_factory_barcode set Status=2 where ID=$result->BarcodeID");
                            $data['error']  = false;
                            if($remain==1){
                                $data['ready']=$order->ID;
                                $this->readyclose($order->ExportID);
                            }
                        }else{
                            $data['error']="Mã QRCode này đã được quét rồi !";
                        }
                    }else{
                        $data['error']="Mã QRCode không khớp sản phẩm hoặc lô hàng!.";
                    }
                }
            }else{
                $result = $this->db->query("select a.*,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=1 and a.Published=1 and b.Published=1 and a.OrderID=0 $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=2 and QRID=$row->ID")->row();
                                if(!$check){
                                    $checkshipment = $this->db->query("select ID from ttp_report_orderdetails where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and OrderID=$ID")->row();
                                    if($checkshipment){
                                        $this->db->query("update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2 where ID=$row->ID");
                                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$ID,2)");
                                        if($remain==count($result) && $temp==1){
                                            $data['ready']=$order->ID;
                                            $this->readyclose($order->ExportID);
                                            $temp++;
                                        }
                                    }else{
                                        $checkshipment = $this->db->query("select ID from ttp_report_orderdetails_bundle where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and OrderID=$ID")->row();
                                        if($checkshipment){
                                            $this->db->query("update ttp_report_factory_qrcode set OrderID=$order->ID,Status=2 where ID=$row->ID");
                                            $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$ID,2)");
                                            if($remain==count($result) && $temp==1){
                                                $data['ready']=$order->ID;
                                                $this->readyclose($order->ExportID);
                                                $temp++;
                                            }
                                        }else{
                                            $data['error']="Mã QRCode không khớp sản phẩm hoặc lô hàng!.";
                                        }
                                    }
                                }else{
                                    $data['error']="Mã QRCode này đã được quét rồi !";
                                }
                            }else{
                                $data['error']="Số lượng sản phẩm trong thùng không khớp với sức chứa của thùng ! Kiểm tra lại nhé .";
                            }
                        }
                    }else{
                        $data['error']="Số lượng sản phẩm trong thùng vượt quá số lượng cần quét ! Kiểm tra lại nhé .";
                    }
                }
            }
        }
        echo json_encode($data);
    }

    /*
    *   Export transfer products
    */
    public function export_mobilization(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid_sender = isset($_GET['warehousesender']) ? (int)$_GET['warehousesender'] : 0 ;
        $warehouseid_reciver = isset($_GET['warehousereciver']) ? (int)$_GET['warehousereciver'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " a.Code ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $warehouse_accept = $this->getwarehouselist();
        $bonus = $warehouseid_sender==0 ? "" : " and d.WarehouseSender=$warehouseid_sender and d.WarehouseSender in$warehouse_accept" ;
        $bonus = $warehouseid_reciver==0 ? $bonus : $bonus." and d.WarehouseReciver=$warehouseid_reciver and d.WarehouseReciver in$warehouse_accept" ;

        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in$warehouse_accept")->result();
        $result = $this->db->query("select d.ID,a.Amount,a.Code,b.MaKho as WarehouseReciver,c.MaKho as WarehouseSender,a.Code from ttp_report_transferorder_mobilization a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder d where a.OrderID=d.ID and b.ID=d.WarehouseReciver and c.ID=d.WarehouseSender and a.Scan=1 and a.IsScan=0 and d.Status=2 and a.Code like'%$keywords%' and a.Skip=0 $bonus order by $orderby $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_transferorder_mobilization a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder d where a.OrderID=d.ID and b.ID=d.WarehouseReciver and c.ID=d.WarehouseSender and a.Scan=1 and a.IsScan=0 and d.Status=2 and a.Code like'%$keywords%' and a.Skip=0 $bonus order by $orderby")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'keywords'  => $keywords,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/export_mobilization',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan_export_mobilization',$data);
        $this->template->render();
    }

    public function preview_mobilization($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('Preview Mobilization | Warehouse Report Tools');
        $result = $this->db->query("select a.*,b.Makho as SenderTitle,b.Address as SenderAddress,c.Makho as ReciverTitle,c.Address as ReciverAddress from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c where a.WarehouseSender=b.ID and a.WarehouseReciver=c.ID and a.ID=$id")->row();
        if($result){
            $data = array(
                'data'=>$result,
                'base_link' =>  base_url().ADMINPATH.'/warehouse_scancode/export_mobilization'
            );
            $view = 'admin/warehouse_scan_preview_mobilization' ;
            $this->template->write_view('content',$view,$data);
            $this->template->render();
        }else{
            redirect(ADMINPATH."/report/warehouse_scancode/export_mobilization");
        }
    }

    public function scan_from_transfer_export($id=0){
        $arr_scan = array();
        $result = $this->db->query("select a.*,b.ProductsID,b.ShipmentID from ttp_report_factory_qrcode_trace a,ttp_report_factory_qrcode b where a.ActionType=3 and a.ActionID=$id and a.QRID=b.ID")->result();
        $import_code = 0;
        if(count($result)>0){
            foreach($result as $row){
                if(isset($arr_scan[$row->ProductsID][$row->ShipmentID])){
                    $arr_scan[$row->ProductsID][$row->ShipmentID]++;
                }else{
                    $arr_scan[$row->ProductsID][$row->ShipmentID] = 1;
                }
                $import_code++;
            }
        }
        $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode from ttp_report_transferorder_details a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->result();
        $amount = 0 ;
        $str = "<table><tr>
                    <th>STT</th>
                    <th>Mã SKU</th>
                    <th>Tên sản phẩm</th>
                    <th>Lô hàng</th>
                    <th>Số lượng cần quét</th>
                    <th>Số lượng đã quét</th>
                </tr>";
        $k=1;
        if(count($result)>0){
            foreach($result as $row){
                $amount = $amount+$row->Request;
                $scan = isset($arr_scan[$row->ProductsID][$row->ShipmentID]) ? $arr_scan[$row->ProductsID][$row->ShipmentID] : 0 ;
                $str.="<tr>
                    <td>$k</td>
                    <td>$row->MaSP</td>
                    <td>$row->Title</td>
                    <td>$row->ShipmentCode</td>
                    <td>$row->Request</td>
                    <td>$scan</td>
                </tr>";
                $k++;
            }
        }
        $str.="</table>";
        $remain = $amount-$import_code;
        if($remain>0){
            $ldd = $this->db->query("select * from ttp_report_transferorder_mobilization where OrderID=$id")->row();
            if($ldd){
                $full = $import_code==$amount ? "<div class='alert alert-success'><i class='fa fa-stack-overflow' style='margin-right:5px;font-size: 17px;'></i> Đã quét đủ số lượng sản phẩm , bạn có thể sử dụng thao tác xuất kho .</div>" : "<input type='text' class='form-control' onchange='enterqrcode(this,$id)' remain='$remain' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                        <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>" ;
                echo "
                    <div class='row'>
                    <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                    <div class='col-xs-8'>
                        <h1>QR Scaner</h1>
                        <p>Tổng cộng <b style='font-size:17px'>$import_code/$amount</b> sản phẩm đã được quét cho lệnh điều động số <b class='text-danger' style='font-size:17px'>$ldd->Code</b></p>
                        $full
                        <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                        <div class='alert alert-danger hidden'></div>
                    </div>
                    </div>
                    <div class='row'><div class='col-xs-12'>$str</div></div>";
                echo $import_code==$amount ? '<script>$("#exportbutton").removeClass("hidden")</script>' : '<script>$("#autofocus").focus();</script>';
            }else{
                echo "Không tìm thấy lệnh điều động cho nghiệp vụ chuyển kho này .";
            }
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function check_transfer_export_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];
        }
        $TransferID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $import = $this->db->query("select d.Code,d.Note,a.WarehouseSender,a.WarehouseReciver,b.MaKho,c.MaKho as KhoReciver,d.ID from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder_mobilization d where a.ID=d.OrderID and a.WarehouseReciver=c.ID and a.WarehouseSender=b.ID and a.ID=$TransferID and a.Status=2")->row();
        if($import){
            $warehouse_accept = $this->getwarehouselist();
            $warehouse = " and a.WarehouseID=$import->WarehouseSender and a.WarehouseID in$warehouse_accept";
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code xuất từ $import->MaKho đến $import->KhoReciver theo lệnh điều động $import->Code . Lý do  $import->Note (xuất lưu chuyển kho)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select * from ttp_report_factory_qrcode a where a.Code='$code' $warehouse and a.Published=1")->row();
            if($result){
                $checkshipment = $this->db->query("select ID from ttp_report_transferorder_details where ShipmentID=$result->ShipmentID and WarehouseID=$result->WarehouseID and ProductsID=$result->ProductsID and OrderID=$TransferID")->row();
                if($checkshipment){
                    $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=3 and QRID=$result->ID")->row();
                    if(!$check){
                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$TransferID,3)");
                        $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver,Status=2,BarcodeID=0 where ID=$result->ID");
                        $this->db->query("update ttp_report_factory_barcode set Status=2 where ID=$result->BarcodeID");
                        $data['error']  = false;
                        if($remain==1){
                            $data['ready']=$TransferID;
                            $this->mobilization_readyclose($import->ID);
                        }
                    }
                }else{
                    $data['error']="Mã QRCode không khớp sản phẩm hoặc lô hàng!.";
                }
            }else{
                $result = $this->db->query("select a.*,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.Published=1 and b.Published=1 and a.BarcodeID=b.ID and a.Status=1 and OrderID=0 $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        $arr_query = array();
                        $arr_update = array();
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=3 and QRID=$row->ID")->row();
                                if(!$check){
                                    $checkshipment = $this->db->query("select ID from ttp_report_transferorder_details where ShipmentID=$row->ShipmentID and WarehouseID=$row->WarehouseID and ProductsID=$row->ProductsID and OrderID=$TransferID")->row();
                                    if($checkshipment){
                                        $arr_query[] = "('$Note','$Created',$row->ID,$UserID,$TransferID,3)";
                                        $arr_update[] = "update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver,Status=2 where ID=$row->ID;";

                                        if($remain==count($result) && $temp==1){
                                            $data['ready']=$TransferID;
                                            $this->mobilization_readyclose($import->ID);
                                            $temp++;
                                        }
                                    }
                                }
                            }else{
                                $data['error'] = "Mã thùng không được chấp nhận (đã có hộp quét lẻ)!";
                            }
                        }
                        if(count($arr_query)>0 && count($result)==count($arr_query)){
                            $arr_query = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values".implode(",",$arr_query);
                            $this->db->query($arr_query);
                            if(count($arr_update)>0){
                                foreach($arr_update as $row){
                                    $this->db->query($row);
                                }
                            }
                        }
                    }else{
                        $data['error']="Số lượng sản phẩm trong thùng vượt quá số lượng cần quét ! Kiểm tra lại nhé .";
                    }
                }else{
                    $data['error'] = "Mã thùng không hợp lệ !";
                }
            }
        }
        echo json_encode($data);
    }

    /*
    *   Import transfer products
    */
    public function import_mobilization(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid_sender = isset($_GET['warehousesender']) ? (int)$_GET['warehousesender'] : 0 ;
        $warehouseid_reciver = isset($_GET['warehousereciver']) ? (int)$_GET['warehousereciver'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " a.Code ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $warehouse_accept = $this->getwarehouselist();
        $bonus = $warehouseid_sender==0 ? "" : " and d.WarehouseSender=$warehouseid_sender and d.WarehouseSender in$warehouse_accept" ;
        $bonus = $warehouseid_reciver==0 ? $bonus : $bonus." and d.WarehouseReciver=$warehouseid_reciver and d.WarehouseReciver in$warehouse_accept" ;

        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in$warehouse_accept")->result();
        $result = $this->db->query("select d.ID,a.Amount,a.Code,a.skip,b.MaKho as WarehouseReciver,c.MaKho as WarehouseSender,a.Code from ttp_report_transferorder_mobilization a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder d where a.OrderID=d.ID and b.ID=d.WarehouseReciver and c.ID=d.WarehouseSender and a.Scan=1 and (a.IsScan=1 or a.skip=1) and d.Status=3 $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_transferorder_mobilization a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder d where a.OrderID=d.ID and b.ID=d.WarehouseReciver and c.ID=d.WarehouseSender and a.Scan=1 and (a.IsScan=1 or a.Skip=1) and d.Status=3")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'keywords'  => $keywords,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/import_mobilization',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan_import_mobilization',$data);
        $this->template->render();
    }

    public function scan_from_transfer_import($id=0){
        $arr_scan = array();
        $result = $this->db->query("select a.*,b.ProductsID,b.ShipmentID from ttp_report_factory_qrcode_trace a,ttp_report_factory_qrcode b where a.ActionType=4 and a.ActionID=$id and a.QRID=b.ID")->result();
        $import_code = 0;
        if(count($result)>0){
            foreach($result as $row){
                if(isset($arr_scan[$row->ProductsID][$row->ShipmentID])){
                    $arr_scan[$row->ProductsID][$row->ShipmentID]++;
                }else{
                    $arr_scan[$row->ProductsID][$row->ShipmentID] = 1;
                }
                $import_code++;
            }
        }
        $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode from ttp_report_transferorder_details a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->result();
        $amount = 0 ;
        $str = "<table><tr>
                    <th>STT</th>
                    <th>Mã SKU</th>
                    <th>Tên sản phẩm</th>
                    <th>Lô hàng</th>
                    <th>Số lượng cần quét</th>
                    <th>Số lượng đã quét</th>
                </tr>";
        $k=1;
        if(count($result)>0){
            foreach($result as $row){
                $amount = $amount+$row->TotalExport;
                $scan = isset($arr_scan[$row->ProductsID][$row->ShipmentID]) ? $arr_scan[$row->ProductsID][$row->ShipmentID] : 0 ;
                $str.="<tr>
                    <td>$k</td>
                    <td>$row->MaSP</td>
                    <td>$row->Title</td>
                    <td>$row->ShipmentCode</td>
                    <td>$row->TotalExport</td>
                    <td>$scan</td>
                </tr>";
                $k++;
            }
        }
        $str.="</table>";
        $remain = $amount-$import_code;
        if($remain>0){
            $ldd = $this->db->query("select * from ttp_report_transferorder_mobilization where OrderID=$id")->row();
            if($ldd){
                $checkscan = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$id and ActionType=3")->row();
                $skip = $checkscan ? 0 : 1 ;
                $full = $import_code==$amount ? "<div class='alert alert-success'><i class='fa fa-stack-overflow' style='margin-right:5px;font-size: 17px;'></i> Đã quét đủ số lượng sản phẩm , bạn có thể sử dụng thao tác nhập kho .</div>" : "<input type='text' class='form-control' onchange='enterqrcode(this,$id,$skip)' remain='$remain' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                        <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>" ;
                echo "
                    <div class='row'>
                    <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                    <div class='col-xs-8'>
                        <h1>QR Scaner</h1>
                        <p>Tổng cộng <b style='font-size:17px'>$import_code/$amount</b> sản phẩm đã được quét cho lệnh điều động số <b class='text-danger' style='font-size:17px'>$ldd->Code</b></p>
                        $full
                        <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                        <div class='alert alert-danger hidden'></div>
                    </div>
                    </div>
                    <div class='row'><div class='col-xs-12'>$str</div></div>";
                echo $import_code==$amount ? '<script>$("#exportbutton").removeClass("hidden")</script>' : '<script>$("#autofocus").focus();</script>';
            }else{
                echo "Không tìm thấy lệnh điều động cho nghiệp vụ chuyển kho này .";
            }
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function check_transfer_import_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];
        }
        $TransferID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $import = $this->db->query("select d.Code,d.Note,a.WarehouseSender,a.WarehouseReciver,b.MaKho,c.MaKho as KhoReciver,d.ID from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder_mobilization d where a.ID=d.OrderID and a.WarehouseReciver=c.ID and a.WarehouseSender=b.ID and a.ID=$TransferID and a.Status=3 and d.IsScan=1")->row();
        if($import){
            $warehouse_accept = $this->getwarehouselist();
            $warehouse = " and a.WarehouseID=$import->WarehouseReciver and a.WarehouseID in$warehouse_accept";
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập từ $import->MaKho đến $import->KhoReciver theo lệnh điều động $import->Code . Lý do  $import->Note (nhập lưu chuyển kho)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select a.* from ttp_report_factory_qrcode a,ttp_report_factory_qrcode_trace b where a.Code='$code' and a.Published=1 and a.ID=b.QRID and b.ActionType=3 and b.ActionID=$TransferID $warehouse")->row();
            if($result){
                $checkshipment = $this->db->query("select ID from ttp_report_transferorder_details where ShipmentID=$result->ShipmentID and WarehouseID=$import->WarehouseSender and ProductsID=$result->ProductsID and OrderID=$TransferID")->row();
                if($checkshipment){
                    $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=4 and QRID=$result->ID")->row();
                    if(!$check){
                        $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver where ID=$result->ID");
                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$TransferID,4)");
                        $this->db->query("update ttp_report_factory_barcode set Status=2 where ID=$result->BarcodeID");
                        $data['error']  = false;
                        if($remain==1){
                            $data['ready']=$TransferID;
                            $this->mobilization_readyclose($import->ID,2);
                        }
                    }else{
                        $data['error']="Mã QRCode đã quét cho nghiệp vụ này rồi !.";
                    }
                }else{
                    $data['error']="Mã QRCode không khớp sản phẩm hoặc lô hàng !.";
                }
            }else{
                $result = $this->db->query("select a.*,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c,ttp_report_factory_qrcode_trace d where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status in(1,2) and a.Published=1 and b.Published=1 and a.OrderID=0 and a.ID=d.QRID and d.ActionType=3 and d.ActionID=$TransferID $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        $arr_query = array();
                        $arr_update = array();
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=4 and QRID=$row->ID")->row();
                                if(!$check){
                                     $checkshipment = $this->db->query("select ID from ttp_report_transferorder_details where ShipmentID=$row->ShipmentID and WarehouseID=$import->WarehouseSender and ProductsID=$row->ProductsID and OrderID=$TransferID")->row();
                                    if($checkshipment){
                                        $arr_update[] = "update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver where ID=$row->ID";
                                        $arr_query[] = "('$Note','$Created',$row->ID,$UserID,$TransferID,4)";
                                        if($remain==count($result) && $temp==1){
                                            $data['ready']=$TransferID;
                                            $this->mobilization_readyclose($import->ID,2);
                                            $temp++;
                                        }
                                    }else{
                                        $data['message'] = "select ID from ttp_report_transferorder_details where ShipmentID=$row->ShipmentID and WarehouseID=$import->WarehouseSender and ProductsID=$row->ProductsID and OrderID=$TransferID";
                                    }
                                }
                            }else{
                                $data['error'] = "Mã thùng không được chấp nhận (đã có hộp quét lẻ)!";
                            }
                        }
                        if(count($arr_query)>0 && count($result)==count($arr_query)){
                            $arr_query = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values".implode(",",$arr_query);
                            $this->db->query($arr_query);
                            if(count($arr_update)>0){
                                foreach($arr_update as $row){
                                    $this->db->query($row);
                                }
                            }
                        }
                    }else{
                        $data['error']="Số lượng sản phẩm trong thùng vượt quá số lượng cần quét ! Kiểm tra lại nhé .";
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function check_transfer_import_code_skip(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];
        }
        $TransferID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $import = $this->db->query("select d.Code,d.Note,a.WarehouseSender,a.WarehouseReciver,b.MaKho,c.MaKho as KhoReciver,d.ID from ttp_report_transferorder a,ttp_report_warehouse b,ttp_report_warehouse c,ttp_report_transferorder_mobilization d where a.ID=d.OrderID and a.WarehouseReciver=c.ID and a.WarehouseSender=b.ID and a.ID=$TransferID and a.Status=3 and d.Skip=1")->row();
        if($import){
            $warehouse_accept = $this->getwarehouselist();
            $warehouse = " and a.WarehouseID=$import->WarehouseSender and a.WarehouseID in$warehouse_accept";
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập từ $import->MaKho đến $import->KhoReciver theo lệnh điều động $import->Code . Lý do  $import->Note (nhập lưu chuyển kho)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select a.* from ttp_report_factory_qrcode a,ttp_report_factory_qrcode_trace b where a.Code='$code' and a.Published=1 and a.ID=b.QRID $warehouse")->row();
            if($result){
                $data['message'] = "aa";
                $checkproducts = $this->db->query("select ID from ttp_report_transferorder_details where ShipmentID=$result->ShipmentID and WarehouseID=$import->WarehouseSender and ProductsID=$result->ProductsID and OrderID=$TransferID")->row();
                if($checkproducts){
                    $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=4 and QRID=$result->ID")->row();
                    if(!$check){
                        $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver where ID=$result->ID");
                        $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$TransferID,4)");
                        $data['error']  = false;
                        if($remain==1){
                            $data['ready']=$TransferID;
                            $this->mobilization_readyclose($import->ID,2);
                        }
                    }
                }else{
                    $data['error']="Mã QRCode không khớp sản phẩm !.";
                }
            }else{
                $result = $this->db->query("select a.*,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c,ttp_report_factory_qrcode_trace d where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status in(1,2) and a.Published=1 and b.Published=1 and a.OrderID=0 and a.ID=d.QRID $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        $arr_query = array();
                        $arr_update = array();
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$TransferID and ActionType=4 and QRID=$row->ID")->row();
                                if(!$check){
                                     $checkproducts = $this->db->query("select ID from ttp_report_transferorder_details where ShipmentID=$row->ShipmentID and WarehouseID=$import->WarehouseSender and ProductsID=$row->ProductsID and OrderID=$TransferID")->row();
                                    if($checkproducts){
                                        $arr_update[] = "update ttp_report_factory_qrcode set WarehouseID=$import->WarehouseReciver where ID=$row->ID";
                                        $arr_query[] = "('$Note','$Created',$row->ID,$UserID,$TransferID,4)";
                                        if($remain==count($result) && $temp==1){
                                            $data['ready']=$TransferID;
                                            $this->mobilization_readyclose($import->ID,2);
                                            $temp++;
                                        }
                                    }else{
                                        $data['message'] = "select ID from ttp_report_transferorder_details where WarehouseID=$import->WarehouseSender and ProductsID=$row->ProductsID and OrderID=$TransferID";
                                    }
                                }
                            }
                        }
                        if(count($arr_query)>0 && count($result)==count($arr_query)){
                            $arr_query = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values".implode(",",$arr_query);
                            $this->db->query($arr_query);
                            if(count($arr_update)>0){
                                foreach($arr_update as $row){
                                    $this->db->query($row);
                                }
                            }
                        }
                    }else{
                        $data['error']="Số lượng sản phẩm trong thùng vượt quá số lượng cần quét ! Kiểm tra lại nhé .";
                    }
                }
            }
        }
        echo json_encode($data);
    }

    /*
    *   Import from production
    */
    public function preview_import_production($id=0){
        if($id>0){
            $result = $this->db->query("select a.*,b.FirstName,b.LastName,d.MaKho as KhoTitle,d.Address from ttp_report_inventory_import a,ttp_user b,ttp_report_warehouse d where a.ID=$id and a.KhoID=d.ID and a.UserID=b.ID")->row();
            if($result){
                $this->template->add_title('Import Bill | Inventory Tools');
                $data = array(
                    'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_import/',
                    'data'      => $result
                );
                $view = 'admin/warehouse_scan_preview_import_production' ;
                $this->template->write_view('content',$view,$data);
                $this->template->render();
            }
        }
    }

    public function import_production(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
        $productionid = isset($_GET['production']) ? (int)$_GET['production'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " a.MaNK ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $bonus = $warehouseid==0 ? "" : " and a.KhoID=$warehouseid" ;
        $bonus = $productionid==0 ? $bonus : $bonus." and d.ProductionID=$productionid" ;
        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
        $production = $this->db->query("select b.ID,b.Title from ttp_report_inventory_import a,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse c,ttp_report_production b where d.ProductionID=b.ID and a.ID=c.ImportID and a.KhoID=e.ID and a.POID=d.ID and c.Scan=1 and c.IsScan=0 and a.Type=0 and (d.POCode like '%$keywords%' or a.MaNK like '%$keywords%') $bonus")->result();
        $result = $this->db->query("select a.ID,c.Amount,d.POCode,e.MaKho,c.MaNK from ttp_report_inventory_import a,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse c where a.ID=c.ImportID and a.KhoID=e.ID and a.POID=d.ID and c.Scan=1 and c.IsScan=0 and a.Type=0 and (d.POCode like '%$keywords%' or a.MaNK like '%$keywords%') $bonus order by $orderby $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_inventory_import a,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse c where a.ID=c.ImportID and a.KhoID=e.ID and a.POID=d.ID and c.Scan=1 and c.IsScan=0 and a.Type=0 and (d.POCode like '%$keywords%' or a.MaNK like '%$keywords%') $bonus")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'production'=> $production,
            'keywords'  => $keywords,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/import_production',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan_import_production',$data);
        $this->template->render();
    }

    public function scan_from_import_production($id=0){
        $import_code = $this->db->query("select count(ID) as sl from ttp_report_factory_qrcode_trace where ActionType=1 and ActionID=$id")->row();
        $import_code = $import_code ? $import_code->sl : 0 ;
        $result = $this->db->query("select a.ImportID,sum(a.Amount) as sl from ttp_report_inventory_import_details a,ttp_report_products b where a.ImportID=$id and a.ProductsID=b.ID and b.CodeManage=1 group by a.ImportID")->row();
        $sl = $result ? $result->sl : 0 ;
        $remain = $sl-$import_code;
        if($sl>0){
            $pxk = $this->db->query("select MaNK from ttp_report_import_warehouse where ImportID=$id and Scan=1 and IsScan=0")->row();
            if($pxk){
                echo "
                    <div class='row'>
                    <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                    <div class='col-xs-8'>
                        <h1>QR Scaner</h1>
                        <p>Tổng cộng <b style='font-size:17px'>$import_code/$sl</b> sản phẩm đã được quét cho phiếu <b class='text-danger' style='font-size:17px'>$pxk->MaNK</b></p>
                        <input type='text' class='form-control' onchange='enterqrcode(this,$id)' remain='$remain' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                        <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>
                        <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                        <div class='alert alert-danger hidden'></div>
                    </div>
                    </div>";
                echo '<script>$("#autofocus").focus();</script>';
            }
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function check_import_production_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];            
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $order = $this->db->query("select a.ID,b.MaKho,a.KhoID,d.MaNK,d.ID as ImportID,c.POCode from ttp_report_inventory_import a,ttp_report_warehouse b,ttp_report_import_warehouse d,ttp_report_perchaseorder c where a.POID=c.ID and a.ID=d.ImportID and a.KhoID=b.ID and a.ID=$ID")->row();
        if($order){
            $warehouse = " and a.WarehouseID!=$order->KhoID and a.WarehouseID=0" ;
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập kho $order->MaKho theo phiếu nhập kho $order->MaNK từ PO số $order->POCode(Nhập mua hàng từ NCC)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select ID from ttp_report_factory_qrcode a where a.Code='$code' and a.OrderID=0 $warehouse and a.Published=1")->row();
            if($result){
                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=1 and QRID=$result->ID")->row();
                if(!$check){
                    $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$order->KhoID where ID=$result->ID");
                    $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,1)");
                    $data['error']  = false;
                    if($remain==1){
                        $data['ready']=$order->ID;
                        $this->import_readyclose($order->ImportID);
                    }
                }
            }else{
                $result = $this->db->query("select a.ID,a.BarcodeID,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=1 and a.Published=1 and b.Published=1 and a.OrderID=0 $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$order->KhoID where ID=$row->ID");
                                $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$ID,1)");
                                if($remain==count($result) && $temp==1){
                                    $data['ready']=$order->ID;
                                    $this->import_readyclose($order->ImportID);
                                    $temp++;
                                }
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    /*
    *   Import another
    */

    public function import_another(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " c.MaNK ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $warehouse_accept = $this->getwarehouselist();
        $bonus = $warehouseid==0 ? "" : " and a.KhoID=$warehouseid and a.KhoID in$warehouse_accept" ;
        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in$warehouse_accept")->result();
        $result = $this->db->query("select a.ID,c.Amount,d.POCode,e.MaKho,c.MaNK from ttp_report_inventory_import a,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse c where a.ID=c.ImportID and a.KhoID=e.ID and a.POID=d.ID and c.Scan=1 and c.IsScan=0 and a.Type=2 and (d.POCode like '%$keywords%' or c.MaNK like '%$keywords%') $bonus order by $orderby $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_inventory_import a,ttp_report_perchaseorder d,ttp_report_warehouse e,ttp_report_import_warehouse c where a.ID=c.ImportID and a.KhoID=e.ID and a.POID=d.ID and c.Scan=1 and c.IsScan=0 and a.Type=2 $bonus")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'keywords'  => $keywords,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/import_production',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan_import_another',$data);
        $this->template->render();
    }

    public function check_import_another_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $order = $this->db->query("select a.ID,a.Note,b.MaKho,a.KhoID,d.MaNK,d.ID as ImportID,c.POCode from ttp_report_inventory_import a,ttp_report_warehouse b,ttp_report_import_warehouse d,ttp_report_perchaseorder c where a.POID=c.ID and a.ID=d.ImportID and a.KhoID=b.ID and a.ID=$ID")->row();
        if($order){
            $warehouse = " and a.WarehouseID!=$order->KhoID and a.WarehouseID=0" ;
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập kho $order->MaKho theo phiếu nhập kho $order->MaNK từ PO số $order->POCode lý do $order->Note (Nhập khác)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select ID from ttp_report_factory_qrcode a where a.Code='$code' and a.OrderID=0 $warehouse and a.Published=1")->row();
            if($result){
                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=1 and QRID=$result->ID")->row();
                if(!$check){
                    $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$order->KhoID where ID=$result->ID");
                    $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,1)");
                    $data['error']  = false;
                    if($remain==1){
                        $data['ready']=$order->ID;
                        $this->import_readyclose($order->ImportID);
                    }
                }
            }else{
                $result = $this->db->query("select a.ID,a.BarcodeID,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=1 and a.Published=1 and b.Published=1 and a.OrderID=0 $warehouse")->result();
                if(count($result)>0){
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $this->db->query("update ttp_report_factory_qrcode set WarehouseID=$order->KhoID where ID=$row->ID");
                                $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$ID,1)");
                                if($remain==count($result) && $temp==1){
                                    $data['ready']=$order->ID;
                                    $this->import_readyclose($order->ImportID);
                                    $temp++;
                                }
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    /*
    *   Import reject order
    */

    public function import_rejectorder(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit = "limit $start,$this->limit";
        $keywords = isset($_GET['keywords']) ? mysql_real_escape_string($_GET['keywords']) : '' ;
        $warehouseid = isset($_GET['warehouse']) ? (int)$_GET['warehouse'] : 0 ;
        $orderbyname = isset($_GET['sortname']) ? $_GET['sortname'] : 'pxk' ;
        $orderby ="";
        if($orderbyname!=''){
            $orderby = isset($_GET['sortvalue']) ? (int)$_GET['sortvalue'] : 0 ;
            $orderby = $orderby==0 ? "ASC" : "DESC" ;
            switch ($orderbyname) {
                case 'pxk':
                    $orderby = " a.MaNK ".$orderby;
                    break;
                default:
                    $orderby='';
                    break;
            }
        }
        $warehouse_accept = $this->getwarehouselist();
        $bonus = $warehouseid==0 ? "" : " and a.KhoID=$warehouseid and a.KhoID in$warehouse_accept" ;
        $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse where ID in$warehouse_accept")->result();
        $result = $this->db->query("select d.ID,c.Amount,d.MaDH,e.MaKho,c.MaXK from ttp_report_inventory_import a,ttp_report_order d,ttp_report_warehouse e,ttp_report_export_warehouse c where a.ExportID=c.ID and a.KhoID=e.ID and c.OrderID=d.ID and c.Scan=1 and c.RejectIsScan=0 and a.Type=1 and d.Status=1 and (d.MaDH like '%$keywords%' or c.MaXK like '%$keywords%') $bonus order by $orderby $limit")->result();
        $nav = $this->db->query("select count(a.ID) as nav from ttp_report_inventory_import a,ttp_report_order d,ttp_report_warehouse e,ttp_report_export_warehouse c where a.ExportID=c.ID and a.KhoID=e.ID and c.OrderID=d.ID and c.Scan=1 and c.RejectIsScan=0 and a.Type=1 and d.Status=1 $bonus order by a.ID DESC")->row();
        $nav = $nav ? $nav->nav : 0 ;
        $data = array(
            'data'      => $result,
            'warehouse' => $warehouse,
            'keywords'  => $keywords,
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_scancode/',
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_scancode/import_production',5,$nav,$this->limit)
        );
        $this->template->add_title('Factory | Report Tools');
        $this->template->write_view('content','admin/warehouse_scan_import_reject',$data);
        $this->template->render();
    }

    public function scan_from_import_reject($id=0){
        $arr_scan = array();
        $result = $this->db->query("select a.*,b.ProductsID,b.ShipmentID from ttp_report_factory_qrcode_trace a,ttp_report_factory_qrcode b where a.ActionType=5 and a.ActionID=$id and a.QRID=b.ID")->result();
        $import_code = 0;
        if(count($result)>0){
            foreach($result as $row){
                if(isset($arr_scan[$row->ProductsID])){
                    $arr_scan[$row->ProductsID]++;
                }else{
                    $arr_scan[$row->ProductsID] = 1;
                }
                $import_code++;
            }
        }
        $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->result();
        $sl = 0 ;
        $str = "<table><tr>
                    <th>STT</th>
                    <th>Mã SKU</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng cần quét</th>
                    <th>Số lượng đã quét</th>
                </tr>";
        $k=1;
        $arr_products = array();
        if(count($result)>0){
            foreach($result as $row){
                if(!isset($arr_products[$row->ProductsID])){
                    $sl = $sl+$row->Amount;
                    $scan = isset($arr_scan[$row->ProductsID]) ? $arr_scan[$row->ProductsID] : 0 ;
                    $str.="<tr>
                        <td>$k</td>
                        <td>$row->MaSP</td>
                        <td>$row->Title</td>
                        <td>$row->Amount</td>
                        <td>$scan</td>
                    </tr>";
                    $k++;
                }
            }
        }
        $result = $this->db->query("select a.*,b.MaSP,b.Title,c.ShipmentCode from ttp_report_orderdetails_bundle a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.OrderID=$id and a.ProductsID=b.ID and b.CodeManage=1")->result();
        if(count($result)>0){
            foreach($result as $row){
                if(!isset($arr_products[$row->ProductsID])){
                    $sl = $sl+$row->Amount;
                    $scan = isset($arr_scan[$row->ProductsID]) ? $arr_scan[$row->ProductsID] : 0 ;
                    $str.="<tr>
                        <td>$k</td>
                        <td>$row->MaSP</td>
                        <td>$row->Title</td>
                        <td>$row->Amount</td>
                        <td>$scan</td>
                    </tr>";
                    $k++;
                }
            }
        }
        $str.="</table>";
        $remain = $sl-$import_code;
        if($sl>0){
            $pxk = $this->db->query("select MaXK from ttp_report_export_warehouse where OrderID=$id and Scan=1 and IsScan=1 and RejectIsScan=0")->row();
            if($pxk){
                echo "
                    <div class='row'>
                    <div class='col-xs-4'><img src='public/admin/images/qrcodescaner.jpg' style='width:100%' /></div>
                    <div class='col-xs-8'>
                        <h1>QR Scaner</h1>
                        <p>Tổng cộng <b style='font-size:17px'>$import_code/$sl</b> sản phẩm đã được quét cho phiếu <b class='text-danger' style='font-size:17px'>$pxk->MaXK</b></p>
                        <input type='text' class='form-control' onchange='enterqrcode(this,$id)' remain='$remain' placeholder='Nhập mã QR / Barcode vào đây ...' id='autofocus' />
                        <p style='margin-top:10px;color:#999'>(*) Vui lòng quét hoặc nhập mã QR Code / Barcode thùng hàng vào khung bên trên .</p>
                        <a class='btn hidden saving' id='checking'>Đang kiểm tra mã nhập vào của bạn ...</a>
                        <div class='alert alert-danger hidden'></div>
                    </div>
                    </div>
                    <div class='row'><div class='col-xs-12'>$str</div></div>";
                echo '<script>$("#autofocus").focus();</script>';
            }
        }else{
            echo "Không có sản phẩm cần quản lý bằng QRCODE / BARCODE .";
        }
    }

    public function check_import_reject_code(){
        $code = isset($_POST['data']) ? mysql_real_escape_string($_POST['data']) : '' ;
        $code = explode('=', $code);
        if(count($code)==2){
            $code = (int)$code[1];
        }else{
            $code = $code[0];            
        }
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $remain = isset($_POST['remain']) ? (int)$_POST['remain'] : 0 ;
        $data = array("error"=>true,"ready"=>false);
        $order = $this->db->query("select a.ID,a.MaDH,b.MaKho,a.KhoID,c.Name,d.MaXK,d.ID as ExportID from ttp_report_order a,ttp_report_warehouse b,ttp_report_customer c,ttp_report_export_warehouse d where a.ID=d.OrderID and a.CustomerID=c.ID and a.KhoID=b.ID and a.ID=$ID and a.Status=1")->row();
        if($order){
            $warehouse = " and a.WarehouseID=$order->KhoID" ;
            $Created = date('Y-m-d H:i:s');
            $Note = "Quét QR code nhập kho $order->MaKho hàng bị trả về từ khách hàng $order->Name theo phiếu xuất $order->MaXK (Nhập hàng trả về)";
            $UserID = $this->user->ID;
            $result = $this->db->query("select * from ttp_report_factory_qrcode a where a.Code='$code' and a.OrderID=$ID and a.Status=2 $warehouse and a.Published=1")->row();
            if($result){
                $check = $this->db->query("select ID from ttp_report_factory_qrcode_trace where ActionID=$ID and ActionType=5 and QRID=$result->ID")->row();
                if(!$check){
                    $this->db->query("update ttp_report_factory_qrcode set OrderID=0,Status=3 where ID=$result->ID");
                    $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$result->ID,$UserID,$ID,5)");
                    $data['error']  = false;
                    if($remain==1){
                        $data['ready']=$order->ID;
                        $this->reject_readyclose($order->ExportID);
                    }
                }
            }else{
                $result = $this->db->query("select a.ID,a.BarcodeID,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=2 and a.Published=1 and b.Published=1 and a.OrderID=$ID $warehouse")->result();
                $data['message1'] = "select a.ID,a.BarcodeID,c.Tankage from ttp_report_factory_qrcode a,ttp_report_factory_barcode b,ttp_report_products c where b.ProductsID=c.ID and b.Code='$code' and b.Status=1 and a.BarcodeID=b.ID and a.Status=2 and a.Published=1 and b.Published=1 and a.OrderID=$ID $warehouse";
                if(count($result)>0){
                    $data['message2'] = count($result)."<=$remain";
                    if(count($result)<=$remain){
                        $data['error']  = false;
                        $temp = 1;
                        foreach($result as $row){
                            if($row->Tankage==count($result)){
                                $this->db->query("update ttp_report_factory_qrcode set OrderID=0,Status=3 where ID=$row->ID");
                                $this->db->query("insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID,ActionType) values('$Note','$Created',$row->ID,$UserID,$ID,5)");
                                if($remain==count($result) && $temp==1){
                                    $data['ready']=$order->ID;
                                    $this->reject_readyclose($order->ExportID);
                                    $temp++;
                                }
                            }
                        }
                    }
                }
            }
        }
        echo json_encode($data);
    }

    public function readyclose($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->db->query("update ttp_report_export_warehouse set IsScan=1 where ID=$id");
    }

    public function reject_readyclose($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->db->query("update ttp_report_export_warehouse set RejectIsScan=1 where ID=$id");
    }

    public function import_readyclose($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->db->query("update ttp_report_import_warehouse set IsScan=1 where ID=$id");
    }

    public function mobilization_readyclose($id=0,$status=1){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        if($status==1){
            $skip = 1;
        }else{
            $skip = 2;
        }
        $this->db->query("update ttp_report_transferorder_mobilization set IsScan=$status,skip=$skip where ID=$id");
    }

    public function change_inventory($order,$details,$updatecols='OnHand'){
        if($order){
            if(count($details)>0){
                foreach($details as $row){
                    $bundle = $this->db->query("select * from ttp_report_orderdetails_bundle where DetailsID=$row->ID")->result();
                    if(count($bundle)>0){
                        foreach($bundle as $item){
                            $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                            if($check_exists){
                                $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$item->ProductsID and ShipmentID=$item->ShipmentID and WarehouseID=$order->KhoID and LastEdited=1");
                                if($check_exists->DateInventory==date('Y-m-d')){
                                    if($updatecols=='OnHand'){
                                        $OnHand = $check_exists->OnHand-$item->Amount;
                                        $OnHand = $OnHand<0 ? 0 : $OnHand ;
                                        $this->db->query("update ttp_report_inventory set OnHand=$OnHand,LastEdited=1 where ID=$check_exists->ID");
                                        $data_log = array(
                                            'ProductsID'    =>$item->ProductsID,
                                            'OnHand'        =>$OnHand,
                                            'Available'     =>$check_exists->Available,
                                            'WarehouseID'   =>$order->KhoID,
                                            'ShipmentID'    =>$item->ShipmentID,
                                            'DateInventory' =>$check_exists->DateInventory,
                                            'Type'          =>"OnHand",
                                            'Amount'        =>$item->Amount,
                                            'Action'        =>'change_inventory_update 2',
                                            'Method'        =>'change order id '.$order->ID
                                        );
                                        $this->write_log_inventory($data_log);
                                    }
                                }else{
                                    if($updatecols=='OnHand'){
                                        $OnHand = $check_exists->OnHand-$item->Amount;
                                        $OnHand = $OnHand<0 ? 0 : $OnHand ;
                                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($item->ProductsID,$OnHand,$check_exists->Available,$order->KhoID,$item->ShipmentID,'".date('Y-m-d')."',1)");
                                        $data_log = array(
                                            'ProductsID'    =>$item->ProductsID,
                                            'OnHand'        =>$OnHand,
                                            'Available'     =>$check_exists->Available,
                                            'WarehouseID'   =>$order->KhoID,
                                            'ShipmentID'    =>$item->ShipmentID,
                                            'DateInventory' =>date('Y-m-d'),
                                            'Type'          =>"OnHand",
                                            'Amount'        =>$item->Amount,
                                            'Action'        =>'change_inventory_insert 4',
                                            'Method'        =>'change order id '.$order->ID
                                        );
                                        $this->write_log_inventory($data_log);
                                    }
                                }
                            }
                        }
                    }else{
                        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1 order by DateInventory DESC")->row();
                        if($check_exists){
                            if($check_exists->DateInventory==Date('Y-m-d')){
                                if($updatecols=='OnHand'){
                                    $OnHand = $check_exists->OnHand-$row->Amount;
                                    $OnHand = $OnHand<0 ? 0 : $OnHand ;
                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");
                                    $this->db->query("update ttp_report_inventory set OnHand=$OnHand,LastEdited=1 where ID=$check_exists->ID");
                                    $data_log = array(
                                        'ProductsID'    =>$row->ProductsID,
                                        'OnHand'        =>$OnHand,
                                        'Available'     =>$check_exists->Available,
                                        'WarehouseID'   =>$order->KhoID,
                                        'ShipmentID'    =>$row->ShipmentID,
                                        'DateInventory' =>date('Y-m-d'),
                                        'Type'          =>"OnHand",
                                        'Amount'        =>$row->Amount,
                                        'Action'        =>'change_inventory_update 6',
                                        'Method'        =>'change order id '.$order->ID
                                    );
                                    $this->write_log_inventory($data_log);
                                }
                            }else{
                                $Available = $check_exists->Available;
                                $OnHand = $check_exists->OnHand;
                                if($updatecols=='OnHand'){
                                    $OnHand = $check_exists->OnHand-$row->Amount;
                                    $OnHand = $OnHand<0 ? 0 : $OnHand ;
                                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$row->ShipmentID and ProductsID=$row->ProductsID and WarehouseID=$order->KhoID and LastEdited=1");
                                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($row->ProductsID,$OnHand,$Available,$order->KhoID,$row->ShipmentID,'".date('Y-m-d')."',1)");
                                    $data_log = array(
                                        'ProductsID'    =>$row->ProductsID,
                                        'OnHand'        =>$OnHand,
                                        'Available'     =>$Available,
                                        'WarehouseID'   =>$order->KhoID,
                                        'ShipmentID'    =>$row->ShipmentID,
                                        'DateInventory' =>date('Y-m-d'),
                                        'Type'          =>"OnHand",
                                        'Amount'        =>$row->Amount,
                                        'Action'        =>'change_inventory_insert 8',
                                        'Method'        =>'change order id '.$order->ID
                                    );
                                    $this->write_log_inventory($data_log);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /*
    *   Define QR Code
    */
    public function define_qrcode(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $this->template->add_title('DEFINE QRCODE | Report Tools');
        $data = array(
            'warehouse_accept'=>$this->getwarehouselist(),
            'baselink'  =>base_url().ADMINPATH.'/report/warehouse_scancode/'
        );
        $this->template->write_view('content','admin/warehouse_mapping_single',$data);
        $this->template->render();
    }

    public function save_mapping_single(){
        $WarehouseID = isset($_POST['WarehouseID']) ? (int)$_POST['WarehouseID'] : 0 ;
        $ShipmentID = isset($_POST['ShipmentID']) ? (int)$_POST['ShipmentID'] : 0 ;
        $ProductsID = isset($_POST['ProductsID']) ? (int)$_POST['ProductsID'] : 0 ;
        $WarehouseText = isset($_POST['WarehouseTitle']) ? mysql_real_escape_string($_POST['WarehouseTitle']) : '' ;
        $Qrcode = isset($_POST['data']) ? $_POST['data'] : '' ;
        $code = explode('=', $Qrcode);
        if(count($code)==2){
            $Qrcode = (int)$code[1];
        }else{
            $Qrcode = $code[0];            
        }
        $result = $this->db->query("select ID from ttp_report_factory_qrcode where Code=$Qrcode and ProductsID=0 and WarehouseID=0 and OrderID=0 and Status=0")->row();
        $data = array("error"=>null);
        if($result){
            if($WarehouseID==0 || $ShipmentID=='' || $ProductsID==''){
                $data['error'] = "Bắt buộc phải chọn kho ,lô hàng ,loại sản phẩm !";
            }else{
                $created = date('Y-m-d H:i:s');
                $this->db->query("update ttp_report_factory_qrcode set Status=4,WarehouseID=$WarehouseID,ShipmentID=$ShipmentID,ProductsID=$ProductsID where ID=$result->ID");
                $str[] = "('Quét QRCODE hàng lẻ tại kho / nhà máy $WarehouseText','$created',$result->ID,".$this->user->ID.",$WarehouseID)";
                $str = "insert into ttp_report_factory_qrcode_trace(Note,Created,QRID,UserID,ActionID) values".implode(',',$str);
                $this->db->query($str);
                $check_exists = $this->db->query("select * from ttp_report_inventory where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$WarehouseID and LastEdited=1")->row();
                $i=1;
                if($check_exists){
                    $data['message'] = 1;
                    $this->db->query("update ttp_report_inventory set LastEdited=0 where ProductsID=$ProductsID and ShipmentID=$ShipmentID and WarehouseID=$WarehouseID and LastEdited=1");
                    if($check_exists->DateInventory==date('Y-m-d')){
                        $this->db->query("update ttp_report_inventory set Available=Available+$i,OnHand=OnHand+$i,LastEdited=1 where ID=$check_exists->ID");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$check_exists->OnHand+$i,
                            'Available'     =>$check_exists->Available+$i,
                            'WarehouseID'   =>$WarehouseID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"All",
                            'Amount'        =>$i,
                            'Action'        =>'update',
                            'Method'        =>'Scan qrcode warehouse 1'
                        );
                        $this->write_log_inventory($data_log);
                    }else{
                        $OnHand = $check_exists->OnHand+$i;
                        $Available = $check_exists->Available+$i;
                        $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
                        $data_log = array(
                            'ProductsID'    =>$ProductsID,
                            'OnHand'        =>$OnHand,
                            'Available'     =>$Available,
                            'WarehouseID'   =>$WarehouseID,
                            'ShipmentID'    =>$ShipmentID,
                            'DateInventory' =>date('Y-m-d'),
                            'Type'          =>"All",
                            'Amount'        =>$i,
                            'Action'        =>'insert',
                            'Method'        =>'Scan qrcode warehouse 2'
                        );
                        $this->write_log_inventory($data_log);
                    }
                }else{
                    $data['message'] = 2;
                    $OnHand = $i;
                    $Available = $i;
                    $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
                    $data_log = array(
                        'ProductsID'    =>$ProductsID,
                        'OnHand'        =>$OnHand,
                        'Available'     =>$Available,
                        'WarehouseID'   =>$WarehouseID,
                        'ShipmentID'    =>$ShipmentID,
                        'DateInventory' =>date('Y-m-d'),
                        'Type'          =>"All",
                        'Amount'        =>$i,
                        'Action'        =>'insert',
                        'Method'        =>'Scan qrcode warehouse 3'
                    );
                    $this->write_log_inventory($data_log);
                }
            }
        }else{
            $data['error'] = "Mã QR Code không được chấp nhận . Vui lòng thử lại nhé !";
        }
        echo json_encode($data);
    }

    public function box_add_shipment($id=0){
        $result = $this->db->query("select ID,Title from ttp_report_products where ID=$id")->row();
        if($result){
            $this->load->view("warehouse_inventory_shipment_add",array('data'=>$result));
        }else{
            echo "<p>Dữ liệu gửi lên không chính xác !.</p>";
        }
    }

    public function save_shipment(){
        $ID = isset($_POST['ID']) ? $_POST['ID'] : '' ;
        $ShipmentCode = isset($_POST['ShipmentCode']) ? $_POST['ShipmentCode'] : '' ;
        $DateProduction = isset($_POST['DateProduction']) ? $_POST['DateProduction'] : '' ;
        $DateExpiration = isset($_POST['DateExpiration']) ? $_POST['DateExpiration'] : '' ;
        if($ShipmentCode!='' && $DateProduction!='' && $DateExpiration!=''){
            $data = array(
                'UserID'        => $this->user->ID,
                'ShipmentCode'  => $ShipmentCode,
                'ProductsID'    => $ID,
                'DateProduction'=> $DateProduction,
                'DateExpiration'=> $DateExpiration,
                'Created'       => date('Y-m-d H:i:s')
            );
            $this->db->insert("ttp_report_shipment",$data);
            $ID = $this->db->insert_id();
            $arr = array("ID"=>$ID,"Title"=>$ShipmentCode,"Error"=>"");
            echo json_encode($arr);
        }else{
            echo json_encode(array("Error"=>"1"));
        }
    }

    public function load_shipment_by_products($id=0){
        $result = $this->db->query("select a.ShipmentCode,a.ID,b.MaSP from ttp_report_shipment a,ttp_report_products b where a.ProductsID=b.ID and a.ProductsID=$id")->result();
        echo '<option value="0">-- Chọn lô sản xuất --</option>';
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->MaSP - $row->ShipmentCode</option>";
            }
        }
    }
}
?>

