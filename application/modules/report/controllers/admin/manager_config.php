<?php 
class Manager_config extends Admin_Controller { 
 
    public $limit = 30;
    public $user;
    public $classname="manager_config";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $group = isset($_GET['group']) ? $_GET['group'] : '' ;
        $type = isset($_GET['type']) ? $_GET['type'] : '' ;
        $bonus = " where 1=1";
        $bonus .= $group!='' ? " and `group`='$group'" : "" ;
        $bonus .= $type!='' ? " and `type`='$type'" : "" ;
        $nav = $this->db->query("select count(1) as nav from ttp_define $bonus")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_define $bonus order by id DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_config/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_config/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Static config | Manager Report Tools');
        $this->template->write_view('content','admin/manager_config_static_home',$data);
        $this->template->render();
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Add static config | Manager Report Tools');
        $this->template->write_view('content','admin/manager_config_static_add');
        $this->template->render();
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $result = $this->db->query("select * from ttp_define where id=$id")->row();
        if($result){
            $data = array(
                'data'=>$result
            );
            $this->template->add_title('Edit static config | Manager Report Tools');
            $this->template->write_view('content','admin/manager_config_static_edit',$data);
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $group = isset($_POST['group']) ? $_POST['group'] : '' ;
        $type = isset($_POST['type']) ? $_POST['type'] : '' ;
        $code = isset($_POST['code']) ? $_POST['code'] : '' ;
        $name = isset($_POST['name']) ? $_POST['name'] : '' ;
        $lang = isset($_POST['lang']) ? $_POST['lang'] : '' ;
        $color = isset($_POST['color']) ? $_POST['color'] : '' ;
        $position = isset($_POST['position']) ? $_POST['position'] : '' ;
        $promotion_active = isset($_POST['promotion_active']) ? $_POST['promotion_active'] : '' ;
        $description = isset($_POST['description']) ? $_POST['description'] : '' ;
        if($group!='' && $code!='' && $type!=''){
            $data = array(
                'group'=>$group,
                'type'=>$type,
                'code'=>$code,
                'name'=>$name,
                'lang'=>$lang,
                'color'=>$color,
                'position'=>$position,
                'promotion_active'=>$promotion_active,
                'description'=>$description,
                'created'=>date('Y-m-d H:i:s'),
                'created_by'=>$this->user->ID
            );
            $this->db->insert('ttp_define',$data);
        }
        redirect(base_url().ADMINPATH.'/report/manager_config/');
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $_POST['ID'] : $ID ;
        $group = isset($_POST['group']) ? $_POST['group'] : '' ;
        $type = isset($_POST['type']) ? $_POST['type'] : '' ;
        $code = isset($_POST['code']) ? $_POST['code'] : '' ;
        $name = isset($_POST['name']) ? $_POST['name'] : '' ;
        $lang = isset($_POST['lang']) ? $_POST['lang'] : '' ;
        $color = isset($_POST['color']) ? $_POST['color'] : '' ;
        $position = isset($_POST['position']) ? $_POST['position'] : '' ;
        $promotion_active = isset($_POST['promotion_active']) ? $_POST['promotion_active'] : '' ;
        $description = isset($_POST['description']) ? $_POST['description'] : '' ;
        if($ID>0 && $group!='' && $code!='' && $type!=''){
            $result = $this->db->query("select * from ttp_define where id=$ID")->row();
            if($result){
                $data = array(
                    'group'=>$group,
                    'type'=>$type,
                    'code'=>$code,
                    'name'=>$name,
                    'lang'=>$lang,
                    'color'=>$color,
                    'position'=>$position,
                    'promotion_active'=>$promotion_active,
                    'description'=>$description,
                    'updated'=>date('Y-m-d H:i:s'),
                    'updated_by'=>$this->user->ID
                );
                $this->db->where("id",$ID);
                $this->db->update('ttp_define',$data);
            }
        }
        redirect(base_url().ADMINPATH.'/report/manager_config/');
    }
    
    public function delete($ID=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        $ID = (int)$ID;
        if($ID>0){
            $result = $this->db->query("delete from ttp_define where id=$ID");
        }
        redirect(base_url().ADMINPATH.'/report/manager_config/');
    }

}
?>