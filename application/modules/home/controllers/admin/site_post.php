<?php

class Site_post extends Admin_Controller {

    public $user;
    public $classname = "site_post";
    public $limit = 15;

    public function __construct() {

        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);

        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('Post list | Config Site');
        $limit_str = "limit $start,$this->limit";
        $CategoriesRole = $this->user->CategoriesRole;
        $CategoriesRole = $CategoriesRole != '' ? json_decode($CategoriesRole, true) : array();
        $CategoriesRole = count($CategoriesRole) > 0 ? implode(",", $CategoriesRole) : "0";

        if ($this->user->IsAdmin == 1 || $this->user->UserType == 11) {
            $bonus = "";
        } else {
            $bonus = "";
            //$bonus = " and b.ID in($CategoriesRole)";
        }

        $nav = $this->db->query("select count(1) as nav from ttp_post a,ttp_categories b where a.CategoriesID=b.ID $bonus")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Title as PageTitle from ttp_post a,ttp_categories b where a.CategoriesID=b.ID $bonus order by Created DESC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_post/',
            'data' => $object,
            'start' => $start,
            'find' => $nav,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_post/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/site_post_home', $data);
        $this->template->render();
    }

    public function search($link = 'search') {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $limit_str = "limit $start,$this->limit";
        $keyword = $this->session->userdata("post_filter_Keywords");
        $CategoriesID = $this->session->userdata("post_filter_CategoriesID");
        $Published = $this->session->userdata("post_filter_Published");
        $str_nav = "select count(1) as nav from ttp_post a,ttp_categories b where a.CategoriesID=b.ID";
        $str = "select a.*,b.Title as PageTitle from ttp_post a,ttp_categories b where a.CategoriesID=b.ID";
        $str = $keyword != '' ? $str . " and (a.Title like '%" . urldecode($keyword) . "%' or b.Title like '%" . urldecode($keyword) . "%')" : $str;
        $str_nav = $keyword != '' ? $str_nav . " and (a.Title like '%" . urldecode($keyword) . "%' or b.Title like '%" . urldecode($keyword) . "%')" : $str_nav;
        if ($CategoriesID > 0) {
            $str .= " and a.CategoriesID=$CategoriesID";
            $str_nav .= " and a.CategoriesID=$CategoriesID";
        } else {
            $CategoriesRole = $this->user->CategoriesRole;
            $CategoriesRole = $CategoriesRole != '' ? json_decode($CategoriesRole, true) : array();
            $CategoriesRole = count($CategoriesRole) > 0 ? implode(",", $CategoriesRole) : "0";
            if ($this->user->IsAdmin == 1) {
                $str .= "";
                $str_nav .= "";
            } else {
                $str .= " and a.CategoriesID in ($CategoriesRole)";
                $str_nav .= " and a.CategoriesID in ($CategoriesRole)";
            }
        }

        if ($Published != '') {
            $str .= " and a.Published=$Published";
            $str_nav .= " and a.Published=$Published";
        }
        $nav = $this->db->query($str_nav)->row();
        $nav = $nav ? $nav->nav : 0;
        $this->template->add_title('Tìm kiếm dữ liệu');
        $data = array(
            'data' => $this->db->query($str . " order by ID DESC $limit_str")->result(),
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_post/' . $link, 5, $nav, $this->limit),
            'start' => $start,
            'find' => $nav,
            'base_link' => base_url() . ADMINPATH . '/home/site_post/',
        );

        $this->template->write_view('content', 'admin/site_post_home', $data);
        $this->template->render();
    }

    public function setsessionsearch() {

        if (isset($_POST['Keywords'])) {
            $Keywords = mysql_real_escape_string($_POST['Keywords']);
            $this->session->set_userdata("post_filter_Keywords", $Keywords);
        }

        if (isset($_POST['CategoriesID'])) {
            $CategoriesID = mysql_real_escape_string($_POST['CategoriesID']);
            $this->session->set_userdata("post_filter_CategoriesID", $CategoriesID);
        }

        if (isset($_POST['Published'])) {
            $Published = mysql_real_escape_string($_POST['Published']);
            $this->session->set_userdata("post_filter_Published", $Published);
        }
        $this->search('setsessionsearch');
    }

    public function clearfilter() {
        $this->session->unset_userdata("post_filter_Keywords");
        $this->session->unset_userdata("post_filter_CategoriesID");
        $this->session->unset_userdata("post_filter_Published");
        $this->search('setsessionsearch');
    }

    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $post = $this->db->query("select Thumb from ttp_post where ID=$id")->row();
            if ($post)
                $this->delete_file_path($post->Thumb);
            $this->db->query("delete from ttp_post where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add Post | Config Site');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_post/'
        );
        $this->template->write_view('content', 'admin/site_post_add', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '';
        $Title_en = isset($_POST['Title_en']) ? $_POST['Title_en'] : '';
        $Description = isset($_POST['Description']) ? $_POST['Description'] : '';
        $Description_en = isset($_POST['Description_en']) ? $_POST['Description_en'] : '';
        $Introtext = isset($_POST['Introtext']) ? $_POST['Introtext'] : '';
        $Introtext = str_replace("script", "", $Introtext);
        $Introtext_en = isset($_POST['Introtext_en']) ? $_POST['Introtext_en'] : '';
        $Introtext_en = str_replace("script", "", $Introtext_en);
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title;
        $Link = $this->lib->alias($Alias);
        $Alias_en = isset($_POST['Alias_en']) ? $_POST['Alias_en'] : $Title_en;
        $Link_en = $this->lib->alias($Alias_en);
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0;
        $PagelinksID = isset($_POST['PagelinksID']) ? $this->lib->fill_data($_POST['PagelinksID']) : 0;
        if ($Title != '') {
            $newuser = $this->db->query("select max(ID) as ID from ttp_post")->row();
            $NewID = $newuser ? $newuser->ID + 1 : 1;
            $check = $this->db->query("select * from ttp_post where Alias='$Link'")->row();
            if ($check) {
                $Link = $Link . "-" . $NewID;
            }
            $check = $this->db->query("select * from ttp_categories where Alias='$Link'")->row();
            if ($check) {
                $Link = $Link . "-" . time();
            }
            $check = $this->db->query("select * from ttp_pagelinks where Alias='$Link'")->row();
            if ($check) {
                $Link = $Link . "-" . time();
            }
            $check = $this->db->query("select * from ttp_post where Alias_en='$Link_en'")->row();
            if ($check) {
                $Link_en = $Link_en . "-" . $NewID;
            }
            $check = $this->db->query("select * from ttp_categories where Alias_en='$Link_en'")->row();
            if ($check) {
                $Link_en = $Link_en . "-" . time();
            }
            $check = $this->db->query("select * from ttp_pagelinks where Alias_en='$Link_en'")->row();
            if ($check) {
                $Link_en = $Link_en . "-" . time();
            }

            $data = array(
                'CategoriesID' => $CategoriesID,
                'PagelinksID' => $PagelinksID,
                'Published' => $Published,
                'Title' => $Title,
                'Description' => $Description,
                'Introtext' => $Introtext,
                'Alias' => $Link,
                'Title_en' => $Title_en,
                'Description_en' => $Description_en,
                'Introtext_en' => $Introtext_en,
                'Alias_en' => $Link_en,
                'Created' => date("Y-m-d H:i:s"),
                'LastEdited' => date("Y-m-d H:i:s")
            );
            $arr = array();
            foreach ($_POST as $key => $value) {
                $temp = trim($key);
                $temp = strtolower($key);
                if ($temp == 'tags' || $temp == 'tags_en') {
                    $str = explode(",", $value);
                    $arr_tags = array();
                    if (count($str) > 0) {
                        foreach ($str as $row) {
                            $row = trim($row);
                            $row = mysql_real_escape_string($row);
                            $tagsAlias = $this->lib->alias($row);
                            $check = $this->db->query("select * from ttp_tags where Title like '$row'")->row();
                            if (!$check) {
                                if ($row != '') {
                                    $tags = array(
                                        'Title' => $row,
                                        'Alias' => $tagsAlias . '-' . time(),
                                        'Created' => date("Y-m-d H:i:s"),
                                        'LastEdited' => date("Y-m-d H:i:s"),
                                        'Published' => 1
                                    );
                                    $this->db->insert('ttp_tags', $tags);
                                    $tagsID = $this->db->query("select max(ID) as ID from ttp_tags")->row();
                                    if ($tagsID)
                                        $arr_tags[] = "[$tagsID->ID]";
                                }
                            }else {
                                $arr_tags[] = "[$check->ID]";
                            }
                        }
                    }

                    if (count($arr_tags) > 0) {
                        if ($temp == "tags") {
                            $data['Tags'] = implode(",", $arr_tags);
                        } else {
                            $data['Tags_en'] = implode(",", $arr_tags);
                        }
                    }
                } else {

                    if (!array_key_exists($key, $data) && $key != 'Thumb') {
                        $key = trim($key);
                        $key = strtolower($key);
                        $arr[$key] = $value;
                    }
                }
            }

            $arr = json_encode($arr);
            $data['Data'] = $arr;
            if (isset($_POST['Thumb'])) {
                $data['Thumb'] = $this->lib->fill_data($_POST['Thumb']);
            } else {
                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['error'] == 0) {
                        $this->upload_to = "post_thumb/$NewID";
                        $data['Thumb'] = $this->upload_image_single();
                        // $cropimage = explode(",",IMAGECROP);
                        // foreach($cropimage as $row){
                        // 	$size = explode("x", $row);
                        // 	$width = isset($size[0]) ? (int)$size[0] : 0 ;
                        // 	$height = isset($size[1]) ? (int)$size[1] : 0 ;
                        // 	if($width>0 && $height>0) $this->lib->cropimage($data['Thumb'],$width,$height);
                        // }
                    }
                }
            }

            $this->db->insert("ttp_post", $data);
        }
        redirect(ADMINPATH . '/home/site_post/');
    }

    public function add_and_edit() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $_POST['Title'] : 'Tiêu đề bài viết review';
        $Title_en = isset($_POST['Title_en']) ? $_POST['Title_en'] : '';
        $Description = isset($_POST['Description']) ? $_POST['Description'] : '';
        $Description_en = isset($_POST['Description_en']) ? $_POST['Description_en'] : '';
        $Introtext = isset($_POST['Introtext']) ? $_POST['Introtext'] : '';
//        $Introtext = str_replace("script", "", $Introtext);
        $Introtext_en = isset($_POST['Introtext_en']) ? $_POST['Introtext_en'] : '';
//        $Introtext_en = str_replace("script", "", $Introtext_en);
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title;
        $Link = $this->lib->alias($Alias);
        $Alias_en = isset($_POST['Alias_en']) ? $_POST['Alias_en'] : $Title_en;
        $Link_en = $this->lib->alias($Alias_en);
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 2;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0;
        $CategoriesID = $this->db->query("select ID from ttp_categories limit 0,1")->row();
        $CategoriesID = $CategoriesID ? $CategoriesID->ID : 0;
        $PagelinksID = isset($_POST['PagelinksID']) ? $this->lib->fill_data($_POST['PagelinksID']) : 0;

        if ($Title != '' && $CategoriesID > 0) {

            $newuser = $this->db->query("select max(ID) as ID from ttp_post")->row();

            $NewID = $newuser ? $newuser->ID + 1 : 1;

            $check = $this->db->query("select * from ttp_post where Alias='$Link'")->row();

            if ($check) {

                $Link = $Link . "-" . $NewID;
            }

            $check = $this->db->query("select * from ttp_categories where Alias='$Link'")->row();

            if ($check) {

                $Link = $Link . "-" . time();
            }

            $check = $this->db->query("select * from ttp_pagelinks where Alias='$Link'")->row();

            if ($check) {

                $Link = $Link . "-" . time();
            }



            $check = $this->db->query("select * from ttp_post where Alias_en='$Link_en'")->row();

            if ($check) {

                $Link_en = $Link_en . "-" . $NewID;
            }

            $check = $this->db->query("select * from ttp_categories where Alias_en='$Link_en'")->row();

            if ($check) {

                $Link_en = $Link_en . "-" . time();
            }

            $check = $this->db->query("select * from ttp_pagelinks where Alias_en='$Link_en'")->row();

            if ($check) {

                $Link_en = $Link_en . "-" . time();
            }

            $data = array(
                'ID' => $NewID,
                'CategoriesID' => $CategoriesID,
                'PagelinksID' => $PagelinksID,
                'Published' => $Published,
                'Title' => $Title,
                'Description' => $Description,
                'Introtext' => $Introtext,
                'Alias' => $Link,
                'Title_en' => $Title_en,
                'Description_en' => $Description_en,
                'Introtext_en' => $Introtext_en,
                'Alias_en' => $Link_en,
                'Created' => date("Y-m-d H:i:s"),
                'LastEdited' => date("Y-m-d H:i:s")
            );

            $arr = array();

            $arr = json_encode($arr);

            $data['Data'] = $arr;
            $data['Owner'] = $this->user->ID;
            if ($this->db->insert("ttp_post", $data)) {
                $history = array(
                    'Created' => date('Y-m-d H:i:s'),
                    'PostID' => $this->db->insert_id(),
                    'Type' => 'New Post',
                    'Owner' => $this->user->ID
                );
                $this->db->insert('ttp_post_history', $history);
            }
            redirect(ADMINPATH . '/home/site_post/edit/' . $NewID);
        } else {
            redirect(ADMINPATH . '/home/site_categories');
        }
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_post where ID=$id")->row();
            if (!$result)
                return;
            $this->template->add_title('Edit Post | Config Site');
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_post/',
                'data' => $result
            );
            $this->template->write_view('content', 'admin/site_post_edit', $data);
            $this->template->render();
        }
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : 0;
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '';
        $Title_en = isset($_POST['Title_en']) ? $_POST['Title_en'] : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Description_en = isset($_POST['Description_en']) ? $this->security->xss_clean($_POST['Description_en']) : '';
        $Introtext = isset($_POST['Introtext']) ? $_POST['Introtext'] : '';
        $YoutubeID = isset($_POST['YoutubeID']) ? $_POST['YoutubeID'] : '';
//        $Introtext = str_replace("script", "", $Introtext);
        $Introtext_en = isset($_POST['Introtext_en']) ? $_POST['Introtext_en'] : '';
//        $Introtext_en = str_replace("script", "", $Introtext_en);
        $Alias = isset($_POST['Alias']) ? $_POST['Alias'] : $Title;
        $Link = $this->lib->alias($Alias);
        $Alias_en = isset($_POST['Alias_en']) ? $_POST['Alias_en'] : $Title_en;
        $Link_en = $this->lib->alias($Alias_en);
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $CategoriesID = isset($_POST['CategoriesID']) ? $this->lib->fill_data($_POST['CategoriesID']) : 0;
        $PagelinksID = isset($_POST['PagelinksID']) ? $this->lib->fill_data($_POST['PagelinksID']) : 0;
        $check = $this->db->query("select * from ttp_post where Alias='$Link' and ID!=$ID")->row();
        if ($check) {
            $Link = $Link . "-" . $ID;
        }
        $check = $this->db->query("select * from ttp_categories where Alias='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_pagelinks where Alias='$Link'")->row();
        if ($check) {
            $Link = $Link . "-" . time();
        }
        $check = $this->db->query("select * from ttp_post where Alias_en='$Link_en' and ID!=$ID")->row();
        if ($check) {
            $Link_en = $Link_en . "-" . $ID;
        }
        $check = $this->db->query("select * from ttp_categories where Alias_en='$Link_en'")->row();
        if ($check) {
            $Link_en = $Link_en . "-" . time();
        }
        $check = $this->db->query("select * from ttp_pagelinks where Alias_en='$Link_en'")->row();
        if ($check) {
            $Link_en = $Link_en . "-" . time();
        }
        if ($Title != '') {
            $data = array(
                'CategoriesID' => $CategoriesID,
                'PagelinksID' => $PagelinksID,
                'Published' => $Published,
                'Title' => $Title,
                'Description' => $Description,
                'YoutubeID' => $YoutubeID,
                'Introtext' => $Introtext,
                'Alias' => $Link,
                'Title_en' => $Title_en,
                'Description_en' => $Description_en,
                'Introtext_en' => $Introtext_en,
                'Alias_en' => $Link_en,
                'LastEdited' => date("Y-m-d H:i:s")
            );

            $arr = array();
            foreach ($_POST as $key => $value) {
                $temp = trim($key);
                $temp = strtolower($key);
                if ($temp == 'tags' || $temp == 'tags_en') {
                    $str = explode(",", $value);
                    $arr_tags = array();
                    if (count($str) > 0) {
                        foreach ($str as $row) {
                            $row = trim($row);
                            $row = mysql_real_escape_string($row);
                            $tagsAlias = $this->lib->alias($row);
                            $check = $this->db->query("select * from ttp_tags where Title like '$row'")->row();
                            if (!$check) {
                                if ($row != '') {
                                    $tags = array(
                                        'Title' => $row,
                                        'Alias' => $tagsAlias . '-' . time(),
                                        'Created' => date("Y-m-d H:i:s"),
                                        'LastEdited' => date("Y-m-d H:i:s"),
                                        'Published' => 1
                                    );
                                    $this->db->insert('ttp_tags', $tags);
                                    $tagsID = $this->db->query("select max(ID) as ID from ttp_tags")->row();
                                    if ($tagsID)
                                        $arr_tags[] = "[$tagsID->ID]";
                                }
                            }else {
                                $arr_tags[] = "[$check->ID]";
                            }
                        }
                    }

                    if (count($arr_tags) > 0) {
                        if ($temp == "tags") {
                            $data['Tags'] = implode(",", $arr_tags);
                        } else {
                            $data['Tags_en'] = implode(",", $arr_tags);
                        }
                    }
                } else {
                    if (!array_key_exists($key, $data) && $key != "ID" && $key != 'Thumb') {
                        $key = trim($key);
                        $key = strtolower($key);
                        $arr[$key] = $value;
                    }
                }
            }

            $arr = json_encode($arr);
            $data['Data'] = $arr;
            if (isset($_POST['Thumb'])) {
                $data['Thumb'] = $this->lib->fill_data($_POST['Thumb']);
            } else {
                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['error'] == 0) {
                        $this->upload_to = "post_thumb/$ID";
                        $data['Thumb'] = $this->upload_image_single();

                        // $cropimage = explode(",",IMAGECROP);
                        // foreach($cropimage as $row){
                        // 	$size = explode("x", $row);
                        // 	$width = isset($size[0]) ? (int)$size[0] : 0 ;
                        // 	$height = isset($size[1]) ? (int)$size[1] : 0 ;
                        // 	if($width>0 && $height>0) $this->lib->cropimage($data['Thumb'],$width,$height);
                        // }

                        $this->delete_file_path($user->Thumb);
                    }
                }
            }
            $object_post = $this->db->query("select * from ttp_post where ID=$ID")->row();
            if ($object_post) {
                $this->db->where("ID", $ID);
                if ($this->db->update("ttp_post", $data)) {
                    $notes = array();
                    if ($object_post->Title != $Title) {
                        $notes[] = "Title modified from: <font color='green'>" . $object_post->Title . "</font>";
                    }
                    if ($object_post->Description != $Description) {
                        $notes[] = "Short Description modified from <font color='green'>" . $object_post->Description . "</font>";
                    }
                    if ($object_post->Introtext != $Introtext) {
                        $notes[] = "Introtext modified from <font color='green'>" . $object_post->Introtext . "</font>";
                    }
                    foreach ($notes as $item) {
                        $history = array(
                            'Created' => date('Y-m-d H:i:s'),
                            'PostID' => $ID,
                            'Type' => 'Update',
                            'Notes' => $item,
                            'Owner' => $this->user->ID
                        );
                        $this->db->insert('ttp_post_history', $history);
                    }
                }
            }
        }
        redirect(ADMINPATH . '/home/site_post/');
    }
    
    public function deletehistory($id = 0) {
        $PostID = $this->db->query("select PostID from ttp_post_history where ID=$id")->row()->PostID;
        $this->db->query("delete from ttp_post_history where ID=$id");
        $this->write_log("delete history from ttp_post_history where ID=" . $id);
        redirect(ADMINPATH . '/home/site_post/edit/'.$PostID);
    }

    public function autosave() {

        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);

        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';

        $Title = isset($_POST['Title']) ? $_POST['Title'] : '';

        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';

        $Introtext = isset($_POST['Introtext']) ? $_POST['Introtext'] : '';

        $Introtext = str_replace("script", "", $Introtext);

        if ($Title != '') {

            $data = array(
                'Title' => $Title,
                'Description' => $Description,
                'Introtext' => $Introtext,
                'LastEdited' => date("Y-m-d H:i:s")
            );

            $this->db->where("ID", $ID);

            $this->db->update("ttp_post", $data);
        }
    }

}
