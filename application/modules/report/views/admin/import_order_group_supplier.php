<div class="containner">
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1>Chuyển yêu cầu soạn hàng cho đối tác</h1>
	    </div>
	    <div class="block2">
            <a class="btn btn-success" href="<?php echo base_url().ADMINPATH."/report/import_order/picking_supplier" ?>"><i class="fa fa-book" aria-hidden="true"></i> Trạng thái soạn hàng</a>
			<a class="btn btn-danger" onclick="send_to_supplier(this)"><i class="fa fa-send"></i> Gửi thông tin</a>
	    </div>
    </div>
    <div class="row" style="margin-bottom:20px">
    	<div class="col-xs-2"><label class="control-label">Lọc nhà cung cấp</label></div>
    	<div class="col-xs-2">
    		<select class="form-control" id="SupplierID" onchange="changefillter(this)">
    			<option value='0'>Tất cả nhà cung cấp</option>
    			<?php 
    			$supplier = $this->db->query("select * from ttp_report_production")->result();
    			if(count($supplier)>0){
    				foreach($supplier as $row){
    					$selected = $row->ID==$SupplierID ? 'selected="selected"' : '' ;
    					echo "<option value='$row->ID' $selected>$row->Title</option>";
    				}
    			}
    			?>
    		</select>
    	</div>
        <div class="col-xs-2"><label class="control-label">Thời gian giao hàng</label></div>
        <div class="col-xs-2">
            <select class="form-control" id="Haftday" onchange="changefillter(this)">
                <?php 
                $haft = isset($_GET['haft']) ? $_GET['haft'] : 0 ;
                ?>
                <option value='0' <?php echo $haft==0 ? "selected='selected'" : '' ; ?>>Tất cả</option>
                <option value='1' <?php echo $haft==1 ? "selected='selected'" : '' ; ?>>Chỉ buổi sáng</option>
                <option value='2' <?php echo $haft==2 ? "selected='selected'" : '' ; ?>>Chỉ buổi chiều</option>
            </select>
        </div>
		<?php 
		$arr_branch = array();
        $arr_branch[0] = "<option value='0'>-- Chọn địa điểm --</option>";
		$supplier = $this->db->query("select * from ttp_report_branch")->result();
		if(count($supplier)>0){
			foreach($supplier as $row){
                if(isset($arr_branch[$row->SupplierID])){
                    $arr_branch[$row->SupplierID].="<option value='$row->ID'>$row->Title</option>";
                }else{
                    $arr_branch[$row->SupplierID] = "<option value='$row->ID'>$row->Title</option>";
                }
			}
		}
		?>
    </div>
    <?php 

    $arr_pending = array();
    $pending_out = $this->db->query("select a.* from ttp_report_order_send_supplier a,ttp_report_order b where a.Status=2 and a.WarehouseID>0 and a.OrderID=b.ID and b.Status!=1 and b.Status!=0")->result();
    if(count($pending_out)>0){
        foreach($pending_out as $row){
            if(isset($arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID])){
                $arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID] += $row->Amount;
            }else{
                $arr_pending[$row->WarehouseID][$row->ProductsID][$row->ShipmentID] = $row->Amount;
            }
        }
    }

    $arr_available_ucc = array();
    $available_ucc = $this->db->query("select * from ttp_report_inventory where LastEdited=1")->result();
    if(count($available_ucc)>0){
        foreach($available_ucc as $row){
            $arr_available_ucc[$row->WarehouseID][$row->ProductsID][$row->ShipmentID] = $row->OnHand;
        }
    }

    $result = array();
    $min = 0;
    if(count($data)>0){
    	foreach($data as $row){
    		if(isset($result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID])){
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Amount'] = $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Amount']+$row->Amount;
    		}else{
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Amount'] = $row->Amount;
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['MaDH'] = $row->MaDH;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Time'] = $row->DeliveryTime;
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Title'] = $row->Title;
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Donvi'] = $row->Donvi;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['MisaCode'] = $row->MisaCode;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['District'] = $row->District;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['WarehouseID'] = $row->KhoID;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['RootPrice'] = $row->RootPrice;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['SupplierID'] = $row->SupplierID;
                
    		}
            if($min==0){
                $min=$row->ID;
            }else{
                if($min>$row->ID){
                    $min = $row->ID;
                }
            }
    	}
    }

    if(count($data1)>0){
    	foreach($data1 as $row){
    		if(isset($result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID])){
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Amount'] = $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Amount']+$row->Amount;
    		}else{
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Amount'] = $row->Amount;
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['MaDH'] = $row->MaDH;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Time'] = $row->DeliveryTime;
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Title'] = $row->Title;
    			$result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['Donvi'] = $row->Donvi;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['MisaCode'] = $row->MisaCode;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['District'] = $row->District;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['WarehouseID'] = $row->KhoID;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['RootPrice'] = $row->RootPrice;
                $result[$row->ID][$row->ProductsID.'_'.$row->ShipmentID]['SupplierID'] = $row->SupplierID;
    		}
            if($min==0){
                $min=$row->ID;
            }else{
                if($min>$row->ID){
                    $min = $row->ID;
                }
            }
    	}
    }
    $sended = $this->db->query("select * from ttp_report_order_send_supplier where OrderID>=$min")->result();
    $arr_send = array();
    if(count($sended)>0){
        foreach($sended as $row){
            $arr_send[$row->OrderID][$row->ProductsID] = $row->Status;
        }
    }
    ?>
    <table class="table table-bordered table-hover">
    	<tr style="background:#f5f5f5">
    		<th>STT</th>
    		<th>MÃ ĐƠN HÀNG</th>
    		<th>SẢN PHẨM</th>
    		<th>ĐV</th>
    		<th>SL CẦN</th>
            <th>SL CÓ</th>
    		<th>SL LẤY NCC</th>
            <th>SL LẤY UCC</th>
            <th class="text-center">KHU VỰC GIAO HÀNG</th>
    		<th class="text-center">ĐỊA ĐIỂM LẤY HÀNG</th>
    		<th><input type='checkbox' onclick="checkfull(this)" /></th>
    	</tr>
    <?php 
    ksort($result);
    if(count($result)>0){
    	$i=1;
        $total = 0;
    	foreach($result as $orderid=>$row){
    		if(count($row)>0){
    			foreach($row as $key=>$item){
                    $key = explode('_',$key);
                    $ProductsID = isset($key[0]) ? $key[0] : 0 ;
                    $ShipmentID = isset($key[1]) ? $key[1] : 0 ;
                    $Title = isset($item['Title']) ? $item['Title'] : '--' ;
                    $MaDH = isset($item['MaDH']) ? $item['MaDH'] : '--' ;
                    $Donvi = isset($item['Donvi']) ? $item['Donvi'] : '--' ;
                    $Amount = isset($item['Amount']) ? $item['Amount'] : 0 ;
                    $Time = isset($item['Time']) ? $item['Time'] : '' ;
                    $MisaCode = isset($item['MisaCode']) ? $item['MisaCode'] : '' ;
                    $District = isset($item['District']) ? $item['District'] : '' ;
                    $WarehouseID = isset($item['WarehouseID']) ? $item['WarehouseID'] : 0 ;
                    $RootPrice = isset($item['RootPrice']) ? $item['RootPrice'] : 0 ;
                    $SupplierID = isset($item['SupplierID']) ? $item['SupplierID'] : 0 ;
                    $pending_out_row = isset($arr_pending[$WarehouseID][$ProductsID][$ShipmentID]) ? $arr_pending[$WarehouseID][$ProductsID][$ShipmentID] : 0 ;
                    $onhand_ucc_row = isset($arr_available_ucc[$WarehouseID][$ProductsID][$ShipmentID]) ? $arr_available_ucc[$WarehouseID][$ProductsID][$ShipmentID] : 0 ;
                    $available_ucc_row = $onhand_ucc_row-$pending_out_row;
                    $day = date('d/m/Y',strtotime($Time));
                    $haftday = date('H',strtotime($Time));
                    $haftstate = $haftday<12 ? "<b style='background: #a2cd3a;color: #FFF;padding: 2px 5px;border-radius: 20px;'>Sáng</b>" : "<b style='background: #f96868;color: #FFF;padding: 2px 5px;border-radius: 20px;'>Chiều</b>" ;
                    $truehaft = 0;
                    if($haft==1){
                        if($haftday<=12){
                            $truehaft=1;
                        }
                    }
                    if($haft==2){
                        if($haftday>12){
                            $truehaft=1;
                        }
                    }
                    if($haft==0){
                        $truehaft = 1;
                    }
                    if(!isset($arr_send[$orderid][$ProductsID])){
                        if($truehaft==1){
                            $get_ucc_row = $available_ucc_row>=$Amount ? $Amount : $available_ucc_row ;
                            $get_ncc_row = $Amount-$get_ucc_row;
                            if(isset($arr_available_ucc[$WarehouseID][$ProductsID][$ShipmentID])){
                                $arr_available_ucc[$WarehouseID][$ProductsID][$ShipmentID] = $arr_available_ucc[$WarehouseID][$ProductsID][$ShipmentID]-$get_ucc_row;
                            }
                            $arr_branch[$SupplierID] = isset($arr_branch[$SupplierID]) ? $arr_branch[$SupplierID] : '<option value="0">-- Chọn địa điểm --</option>' ;
                            echo "<tr>
        		    				<td class='text-center'>$i</td>
        		    				<td>$MaDH <br> <small style='margin-top:3px;color:#888;'>".$haftstate.' '.$day."</small></td>
        		    				<td>$Title</td>
        		    				<td style='width:60px'>$Donvi</td>
        		    				<td class='text-right' style='width: 68px;'>".number_format($Amount,2)."</td>
                                    <td class='text-right' style='width: 68px;'>".number_format($available_ucc_row,2)."</td>
        		    				<td class='text-right' style='width: 90px;'><input type='number' min='0' class='amountncc' value='$get_ncc_row' style='padding:3px;border:1px solid #ccc;outline:none;width:90px;text-align:right;vertical-align: middle;' /></td>
                                    <td class='text-right' style='width: 90px;'><input type='number' min='0' class='amountucc' value='$get_ucc_row' style='padding:3px;border:1px solid #ccc;outline:none;width:90px;text-align:right;vertical-align: middle;' /></td>
        		    				<td class='text-center' style='width: 160px;vertical-align: middle;'>$District - $MisaCode</td>
                                    <td class='text-center' style='width: 160px;vertical-align: middle;'><select style='padding:4px 3px;border:1px solid #ccc;outline:none;width:160px' class='branch'>".$arr_branch[$SupplierID]."</select></td>
        		    				<td class='text-center' style='vertical-align: middle;'><input type='checkbox' class='checkedorder' data-products='$ProductsID' data-shipment='$ShipmentID' data-order='$orderid' data-price='$RootPrice' /></td>
        		    			</tr>";
        		    		$i++;
                            $total = $total+$Amount;
                        }
                    }
	    		}
    		}
    	}
        echo $total>0 ? "<tr><th colspan='4' class='text-right'>TỔNG CỘNG</th><th colspan='8'>".number_format($total)."</th></tr>" : "<tr><td colspan='12'>Không tìm thấy dữ liệu theo yêu cầu</td></tr>";
    }else{
    	echo "<tr><td colspan='12'>Không tìm thấy dữ liệu theo yêu cầu</td></tr>";
    }
    ?>
	</table>
</div>
<script>
	var link = "<?php echo $base_link ?>";

	function changefillter(ob){
		var SupplierID = $("#SupplierID").val();
        var Haftday = $("#Haftday").val();
		window.location=link+"group_supplier?supplier="+SupplierID+"&haft="+Haftday;
	}

	function send_to_supplier(ob){
        var SendInfo = [];
        var state=0;
		$(".checkedorder").each(function(){
			if(this.checked==true){
                $(ob).addClass('saving');
                var ProductsID = $(this).attr('data-products');
                var ShipmentID = $(this).attr('data-shipment');
                var OrderID = $(this).attr('data-order');
                var RootPrice = $(this).attr('data-price');
                var Amountncc = parseFloat($(this).parent('td').parent('tr').find('.amountncc').val());
                var Amountucc = parseFloat($(this).parent('td').parent('tr').find('.amountucc').val());
                var Branch = $(this).parent('td').parent('tr').find('.branch').val();
                if(Branch>0){
                    if(Amountncc>0 || Amountucc>0){
                        $(this).parent('td').parent('tr').fadeOut();
                        var row = {
                            ProductsID: ProductsID,
                            ShipmentID: ShipmentID,
                            OrderID: OrderID,
                            AmountNcc: Amountncc,
                            AmountUcc: Amountucc,
                            Branch: Branch,
                            RootPrice: RootPrice
                        }
                        SendInfo.push(row);
                        state=1;
                    }
                }
            }
		});
        if(state==1){
            $.ajax({
                url: link+"send_to_supplier",
                dataType: "html",
                type: "POST",
                data: "data="+JSON.stringify(SendInfo),
                success: function(result){
                    $(ob).removeClass('saving');
                    if(result=='true'){
                        location.reload();
                    }else{
                        alert("Không thực thi được yêu cầu .");
                    }
                    console.log(result);
                },error:function(result){
                    console.log(result.responseText);
                }
            });
        }else{
            alert("Không đủ dữ liệu hợp lệ để gửi.");
        }
	}

    function checkfull(ob){
        if(ob.checked===true){
            $(".checkedorder").each(function(){
                $(this).prop("checked",true);
            });
        }else{
            $(".checkedorder").each(function(){
                $(this).prop("checked",false);
            });
        }
    }

    var sitebar_open = "true";
    $("#closesitebar").click(function(){
        sitebar_open = "false";
        $(".sitebar").addClass("closesitebar");
        $(this).addClass("closesitebar");
        $("#opensitebar").addClass("opensitebar");
        $(".containner").addClass("opensitebar");
        $(".copyright").addClass("opensitebar");
        $(".warning_message").addClass("opensitebar");
    });
</script>

