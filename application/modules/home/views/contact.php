<script>
    var d = document.getElementById("menu_contact");
    d.className = d.className + "current-menu-item";
</script>
<?php 
  if($lang=="en"){
      $pagelink = $this->uri->segment(2);
  }else{
      $pagelink = $this->uri->segment(1);
  }
?>
<div class="content contact-page">
      <div class="top-slide hidden-xs">
          <?php 
          $headerimage = $this->db->query("select * from ttp_banner where PositionID=4 order by ID DESC limit 0,1")->row();
          if($headerimage){
            echo file_exists($headerimage->Thumb) ? "<img src='$headerimage->Thumb' alt='' class='img-responsive'>" : "";
          }
          ?>
      </div>
      <!-- top-slide-->
      <div class="content-box">
        <div class="container">
          <div class="row">
            <div class="contact-form col-xs-12 col-sm-8 col-lg-9">
              <?php 
              if($lang=="en"){
                  $contact = $this->db->query("select a.ID,a.Title_en as Title,a.Introtext_en as Introtext,a.Data from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 limit 0,1")->row();
              }else{
                  $contact = $this->db->query("select a.ID,a.Title,a.Introtext,a.Data from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 limit 0,1")->row();
              }
              if($contact){
                  $json = json_decode($contact->Data);
                  echo "<h3>$contact->Title</h3>$contact->Introtext";
              }
              ?>
              <form action="<?php echo base_url().'send-contact' ?>" method="post">
                  <div class="row">
                    <div class="form-group col-xs-6">
                      <input placeholder="<?php echo $lang=="en" ? NAME_EN : NAME_VI ; ?>" type="text" class="form-control" name="Name" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <input placeholder="Email" type="email" class="form-control" name="Email" required>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-xs-12">
                      <textarea placeholder="<?php echo $lang=="en" ? NOIDUNG_EN : NOIDUNG_VI ; ?>" rows="10" class="form-control" name="Note"></textarea>
                    </div>
                    <div class="form-group col-xs-12">
                      <button type="submit" class="btn btn-primary pull-right">Gửi tin</button>
                    </div>
                  </div>
              </form>
            </div>
            <div class="contact-info col-xs-12 col-sm-4 col-lg-3">
              <div class="row">
                <div class="col-xs-6 col-sm-12">
                  <h3><?php echo $lang=="en" ? INFO_EN : INFO_VI ; ?></h3>
                  <p><i class="fa fa-location-arrow"></i><span><?php echo isset($json->address) ? $json->address : "" ; ?></span></p>
                  <p><i class="fa fa-envelope"></i><span><?php echo isset($json->email) ? $json->email : "" ; ?></span></p>
                  <p><i class="fa fa-mobile-phone"></i><span><?php echo isset($json->hotline) ? $json->hotline : "" ; ?></span></p>
                </div>
                <div class="col-xs-6 col-sm-12">
                  <h3><?php echo $lang=="en" ? WORKTIME_EN : WORKTIME_VI ; ?></h3>
                  <p><?php echo isset($json->time) ? $json->time : "" ; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php echo isset($json->map) ? $json->map : "" ; ?>
    </div>