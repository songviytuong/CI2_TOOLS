<script>
    var d = document.getElementById("menu_recruitment");
    d.className = d.className + "current-menu-item";
</script>
<?php 
  if($lang=="en"){
      $pagelink = $this->uri->segment(2);
  }else{
      $pagelink = $this->uri->segment(1);
  }
?>
<div class="content recruitment-page">
      <?php 
      $apply = $lang=="en" ? APPLY_EN : APPLY_VI ;
      if($lang=="en"){
          $head = $this->db->query("select a.ID,a.Title_en as Title,a.Introtext_en as Introtext,a.Data from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias_en='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"specialrecruitment\":\"true\"%'")->result();
      }else{
          $head = $this->db->query("select a.ID,a.Title,a.Introtext,a.Data from ttp_post a,ttp_pagelinks b where a.PagelinksID=b.ID and b.Alias='$pagelink' and b.Published=1 and a.Published=1 and a.Data like '%\"specialrecruitment\":\"true\"%'")->result();
      }
      if(count($head)>0){
          echo '<div class="top-media-slide">
                  <div class="container">
                    <div class="row"><div id="carousel-slider" data-ride="carousel" class="carousel slide">';
          $i=1;
          $navigation = '<ol class="carousel-indicators">';
          $list = '<div role="listbox" class="carousel-inner">';
          foreach($head as $row){
              $json = json_decode($row->Data);
              $video = isset($json->video) ? '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$json->video.'" allowfullscreen></iframe></div>' : "" ;
              $j=$i-1;
              $is_current = $i==1 ? 'active' : '' ;
              $navigation.='<li data-target="#carousel-slider" data-slide-to="'.$j.'" class="'.$is_current.'"></li>';
              $list.='<div class="item '.$is_current.'">
                <div class="meta col-md-5 col-lg-6 hidden-xs hidden-sm">
                  <h3>'.$row->Title.'</h3>
                  '.$row->Introtext.'
                  <a type="button" data-toggle="modal" data-target="#recruitment-modal" data-id="'.$row->ID.'" data-title="'.$row->Title.'" class="btn btn-default btn-lg center-block"><i class="fa fa-forward"> </i> '.$apply.'</a>
                </div>
                <div class="video-embed col-xs-12 col-md-7 col-lg-6">'.$video.'</div>
              </div>';
              $i++;
          }
          $list.='</div>';
          $navigation .= "</ol>";
          echo $navigation;
          echo $list;
          echo "</div></div></div></div>";
      }
      ?>
      <div class="content-box">
        <div class="container">
          <div class="panel">
            <ul id="recruitment-tab" class="nav nav-tabs nav-justified">
              <li class="active"><a aria-expanded="true" data-toggle="tab" href="#recruitment-position"><?php echo $lang=="en" ? POSITION_EN : POSITION_VI ; ?></a></li>
              <li><a aria-expanded="false" data-toggle="tab" href="#why-ttp"> <?php echo $lang=="en" ? WHY_EN : WHY_VI ; ?></a></li>
            </ul>
            <div id="recruitmentTabContent" class="tab-content">
              <div id="recruitment-position" class="tab-pane fade active in recruitment-position">
                <div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                  
                  <?php 
                  if($lang=="en"){
                      $block1 = $this->db->query("select a.ID,a.Title_en as Title,a.Description_en as Description,a.Data,a.Introtext_en as Introtext from ttp_post a,ttp_categories b,ttp_pagelinks c where a.CategoriesID=b.ID and b.PagelinksID=c.ID and a.Published=1 and b.Published=1 and c.Published=1 and c.Alias_en='$pagelink' and b.Data like '%\"block\":\"block1\"%' order by a.ID DESC")->result();
                  }else{
                      $block1 = $this->db->query("select a.ID,a.Title,a.Description,a.Data,a.Introtext from ttp_post a,ttp_categories b,ttp_pagelinks c where a.CategoriesID=b.ID and b.PagelinksID=c.ID and a.Published=1 and b.Published=1 and c.Published=1 and c.Alias='$pagelink' and b.Data like '%\"block\":\"block1\"%' order by a.ID DESC")->result();
                  }
                  $xemchitiet = $lang=="en" ? VIEWDETAILS_EN : VIEWDETAILS_VI ;
                  $death = $lang=="en" ? DEATHLINE_EN : DEATHLINE_VI ;
                  $applydes = $lang=="en" ? APPLYDES_EN : APPLYDES_VI ;
                  if(count($block1)>0){
                      $i=1;
                      foreach ($block1 as $row) {
                          $json = json_decode($row->Data);
                          $deathline = isset($json->deathline) ? $json->deathline : "" ;
                          if($lang=="vi"){
                            $worktime = isset($json->worktime) ? $json->worktime : "" ;
                            $area = isset($json->area) ? $json->area : "" ;
                            $position = isset($json->position) ? $json->position : "" ;
                          }else{
                            $worktime = isset($json->worktime_en) ? $json->worktime_en : "" ;
                            $area = isset($json->area) ? $json->area : "" ;
                            $position = isset($json->position_en) ? $json->position_en : "" ;
                          }
                          echo '<div class="panel panel-default">
                                  <div id="heading'.$i.'" role="tab" class="panel-heading no-gutters">
                                    <h4 class="col-xs-12 col-sm-6">'.$row->Title.'<span>'.$position.'</span></h4>
                                    <p class="col-xs-12 col-sm-2 hidden-xs">'.$worktime.'</p>
                                    <p class="col-xs-12 col-sm-2">'.$area.'</p><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'.$i.'" aria-expanded="true" aria-controls="collapse'.$i.'" class="btn btn-default btn-line">'.$xemchitiet.'</a>
                                  </div>
                                  <div id="collapse'.$i.'" role="tabpanel" aria-labelledby="heading'.$i.'" class="panel-collapse collapse">
                                    <div class="panel-body">
                                      '.$row->Introtext.'
                                      <div class="well no-gutters">
                                        <div class="col-xs-12 col-sm-8">
                                          <h5><i class="fa fa-clock-o"></i> '.$death.$deathline.'</h5>
                                          <p>'.$applydes.'</p>
                                        </div><a type="button" data-toggle="modal" data-target="#recruitment-modal" class="btn btn-default btn-lg col-xs-12 col-sm-4 pull-right" data-id="'.$row->ID.'" data-title="'.$row->Title.'"><i class="fa fa-forward"> </i> '.$apply.'</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>';
                        $i++;
                      }
                  }
                  ?>
                  
                </div>
              </div>
              <div id="why-ttp" class="tab-pane fade why-ttp">
                <?php 
                if($lang=="en"){
                    $block2 = $this->db->query("select a.Title_en as Title,a.Description_en as Description,a.Data,a.Thumb,a.Introtext_en as Introtext from ttp_post a,ttp_categories b,ttp_pagelinks c where a.CategoriesID=b.ID and b.PagelinksID=c.ID and a.Published=1 and b.Published=1 and c.Published=1 and c.Alias_en='$pagelink' and a.Thumb!='' and b.Data like '%\"block\":\"block2\"%'")->result();
                }else{
                    $block2 = $this->db->query("select a.Title,a.Description,a.Data,a.Thumb,a.Introtext from ttp_post a,ttp_categories b,ttp_pagelinks c where a.CategoriesID=b.ID and b.PagelinksID=c.ID and a.Published=1 and b.Published=1 and c.Published=1 and c.Alias='$pagelink' and a.Thumb!='' and b.Data like '%\"block\":\"block2\"%'")->result();
                }
                if(count($block2)>0){
                    foreach ($block2 as $row) {
                        echo $row->Introtext;
                    }
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php 
        if($lang=="en"){
            $footerimage = $this->db->query("select Data_en as Data,Thumb from ttp_banner where PositionID=6 order by ID DESC")->row();
        }else{
            $footerimage = $this->db->query("select * from ttp_banner where PositionID=6 order by ID DESC")->row();
        }
        
        if($footerimage){
            $image = file_exists($footerimage->Thumb) ? "url('$footerimage->Thumb') top center no-repeat" : "";
            $json = json_decode($footerimage->Data);
            $description = isset($json->Description) ? $json->Description : "" ;
            echo '<div class="bottom-quote" style="background:'.$image.'">
                      <div class="container">
                          <div class="meta col-xs-8 col-sm-6 col-md-4 center-block">
                          '.$description.'
                        </div>
                      </div>
                  </div>';
            
        }
      ?>
      <div id="recruitment-modal" tabindex="-1" role="dialog" aria-labelledby="recruitment-modal-Label" class="modal fade">
        <form action="<?php echo base_url().'ung-tuyen' ?>" method="post" enctype="multipart/form-data">
          <div role="document" class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                <h4 id="recruitment-modal-Label" class="modal-title">Ứng tuyển trực tiếp</h4>
              </div>
              <div class="modal-body">
                <p><?php echo $lang=="en" ? FORMDES_EN : FORMDES_VI ; ?></p>
                <div class="row">
                  <div class="col-xs-6">
                    <label><?php echo $lang=="en" ? NAME_EN : NAME_VI ; ?>:</label>
                    <input placeholder="<?php echo $lang=="en" ? NAME_EN : NAME_VI ; ?>" type="text" class="form-control" name="Name" required>
                  </div>
                  <div class="col-xs-6">
                    <label>Email:</label>
                    <input placeholder="Email" type="email" class="form-control" name="Email" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <label><?php echo $lang=="en" ? NOIDUNG_EN : NOIDUNG_VI ; ?>:</label>
                    <textarea placeholder="<?php echo $lang=="en" ? NOIDUNG_EN : NOIDUNG_VI ; ?>" rows="3" class="form-control" name="Ghichu"></textarea>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <label><?php echo $lang=="en" ? ATTACH_EN : ATTACH_VI ; ?>: </label>
                    <div class="clearfix"></div><a target="_blank" href="<?php echo base_url().'public/file/Thongtinungvien.doc' ?>" class="pull-right"><i class="fa fa-download"> </i> <?php echo $lang=="en" ? DOWNLOAD_EN : DOWNLOAD_VI ; ?></a>
                    <input type="file" name="Images_upload[]" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple />
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" name="PostID" id="PostID" />
                <button type="submit" class="btn btn-primary"><?php echo $lang=="en" ? APPLYO_EN : APPLYO_VI ; ?>				</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>