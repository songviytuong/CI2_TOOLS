/*---------------------------------------------------------
  Configuration
---------------------------------------------------------*/

// Set this to the server side language you wish to use.
var lang = 'php'; // options: lasso, php, py

// Set this to the directory you wish to manage.

// var site_url = window.location;
var current_url = String(window.location.hostname);
var productionURL = ""; // no 'http://'
var developmentURL = "http://tools.ucc.dev";
var uploadFolder = productionURL+"/";

// alert(current_url+'/assets/');

// Deployment specific path issues... a little annoying to deal with.
var fileRoot = '/assets/';
if(current_url == 'http://tools.ucc.dev') {
	fileRoot = '/assets/';	
}
if(current_url == productionURL) {
	fileRoot = 'assets/';	
}
// var fileRoot = BASE_HREF;

// Show image previews in grid views?
var showThumbs = true;
