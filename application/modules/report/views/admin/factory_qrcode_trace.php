<?php 
$arr = array(0=>'Chưa sử dụng',1=>'<span class="text-danger">Đã gán thùng</span>',2=>'<span class="text-danger">Đang lưu thông</span>',3=>'<span class="text-danger">Hàng trả về</span>',4=>'<span class="text-danger">Hàng quét lẻ</span>');
?>
<div class="row">
	<div class="col-xs-2">
		<img class="img-responsive pull-right" src='<?php echo base_url().ADMINPATH."/report/factory/qrcode_generator/$data->ID" ?>' />
	</div>
	<div class="col-xs-5">
		<h4 class="text-success"><?php echo "Thông tin mã QR Code" ?></h4>
		<p><?php echo "Nội dung mã : ".$data->Code ?></p>
		<p><?php echo "Mã SKU : ".$data->MaSP; ?></p>
		<p><?php echo "Lô hàng : ".$data->ShipmentCode; ?></p>
	</div>
	<div class="col-xs-5">
		<h4><?php echo $arr[$data->Status] ?></h4>
		<?php 
		if($data->Status==1){
			$barcode = $this->db->query("select b.Code,d.MaKho from ttp_report_factory_qrcode a,ttp_report_factory_barcode b ,ttp_report_warehouse d where a.WarehouseID=d.ID and a.BarcodeID=b.ID and a.ID=$data->ID")->row();
			echo $barcode ? "<p>BARCODE thùng : $barcode->Code</p>" : "";
			echo $barcode ? "<p>Vị trí hiện tại : kho $barcode->MaKho</p>" : "";
		}
		if($data->Status==2){
			$order = $this->db->query("select b.MaDH,c.Name,b.ID,b.CustomerID from ttp_report_factory_qrcode a,ttp_report_order b,ttp_report_customer c where a.OrderID=b.ID and b.CustomerID=c.ID and a.ID=$data->ID")->row();
			echo $order ? "<p>Xuất cho đơn hàng : <a target='_blank' href='".base_url().ADMINPATH."/report/import_order/preview/$order->ID'>$order->MaDH</a></p>" : "";
			echo $order ? "<p>Người mua hàng : $order->Name <a onclick='showhistory(this,$order->CustomerID)' style='margin-left:5px'><i class='fa fa-angle-double-down fa_dropdown'></i></a></p>" : "";
		}
		if($data->Status==3){
			$warehouse = $this->db->query("select d.MaKho from ttp_report_factory_qrcode a,ttp_report_warehouse d where a.WarehouseID=d.ID and a.ID=$data->ID")->row();
			echo $warehouse ? "<p>Vị trí hiện tại : kho $warehouse->MaKho</p>" : "";
		}
		if($data->Status==4){
			$warehouse = $this->db->query("select d.MaKho from ttp_report_factory_qrcode a,ttp_report_warehouse d where a.WarehouseID=d.ID and a.ID=$data->ID")->row();
			echo $warehouse ? "<p>Vị trí hiện tại : kho $warehouse->MaKho</p>" : "";
		}
		?>
	</div>
</div>
<div class='row' style='display:none;margin:10px 0px' id='historybox'></div>
<div class="row" style="border-top:1px solid #888"></div>
<div class="row" style="position: relative">
	<div id="trace_progress"></div>
	<?php 
	$trace = $this->db->query("select a.*,b.UserName from ttp_report_factory_qrcode_trace a,ttp_user b where a.QRID=$data->ID and a.UserID=b.ID")->result();
	if(count($trace)>0){
		$position = 1;
		foreach($trace as $row){
			if($position%2==1){
				echo '
				<div class="col-xs-12">
					<div class="col-xs-5 item_trace item_trace_left">
						<p class="item_trace_title">'.$row->Note.'</p>
						<p class="item_trace_name">'.$row->UserName.' - <i>'.$row->Created.'</i></p>
					</div>
					<div class="col-xs-2 item_trace">
						<p class="item_trace_point point_'.$position.'">'.$position.'</p>
					</div>
					<div class="col-xs-5 item_trace"></div>
				</div>';
			}else{
				echo '
				<div class="col-xs-12">
					<div class="col-xs-5 item_trace"></div>
					<div class="col-xs-2 item_trace">
						<p class="item_trace_point point_'.$position.'">'.$position.'</p>
					</div>
					<div class="col-xs-5 item_trace item_trace_right">
						<p class="item_trace_title">'.$row->Note.'</p>
						<p class="item_trace_name">'.$row->UserName.' - <i>'.$row->Created.'</i></p>
					</div>
				</div>';
			}
			$position++;
		}
	}
	?>
</div>
<script>
	var point = 1;
	var boxheight = <?php echo ($position-1)*110; ?>;
	
	var mytrace = setInterval(function(){
		position=parseInt(boxheight-(110*point) - 10);
		console.log(position+"<"+boxheight+" .point_"+point);
		$(".point_"+point).addClass("active");
		if(position>=100 && boxheight>=220){
			$("#trace_progress").css({"bottom":position+"px"});
		}else{
			$(".point_"+point).addClass("last");
			stopTrace();
		}
		point = point+1;
	},800);


	function stopTrace() {
	    clearInterval(mytrace);
	}

	function showhistory(ob,id){
		$("#historybox").load("<?php echo base_url().ADMINPATH.'/report/factory/loadhistorybox/' ?>"+id);
		$("#historybox").toggle();
	}
</script>