<div class="containner">
	<div class="bill_print export_internal">
		<div class="block1">
			<table id="table1">
				<tr>
					<td rowspan="3"><img src='public/admin/images/logo.png' /></td>
					<td rowspan="3"><h1>PHIẾU NHẬP KHO</h1></td>
					<td>Mã: TP.FA.DI.15.FM04</td>
				</tr>
				<tr>
					<td>PB: 00</td>
				</tr>
				<tr>
					<td>NBH: 01/12/2015</td>
				</tr>
			</table>
			<div class="block1_2">
				<?php 
				$export = $this->db->query("select * from ttp_report_import_warehouse where ImportID=$data->ID")->row();
				?>
				<a style="left:5px;top:40px" id="back_page" href='<?php echo base_url().ADMINPATH."/report/warehouse_scancode/import_production" ?>'><i class="fa fa-undo"></i> Quay lại</a>
				<?php 
				$thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_import_warehouse where MONTH(Ngaynhapkho)=$thismonth and YEAR(Ngaynhapkho)=$thisyear and DATE(Ngaynhapkho)>='2016-04-21' and DepartmentID=$data->DepartmentID")->row();
				$max = $max ? $max->max + 1 : 1 ;
				$thisyear = date('Y',time());
				$Department = $this->db->query("select Code from ttp_department where ID=$data->DepartmentID")->row();
				$hinhthucnhapkho = $Department ? $Department->Code : 'LO' ;
				$NK = $data->Type==2 ? "NKK" : "NK" ;
                $max = $NK.$hinhthucnhapkho.$thisyear.$thismonth.'.'.str_pad($max, 5, '0', STR_PAD_LEFT);
				?>
				<p>Ngày <?php echo date("d",time()); ?> Tháng <?php echo date("m",time()); ?> Năm <?php echo date("Y",time()); ?></p>
				<p>Số : <span id="next_MaXK"><?php echo $export ? $export->MaNK : $max ; ?></span></p>
			</div>
			<div class="block1_3">
				<table>
					<tr>
						<td>TK Nợ:</td>
						<td><input type="text" id="TKNO" value="<?php echo $export ? $export->TKNO : "............" ; ?>" /></td>
					</tr>
					<tr>
						<td>TK Có:</td>
						<td><input type="text" id="TKCO" value="<?php echo $export ? $export->TKCO : "............" ; ?>" /></td>
					</tr>
				</table>
				<input type="hidden" id="KPP" value="LO" />
			</div>
		</div>

		<div class="block2">
			<?php 
			if($data->Type==0 || $data->Type==2){
				$PO = $this->db->query("select POCode,b.Title,c.Address from ttp_report_perchaseorder a,ttp_report_production b,ttp_report_warehouse c where c.ID=a.WarehouseID and a.ProductionID=b.ID and a.ID=$data->POID")->row();
				$code = $PO ? $PO->POCode : "--" ;
			}elseif($data->Type==1){
				$PO = $this->db->query("select a.MaXK,b.Address,a.Ngayxuatkho from ttp_report_export_warehouse a,ttp_report_warehouse b where b.ID=a.KhoID and a.ID=$data->ExportID")->row();
				$code = $PO ? $PO->MaXK : "--" ;
			}
			?>
			<div class="row">
				<li>Họ và tên người giao:............................................................................................................................................................</li>
			</div>
			<div class="row">
				<li>Tên bộ phận / Nhà cung cấp </li>
				<li style="font-weight:normal"> : <?php echo $data->Type==0 ? $PO->Title : '.................................................................................................................................................' ; ?></li>
			</div>
			<div class="row">
				<li>Theo hợp đồng / PO / Phiếu yêu cầu nhập kho số  </li>
				<li> : <?php echo $code ?> - Ngày : <?php echo date("d/m/Y",strtotime($data->NgayNK)) ?></li>
			</div>
			<div class="row">
				<li>Nhập hàng tại kho</li>
				<li>: 
					<?php echo $data->KhoTitle ?>
					<input type="hidden" id="KhoID" value="<?php echo $data->KhoID ?>" />
				</li>
			</div>
			<div class="row">
				<li>Địa điểm </li>
				<li class="special">: <?php echo $PO ? $PO->Address : '--' ; ?></li>
			</div>
			<div class="row">
				<li>Diễn giải</li>
				<li class="special">: <?php echo $data->Note ?></li>
			</div>
		</div>

		<div class="block3">
			<table>
				<tr>
					<th rowspan='2'>Số TT</th>
					<th rowspan='2'>Tên, nhãn hiệu, quy cách phẩm chất vật tư, dụng cụ sản phẩm hàng hóa</th>
					<th rowspan='2'>Mã số</th>
					<th rowspan='2'>Số lô</th>
					<th rowspan='2'>Đơn vị tính</th>
					<th colspan='2'>Số lượng</th>
					<th rowspan='2'>Đơn giá</th>
					<th rowspan='2'>Thành tiền</th>
				</tr>
				<tr>
					<?php 
					echo $data->Type==0 ? '<th style="width:110px">Yêu cầu nhập</th>' : '<th style="width:110px">Theo chứng từ</th>' ;
					?>
					<th style="width:110px">Thực nhập</th>
				</tr>
				<?php 
				$details = $this->db->query("select a.Title,a.MaSP,a.Donvi,b.*,c.ShipmentCode from ttp_report_products a,ttp_report_inventory_import_details b,ttp_report_shipment c where b.ShipmentID=c.ID and a.ID=b.ProductsID and b.ImportID=$data->ID")->result();
				$i=1;
				if(count($details)>0){
					foreach($details as $row){
						$dongiaVND = $row->PriceCurrency*$row->ValueCurrency;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td>$row->Title</td>";
						echo "<td>$row->MaSP</td>";
						echo "<td>$row->ShipmentCode</td>";
						echo "<td>$row->Donvi</td>";
						if($data->Type==0){
							echo "<td style='text-align:right'>".number_format($row->Request)."</td>";
						}else{
							echo "<td style='text-align:right'>".number_format($row->Amount)."</td>";
						}
						echo "<td style='text-align:right'>".number_format($row->Amount)."</td>";
						echo "<td>.....................</td>";
						echo "<td>.....................</td>";
						echo "</tr>";
						$i++;
					}
				}
				$k = $i<10 ? 10-$i : 0 ;
				for ($i=0; $i < $k; $i++) {
					echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
				}

				?>
				
				<tr>
					<td></td>
					<td>TỔNG CỘNG SỐ LƯỢNG</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style='text-align:right'>
					<?php echo number_format($data->TotalAmount) ?>
					</td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="block5">
			<p class="title">- Tổng số tiền viết bằng chữ : </p>
			<p class="title">- Số chứng từ gốc kèm theo : </p>
			<div>
				<p><span>1. Hợp đồng kinh tế số </span>: .............................................................................................</p>
				<p><span>2. PO số </span>: .............................................................................................</p>
				<p><span>3. Biên bản giao nhận số </span>: .............................................................................................</p>
				<p><span>4. Biên bản kiểm tra CLSP số </span>: .............................................................................................</p>
				<p><span>5. Hóa đơn số </span>: .............................................................................................</p>
				<p><span>6. Phiếu yêu cầu nhập kho số </span>: .............................................................................................</p>
				<p><span>7. Chứng từ khác số </span>: .............................................................................................</p>
			</div>
		</div>
		<div class="block4">
			<div>
				<p>Người lập phiếu</p>
				<p>(Ký, họ tên)</p>
				<div></div>
				<p><?php echo date("H:i A d/m/Y",time()) ?></p>
				<p>In từ tools.trantoanphat.com</p>
			</div>
			<div>
				<p>Người nhận hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Người giao hàng</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Trưởng phòng ban</p>
				<p>(Ký, họ tên)</p>
			</div>
			<div>
				<p>Kế toán trưởng</p>
				<p>(Ký, họ tên)</p>
			</div>
		</div>
	</div>
</div>