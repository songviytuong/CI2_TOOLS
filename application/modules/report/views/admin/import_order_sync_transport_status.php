<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="gridSystemModalLabel">Trạng thái đơn hàng từ đơn vị vận chuyển</h4>
</div>
<div class="modal-body">
    <div class="row">
        <form id="submit_order">
            <div class="col-md-4 col-md-offset-4">
                <p>Chọn đơn vị vận chuyển</p>
                <select class="form-control" name="TransportID">
                    <?php 
                    if(count($Transport)>0){
                      foreach($Transport as $row){
                        echo "<option value='$row->ID'>$row->Title</option>";
                      }
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4 col-md-offset-4" style="margin-top:20px;">
              <button type="button" class="btn btn-primary" onclick="new ERPPlugin(this).submitform_previewdata()" erp-url-excute="<?php echo base_url().ADMINPATH.'/report/import_order/sync_status_from_transport' ?>" erp-object-excute="#submit_order" erp-object-message="#box-warning" erp-object-preview="#submit_order">Thu thập trạng thái</button>
            </div>
        </form>
    </div>
</div>