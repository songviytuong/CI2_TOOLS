<?php 
class Manager_customer extends Admin_Controller { 
 
    public $limit = 30;
    public $user;
    public $classname="manager_customer";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/manager_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $bonus = "";
        $search = $this->session->userdata("report_filter_customer_Search");
        $bonus = $search!='' ? " where Phone1 like '%$search%' or Name like '%$search%'" : '' ;
        $nav = $this->db->query("select count(1) as nav from ttp_report_customer $bonus")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_customer $bonus order by ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_customer/',
            'data'      =>  $object,
            'start'     =>  $start,
            'find'      =>  $nav,
            'nav'       =>  $this->lib->nav(base_url().ADMINPATH.'/report/manager_customer/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Customer | Manager Report Tools');
        $this->template->write_view('content','admin/manager_customer_home',$data);
        $this->template->render();
    }

    public function setsessionsearch(){
        if(isset($_POST['Search'])){
            $Search = mysql_real_escape_string($_POST['Search']);
            $this->session->set_userdata("report_filter_customer_Search",$Search);
        }
        redirect(ADMINPATH."/report/manager_customer");
    }

    public function clearfilter(){
        $this->session->unset_userdata("report_filter_customer_Search");
        redirect(ADMINPATH."/report/manager_customer");
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Customer add | Manager report Tools');
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/manager_customer/'
        );
        $this->template->write_view('content','admin/manager_customer_add',$data);
        $this->template->render();
    }

    public function add_new(){
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
        $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
        $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $Age = isset($_POST['Age']) ? $_POST['Age'] : '' ;
        $Birthday = isset($_POST['Birthday']) ? $_POST['Birthday'] : '' ;
        $Birthday = $Birthday!='' ? explode('/',$Birthday) : '' ;
        if(count($Birthday)==3){
            $Birthday = $Birthday[2].'-'.$Birthday[1].'-'.$Birthday[0];
        }
        if($Name!='' && $Phone1!='' && $Address!=''){
            $check = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
            if(!$check){
                $data = array(
                    'Name'          => $Name,
                    'Phone1'        => $Phone1,
                    'Phone2'        => $Phone2,
                    'Address'       => $Address,
                    'Age'           => $Age,
                    'Birthday'      => $Birthday
                );
                $this->db->insert("ttp_report_customer",$data);
            }else{
                echo "<script>alert('Số điện thoại này đã có người sử dụng !')</script>";
                echo "<script>window.location='".base_url().ADMINPATH."/report/manager_customer/add'</script>";
                return;    
            }
        }
        redirect(ADMINPATH.'/report/manager_customer/');
    }

    public function delete($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $this->db->query("delete from ttp_report_customer where ID=$id");
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
        redirect($return);
    }

    public function edit($id=0){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        if(is_numeric($id) && $id>0){
            $result = $this->db->query("select * from ttp_report_customer where ID=$id")->row();
            if(!$result) return;
            $this->template->add_title('Edit Customer | Manager report Tools');
            $data = array(
                'base_link' =>  base_url().ADMINPATH.'/report/manager_customer/',
                'data'      =>  $result
            );
            $this->template->write_view('content','admin/manager_customer_edit',$data);
            $this->template->render();
        }
    }
    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
        $Name = isset($_POST['Name']) ? $_POST['Name'] : '' ;
        $Phone1 = isset($_POST['Phone1']) ? $_POST['Phone1'] : '' ;
        $Phone2 = isset($_POST['Phone2']) ? $_POST['Phone2'] : '' ;
        $Address = isset($_POST['Address']) ? $_POST['Address'] : '' ;
        $Age = isset($_POST['Age']) ? $_POST['Age'] : '' ;
        $Birthday = isset($_POST['Birthday']) ? $_POST['Birthday'] : '' ;
        $Birthday = $Birthday!='' ? explode('/',$Birthday) : '' ;
        if(count($Birthday)==3){
            $Birthday = $Birthday[2].'-'.$Birthday[1].'-'.$Birthday[0];
        }
        if($Name!='' && $Phone1!='' && $Address!=''){
            $data = array(
                'Name'          => $Name,
                'Phone1'        => $Phone1,
                'Phone2'        => $Phone2,
                'Address'       => $Address,
                'Age'           => $Age,
                'Birthday'      => $Birthday
            );
            $this->db->where("ID",$ID);
            $this->db->update("ttp_report_customer",$data);
        
        }
        redirect(ADMINPATH.'/report/manager_customer/');
    }
}
?>
