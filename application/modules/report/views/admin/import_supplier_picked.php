<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
	<div class="import_select_progress">
        <div class="block1">
            <h1>Danh sách hàng đã giao cho vận chuyển</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom:10px">
    	<div class="btn-group" style="margin-left:15px;margin-right:8px">
        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Nhà cung cấp / đối tác</a>
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
            <ul class="dropdown-menu">
            	<li <?php echo $SupplierID==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,0,<?php echo $BranchID ?>)'>Tất cả nhà cung cấp</a></li>
				<?php 
				if($this->user->IsAdmin==1){
					$supplier_list = $this->db->query("select b.* from ttp_report_production b")->result();
				}else{
					$supplier_list = $this->db->query("select b.* from ttp_user_supplier a,ttp_report_production b where a.SupplierID=b.ID and a.UserID=".$this->user->ID)->result();
				}
				if(count($supplier_list)>0){
					foreach($supplier_list as $row){
						$style = $row->ID==$SupplierID ? "style='background:#c3e2ff;'" : '' ;
						echo "<li $style><a onclick='changefillter(this,$row->ID,$BranchID)'>$row->Title</a></li>";
					}
				}
				?>
	        </ul>
        </div>
        <div class="btn-group" style="margin-left:15px;margin-right:8px">
        	<a class="btn btn-default"><i class="fa fa-filter" aria-hidden="true"></i> Chi nhánh / cửa hàng</a>
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></a>
            <ul class="dropdown-menu">
            	<li <?php echo $BranchID==0 ? "style='background:#c3e2ff;'" : '' ; ?>><a onclick='changefillter(this,<?php echo $SupplierID ?>,0)'>Tất cả chi nhánh</a></li>
				<?php 
				if($this->user->IsAdmin==1){
					$branch_list = $this->db->query("select c.* from ttp_report_branch c")->result();
				}else{
					$branch_list = $this->db->query("select c.* from ttp_user_supplier a,ttp_report_production b,ttp_report_branch c where b.ID=c.SupplierID and a.SupplierID=b.ID and a.UserID=".$this->user->ID)->result();
				}
				if(count($branch_list)>0){
					foreach($branch_list as $row){
						$style = $row->ID==$BranchID ? "style='background:#c3e2ff;'" : '' ;
						echo "<li $style><a onclick='changefillter(this,$row->SupplierID,$row->ID)'>$row->Title</a></li>";
					}
				}
				?>
	        </ul>
        </div>
    </div>
    <div class="row" style="margin:10px 0px">
    	<?php 
    	$user_trans = $this->db->query("select ID,Thumb,FirstName,LastName from ttp_user where UserType=15")->result();
    	$arr_user = array();
    	if(count($user_trans)>0){
    		foreach($user_trans as $row){
    			$arr_user[$row->ID]['Thumb'] = $this->lib->get_thumb($row->Thumb);
    			$arr_user[$row->ID]['Name'] = $row->FirstName.' '.$row->LastName;
    		}
    	}
    	?>
    	<table class="table table-bordered">
    		<tr style='background:#f1f1f1'>
    			<th>STT</th>
    			<th>Đơn hàng</th>
    			<th>Mã s.phẩm</th>
    			<th>Tên s.phẩm</th>
    			<th>ĐV</th>
    			<th>Số lượng</th>
    			<th>Địa điểm</th>
    			<th>Người nhận hàng</th>
                <th>Thời gian</th>
    		</tr>
    		<?php 
    		if(count($data)>0){
    			$i=1;
    			foreach($data as $row){
    				echo "<tr>";
    				echo "<td>$i</td>";
    				echo "<td>$row->MaDH</td>";
    				echo "<td>$row->BarcodeClient</td>";
    				echo "<td>$row->Title</td>";
    				echo "<td>$row->Donvi</td>";
    				echo "<td>$row->Amount</td>";
    				echo "<td>$row->BranchTitle</td>";
    				echo !isset($arr_user[$row->UserReciver]) ? "<td>chưa chọn người</td>" : "<td><a onclick='viewtransporter($row->UserReciver)'><img style='width:20px;height:20px;border-radius:50%;margin-right:10px;' src='".$arr_user[$row->UserReciver]['Thumb']."' />".$arr_user[$row->UserReciver]['Name']."</a></td>";
                    echo "<td>".date('d/m/Y H:i',strtotime($row->TimeReciver))."</td>";
    				echo "</tr>";
    				$i++;
    			}
    		}else{
    			echo "<tr><td colspan='9'>Không tìm thấy dữ liệu</td></tr>";
    		}
    		?>
    	</table>
        <?php 
            $httpbuild = http_build_query($_GET);
            echo str_replace('href=','onclick="nav(this)" data-get="?'.$httpbuild.'" data=',$nav);
        ?>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var link = "<?php echo $base_link ?>";

	function changefillter(ob,SupplierID,BranchID){
		window.location = link+"picked?supplier="+SupplierID+"&branch="+BranchID;
	}

    function viewtransporter(ID){
    	$(".over_lay").addClass("black");
    	var baselink = $("#baselink_report").val();
	    $.ajax({
	        url: baselink+"viewtransport",
	        dataType: "html",
	        type: "POST",
	        data: "ID="+ID,
	        success: function(result){
	            enablescrollsetup();
	            $(".over_lay").html(result);
	        	$(".over_lay").removeClass('in');
		    	$(".over_lay").fadeIn('fast');
				$(".over_lay").addClass('in');
	        },error:function(result){
	            console.log(result);
	        }
	    });
    }

    function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-200;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	
</script>