<?php 
class Gallery_images extends Admin_Controller { 
	public $user;
	public $classname="gallery_images";
	public $limit = 96;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$page = $this->uri->segment(5);
		$start = is_numeric($page) ? $page : 0 ;
        if(!is_numeric($start)) $start=0;
        $this->template->add_title('Images list | Gallery');
		$limit_str = "limit $start,$this->limit";
		$nav = $this->db->query("select count(1) as nav from ttp_images")->row();
        $nav = $nav ? $nav->nav : 0 ;
		$object = $this->db->query("select * from ttp_images order by ID DESC $limit_str")->result();
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/gallery_images/',
			'data'		=>	$object,
			'start'		=>	$start,
			'nav'		=>	$this->lib->nav(base_url().ADMINPATH.'/home/gallery_images/index',5,$nav,$this->limit)
		);
		$this->template->write_view('content','admin/gallery_images_home',$data); 
		$this->template->render();
	}

	public function delete($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'d',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$images = $this->db->query("select Thumb from ttp_images where ID=$id")->row();
			if($images) $this->delete_file_path($images->Thumb);
			$this->db->query("delete from ttp_images where ID=$id");
			$this->db->query("update ttp_album set Data=REPLACE(Data,'\"$id\"','\"0\"') where Data like '%\"$id\"%'");
		}
		$return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url().ADMINPATH;
		redirect($return);
	}
	

	public function add(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$this->template->add_title('Add Images | Gallery');
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/gallery_images/'
		);
		$this->template->write_view('content','admin/gallery_images_add',$data);
		$this->template->render();
	}

	public function addnew(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
		$Folder = isset($_POST['Folder']) ? $_POST['Folder'] : '' ;
		$Folder = trim($Folder);
		$Folder = $this->lib->alias($Folder);
		if($Folder!=''){
			if(isset($_FILES['Images_upload'])){
				if(count($_FILES['Images_upload']['name'])>0){
					$this->upload_to = "album_thumb/$Folder";
					$reciver = $this->UploadMultiImages();
					if(isset($reciver['Thumb'])){
						foreach($reciver['Thumb'] as $item){
							$cropimage = explode(",",IMAGECROP);
							foreach($cropimage as $row){
								$size = explode("x", $row);
								$width = isset($size[0]) ? (int)$size[0] : 0 ;
								$height = isset($size[1]) ? (int)$size[1] : 0 ;
								if($width>0 && $height>0) $this->lib->cropimage($item,$width,$height);
							}
						}
					}
				}
			}
		}
		redirect(ADMINPATH.'/home/gallery_images/');
	}

	public function edit($id=0){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(is_numeric($id) && $id>0){
			$result = $this->db->query("select * from ttp_images where ID=$id")->row();
			if(!$result) return;
			$this->template->add_title('Edit Images | Gallery');
			$data = array(
				'base_link' =>	base_url().ADMINPATH.'/home/gallery_images/',
				'data'		=>	$result
			);
			$this->template->write_view('content','admin/gallery_images_edit',$data);
			$this->template->render();
		}
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		$ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '' ;
		$Name = isset($_POST['Name']) ? $this->security->xss_clean($_POST['Name']) : '' ;
		if($Name!=''){	
			$images = $this->db->query("select * from ttp_images where ID=$ID")->row();
			$path = explode("/",str_replace("assets/","",$images->Thumb));
			if(count($path)>0){
				unset($path[count($path)-1]);
			}
			$path = count($path)>0 ? implode('/',$path) : '' ;
			if($images){
				$data = array(
					'Name'			=> $Name,
					'LastEdited'	=> date("Y-m-d H:i:s")
				);
				if(isset($_FILES['Image_upload'])){
					if($_FILES['Image_upload']['tmp_name']!=''){
						$this->upload_to = $path;
						echo $this->upload_to;
						$data['Thumb'] = $this->upload_image_single();
						$cropimage = explode(",",IMAGECROP);
						foreach($cropimage as $row){
							$size = explode("x", $row);
							$width = isset($size[0]) ? (int)$size[0] : 0 ;
							$height = isset($size[1]) ? (int)$size[1] : 0 ;
							if($width>0 && $height>0) $this->lib->cropimage($data['Thumb'],$width,$height);
						}
						$this->delete_file_path($images->Thumb);
					}
				}
				$this->db->where("ID",$ID);
				$this->db->update("ttp_images",$data);
			}
		}
		redirect(ADMINPATH.'/home/gallery_images/');
	}

	public function remove($album=0,$id=0){
		if(is_numeric($album) && is_numeric($id)){
			if($album>0 && $id>0){
				$result = $this->db->query("select * from ttp_images where ID=$album")->row();
				if($result){
					$data = json_decode($result->Data,true);
					if(($key = array_search($id, $data)) !== false) {
					    unset($data[$key]);
					}
					$data = json_encode($data);
					$this->db->query("update ttp_images set Data='$data' where ID=$album");
				}
			}
		}
		redirect(ADMINPATH.'/home/gallery_images/edit/'.$album);
	}

	public function loadimage(){
		$ID = isset($_POST['ID']) ? $_POST['ID'] : 0 ;
		$result = $this->db->query("select * from ttp_images where ID=$ID")->row();
		if($result){
			if(isset($_FILES['Images_upload'])){
				if(count($_FILES['Images_upload']['name'])>0){
					$this->upload_to = "album_thumb/$ID";
					$reciver = $this->UploadMultiImages();
					if(isset($reciver['Thumb'])){
						foreach($reciver['Thumb'] as $item){
							$cropimage = explode(",",IMAGECROP);
							foreach($cropimage as $row){
								$size = explode("x", $row);
								$width = isset($size[0]) ? (int)$size[0] : 0 ;
								$height = isset($size[1]) ? (int)$size[1] : 0 ;
								if($width>0 && $height>0) $this->lib->cropimage($item,$width,$height);
							}
						}
					}
					if(isset($reciver['ID'])){
						$json = json_decode($result->Data,true);
						$json = is_array($json) ? $json : array() ;
						$merge = array_merge($json,$reciver['ID']);
						$data['Data'] = json_encode($merge);
						$this->db->where("ID",$ID);
						$this->db->update("ttp_images",$data);
						echo "true";
						return;
					}
				}
			}
		}
		echo "false";
	}

	public function move(){
		$Album = $this->uri->segment(5);
		$ID = $this->uri->segment(6);
		$current = $this->uri->segment(7);
		$Move = $this->uri->segment(8);
		if(is_numeric($Album) && $Album>0){
			$result = $this->db->query("select * from ttp_images where ID=$Album")->row();
			if($result){
				$json = json_decode($result->Data,true);
				if(isset($json[$Move])){
					$temp = $json[$Move];
					$json[$Move] = $ID;
					$json[$current] = $temp;
				}
				$data['Data'] = json_encode($json);
				$this->db->where("ID",$Album);
				$this->db->update("ttp_images",$data);
			}
		}
		$link = ADMINPATH.'/home/gallery_images/edit/'.$Album;
		redirect($link);
	}
}
?>