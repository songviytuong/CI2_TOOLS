<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
    <div class="import_select_progress">
	    <div class="block1">
	    	<h1>Tổng hợp phiếu chi</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<a href="<?php echo $base_link.'add' ?>" class="btn btn-danger"><i class="fa fa-plus"></i> Tạo phiếu chi</a>
    		<a href="<?php echo $base_link.'export' ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export excel</a>
    	</div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<table class="table table-bordered table-hovered">
    			<tr style="background:#EEE;">
    				<th>STT</th>
    				<th>Ngày chi</th>
    				<th>Loại phiếu chi</th>
    				<th>Nguồn chi</th>
    				<th>Mã chứng từ</th>
    				<th>Số tiền</th>
    				<th>Thanh toán</th>
    				<th>Quỹ / tài khoản chi</th>
    			</tr>
    			<?php 
    			$arr_cash = array();
    			$cash = $this->db->query("select * from ttp_report_finance_cash")->result();
				if(count($cash)>0){
					foreach ($cash as $row) {
						$arr_cash[$row->ID] = $row->Title;
					}
				}

				$arr_bank = array();
				$bank = $this->db->query("select * from ttp_report_finance_bankaccount")->result();
				if(count($bank)>0){
					foreach ($bank as $row) {
						$arr_bank[$row->ID] = $row->Title;
					}
				}

    			$arr_finance_type = $this->lib->get_config_define("finance_export","finance_export_type");
    			$arr_finance_source = $this->lib->get_config_define("finance_export","finance_export_source");
    			$arr_payment = array(0=>"Tiền mặt",1=>"Chuyển khoản");
    			if(count($data)>0){
    				$i=$start+1;
    				foreach($data as $row){
                        $row->Hardcode = $row->Hardcode!='' ? $row->Hardcode : '' ;
                        $row->Note = $row->Note!='' ? "($row->Note)" : '' ;
                        echo "<tr onclick='edit($row->ID)'>";
    					echo "<td>$i</td>";
    					echo "<td>".date('d/m/Y',strtotime($row->Dayfinance))."</td>";
    					echo isset($arr_finance_type[$row->TypeFinance]) ? "<td>".$arr_finance_type[$row->TypeFinance]."</td>" : "<td>--</td>";
    					echo isset($arr_finance_source[$row->SourceFinance]) ? "<td>".$arr_finance_source[$row->SourceFinance]."</td>" : "<td>--</td>";
    					echo "<td>$row->Hardcode ".$row->Note."</td>";
    					echo "<td class='text-right'>".number_format($row->Price)."</td>";
    					echo isset($arr_payment[$row->Payment]) ? "<td>".$arr_payment[$row->Payment]."</td>" : "<td>--</td>";
    					if($row->Payment==0){
    						echo isset($arr_cash[$row->CashPayment]) ? "<td>".$arr_cash[$row->CashPayment]."</td>" : "<td>--</td>";
    					}else{
    						echo isset($arr_bank[$row->AccountPayment]) ? "<td>".$arr_bank[$row->AccountPayment]."</td>" : "<td>--</td>";
    					}
    					echo "</tr>";
    					$i++;
    				}
                    echo "<tr><th colspan='5' class='text-right'>TỔNG CỘNG</th><th class='text-right'>".number_format($total)."</th><th colspan='2'></th></tr>";
    			}else{
    				echo "<tr><td colspan='8' class='text-center'>Không tìm thấy dữ liệu</td></tr>";
    			}
    			?>
    		</table>
    	</div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2016',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"import/set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                if(result=="OK"){
	                	location.reload();
	                }else{
	                	$(".over_lay").fadeOut();
	                	$(".warning_message").slideDown('slow');
	                }
	            }
	        });	
		});
    });

    function edit(id){
    	window.location = "<?php echo base_url().ADMINPATH.'/report/finance_export/edit_export/' ?>"+id;
    }
</script>