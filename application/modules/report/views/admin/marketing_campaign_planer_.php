<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<style>
    .body_content .containner .black .box_inner .block2_inner{
        max-height: 500px;
        overflow: auto;
        padding: 0px 10px 0px 10px;
    }
    .body_content .containner .black .box_inner .block1_inner h1{
        font-size:13px;
    }
</style>
<?php
    $disabled = "";
    if($market_status_active == 5){
        if($this->user->IsAdmin==1){
            $disabled = "disabled";
        }
    }else if($market_status_active == 7 || $market_status_active == 4){
        $disabled = "disabled";
    }
?>
<div class="alert alert-success alert-dismissable showMessenger hidden">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <p class="Messenger"></p>
</div>
<form id="frmAddPlaner" method="post">
<div class="table_data">
    <?php
        if(true){
    ?>
    <table id="table_data">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center" colspan="3"><textarea class="resizable_textarea form-control ckeditor" id="planer_name" name="planer_name"></textarea></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Thời gian bắt đầu</th>
            <th class="text-center">
                <div class="form-group">
                    <input type='text' name="planer_start" id="planer_start" placeholder="2016-01-01 00:00:00" value="" class="form-control" />
                </div>
            </th>
            <th class="text-center"></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3">Thời gian kết thúc</th>
            <th class="text-center">
                <div class="form-group">
                    <input type='text' name="planer_end" id="planer_end" placeholder="2016-01-01 00:00:00" value="" class="form-control" />
                </div>
            </th>
            <th class="text-center"></th>
        </tr>
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-right col-xs-3"></th>
            <th class="text-center"></th>
            <th class="text-center"><a class="btn btn-primary addPlaner <?=$disabled;?>" rel=""><i class="fa fa-save"></i> Thêm mới</a></th>
        </tr>
        
    </table>
    <div class="clearfix"><br/></div>
    <?php } ?>
    <table id="table_data" class="tablePlaner hidden">
        <tr>
            <th class="text-center hidden"></th>
            <th class="text-center col-xs-1">STT</th>
            <th class="text-center">Kế hoạch thực hiện</th>
            <th class="text-center col-xs-1"></th>
        </tr>
        <tbody class="showContent">
            
        </tbody>
    </table>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
    <input type='hidden' id="market_id" name="market_id" value="<?=$market_id;?>" />
    <input type='hidden' id="parent" name="parent" value="<?=$parent;?>" />
</div>
</form>
<script type="text/javascript">
var baselink = $("#baselink_report").val();
var marketID = "<?= $market_id; ?>";
var parentID = "<?= $parent; ?>";

CKEDITOR.replace('planer_name', {
    toolbar: [
        {name : 'clipboard', items: ['Undo','Redo','Bold','Italic','Underline','Heading']},
        {name: 'paragraph', items: ['Undo'] ,groups: [ 'Undo', 'list', 'indent', 'blocks', 'align', 'bidi' ],  groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote','-','Cut','Copy','Paste','PasteText','PasteFromWord','-'] },
    ],
    height: 100
});

$(document).ready(function () {
    setTimeout(function(){
        //loadPlaner();
    },100);
});

//$('#planer_name').keypress(function(e){
//    if(e.which == 13) {
//        $('.addPlaner').click();
//        return false;
//    }
//});
//
//$('#planer_name').keyup(function(){
//    if($(this).val() == ""){
//        $('.addPlaner').addClass("disabled");
//    }else{
//        $('.addPlaner').removeClass("disabled");
//    }
//});

function loadPlaner(){
    $.ajax({
        url: baselink+"marketing_campaign/loadPlaner",
        dataType: "html",
        type: "POST",
        data: "market_id="+marketID+"&parent="+parentID,
        success: function(result){
            $('.showContent').html(result);
        }
    });
}

$('.addPlaner').click(function(){
    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
    var planerName = $('#planer_name').val();
    if(planerName == ""){
        alert('Nhập tên kế hoạch cần làm !!!');
        $('#planer_name').focus();
    }else{
        $.ajax({
            url: baselink+"marketing_campaign/addPlaner",
            dataType: "html",
            type: "POST",
            data: $('#frmAddPlaner').serialize(),
            success: function(result){
                console.log(result);
                if(result == "OK"){
                    location.reload();
                    //loadPlaner();
                    //$('#planer_name').val("");
                    //$('#planer_name').focus();
                }else{
                    alert('Lỗi hệ thống');
                }
                //console.log(result);
            }
        });
    }
});

$("#close_overlay").click(function(){
    $(".over_lay").removeClass('in');
    $(".over_lay").addClass('out');
    location.reload();
});

function removePlaner(id){
    if (!confirm('Bạn muốn xóa?\nLưu ý: Toàn bộ hành động thuộc kế hoạch này sẽ bị xóa :(')) {
        return false;
    }else{
        $.ajax({
            url: baselink+"marketing_campaign/removePlaner",
            dataType: "html",
            type: "POST",
            data: "id="+id,
            success: function(result){
                if(result == "OK"){
                    loadPlaner();
                }else{
                    alert('Lỗi hệ thống');
                }
            }
        });
    }
}
</script>