<?php

class Banner_model extends CI_Model {

    public $tablename = 'ttp_banner';
    public $tablename_position = 'ttp_banner_position';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getAllBanner($id = null) {
        $this->db->select('*');
        if ($id) {
            $this->db->where('ID', $id);
        }
        $result = $this->db->get($this->tablename)->result_array();
        return $result;
    }

    function getAllBannerPosition() {
        $this->db->select('*');
        $result = $this->db->get($this->tablename_position)->result_array();
        return $result;
    }
    
    function defineAllBanner($id = null){
        
    }

    function defineOptionBanner() {
        
    }

    function defineOptionPosition() {
        $data_position = $this->getAllBannerPosition();
        $res = array();
        foreach ($data_position as $key => $row) {
            $res[$key] = $row['ID'];
            $res[$key] = ($row['Description']) ? $row['Description'] : $row['Title'];
        }
        return $res;
    }

}
