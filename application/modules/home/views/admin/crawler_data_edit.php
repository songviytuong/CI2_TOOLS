<script type="text/javascript" src="<?php echo base_url()?>public/plugin/ckeditor/ckeditor.js"></script>
<div class="">
    <div class="row">
        <form data-parsley-validate class="form-horizontal form-label-left" id="formdata" action="<?php echo $base_link ."update" ?>" method="post" enctype="multipart/form-data">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="tab1 tabcontent">
                    <div class="x_title">
                        <h2>POST <small> / Edit post</small></h2>
                        <button type="submit" class="btn btn-success" style="float:right">Save change</button>
                        <div class="clearfix"></div>
                        
                    </div>
                    <?php 
                    $json = json_decode($data->Data,true);
                    ?>
                    <div class="x_content">
                            <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Title (<span class="required">*</span>)</label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" required="required" class="form-control col-md-12 col-xs-12" name="Title" value="<?php echo isset($json['Title']) ? trim(str_replace('"',"'", $json['Title'])) : '' ; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12 col-sm-12 col-xs-12">Description</label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="resizable_textarea form-control" name="Description" id="Description" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 130px;"><?php echo isset($json['Description']) ? trim($json['Description']) : '' ; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="resizable_textarea form-control ckeditor" name="Introtext" id="Introtext"><?php echo isset($json['Introtext']) ? $json['Introtext'] : '' ; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
