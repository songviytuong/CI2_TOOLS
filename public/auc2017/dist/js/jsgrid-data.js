﻿/*Jsgrid Init*/
$(function() {
	"use strict";
	
    $("#jsgrid_1").jsGrid({
        heading: true,
        height: "auto",
        width: "100%",
        filtering: true,
        selecting: true,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        pageSize: 15,
        pageButtonCount: 5,
        deleteConfirm: "Do you really want to delete the client?",
        controller: db,
        rowClick: function(args) {
            console.log(args.item);
        },
        fields: [
            { name: "Order", type: "text", width: 150 },
            { name: "Order Date", type: "text", width: 150 },
            { name: "Customer", type: "text", width: 150 },
            { name: "Address", type: "text", width: 200 },
            { name: "Country", type: "select", items: db.countries, valueField: "Id", textField: "Name" },
            { name: "Married", type: "checkbox", title: "Is Married", sorting: false },
        ]
    });
    
 
});
