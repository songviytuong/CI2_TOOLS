<?php

class Home extends CI_Controller {

    public $limit = 20;
    public $upload_to = '';

    public function __construct() {
        parent::__construct();
        $this->load->library('template');
        $this->template->set_template('site');

        #Load Static Config
        $this->load->model('static_model');
        $static_config = $this->static_model->getConfig();

        $lang = $this->uri->segment(1);
        $lang = $lang == "en" ? "en" : "vi";

        $datafooter = array(
            "static" => $static_config,
            'lang' => $lang
        );
        $this->template->write_view('header', 'header', array('lang' => $lang));
        $this->template->write_view('footer', 'footer', $datafooter);
        $this->template->add_doctype();
    }

    public function addmyiptolist() {
        $params = array_merge($_GET, $_POST);
        $this->load->model('global_model','global');
        if($params['token'] == 'auchan'){
            $this->global->addIP();
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function removemyip() {
        $this->load->model('global_model','global');
        if($this->global->removeIP()){
            redirect($_SERVER['HTTP_REFERER']);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function make_folder($str, $folder = 'assets/') {
        if ($str != '') {
            if (!file_exists($folder . $str)) {
                mkdir("./$folder" . $str, 0777, true);
            }
        }
    }

    public function downloadimage_by_link($image = '') {
        $this->upload_to = date('Y') . '/' . date('Y-m');
        $this->make_folder($this->upload_to, 'assets/');
        @$file = file_get_contents($image);
        if ($file != '') {
            $name = explode('/', $image);
            $name = isset($name[count($name) - 1]) ? $name[count($name) - 1] : '';
            if ($name != '') {
                file_put_contents("assets/" . $this->upload_to . '/' . $name, $file);
                $cropimage = explode(",", IMAGECROP);
                foreach ($cropimage as $row) {
                    $size = explode("x", $row);
                    $width = isset($size[0]) ? (int) $size[0] : 0;
                    $height = isset($size[1]) ? (int) $size[1] : 0;
                    if ($width > 0 && $height > 0)
                        $this->lib->cropimage("assets/" . $this->upload_to . '/' . $name, $width, $height);
                }
                return "assets/" . $this->upload_to . '/' . $name;
            }
        }else {
            return '';
        }
    }

    public function postsite() {
        $getcat = isset($_GET['getcat']) ? $_GET['getcat'] : '';
        if ($getcat == 'true') {
            $result = $this->db->query("select * from ttp_categories")->result();
            if (count($result) > 0) {
                echo "<ul>";
                foreach ($result as $row) {
                    echo "<li><input type='checkbox' value='$row->ID' /> $row->Title";
                }
                echo "</ul>";
            }
        }
        $transfer = isset($_GET['trans']) ? $_GET['trans'] : "";
        if ($transfer == "true") {
            $keyaccept = sha1("@hoathienphu.com.vn");
            $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "None";
            $key = isset($_POST['key']) ? $_POST['key'] : "None";
            if ($keyaccept == $key && $referer == "http://tools.hoathienphu.com.vn/") {
                $post_thumb = isset($_POST['post_thumb']) ? $_POST['post_thumb'] : "";
                $post_title = isset($_POST['post_title']) ? $_POST['post_title'] : "";
                $post_content = isset($_POST['post_content']) ? $_POST['post_content'] : "";
                $post_description = isset($_POST['post_description']) ? $_POST['post_description'] : "";
                $post_categories = isset($_POST['post_categories']) ? $_POST['post_categories'] : 0;
                if ($post_title != '' && $post_content != '' && $post_categories > 0) {
                    $thumb = $this->downloadimage_by_link($post_thumb);
                    $data = array(
                        'Title' => $post_title,
                        'Description' => $post_description,
                        'Introtext' => $post_content,
                        'CategoriesID' => $post_categories,
                        'Published' => 0,
                        'Alias' => $this->lib->alias($post_title),
                        'Created' => date('Y-m-d H:i:s'),
                        'LastEdited' => date('Y-m-d H:i:s'),
                        'Thumb' => $thumb
                    );
                    $this->db->insert("ttp_post", $data);

                    /*                     * * End Insert ** */
                    echo "true";
                } else {
                    echo "false";
                }
            } else {
                if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARTDED_FOR'] != '') {
                    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip_address = $_SERVER['REMOTE_ADDR'];
                }
                date_default_timezone_set('Asia/Ho_Chi_Minh');
                @file_put_contents("log_connect.txt", "******** Error authentication *********\nTime : " . date("d/m/Y H:i:s", time()) . "\n" . "Referer : $referer \nKey : $key \nIp : $ip_address \n \n", FILE_APPEND);
                echo "false";
            }
        }
    }

    public function accept_deny() {
        $this->load->view('accept_deny');
    }

    public function test() {
        echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
        $site = $this->db->query("select * from ttp_report_order")->result();
        $arr = array();
        foreach ($site as $row) {
            $arr[$row->UserID][] = '';
            $monyear = date('Ym', strtotime($row->Ngaydathang));
            $idbyUser = str_pad(count($arr[$row->UserID]), 5, '0', STR_PAD_LEFT);
            $MaDH = 'DH' . $monyear . "_" . $idbyUser . "_" . $row->UserID;
            echo "update ttp_report_order set MaDH='$MaDH' where ID=$row->ID ;<br>";
        }
    }

    public function checkstatus() {
        echo "true";
    }

    public function en_render() {
        $alias = $this->uri->segment(2);
        if ($alias == '') {
            $this->index('en');
            return;
        }
        $check = $this->db->query("select * from ttp_pagelinks where Alias_en='$alias'")->row();
        if ($check) {
            switch ($check->Classname) {
                case 'about':
                    $this->about("en");
                    break;
                case 'news':
                    $this->news("en");
                    break;
                case 'recruitment':
                    $this->recruitment("en");
                    break;
                case 'products':
                    $this->products("en");
                    break;
                case 'family':
                    $this->family("en");
                    break;
                case 'contact':
                    $this->contact("en");
                    break;
            }
        } else {
            $check = $this->db->query("select ID,Title_en as Title,Title as MetaTitle,Title as MetaKeywords,Title as MetaDescription,MetaExt,CategoriesID,Created,Description_en as Description,Data,Alias_en as Alias,Thumb,Introtext_en as Introtext,Tags_en as Tags from ttp_post where Alias_en='$alias' and Published=1")->row();
            if ($check) {
                $this->details($check, "en");
            } else {
                $check = $this->db->query("select ID,Title_en as Title,Description_en as Description,Data,Alias_en as Alias from ttp_categories where Alias_en='$alias' and Published=1")->row();
                if ($check) {
                    $this->categories($check, "en");
                } else {
                    $check = $this->db->query("select * from ttp_tags where Alias='$alias' and Published=1")->row();
                    if ($check) {
                        $this->tags($check, "en");
                    } else {
                        $this->errorpage();
                    }
                }
            }
        }
    }

    public function index($lang = 'vi') {
        $temp = rand(10000, 99999);
//        $this->set_barcode($temp);
        return;
        $this->template->add_title("System Administrator");
        $this->template->write_view('content', 'home', array('language' => $lang));
        $this->template->render();
    }

    private function set_barcode($code) {
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        Zend_Barcode::render('code128', 'image', array('text' => $code), array());
    }

    public function about($lang = 'vi') {
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='about' limit 0,1")->row();
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->MetaTitle_en != '' ? $ob->MetaTitle_en : $ob->Title_en;
            $description = $ob->MetaDescription_en != '' ? $ob->MetaDescription_en : $ob->Title_en;
            $keywords = $ob->MetaKeywords_en != '' ? $ob->MetaKeywords_en : $ob->Title_en;
        }
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'about', array('language' => $lang));
        $this->template->render();
    }

    public function family($lang = 'vi') {
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='family' limit 0,1")->row();
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->MetaTitle_en != '' ? $ob->MetaTitle_en : $ob->Title_en;
            $description = $ob->MetaDescription_en != '' ? $ob->MetaDescription_en : $ob->Title_en;
            $keywords = $ob->MetaKeywords_en != '' ? $ob->MetaKeywords_en : $ob->Title_en;
        }
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'family', array('language' => $lang));
        $this->template->render();
    }

    public function news($lang = 'vi') {
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='news' limit 0,1")->row();
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->MetaTitle_en != '' ? $ob->MetaTitle_en : $ob->Title_en;
            $description = $ob->MetaDescription_en != '' ? $ob->MetaDescription_en : $ob->Title_en;
            $keywords = $ob->MetaKeywords_en != '' ? $ob->MetaKeywords_en : $ob->Title_en;
        }
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'news', array('language' => $lang));
        $this->template->render();
    }

    public function products($lang = 'vi') {
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='products' limit 0,1")->row();
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->MetaTitle_en != '' ? $ob->MetaTitle_en : $ob->Title_en;
            $description = $ob->MetaDescription_en != '' ? $ob->MetaDescription_en : $ob->Title_en;
            $keywords = $ob->MetaKeywords_en != '' ? $ob->MetaKeywords_en : $ob->Title_en;
        }
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'products', array('language' => $lang));
        $this->template->render();
    }

    public function contact($lang = 'vi') {
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='contact' limit 0,1")->row();
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->MetaTitle_en != '' ? $ob->MetaTitle_en : $ob->Title_en;
            $description = $ob->MetaDescription_en != '' ? $ob->MetaDescription_en : $ob->Title_en;
            $keywords = $ob->MetaKeywords_en != '' ? $ob->MetaKeywords_en : $ob->Title_en;
        }
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'contact', array('language' => $lang));
        $this->template->render();
    }

    public function recruitment($lang = 'vi') {
        $ob = $this->db->query("select * from ttp_pagelinks where Classname='recruitment' limit 0,1")->row();
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->MetaTitle_en != '' ? $ob->MetaTitle_en : $ob->Title_en;
            $description = $ob->MetaDescription_en != '' ? $ob->MetaDescription_en : $ob->Title_en;
            $keywords = $ob->MetaKeywords_en != '' ? $ob->MetaKeywords_en : $ob->Title_en;
        }
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'recruitment', array('language' => $lang));
        $this->template->render();
    }

    public function checklink() {
        $Alias = $this->uri->uri_string();
        $check = $this->db->query("select * from ttp_post where Alias='$Alias' and Published=1")->row();
        if ($check) {
            $this->details($check);
        } else {
            $segment = explode('/', $Alias);
            $page = count($segment) > 0 ? $segment[count($segment) - 1] : 0;
            if ($page > 0) {
                $Alias = str_replace("/$page", "", $Alias);
            }
            $check = $this->db->query("select * from ttp_categories where Alias='$Alias' and Published=1")->row();
            if ($check) {
                $this->categories($check);
            } else {
                $check = $this->db->query("select * from ttp_tags where Alias='$Alias' and Published=1")->row();
                if ($check) {
                    $this->tags($check);
                } else {
                    $this->errorpage();
                }
            }
        }
    }

    public function Upload_cv() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->library("upload");
        $this->upload->initialize(array(
            "upload_path" => "./assets/cv",
            'allowed_types' => 'doc|docx|pdf',
            'max_size' => '5000',
            'encrypt_name' => TRUE
        ));
        if ($this->upload->do_multi_upload("Images_upload")) {
            $url_thumb = array();
            $data_file = $this->upload->get_multi_upload_data();
            for ($i = 0; $i < count($_FILES['Images_upload']["name"]); $i++) {
                $url_thumb[] = "assets/cv/" . $data_file[$i]['file_name'];
            }
            return $url_thumb;
        }
        return array();
    }

    public function apply() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $Name = isset($_POST['Name']) ? $this->security->xss_clean($_POST['Name']) : "";
        $Email = isset($_POST['Email']) ? $this->security->xss_clean($_POST['Email']) : "";
        $Ghichu = isset($_POST['Ghichu']) ? $this->security->xss_clean($_POST['Ghichu']) : "";
        $PostID = isset($_POST['PostID']) ? $this->security->xss_clean($_POST['PostID']) : 0;
        $data = array(
            'Name' => $Name,
            'Email' => $Email,
            'PostID' => $PostID,
            'Created' => date('Y/m/d H:i:s')
        );
        $json = array();
        if (count($_POST) > 0) {
            foreach ($_POST as $key => $value) {
                if (!array_key_exists($key, $data)) {
                    $json[$key] = $this->security->xss_clean($value);
                }
            }
        }
        $cv = $this->Upload_cv();
        $json['CV'] = $cv;
        $data['Data'] = json_encode($json);
        $this->db->insert("ttp_recruitment", $data);
        $link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
        $config = $this->db->query("select * from ttp_email where Published=1 limit 0,1")->row();
        if ($config) {
            $json = $config->Data != '' ? json_decode($config->Data) : (object) array();
            if (isset($json->Themes)) {
                if (file_exists("public/themes_email/" . $json->Themes . ".html")) {
                    $post = $this->db->query("select Title from ttp_post where ID=$PostID")->row();
                    if ($post) {
                        $template = file_get_contents("public/themes_email/" . $json->Themes . ".html");
                        $content1 = "<p style='background:#F44336;color:#FFF;padding:10px;text-align:center'>****** Thư gửi từ hệ thống Email tự động ******</p>";
                        $content1 .= "<p><b>Vị trí ứng tuyển :</b> $post->Title</p>";
                        $content1 .= "<p><b>Tên người ứng tuyển :</b> $Name</p>";
                        $content1 .= "<p><b>Email :</b> $Email</p>";
                        $content1 .= "<p><b>Mô tả :</b> $Ghichu</p>";
                        $content = isset($json->Sender_content) ? $json->Sender_content : "";
                        $content = "<p style='background:#F44336;color:#FFF;padding:10px;text-align:center'>****** Thư gửi từ hệ thống Email tự động ******</p>";
                        $content .= "<p><b>Vị trí ứng tuyển :</b> $post->Title</p>";
                        $content .= "<p><b>Tên người ứng tuyển :</b> $Name</p>";
                        $content .= "<p><b>Email :</b> $Email</p>";
                        $content .= "<p><b>Mô tả :</b> $Ghichu</p>";
                        if (count($cv) > 0 && is_array($cv)) {
                            $content .= "<hr> File đính kèm <br><ul>";
                            $content1 .= "<hr> File đính kèm <br><ul>";
                            foreach ($cv as $row) {
                                $content .= "<li style='padding:5px 0px'><a href='" . base_url() . $row . "'>$row</a> <= Click to download file</li>";
                                $content1 .= "<li style='padding:5px 0px'><a href='" . base_url() . $row . "'>$row</a> <= Click to download file</li>";
                            }
                            $content .= "</ul>";
                            $content1 .= "</ul>";
                        }
                        $template1 = str_replace("{content}", $content, $template);
                        $template2 = str_replace("{content}", $content1, $template);

                        $title_reciver = isset($json->Reciver_title) ? $json->Reciver_title : "";
                        $title_sender = isset($json->Sender_title) ? $json->Sender_title : "";
                        $email_reciver = isset($json->Email_reciver) ? $json->Email_reciver : "";
                        $email_sender = $Email;

                        @$this->sendmail($title_reciver, $email_reciver, $template1);
                        @$this->sendmail($title_sender, $email_sender, $template);
                    }
                }
            }
        }
        echo '<meta charset="utf-8">';
        echo "<script>alert('Gửi thư ứng tuyển thành công !. Chúng tôi sẽ xét duyệt và liên hệ lại với bạn trong thời gian sớm nhất !');
        window.location='" . $link . "';</script>";
    }

    public function sendmail($Title = "", $reciver = "", $message = "") {
        if ($Title != "" && $reciver != '' && $message != '') {
            $config = $this->db->query("select * from ttp_email where Published=1 limit 0,1")->row();
            if ($config) {
                $json = $config->Data != '' ? json_decode($config->Data) : (object) array();
                $this->load->library("email");
                $this->load->library("my_email");
                $data = array(
                    'message' => $message,
                    'user' => $json->SMTP_user,
                    'password' => $json->SMTP_password,
                    'protocol' => $json->Protocol,
                    'smtp_host' => $json->SMTP_host,
                    'smtp_port' => $json->SMTP_port,
                    'from_sender' => $json->SMTP_user,
                    'subject_sender' => $Title,
                    'to_receiver' => $reciver
                );
                $this->my_email->config($data);
                $this->my_email->sendmail();
            }
        }
    }

    public function sendcontact() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $Name = isset($_POST['Name']) ? $this->security->xss_clean($_POST['Name']) : "";
        $Email = isset($_POST['Email']) ? $this->security->xss_clean($_POST['Email']) : "";
        $Note = isset($_POST['Note']) ? $this->security->xss_clean($_POST['Note']) : "";
        if ($Note != '' && $Name != '' && $Email != '') {
            $data = array(
                'Name' => $Name,
                'Email' => $Email,
                'Created' => date('Y/m/d H:i:s')
            );
            $json = array();
            if (count($_POST) > 0) {
                foreach ($_POST as $key => $value) {
                    if (!array_key_exists($key, $data)) {
                        $json[$key] = $this->security->xss_clean($value);
                    }
                }
            }
            $data['Data'] = json_encode($json);
            $this->db->insert("ttp_contact", $data);
        }
        $link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
        echo '<meta charset="utf-8">';
        echo "<script>alert('Chúng tôi đã nhận được tin nhắn từ Quý Khách !. Chúng tôi sẽ liên hệ lại với Quý Khách trong thời gian sớm nhất !');
        window.location='" . $link . "';</script>";
    }

    public function saveemail() {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $Email = isset($_POST['Email']) ? $this->security->xss_clean($_POST['Email']) : "";
        if ($Email != '') {
            $check = $this->db->query("select * from ttp_customeremail where Email='$Email'")->row();
            if (!$check) {
                $data = array(
                    'Email' => $Email,
                    'Created' => date('Y/m/d H:i:s')
                );
                $this->db->insert("ttp_customeremail", $data);
            }
        }
        $link = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
        echo '<meta charset="utf-8">';
        echo "<script>alert('Chúng tôi đã nhận được tài khoản Email từ Quý Khách !. Mọi thông tin mới nhất từ website chúng tôi sẽ gửi đến địa chỉ Email mà Quý Khách đã đăng ký !');
        window.location='" . $link . "';</script>";
    }

    public function details($ob, $lang = "vi") {
        $this->db->query("update ttp_post set View = View+1 where ID=$ob->ID");
        if ($lang == "vi") {
            $title = $ob->MetaTitle != '' ? $ob->MetaTitle : $ob->Title;
            $description = $ob->MetaDescription != '' ? $ob->MetaDescription : $ob->Title;
            $keywords = $ob->MetaKeywords != '' ? $ob->MetaKeywords : $ob->Title;
        } else {
            $title = $ob->Title;
            $description = $ob->Title;
            $keywords = $ob->Title;
        }
        $ext = $ob->MetaExt != '' ? $ob->MetaExt : "";
        $ext .= "<meta property='og:title' content='$title' />";
        $ext .= "<meta property='og:description' content='$description' />";
        $ext .= file_exists($ob->Thumb) ? "<meta property='og:image' content='" . base_url() . "$ob->Thumb' />" : "";
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write('MetaExtend', $ext);
        $data = array(
            'data' => $ob,
            'lang' => $lang
        );
        $this->template->write_view('content', 'detail', $data);
        $this->template->render();
    }

    public function preview($id = 0) {
        if (!isset($_SESSION['data'])) {
            $_SESSION['data'] = $_POST;
        }
        print_r($_SESSION['data']);
        return;
        $this->template->add_title("PREVIEW POST");
        $data = array(
            'data' => ""
        );
        $this->template->write_view('content', 'detail', $data);
        $this->template->render();
    }

    public function categories($ob, $lang = "vi") {
        if ($lang == "en") {
            $link = "en/" . $this->uri->segment(2);
            $page = $this->uri->segment(3);
            $langnume = 3;
        } else {
            $link = $this->uri->segment(1);
            $page = $this->uri->segment(2);
            $langnume = 2;
        }

        $start = is_numeric($page) ? $page : 0;
        $limit_str = "limit $start,$this->limit";
        if (!is_numeric($start))
            $start = 0;
        $nav = $this->db->query("select count(1) as nav from ttp_post where Thumb!='' and Published=1 and CategoriesID = $ob->ID")->row();
        $nav = $nav ? $nav->nav : 0;
        if ($lang == "en") {
            $result = $this->db->query("select ID,Title_en as Title,Description_en as Description,Alias_en as Alias,Thumb,Created from ttp_post where Thumb!='' and Published=1 and CategoriesID = $ob->ID order by ID DESC $limit_str")->result();
        } else {
            $result = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and CategoriesID = $ob->ID order by ID DESC $limit_str")->result();
        }
        $data = array(
            'categories' => $ob,
            'data' => $result,
            'result' => $nav,
            'nav' => $this->lib->nav(base_url() . $link, $langnume, $nav, $this->limit),
            'lang' => $lang
        );
        $json = json_decode($ob->Data);
        $metatitle = isset($json->metatitle) ? $json->metatitle : "";
        $metadescription = isset($json->metadescription) ? $json->metadescription : "";
        $metakeywords = isset($json->metakeywords) ? $json->metakeywords : "";
        $title = $metatitle != '' ? $metatitle : $ob->Title;
        $description = $metadescription != '' ? $metadescription : $ob->Title;
        $keywords = $metakeywords != '' ? $metakeywords : $ob->Title;
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'categories', $data);
        $this->template->render();
    }

    public function tags($ob, $lang = "vi") {
        if ($lang == "en") {
            $link = "en/" . $this->uri->segment(2);
            $page = $this->uri->segment(3);
            $langnume = 3;
        } else {
            $link = $this->uri->segment(1);
            $page = $this->uri->segment(2);
            $langnume = 2;
        }
        $start = is_numeric($page) ? $page : 0;
        $limit_str = "limit $start,$this->limit";
        if (!is_numeric($start))
            $start = 0;
        $nav = $this->db->query("select count(1) as nav from ttp_post where Thumb!='' and Published=1 and Tags like '%[$ob->ID]%'")->row();
        $nav = $nav ? $nav->nav : 0;
        if ($lang == "en") {
            $result = $this->db->query("select ID,Title_en as Title,Description_en as Description,Alias_en as Alias,Thumb,Created from ttp_post where Thumb!='' and Published=1 and Tags like '%[$ob->ID]%' order by ID DESC $limit_str")->result();
        } else {
            $result = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and Tags like '%[$ob->ID]%' order by ID DESC $limit_str")->result();
        }
        $data = array(
            'categories' => $ob,
            'data' => $result,
            'result' => $nav,
            'nav' => $this->lib->nav(base_url() . $link, $langnume, $nav, $this->limit),
            'lang' => $lang
        );
        $json = json_decode($ob->Data);
        $metatitle = isset($json->metatitle) ? $json->metatitle : "";
        $metadescription = isset($json->metadescription) ? $json->metadescription : "";
        $metakeywords = isset($json->metakeywords) ? $json->metakeywords : "";
        $title = $metatitle != '' ? $metatitle : $ob->Title;
        $description = $metadescription != '' ? $metadescription : $ob->Title;
        $keywords = $metakeywords != '' ? $metakeywords : $ob->Title;
        $this->template->add_title($title);
        $this->template->write('MetaKeywords', $keywords);
        $this->template->write('MetaDescription', $description);
        $this->template->write_view('content', 'tags', $data);
        $this->template->render();
    }

    public function errorpage() {
        $this->load->view('404');
    }

    public function search() {
        $keywords = $this->session->userdata("keywords");
        if (isset($_POST['keywords'])) {
            $keywords = mysql_real_escape_string($_POST['keywords']);
            $this->session->set_userdata("keywords", $keywords);
        }
        $page = $this->uri->segment(2);
        $start = is_numeric($page) ? $page : 0;
        $limit_str = "limit $start,$this->limit";
        if (!is_numeric($start))
            $start = 0;
        $nav = $this->db->query("select count(1) as nav from ttp_post where Thumb!='' and Published=1 and Title like '%$keywords%'")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select * from ttp_post where Thumb!='' and Published=1 and Title like '%$keywords%' order by ID DESC $limit_str")->result();
        $data = array(
            'keywords' => $keywords,
            'data' => $result,
            'result' => $nav,
            'nav' => $this->lib->nav(base_url() . '/search', 2, $nav, $this->limit),
            'lang' => "vi"
        );
        $this->template->add_title('Tìm kiếm từ khóa ' . $keywords);
        $this->template->write_view('content', 'search', $data);
        $this->template->render();
    }

    public function search_en() {
        $keywords = $this->session->userdata("keywords");
        $lang = $this->session->userdata("lang");
        if (isset($_POST['keywords'])) {
            $keywords = mysql_real_escape_string($_POST['keywords']);
            $this->session->set_userdata("keywords", $keywords);
        }
        if (isset($_POST['lang'])) {
            $lang = mysql_real_escape_string($_POST['lang']);
            $this->session->set_userdata("lang", $lang);
        }
        $page = $this->uri->segment(3);
        $start = is_numeric($page) ? $page : 0;
        $limit_str = "limit $start,$this->limit";
        if (!is_numeric($start))
            $start = 0;
        $nav = $this->db->query("select count(1) as nav from ttp_post where Thumb!='' and Published=1 and Title like '%$keywords%'")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select ID,Title_en as Title,Description_en as Description,Created,Alias_en as Alias,Thumb from ttp_post where Thumb!='' and Published=1 and Title like '%$keywords%' order by ID DESC $limit_str")->result();
        $data = array(
            'keywords' => $keywords,
            'data' => $result,
            'result' => $nav,
            'nav' => $this->lib->nav(base_url() . 'en/search', 3, $nav, $this->limit),
            'lang' => $lang
        );
        $this->template->add_title('Search keywords ' . $keywords);
        $this->template->write_view('content', 'search', $data);
        $this->template->render();
    }

}

?>