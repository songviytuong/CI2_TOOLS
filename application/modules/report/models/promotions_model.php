<?php

class Promotions_model extends CI_Model {

    var $tablename = 'ttp_report_promotions';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    function getPromotions($status = 0) {
        $this->db->select('*');
        $this->db->where('Status', $status);
        $result = $this->db->get($this->tablename)->result_array();
        return $result;
    }
    
    function getActivePromotions($id){
        $this->db->query("SELECT * ttp_report_products WHERE Ta");
        $this->db->where_in($id, $id);
        $data = $this->db->get('ttp_report_products')->row();
        return $data;
    }

}
