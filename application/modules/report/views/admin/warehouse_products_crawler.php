<div class="containner opensitebar">
	<h4>Lấy dữ liệu từ website khác</h4>
	<hr>
	<div class="row">
		<div class="col-xs-2">
			<label class="control-label">Chọn site lấy tin</label>
		</div>
		<div class="col-xs-3">
			<select class="form-control">
				<option value="1">Allrecipies.com</option>
			</select>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2">
			<label class="control-label">Trang đích lấy nội dung</label>
		</div>
		<div class="col-xs-10">
			<input type="text" id="DesLinks" class="form-control" placeholder="Ví dụ : http://allrecipes.com/recipe/240326/vegetables-and-cabbage-stir-fry-with-oyster-sauce/" />
		</div>
	</div>
	<div class="row hidden" id="information_data" style="margin:0px;"></div>
	<div class="row">
		<div class="col-xs-2"></div>
		<div class="col-xs-10">
			<button class="btn btn-danger" id="getdata" onclick="get_infomation(this)"><i class="fa fa-reply-all" aria-hidden="true"></i> Lấy nội dung</button>
		</div>
	</div>
</div>
<script>
	function get_infomation(ob){
		$(ob).addClass("saving");
		var baselink = "<?php echo base_url().ADMINPATH.'/report/warehouse_products/allrecipes' ?>";
		var links = $("#DesLinks").val();
		if(links==''){
			$("#DesLinks").focus();
			return;
		}
		$.ajax({
            url: baselink,
            dataType: "html",
            type: "POST",
            data: "Link="+links,
            context:this,
            success: function(result){
            	$(ob).removeClass("saving");
            	$(ob).addClass('hidden');
                $("#information_data").html(result);
                $("#information_data").removeClass('hidden');
            }
        });
	}

	function backandnext(){
		$("#DesLinks").focus();
		$("#getdata").removeClass('hidden');
		$("#information_data").addClass('hidden');
		$("#information_data").html('');
	}
</script>