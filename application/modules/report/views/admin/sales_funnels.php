<?php 
$VISITORS = $this->db->query("select * from ttp_funels where ID=1")->row()->View;
$customer = $this->db->query("select count(1) as View,sum(Total) as Total from ttp_report_order where FunelID=1")->row();
$ORDERS = $customer->View;
$TOTAl = $customer->Total;
$PURCHASE = $this->db->query("select count(1) as View from ttp_funels_report where FunelsID=1 and Click=2")->row()->View;
$POSITION = $this->db->query("select Position,TitleButton,count(1) as total from ttp_funels_report where FunelsID=1 and Click=1 group by Position")->result();
$PRODUCTS = $this->db->query("select Position,TitleButton,count(1) as total from ttp_funels_report where FunelsID=1 and Click=2 group by Position")->result();
$OPTION = $this->db->query("select YesNo,count(1) as total from ttp_funels_report where FunelsID=1 and Click=4 group by YesNo")->result();
$arr_option = array(0=>0,1=>0);
if(count($OPTION)>0){
	foreach ($OPTION as $row) {
		if($row->YesNo==1){
			$arr_option[1] = $arr_option[1]+1;
		}else{
			$arr_option[0] = $arr_option[0]+1;
		}
	}
}
?>
<div class="containner">
	<div class="row">
		<div class="col-xs-3">
			<h5><i class="fa fa-users" aria-hidden="true"></i> NGƯỜI DÙNG</h5>
			<h3><?php echo number_format($VISITORS) ?></h3>
		</div>
		<div class="col-xs-3">
			<h5><i class="fa fa-shopping-cart" aria-hidden="true"></i> MUA HÀNG</h5>
			<h3><?php echo number_format($PURCHASE) ?></h3>
		</div>
		<div class="col-xs-3">
			<h5><i class="fa fa-calendar" aria-hidden="true"></i> ĐƠN HÀNG</h5>
			<h3><?php echo number_format($ORDERS) ?></h3>
		</div>
		<div class="col-xs-3">
			<h5><i class="fa fa-usd" aria-hidden="true"></i> GIÁ TRỊ</h5>
			<h3><?php echo number_format($TOTAl) ?></h3>
		</div>
	</div>
	<hr>
	<div class="row" style="margin-top:20px">
		<div class="col-xs-12">
			<div id="dual_x_div" style="width:100%;height: 300px;"></div>
		</div>
	</div>
	<div class="row" style="margin-top:20px">
		<div class="col-xs-12">
			<div id="dual_x_div1" style="width:100%;height: 300px;"></div>
		</div>
	</div>
</div>
<style>
	.col-xs-2{border-right:1px solid #eee;margin-bottom:20px;}
	.col-xs-2 i{margin-right: 5px;}
	.col-xs-2:last-child{border:none;}
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Ví trí người dùng click', 'Số lượt click'],
          <?php 
          foreach ($POSITION as $row) {
          	echo "['$row->TitleButton - vị trí $row->Position', $row->total],";
          }
          ?>
        ]);

        var data1 = new google.visualization.arrayToDataTable([
          ['Gói sản phẩm người dùng click mua', 'Số lượt click'],
          <?php 
          foreach ($PRODUCTS as $row) {
          	echo "['$row->TitleButton', $row->total],";
          }
          ?>
        ]);

        var options = {
          width: 1000,
          chart: {
            title: 'ĐẶT HÀNG NGAY',
            subtitle: 'Thống kê số lượng tương tác từ người dùng trên nút lệnh này'
          },
          bars: 'horizontal',
          series: {
            0: { axis: 'distance' }
          },
          axes: {
            x: {
              distance: {label: 'Số lượt click vào các vị trí'}
            }
          }
        };

        var options1 = {
          width: 1000,
          chart: {
            title: 'CHỌN MUA SẢN PHẨM',
            subtitle: 'Thống kê số lượng tương tác từ người dùng trên nút lệnh này'
          },
          bars: 'horizontal',
          series: {
            0: { axis: 'distance' }
          },
          axes: {
            x: {
              distance: {label: 'Số lượt click chọn mua sản phẩm'}
            }
          }
        };

      	var chart = new google.charts.Bar(document.getElementById('dual_x_div'));
      	chart.draw(data, options);
      	
      	var chart1 = new google.charts.Bar(document.getElementById('dual_x_div1'));
      	chart1.draw(data1, options1);
    };
    </script>