<div class="containner">
	<div class="waiting"><h1>Loading...</h1></div>
	<?php
	if(count($data)>0){
		$district = $this->db->query("select a.ID,a.Title,b.Title as CityTitle from ttp_report_district a,ttp_report_city b where a.CityID=b.ID")->result();
		$arr_district = array();
		if(count($district)>0){
			foreach($district as $row){
				$arr_district[$row->ID]['DistrictTitle'] = $row->Title;
				$arr_district[$row->ID]['CityTitle'] = $row->CityTitle;
			}
		}

		$temp='';
		foreach($data as $row){
			echo "<div class='list'>";
			echo "<p class='ordertitle'><b>$row->MaDH</b></p>";
			echo "<p class='productstitle'><span>Khách hàng </span>: <b style='text-transform: uppercase;'>$row->Name</b></p>";
			echo "<p class='productstitle'><span>Số điện thoại </span>: <b>$row->Phone</b></p>";
			echo "<p class='productsamount'><span>Tỉnh/Thành phố </span>: ".$arr_district[$row->DistrictID]['CityTitle']."</p>";
			echo "<p class='productsamount'><span>Quận/Huyện </span>: ".$arr_district[$row->DistrictID]['DistrictTitle']."</p>";
			echo "<p class='productsamount'><span>Địa chỉ nhận hàng </span>: $row->AddressOrder</p>";
			echo "<p class='productsamount'><span>Ghi chú giao hàng </span>: $row->Note</p>";
			echo "<label class='shippingorder' onclick='showmenufunction(this)'><i class='fa fa-bars' aria-hidden='true'></i></label>";
			echo "<div class='menufunction'>
					<a onclick='shippingorder(this,$row->ID)' data='".number_format($row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau)."'><i class='fa fa-check-circle' aria-hidden='true'></i> Giao thành công</a>
					<a onclick='notshipping($row->ID)'><i class='fa fa-repeat' aria-hidden='true'></i> Giữ lại kho</a>
				</div>";
			$chiphi = $row->Chiphi==0 ? 'Miễn phí' : number_format($row->Chiphi).' đ' ;
			echo "<p class='productstitle' style='border-top: 1px solid #ccc;padding-top: 10px;'><span style='width:170px;'>Chi phí giao hàng </span>: ".$chiphi."</p>";
			echo "<p class='productstitle'><span style='font-size:18px;width:170px;'>Tổng tiền phải thu </span>: <b style='color:#fb3737;font-size:18px' id='total_order$row->ID'>".number_format($row->Total+$row->Chiphi-$row->Reduce-$row->Chietkhau)." đ</b></p>";
			echo "<a onclick='loaddetails_order(this,$row->ID)' class='viewdetails'><i class='fa fa-bars' aria-hidden='true'></i> Xem chi tiết</a>";
			echo "<div class='listdetails' id='listdetails$row->ID'></div>";
			echo "</div>";
		}
	}else{
		echo '<p class="text-center" style="margin:100px 0px 20px 0px"><i class="fa fa-smile-o" aria-hidden="true" style="font-size:80px;color:#ccc;"></i></p><p class="text-center" style="font-size: 20px;color: #ccc;text-shadow:0px 1px 0px #FFF;">Bạn đã xử lý hết yêu cầu .</p>';
	}
	?>
</div>

<style>
	.waiting{display: none;}
</style>
<script type="text/javascript">
	var link = "<?php echo $base_link ?>";

	function shippingorder(ob,ID){
		var r = confirm("Bạn có chắc đơn hàng này giao thành công !");
		if (r == true) {
			var total = $(ob).attr('data');
			var getValue = prompt("Vui lòng nhập đúng số tiền bạn nhận được từ khách hàng",total);
      		if(getValue!=null){
				$(".waiting").show();
				window.location=link+'success_order?data='+ID+'&reciver='+getValue;
			}
		}
	}

	function loaddetails_order(ob,ID){
		$(ob).hide();
		$(ob).parent('.list').find('.listdetails').html("<p style='text-align:center'>Loading...</p>");
		$(ob).parent('.list').find('.listdetails').load(link+"details_order/"+ID);
		$(ob).parent('.list').find('.listdetails').show();
	}

	function minimumdetails(ob){
		$(ob).parent(".list_products_details").parent(".listdetails").hide();
		$(ob).parent(".list_products_details").parent(".listdetails").parent(".list").find(".viewdetails").show();
	}

	function showmenufunction(ob){
		$(ob).parent('.list').find('.menufunction').toggle();
	}

	function notshipping(ID){
		var r = confirm("Bạn có chắc đơn hàng này giữ lại kho !");
		if (r == true) {
			$(".waiting").show();
			$.ajax({
					url: link+"not_reciver_order",
					dataType: "html",
					type: "POST",
					data: "ID="+ID,
					success: function(result){
						location.reload();
					}
			});
		}
	}

	function not_reciver(ob,ID,OrderID){
		var r = confirm("Bạn có chắc khách hàng không nhận sản phẩm này !");
		if (r == true) {
			$(ob).find('i.fa').removeClass("fa-check");
			$(ob).find('i.fa').addClass("fa-spinner");
			listdetails = $(ob).parent(".list_products_details").parent("#listdetails"+OrderID);
			$(".waiting").show();
			$.ajax({
					url: link+"not_reciver",
					dataType: "html",
					type: "POST",
					data: "ID="+ID,
					success: function(result){
						$(ob).find('i.fa').removeClass("fa-spinner");
						$(ob).find('i.fa').addClass("fa-check");
						if(result=='OK'){
							listdetails.html("<p style='text-align:center'>Loading...</p>");
							listdetails.load(link+"details_order/"+OrderID);
						}else{
							alert("Không thực hiện được yêu cầu.");
						}
						$(".waiting").hide();
					}
			});
		}
	}

	function yes_reciver(ob,ID,OrderID){
		var r = confirm("Bạn có chắc khách hàng nhận lại sản phẩm này !");
		if (r == true) {
			$(ob).find('i.fa').removeClass("fa-check");
			$(ob).find('i.fa').addClass("fa-spinner");
			listdetails = $(ob).parent(".list_products_details").parent("#listdetails"+OrderID);
			$(".waiting").show();
			$.ajax({
					url: link+"yes_reciver",
					dataType: "html",
					type: "POST",
					data: "ID="+ID,
					success: function(result){
						console.log(result);
						$(ob).find('i.fa').removeClass("fa-spinner");
						$(ob).find('i.fa').addClass("fa-check");
						if(result=='OK'){
							listdetails.html("<p style='text-align:center'>Loading...</p>");
							listdetails.load(link+"details_order/"+OrderID);
						}else{
							alert("Không thực hiện được yêu cầu.");
						}
						$(".waiting").hide();
					}
			});
		}
	}
</script>
