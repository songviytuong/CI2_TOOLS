<?php
$current = $this->uri->segment(3);
$current = explode('_', $current);
$current = isset($current[0]) ? $current[0] : '';
$this->load->model('users_model', 'users');
?>

<div class="header">
    <div class="block1 text-center">
        <a href="<?php echo base_url() . ADMINPATH . "/report" ?>">
            <img src='<?= ADMIN_PATH; ?>/images/logo.png' />
        </a>
    </div>
    <label for="openmenu" class="btn-openmenu-mobile"></label>
    <input type="checkbox" id="openmenu" />
    <div class="block2">
        <ul>
            <?php
            $menu = $this->lib->get_menu_permission(0, $this->user->ID, $this->user->IsAdmin);
            if (count($menu) > 0) {
                foreach ($menu as $row) {
                    $current_checked = json_decode($row->CurrentChecked, true);
                    $selected = in_array($current, $current_checked) ? "class='current_group'" : " href='" . base_url() . ADMINPATH . "/$row->Alias'";
                    $label = in_array($current, $current_checked) ? "<label onclick='showsitebar(this)'>$row->Title</label>" : $row->Title;
                    echo "<li><a $selected>$label</a></li>";
                }
            }
            ?>
        </ul>
    </div>
    <div class="info_user">
        <ul>
            <?php
            $checkleader = "";
            $data_type = $this->lib->get_config_define('position', 'users', 1, 'id');
            foreach ($data_type as $key => $item) {
                $checkleader = ($item->code == $this->user->UserType) ? $item->name : $checkleader;
            }
            $checkleader = $this->user->IsAdmin == 1 ? "Developer" : $checkleader;
            echo file_exists($this->user->Thumb) ? "<a href='" . base_url() . ADMINPATH . "/home/profile" . "'><li class='li1'><img src='" . $this->user->Thumb . "' /></li></a>" : "<a href='" . base_url() . ADMINPATH . "/home/profile" . "'><li class='li1'><img src='" . ADMIN_PATH . "/images/user.png' /></li></a>";
            echo "<li class='li2'><a href='" . base_url() . ADMINPATH . "/home/profile" . "'>" . $this->user->FirstName . " <br><span>$checkleader</span></a></li>";
            echo "<li class='li3'><a href='" . base_url() . ADMINPATH . "/logout" . "'>Logout <i class='fa fa-sign-out pull-right'></i></a></li>";
            ?>
        </ul>
    </div>
</div>

