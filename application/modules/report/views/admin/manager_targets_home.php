<div class="containner">
	<div class="manager">
		<div class="fillter_bar">
			<div class="block1">
				<form action="<?php echo $base_link ?>setsessionsearch" method="post">
					<span>Lọc dữ liệu</span>
					<select name="CategoriesID" class="form-control">
						<option value="">-- Tháng --</option>
					</select>
					<input type="submit" class="btn btn-default" value="Filter" />
                    <a class="btn btn-default" href="<?php echo $base_link.'clearfilter' ?>">Clear Filter</a>
				</form>
			</div>
			<div class="block2">
				<a href="<?php echo $base_link.'add'; ?>" class="btn btn-primary">Add new</a>
			</div>
		</div>
		<div class="table_data">
			<table>
				<tr>
					<th>STT</th>
					<th>Tháng / Năm</th>
					<th>Chỉ tiêu</th>
					<th>Action</th>
				</tr>
				<?php 
				$arr = array(
					'Sales'		=>'Chỉ tiêu tổng doanh số',
					'Amount'	=>'Chỉ tiêu tổng số lượng sản phẩm bán ra',
					'Order'		=>'Chỉ tiêu tổng số lượng đơn hàng',
					'Customers'	=>'Chỉ tiêu tổng số lượng khách hàng'
				);
				if(count($data)>0){
					$i=$start;
					foreach($data as $row){
						$i++;
						echo "<tr>";
						echo "<td>$i</td>";
						echo "<td style='width:120px'>$row->Month</td>";
						$json = json_decode($row->Data,true);
						echo "<td>";
						if(count($json)>0){
							foreach($json as $key=>$value){
								echo isset($arr[$key]) ? "<small><span>".$arr[$key]."</span> : <b>".number_format($value)."</b></small>" : '' ;
							}
						}
						echo "</td>";
						echo "<td>
								<a href='{$base_link}edit/$row->ID'><i class='fa fa-pencil-square-o'></i> Edit </a>
                                                            &nbsp;&nbsp;&nbsp;
                                <a href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i> Delete</a>
                            </td>";
						echo "</tr>";
					}
				}else{
					echo "<tr><td colspan='4' style='text-align:center;background:#FFF'>Không tìm thấy dữ liệu</td></tr>";
				}
				?>
			</table>
			<?php if(count($data)>0) echo $nav; ?>
		</div>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
</style>