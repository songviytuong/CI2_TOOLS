<!DOCTYPE html>
<html>
    <head>
        <base href="<?php echo base_url() ?>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="image/x-icon" href="public/admin/images/favicon.png" rel="shortcut icon">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="public/admin/css/multiple-select.css" rel="stylesheet">
        <script src="public/admin/js/jquery.min.js"></script>
        <script src="public/admin/js/bootstrap.min.js"></script>
        <link href="public/admin/css/bootstrap.min.css" rel="stylesheet">

        <link href="public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="public/admin/css/animate.min.css" rel="stylesheet">
        <!-- daterangepicker -->
        <script type="text/javascript" src="public/admin/js/moment.min2.js"></script>
        <script type="text/javascript" src="public/admin/js/datepicker/daterangepicker.js"></script>
        <!-- Custom styling plus plugins -->
        <link href="public/admin/css/custom.css" rel="stylesheet">
        <link href="public/admin/css/icheck/flat/green.css" rel="stylesheet">
        <script type="text/javascript" src="public/admin/js/multiple-select.js"></script>
        <?php echo $_styles; ?>
        <?php echo $_title; ?>
    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <?php echo $sitebar; ?>
                </div>
                <!-- top navigation -->
                <div class="top_nav">
                    <?php echo $topnav; ?>
                </div>
                <!-- /top navigation -->
                <!-- page content -->
                <div class="right_col" role="main">
                    <br />
                    <?php echo $content; ?>
                </div>
                <!-- /page content -->
            </div>
        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
        <div id="connection_box_warning"><div class='box'><h3>Cảnh báo:</h3> <p>Đường truyền internet của bạn có vấn đề !. Vui lòng lưu tất cả dữ liệu đang soạn thảo (nếu có) về máy để tránh mất dữ liệu đáng tiếc.</p><input type="button" class="btn btn-primary" id="close_warning" value="Đóng cảnh báo" /></div></div>
        <script src="public/admin/js/bootstrap.min.js"></script>
        <script src="public/admin/js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- chart js -->
        <script src="public/admin/js/chartjs/chart.min.js"></script>
        <!-- bootstrap progress js -->
        <script src="public/admin/js/progressbar/bootstrap-progressbar.min.js"></script>
        <!-- icheck -->
        <script src="public/admin/js/icheck/icheck.min.js"></script>
        <!-- daterangepicker -->
        <script type="text/javascript" src="<?php echo base_url() ?>public/admin/js/moment.min2.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>public/admin/js/datepicker/daterangepicker.js"></script>
        <!-- sparkline -->
        <script src="public/admin/js/sparkline/jquery.sparkline.min.js"></script>

        <script src="public/admin/js/custom.js"></script>
        <?php echo $_scripts; ?>
        <script>
            function doesConnectionExist() {
                var xhr = new XMLHttpRequest();
                var file = "<?php echo base_url() ?>checkstatus?" + new Date().getTime();
                xhr.open('HEAD', file, false);
                try {
                    xhr.send();
                    if (xhr.status >= 200 && xhr.status < 304) {
                        console.log(xhr.status + " | " + file);
                        return true;
                    } else {
                        return false;
                    }
                } catch (e) {
                    return false;
                }
            }

            function check()
            {
                var status = document.getElementById('connection_box_warning');
                if (!doesConnectionExist()) {
                    status.setAttribute("style", "display:block");
                } else {
                    status.setAttribute("style", "display:none");
                }
            }

            setInterval(function () {
                check();
            }, 10000);

            $("#close_warning").click(function () {
                var status = document.getElementById('connection_box_warning');
                status.setAttribute("style", "display:none");
            });
        </script>
    </body>
</html>
