<!DOCTYPE html>

<html>

    <head>

        <base href="<?php echo base_url() ?>">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="image/x-icon" href="public/admin/images/favicon.png" rel="shortcut icon">
	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="public/admin/css/bootstrap.min.css" rel="stylesheet">
	    <link href="public/admin/fonts/css/font-awesome.min.css" rel="stylesheet">

	    <!-- Custom styling plus plugins -->

	    <link href="public/admin/css/style_transporter.css" rel="stylesheet">
        <script src="public/admin/js/jquery.min.js"></script>
        <script src="public/admin/js/bootstrap.min.js"></script>
        <?php echo $_styles; ?>
        <?php echo $_title; ?>
    </head>

    <body>
        <div class="body_content">
            <?php echo $header; ?>
            <?php echo $sitebar; ?>
            <?php echo $content; ?>
        </div>
        <?php echo $_scripts; ?>
    </body>
</html>

