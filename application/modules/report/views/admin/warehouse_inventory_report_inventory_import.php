<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($FromDate))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($ToDate))/(3600*24);
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>BÁO CÁO NHẬP KHO HÀNG HÓA</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="import_orderlist">
        <div class="block2 row">
            <div class="block_2_1 col-xs-9">
                <div class="form-group">
                <div class="col-xs-2">
                    <select class="form-control" id="WarehouseID" onchange="fillter_change()">
                        <?php 
                        $warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
                        if(count($warehouse)>0){
                            foreach($warehouse as $row){
                                $selected = $row->ID==$WarehouseID ? 'selected="selected"' : '' ;
                                echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-xs-3" style='margin-left:5px' onchange="fillter_change()">
                    <select class="form-control" id="Type">
                        <option value="0" <?php echo $Type==0 ? "selected='selected'" : '' ; ?>>Mua hàng từ NCC</option>
                        <option value="1" <?php echo $Type==1 ? "selected='selected'" : '' ; ?>>Hàng bán trả lại</option>
                        <option value="2" <?php echo $Type==2 ? "selected='selected'" : '' ; ?>>Lưu chuyển kho</option>
                    </select>
                </div>
                <div class="col-xs-3"></div>
                <div class="col-xs-3"></div>
                </div>
            </div>
            <div class="block_2_2 col-xs-3">
                <a class="btn btn-primary" role="button" href="<?php echo base_url().ADMINPATH."/report/import/export_by_resultdata?TypeExport=2&Type=2" ?>"><i class="fa fa-download"></i> Export</a>
            </div>
        </div>
        <div class="clear"></div>
        <div class="block3 table_data">
            <table id="table_data">
                <tr>
                    <th>STT</th>
                    <th>SKU</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng nhập</th>
                    <th>Nhập tại kho</th>
                </tr>
                <?php
                if(count($data)>0){
                    $i=1;
                    foreach($data as $row){
                        echo "<tr>";
                        echo "<td>$i</td>";
                        echo "<td>$row->MaSP</td>";
                        echo "<td>$row->Tensanpham</td>";
                        echo "<td>".number_format($row->TotalAmount,2)."</td>";
                        echo "<td>$row->MaKho</td>";
                        echo "</tr>";
                        $i++;
                    }
                }else{
                    echo "<tr><td colspan='5'>Không tìm thấy dữ liệu.</td></tr>";
                }
                ?>
            </table>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>

<script type="text/javascript">
    var baselink = $("#baselink_report").val();

    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    function fillter_change(){
        var WarehouseID = $("#WarehouseID").val();
        var Type = $("#Type").val();
        window.location="<?php echo current_url().'?WarehouseID=' ?>"+WarehouseID+"&Type="+Type;
    }


</script>
