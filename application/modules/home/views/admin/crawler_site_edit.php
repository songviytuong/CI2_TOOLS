<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>CRAWL <small> / Edit site</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
                        <input type="hidden" name="ID" value="<?php echo $data->ID ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Site Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="Site" value="<?php echo $data->Site ?>">
                            </div>
                        </div>
                        <?php 
                        $json = json_decode($data->Data,true);
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Title_code <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="Title" value='<?php echo isset($json['Title']) ? $json['Title'] : "" ; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description_code <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="Description" value='<?php echo isset($json['Description']) ? $json['Description'] : "" ; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categories_code <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="Categories" value='<?php echo isset($json['Categories']) ? $json['Categories'] : "" ; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Content_code <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" required="required" class="form-control col-md-7 col-xs-12" name="Content" value='<?php echo isset($json['Content']) ? $json['Content'] : "" ; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Thumb_code
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" class="form-control col-md-7 col-xs-12" name="Thumb" value='<?php echo isset($json['Thumb']) ? $json['Thumb'] : "" ; ?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Published</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Published" value="1" <?php echo $data->Published==1 ? "checked='checked'" : '' ; ?>> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Published" value="0" <?php echo $data->Published==0 ? "checked='checked'" : '' ; ?>> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
