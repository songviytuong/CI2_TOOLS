<div class="containner">
		<div class="import_orderlist">
			<div class="block2 row">
	    		<div class="block_2_1 col-xs-8">
	    			<a class="btn btn-primary" href="<?php echo $base_link.'add'; ?>"><i class="fa fa-plus"></i> KHÁCH HÀNG</a>
	    			<form action="<?php echo $base_link ?>setsessionsearch" method="post">
						<input type="text" name="Search" placeholder="Search..." style="height: 36px;padding: 5px 10px;border: 1px solid #ccc;float:left" />
						<button type="submit" class="btn btn-default" style="height: 36px;border-radius: 0px 5px 5px 0px;padding: 5px 10px;background: #EEE;border-left: none;"><i class="fa fa-search"></i> Fillter</button>
	                    <a href="<?php echo $base_link.'clearfilter' ?>" style='margin-left: 10px;text-decoration: underline;'>Clear Filter</a>
					</form>
	    		</div>
	    		<div class="clear"></div>
	    	</div>
	    	<div class="block3 table_data">
				<table id="table_data">
					<tr>
						<th>STT</th>
						<th>Tên khách hàng</th>
						<th>Số điện thoại</th>
						<th>Email</th>
						<th>Address</th>
						<th>Tỉnh thành</th>
						<th>Quận huyện</th>
					</tr>
					<?php 
					$arr_area = array();
					$arr_city = array();
					$arr_district = array();
					if(count($data)>0){
						foreach($data as $row){
							$arr_area[] = $row->AreaID;
							$arr_city[] = $row->CityID;
							$arr_district[] = $row->DistrictID;
						}
						$arr_city = count($arr_city) ? implode(',',$arr_city) : '0' ;
						$city = $this->db->query("select ID,Title from ttp_report_city where ID in($arr_city)")->result();
						$arr_city = array();
						if(count($city)>0){
							foreach($city as $row){
								$arr_city[$row->ID] = $row->Title ;
							}
						}
						$arr_district = count($arr_district) ? implode(',',$arr_district) : '0' ;
						$district = $this->db->query("select ID,Title from ttp_report_district where ID in($arr_district)")->result();
						$arr_district = array();
						if(count($district)>0){
							foreach($district as $row){
								$arr_district[$row->ID] = $row->Title ;
							}
						}
						$i=$start;
						foreach($data as $row){
							$area = isset($arr_area[$row->AreaID]) ? $arr_area[$row->AreaID] : '--' ;
							$city = isset($arr_city[$row->CityID]) ? $arr_city[$row->CityID] : '--' ;
							$district = isset($arr_district[$row->DistrictID]) ? $arr_district[$row->DistrictID] : '--' ;
							$i++;
							echo "<tr>";
							echo "<td style='width:30px;text-align:center;background:#F7F7F7'><a href='{$base_link}edit/$row->ID'>$i</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID' style='max-width:300px;'>$row->Name</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$row->Phone1</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$row->Email</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$row->Address</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$city</a></td>";
							echo "<td><a href='{$base_link}edit/$row->ID'>$district</a></td>";
							echo "</tr>";
						}
					}else{
						$keywords = $keywords!='' ? '"<b>'.$keywords.'</b>"' : $keywords ;
						echo "<tr><td colspan='8'>Không tìm thấy dữ liệu $keywords.</td></tr>";
					}
					?>
				</table>
				<?php if(count($data)>0) echo $nav; ?>
			</div>
		</div>
</div>
<style>
	.body_content .containner table tr td{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;}
	.body_content .containner table tr td:nth-child(5){max-width: 200px}
</style>
