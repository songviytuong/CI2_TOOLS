<?php 
class Crawler_setting extends Admin_Controller { 
	public $user;
	public $classname="crawler_setting";
	public $limit = 10;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
		$session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
		if($this->user->IsAdmin!=1){
			$this->lib->published_module($this->classname);
		}
		$this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user));
        $this->template->write_view('topnav','admin/topnav',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

	public function index(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
		$this->template->add_title('Bot setting | Crawler config');
		$object = $this->db->query("select * from ttp_crawler_setting where ID=1")->row();
		$json = $object ? json_decode($object->Data,true) : array() ;
		$data = array(
			'base_link' =>	base_url().ADMINPATH.'/home/crawler_setting/',
			'data'		=>	$json
		);
		$this->template->write_view('content','admin/crawler_setting_home',$data); 
		$this->template->render();
	}

	public function update(){
		$this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
		if(count($_POST)>0){
			foreach($_POST as $key=>$value){
				$data[$key] = $value;
			}
		}
		$this->db->where("ID",1);
		$this->db->update("ttp_crawler_setting",array("Data"=>json_encode($data)));	
		redirect(ADMINPATH.'/home/crawler_setting/');
	}
}
?>