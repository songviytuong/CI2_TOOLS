<script type="text/javascript" src="<?php echo base_url() ?>public/plugin/ckeditor/ckeditor.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<div class="containner opensitebar">
    <?php
    $currenttab = isset($_GET['tab']) ? $_GET['tab'] : 'tab1';
    ?>
    <div class="products_containner">
        <form action="<?php echo base_url() . ADMINPATH . "/report/warehouse_products/update" ?>" method="POST" id='products_form' enctype='multipart/form-data'>
            <div><input type='hidden' id="IDEdit" name="ID" value='<?php echo $data->ID ?>' /></div>
            <div class="head_products">
                <div class="block1">
                    <h1>THÔNG TIN SẢN PHẨM</h1>
                </div>
                <div class="block2">
                    <span>Loại sản phẩm : </span>
                    <?php
                    switch ($data->VariantType) {
                        case 0:
                            echo "<span>Sản phẩm đơn</span>";
                            break;
                        case 1:
                            echo "<span>Sản phẩm biến thể</span>";
                            break;
                        case 2:
                            echo "<span>Sản phẩm Bundle</span>";
                            break;
                        case 3:
                            echo "<span>Sản phẩm nhóm</span>";
                            break;
                        default:
                            break;
                    }
                    echo "<input type='hidden' name='VariantType' value='$data->VariantType' />";
                    ?>
                    <!--<select name="VariantType" id="VariantType">
                            <option value="0" <?php echo $data->VariantType == 0 ? "selected='selected'" : ''; ?>>Sản phẩm đơn</option>
                            <option value="1" <?php echo $data->VariantType == 1 ? "selected='selected'" : ''; ?>>Sản phẩm biến thể</option>
                            <option value="2" <?php echo $data->VariantType == 2 ? "selected='selected'" : ''; ?>>Sản phẩm Bundle</option>
                            <option value="3" <?php echo $data->VariantType == 3 ? "selected='selected'" : ''; ?>>Sản phẩm nhóm</option>
                    </select>-->
                </div>
                <div class="block3">
                    <a href="<?= ROOT_URL . '/' . $data->Alias . page_extension; ?>" target="_blank" class="btn btn-danger"> <i class="fa fa-eye"></i> View</a>
                    <?php if ($this->user->IsAdmin) : ?>
                        <a class="btn btn-danger delete" href="<?php echo base_url() . ADMINPATH . '/report/warehouse_products/delete/' . $data->ID ?>"><i class="fa fa-trash-o"></i> Delete</a>
                    <?php endif; ?>
                    <?php if ($this->user->IsAdmin) : ?>
                        <a class="btn btn-success" onclick="warning_message(this)" data="<?php echo base_url() . ADMINPATH . '/report/warehouse_products/dupplicate/' . $data->ID ?>"><i class="fa fa-files-o"></i> Copy</a>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
                        <a class="btn btn-info" onclick="saveall(this)"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Save & Close</a>
                    <?php elseif (in_array($this->user->ID, isWriterManager())): ?>
                        <button class="btn btn-primary" type="submit"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save</button>
                    <?php else: ?>
                        <?php if ($lastUpdate->Status != 3 || in_array($this->user->ID, array(21)) || !in_array($this->user->ID, isCollaborators())): ?>
                            <button class="btn btn-success" type="submit"><i class="fa fa-check-square-o" aria-hidden="true"></i> Save Draft</button>
                            <a class="btn btn-info" onclick="WaitApproval(this)"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Wait Approval</a>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>
            </div>
            <div class="content_products">
                <div class="control_tab">
                    <a data="tab1" <?php echo $currenttab == 'tab1' ? 'class="current"' : ''; ?>>Thông tin chung</a>
                    <a data="tab3" <?php echo $currenttab == 'tab3' ? 'class="current"' : ''; ?>>Website</a>
                    <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                        <a data="tab2" <?php echo $currenttab == 'tab2' ? 'class="current"' : ''; ?>>Giá bán</a>
                        <a data="tab5" <?php echo $currenttab == 'tab5' ? 'class="current"' : ''; ?>>Tồn kho</a>
                        <a data="tab6" <?php echo $currenttab == 'tab6' ? 'class="current"' : ''; ?>>Ngành hàng</a>
                        <a data="tab7" <?php echo $currenttab == 'tab7' ? 'class="current"' : ''; ?>>Thuộc tính</a>
                        <a data="tab8" <?php echo $currenttab == 'tab8' && $data->VariantType == 1 ? 'class="current"' : ''; ?> <?php echo $data->VariantType == 1 ? "style='display:inline-block'" : "style='display:none'"; ?>>Biến thể</a>
                        <a data="tab9" <?php echo $currenttab == 'tab9' ? 'class="current"' : ''; ?>>Dịch vụ quà tặng</a>
                        <a data="tab10" <?php echo $currenttab == 'tab10' && $data->VariantType >= 2 ? 'class="current"' : ''; ?> <?php echo $data->VariantType >= 2 ? "style='display:inline-block'" : "style='display:none'"; ?>>Gom nhóm</a>
                        <a data="tab12" <?php echo $currenttab == 'tab12' ? 'class="current"' : ''; ?> >CT Khuyến mại</a>
                    <?php endif; ?>
                    <a data="tab4" <?php echo $currenttab == 'tab4' ? 'class="current"' : ''; ?>>Hình ảnh</a>
                    <a data="tab11" <?php echo $currenttab == 'tab11' ? 'class="current"' : ''; ?>>Video</a>
                </div>
                <!-- Thông tin chung -->
                <div class="content_tab content_tab1" <?php echo $currenttab == 'tab1' ? 'style="display:block"' : 'style="display:none"'; ?>>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                    Mã sản phẩm (SKU)
                                </label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" name="MaSP" value='<?php echo $data->MaSP ?>' <?php if (!$this->user->IsAdmin) : ?>readonly=""<?php endif; ?>/>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                    Loại hàng
                                </label>
                                <div class="col-xs-8">

                                    <?php
                                    $productype = $this->lib->get_config_define("products", "productstype", 1, "code");
                                    ?>
                                    <select name="TypeProducts" class="form-control">
                                        <option value="-1" selected="selected">-- Select --</option>
                                        <?php
                                        foreach ($productype as $row) {
                                            $selected = $data->TypeProducts == $row->code ? "selected='selected'" : "";
                                            echo "<option value='$row->code' $selected>$row->name</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                    Tên sản phẩm (*)
                                </label>
                                <div class="col-xs-8">
                                    <input type="text" required="required" class="form-control" name="Title" id="TitleProducts" value="<?php echo str_replace("\'", "'", $data->Title) ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                    Url
                                </label>
                                <div class="col-xs-8"><input type="text" class="form-control" name="Alias" id="Alias" value="<?php echo str_replace("\'", "'", $data->Alias) ?>" /></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                    Tên viết tắt
                                </label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" name="ShortName" value="<?php echo str_replace("\'", "'", $data->ShortName) ?>" <?= (!empty($data->ShortName)) ? 'readonly' : '' ?>/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="col-xs-2 control-label">
                                    Mô tả ngắn (*)
                                </label>
                                <div class="col-xs-10">
                                    <?php
                                    $description = str_replace("\'", "'", $data->Description);
                                    $description = str_replace('"', "", $description);
                                    ?>
                                    <textarea class="form-control special_input" required="required" name="Description" placeholder="Yêu cầu bắt buộc nhập thông tin mô tả"><?php echo $description ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">
                                    Trọng lượng / Dung lượng / Package
                                </label>
                                <div class="col-xs-2">
                                    <input type="text" class="form-control" name="Weight" value='<?php echo $data->Weight ?>' />
                                </div>
                                <div class="col-xs-3">
                                    <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                                        <div class="input-group">
                                        <?php else: ?>
                                            <div class="form-group">
                                            <?php endif; ?>
                                            <select name="Capacity" class="form-control" id="Capacity">
                                                <option value="-1" selected="selected">-- Select --</option>
                                                <?php
                                                $unit_capacity = $this->db->query("select * from ttp_report_unit_capacity")->result();
                                                if (count($unit_capacity) > 0) {
                                                    foreach ($unit_capacity as $row) {
                                                        $selected = $data->Capacity == $row->Title ? "selected='selected'" : '';
                                                        echo "<option value='$row->Title' $selected>$row->Title</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" onclick="add_capacity_unit(this)"><i class="fa fa-plus"></i></button>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                                            <div class="input-group">
                                            <?php else: ?>
                                                <div class="form-group">
                                                <?php endif; ?>
                                                <select name="Donvi" class="form-control" id="Donvi">
                                                    <option value="-1" selected="selected">-- Select --</option>
                                                    <?php
                                                    $unit = $this->db->query("select * from ttp_report_unit")->result();
                                                    if (count($unit) > 0) {
                                                        foreach ($unit as $row) {
                                                            $selected = $data->Donvi == $row->Title ? "selected='selected'" : '';
                                                            echo "<option value='$row->Title' $selected>$row->Title</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" type="button" onclick="add_unit(this)"><i class="fa fa-plus"></i></button>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Kích thước (cm) (dài x rộng x cao)
                                        </label>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="Length" placeholder="Dài" value='<?php echo $data->Length ?>' <?php if (in_array($this->user->ID, isCollaborators())) : ?>readonly=""<?php endif; ?>/>
                                            </div>
                                        </div>
                                        <div class="col-xs-1 text-center"> x </div>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="Width" placeholder="Rộng" value='<?php echo $data->Width ?>' <?php if (in_array($this->user->ID, isCollaborators())) : ?>readonly=""<?php endif; ?>/>
                                            </div>
                                        </div>
                                        <div class="col-xs-1 text-center"> x </div>
                                        <div class="col-xs-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="Height" placeholder="Cao" value='<?php echo $data->Height ?>' <?php if (in_array($this->user->ID, isCollaborators())) : ?>readonly=""<?php endif; ?>/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Là SP mới từ ngày
                                        </label>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control" name="Startday" id="Input_Startday" placeholder="yyyy-mm-dd" value='<?php echo $data->NewStartDay != '0000-00-00' ? $data->NewStartDay : ''; ?>' <?php if (in_array($this->user->ID, isCollaborators())) : ?>readonly=""<?php endif; ?>/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Là SP mới đến ngày
                                        </label>
                                        <div class="col-xs-8">
                                            <input type="text" class="form-control" name="Stopday" id="Input_Stopday" placeholder="yyyy-mm-dd" value='<?php echo $data->NewStopDay != '0000-00-00' ? $data->NewStopDay : ''; ?>' <?php if (in_array($this->user->ID, isCollaborators())) : ?>readonly=""<?php endif; ?>/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Tình trạng 
                                            <?php
                                            if (count(json_decode($data->CategoriesID)) == 0) {
                                                echo " <small style='background-color:red; color:#fff; padding:3px;'>Chọn Ngành hàng !!!</small>";
                                            }
                                            ?>
                                        </label>
                                        <div class="col-xs-8">
                                            <select name="Published" class="form-control" <?php if (in_array($this->user->ID, isCollaborators())) : ?>disabled=""<?php endif; ?>>
                                                <option value="1" <?php echo $data->Published == 1 ? "selected='selected'" : ""; ?>>Kích hoạt</option>
                                                <option value="0" <?php echo $data->Published == 0 ? "selected='selected'" : ""; ?>>Ngưng kích hoạt</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Cho phép hiển thị
                                        </label>
                                        <div class="col-xs-8">
                                            <select name="Show" class="form-control" <?php if (in_array($this->user->ID, isCollaborators())) : ?>disabled=""<?php endif; ?>>
                                                <option value="1" <?php echo $data->Status == 1 ? "selected='selected'" : ""; ?>>Kích hoạt</option>
                                                <option value="0" <?php echo $data->Status == 0 ? "selected='selected'" : ""; ?>>Ngưng kích hoạt</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Nước sản xuất
                                        </label>
                                        <div class="col-xs-8">
                                            <select name="CountryID" class="form-control">
                                                <option value="-1" selected="">-- Select --</option>
                                                <?php
                                                $country = $this->db->query("select * from ttp_report_country")->result();
                                                if (count($country) > 0) {
                                                    foreach ($country as $row) {
                                                        $selected = $row->ID == $data->CountryID ? "selected='selected'" : '';
                                                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <label class="col-xs-4 control-label">
                                            Thương hiệu <?php if (!in_array($this->user->ID, isCollaborators())) : ?> <a href="administrator/report/warehouse_trademark" target="_blank" class="text-danger"><small><b>( Quản lý <i class="fa fa-arrow-circle-o-right"></i>)</b></small></a><?php else: ?><small class="text-danger">(Báo QL thêm nếu thiếu)</small><?php endif; ?>
                                        </label>
                                        <div class="col-xs-8">
                                            <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                                                <div class="input-group">
                                                <?php else: ?>
                                                    <div class="form-group">
                                                    <?php endif; ?>
                                                    <select name="TrademarkID" id="TrademarkID" class="selectpicker form-control" data-show-subtext="false" data-live-search="true">
                                                        <option value="-1" selected="">-- Select --</option>
                                                        <?php
                                                        $trademark = $this->db->query("select * from ttp_report_trademark GROUP BY Title order by Title ASC")->result();
                                                        if (count($trademark) > 0) {
                                                            foreach ($trademark as $row) {
                                                                $selected = ($row->ID == $data->TrademarkID) ? "selected='selected'" : '';
                                                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                    <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-default" type="button" onclick="add_trade(this)"><i class="fa fa-plus"></i></button>
                                                        </span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Nhà cung cấp
                                            </label>
                                            <div class="col-xs-8">
                                                <select name="SupplierID" class="form-control" <?php if (!$this->user->IsAdmin) : ?>disabled=""<?php endif; ?>>
                                                    <option value="-1" selected="">-- Select --</option>
                                                    <?php
                                                    $supplier = $this->db->query("select * from ttp_report_production")->result();
                                                    if (count($supplier) > 0) {
                                                        foreach ($supplier as $row) {
                                                            $selected = $data->SupplierID == $row->ID ? "selected='selected'" : '';
                                                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Sản phẩm bán chạy
                                            </label>
                                            <div class="col-xs-8">
                                                <select name="Chimmoi" class="form-control" <?php if (in_array($this->user->ID, isCollaborators())) : ?>disabled=""<?php endif; ?>>
                                                    <option value='0' <?php echo $data->Chimmoi == 0 ? "selected='selected'" : ''; ?>>No</option>
                                                    <option value='1' <?php echo $data->Chimmoi == 1 ? "selected='selected'" : ''; ?>>Yes</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-xs-6 ">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Sức chứa (nếu là thùng)
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="number" class="form-control" name="Tankage" value="<?php echo $data->Tankage ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Sắp xếp tt khuyến mãi
                                            </label>
                                            <div class="col-xs-2">
                                                <input type="number" class="form-control" name="Orders" value="<?php echo $data->Orders ?>" />
                                            </div>
                                            <label class="col-xs-4 control-label text-center">
                                                Sắp xếp thứ tự cate
                                            </label>
                                            <div class="col-xs-2">
                                                <input type="number" class="form-control" name="OrdersCat" value="<?php echo $data->OrdersCat ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Nhóm SP (Thêm)
                                            </label>
                                            <div class="col-xs-8">
                                                <select name="AddOnCategories" class="form-control" <?php if (in_array($this->user->ID, isCollaborators())) : ?>disabled=""<?php endif; ?>>
                                                    <option value="-1" selected="">-- Select --</option>
                                                    <option value='1' <?php echo $data->AddOnCategories == 1 ? "selected='selected'" : ''; ?>>Phi thực phẩm</option>
                                                    <option value='2' <?php echo $data->AddOnCategories == 2 ? "selected='selected'" : ''; ?>>Thực phẩm tươi sống</option>
                                                    <option value='3' <?php echo $data->AddOnCategories == 3 ? "selected='selected'" : ''; ?>>Hàng tiêu dùng nhanh</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Barcode từ nhà cung cấp</label>
                                            <div class="col-xs-8"><input type="text" class="form-control" name="Barcode" value="<?php echo $data->Barcode ?>"  <?php if (!$this->user->IsAdmin) : ?>readonly=""<?php endif; ?>/></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!--                                    <div class="col-xs-6">
                                                                            <div class="form-group">
                                                                                <label class="col-xs-4 control-label">Barcode hệ thống</label>
                                                                                <label class="col-xs-8 control-label"></label>
                                                                            </div>
                                                                        </div>-->

                                    <div class="col-xs-6 hidden">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Chương trình KM (Tabs)</label>
                                            <label class="col-xs-8 control-label">
                                                <select name="TabsPromotion[]" class="form-control">
                                                    <option value="-1" selected="">-- Select --</option>
                                                    <?php
                                                    $this->load->model('promotions_model', 'promotions');
                                                    $data_promotions = $this->promotions->getPromotions();
                                                    foreach ($data_promotions as $key => $row) {
                                                        $ids = $this->db->query("SELECT TabsPromotion FROM ttp_report_products WHERE ID = $data->ID")->row()->TabsPromotion;
                                                        $ids = ($ids) ? json_decode($ids) : array();
                                                        ?>
                                                        <option value='<?= $row['ID']; ?>' <?= (in_array($row['ID'], $ids)) ? 'selected=selected' : ''; ?>><?= $row['Name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Image Barcode</label>
                                            <label class="col-xs-8 control-label">
                                                <?php
                                                $barcode_arr = $data->Barcode;
                                                $barcode_arr = explode(",", $barcode_arr);
                                                foreach ($barcode_arr as $barc) {
                                                    ?>
                                                    <img src='<?php echo base_url() . ADMINPATH . '/report/warehouse_products/set_barcode/' . $barc ?>' />
                                                    <?php
                                                }
                                                ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                    if ($data->BarcodeClient != '') {
                                        ?>
                                        <!--                                        <div class="col-xs-6">
                                                                                    <div class="form-group">
                                                                                        <label class="col-xs-4 control-label">Image Barcode</label>
                                                                                        <label class="col-xs-8 control-label"><img src='<?php echo base_url() . ADMINPATH . '/report/warehouse_products/set_barcode/' . $data->BarcodeClient ?>' /></label>
                                                                                    </div>
                                                                                </div>-->
                                    <?php } ?>
                                </div>
                                <?php
                                if (in_array($this->user->ID, isWriterManager())) {
                                    ?>
                                    <div class="row">
                                        <div class="col-xs-12 btn-group">
                                            <?php if ($lastUpdate->Status == 3): ?>
                                                <button type="button" class="btn btn-primary btn-sm" onclick="approved('<?php echo $data->ID ?>', '<?php echo substr(md5($data->ID . 'approved'), 27, 06); ?>')"><i class="fa fa-check-circle"></i> Approved</button>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="rejected('<?php echo $data->ID ?>', '<?php echo substr(md5($data->ID . 'rejected'), 27, 06); ?>')"><i class="fa fa-close"></i> Rejected</button>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <?php
                                        $history = $this->db->query("select * from ttp_report_products_history where ProductsID=$data->ID order by ID DESC LIMIT 0,15")->result();
                                        if (count($history) > 0) {
                                            echo "<table id='table_data' class='table table-bordered'>
							    			<tr style='background-color:#F6F6f6'>
							    				<th class=\"col-xs-2\">Last Edited</th>
							    				<th class=\"col-xs-1\">Owner</th>
							    				<th class=\"col-xs-1\">Action</th>
                                                                                        <th class=\"col-xs-8\">History</th>";
                                            if ($this->user->IsAdmin) {
                                                echo "<th class=\"col-xs-1\"></th>";
                                            }
                                            echo "</tr>";
                                            foreach ($history as $row) {
                                                $this->load->model('users_model', 'users');
                                                $Users = $this->users->defineUsers(array('userid' => ($row->Owner) ? $row->Owner : 1));
                                                echo "<tr>
                                                    <td>" . date('d/m/Y H:i:s', strtotime($row->Created)) . "</td>
                                                        <td>" . ($Users['FirstName']) . "</td>
                                                        <td>" . ((($row->Type == 'Approved') ? '<font color="blue">' . $row->Type . '</font>' : (($row->Type == 'Rejected') ? '<font color="red">' . $row->Type . '</font>' : $row->Type))) . "</td>
                                                        <td>" . $row->Notes . "</td>";
                                                if ($this->user->IsAdmin) {
                                                    echo "<td class=\"text-center\"><a title=\"Xóa sản phẩm này\" href=\"/administrator/report/warehouse_products/deletehistory/" . $row->ID . "\"><i class=\"fa fa-trash-o\"></i></a></td>";
                                                }
                                                echo "</tr>";
                                            }
                                            echo "</table>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!-- Giá bán -->
                            <div class="content_tab content_tab2" <?php echo $currenttab == 'tab2' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá bán</label>
                                            <div class="col-xs-8">
                                                <input type="text" name="Price" class="form-control" value="<?php echo $data->Price ?>" required />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá mua</label>
                                            <div class="col-xs-8">
                                                <input type="text" name="RootPrice" class="form-control" value="<?php echo $data->RootPrice ?>" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Thuế VAT</label>
                                            <div class="col-xs-8">
                                                <select name="VAT" class="form-control">
                                                    <option value="0" <?php echo $data->VAT == 0 ? "selected='selected'" : ''; ?>>Không có</option>
                                                    <option value="5" <?php echo $data->VAT == 5 ? "selected='selected'" : ''; ?>>5%</option>
                                                    <option value="10" <?php echo $data->VAT == 10 ? "selected='selected'" : ''; ?>>10%</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá đặc biệt</label>
                                            <div class="col-xs-8">
                                                <input type="text" name="SpecialPrice" class="form-control" value="<?php echo $data->SpecialPrice ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Từ ngày</label>
                                            <div class="col-xs-8">
                                                <input type="text" name="Special_startday" class="form-control" id="Input_Special_startday" placeholder="yyyy-mm-dd" value='<?php echo $data->SpecialStartday != '0000-00-00' ? $data->SpecialStartday : ''; ?>' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Đến ngày</label>
                                            <div class="col-xs-8">
                                                <input type="text" name="Special_stopday" class="form-control" id="Input_Special_stopday" placeholder="yyyy-mm-dd" value='<?php echo $data->SpecialStopday != '0000-00-00' ? $data->SpecialStopday : ''; ?>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá đ/vị chuẩn</label>
                                            <div class="col-xs-8">
                                                <input type="text" name="BasePrice" class="form-control" placeholder="ví dụ : 200,000 / kg" value='<?php echo $data->BasePrice; ?>' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá thị trường</label>
                                            <div class="col-xs-8">
                                                <input type="number" name="SocialPrice" class="form-control" placeholder="" value='<?php echo $data->SocialPrice; ?>' />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a data-toggle="tab" href="#home">Lịch sử cập nhật giá</a></li>
                                            <li><a data-toggle="tab" href="#menu1">Lịch sử cập nhật giá đối thủ</a></li>
                                            <li><a data-toggle="tab" href="#menu2">Lịch sử cập nhật giá đặc biệt</a></li>
                                        </ul>

                                        <div class="tab-content" style="padding-top:20px">
                                            <div id="home" class="tab-pane fade in active">
                                                <?php
                                                $pricetable = $this->db->query("select * from ttp_report_products_price_analytics where ProductsID=$data->ID order by ID DESC")->result();
                                                if (count($pricetable) > 0) {
                                                    echo "<table class='table table-bordered'>
							    			<tr>
							    				<th>Ngày cập nhật</th>
							    				<th>Giá bán ra</th>
							    				<th>Giá mua vào</th>
							    				<th>VAT</th>
							    			</tr>";
                                                    foreach ($pricetable as $row) {
                                                        $price = date('Y-m-d') >= date('Y-m-d', strtotime($row->SpecialStartday)) && date('Y-m-d') <= date('Y-m-d', strtotime($row->SpecialStopday)) ? $row->SpecialPrice : $row->Price;
                                                        echo "<tr>
							    				<td>" . date('d/m/Y H:i', strtotime($row->Created)) . "</td>
							    				<td>" . number_format($price) . "</td>
							    				<td>" . number_format($row->RootPrice) . "</td>
							    				<td>" . $row->VAT . "%</td>
							    			</tr>";
                                                    }
                                                    echo "</table>";
                                                }
                                                ?>
                                            </div>
                                            <div id="menu1" class="tab-pane fade">
                                                <?php
                                                if (count($pricetable) > 0) {
                                                    echo "<table class='table table-bordered'>
							    			<tr>
							    				<th>Ngày cập nhật</th>
							    				<th>Giá mua vào</th>
							    				<th>Giá bán ra</th>
							    				<th>Giá thị trường</th>
							    			</tr>";
                                                    foreach ($pricetable as $row) {
                                                        $price = date('Y-m-d') >= date('Y-m-d', strtotime($row->SpecialStartday)) && date('Y-m-d') <= date('Y-m-d', strtotime($row->SpecialStopday)) ? $row->SpecialPrice : $row->Price;
                                                        echo "<tr>
							    				<td>" . date('d/m/Y H:i', strtotime($row->Created)) . "</td>
							    				<td>" . number_format($row->RootPrice) . "</td>
							    				<td>" . number_format($price) . "</td>
							    				<td>" . number_format($row->SocialPrice) . "</td>
							    			</tr>";
                                                    }
                                                    echo "</table>";
                                                }
                                                ?>
                                            </div>
                                            <div id="menu2" class="tab-pane fade">
                                                <?php
                                                if (count($pricetable) > 0) {
                                                    echo "<table class='table table-bordered'>
							    			<tr>
							    				<th>Ngày cập nhật</th>
							    				<th>Giá bán</th>
							    				<th>Giá bán đặc biệt</th>
							    				<th>Bán từ ngày</th>
							    				<th>Bán đến ngày</th>
							    			</tr>";
                                                    foreach ($pricetable as $row) {
                                                        echo "<tr>
							    				<td>" . date('d/m/Y H:i', strtotime($row->Created)) . "</td>
							    				<td>" . number_format($row->Price) . "</td>
							    				<td>" . number_format($row->SpecialPrice) . "</td>
							    				<td>" . date('d/m/Y', strtotime($row->SpecialStartday)) . "</td>
							    				<td>" . date('d/m/Y', strtotime($row->SpecialStopday)) . "</td>
							    			</tr>";
                                                    }
                                                    echo "</table>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p><b>Lịch sử cập nhật giá bán lẻ đối thủ</b></p>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>Ngày</td>
                                                <td>Tên đối thủ 01</td>
                                                <td>Tên đối thủ 02</td>
                                                <td>Tên đối thủ 03</td>
                                                <td>Tên đối thủ 04</td>
                                                <td>Tên đối thủ 05</td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">Chưa có dữ liệu</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Nhóm giá : </label>
                                            <label class="col-xs-8 control-label">Nhóm khách hàng</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá bán</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="PriceGroup">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"></label>
                                            <label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceGroup_Title[]" /></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"><input type="text" class="form-control" name="PriceGroup_Price[]" /></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"></label>
                                            <a class='btn btn-default' onclick="add_PriceGroup()"><i class="fa fa-plus"></i> Thêm nhóm giá</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Bước giá : </label>
                                            <label class="col-xs-8 control-label">Nhóm khách hàng</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Giá bán</label>
                                            <label class="col-xs-1 control-label"></label>
                                            <label class="col-xs-2 control-label">Số lượng</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="PriceRange">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"></label>
                                            <label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceRange_Title[]" /></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"><input type="text" name="PriceRange_Price[]" class="form-control" /></label>
                                            <label class="col-xs-1 control-label"></label>
                                            <label class="col-xs-1 control-label"><input type="number" min="0" class="form-control" name="PriceRange_Amount[]" value="0" /></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label"></label>
                                            <a class='btn btn-default' onclick="add_PriceRange()"><i class="fa fa-plus"></i> Thêm bước giá</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Thẻ Meta -->
                            <div class="content_tab content_tab3" <?php echo $currenttab == 'tab3' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Sản phẩm tiêu biểu</label>
                                            <div class="col-xs-2">
                                                <select class="form-control" name="Featured">
                                                    <option value="0" <?php echo $data->Featured == 0 ? "selected='selected'" : ''; ?>>Disable</option>
                                                    <option value="1" <?php echo $data->Featured == 1 ? "selected='selected'" : ''; ?>>Enable</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Meta Title</label>
                                            <div class="col-xs-10">
                                                <input type="text" name="MetaTitle" class="form-control" value="<?php echo str_replace("\'", "'", $data->MetaTitle) ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Meta Keywords</label>
                                            <div class="col-xs-10">
                                                <input type="text" name="MetaKeywords" class="form-control" value="<?php echo str_replace("\'", "'", $data->MetaKeywords) ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Meta Description</label>
                                            <div class="col-xs-10">
                                                <input type="text" name="MetaDescription" class="form-control" value="<?php echo str_replace('"', '', $data->MetaDescription) ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if ($data->VariantType == 3) {
                                    echo "<hr><h4>Thành phần cần thiết</h4><hr>";
                                    echo '<div class="row" style="margin-bottom:10px">
								<div class="col-xs-10">
									<input type="text" id="list_ingredient" class="form-control" placeholder="Phân cách các thành phần bằng dấu phẩy(,)"></input>
								</div>
								<div class="col-xs-2">
									<a class="btn btn-default" onclick="add_ingredients(this)"><i class="fa fa-plus"></i> Thêm</a>
								</div>
							</div>
						<div class="row">
						<div class="col-xs-12">
							<div class="form-group" id="Ingredients">';
                                    $Ingredients = $this->db->query("select * from ttp_report_products_ingredients where ProductsID=$data->ID")->result();
                                    if (count($Ingredients) > 0) {
                                        foreach ($Ingredients as $row) {
                                            echo "<div style='float:left;margin-right:30px;margin-bottom:10px'><a onclick='remove_ingredients(this,$row->ID)'><i class='fa fa-times-circle' aria-hidden='true' style='color:#c1c1c1;font-size:15px;margin-right:5px;'></i></a><a onclick='changeIngredients(this,$row->ID)'>$row->Title</a></div>";
                                        }
                                    }
                                    echo '</div></div></div>';
                                }

                                if ($data->VariantType == 3) {
                                    echo "<h4 style='clear:both;padding-top:20px;'>Giá trị dinh dưỡng</h4><hr>";
                                    echo '<div class="row">
						<div class="col-xs-12">
							<div class="form-group">';
                                    $nutrition = $this->db->query("select * from ttp_report_nutrition_facts")->result();
                                    $current_nutrition = $this->db->query("select * from ttp_report_products_nutrition_facts where ProductsID=$data->ID")->result();
                                    $arr_current_nutrition = array();
                                    if (count($current_nutrition) > 0) {
                                        foreach ($current_nutrition as $row) {
                                            $arr_current_nutrition[$row->NutritionID] = array('data' => $row->Data, 'percent' => $row->Percent);
                                        }
                                    }
                                    if (count($nutrition) > 0) {
                                        foreach ($nutrition as $row) {
                                            $data_row = isset($arr_current_nutrition[$row->ID]['data']) ? $arr_current_nutrition[$row->ID]['data'] : '';
                                            $data_percent = isset($arr_current_nutrition[$row->ID]['percent']) ? $arr_current_nutrition[$row->ID]['percent'] : '';
                                            echo "<div class='col-xs-4' style='margin-bottom:10px'>
									<label class='control-label col-xs-5'><b>$row->Title</b></label>
									<div class='col-xs-7'>
										<div class='col-xs-5' style='margin-right:5px'>
											<input type='text' name='nutrition_value[]' class='form-control' value='$data_row' placeholder='...' />
										</div>
										<div class='col-xs-1'></div>
										<div class='col-xs-5'>
											<input type='number' name='nutrition_percent[]' placeholder='%' value='$data_percent' class='form-control' />
										</div>
									</div>
								</div>";
                                        }
                                    }
                                    echo "</div></div></div>";
                                }
                                ?>
                                <div class="row hidden">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="col-xs-2 control-label">Hướng dẫn sử dụng sản phẩm</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">
                                    <div class="col-xs-12">
                                        <textarea class="resizable_textarea form-control ckeditor" name="Instruction"><?php echo $data->Instruction ?></textarea>
                                    </div>
                                </div>
                                <!--h4 style='clear:both;padding-top:20px;'>Nội dung mô tả sản phẩm <a class="btn btn-default pull-right" onclick="preview()"><i class="fa fa-search"></i> Xem trước</a></h4-->
                                <hr>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <textarea class="resizable_textarea form-control ckeditor" name="Content"><?php echo $data->Content ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- Hình ảnh -->
                            <div class="content_tab content_tab4" <?php echo $currenttab == 'tab4' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <table>
                                    <tr>
                                        <td>Hình ảnh</td>
                                        <td>Nhãn cho ảnh</td>
                                        <td>Thứ tự</td>
                                        <td>Ảnh chính</td>
                                        <td>Ảnh nhỏ</td>
                                        <td>Ảnh thumbnail</td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    $images = $this->db->query("select * from ttp_report_images_products where ProductsID=$data->ID")->result();
                                    if (count($images) > 0) {
                                        $i = 0;
                                        foreach ($images as $row) {
                                            $checked_primary = $row->PrimaryImage == 1 ? "checked='checked'" : '';
                                            $checked_small = $row->MiniImage == 1 ? "checked='checked'" : '';
                                            $checked_thumb = $row->ThumbImage == 1 ? "checked='checked'" : '';
                                            echo "<tr class='imagerow_tr imagerow_tr$i' data='$row->ID'>";
                                            echo file_exists($this->lib->get_thumb($row->Url)) ? "<td><div class='image_row'><img src='" . $this->lib->get_thumb($row->Url) . "' /> <i class='fa fa-chain-broken'></i> <input type='file' onchange='changeimagerow(this)' data='$row->ID' /><div class='progress'><div class='bar'></div></div></div></td>" : "<td></td>";
                                            echo "<td><input class='label_tr form-control' type='text' value='$row->Label' /></td>";
                                            echo "<td><input class='stt_tr form-control' type='number' value='$row->STT' /></td>";
                                            echo "<td class='text-center'><input $checked_primary class='primaryimage_tr' name='primaryimage_tr' type='radio' value='$row->ID' /></td>";
                                            echo "<td class='text-center'><input $checked_small class='smallimage_tr' name='smallimage_tr' type='radio' value='$row->ID' /></td>";
                                            echo "<td class='text-center'><input $checked_thumb class='thumbimage_tr' name='thumbimage_tr' type='radio' value='$row->ID' /></td>";
                                            echo "<td><a title='Xóa hình ảnh này' onclick='rm_image_products(this,$row->ID)'><i class='fa fa-times'></i></a></td>";
                                            echo "</tr>";
                                            $i++;
                                        }
                                    } else {
                                        echo "<tr><td colspan='7'>Không có hình ảnh cho sản phẩm này</td></tr>";
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="7">
                                            <div class="input-group-btn text-right">
                                                <a class='btn btn-success last-a' style="display:none"><i class="fa fa-check-circle"></i> Lưu thành công</a>
                                                <a class='btn btn-default save_update_image' onclick="active_save_image(this, 0)"><i class="fa fa-refresh"></i> Lưu cập nhật</a>
                                                <a class="btn btn-default activeupload" onclick="activeupload(this, 'filerow')"><i class="fa fa-upload" aria-hidden="true"></i> Đăng tải hình</a>
                                                <a class="btn btn-primary btn-file choosefile">
                                                    <i class="fa fa-folder-open" aria-hidden="true"></i> Browse...
                                                    <input type="file" name="Images_upload[]" id="choosefile" multiple />
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="dvPreview"></div>
                            </div>
                            <!-- Video -->
                            <?php
                            $video = json_decode($data->Video);
                            ?>
                            <div class="content_tab content_tab11" <?php echo $currenttab == 'tab11' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Mã video (Youtube)
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="text" name="YoutubeCode" class="form-control" value="<?php echo isset($video->YoutubeCode) ? $video->YoutubeCode : ''; ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                hoặc Upload video
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="file" name="VideoSource" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <label class="col-xs-4 control-label">
                                            Hình đại diện video
                                        </label>
                                        <div class="col-xs-8">
                                            <input type="file" name="ImageVideo" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <div align="center" class="embed-responsive embed-responsive-16by9">
                                                <video class="embed-responsive-item">
                                                    <source src="<?php echo isset($video->VideoSource) ? $video->VideoSource : ''; ?>" type="video/mp4">
                                                </video>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <img class="img-responsive" src="<?php echo isset($video->ImageVideo) ? $video->ImageVideo : ''; ?>" />
                                    </div>
                                </div>
                            </div>
                            <!-- Tồn kho -->
                            <div class="content_tab content_tab5" <?php echo $currenttab == 'tab5' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Số lượng hiện tại
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="number" min="0" name="SLCurrent" class="form-control" value="<?php echo $data->CurrentAmount ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                SL báo hết hàng
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="number" min="0" name="SLOutOfStock" class="form-control" value="<?php echo $data->WarningAmount ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                SL tối thiểu trong giỏ hàng
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="number" min="0" name="SLMinInCart" class="form-control" value="<?php echo $data->MinCartAmount ?>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                SL tối đa trong giỏ hàng
                                            </label>
                                            <div class="col-xs-8">
                                                <input type="number" min="0" name="SLMaxInCart" class="form-control" value="<?php echo $data->MaxCartAmount ?>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Cho phép đặt hàng âm
                                            </label>
                                            <div class="col-xs-8">
                                                <select name="AddcartNegativeNumber" class="form-control">
                                                    <option value="1" <?php echo $data->PutcartNegative == 1 ? "selected='selected'" : ''; ?>>Yes</option>
                                                    <option value="0" <?php echo $data->PutcartNegative == 0 ? "selected='selected'" : ''; ?>>No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">
                                                Tình trạng tồn kho
                                            </label>
                                            <div class="col-xs-8">
                                                <select name="Available" class="form-control">
                                                    <option value="1" <?php echo $data->InventoryStatus == 1 ? "selected='selected'" : ''; ?>>Còn hàng</option>
                                                    <option value="0" <?php echo $data->InventoryStatus == 0 ? "selected='selected'" : ''; ?>>Hết hàng</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Nhóm ngành hàng -->
                            <div class="content_tab content_tab6" <?php echo $currenttab == 'tab6' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div>
                                    <?php
                                    $arr_catgories = json_decode($data->CategoriesID, true);
                                    $arr_catgories = is_array($arr_catgories) ? implode(',', $arr_catgories) : '0';
                                    $arr_catgories = $arr_catgories == '' ? "(0)" : "($arr_catgories)";
                                    $current_categories = $this->db->query("select ID,Title from ttp_report_categories where ID in $arr_catgories")->result();
                                    if (count($current_categories) > 0) {
                                        echo "<h4>Nhóm ngành hàng đã chọn:</h4>";
                                        echo "<div class='current_categories'>";
                                        foreach ($current_categories as $row) {
                                            echo "<a><input type='checkbox' checked='checked' name='CategoriesID[]' value='$row->ID' /> $row->Title <span><i class='fa fa-times' onclick='remove_categories(this)'></i></span></a>";
                                        }
                                        echo "</div>";
                                    }
                                    $categories = $this->db->query("select Title,ID from ttp_report_categories where ParentID=0")->result();
                                    if (count($categories) > 0) {
                                        echo "<h4>Chọn thêm ngành hàng:</h4>";
                                        echo "<ul>";
                                        foreach ($categories as $row) {
                                            echo "<li><span onclick='show_child_next(this)' data='$row->ID'>+</span><input type='checkbox' value='$row->ID' name='CategoriesID[]' /> $row->Title</li>";
                                        }
                                        echo "</ul>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- Thuộc tính -->
                            <div class="content_tab content_tab7" <?php echo $currenttab == 'tab7' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4>Danh sách thuộc tính</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="input-group">
                                            <a class="input-group-addon" onclick="select_properties()"><i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> Chọn thuộc tính</a>
                                            <select id="PropertiesID" class="form-control">
                                                <?php
                                                $properties = $this->db->query("select ID,Title from ttp_report_properties where ParentID=0")->result();
                                                $arr_name_parent = array();
                                                if (count($properties) > 0) {
                                                    foreach ($properties as $row) {
                                                        echo "<option value='$row->ID'>$row->Title</option>";
                                                        $arr_name_parent[$row->ID] = $row->Title;
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <span class="input-group-btn">
                                                <a class="btn btn-default" onclick="senddata('add_properties', 'Thêm thuộc tính mới', '')"><i class="fa fa-plus"></i> Tạo mới</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4"><h4>Tên thuộc tính</h4></div>
                                    <div class="col-xs-8"><h4>Giá trị thuộc tính</h4></div>
                                </div>
                                <?php
                                $Properties = json_decode($data->VariantValue, true);
                                $TemPropertiesID = isset($Properties['Properties']) ? $Properties['Properties'] : array();
                                $PropertiesID = count($TemPropertiesID) > 0 ? '(' . implode(',', $TemPropertiesID) . ')' : '(0)';
                                $PropertiesPublished = isset($Properties['Published']) ? $Properties['Published'] : array();
                                $current_parent_properties = $this->db->query("select ID,Title,ParentID from ttp_report_properties where ParentID in (select DISTINCT ParentID from ttp_report_properties where ID in $PropertiesID)")->result();
                                $arr_parent = array();
                                $arr_parent_js = array();
                                if (count($current_parent_properties) > 0) {
                                    foreach ($current_parent_properties as $row) {
                                        $selected = in_array($row->ID, $TemPropertiesID) ? 'checked="checked"' : '';
                                        if (isset($arr_parent[$row->ParentID])) {
                                            $arr_parent[$row->ParentID] .= "<div class='col-xs-3'><input class='valuekey$row->ParentID' type='checkbox' value='$row->ID' name='PropertiesID[]' $selected /> $row->Title</div>";
                                        } else {
                                            $arr_parent_js[] = '"data' . $row->ParentID . '"';
                                            $arr_parent[$row->ParentID] = "<div class='col-xs-3'><input class='valuekey$row->ParentID' type='checkbox' value='$row->ID' name='PropertiesID[]' $selected /> $row->Title</div>";
                                        }
                                    }
                                }
                                if (count($arr_parent) > 0) {
                                    foreach ($arr_parent as $key => $value) {
                                        $checked = in_array($key, $PropertiesPublished) ? "checked='checked'" : '';
                                        echo '<div class="row">
								<div class="col-xs-4 properties_variant"><b>' . $arr_name_parent[$key] . '</b> <br> <input type="checkbox" name="PropertiesPublished[]" value="' . $key . '" ' . $checked . ' /> Có thể nhìn thấy trên trang sản phẩm</div>
								<div class="col-xs-8 value_properties_variant">
									<div class="sub_row valuekey value' . $key . '">
										' . $value . '
									</div>
									<div class="sub_row row input-group-btn">
										<a class="btn btn-default" onclick="check_full_rowtab(this,' . $key . ')"><i class="fa fa-check-square-o"></i> Chọn tất cả</a>
										<a class="btn btn-default" onclick="uncheck_full_rowtab(this,' . $key . ')"><i class="fa fa-times"></i> Bỏ chọn tất cả</a>
										<a class="btn btn-default" onclick="remove_rowtab(this)"><i class="fa fa-trash-o"></i> Bỏ chọn thuộc tính</a>
										<a class="btn btn-primary" onclick="senddata(\'add_value_properties\',\'Thêm giá trị thuộc tính mới\',' . $key . ')"><i class="fa fa-plus"></i> Tạo mới giá trị</a>
									</div>
								</div>
							</div>';
                                    }
                                } else {
                                    echo '<div class="message_empty">Sản phẩm này chưa có thuộc tính . Vui lòng chọn thuộc tính cho sản phẩm này .</div>';
                                }
                                ?>
                            </div>
                            <!-- Biến thể -->
                            <div class="content_tab content_tab8" <?php echo $currenttab == 'tab8' && $data->VariantType == 1 ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div class="row">
                                    <a class="btn btn-danger" onclick="AcceptVariant(this, 0)"><i class="fa fa-plus"></i> Tạo biến thể mới</a>
                                </div>
                                <?php
                                $variant_list = $this->db->query("select * from ttp_report_products where ParentID=$data->ID")->result();
                                if (count($variant_list) > 0) {
                                    foreach ($variant_list as $row) {
                                        $this->load->view("warehouse_products_variant_add", array('parentdata' => $data, 'data' => $row));
                                    }
                                }
                                ?>
                            </div>
                            <!-- Dịch vụ quà tặng -->
                            <div class="content_tab content_tab9" <?php echo $currenttab == 'tab9' ? 'style="display:block"' : 'style="display:none"'; ?>>
                                <div>
                                    <div>
                                        <input type='checkbox' name="AcceptGift" <?php echo $data->AcceptGift == 1 ? "checked='checked'" : ''; ?> /> Cho phép gói quà
                                    </div>
                                    <div>
                                        <p>Chi phí gói quà</p>
                                        <input type='text' name="SaleGift" value='<?php echo $data->SaleGift ?>' />
                                    </div>
                                </div>
                            </div>
                            <!-- Gom nhóm sản phẩm bundel -->
                            <div class="content_tab content_tab10" <?php echo $currenttab == 'tab10' && $data->VariantType == 2 ? 'style="display:block;min-height:800px"' : 'style="display:none"'; ?>>
                                <h4>Danh sách sản phẩm thuộc nhóm <a class="btn btn-default pull-right" onclick="show_productsbox(this)"><i class="fa fa-plus"></i> Thêm sản phẩm</a></h4>
                                <div class="row" id="bundle-list">
                                    <?php
                                    $products_group = $this->db->query("select b.PrimaryImage,b.Title,b.Price,a.Price as PriceCombo,a.Quantity,a.ID as BundleID,a.MusHave from ttp_report_products_bundle a,ttp_report_products b where Src_ProductsID=$data->ID and a.Des_ProductsID=b.ID")->result();
                                    if (count($products_group) > 0) {
                                        foreach ($products_group as $row) {
                                            $active = $row->MusHave == 1 ? "active" : "";
                                            echo '<div class="col-xs-4">
										<div class="form-group panel panel-default" style="overflow:hidden">
											<div class="col-xs-3">
												<img class="img-responsive" src="' . $row->PrimaryImage . '" style="width:90%" />
												<a class="' . $active . '" onclick="mushave(this,' . $row->MusHave . ',' . $row->BundleID . ')"><i class="fa fa-check-square" aria-hidden="true"></i> <br>Bắt buộc</a>
											</div>
											<div class="col-xs-9">
												<p>' . $row->Title . '</p>
												<p class="text-danger"><span style="color:#555">Giá bán lẻ : </span>' . number_format($row->Price) . 'đ</p>
												<div class="col-xs-3" style="clear:both"><input class="form-control" type="number" value="' . $row->Quantity . '" onchange="updatequantitybundle(this,' . $row->BundleID . ')" /></div>
												<div class="col-xs-6" style="margin-left: 5px;">
													<input type="number" value="' . $row->PriceCombo . '" class="form-control" onchange="updatepricebundle(this,' . $row->BundleID . ')" />
												</div>
												<div class="col-xs-2" style="margin-left: 5px;"><a class="pull-right btn btn-default" title="Loại bỏ sản phẩm này ra khỏi nhóm" onclick="removeoutbundle(this,' . $row->BundleID . ')"><i class="fa fa-times"></i></a>
												</div>
											</div>
										</div>
									</div>';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="content_tab content_tab12" <?php echo $currenttab == 'tab12' ? 'style="display:block;min-height:800px"' : 'style="display:none"'; ?>>
                                <div>
                                    <?php
                                    $arr_promotions = json_decode($data->PromotionsID, true);
                                    $arr_promotions = is_array($arr_promotions) ? implode(',', $arr_promotions) : '0';
                                    $arr_promotions = $arr_promotions == '' ? "(0)" : "($arr_promotions)";
                                    $current_promotions = $this->db->query("select ID,Name from ttp_report_promotions where ID in $arr_promotions")->result();
                                    if (count($current_promotions) > 0) {
                                        echo "<h4>Chương trình KM đang được áp dụng:</h4>";
                                        echo "<div class='current_promotions'>";
                                        foreach ($current_promotions as $row) {
                                            echo "<a><input type='checkbox' checked='checked' name='PromotionsID[]' value='$row->ID' /> $row->Name <span><i class='fa fa-times' onclick='remove_categories(this)'></i></span></a>";
                                        }
                                        echo "</div>";
                                    }
                                    $promotions = $this->db->query("select Name,ID,Start,End from ttp_report_promotions where Status in (0,1,2)")->result();
                                    if (count($promotions) > 0) {

                                        echo "<h4>Chọn thêm CT Khuyến mại:</h4>";
                                        echo "<ul>";
                                        foreach ($promotions as $row) {
                                            $dt = new DateTime();
                                            $now = $dt->format('Y-m-d H:i:s');
                                            $dis = "";
                                            if (($row->End < $now) || ($now < $row->Start)) {
//                                    $dis = "disabled=disabled";
                                            }

                                            $dateStart = new DateTime($row->Start);
                                            $dateEnd = new DateTime($row->End);
                                            $start = $dateStart->format('d/m/Y');
                                            $end = $dateEnd->format('d/m/Y');
                                            echo "<li><input type='checkbox' value='$row->ID' name='PromotionsID[]' $dis/> $row->Name";
                                            echo "<p style='float:right;'>$start -> $end</p></li>";
                                        }
                                        echo "</ul>";
                                    }
                                    ?>
                                </div>
                            </div>


                        </div>
                        <input type='hidden' name="currenttab" id="currenttab" value="<?php echo $currenttab; ?>" />
                        <input type='hidden' id="baselink_url" value="<?php echo base_url() . ADMINPATH . '/report/warehouse_products/'; ?>" />
                        </form>
                    </div>
                    <div class="over_lay black">
                        <div class="box_inner">
                            <div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
                            <div class="block2_inner"></div>
                        </div>
                    </div>
                </div>
                <style>
                    .daterangepicker{width: auto;}
                </style>
                <script>
                    $(document).ready(function () {
                        $('#Input_Startday,#Input_Stopday,#Input_Special_startday,#Input_Special_stopday').daterangepicker({
                            singleDatePicker: true,
                            calender_style: "picker_4",
                            format: 'YYYY-MM-DD 00:00:00',
                        });
                        $(".Variant_StartSpecialPrice,.Variant_StopSpecialPrice").daterangepicker({
                            singleDatePicker: true,
                            calender_style: "picker_4",
                            format: 'YYYY-MM-DD 00:00:00',
                        });
                    });

                    $(".control_tab a").click(function () {
                        $(".control_tab a").removeClass("current");
                        var data = $(this).attr('data');
                        $(this).addClass("current");
                        $(".content_tab").hide();
                        $(".content_" + data).show();
                        $("#currenttab").val(data);
                    });

                    $("#VariantType").change(function () {
                        var value = $(this).val();
                        if (value == 1) {
                            $(".control_tab a:nth-child(8)").show();
                        } else {
                            $(".control_tab a:nth-child(8)").hide();
                        }
                    });

                    $("#close_overlay").click(function () {
                        $(".over_lay").hide();
                        disablescrollsetup();
                    });

                    function warning_message(ob) {
                        if (!confirm('BẠN CÓ CHẮC THỰC HIỆN THAO TÁC NÀY KHÔNG ??')) {
                            return false;
                        } else {
                            href = $(ob).attr('data');
                            window.location = href;
                        }
                    }

                    function enablescrollsetup() {
                        $(window).scrollTop(70);
                        $("body").css({'height': '100%', 'overflow-y': 'hidden'});
                        h = window.innerHeight;
                        h = h - 200;
                        $(".over_lay .box_inner .block2_inner").css({"max-height": h + "px"});
                    }

                    function disablescrollsetup() {
                        $("body").css({'height': 'auto', 'overflow-y': 'scroll'});
                    }

                    function close_PriceGroup(ob) {
                        $(ob).parent('div').parent('div').remove();
                    }

                    function add_PriceGroup() {
                        $("#PriceGroup").append('<div class="col-xs-4"><div class="form-group"><label class="col-xs-4 control-label"></label><label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceGroup_Title[]" /></label></div></div><div class="col-xs-8"><div class="form-group"><label class="col-xs-4 control-label"><input type="text" class="form-control" name="PriceGroup_Price[]" /></label></div></div></div>');
                    }

                    function close_PriceRange(ob) {
                        $(ob).parent('div').parent('div').remove();
                    }

                    function add_PriceRange() {
                        $("#PriceRange").append('<div class="col-xs-4"><div class="form-group"><label class="col-xs-4 control-label"></label><label class="col-xs-8 control-label"><input type="text" class="form-control" name="PriceRange_Title[]" /></label></div></div><div class="col-xs-8"><div class="form-group"><label class="col-xs-4 control-label"><input type="text" name="PriceRange_Price[]" class="form-control" /></label><label class="col-xs-1 control-label"></label><label class="col-xs-1 control-label"><input type="number" min="0" class="form-control" name="PriceRange_Amount[]" value="0" /></label></div></div>');
                    }

                    function check_full(ob) {
                        if (ob.checked === true) {
                            $(ob).parent('li').find("input").prop('checked', true);
                        } else {
                            $(ob).parent('li').find("input").prop('checked', false);
                        }
                    }

                    function check_full_rowtab(ob, key) {
                        $("input.valuekey" + key).prop('checked', true);
                    }

                    function uncheck_full_rowtab(ob, key) {
                        $("input.valuekey" + key).prop('checked', false);
                    }

                    function show_child(ob) {
                        var current = $(ob).html();
                        if (current === "+") {
                            $(ob).html("-");
                        } else {
                            $(ob).html("+");
                        }
                        $(ob).parent('li').find("ul").toggle('fast');
                    }

                    function remove_categories(ob) {
                        $(ob).parent('span').parent('a').remove();
                    }

                    var isloaddata = [];

                    function show_child_next(ob) {
                        var ID = $(ob).attr('data');
                        var current = $(ob).html();
                        if (current === "+") {
                            $(ob).html("-");
                            if (ID != '') {
                                if (jQuery.inArray("data" + ID, isloaddata) < 0) {
                                    $.ajax({
                                        url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/get_child_categories' ?>",
                                        dataType: "html",
                                        type: "POST",
                                        data: "ID=" + ID,
                                        success: function (result) {
                                            if (result != 'False') {
                                                $(ob).parent('li').append(result);
                                                isloaddata.push("data" + ID);
                                            } else {
                                                $(ob).html("");
                                                $(ob).css({"background": "#FFF", "border": "1px solid #FFF"});
                                            }
                                        }
                                    });
                                }
                            }
                        } else {
                            $(ob).html("+");
                        }
                        $(ob).parent('li').find("ul").toggle("fast");
                    }

                    function senddata(url, title, data) {
                        enablescrollsetup();
                        $(".over_lay .block1_inner h1").html(title);
                        $(".over_lay .block2_inner").html("");
                        $.ajax({
                            url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/' ?>" + url,
                            dataType: "html",
                            type: "POST",
                            data: "Data=" + data,
                            success: function (result) {
                                if (result != 'false') {
                                    $(".over_lay .box_inner .block2_inner").html(result);
                                    $(".over_lay").removeClass('in');
                                    $(".over_lay").fadeIn('fast');
                                    $(".over_lay").addClass('in');
                                } else {
                                    alert("Không thực hiện được chức năng theo yêu cầu.");
                                }
                            }
                        });
                    }

                    function save_properties(ob) {
                        $(ob).addClass("saving");
                        var form = $(ob).parent('div').parent('div').parent('div').parent('form');
                        $.ajax({
                            url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/save_properties' ?>",
                            dataType: "html",
                            type: "POST",
                            data: form.serialize(),
                            success: function (result) {
                                var ar = result.split("|");
                                if (ar[0] != 'False') {
                                    if (ar[0] != 0) {
                                        $(".value" + ar[1]).append(ar[2]);
                                    }
                                    if (ar[0] != 0) {
                                        $("#PropertiesID").append(ar[2]);
                                    }
                                    disablescrollsetup();
                                    $(".over_lay").hide();
                                } else {
                                    alert("Không thực hiện được chức năng theo yêu cầu.");
                                }
                            }
                        });
                    }

                    var PropertiesList = [<?php echo implode(',', $arr_parent_js) ?>];

                    function select_properties() {
                        var ID = $("#PropertiesID").val();
                        if (ID != '') {
                            if (jQuery.inArray("data" + ID, PropertiesList) < 0) {
                                $(".message_empty").remove();
                                $.ajax({
                                    url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/add_newrow_properties' ?>",
                                    dataType: "html",
                                    type: "POST",
                                    data: 'Data=' + ID,
                                    success: function (result) {
                                        $(".content_tab7").append(result);
                                        PropertiesList.push("data" + ID);
                                    }
                                });
                            }
                        }
                    }

                    function AcceptVariant(ob, type) {
                        var variantvalue = $("#VariantSelect").val();
                        if (type == 0) {
                            select_properties_variant(ob);
                        } else {
                            select_properties_variant(ob);
                        }
                    }

                    function select_properties_variant(ob) {
                        $(ob).addClass("saving");
                        var baselink = $("#baselink_url").val();
                        var IDEdit = $("#IDEdit").val();
                        $.ajax({
                            url: baselink + "variant_add",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + IDEdit,
                            success: function (result) {
                                $(ob).removeClass("saving");
                                $(".content_tab8").append(result);
                            },
                            error: function (result) {
                                console.log(result.responseText);
                            }
                        });
                    }

                    function rm_variant(ob, ID) {
                        if (!confirm('BẠN CÓ CHẮC XÓA DỮ LIỆU NÀY KHÔNG ??')) {
                            return false;
                        }
                        $(ob).addClass("saving");
                        var baselink = $("#baselink_url").val();
                        $.ajax({
                            url: baselink + "delete_variant",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + ID,
                            success: function (result) {
                                $(ob).parent('div').parent('div').remove();
                            }
                        });
                    }

                    function showtable(ob) {
                        $(ob).parent('div').parent('div').find('table').toggle();
                    }

                    function save_variant(ob, ID) {
                        $(ob).addClass("saving");
                        var baselink = $("#baselink_url").val();
                        var Variant_Properties = "";
                        var Variant_Properties = "";
                        $(".Variant_Properties_" + ID).each(function () {
                            Variant_Properties = Variant_Properties + "|" + $(this).val();
                        });
                        var Variant_Published = $(".variant_" + ID + " .Variant_Published").is(':checked') ? 1 : 0;
                        var Variant_InventoryStatus = $(".variant_" + ID + " .Variant_InventoryStatus").val();
                        var Variant_SKU = $(".variant_" + ID + " .Variant_SKU").val();
                        var Variant_Title = $(".variant_" + ID + " .Variant_Title").val();
                        var Variant_Price = $(".variant_" + ID + " .Variant_Price").val();
                        var Variant_SpecialPrice = $(".variant_" + ID + " .Variant_SpecialPrice").val();
                        var Variant_Weight = $(".variant_" + ID + " .Variant_Weight").val();
                        var Variant_Length = $(".variant_" + ID + " .Variant_Length").val();
                        var Variant_Width = $(".variant_" + ID + " .Variant_Width").val();
                        var Variant_Height = $(".variant_" + ID + " .Variant_Height").val();
                        var Variant_Description = $(".variant_" + ID + " .Variant_Description").val();
                        var Variant_StartSpecialPrice = $(".variant_" + ID + " .Variant_StartSpecialPrice").val();
                        var Variant_StopSpecialPrice = $(".variant_" + ID + " .Variant_StopSpecialPrice").val();
                        $.ajax({
                            url: baselink + "update_variant",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + ID + "&Properties=" + Variant_Properties + "&Published=" + Variant_Published + "&InventoryStatus=" + Variant_InventoryStatus + "&SKU=" + Variant_SKU + "&Price=" + Variant_Price + "&SpecialPrice=" + Variant_SpecialPrice + "&Weight=" + Variant_Weight + "&Width=" + Variant_Width + "&Height=" + Variant_Height + "&Length=" + Variant_Length + "&Description=" + Variant_Description + "&StartSpecialPrice=" + Variant_StartSpecialPrice + "&StopSpecialPrice=" + Variant_StopSpecialPrice + "&Namebonus=" + Variant_Title,
                            success: function (result) {
                                $(ob).removeClass("saving");
                                $(ob).parent('div').find(".btn-success").fadeIn('slow').delay("1000").fadeOut('slow');
                            }
                        });
                    }

                    function rm_image_products(ob, ID) {
                        var baselink = $("#baselink_url").val();
                        $(ob).find('i').hide();
                        $(ob).addClass("saving");
                        $.ajax({
                            url: baselink + "delete_images",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + ID,
                            success: function (result) {
                                $(ob).removeClass("saving");
                                $(ob).parent('td').parent("tr").fadeOut();
                            }
                        });
                    }

                    function show_dateinput(ob) {
                        $(ob).parent('p').parent('td').find(".date_div").toggle();
                    }

                    function changeimages(ob) {
                        var baselink = $("#baselink_url").val();
                        var img = $(ob).parent('div').find('img');
                        var bar = $(ob).parent('div').find('.progress').find('.bar');
                        var percent = $(ob).parent('div').find('.progress').find('.percent');
                        var progress = $(ob).parent('div').find('.progress');
                        progress.show();
                        var percentValue = "0%";
                        var file = $(ob)[0].files[0];
                        var fd = new FormData();
                        var IDEdit = $(ob).attr('data');
                        fd.append('file', file);
                        fd.append('ID', IDEdit);
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', baselink + "changeimages", true);
                        xhr.upload.onprogress = function (e) {
                            if (e.lengthComputable) {
                                var percentValue = (e.loaded / e.total) * 100 + '%';
                                percent.html(percentValue);
                                bar.css({'width': percentValue});
                            }
                        };
                        xhr.onload = function () {
                            if (this.status == 200) {
                                if (xhr.responseText != "False") {
                                    img.attr("src", xhr.responseText);
                                } else {
                                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu . Vui lòng kiểm tra lại đường truyền !");
                                }
                            }
                            ;
                            progress.hide();
                        };

                        xhr.send(fd);
                    }
                    ;

                    function remove_rowtab(ob) {
                        $(ob).parent('div').parent('div').parent('div').remove();
                    }

                    var imageeach = [];

                    function activeupload(ob, temp, IDEdit) {
                        var i = 0;
                        $("." + temp).each(function () {
                            imageeach[i] = $(this).attr('data');
                            i++;
                        });
                        if (i > 0) {
                            $(ob).find('i').hide();
                            $(ob).addClass("saving");
                            if (temp == 'filerow') {
                                eachupload(0, imageeach.length);
                            }
                            if (temp == 'filemultirow') {
                                eachuploadmulti(0, imageeach.length, IDEdit);
                            }
                        } else {
                            alert("Vui lòng chọn file");
                        }
                    }

                    function eachupload(ob, count) {
                        temp = ob + 1;
                        if (ob == count) {
                            var tab = $("#currenttab").val();
                            window.location = "<?php echo current_url() ?>?tab=" + tab;
                            return false;
                        }
                        ob = imageeach[ob];
                        var data = $(".filerow" + ob).attr('data');
                        var bar = $('.bar' + data);
                        var progress = $('.progress' + data);
                        progress.show();
                        var percentValue = "0%";
                        var baselink = $("#baselink_url").val();
                        var Fileinput = document.getElementById("choosefile");
                        var file = Fileinput.files[data];
                        var fd = new FormData();
                        var IDEdit = $("#IDEdit").val();
                        fd.append('file', file);
                        fd.append('ID', IDEdit);
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', baselink + "upload_image", true);
                        xhr.upload.onprogress = function (e) {
                            if (e.lengthComputable) {
                                var percentValue = (e.loaded / e.total) * 100 + '%';
                                bar.css({'width': percentValue});
                            }
                        };
                        xhr.onload = function () {
                            if (this.status == 200) {
                                if (xhr.responseText != "False") {
                                    $(".filerow" + ob).remove();
                                } else {
                                    $(this).css({'border': '1px solid #F00'});
                                }
                                eachupload(temp, imageeach.length);
                            } else {
                                eachupload(temp, imageeach.length);
                            }
                        };
                        xhr.send(fd);
                    }

                    function eachuploadmulti(ob, count, IDEdit) {
                        temp = ob + 1;
                        if (ob == count) {
                            var tab = $("#currenttab").val();
                            window.location = "<?php echo current_url() ?>?tab=" + tab;
                            return false;
                        }
                        ob = imageeach[ob];
                        var data = $(".filemultirow" + ob).attr('data');
                        var bar = $('.bar' + data);
                        var progress = $('.progress' + data);
                        progress.show();
                        var percentValue = "0%";
                        var baselink = $("#baselink_url").val();
                        var Fileinput = document.getElementById("multipleimages" + IDEdit);
                        var file = Fileinput.files[data];
                        var fd = new FormData();
                        fd.append('file', file);
                        fd.append('ID', IDEdit);
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', baselink + "upload_image", true);
                        xhr.upload.onprogress = function (e) {
                            if (e.lengthComputable) {
                                var percentValue = (e.loaded / e.total) * 100 + '%';
                                bar.css({'width': percentValue});
                            }
                        };
                        xhr.onload = function () {
                            if (this.status == 200) {
                                if (xhr.responseText != "False") {
                                    $(".filemultirow" + ob).remove();
                                } else {
                                    $(this).css({'border': '1px solid #F00'});
                                }
                                eachuploadmulti(temp, imageeach.length, IDEdit);
                            } else {
                                eachuploadmulti(temp, imageeach.length, IDEdit);
                            }
                        };
                        xhr.send(fd);
                    }

                    $("#choosefile").change(function () {
                        var Fileinput = document.getElementById("choosefile");
                        var numfile = Fileinput.files.length;
                        var dvPreview = $("#dvPreview");
                        if (numfile > 0) {
                            dvPreview.html("");
                            dvPreview.append("<div class='row'><div class='col-md-1 col-sm-1 col-xs-12'>Preview</div><div class='col-md-1 col-sm-1 col-xs-12'></div><div class='col-md-5 col-sm-5 col-xs-12'>FileName</div><div class='col-md-2 col-sm-2 col-xs-12'>File Type</div><div class='col-md-2 col-sm-2 col-xs-12'>File Size</div><div class='col-md-1 col-sm-1 col-xs-12'>Remove</div></div>");
                            var j = 0;
                            for (var i = 0; i < numfile; i++) {
                                var reader = new FileReader();
                                var file = Fileinput.files[i];
                                var imageType = /image.*/;
                                var head = "";
                                if (file.type.match(imageType)) {
                                    reader.onloadend = function (e) {
                                        var file = Fileinput.files[j];
                                        dvPreview.append("<div class='row filerow filerow" + j + "' data='" + j + "'><div class='col-md-1 col-sm-1 col-xs-12'><img src='" + e.target.result + "' class='img-responsive' /></div><div class='col-md-1 col-sm-1 col-xs-12'></div><div class='col-md-5 col-sm-5 col-xs-12'>" + file.name + "</div><div class='col-md-2 col-sm-2 col-xs-12'>" + file.type + "</div><div class='col-md-2 col-sm-2 col-xs-12'>" + file.size + "</div><div class='col-md-1 col-sm-1 col-xs-12'><a onclick='remove_imagerow(this)' title='Gỡ bỏ file này khỏi danh sách'><i class='fa fa-times'></i></a></div><div class='progress" + j + "'><div class='bar" + j + "'></div></div></div>");
                                        $(".activeupload").show();
                                        j++;
                                    }
                                } else {
                                    console.log("Not an Image");
                                }
                                reader.readAsDataURL(file);
                            }
                            $(".dvPreview").slideDown('slow');
                        }
                    });

                    function changemultipleimages(ob) {
                        var id = $(ob).attr('id');
                        var Fileinput = document.getElementById(id);
                        var numfile = Fileinput.files.length;
                        var dvPreview = $(ob).parent('a').parent('div').parent('.image_variant').find('.list-preview-images');
                        if (numfile > 0) {
                            dvPreview.html("");
                            var j = 0;
                            for (var i = 0; i < numfile; i++) {
                                var reader = new FileReader();
                                var file = Fileinput.files[i];
                                var imageType = /image.*/;
                                var head = "";
                                if (file.type.match(imageType)) {
                                    reader.onloadend = function (e) {
                                        var file = Fileinput.files[j];
                                        dvPreview.append("<div class='col-xs-3 filemultirow filemultirow" + j + "' data='" + j + "'><img src='" + e.target.result + "' class='img-responsive' /></div>");
                                        $(".activeupload").show();
                                        j++;
                                    }
                                } else {
                                    console.log("Not an Image");
                                }
                                reader.readAsDataURL(file);
                            }
                            $(".dvPreview").slideDown('slow');
                        }
                    }

                    function remove_imagerow(ob) {
                        $(ob).parent('div').parent('div').remove();
                        var temp = 0;
                        $(".filerow").each(function () {
                            temp++;
                        });
                        if (temp == 0) {
                            $(".activeupload").hide();
                            $("#dvPreview").html("");
                        }
                    }

                    function active_save_image(ob, i) {
                        if ($(".imagerow_tr").length == i) {
                            $(ob).removeClass("saving");
                            $(ob).find('i').show();
                            $(ob).parent('div').find(".last-a").fadeIn('slow').delay("500").fadeOut('slow');
                            return false;
                        }
                        $(ob).find('i').hide();
                        $(ob).addClass("saving");
                        var baselink = $("#baselink_url").val();
                        var ID = $(".imagerow_tr" + i).attr('data');
                        var label_tr = $(".imagerow_tr" + i + " td .label_tr").val();
                        var stt_tr = $(".imagerow_tr" + i + " td .stt_tr").val();
                        var primaryimage_tr = $(".imagerow_tr" + i + " td .primaryimage_tr").is(":checked") ? 1 : 0;
                        var smallimage_tr = $(".imagerow_tr" + i + " td .smallimage_tr").is(":checked") ? 1 : 0;
                        var thumbimage_tr = $(".imagerow_tr" + i + " td .thumbimage_tr").is(":checked") ? 1 : 0;
                        $.ajax({
                            url: baselink + "row_image_update",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + ID + "&Label=" + label_tr + "&STT=" + stt_tr + "&Primary=" + primaryimage_tr + "&Small=" + smallimage_tr + "&Thumb=" + thumbimage_tr,
                            success: function (result) {}
                        }).always(function () {
                            active_save_image(ob, ++i);
                        });
                    }

                    function changeimagerow(ob) {
                        var baselink = $("#baselink_url").val();
                        var img = $(ob).parent('div').find('img');
                        var bar = $(ob).parent('div').find('.progress').find('.bar');
                        var progress = $(ob).parent('div').find('.progress');
                        progress.show();
                        var percentValue = "0%";
                        var file = $(ob)[0].files[0];
                        var fd = new FormData();
                        var IDEdit = $(ob).attr('data');
                        fd.append('file', file);
                        fd.append('ID', IDEdit);
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', baselink + "changeimagerow", true);
                        xhr.upload.onprogress = function (e) {
                            if (e.lengthComputable) {
                                var percentValue = (e.loaded / e.total) * 100 + '%';
                                bar.css({'width': percentValue});
                            }
                        };
                        xhr.onload = function () {
                            if (this.status == 200) {
                                if (xhr.responseText != "False") {
                                    img.attr("src", xhr.responseText);
                                } else {
                                    alert("Có lỗi xảy ra trong quá trình truyền nhận dữ liệu . Vui lòng kiểm tra lại đường truyền !");
                                }
                            }
                            ;
                            progress.hide();
                        };

                        xhr.send(fd);
                    }
                    ;

                    function saveall(ob) {
                        $(ob).addClass("saving");
                        $("#products_form").append("<input type='hidden' name='SaveAndExit' value='1' />");
                        $("#products_form").submit();
                    }

                    function WaitApproval(ob) {
                        var r = confirm("Bài viết sẽ được duyệt, bạn không thể thay đổi được.");
                        if (r == true) {
                            $(ob).addClass("saving");
                            $("#products_form").append("<input type='hidden' name='WaitApproval' value='1' />");
                            $("#products_form").submit();
                        }
                    }

                    function approved(id, token) {
                        var baselink = $("#baselink_url").val();
                        $.ajax({
                            url: baselink + "approved",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + id + "&token=" + token,
                            success: function (result) {
                                if (result == "OK") {
                                    alert('Bạn đã duyệt thành công.');
                                }
                                location.reload();
                            }
                        });
                    }

                    function rejected(id, token) {
                        var baselink = $("#baselink_url").val();
                        var msg = prompt("Nhập thông báo tới thành viên");
                        if (msg != null) {
                            $.ajax({
                                url: baselink + "rejected",
                                dataType: "html",
                                type: "POST",
                                data: "ID=" + id + "&token=" + token + "&msg=" + msg,
                                success: function (result) {
                                    if (result == "OK") {
                                        alert('Bạn đã từ chối thành công.');
                                    }
                                    location.reload();
                                }
                            });
                        }
                    }

                    function remove_images_variant(ob, ID) {
                        var baselink = $("#baselink_url").val();
                        var listID = [];
                        $(".select_image").each(function () {
                            listID.push($(this).attr('data'));
                        });
                        if (listID.length == 0) {
                            alert("Vui lòng chọn hình ảnh để xóa !");
                            return false;
                        }
                        $(ob).find('i').hide();
                        $(ob).addClass("saving");
                        listID = JSON.stringify(listID);
                        $.ajax({
                            url: baselink + "delete_images_variant",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + ID + "&listID=" + listID,
                            success: function (result) {
                                $(ob).removeClass("saving");
                                $(ob).find('i').show();
                                $(".select_image").parent('div').remove();
                            }
                        });
                    }

                    function setprimaryimage(ob, imageID, productsID) {
                        var baselink = $("#baselink_url").val();
                        var src = $(ob).parent('div.col-xs-3').find('img').attr('data-src');
                        $.ajax({
                            url: baselink + "setprimary_images_variant",
                            dataType: "html",
                            type: "POST",
                            data: "ID=" + productsID + "&ImageID=" + imageID,
                            success: function (result) {
                                $(ob).addClass('text-info');
                                $(ob).parent('div.col-xs-3').parent('div.list-preview-images').parent('.image_variant').find('.img-primary-variant').attr('src', src);
                            }
                        });
                    }

                    function select_image(ob) {
                        var dataselected = $(ob).attr('data-selected');
                        if (dataselected == 0) {
                            $(ob).addClass("select_image");
                            $(ob).attr('data-selected', 1);
                        } else {
                            $(ob).removeClass("select_image");
                            $(ob).attr('data-selected', 0);
                        }
                    }

                    function show_productsbox(ob) {
                        var baselink = $("#baselink_url").val();
                        $(ob).addClass('saving');
                        $.ajax({
                            url: baselink + "get_products_list",
                            dataType: "html",
                            type: "POST",
                            context: this,
                            data: "",
                            success: function (result) {
                                if (result != 'FALSE') {
                                    $(".over_lay .box_inner").css({'margin-top': '50px'});
                                    $(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
                                    $(".over_lay .box_inner .block2_inner").html(result);
                                    $(".over_lay").removeClass("in");
                                    $(".over_lay").fadeIn('fast');
                                    $(".over_lay").addClass("in");
                                } else {
                                    alert("Không tìm thấy dữ liệu theo yêu cầu.");
                                }
                                $(ob).removeClass('saving');
                            }
                        });
                    }

                    function input_search_products(ob) {
                        var baselink = $("#baselink_url").val();
                        var data = $(ob).val();
                        $.ajax({
                            url: baselink + "get_products_list",
                            dataType: "html",
                            type: "POST",
                            context: this,
                            data: "Title=" + data,
                            success: function (result) {
                                if (result != 'FALSE') {
                                    $(".over_lay .box_inner .block2_inner").html(result);
                                } else {
                                    alert("Không tìm thấy dữ liệu theo yêu cầu.");
                                }
                            }
                        });
                    }

                    function changeIngredients(ob, id) {
                        var data = prompt("Nhập nội dung cần thay đổi", $(ob).html());
                        if (data != null) {
                            var baselink = $("#baselink_url").val();
                            $.ajax({
                                url: baselink + "changeIngredients",
                                dataType: "html",
                                type: "POST",
                                context: this,
                                data: "ID=" + id + "&Title=" + data,
                                success: function (result) {
                                    $(ob).html(data);
                                }
                            });
                        }
                    }

                    function addproductstobundle(ob, ProductsID) {
                        var baselink = $("#baselink_url").val();
                        $.ajax({
                            url: baselink + "addproductstobundle",
                            dataType: "html",
                            type: "POST",
                            context: this,
                            data: "BundleID=<?php echo $data->ID ?>&ProductsID=" + ProductsID,
                            success: function (result) {
                                if (result == 'OK') {
                                    reload_bundle_list();
                                    if (!confirm('THỰC HIỆN THAO TÁC THÀNH CÔNG !! \nBẠN CÓ MUỐN CHỌN SẢN PHẨM TIẾP KHÔNG ??')) {
                                        var tab = $("#currenttab").val();
                                        window.location = "<?php echo current_url() ?>?tab=" + tab;
                                    }
                                }
                                if (result == 'FALSE') {
                                    alert("Sản phẩm đã được thêm vào nhóm này rồi ! Vui lòng không chọn nữa !");
                                }
                                if (result == 'FALSE1') {
                                    alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
                                }
                            }
                        });
                    }

                    function mushave(ob, state, id) {
                        if (state == 1) {
                            $(ob).removeClass("active");
                            var statenext = 0;
                        } else {
                            $(ob).addClass("active");
                            var statenext = 1;
                        }
                        var baselink = $("#baselink_url").val();
                        $.ajax({
                            url: baselink + "changemushave",
                            dataType: "html",
                            type: "POST",
                            context: this,
                            data: "ID=" + id + "&MusHave=" + statenext,
                            success: function (result) {
                                console.log(result);
                            }
                        });
                    }

                    function removeoutbundle(ob, IDBundle) {
                        $(ob).find('i').hide();
                        $(ob).addClass("saving");
                        var baselink = $("#baselink_url").val();
                        $.ajax({
                            url: baselink + "removeoutbundle",
                            dataType: "html",
                            type: "POST",
                            context: this,
                            data: "ID=" + IDBundle,
                            success: function (result) {
                                if (result == 'OK') {
                                    reload_bundle_list();
                                }
                                if (result == 'FALSE') {
                                    $(ob).find('i').show();
                                    $(ob).removeClass("saving");
                                    alert("Thao tác thực hiện không thành công ! Vui lòng kiểm tra lại đường truyền !");
                                }
                                if (result == 'FALSE1') {
                                    $(ob).find('i').show();
                                    $(ob).removeClass("saving");
                                    alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
                                }
                            }
                        });
                    }

                    function updatequantitybundle(ob, IDBundle) {
                        var Quantity = $(ob).val();
                        if (Quantity > 0) {
                            var baselink = $("#baselink_url").val();
                            $.ajax({
                                url: baselink + "updatequantitybundle",
                                dataType: "html",
                                type: "POST",
                                context: this,
                                data: "ID=" + IDBundle + "&Quantity=" + Quantity,
                                success: function (result) {
                                    if (result == 'FALSE') {
                                        alert("Thao tác thực hiện không thành công ! Vui lòng kiểm tra lại đường truyền !");
                                    }
                                    if (result == 'FALSE1') {
                                        alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
                                    }
                                }
                            });
                        }
                    }

                    function updatepricebundle(ob, IDBundle) {
                        var Price = $(ob).val();
                        if (Price >= 0) {
                            var baselink = $("#baselink_url").val();
                            $.ajax({
                                url: baselink + "updatepricebundle",
                                dataType: "html",
                                type: "POST",
                                context: this,
                                data: "ID=" + IDBundle + "&Price=" + Price,
                                success: function (result) {
                                    if (result == 'FALSE') {
                                        alert("Thao tác thực hiện không thành công ! Vui lòng kiểm tra lại đường truyền !");
                                    }
                                    if (result == 'FALSE1') {
                                        alert("Sản phẩm này đã bị khóa . Mọi thay đổi sẽ không được thực hiện !");
                                    }
                                },
                                error: function (result) {
                                    console.log(result);
                                }
                            });
                        }
                    }

                    function reload_bundle_list() {
                        var baselink = $("#baselink_url").val();
                        $("#bundle-list").load(baselink + "get_products_bundle_list/<?php echo $data->ID ?>");
                    }

                    function add_unit() {
                        var unit = prompt("Vui lòng điền đơn vị muốn tạo mới");
                        if (unit != null) {
                            $.ajax({
                                url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/add_unit' ?>",
                                dataType: "html",
                                type: "POST",
                                data: 'Data=' + unit,
                                success: function (result) {
                                    $("#Donvi").prepend("<option value='" + unit + "'>" + unit + "</option>");
                                    $("#Donvi").val(unit);
                                }
                            });
                        }
                    }

                    function add_trade() {
                        var trade = prompt("Vui lòng điền thương hiệu muốn tạo mới");
                        if (trade != null) {
                            $.ajax({
                                url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/add_trade' ?>",
                                dataType: "html",
                                type: "POST",
                                data: 'Data=' + trade,
                                success: function (result) {
                                    if (result == 'Error') {
                                        alert('Đã tồn tại trong hệ thống.');
                                    } else {
                                        $("#TrademarkID").prepend("<option value='" + result + "'>" + trade + "</option>");
                                        $("#TrademarkID").val(trade);
                                        $('select[name=TrademarkID]').val(result);
                                        $('.selectpicker').selectpicker('refresh');
                                    }
                                }
                            });
                        }
                    }

                    function add_capacity_unit() {
                        var unit = prompt("Vui lòng điền đơn vị muốn tạo mới");
                        if (unit != null) {
                            $.ajax({
                                url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/add_capacity_unit' ?>",
                                dataType: "html",
                                type: "POST",
                                data: 'Data=' + unit,
                                success: function (result) {
                                    $("#Capacity").prepend("<option value='" + unit + "'>" + unit + "</option>");
                                    $("#Capacity").val(unit);
                                }
                            });
                        }
                    }

                    function remove_ingredients(ob, id) {
                        var r = confirm("Bạn có chắc muốn xóa thành phần này ?");
                        if (r == true) {
                            $.ajax({
                                url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/remove_ingredients' ?>",
                                dataType: "html",
                                type: "POST",
                                data: 'ID=' + id,
                                success: function (result) {
                                    $(ob).parent('div').remove();
                                }
                            });
                        }
                    }

                    function load_ingredient() {
                        $.ajax({
                            url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/load_ingredient' ?>",
                            dataType: "html",
                            type: "POST",
                            data: 'ID=<?php echo $data->ID ?>',
                            success: function (result) {
                                $("#Ingredients").html(result);
                            }
                        });

                    }

                    function add_ingredients() {
                        var list = $("#list_ingredient").val();
                        if (list != '') {
                            var data = {data: list, id:<?php echo $data->ID ?>};
                            $.ajax({
                                url: "<?php echo base_url() . ADMINPATH . '/report/warehouse_products/add_ingredients' ?>",
                                dataType: "html",
                                type: "POST",
                                data: 'data=' + JSON.stringify(data),
                                success: function (result) {
                                    $("#list_ingredient").val('');
                                    load_ingredient();
                                }
                            });
                        }
                    }

                    function add_competitor() {
                        enablescrollsetup();
                        $(".over_lay .box_inner").css({'margin-top': '50px'});
                        $(".over_lay .box_inner .block1_inner h1").html("Danh sách đối thủ");
                        $(".over_lay .box_inner .block2_inner").load("<?php echo base_url() . ADMINPATH . "/report/warehouse_products/competitor" ?>");
                        $(".over_lay").removeClass("in");
                        $(".over_lay").fadeIn('fast');
                        $(".over_lay").addClass("in");
                    }

                    function preview() {
                        enablescrollsetup();
                        var introtext = CKEDITOR.instances.Content.getData();
                        var Title = $("#TitleProducts").val();
                        if (Title != '' && introtext != '') {
                            $(".over_lay .block1_inner h1").html("Xem trước : " + Title);
                            $(".over_lay .box_inner .block2_inner").html("<div class='preview_box'>" + introtext + "</div>");
                            $(".over_lay").removeClass('in');
                            $(".over_lay").fadeIn('fast');
                            $(".over_lay").addClass('in');
                        } else {
                            alert("Vui lòng nhập dữ liệu trước khi preview");
                        }
                    }
                    ;
                </script>
                <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin,vietnamese" rel="stylesheet" type="text/css">
                <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&amp;subset=vietnamese" rel="stylesheet">
                <style>
                    .over_lay .box_inner .block2_inner .preview_box{font-family: "Open Sans","Helvatica Neue",Arial,san-serifsans-serif;font-size: 16px;line-height: 1.5;color: #55595c;background-color: #fff}
                    .over_lay .box_inner .block2_inner .preview_box p,.over_lay .box_inner .block2_inner .preview_box span{font-size: inherit;}
                    .over_lay .box_inner .block2_inner .preview_box p em{border-radius: 20px;
                                                                         width: 40px;
                                                                         height: 40px;
                                                                         display: inline-block;
                                                                         margin-right: 10px;
                                                                         text-align: center;
                                                                         border: 1px solid #ddd;
                                                                         line-height: 40px;
                                                                         font-weight: bold;}
                    .over_lay .box_inner .block2_inner .preview_box p{padding: 30px 20px;
                                                                      overflow: hidden;
                                                                      margin: 0;}
                    .over_lay .box_inner .block2_inner .preview_box p:nth-child(2n){
                        background-color: #f7f7f9;border-radius: 10px;
                    }

                </style>