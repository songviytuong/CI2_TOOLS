<div class="sitebar">
	<div class="primary_image">
		<img src="<?php echo $this->user->Thumb ?>" />
		<h2><?php echo $this->user->FirstName.' '.$this->user->LastName ?></h2>
		<p>Nhân viên giao nhận hàng hóa</p>
	</div>
	<div class="user_information">
		<a><i class="fa fa-info-circle" aria-hidden="true"></i> Thông tin tài khoản</a>
		<a><i class="fa fa-bar-chart" aria-hidden="true"></i> Thống kê giao nhận</a>
	</div>
</div>