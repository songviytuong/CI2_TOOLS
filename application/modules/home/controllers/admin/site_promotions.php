<?php

class Site_Promotions extends Admin_Controller {

    public $user;
    public $classname = "site_promotions";
    public $limit = 20;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('Promotions List | Config Site');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_promotions")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select * from ttp_report_promotions order by ID ASC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_promotions/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_promotions/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/site_promotions', $data);
        $this->template->render();
    }
    
    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if ($id != 0) {
            $this->db->delete('ttp_report_promotions',array('ID'=>$id));
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add user | Config System');
        $this->load->model('warehouse_model','warehouse');
        $WH = $this->warehouse->defineGroupCity(); 
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_promotions/addnew',
            'warehouse' => $WH
        );
        $this->template->write_view('content', 'admin/site_promotions_edit', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : 0;
        
        $Store = isset($_POST['store']) ? $_POST['store'] : array() ;
        $Store = array_unique($Store);
        $Store = json_encode($Store);
        
        $Start = isset($_POST['Start']) ? $_POST['Start'] : '';
        $End = isset($_POST['End']) ? $_POST['End'] : '';
        $data = array(
                    'Name' => $Name,
                    'Description' => $Description,
                    'Status' => $Status,
                    'Created' => date('Y-m-d H:i:s'),
                    'LastEdited' => date('Y-m-d H:i:s'),
                    'Start' => $Start,
                    'End' => $End,
                    'Store' => $Store
                );
        $this->db->insert("ttp_report_promotions", $data);
        redirect(ADMINPATH . '/home/site_promotions/');
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_report_promotions where ID=$id")->row();
            if (!$result)
                show_error("Data is empty !");
            $this->load->model('warehouse_model','warehouse');
            $WH = $this->warehouse->defineGroupCity(); 
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_promotions/update',
                'data' => $result,
                'warehouse' => $WH
            );
            $this->template->add_title('Edit Promotions | Config Site');
            $this->template->write_view('content', 'admin/site_promotions_edit', $data);
            $this->template->render();
        }
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = $_POST['ID'];
        $Name = isset($_POST['Name']) ? $this->lib->fill_data($_POST['Name']) : '';
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Status = isset($_POST['Status']) ? $this->lib->fill_data($_POST['Status']) : 0;
        
        $Store = isset($_POST['store']) ? $_POST['store'] : array() ;
        $Store = array_unique($Store);
        $Store = json_encode($Store);
        
        $Start = isset($_POST['Start']) ? $_POST['Start'] : '';
        $End = isset($_POST['End']) ? $_POST['End'] : '';
        if ($Name != '') {
            $promotion = $this->db->query("select * from ttp_report_promotions where ID=$ID")->row();
            if ($promotion) {
                
                $data = array(
                    'Name' => $Name,
                    'Description' => $Description,
                    'Status' => $Status,
                    'LastEdited' => date('Y-m-d H:i:s'),
                    'Start' => $Start,
                    'End' => $End,
                    'Store' => $Store
                    
                );
                $this->db->where("ID", $ID);
                $this->db->update("ttp_report_promotions", $data);
            }
        }
        redirect(ADMINPATH . '/home/site_promotions/');
    }

}

?>