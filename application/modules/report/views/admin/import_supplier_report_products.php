<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>Thống kê doanh thu theo sản phẩm</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-12">
            <select onchange="changetype(this)" style="padding:5px 10px" class="pull-right">
                <option value="0">-- Tất cả chi nhánh --</option>
                <?php 
                if($this->user->IsAdmin==1){
                    $branch = $this->db->query("select * from ttp_report_branch")->result();
                }else{
                    $branch = $this->db->query("select * from ttp_report_branch where SupplierID in(".implode(',',$supplier_list).")")->result();
                }
                if(count($branch)>0){
                    foreach($branch as $row){
                        $selected = $row->ID==$branchid ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="quick_view" style="margin-top:0px">
        <div class="block1">
            <div style="margin-bottom:30px"></div>
            <canvas id="canvas_pie"></canvas>
            <div class='current_view_chart'>Biểu đồ : <b>Doanh số theo ngành hàng</b></div>
        </div>
        <div class="block2">
            <h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Theo ngành hàng</h3>
            <?php 
            $arr = array();
            $arr_total = array('SLDH'=>0,'SLSP'=>0,'Total'=>0);
            $categories = $this->db->query("select ID,Title,ParentID from ttp_report_categories where IsLast=1 and Path like '1/%'")->result();
            if(count($categories)>0){
            	foreach($categories as $row){
            		$arr[$row->ID] = array('Title'=>$row->Title,'Total'=>0,'ParentID'=>$row->ParentID,'SLDH'=>0,'SLSP'=>0);
            	}
            }
            $arr_brand_title = array();
            $arr_brand = array();
            $brand = $this->db->query("select ID,Title from ttp_report_trademark")->result();
            if(count($brand)>0){
            	foreach($brand as $row){
            		$arr_brand_title[$row->ID] = $row->Title;
            	}
            }
            $array_color = array("#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B","#27c","#F00","#56AEE2","#56E289","#FF6501","#E28956","#AEE256","#090","#E25668","#8A56E2","#F7435B");
            if(count($data)>0){
                foreach($data as $row){
                	$json_categories = json_decode($row->CategoriesID,true);
                	$json_categories = is_array($json_categories) ? $json_categories : array() ;
                	if(count($json_categories)>0){
	                    $arr_total['SLDH'] = $arr_total['SLDH']+1;
	                    $arr_total['SLSP'] = $arr_total['SLSP']+$row->Amount;
	                    $arr_total['Total'] = $arr_total['Total']+($row->Price*$row->Amount);
	                    foreach ($json_categories as $value) {
	                    	if(isset($arr[$value])){
		                        $arr[$value]['SLDH'] = $arr[$value]['SLDH']+1;
		                        $arr[$value]['SLSP'] = $arr[$value]['SLSP']+$row->Amount;
		                        $arr[$value]['Total'] = $arr[$value]['Total']+($row->Price*$row->Amount);
		                    }
	                    }
                    }
                    if(isset($arr_brand[$row->TrademarkID])){
                    	$arr_brand[$row->TrademarkID]['SLDH'] = $arr_brand[$row->TrademarkID]['SLDH']+1;
                        $arr_brand[$row->TrademarkID]['SLSP'] = $arr_brand[$row->TrademarkID]['SLSP']+$row->Amount;
                        $arr_brand[$row->TrademarkID]['Total'] = $arr_brand[$row->TrademarkID]['Total']+($row->Price*$row->Amount);
                    }else{
                    	$arr_brand[$row->TrademarkID]['SLDH'] = 1;
                        $arr_brand[$row->TrademarkID]['SLSP'] = $row->Amount;
                        $arr_brand[$row->TrademarkID]['Total'] = ($row->Price*$row->Amount);
                        $arr_brand[$row->TrademarkID]['Title'] = isset($arr_brand_title[$row->TrademarkID]) ? $arr_brand_title[$row->TrademarkID] : '--' ;
                    }
                }
            }
            $arr_chart = array();
            if(count($arr)>0){
                echo "<table>
                        <tr>
                            <th>Tên ngành hàng</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                $i = 0;
                foreach($arr as $key=>$value){
                	if($value['SLDH']>0 && $value['SLSP']>0){
	                    $percent = $arr_total['Total']==0 ? 0 : round($value['Total']/($arr_total['Total']/100),1);
	                    echo "<tr>";
	                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$i]."'></span>".$value['Title']."</td>";
	                    echo "<td>".number_format($value['SLDH'])."</td>";
	                    echo "<td>".number_format($value['SLSP'])."</td>";
	                    echo "<td>".number_format($value['Total'])."</td>";
	                    echo "<td>".$percent."%</td>";
	                    echo "</tr>";
	                    $arr_chart[] = "{
	                                        value: ".$percent.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: '".$value['Title']."'
	                                    }";
	                    $i++;
                    }
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }
            ?>
        </div>
        <div style="clear:both;width:100%"></div>
        <div class="block1">
            <div style="margin-bottom:50px"></div>
            <canvas id="canvas_pie1"></canvas>
            <div class='current_view_chart'>Biểu đồ : <b>Doanh số theo thương hiệu</b></div>
        </div>
        <div class="block2">
            <h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Theo thương hiệu</h3>
            <?php 

            $arr_chart1 = array();
            if(count($arr_brand)>0){
                echo "<table>
                        <tr>
                            <th>Tên thương hiệu</th>
                            <th>SL đơn hàng</th>
                            <th>SL sản phẩm</th>
                            <th>Tổng doanh số</th>
                            <th>Tỷ lệ %</th>
                        </tr>
                        ";
                $i = 0;
                foreach($arr_brand as $key=>$value){
                	if($value['SLDH']>0 && $value['SLSP']>0){
	                    $percent = $arr_total['Total']==0 ? 0 : round($value['Total']/($arr_total['Total']/100),1);
	                    echo "<tr>";
	                    echo "<td><span style='padding: 6px;float:left;margin-top: 5px;margin-right: 8px;background:".$array_color[$i]."'></span>".$value['Title']."</td>";
	                    echo "<td>".number_format($value['SLDH'])."</td>";
	                    echo "<td>".number_format($value['SLSP'])."</td>";
	                    echo "<td>".number_format($value['Total'])."</td>";
	                    echo "<td>".$percent."%</td>";
	                    echo "</tr>";
	                    $value['Title'] = str_replace("'",'',$value['Title']);
	                    $arr_chart1[] = "{
	                                        value: ".$percent.",
	                                        color: '".$array_color[$i]."',
	                                        highlight: '".$array_color[$i]."',
	                                        label: 'Thương hiệu ".$value['Title']."'
	                                    }";
	                    $i++;
                    }
                }
                echo "<tr>
                        <th>Tổng cộng</th>
                        <th>".number_format($arr_total['SLDH'])."</th>
                        <th>".number_format($arr_total['SLSP'])."</th>
                        <th>".number_format($arr_total['Total'])."</th>
                        <th>100%</th>
                    </tr>";
                echo "</table>";
            }
            ?>
        </div>
        <div class="row table_data" style="margin:0px">
        	<h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Doanh thu theo ngành hàng - thương hiệu</h3>
        	<?php 
        	$arr_products = array();
        	$arr_area = array();
        	if(count($data)>0){
        		foreach($data as $row){
        			if(!isset($arr_products[$row->ProductsID])){
        				$categories_products = json_decode($row->CategoriesID,true);
        				$categories_products = is_array($categories_products) ? $categories_products : array() ;
        				$str = array();
        				if(count($categories_products)>0){
        					foreach($categories_products as $item){
        						if(isset($arr[$item]['Title'])){
        							$str[] = $arr[$item]['Title'];
        						}
        					}
        				}
        				$arr_products[$row->ProductsID] = array(
        					'Title'=>$row->Title,
        					'Order'=>array($row->ID),
        					'SLSP'=>$row->Amount,
        					'Total'=>$row->Price*$row->Amount,
        					'Price'=>$row->Price,
        					'Brand'=>$row->Brand,
        					'Categories'=>implode(',',$str),
        					'Area' =>array(
        						$row->AreaID => array(
        							'SLSP'	=> $row->Amount,
        							'Total' => $row->Price*$row->Amount
        						)
        					)
        				);
        			}else{
        				$arr_products[$row->ProductsID]['Order'][] = $row->ID;
        				$arr_products[$row->ProductsID]['Order'] = array_unique($arr_products[$row->ProductsID]['Order']);
        				$arr_products[$row->ProductsID]['SLSP'] = $arr_products[$row->ProductsID]['SLSP']+$row->Amount;
        				$arr_products[$row->ProductsID]['Total'] = $arr_products[$row->ProductsID]['Total']+($row->Price*$row->Amount);
        				$arr_products[$row->ProductsID]['Price'] = $arr_products[$row->ProductsID]['Price']+($row->Price*$row->Amount);
        				if(isset($arr_products[$row->ProductsID]['Area'][$row->AreaID])){
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['SLSP'] += $row->Amount;
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['Total'] += ($row->Price*$row->Amount);
        				}else{
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['SLSP'] = $row->Amount;
        					$arr_products[$row->ProductsID]['Area'][$row->AreaID]['Total'] = ($row->Price*$row->Amount);
        				}
        			}
        			if(isset($arr_area[$row->AreaID])){
    					$arr_area[$row->AreaID]['SLSP'] = $arr_area[$row->AreaID]['SLSP']+$row->Amount;
    					$arr_area[$row->AreaID]['Total'] = $arr_area[$row->AreaID]['Total']+($row->Price*$row->Amount);
    				}else{
    					$arr_area[$row->AreaID] = array(
							'SLSP'	=> $row->Amount,
							'Total' => ($row->Price*$row->Amount)
						);
    				}
        		}
        	}
        	echo "<table><tr>
        		<th>STT</th>
				<th>Ngành hàng</th>
				<th>Tên sản phẩm</th>
				<th>Brand</th>
				<th>SL đ.hàng</th>
				<th>SL s.phẩm</th>
                <th>Đơn giá TB</th>
				<th>Tổng doanh số</th>
        	</tr>";
        	$total_arr_products = array('Order'=>0,'SLSP'=>0,'Total'=>0,'Price'=>0,'RootPrice'=>0);
        	if(count($arr_products)>0){
        		$i = 1;
        		foreach($arr_products as $row){
        			$row['Title'] = str_replace("\'","'",$row['Title']);
        			$row['Brand'] = str_replace("\'","'",$row['Brand']);
        			echo "<tr>";
        			echo "<td class='text-center'>$i</td>";
        			echo "<td class='width150'>".$row['Categories']."</td>";
        			echo "<td>".$row['Title']."</td>";
        			echo "<td>".$row['Brand']."</td>";
        			echo "<td class='width100'>".count($row['Order'])."</td>";
        			echo "<td class='width100'>".number_format($row['SLSP'])."</td>";
                    echo "<td class='width100'>".number_format($row['Total']/$row['SLSP'])."</td>";
        			echo "<td class='width100'>".number_format($row['Total'])."</td>";
        			echo "</tr>";
        			$total_arr_products['SLSP'] = $total_arr_products['SLSP'] + $row['SLSP'];
        			$total_arr_products['Total'] = $total_arr_products['Total'] + $row['Total'];
        			$i++;
        		}
        	}
        	echo "<tr>";
			echo "<th colspan='5'>Tổng cộng</th>";
			echo "<th class='width100'>".number_format($total_arr_products['SLSP'])."</th>";
            echo "<th></th>";
			echo "<th class='width100'>".number_format($total_arr_products['Total'])."</th>";
			echo "</tr>";
        	echo "</table>";
        	?>
        </div>
        <div class="row table_data" style="margin:0px">
        	<h3 style="padding-bottom: 10px;border-bottom: 1px solid #e1e1e1;margin-bottom: 15px;box-shadow: 0px 1px #FFF;">Doanh thu theo khu vực</h3>
        	<?php 
        	echo "<table><tr><th colspan='4'></th>";
        	$area = $this->db->query("select * from ttp_report_area where Title !='Undefine'")->result();
        	if(count($area)>0){
				foreach($area as $row){
					echo "<th colspan='2' class='text-center'>$row->Title</th>";
				}
			}
        	echo "</tr><tr><th>STT</th><th>Ngành hàng</th><th>Tên sản phẩm</th><th>Brand</th>";
			if(count($area)>0){
				foreach($area as $row){
					echo "<th>SL</th>";
					echo "<th>Doanh số</th>";
				}
			}
        	echo "</tr>";
        	if(count($arr_products)>0){
        		$i = 1;
        		foreach($arr_products as $row){
        			$row['Title'] = str_replace("\'","'",$row['Title']);
        			$row['Brand'] = str_replace("\'","'",$row['Brand']);
        			echo "<tr>";
        			echo "<td class='text-center'>$i</td>";
        			echo "<td class='width100'>".$row['Categories']."</td>";
        			echo "<td>".$row['Title']."</td>";
        			echo "<td>".$row['Brand']."</td>";
        			if(count($area)>0){
						foreach($area as $item){
							echo isset($row['Area'][$item->ID]['SLSP']) ? "<td>".number_format($row['Area'][$item->ID]['SLSP'])."</td>" : "<td>0</td>";
							echo isset($row['Area'][$item->ID]['Total']) ? "<td>".number_format($row['Area'][$item->ID]['Total'])."</td>" : "<td>0</td>";
						}
					}
        			$i++;
        		}
        	}
        	echo "<tr>";
			echo "<th colspan='4'>Tổng cộng</th>";
			$area = $this->db->query("select * from ttp_report_area where Title !='Undefine'")->result();
        	if(count($area)>0){
				foreach($area as $row){
					echo isset($arr_area[$row->ID]['SLSP']) ? "<th>".number_format($arr_area[$row->ID]['SLSP'])."</th>" : "<th>0</th>";
					echo isset($arr_area[$row->ID]['Total']) ? "<th>".number_format($arr_area[$row->ID]['Total'])."</th>" : "<th>0</th>";
				}
			}
			echo "</tr>";
        	echo "</table>";
        	?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });

    var sharePiePolorDoughnutData = [
        <?php 
        echo implode(',', $arr_chart);
        ?>
    ];

    var sharePiePolorDoughnutData1 = [
        <?php 
        echo implode(',', $arr_chart1);
        ?>
    ];
    
    $(document).ready(function () {
        window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });

        window.myPie = new Chart(document.getElementById("canvas_pie1").getContext("2d")).Pie(sharePiePolorDoughnutData1, {
            responsive: true,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });

    function changetype(ob){
        var branch = $(ob).val();
        window.location="<?php echo current_url().'?branch=' ?>"+branch;
    }

</script>
<style>
    .version{display:none;}
    .copyright{display:none;}
</style>