<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$total = 0;
$order = array();
$order_count = 0;
$order_success = 0;
$order_false = 0;
$order_waiting = 0;
$products = array();
$products_total = 0;
$arr_day = array();
if(count($data)>0){
	foreach ($data as $row) {
		if(!isset($order[$row->ID])){
			$order[$row->ID] = 1;
			$order_count++;
			if($row->Status==0){
				$order_success++;
				$total = $total + ($row->Total-$row->Chietkhau+$row->Chiphi);
			}elseif($row->Status==1){
				$order_false++;
			}else{
				$order_waiting++;
			}
			$day_row = date('d-m-Y',strtotime($row->Ngaydathang));
			if(isset($arr_day[$day_row][$row->KhoID])){
				$arr_day[$day_row][$row->KhoID] = $arr_day[$day_row][$row->KhoID]+($row->Total-$row->Chietkhau+$row->Chiphi);
			}else{
				$arr_day[$day_row][$row->KhoID] = ($row->Total-$row->Chietkhau+$row->Chiphi);
			}
		}
		if(isset($products[$row->ProductsID])){
			$products[$row->ProductsID]['Amount'] += $row->Amount;
			$products[$row->ProductsID]['Total'] += $row->DetailsTotal;
			$products_total+=$row->DetailsTotal;
		}else{
			$products[$row->ProductsID]['MaSP'] = $row->MaSP;
			$products[$row->ProductsID]['Title'] = $row->Title;
			$products[$row->ProductsID]['Donvi'] = $row->Donvi;
			$products[$row->ProductsID]['Amount'] = $row->Amount;
			$products[$row->ProductsID]['Total'] = $row->DetailsTotal;
			$products_total+=$row->DetailsTotal;
		}
	}
}
$giatritrungbinh = number_format(round($total/$order_count));

?>
<div class="containner non-submenu">
	<div class="row" style="margin:20px 0px 0px 0px">
		<div class="col-xs-6">
			<h4 style="margin: 5px 20px;">TỔNG QUAN HOẠT ĐỘNG BÁN HÀNG </h4>
		</div>
		<div class="col-xs-6 text-right">
			<div id="reportrange">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
		</div>
	</div>
	<hr>
	<div class="row overview_sales" style="margin:10px 0px 0px 0px">
		<div class="form-group">
			<div class="col-xs-12 col-md-8 col-lg-8">
				<div class="form-group">
					<div class="col-xs-12">
						<div class="panel panel-success">
							<div class="panel-heading"><i class="fa fa-pie-chart" aria-hidden="true" style="margin-right:5px"></i> Kết quả bán hàng hôm nay</div>
							<div class="panel-body">
								<div class="col-xs-3" style="border-right:1px solid #EEE">
									<div class="row">
										<div class="col-xs-3"><h2 style="background: #85cc69;width: 35px;height: 35px;text-align: center;border-radius: 50%;line-height: 35px;font-size: 20px;margin: 5px 0px;color: #FFF;"><i class="fa fa-shopping-cart" aria-hidden="true"></i></h2></div>
										<div class="col-xs-9">
											<p>Số đơn hàng</p>
											<h4><?php echo number_format($current->TotalOrder) ?></h4>
										</div>
									</div>
								</div>
								<div class="col-xs-3" style="border-right:1px solid #EEE">
									<div class="row">
										<div class="col-xs-3"><h2 style="background: #e39244;width: 35px;height: 35px;text-align: center;border-radius: 50%;line-height: 35px;font-size: 20px;margin: 5px 0px;color: #FFF;"><i class="fa fa-usd" aria-hidden="true"></i></h2></div>
										<div class="col-xs-9">
											<p>Doanh số</p>
											<h4><?php echo number_format($current->Total-$current->Chietkhau+$current->Chiphi) ?></h4>
										</div>
									</div>
								</div>
								<div class="col-xs-3" style="border-right:1px solid #EEE">
									<p>So với hôm qua</p>
									<?php 
									$current_total = $current->Total-$current->Chietkhau+$current->Chiphi;
									$yesterday_total = $yesterday->Total-$yesterday->Chietkhau+$yesterday->Chiphi;
									if($yesterday_total>$current_total){
										$grow = $yesterday_total>0 ? round(($yesterday_total-$current_total)/($yesterday_total/100),1) : 0;
										echo '<h4 class="text-danger" style="color:#ce4844">'.$grow.'%<i class="fa fa-arrow-circle-down" aria-hidden="true" style="margin-left:20px"></i></h4>';
									}else{
										$grow = $yesterday_total>0 ? round(($current_total-$yesterday_total)/($yesterday_total/100),1) : 0;
										echo '<h4 class="text-success" style="color:#5cb85c">'.$grow.'%<i class="fa fa-arrow-circle-up" aria-hidden="true" style="margin-left:20px"></i></h4>';
									}
									?>
								</div>
								<div class="col-xs-3">
									<p>So với cùng kỳ tháng trước</p>
									<?php 
									$lastmonth_total = 0;
									if(count($result_lastmonth)>0){
										foreach($result_lastmonth as $row){
											if($lastmonth_total<($row->Total-$row->Chietkhau+$row->Chiphi)){
												$lastmonth_total = $row->Total-$row->Chietkhau+$row->Chiphi;
											}
										}
									}
									if($lastmonth_total>$current_total){
										$grow = round(($lastmonth_total-$current_total)/($lastmonth_total/100),1);
										echo '<h4 class="text-danger" style="color:#ce4844">'.$grow.'%<i class="fa fa-arrow-circle-down" aria-hidden="true" style="margin-left:20px"></i></h4>';
									}else{
										$grow = round(($current_total-$lastmonth_total)/($lastmonth_total/100),1);
										echo '<h4 class="text-success" style="color:#5cb85c">'.$grow.'%<i class="fa fa-arrow-circle-up" aria-hidden="true" style="margin-left:20px"></i></h4>';
									}
									?>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="panel panel-info">
							<div class="panel-heading"><i class="fa fa-pie-chart" aria-hidden="true" style="margin-right:5px"></i> Hiệu quả đạt được trong khoản thời gian <?php echo date('d/m/Y',strtotime($startday)).' đến ngày '.date('d/m/Y',strtotime($stopday)) ?></div>
							<div class="panel-body">
								<div class="col-xs-3" style="border-right:1px solid #EEE">
									<div class="row">
										<div class="col-xs-3"><h2 style="background: #85cc69;width: 35px;height: 35px;text-align: center;border-radius: 50%;line-height: 35px;font-size: 20px;margin: 5px 0px;color: #FFF;"><i class="fa fa-shopping-cart" aria-hidden="true"></i></h2></div>
										<div class="col-xs-9">
											<p>Số đơn hàng</p>
											<h4><?php echo number_format($order_count) ?></h4>
										</div>
									</div>
								</div>
								<div class="col-xs-3" style="border-right:1px solid #EEE">
									<div class="row">
										<div class="col-xs-3"><h2 style="background: #e39244;width: 35px;height: 35px;text-align: center;border-radius: 50%;line-height: 35px;font-size: 20px;margin: 5px 0px;color: #FFF;"><i class="fa fa-usd" aria-hidden="true"></i></h2></div>
										<div class="col-xs-9">
											<p>Doanh số</p>
											<h4><?php echo number_format($total) ?></h4>
										</div>
									</div>
								</div>
								<div class="col-xs-3" style="border-right:1px solid #EEE">
									<p>Thành công <span class="label label-success"><?php echo number_format($order_success) ?></span>  </p>
									<p>Đang xử lý <span class="label label-warning"><?php echo number_format($order_waiting) ?></span> </p>
									<p>Bị hủy <span class="label label-danger"><?php echo number_format($order_false) ?></span> </p>
								</div>
								<div class="col-xs-3">
									<p>Giá trị trung bình / đơn hàng</p>
									<h4><?php echo $giatritrungbinh ?></h4>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="panel panel-default">
							<div class="panel-heading"><i class="fa fa-bar-chart" aria-hidden="true" style="margin-right:5px"></i> Doanh số đạt được trong khoản thời gian <?php echo date('d/m/Y',strtotime($startday)).' đến ngày '.date('d/m/Y',strtotime($stopday)) ?></div>
							<div class="panel-body">
								<div id="columnchart_material" style="height: 400px;"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="panel panel-default">
							<div class="panel-heading"><i class="fa fa-line-chart" aria-hidden="true" style="margin-right:5px"></i> Hàng hóa bán chạy trong khoản thời gian <?php echo date('d/m/Y',strtotime($startday)).' đến ngày '.date('d/m/Y',strtotime($stopday)) ?></div>
							<div class="panel-body">
								<div id="chart_div"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-lg-4">
				<h4><i class="fa fa-history" aria-hidden="true" style="margin-right:10px"></i> CÁC HOẠT ĐỘNG GẦN ĐÂY</h4>
				<?php 
					$log = $this->db->query("select * from ttp_log order by ID DESC limit 0,18")->result();
					if(count($log)>0){
						$arrstate = array(
							0 => 'success',
							1 => 'danger',
							2 => 'waining',
							3 => 'info',
						);
						foreach($log as $row){
							echo "<div class='bs-callout bs-callout-".$arrstate[$row->Status]."'><p>$row->Note <br><small class='label label-default'>".$this->lib->get_times_remains($row->Created)."</small></p></div>";
						}
					}
				?>
			</div>
		</div>
	</div>
</div>
<!-- daterangepicker -->
<script type="text/javascript" src="public/admin/js/moment.min2.js"></script>
<script type="text/javascript" src="public/admin/js/datepicker/daterangepicker.js"></script>
<script>
	$(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
		    window.location = "<?php echo ADMINPATH.'/pos/pos_sales/overview' ?>?startday="+startday+"&stopday="+stopday;
		});
    });
</script>
<?php 
	$warehouse = $this->db->query("select * from ttp_report_warehouse")->result();
	$arr_warehouse = array();
	if(count($warehouse)>0){
		foreach($warehouse as $row){
			$arr_warehouse[$row->ID] = "$row->Title";
		}
	}
	$numday = $this->lib->get_nume_day($startday,$stopday);
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  	google.charts.load('current', {'packages':['corechart','bar']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Ngày bán hàng tại <?php echo $arr_warehouse[$this->WarehouseID]; ?> ', '<?php echo $arr_warehouse[$this->WarehouseID]; ?>', { role: 'style' }]
		  <?php 
			if($numday>0){
				for($i=0;$i<$numday+1;$i++){
					$titleday = date('d',strtotime($startday)+($i*3600*24));
					$day_row = date('d-m-Y',strtotime($startday)+($i*3600*24));
					$value = isset($arr_day[$day_row][$this->WarehouseID]) ? $arr_day[$day_row][$this->WarehouseID] : 0 ;
					echo ",['$titleday',$value,'color: #06941a']";
				}
			}
		  ?>
        ]);

        var options = {
          chart: {
            title: 'Biểu đồ doanh số',
            subtitle: 'từ ngày <?php echo date('d/m/Y',strtotime($startday)).' đến ngày '.date('d/m/Y',strtotime($stopday)) ?>'
          },
		  legend: { position: "none" }
        };
        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        chart.draw(data, options);
		
		var data1 = google.visualization.arrayToDataTable([
			['Products', '<?php echo $arr_warehouse[$this->WarehouseID]; ?>']
			<?php 
			if(count($products)>0){
				foreach($products as $row){
					echo ",['".$row['Title']."',".$row['Total']."]";
				}
			}
		  ?>
		  ]);

		  var options1 = {
			title: 'Biểu đồ doanh số trên từng sản phẩm',
			legend: {position: 'top', maxLines: 3},
			height: <?php echo count($products)*40 ?>,
			hAxis: {
			  title: 'Doanh số đạt được',
			  minValue: 0,
			  textStyle: {
				fontSize: 13,
				bold: true,
				italic: false
			  }
			},
			vAxis: {
			  title: 'Loại sản phẩm',
			  textStyle: {
				fontSize: 13,
				bold: false,
				italic: false
			  }
			}
		  };

		  var chart1 = new google.visualization.BarChart(document.getElementById('chart_div'));
		  chart1.draw(data1, options1);
    }
</script>