<div class="containner">
	<div class="row">
		<div class="col-xs-9">
			<h3>Đồng bộ số lượng tồn kho sản phẩm</h3>
		</div>
		<div class="col-xs-3">
			<div class="form-group">
				<div class="input-group">
					<input type='text' id='date' class="form-control" value='<?php echo date('Y-m-d') ?>' placeholder='Chọn ngày đồng bộ tồn kho' />
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button" id='sync'><i class="fa fa-refresh"></i> Đồng bộ</button>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="alert alert-success text-center hidden">
		<p>Quá trình đồng bộ dữ liệu tồn kho đã hoàn tất ! <br>Số lượng bản ghi được cập nhật : <?php echo count($data) ?></p>
	</div>
	<div class="row hidden loading">
		<div class="col-xs-2">
    		<div class="loader">Loading...</div>
    	</div>
    	<div class="col-xs-10">
    		<div class="state_progress"></div>
		</div>
	</div>
	<div class="table_data">
		<table class="table_data">
			<tr>
				<th>STT</th>
				<th>Tên sản phẩm</th>
				<th>Mã SKU</th>
				<th>Lô hàng</th>
				<th>Tại kho</th>
				<th>Trạng thái</th>
			</tr>
			<?php 
			if(count($data)>0){
				$i=1;
				foreach($data as $row){
					echo "<tr>";
					echo "<td>$i</td>";
					echo "<td class='products_title'>$row->Title</td>";
					echo "<td class='products_code'>$row->MaSP</td>";
					echo "<td class='shipment_code'>$row->ShipmentCode</td>";
					echo "<td class='warehouse_title'>$row->MaKho</td>";
					echo "<td><span id='sync_data_$i' data-products='$row->ProductsID' data-shipment='$row->ShipmentID' data-warehouse='$row->WarehouseID'>Chưa xử lý</span></td>";
					echo "</tr>";
					$i++;
				}
			}
			?>
		</table>
	</div>
	<div class="over_lay"></div>
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script>
	$(document).ready(function () {
        $('#date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY-MM-DD',
        });
    });
</script>
<script>
	
    function send_ajax(num, index)
    {
        if (index >= num ){
            $(".alert").removeClass('hidden');
            $("body,html").scrollTop(0);
            $(".loading").addClass('hidden');
            $(".over_lay").fadeOut();
            return false;
        }
        var products_title = $('#sync_data_'+index).parent('td').parent('tr').find('.products_title').html();
        var products_code = $('#sync_data_'+index).parent('td').parent('tr').find('.products_code').html();
        var shipment_code = $('#sync_data_'+index).parent('td').parent('tr').find('.shipment_code').html();
        var warehouse_title = $('#sync_data_'+index).parent('td').parent('tr').find('.warehouse_title').html();
        $(".state_progress").html("<h3>Đang đồng bộ dữ liệu tồn kho :</h3>- "+products_title+"<br>- Mã sản phẩm : "+products_code+"<br>- Mã lô hàng : "+shipment_code+"<br>- Kho hàng : "+warehouse_title+"<br><br><small class='btn-sm btn-primary'>Total records : "+index+"/"+num+"</small>");
        $('#sync_data_'+index).addClass('text-warning').html('Sending...');
        var products = $('#sync_data_'+index).attr('data-products');
        var shipment = $('#sync_data_'+index).attr('data-shipment');
        var warehouse = $('#sync_data_'+index).attr('data-warehouse');
        var datesync = $("#date").val();
        $.ajax({
            url : '<?php echo base_url().ADMINPATH."/report/warehouse/sync_data" ?>',
            type : 'POST',
            dataType:"text",
            data : "products="+products+"&shipment="+shipment+"&warehouse="+warehouse+"&date="+datesync,
            success : function(result)
            {
                if(result=="False"){
                    $('#sync_data_'+index).removeClass('text-warning').addClass('text-danger');
                    $('#sync_data_'+index).html('False');
                }else{
                	$('#sync_data_'+index).removeClass('text-warning').addClass('text-success');
                    $('#sync_data_'+index).html('Finished');
                }
            }
        })
        .always(function(){
            send_ajax(num, ++index);
        });
    }

    $('#sync').click(function()
    {  
    	$(".over_lay").fadeIn();
    	$(".alert").addClass('hidden');
        var num = <?php echo $i ?>;
        $(this).parent('row').hide();
        $(".loading").removeClass('hidden');
        send_ajax(num, 1);
    });
</script>