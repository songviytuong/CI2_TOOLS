<?php

class Site_banner extends Admin_Controller {

    public $user;
    public $classname = "site_banner";
    public $limit = 20;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
        $this->user = $this->lib->get_user($session, $this->classname);
        if ($this->user->IsAdmin != 1) {
            $this->lib->published_module($this->classname);
        }
        $this->load->library('template');
        $this->template->set_template('admin');
        $this->template->write_view('sitebar', 'admin/sitebar', array('user' => $this->user));
        $this->template->write_view('topnav', 'admin/topnav', array('user' => $this->user));
        $this->template->add_js("public/admin/js/script.js");
        $this->template->add_doctype();
    }

    public function index() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if (!is_numeric($start))
            $start = 0;
        $this->template->add_title('Banner list | Config Site');
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_banner")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.Title,b.Description from ttp_banner a,ttp_banner_position b where a.PositionID=b.ID order by a.ID ASC $limit_str")->result();
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_banner/',
            'data' => $object,
            'start' => $start,
            'nav' => $this->lib->nav(base_url() . ADMINPATH . '/home/site_banner/index', 5, $nav, $this->limit)
        );
        $this->template->write_view('content', 'admin/site_banner_home', $data);
        $this->template->render();
    }
    
    public function bannerPosition(){
        $this->template->write_view('content', 'admin/site_banner_position');
        $this->template->render();
    }

    public function search() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        if (isset($_GET['keyword'])) {
            $keyword = $this->lib->fill_data($_GET['keyword']);
            $str = "select a.*,b.Title,b.Description from ttp_banner a,ttp_banner_position b where a.PositionID=b.ID";
            $str = $keyword != '' ? $str . " and b.Title like '%" . urldecode($keyword) . "%'" : $str;
            $this->template->add_title('Tìm kiếm dữ liệu');
            $data = array(
                'data' => $this->db->query($str)->result(),
                'nav' => '',
                'start' => 0,
                'base_link' => base_url() . ADMINPATH . '/home/site_banner/',
            );
            $this->template->write_view('content', 'admin/site_banner_home', $data);
            $this->template->render();
            return;
        }
        redirect(ADMINPATH . '/home/site_banner/');
    }
    
    public function group(){
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'r', $this->user->IsAdmin);
        $segment = $this->uri->segment(4);
        if($segment != 'group'){
            redirect(ADMINPATH . '/home/site_banner/');
        } else {
            $gid = $this->uri->segment(5);
        }
        echo $gid;
    }
    
    public function delete($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'd', $this->user->IsAdmin);
        if ($id != 0) {
            $image = $this->db->query("select Thumb from ttp_banner where ID=$id")->row();
            if ($image)
                $this->delete_file_path($image->Thumb);
            $this->db->delete('ttp_banner',array('ID'=>$id));
        }
        $return = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url() . ADMINPATH;
        redirect($return);
    }

    public function add() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $this->template->add_title('Add user | Config System');
        $data = array(
            'base_link' => base_url() . ADMINPATH . '/home/site_banner/'
        );
        $this->template->write_view('content', 'admin/site_banner_add', $data);
        $this->template->render();
    }

    public function addnew() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'w', $this->user->IsAdmin);
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : '';
        $Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : $Title;
        $PositionID = isset($_POST['PositionID']) ? $this->lib->fill_data($_POST['PositionID']) : 0;
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Description_en = isset($_POST['Description_en']) ? $this->security->xss_clean($_POST['Description_en']) : '';
        $Link = isset($_POST['Link']) ? $this->lib->fill_data($_POST['Link']) : '';
        $Link = strpos($Link, "http") == 0 ? $Link : "http://" . str_replace("http://", "", $Link);
        $Link_en = isset($_POST['Link_en']) ? $this->lib->fill_data($_POST['Link_en']) : '';
        $Link_en = strpos($Link_en, "http") == 0 ? $Link_en : "http://" . str_replace("http://", "", $Link_en);
        $Target = isset($_POST['Target']) ? $this->lib->fill_data($_POST['Target']) : '';
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $Width = isset($_POST['Width']) ? (int) $_POST['Width'] : 0;
        $Height = isset($_POST['Height']) ? (int) $_POST['Height'] : 0;
        if ($Title != '' && $PositionID != 0) {
            $newuser = $this->db->query("select max(ID) as ID from ttp_banner")->row();
            $NewID = $newuser ? $newuser->ID + 1 : 1;
            $arr = array();
            $arr['Title'] = $Title != '' ? $Title : "";
            $arr['Description'] = $Description != '' ? $Description : "";
            $arr['Link'] = $Link != '' ? $Link : "";
            $arr['Target'] = $Target != '' ? $Target : "";
            $arr = json_encode($arr);
            $arr1 = array();
            $arr1['Title'] = $Title_en != '' ? $Title_en : "";
            $arr1['Description'] = $Description_en != '' ? $Description_en : "";
            $arr1['Link'] = $Link_en != '' ? $Link_en : "";
            $arr1 = json_encode($arr1);
            $data = array(
                'ID' => $NewID,
                'PositionID' => $PositionID,
                'Published' => $Published,
                'Created' => date('Y-m-d H:i:s'),
                'LastEdited' => date('Y-m-d H:i:s'),
                'Data' => $arr,
                'Data_en' => $arr1
            );
            if (isset($_FILES['Image_upload'])) {
                if ($_FILES['Image_upload']['error'] == 0) {
                    $this->upload_to = "banner_thumb/$NewID";
                    $data['Thumb'] = $this->upload_image_single();
                    if ($Width > 0 && $Height > 0) {
                        $this->resize_image($data['Thumb'], $Width, $Height);
                    }
                }
            }
            $this->db->insert("ttp_banner", $data);
        }
        redirect(ADMINPATH . '/home/site_banner/');
    }

    public function edit($id = 0) {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        if (is_numeric($id) && $id > 0) {
            $result = $this->db->query("select * from ttp_banner where ID=$id")->row();
            if (!$result)
                show_error("Data is empty !");
            $this->load->model('warehouse_model','warehouse');
            $WH = $this->warehouse->defineGroupCity(); 
            $data = array(
                'base_link' => base_url() . ADMINPATH . '/home/site_banner/',
                'data' => $result,
                'warehouse' => $WH
            );
            $this->template->add_title('Edit banner | Config Site');
            $this->template->write_view('content', 'admin/site_banner_edit', $data);
            $this->template->render();
        }
    }

    public function update() {
        $this->lib->check_permission($this->user->DetailRole, $this->classname, 'm', $this->user->IsAdmin);
        $ID = isset($_POST['ID']) ? $this->lib->fill_data($_POST['ID']) : '';
        $Title = isset($_POST['Title']) ? $this->lib->fill_data($_POST['Title']) : $Title;
        $Title_en = isset($_POST['Title_en']) ? $this->lib->fill_data($_POST['Title_en']) : '';
        $PositionID = isset($_POST['PositionID']) ? $this->lib->fill_data($_POST['PositionID']) : 0;
        $Description = isset($_POST['Description']) ? $this->security->xss_clean($_POST['Description']) : '';
        $Description_en = isset($_POST['Description_en']) ? $this->security->xss_clean($_POST['Description_en']) : '';
        $Link = isset($_POST['Link']) ? $this->lib->fill_data($_POST['Link']) : '';
        $Link = strpos($Link, "http") == 0 ? $Link : "http://" . str_replace("http://", "", $Link);
        $Link_en = isset($_POST['Link_en']) ? $this->lib->fill_data($_POST['Link_en']) : '';
        $Link_en = strpos($Link_en, "http") == 0 ? $Link_en : "http://" . str_replace("http://", "", $Link_en);
        $Target = isset($_POST['Target']) ? $this->lib->fill_data($_POST['Target']) : '';
        $Published = isset($_POST['Published']) ? $this->lib->fill_data($_POST['Published']) : 0;
        $Width = isset($_POST['Width']) ? (int) $_POST['Width'] : 0;
        $Height = isset($_POST['Height']) ? (int) $_POST['Height'] : 0;
        
        $Store = isset($_POST['store']) ? $_POST['store'] : array() ;
        $Store = array_unique($Store);
        $Store = json_encode($Store);
        
        $Start = isset($_POST['start']) ? $_POST['start'] : '';
        $End = isset($_POST['end']) ? $_POST['end'] : '';
        
        if ($Title != '' && $PositionID != 0) {
            $banner = $this->db->query("select * from ttp_banner where ID=$ID")->row();
            if ($banner) {
                $arr = array();
                $arr['Title'] = $Title != '' ? $Title : "";
                $arr['Description'] = $Description != '' ? $Description : "";
                $arr['Link'] = $Link != '' ? $Link : "";
                $arr['Target'] = $Target != '' ? $Target : "";
                
                $dateStart = new DateTime($Start);
                $dateEnd = new DateTime($End);
                $start = $dateStart->format('Y-m-d H:i:s');
                $end = $dateEnd ->format('Y-m-d H:i:s');
                
//                $arr['Start'] = $Start;
//                $arr['End'] = $End;
                
                $arr = json_encode($arr);
                $arr1 = array();
                $arr1['Title'] = $Title_en != '' ? $Title_en : "";
                $arr1['Description'] = $Description_en != '' ? $Description_en : "";
                $arr1['Link'] = $Link_en != '' ? $Link_en : "";
                $arr1 = json_encode($arr1);
                $data = array(
                    'PositionID' => $PositionID,
                    'Published' => $Published,
                    'Created' => date('Y-m-d H:i:s'),
                    'Data' => $arr,
                    'Data_en' => $arr1,
                    'Start' => $start,
                    'End' => $end,
                    'Store' => $Store
                    
                );
                if (isset($_FILES['Image_upload'])) {
                    if ($_FILES['Image_upload']['tmp_name'] != '') {
                        $this->upload_to = "banner_thumb/$ID";
                        $data['Thumb'] = $this->upload_image_single();
                        $this->delete_file_path($user->Thumb);
                    }
                }
                $this->db->where("ID", $ID);
                $this->db->update("ttp_banner", $data);
            }
        }
        redirect(ADMINPATH . '/home/site_banner/');
    }

}

?>