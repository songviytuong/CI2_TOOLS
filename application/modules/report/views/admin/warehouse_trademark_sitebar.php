<?php 
$url = base_url().ADMINPATH.'/report';
$segment = $this->uri->segment(3);
$segment_current_bonus = $this->uri->segment(4);
?>
<div class="sitebar">
    <p class="title"><i class="fa fa-qrcode" aria-hidden="true"></i> Quét QR Code</p>
    <ul>
        <li><a href="<?php echo $url.'/warehouse_scancode/sale' ?>" <?php echo $segment=="warehouse_scancode" && ($segment_current_bonus=="sale" || $segment_current_bonus=="" || $segment_current_bonus=="preview_order") ? "class='active'" : '' ; ?>>Xuất bán hàng</a></li>
        <li><a href="<?php echo $url.'/warehouse_scancode/export_another' ?>" <?php echo $segment=="warehouse_scancode" && ($segment_current_bonus=="export_another" || $segment_current_bonus=="preview_export_bill") ? "class='active'" : '' ; ?>>Xuất cho/tặng/hủy</a></li>
        <li><a href="<?php echo $url.'/warehouse_scancode/export_mobilization' ?>" <?php echo $segment=="warehouse_scancode" && ($segment_current_bonus=="export_mobilization" || $segment_current_bonus=="preview_mobilization") ? "class='active'" : '' ; ?>>Xuất lưu chuyển kho</a></li>
        <li><a href="<?php echo $url.'/warehouse_scancode/import_rejectorder' ?>" <?php echo $segment=="warehouse_scancode" && $segment_current_bonus=="import_rejectorder" ? "class='active'" : '' ; ?>>Nhập hàng trả về</a></li>
        <li><a href="<?php echo $url.'/warehouse_scancode/import_mobilization' ?>" <?php echo $segment=="warehouse_scancode" && $segment_current_bonus=="import_mobilization" ? "class='active'" : '' ; ?>>Nhập lưu chuyển kho</a></li>
        <li><a href="<?php echo $url.'/warehouse_scancode/define_qrcode' ?>" <?php echo $segment=="warehouse_scancode" && $segment_current_bonus=="define_qrcode" ? "class='active'" : '' ; ?>>Khai báo mã QR Code</a></li>
    </ul>
    <p class="title"><i class="fa fa-area-chart fa-fw"></i> Mua hàng</p>
    <ul>
        <li><a href="<?php echo $url.'/warehouse_production' ?>" <?php echo $segment=="warehouse_production" ? "class='active'" : '' ; ?>>Nhà cung cấp</a></li>
        <li><a>Hợp đồng</a></li>
        <li><a href="<?php echo $url.'/warehouse_inventory_perchaseorder/index' ?>" <?php echo $segment=="warehouse_inventory_perchaseorder" ? "class='active'" : '' ; ?>>Đề nghị mua hàng</a></li>
    </ul>
    <p class="title"><i class="fa fa-th fa-fw"></i> Quản lý kho</p>
    <ul>
        <?php 
        if($this->user->UserType!=6){
        ?>
        <li><a href="<?php echo $url.'/warehouse_warehouse' ?>" <?php echo $segment=="warehouse_warehouse" && $segment_current_bonus=="" ? "class='active'" : '' ; ?>>Danh mục kho</a></li>
        <li><a href="<?php echo $url.'/warehouse_warehouse/transaction' ?>"  <?php echo $segment=="warehouse_warehouse" && $segment_current_bonus=="transaction" ? "class='active'" : '' ; ?>>Hoạt động kho</a></li>
        <li><a>Shipment Plan</a></li>
        <?php 
        }
        ?>
        <li><p <?php echo $segment=="warehouse_inventory_import" ? "class='active'" : '' ; ?>><a href="<?php echo $url.'/warehouse_inventory_import' ?>">Nhập kho</a> <i class="fa fa-caret-down"></i></p>
            <ul <?php echo $segment=='warehouse_inventory_import' ? "style='display:block'" : "" ; ?>>
                <?php 
                //echo $this->user->UserType==6 ? "<a href='".$url.'/warehouse_inventory_import/import_from_production'."'>Phiếu yêu cầu nhập kho</a>" : "";
                ?>
                <li><a href="<?php echo $url.'/warehouse_inventory_import/set_import_type?Type=0' ?>">Mua từ nhà cung cấp</a></li>
                <li><a href="<?php echo $url.'/warehouse_inventory_import/set_import_type?Type=2' ?>">Nhập khác</a></li>
                <?php 
                if($this->user->UserType!=6){
                ?>
                <li><a href="<?php echo $url.'/warehouse_inventory_import/set_import_type?Type=1' ?>">Hàng bán bị trả lại</a></li>
                <?php 
                }
                ?>
            </ul>
        </li>
        <?php 
        if($this->user->UserType!=6){
        ?>
        <li><p <?php echo $segment=="warehouse_inventory_export" ? "class='active'" : '' ; ?>><a>Xuất kho </a><i class="fa fa-caret-down"></i></p>
            <ul <?php echo $segment=='warehouse_inventory_export' ? "style='display:block'" : "" ; ?>>
                <li><a href="<?php echo $url.'/warehouse_inventory_export/sale' ?>">Bán hàng</a></li>
                <li><a href="<?php echo $url.'/warehouse_inventory_export/internal' ?>">Cho / Tặng / Hủy</a></li>
                <li><a href="<?php echo $url.'/warehouse_inventory_export/request_picking' ?>">Đề nghị soạn hàng</a></li>
            </ul>
        </li>
        <li><a href="<?php echo $url.'/warehouse_inventory_transferproducts/' ?>" <?php echo $segment=="warehouse_inventory_transferproducts" ? "class='active'" : '' ; ?>>Lưu chuyển nội bộ</a></li>
        <li><a href="<?php echo $url.'/warehouse_inventory_position/' ?>" <?php echo $segment=="warehouse_inventory_position" ? "class='active'" : '' ; ?>>Quản lý vị trí kho</a></li>
        <li><p <?php echo $segment=="warehouse" ? "class='active'" : '' ; ?>><a>Báo cáo kho</a> <i class="fa fa-caret-down"></i></p>
            <ul <?php echo $segment=='warehouse' ? "style='display:block'" : "" ; ?>>
                <li><a>Thẻ kho</a></li>
                <li><a href="<?php echo $url.'/warehouse/report_inventory_import' ?>">Báo cáo nhập kho</a></li>
                <li><a href="<?php echo $url.'/warehouse/report_inventory_export' ?>">Báo cáo xuất kho</a></li>
                <li><a href="<?php echo $url.'/warehouse/report_inventory' ?>">Báo cáo xuất nhập tồn</a></li>
                <li><a href="<?php echo $url.'/warehouse/inventory' ?>">Kiểm tra tồn kho hàng hóa</a></li>
            </ul>
        </li>
        <?php 
        } 
        ?>
    </ul>
    <p class="title"><i class="fa fa-cubes fa-fw"></i> Quản lý sản phẩm</p>
    <ul>
        <li><a href="<?php echo $url.'/warehouse_products' ?>" <?php echo $segment=="warehouse_products" ? "class='active'" : '' ; ?>>Sản phẩm</a></li>
        <li><a href="<?php echo $url.'/warehouse_shipment' ?>" <?php echo $segment=="warehouse_shipment" ? "class='active'" : '' ; ?>>Quản lý lô sản phẩm</a></li>
        <li><a>Quản lý QR code</a></li>
        <li><a>Ngành hàng</a></li>
    </ul>
    <p class="title"><i class="fa fa-cogs fa-fw"></i> Liên kết sản phẩm</p>
    <ul>
        <li><a href="<?php echo $url.'/warehouse_country' ?>" <?php echo $segment=="warehouse_country" ? "class='active'" : '' ; ?>>Nước sản xuất</a></li>
        <li><a href="<?php echo $url.'/warehouse_trademark' ?>" <?php echo $segment=="warehouse_trademark" ? "class='active'" : '' ; ?>>Thương hiệu</a></li>
		<li><a href="<?php echo $url.'/warehouse_trademark_faq' ?>" <?php echo $segment=="warehouse_trademark_faq" ? "class='active'" : '' ; ?>>Thương hiệu - FAQ</a></li>
        <li><a href="<?php echo $url.'/warehouse_properties' ?>" <?php echo $segment=="warehouse_properties" ? "class='active'" : '' ; ?>>Thuộc tính sản phẩm</a></li>
    </ul>
</div>
<a id='closesitebar' title='Đóng sitebar & mở rộng khung nội dung chính'><i class="fa fa-angle-double-left"></i></a>
<a id='opensitebar' title='Hiển thị sitebar'><i class="fa fa-angle-double-right"></i></a>