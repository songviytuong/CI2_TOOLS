<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$products_bundle = $this->db->query("select Title,ID from ttp_report_products where VariantType=2")->result();
$arr_bundle = array();
if(count($products_bundle) >0){
    foreach($products_bundle as $row){
        $arr_bundle[$row->ID] = $row->Title;
    }
}
?>
<div class="containner">
	<div class="import_select_progress">
        <div class="block1">
            <h1></h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row">
    	<label class="control-label col-xs-2">Thời gian báo cáo <span class='pull-right'>:</span> </label>
    	<label class="col-xs-4"><?php echo "Từ <b style='margin-right:10px;margin-left:10px'>".date('d/m/Y',strtotime($startday))."</b> đến <b style='margin-left:10px'>".date('d/m/Y',strtotime($stopday)); ?></b></label>
    	<div class="col-xs-6 text-right">
    		<?php 
    		$querystr = "?".http_build_query($_GET);
    		?>
    		<a href="<?php echo base_url().ADMINPATH.'/report/report_sales/export_report_search_3'.$querystr ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export excel</a>
    	</div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Loại đơn hàng <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select name="OrderType" class="form-control" id="OrderType" onchange="changefillter()">
                <option value='-1' <?php echo $OrderType==-1 ? "selected='selected'" : "" ; ?>>Tất cả đơn hàng</option>
                <?php 
                $arr_status = $this->define_model->get_order_status('status','order');
                $array_status = array();
                foreach($arr_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $array_status[$code] = $ite->name;
                    $selected = $code==$OrderType ? "selected='selected'" : "" ;
                    echo "<option value='$code' $selected>$ite->name</option>";
                }   
                ?>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Kho bán hàng <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="WarehouseID" onchange="changefillter()">
                <option value="0">-- Chọn kho bán hàng --</option>
                <?php 
                $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
                if(count($warehouse)>0){
                    foreach($warehouse as $row){
                        $selected = $row->ID==$WarehouseID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label class="control-label col-xs-2 text-right">Thương hiệu </label>
        <div class="col-xs-4">
            <select class="form-control" id="TrademarkID" onchange="changefillter()">
                <option value="0">-- Tất cả thương hiệu --</option>
                <?php 
                $trademark = $this->db->query("select ID,Title from ttp_report_trademark order by Title ASC")->result();
                if(count($trademark)>0){
                    foreach($trademark as $row){
                        $selected = $row->ID==$TrademarkID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Loại sản phẩm <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="ProductsID" onchange="changefillter()">
                <option value="0">-- Tất cả sản phẩm --</option>
                <?php 
                $productslist = $this->db->query("select DISTINCT d.ID,d.Title from ttp_report_products d where d.TrademarkID=$TrademarkID and VariantType in(0,1) order by d.Title ASC")->result();
                if(count($productslist)>0){
                    foreach($productslist as $row){
                        $selected = $row->ID==$ProductsID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>".$row->Title."</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label class="control-label col-xs-2 text-right">Lô sản phẩm  </label>
        <div class="col-xs-4">
            <select class="form-control" id="ShipmentID" onchange="changefillter()">
                <option value="0">-- Tất cả lô hàng --</option>
                <?php 
                $Shipment = $this->db->query("select * from ttp_report_shipment where ProductsID=$ProductsID")->result();
                if(count($Shipment)>0){
                    foreach($Shipment as $row){
                        $selected = $row->ID==$ShipmentID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->ShipmentCode</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin: 0px;">
    	<table class="table table-bordered">
    		<tr>
    			<th>STT</th>
    			<th>Mã SP</th>
    			<th style="width:200px">Tên SP </th>
    			<th>Đơn vị</th>
    			<th>Số lượng bán</th>
                <th>Mã đơn hàng</th>
                <th>Ngày bán hàng</th>
                <th>Trạng thái DH</th>
    		</tr>
            <tr><th colspan='9'>HÀNG BÁN LẺ</th></tr>
    		<?php 
            if(count($data)>0){
    			$i=1;
                $total = 0;
    			foreach($data as $key=>$row){
                    $total = $total+$row->Amount;
    				echo "<tr>";
    				echo "<td>$i</td>";
    				echo "<td>".$row->MaSP."</td>";
    				echo "<td>".$row->Title."</td>";
    				echo "<td>".$row->Donvi."</td>";
                    echo "<td class='text-right'><b>".number_format($row->Amount)."</b></td>";
                    echo "<td>".$row->MaDH."</td>";
                    echo "<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>";
                    echo "<td>".$array_status[$row->Status]."</td>";
    				echo "</tr>";
    				$i++;
    			}
                echo "<tr><th colspan='4' class='text-right'>TỔNG CỘNG</th><th class='text-right'>".number_format($total)."</th><th colspan='3'></th></tr>";
    		}else{
                echo "<tr><td colspan='8'>Không có dữ liệu !</td></tr>";
            }
            echo "<tr><th colspan='8'>HÀNG BÁN COMBO</th></tr>";
            if(count($data1)>0){
                $i=1;
                $total = 0;
                foreach($data1 as $key=>$row){
                    $total = $total+$row->Amount;
                    $combo = isset($arr_bundle[$row->ProductsID]) ? $arr_bundle[$row->ProductsID] : 'Không xác định combo' ;
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td><a title='".$combo."'>".$row->MaSP."</a></td>";
                    echo "<td><a title='".$combo."'>".$row->Title."</a></td>";
                    echo "<td>".$row->Donvi."</td>";
                    echo "<td class='text-right'><b>".number_format($row->Amount)."</b></td>";
                    echo "<td>".$row->MaDH."</td>";
                    echo "<td>".date('d/m/Y',strtotime($row->Ngaydathang))."</td>";
                    echo "<td>".$array_status[$row->Status]."</td>";
                    echo "</tr>";
                    $i++;
                }
                echo "<tr><th colspan='4' class='text-right'>TỔNG CỘNG</th><th class='text-right'>".number_format($total)."</th><th colspan='3'></th></tr>";
            }else{
                echo "<tr><td colspan='8'>Không có dữ liệu !</td></tr>";
            }
    		?>
    	</table>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
	.table tr th{background:#EEE;}
</style>
<script type="text/javascript">

    $(document).ready(function () {

        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });
    

    function changefillter(){
    	var WarehouseID = $("#WarehouseID").val();
    	var ShipmentID = $("#ShipmentID").val();
    	var TrademarkID = $("#TrademarkID").val();
    	var ProductsID = $("#ProductsID").val();
        var OrderType = $("#OrderType").val();
    	window.location = "<?php echo base_url().ADMINPATH.'/report/report_sales/report_search_case_3?' ?>"+"WarehouseID="+WarehouseID+"&ShipmentID="+ShipmentID+"&TrademarkID="+TrademarkID+"&ProductsID="+ProductsID+"&OrderType="+OrderType;
    }

</script>