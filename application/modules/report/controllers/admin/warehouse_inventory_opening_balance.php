<?php 
class Warehouse_inventory_opening_balance extends Admin_Controller { 
 
    public $limit = 30;
 	public $user;
 	public $classname="warehouse_inventory_opening_balance";

    public function __construct() { 
        parent::__construct();   
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->write_view('sitebar','admin/warehouse_sitebar',array('user'=>$this->user));
        $this->template->write_view('header','admin/header',array('user'=>$this->user));
        $this->template->add_js("public/admin/js/script_report.js");
        $this->template->add_doctype(); 
    }

    public function index(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'r',$this->user->IsAdmin);
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;

        $startday = $this->session->userdata("import_startday");
        $startday = $startday!='' ? $startday : date('Y-m-01',time()) ;
        $stopday = $this->session->userdata("import_stopday");
        $stopday = $stopday!='' ? $stopday : date('Y-m-d',time()) ;

        $limit_str = "limit $start,$this->limit";
        $fillter = $this->session->userdata('fillter');
        $fillter = $fillter=='' ? "" : " and ".$fillter;
        $bonus = $this->get_bonus_fillter();
        $fillter = $bonus=='' ? $fillter : $fillter." and ".$bonus;
        $nav = $this->db->query("select count(1) as nav from ttp_report_inventory_balance a,ttp_report_products b,ttp_report_shipment c,ttp_report_warehouse_position d where d.ID=a.PositionID and c.ID=a.ShipmentID and a.ProductsID=b.ID $fillter")->row();
        $nav = $nav ? $nav->nav : 0;
        $object = $this->db->query("select a.*,b.MaSP,b.Donvi,b.Title as TenSP,c.ShipmentCode as Shipment,d.Position,d.Rack,d.Col,d.Row from ttp_report_inventory_balance a,ttp_report_products b,ttp_report_shipment c,ttp_report_warehouse_position d where d.ID=a.PositionID and c.ID=a.ShipmentID and a.ProductsID=b.ID $fillter order by a.ID DESC $limit_str")->result();
        $data = array(
            'base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_opening_balance/',
            'fill_data' => $this->session->userdata('fillter'),
            'fillter'   => $fillter,
            'data'      => $object,
            'start'     => $start,
            'find'      => $nav,
            'startday'  => $startday,
            'stopday'   => $stopday,
            'nav'       => $this->lib->nav(base_url().ADMINPATH.'/report/warehouse_inventory_opening_balance/index',5,$nav,$this->limit)
        );
        $this->template->add_title('Balance | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_opening_balance_home',$data);
        $this->template->render();
    }

    public function setfillter(){
        $arr_fieldname = array(0=>"b.MaSP",1=>"c.ShipmentCode",2=>"a.WarehouseID",3=>"a.CheckedDay");
        $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
        $FieldName = isset($_POST['FieldName']) ? $_POST['FieldName'] : array() ;
        $FieldOparation = isset($_POST['FieldOparation']) ? $_POST['FieldOparation'] : array() ;
        $FieldText = isset($_POST['FieldText']) ? $_POST['FieldText'] : array() ;
        $str = array();
        if(count($FieldName)>0){
            $i=0;
            $arrtemp = array();
            foreach ($FieldName as $key => $value) {
                if(!in_array($value,$arrtemp)){
                    if(isset($arr_fieldname[$value]) && isset($FieldOparation[$i])){
                        if(isset($arr_oparation[$FieldOparation[$i]]) && isset($FieldText[$i])){
                            if($arr_oparation[$FieldOparation[$i]]=='like'){
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '%".mysql_real_escape_string($FieldText[$i])."%'";
                            }else{
                                $str[] = $arr_fieldname[$value].' '.$arr_oparation[$FieldOparation[$i]]." '".mysql_real_escape_string($FieldText[$i])."'";
                            }
                            $i++;
                        }
                    }
                    $arrtemp[] = $value;
                }
            }
            if(count($str)>0){
                $sql = implode(' and ',$str);
                $this->session->set_userdata("fillter",$sql);
            }else{
                $this->session->set_userdata("fillter","");    
            }
        }else{
            $this->session->set_userdata("fillter","");
        }
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ;
        redirect($referer);
    }

    public function get_bonus_fillter(){
        $fill = array();
        $warehouse = $this->session->userdata('WarehouseID');
        if($warehouse!=''){
            $fill[] = "a.WarehouseID=".$warehouse;
        }
        $CheckedDay = $this->session->userdata('CheckedDay');
        if($CheckedDay!=''){
            $fill[] = "a.CheckedDay='".$CheckedDay."'";
        }
        return implode(' and ', $fill);
    }

    public function set_bonus_fillter(){
        $WarehouseID = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        $CheckedDay = isset($_GET['CheckedDay']) ? $_GET['CheckedDay'] : '' ;
        $this->session->set_userdata('WarehouseID',$WarehouseID);
        $this->session->set_userdata('CheckedDay',$CheckedDay);
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_opening_balance';
        redirect($refer);
    }

    public function add(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $this->template->add_title('Import balance | Warehouse Report Tools');
        $this->template->write_view('content','admin/warehouse_inventory_opening_balance_add',array('base_link' =>  base_url().ADMINPATH.'/report/warehouse_inventory_opening_balance/'));
        $this->template->render();
    }

    public function edit(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $warehouse = $this->uri->segment(5);
        $date = $this->uri->segment(6);
        if($warehouse>0 && $date!='' && strtotime($date)!=''){
            $this->template->add_title('View Import balance | Warehouse Report Tools');
            $this->template->write_view('content','admin/warehouse_inventory_opening_balance_edit',
                array(
                    'base_link' => base_url().ADMINPATH.'/report/warehouse_inventory_opening_balance/',
                    'WarehouseID' => $warehouse,
                    'date'      => $date
                )
            );
            $this->template->render();
        }
    }

    public function add_new(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'w',$this->user->IsAdmin);
        $BalanceDate = isset($_POST['BalanceDate']) ? $_POST['BalanceDate'] : date('Y-m-d') ;
        $WarehouseID = isset($_POST['WarehouseID']) ? $_POST['WarehouseID'] : 0 ;
        if($WarehouseID>0 && is_numeric($WarehouseID)){
            $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
            $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
            $PositionID = isset($_POST['PositionID']) ? $_POST['PositionID'] : array() ;
            $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
            if(is_array($ProductsID) && count($ProductsID)>0){
                $arr_sql = array();
                foreach($ProductsID as $key=>$value){
                    if($value!=''){
                        $shipment = isset($ShipmentID[$key]) ? (int)$ShipmentID[$key] : 0 ;
                        $position = isset($PositionID[$key]) ? (int)$PositionID[$key] : 0 ;
                        $amountitem = isset($Amount[$key]) ? (int)$Amount[$key] : 0 ;
                        $amountitem = $amountitem<0 ? 0 : $amountitem ;
                        if($shipment>0 && $position>0){
                            $check = $this->db->query("select ID from ttp_report_inventory_balance where CheckedDay = '$BalanceDate' and WarehouseID=$WarehouseID and ProductsID=$value and ShipmentID=$shipment and PositionID=$position")->row();
                            if($check){
                                $this->db->query("update ttp_report_inventory_balance set Amount=$amountitem where CheckedDay = '$BalanceDate' and WarehouseID=$WarehouseID and ProductsID=$value and ShipmentID=$shipment and PositionID=$position");
                            }else{
                                $arr_sql[] = "($WarehouseID,$shipment,$position,$value,'".date('Y-m-d H:i:s')."','$BalanceDate',$amountitem,".$this->user->ID.")";
                            }
                        }
                    }
                }
                if(count($arr_sql)>0){
                    $arr_sql = 'insert into ttp_report_inventory_balance(WarehouseID,ShipmentID,PositionID,ProductsID,Created,CheckedDay,Amount,UserID) values'.implode(',',$arr_sql);
                    $this->db->query($arr_sql);
                }
            }
        }
        redirect(ADMINPATH.'/report/warehouse_inventory_opening_balance/');
    }

    public function update(){
        $this->lib->check_permission($this->user->DetailRole,$this->classname,'m',$this->user->IsAdmin);
        $BalanceDate = isset($_POST['BalanceDate']) ? $_POST['BalanceDate'] : date('Y-m-d') ;
        $WarehouseID = isset($_POST['WarehouseID']) ? $_POST['WarehouseID'] : 0 ;
        if($WarehouseID>0 && is_numeric($WarehouseID)){
            $ProductsID = isset($_POST['ProductsID']) ? $_POST['ProductsID'] : array() ;
            $ShipmentID = isset($_POST['ShipmentID']) ? $_POST['ShipmentID'] : array() ;
            $PositionID = isset($_POST['PositionID']) ? $_POST['PositionID'] : array() ;
            $Amount = isset($_POST['Amount']) ? $_POST['Amount'] : array() ;
            if(is_array($ProductsID) && count($ProductsID)>0){
                $arr_sql = array();
                foreach($ProductsID as $key=>$value){
                    if($value!=''){
                        $shipment = isset($ShipmentID[$key]) ? (int)$ShipmentID[$key] : 0 ;
                        $position = isset($PositionID[$key]) ? (int)$PositionID[$key] : 0 ;
                        $amountitem = isset($Amount[$key]) ? (int)$Amount[$key] : 0 ;
                        $amountitem = $amountitem<0 ? 0 : $amountitem ;
                        if($shipment>0 && $position>0){
                            $check = $this->db->query("select ID from ttp_report_inventory_balance where CheckedDay = '$BalanceDate' and WarehouseID=$WarehouseID and ProductsID=$value and ShipmentID=$shipment and PositionID=$position")->row();
                            if($check){
                                $this->db->query("update ttp_report_inventory_balance set Amount=$amountitem where CheckedDay = '$BalanceDate' and WarehouseID=$WarehouseID and ProductsID=$value and ShipmentID=$shipment and PositionID=$position");
                            }else{
                                $arr_sql[] = "($WarehouseID,$shipment,$position,$value,'".date('Y-m-d H:i:s')."','$BalanceDate',$amountitem,".$this->user->ID.")";
                            }
                        }
                    }
                }
                if(count($arr_sql)>0){
                    $arr_sql = 'insert into ttp_report_inventory_balance(WarehouseID,ShipmentID,PositionID,ProductsID,Created,CheckedDay,Amount,UserID) values'.implode(',',$arr_sql);
                    $this->db->query($arr_sql);
                }
            }
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_opening_balance/edit/'.$WarehouseID.'/'.$BalanceDate ;
        redirect($refer);
    }

    public function checkredirect(){
        $warehouse = $this->uri->segment(5);
        $date = $this->uri->segment(6);
        if($warehouse>0 && $date!='' && strtotime($date)!=''){
            $check = $this->db->query("select count(1) as SL from ttp_report_inventory_balance where CheckedDay = '$date' and WarehouseID=$warehouse")->row();
            if($check->SL>0){
                redirect(ADMINPATH.'/report/warehouse_inventory_opening_balance/edit/'.$warehouse.'/'.$date);
            }
        }
        $refer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : ADMINPATH.'/report/warehouse_inventory_opening_balance/add';
        redirect($refer);
    }

    public function remove_row(){
        $ID = isset($_POST['ID']) ? (int)$_POST['ID'] : 0 ;
        $this->db->query("delete from ttp_report_inventory_balance where ID=$ID");
    }

    public function load_position(){
        $warehouse = isset($_GET['WarehouseID']) ? (int)$_GET['WarehouseID'] : 0 ;
        echo "<option value=''>-- Chọn --</option>";
        $result = $this->db->query("select DISTINCT Position from ttp_report_warehouse_position where WarehouseID=$warehouse")->result();
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->Position'>$row->Position</option>";
            }
        }
    }

    public function load_rack_by_position(){
        $Title = isset($_POST['Title']) ? $_POST['Title'] : '' ;
        echo "<option value=''>-- Chọn --</option>";
        $result = $this->db->query("select DISTINCT Rack from ttp_report_warehouse_position where Position = '$Title'")->result();
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->Rack'>$row->Rack</option>";
            }
        }
    }

    public function load_cols_by_rack(){
        $Position = isset($_POST['Position']) ? $_POST['Position'] : '' ;
        $Rack = isset($_POST['Rack']) ? $_POST['Rack'] : '' ;
        echo "<option value=''>-- Chọn --</option>";
        $result = $this->db->query("select DISTINCT Col from ttp_report_warehouse_position where Position='$Position' and Rack='$Rack'")->result();
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->Col'>$row->Col</option>";
            }
        }
    }

    public function load_row_by_col(){
        $Col = isset($_POST['Col']) ? $_POST['Col'] : '' ;
        $Position = isset($_POST['Position']) ? $_POST['Position'] : '' ;
        $Rack = isset($_POST['Rack']) ? $_POST['Rack'] : '' ;
        echo "<option value=''>-- Chọn --</option>";
        $result = $this->db->query("select DISTINCT Row,ID from ttp_report_warehouse_position where Position='$Position' and Rack='$Rack' and Col='$Col'")->result();
        if(count($result)>0){
            foreach($result as $row){
                echo "<option value='$row->ID'>$row->Row</option>";
            }
        } 
    }
    
}