<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'add_new' ?>" method="POST">
			<h3>Tạo mới đơn vị vận chuyển <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Save</button></h3>
			<hr>
			<div class="row">
				<label for="" class="col-xs-2 control-label">Tên đơn vị vận chuyển</label>
				<div class="col-xs-4">
					<input type="text" class="form-control" name="Title" required />
				</div>
				<label for="" class="col-xs-2 control-label text-right">Mã viết tắt </label>
				<div class="col-xs-4">
					<input type="text" class="form-control" name="Code" required />
				</div>
			</div>
			<hr>
			<h3>Danh sách nhân viên giao nhận</h3>
			<hr>
			<div class="row">
				<?php 
				$result = $this->db->query("select * from ttp_user where UserType=15")->result();
				if(count($result)>0){
					foreach($result as $row){
						echo "<div class='col-xs-2'><input type='checkbox' name='user[]' value='$row->ID' /> $row->UserName</div>";
					}
				}
				?>
			</div>
		</form>
	</div>
</div>
<style>
	h3{margin:0px;}
	.body_content .containner{min-height: 569px !important;}
</style>