<div class="containner">
    <form action="<?php echo $base_link.'save_data_update_supplier' ?>" method="post">
    	<div class="import_select_progress">
    	    <div class="block1">
    	    	<h1>Cập nhật danh sách sản phẩm</h1>
    	    </div>
        </div>
        <div class="row">
        	<label class="col-xs-2 control-label">Chọn nhà cung cấp</label>
        	<div class="col-xs-3">
        		<select class="form-control" id="supplier" name="SupplierID">
        		<?php 
        		$supplier_list = $this->db->query("select a.* from ttp_user_supplier b,ttp_report_production a where b.SupplierID=a.ID and b.UserID=".$this->user->ID)->result();
        		if(count($supplier_list)>0){
        			foreach($supplier_list as $row){
        				echo "<option value='$row->ID'>$row->Title</option>";
        			}
        		}
        		?>
        		</select>
        	</div>
        </div>
        <div class="row">
            <label class="col-xs-2 control-label">Tiêu đề bảng cập nhật</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" name="Title" placeholder="Ví dụ :  Cập nhật giá cho tháng 09/2016,..." required />
            </div>
        </div>
        <div class="row">
            <label class="col-xs-2 control-label">Mô tả cho bảng cập nhật</label>
            <div class="col-xs-10">
                <textarea name="Note" class="form-control" placeholder="Ví dụ : Đợt cập nhật này giá các sản phẩm thịt bò sẽ được chiết khấu thêm 5% nữa ." required></textarea>
            </div>
        </div>
        <hr>
        <div class="row" style="min-height:100px">
        	<div class="col-xs-1"><span class="icon_list" style="background: #c8f7c8;">1</span></div>
        	<label class="col-xs-2 control-label">Chọn danh sách sản phẩm</label>
        	<div class="col-xs-5">
        		<input type="file" id="file_import" onchange="activeimport()" /><br>
        		<p>(*) chỉ chấp nhận file excel (xls,xlsx)</p>
        	</div>
        	<div class="col-xs-4"><a class="btn btn-primary pull-right" href="assets/template/template_update_products.xlsx"><i class="fa fa-download"></i> Download danh sách sản phẩm mẫu</a></div>
        </div>
        <div id="content_import"></div>
    </form>
</div>
<style>
	.icon_list{width: 30px;height:30px;text-align:center;line-height: 30px;background: #eee;border-radius: 50%;float:right;}
	.col-xs-4.specialcol4{    background: #fff9dc;
    padding: 10px 15px;
    min-height: 105px;
    margin-right:15px;
    border: 1px solid #f9eebb;}
    .col-xs-4.specialcol4 a{margin-right:15px;}
    .col-xs-4.specialcol4 i{margin-right: 5px;font-size: 15px;color:#ccc;}
    .col-xs-4.specialcol4 i.active{color:#090;}
    .col-xs-4.specialcol4 b{color:#27c;}
</style>

<script>
	
    var baselink = "<?php echo $base_link ?>";

    function activeimport(){
        var supplier = $("#supplier").val();
        var Fileinput = document.getElementById("file_import");
        var file = Fileinput.files[0];
        var fd = new FormData();
        fd.append('supplier',supplier);
        fd.append('file', file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', baselink+'upload_import', true);
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                $("#content_import").html('<div class="loader">Loading...</div>');
                $("#content_import").html("Đang tải file lên hệ thống ...");
            }
        };
        xhr.onload = function() {
            if (this.status == 200) {
                $("#content_import").html(xhr.responseText);
            }else{
                console.log(xhr.responseText);
            }
        };
        xhr.send(fd);
    }

</script>