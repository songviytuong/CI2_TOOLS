<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3>SYSTEM GROUP</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <form action="<?php echo $base_link . 'search'; ?>" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Search for..." required>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ROLES <small> / Edit role</small></h2>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form data-parsley-validate class="form-horizontal form-label-left" action="<?php echo $base_link ."update" ?>" method="post">
                        <input type='hidden' name="ID" value="<?php echo $data->ID ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Roles Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" required="required" class="form-control col-md-7 col-xs-12" name="Title" value="<?php echo $data->Title ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div>
                                    <label style="padding-top:7px">
                                        <input type="radio" name="Status" value="1" <?php echo $data->Published==1 ? "checked='checked'" : '' ; ?>> &nbsp; Enable &nbsp;
                                        <input type="radio" name="Status" value="0" <?php echo $data->Published==0 ? "checked='checked'" : '' ; ?>> Disable
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php 
                                $result = $this->db->query("select * from ttp_sitetree")->result();
                                if(count($result)>0){
                                    echo '<table class="table table-striped responsive-utilities jambo_table">';
                                    echo '<thead>
                                            <tr class="headings">
                                                <th>STT</th>
                                                <th>Module </th>
                                                <th>Read</th>
                                                <th>Write</th>
                                                <th>Modify</th>
                                                <th>Delete</th>
                                                <th>Full</th>
                                                <th>Published</th>
                                            </tr>
                                        </thead><tbody>';
                                    $i=0;
                                    $permission = json_decode($data->DetailRole,true);
                                    foreach($result as $row){
                                        $i++;
                                        $read='';
                                        $write='';
                                        $modify='';
                                        $delete='';
                                        $full='';
                                        $temp=0;
                                        if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('r',$permission[$row->Classname])){
                                            $read="checked='checked'";
                                            $temp++;
                                        }
                                        if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('w',$permission[$row->Classname])){
                                            $write="checked='checked'";
                                            $temp++;
                                        }
                                        if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('m',$permission[$row->Classname])){
                                            $modify="checked='checked'";
                                            $temp++;
                                        }
                                        if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('d',$permission[$row->Classname])){
                                            $delete="checked='checked'";
                                            $temp++;
                                        }
                                        if($temp==4){
                                            $full="checked='checked'";
                                        }
                                    ?>
                                    
                                        <tr class="even pointer">
                                            <td class="a-center ">
                                                <?php echo $i; ?>
                                            </td>
                                            <td><?php echo $row->Title; ?></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="r" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $read ?> /></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="w" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $write ?> /></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="m" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $modify ?> /></td>
                                            <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="d" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $delete ?> /></td>
                                            <td><input type='checkbox' class='checkfull' data="<?php echo $row->Classname ?>" <?php echo $full ?> /></td>
                                            <td class="a-right a-right "><?php echo $row->Published==1 ? "Enable" : "Disable" ; ?></td>
                                            
                                        </tr>

                                    <?php 
                                    }
                                    echo "</tbody></table>";
                                }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
