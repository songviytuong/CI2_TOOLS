<?php 
class Pos_inventory_import extends Admin_Controller { 
 
 	public $user;
 	public $classname="pos_inventory_import";
    public $limit=15;
    public $WarehouseID=2;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('pos');
        $this->template->add_doctype();
        $warehouse = $this->session->userdata('branch');
        $this->WarehouseID = $warehouse!='' ? $warehouse : $this->WarehouseID ;
    }

    public function create(){
        $this->lib->check_permission_function($this->user);
        $files = scandir('log/pos/purchase_order');
        if(count($files)==2){
            $txt = time();
            file_put_contents('log/pos/purchase_order/draft_order_'.$txt.'.txt',json_encode(array('products'=>array(),'price'=>array(),'partner'=>array('ID'=>'','Note'=>'','Number'=>'','WarehouseID'=>$this->WarehouseID,'Production'=>45,'Active'=>0,'Day'=>date('Y-m-d')))));
        }
        $this->template->add_title('Import warehouse | POS Tools');
		$this->template->write_view('content','admin/purchase_order');
        $this->template->write_view('header','admin/header',array(
            'user'=>$this->user,
            'controllers'=>'Giao dịch',
            'function'=>'<a href="'.ADMINPATH.'/pos/pos_inventory_import">Nhập hàng</a>',
            'bonus'=>'<a>Tạo phiếu nhập hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'purchase_order'));
		$this->template->render();
	}

    public function index(){
        $this->template->add_title('PO | POS Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_perchaseorder a,ttp_report_production b where a.WarehouseID=$this->WarehouseID and a.ProductionID=b.ID")->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select a.*,b.Title from ttp_report_perchaseorder a,ttp_report_production b where a.WarehouseID=$this->WarehouseID and a.ProductionID=b.ID order by a.ID DESC ".$limit_str)->result();
        $data = array(
            'data'  =>$result,
            'total' =>$nav,
            'start' =>$start,
            'limit' =>$this->limit,
            'nav'   =>$this->lib->nav(base_url().ADMINPATH.'/pos/pos_inventory_import/bill',5,$nav,$this->limit)
        );
        $this->template->write_view('content','admin/purchase_order_bill',$data);
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Giao dịch','function'=>'<a>Nhập hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'purchase_order'));
        $this->template->render();
    }

    public function view_order($id=0){
        $result = $this->db->query("select a.*,b.MaKho as WarehouseTitle,c.UserName,d.Title as ProductionTitle from ttp_report_perchaseorder a,ttp_report_warehouse b,ttp_user c,ttp_report_production d where a.ProductionID=d.ID and a.UserID=c.ID and a.WarehouseID=b.ID and a.ID=$id")->row();
        if($result){
            $this->template->add_title('Sales | POS Tools');
            $this->template->write_view('content','admin/purchase_order_view',array('data'=>$result));
            $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Bán hàng','function'=>'<a href="'.ADMINPATH.'/pos/pos_inventory_import/">Nhập hàng</a>','bonus'=>'<a>Thông tin phiếu nhập</a>'));
            $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'bill'));
            $this->template->render();
        }
    }

    public function draftorder(){
        $txt = time();
        file_put_contents('log/pos/purchase_order/draft_order_'.$txt.'.txt',json_encode(array('products'=>array(),'price'=>array(),'partner'=>array('ID'=>'','Note'=>'','Number'=>'','WarehouseID'=>$this->WarehouseID,'Production'=>45,'Active'=>0,'Day'=>date('Y-m-d')))));
        $response['error'] = 0;
        $response['order'] = $txt;
        echo json_encode($response);
    }

    public function remove_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : '' ;
        @unlink("log/pos/purchase_order/draft_order_".$order.".txt");
        $files = scandir('log/pos/purchase_order');
        $first = 0;
        if(count($files)>0){
            $i=1;
            foreach($files as $row){
                if($row!='.' && $row!='..'){
                    $name = explode('_',$row);
                    $name = str_replace('.txt','',$name[2]);
                    if($i==1){
                        $first = $name;
                    }
                    $i++;
                }
            }
        }
        echo $first;
    }

    public function export_data($currentday=0){
        if($currentday==0){
            $month = date('m');
            $year = date('Y');
            $result = $this->db->query("select c.ID,c.MaSP,c.Title,b.Amount,b.PriceCurrency,c.Donvi,a.POCode,a.Created from ttp_report_perchaseorder a,ttp_report_perchaseorder_details b,ttp_report_products c where a.ID=b.POID and b.ProductsID=c.ID and month(a.Created)='$month' and year(a.Created)='$year' and a.WarehouseID=".$this->WarehouseID)->result();
        }else{
            $result = $this->db->query("select c.ID,c.MaSP,c.Title,b.PriceCurrency,b.Amount,c.Donvi,a.POCode,a.Created from ttp_report_perchaseorder a,ttp_report_perchaseorder_details b,ttp_report_products c where a.ID=b.POID and b.ProductsID=c.ID and date(a.Created)='".date('Y-m-d')."' and a.WarehouseID=".$this->WarehouseID)->result();
        }
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'SKU')
                        ->setCellValue('B1', 'Tên sản phẩm')
                        ->setCellValue('C1', 'Donvi')
                        ->setCellValue('D1', 'Số lượng')
                        ->setCellValue('E1', 'Giá nhập')
                        ->setCellValue('F1', 'Mã PO')
                        ->setCellValue('G1', 'Ngày nhập');
            $i=2;
            foreach($result as $row){
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A'.$i, $row->MaSP)
                        ->setCellValue('B'.$i, $row->Title)
                        ->setCellValue('C'.$i, $row->Donvi)
                        ->setCellValue('D'.$i, $row->Amount)
                        ->setCellValue('E'.$i, $row->PriceCurrency)
                        ->setCellValue('F'.$i, $row->POCode)
                        ->setCellValue('G'.$i, $row->Created);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('RESULT_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data !";
        }
    }

    public function get_products_by_barcode(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select ID from ttp_report_products where Barcode='$keywords'")->row();
        $response = array('error'=>1);
        if($result){
            $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$result->ID])){
                $data['products'][$result->ID] = $data['products'][$result->ID] + 1;
            }else{
                $data['products'][$result->ID] = 1;
            }
            file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
            $response = array('error'=>0);
        }
        echo json_encode($response);
    }

    public function get_products_list(){
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select MaSP,Title,ID from ttp_report_products where Title like '%$keywords%' or MaSP like '%$keywords%' or Barcode='$keywords' limit 0,20")->result();
        if(count($result)>0){
            foreach ($result as $key => $value) {
                echo "<p><a onclick='purchase_order_apply_products($value->ID)'>($value->MaSP) | $value->Title</a></p>";
            }
        }
    }

    public function reset_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['products'] = array();
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(!isset($data['products'])){
            $data['products'] = array();
        }
        if(!isset($data['reduce'])){
            $data['reduce'] = 0;
        }
        if(isset($data['products'][$products])){
            $data['products'][$products] = $data['products'][$products]+1;
        }else{
            $data['products'][$products] = 1;
        }
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function remove_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(isset($data['products'][$products])){
            unset($data['products'][$products]);
        }
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function change_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 1 ;
        if($amount>0 && $products>0){
            $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$products])){
                $data['products'][$products] = $amount;
            }
            file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function change_products_price_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $price = isset($_POST['price']) ? $_POST['price'] : 0 ;
        if($price>=0 && $products>0){
            $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['price'][$products] = $price;
            file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function load_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $this->load->view("admin/purchase_order_draft_order_products",array('data'=>$data));
    }

    public function load_customers_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        echo json_encode($data['partner']);
    }

    public function apply_code_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $code = isset($_POST['code']) ? $_POST['code'] : '' ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['Number'] = $code;
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_note_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $note = isset($_POST['note']) ? $_POST['note'] : '' ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['Note'] = $note;
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_warehouse_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $warehouse = isset($_POST['warehouse']) ? $_POST['warehouse'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['WarehouseID'] = $warehouse;
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_production_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $production = isset($_POST['production']) ? $_POST['production'] : 0 ;
        $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['partner']['Production'] = $production;
        file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_reduce_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $reduce = isset($_POST['reduce']) ? $_POST['reduce'] : 0 ;
        if($reduce>=0){
            $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['reduce'] = $reduce;
            file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function print_order($id=0){
        $order = $this->db->query("select ID,MaDH,Ngaydathang,Total,Chietkhau,Chiphi from ttp_report_order where ID=$id")->row();
        if($order){
            $this->load->view("admin/sales_draft_order_print",array('data'=>$order));
        }
    }

    public function create_order(){
        $order = isset($_POST['order']) ? $_POST['order'] : 1483950010 ;
        $code = isset($_POST['code']) ? $_POST['code'] : '' ;
        $warehouse = isset($_POST['warehouse']) ? $_POST['warehouse'] : 2 ;
        $note = isset($_POST['note']) ? $_POST['note'] : '' ;
        $production = isset($_POST['production']) ? $_POST['production'] : 45 ;
        $day = isset($_POST['purchase_day']) ? $_POST['purchase_day'] : '' ;
        $response = array("error"=>1,"message"=>"Nhập hàng thất bại !");
        if($warehouse>0 && $production>0){
            $data = file_get_contents('log/pos/purchase_order/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products']) && count($data['products'])>0){
                $monthuser = date("m",time());
                $yearuser = date("Y",time());
                $userorder = $this->db->query("select count(ID) as SL from ttp_report_perchaseorder where MONTH(Created)=$monthuser and YEAR(Created)=$yearuser")->row();
                $yearuser = date("y",time());
                $userorder = $userorder ? $userorder->SL+1+$monthuser+$yearuser : 1+$monthuser+$yearuser ;
                $monyear = date('ym');
                $idbyUser = str_pad($userorder, 5, '0', STR_PAD_LEFT);
                $MaDH = 'POCM'.$monyear."_".$idbyUser;

                $sql = array(
                    'POCode'        => $MaDH,
                    'UserID'        => $this->user->ID,
                    'HardCode'      => $code,
                    'ProductionID'  => $production,
                    'Note'          => $note,
                    'WarehouseID'   => $warehouse,
                    'Created'       => date('Y-m-d H:i:s'),
                    'DatePO'        => $day,
                    'Status'        => 5
                );
                $this->db->insert("ttp_report_perchaseorder",$sql);
                $orderid = $this->db->insert_id();
                $data_his = array(
                    'POID'      => $orderid,
                    'Status'    => 5,
                    'Note'      => 'Tạo PO mua hàng từ nhà cung cấp',
                    'UserID'    => $this->user->ID,
                    'Created'   => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_perchaseorder_history',$data_his);
                
                $total_order = 0;
                $total_products = 0;
                $total_reduce = 0;
                if(isset($data['reduce'])){
                    $total_reduce = $data['reduce'];
                }
            
                $details_insert = array();
                $arr_request = array();
                $key = array_keys($data['products']);
                $total_products = count($key);
                $products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
                if(count($products)>0){
                    foreach ($products as $row) {
                        $amount = $data['products'][$row->ID];
                        $price = $row->RootPrice ;
                        if(isset($data['price'][$row->ID])){
                            $price = $data['price'][$row->ID];
                        }
                        $total = $amount*$price;
                        $total_order += $total;
                        $shipment = $this->db->query("select ID from ttp_report_shipment where ProductsID=$row->ID")->row();
                        $shipment = $shipment ? $shipment->ID : 0 ;

                        $details_insert[] = "($orderid,$row->ID,'VND',1,$price,$amount,$total,$total,$shipment)";
                        $arr_request[] = "(ImportIDDV,$row->ID,$shipment,'VND',1,$price,$amount,$total,$total,$amount)";
                        $this->change_inventory($row->ID,$shipment,$warehouse,$amount);
                    }
                    if(count($details_insert)>0){
                        $query = "insert into ttp_report_perchaseorder_details(POID,ProductsID,Currency,ValueCurrency,PriceCurrency,Amount,TotalCurrency,TotalVND,ShipmentID) values".implode(',',$details_insert);
                        $this->db->query($query);
                        $sql['ID'] = $orderid;
                        $this->create_request_import($sql,$arr_request);
                    }
                    $this->db->query("update ttp_report_perchaseorder set TotalPrice=$total_order,TotalAmount=$total_products,Reduce=$total_reduce where ID=$orderid");
                    $data["partner"]["Active"] = $orderid;
                    file_put_contents('log/pos/purchase_order/draft_order_'.$order.'.txt',json_encode($data));
					$this->lib->write_log_data("<b>".$this->user->UserName."</b> tạo phiếu nhập mua hàng <b class='text-primary'>$MaDH</b> giá trị <b class='text-danger'>".number_format($TotalPrice-$total_reduce)."</b>",0);
                }
                $response = array("error"=>0,"message"=>"Nhập hàng thành công !","id"=>$orderid);
            }else{
                $response = array("error"=>1,"message"=>"Nhập hàng thất bại. Đơn hàng không có sản phẩm");
            }
        }
        echo json_encode($response);
    }

    public function create_request_import($data=array(),$arr = array(),$POType=0){
        $KhoID = isset($data['WarehouseID']) ? isset($data['WarehouseID']) : 0 ;
        $POID = isset($data['ID']) ? $data['ID'] : 0 ;
        $ProductionID = isset($data['ProductionID']) ? $data['ProductionID'] : 0 ;
        $DateExpected = date('Y-m-d') ;
        $POCode = isset($data['POCode']) ? $data['POCode'] : $POID ;
        $TotalAmount = isset($data['TotalAmount']) ? $data['TotalAmount'] : 0 ;
        $TotalPrice = isset($data['TotalPrice']) ? $data['TotalPrice'] : 0 ;
        $Note = isset($data['Note']) ? $data['Note'] : '' ;
        
        $Type = $POType;
        if($KhoID>0 && $POID>0){
            if(count($arr)>0){
                $thismonth = date('m',time());
                $thisyear = date('Y',time());
                $max = $this->db->query("select count(1) as max from ttp_report_inventory_import where MONTH(Created)=$thismonth and YEAR(Created)=$thisyear and Type=0")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                $max = "YCNK".$Type.'_'.$KhoID.'_'.$thisyear.$thismonth.'_'.str_pad($max, 7, '0', STR_PAD_LEFT);

                $data = array(
                    'MaNK'      => $max,
                    'POID'      => $POID,
                    'KhoID'     => $KhoID,
                    'UserID'    => $this->user->ID,
                    'NgayNK'    => $DateExpected,
                    'Type'      => $Type,
                    'ProductionID'=> $ProductionID,
                    'Note'      => "$Note ($POCode)",
                    'Status'    => 3,
                    'Ghichu'    => "$Note ($POCode)",
                    'TotalAmount'=> $TotalAmount,
                    'TotalPrice'=> $TotalPrice,
                    'Created'   => date('Y-m-d H:i:s'),
                    'LastEdited'=> date('Y-m-d H:i:s'),
                    'DepartmentID'=>$this->user->DepartmentID
                );
                $this->db->insert('ttp_report_inventory_import',$data);
                $ID = $this->db->insert_id();
                $arr = "insert into ttp_report_inventory_import_details(ImportID,ProductsID,ShipmentID,Currency,ValueCurrency,PriceCurrency,Request,TotalCurrency,TotalVND,Amount) values".implode(',',$arr);
                $arr = str_replace('ImportIDDV',$ID,$arr);
                $this->db->query($arr);
                $data_his = array(
                    'ImportID'  => $ID,
                    'Status'    => 0,
                    'Note'      => "Yêu cầu nhập kho tự động từ PO số $POCode",
                    'UserID'    => $this->user->ID,
                    'Created'   => date('Y-m-d H:i:s')
                );
                $this->db->insert('ttp_report_inventory_import_history',$data_his);
            }
        }
    }

    public function change_inventory($ProductsID,$ShipmentID,$WarehouseID,$Amount){
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1");
        if($check_exists){
            if($check_exists->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=Available+$Amount,OnHand=OnHand+$Amount,LastEdited=1 where ID=$check_exists->ID");
            }else{
                $Available = $check_exists->Available+$Amount;
                $OnHand = $check_exists->OnHand+$Amount;
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
            }
        }else{
            $Available = $Amount;
            $OnHand = $Amount;
            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
        }
    }
}
?>