<?php 
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);
$Orderstatus = isset($_GET['payment']) ? $_GET['payment'] : -1 ;

/*
*********************************
*	Define variable				*
*								*
*********************************
*/

$total_Order 		= array();
$total_no 			= 0;
$total_ok 			= 0;
$total_wait 		= 0;
$total_recived 		= 0;
$total_no_percent 	= 0;
$total_ok_percent 	= 0;
$total_wait_percent = 0;
$total_recived_percent = 0;
$total_Sales 		= 0;
$total_Products 	= 0;
$total_new_order 	= 0;
$percent_new_order 	= 0;
$percent_prod_order = 0;
$percent_success	= 0;
$percent_false		= 0;
$abs_order			= 0;
$city_order 		= array();
$area_order 		= array();
$products_order 	= array();
$branch_order 		= array();

$point_per_day		= array();
$point_per_order	= array();
$point_per_no		= array();
$point_per_ok		= array();
$point_per_wait 	= array();
$point_per_recived 	= array();

$total_day			= array();
$show_day			= array();


/*
*********************************************
*	Excute and set variable	in first range	*
*											*
*********************************************
*/

if(count($data)>0){
	foreach($data as $row){
		if(isset($products_order[$row->ProductsID])){
			$products_order[$row->ProductsID]["Price"]+=($row->Price*$row->Amount);
			$products_order[$row->ProductsID]["Total"]+=$row->Amount;
			$products_order[$row->ProductsID]["Order"][$row->ID]=1;
		}else{
			$products_order[$row->ProductsID]["Price"] = ($row->Price*$row->Amount);
			$products_order[$row->ProductsID]["Total"] = $row->Amount;
			$products_order[$row->ProductsID]["Title"] = $row->Title;
			$products_order[$row->ProductsID]["Order"][$row->ID]=1;
		}
		/* Total order */
		if(!array_key_exists($row->ID,$total_Order)){
			$total_Order[$row->ID] = "";
		}
		/* Total success and false order */
		if($row->NCC==0){
			$total_no+=($row->Price*$row->Amount);
		}
		if($row->NCC==1){
			$total_ok+=($row->Price*$row->Amount);
		}
		if($row->NCC==2){
			$total_wait+=($row->Price*$row->Amount);
		}
		if($row->NCC==3){
			$total_recived+=($row->Price*$row->Amount);
		}
		/* Total Sales */
		$total_Sales+= ($row->Price*$row->Amount) ;

		/* Total order group by city */
		if(isset($city_order[$row->CityID])){
			$city_order[$row->CityID]['sl'] += 1;
			$city_order[$row->CityID]['total'] += ($row->Price*$row->Amount) ;
			$city_order[$row->CityID]['order'][$row->ID] = 1;
		}else{
			$city_order[$row->CityID]['sl'] = 1 ;
			$city_order[$row->CityID]['total'] = ($row->Price*$row->Amount);
			$city_order[$row->CityID]['order'][$row->ID] = 1;
		}

		if(isset($area_order[$row->AreaID])){
			$area_order[$row->AreaID]['sl'] += 1;
			$area_order[$row->AreaID]['total'] += ($row->Price*$row->Amount) ;
			$area_order[$row->AreaID]['order'][$row->ID] = 1;
		}else{
			$area_order[$row->AreaID]['sl'] = 1 ;
			$area_order[$row->AreaID]['total'] = ($row->Price*$row->Amount);
			$area_order[$row->AreaID]['order'][$row->ID] = 1;
		}

		if(isset($branch_order[$row->BranchID])){
			$branch_order[$row->BranchID]['sl'] += 1;
			$branch_order[$row->BranchID]['total'] += ($row->Price*$row->Amount) ;
			$branch_order[$row->BranchID]['order'][$row->ID] = 1;
		}else{
			$branch_order[$row->BranchID]['sl'] = 1 ;
			$branch_order[$row->BranchID]['total'] = ($row->Price*$row->Amount);
			$branch_order[$row->BranchID]['order'][$row->ID] = 1;
		}

		/* Get list day */
		$time_key = date('Ymd',strtotime($row->TimeReciver));
		if(array_key_exists($time_key,$point_per_day)){
			$point_per_products[$time_key] += 1 ;
			$point_per_new[$time_key] += 1 ;
			$point_per_customer[$time_key] += 1 ;
			$point_per_order[$time_key] += 1 ;
			$point_per_day[$time_key] += ($row->Price*$row->Amount);
		}else{
			$point_per_products[$time_key] = 1 ;
			$point_per_new[$time_key] = 1 ;
			$point_per_customer[$time_key] = 1 ;
			$point_per_order[$time_key] = 1 ;
			$point_per_day[$time_key] = ($row->Price*$row->Amount);
		}
	}
	$total_Order = count($total_Order);
	$total_no_percent = $total_Sales==0 ? 0 : $total_no/($total_Sales/100);
	$total_ok_percent = $total_Sales==0 ? 0 : $total_ok/($total_Sales/100);
	$total_wait_percent = $total_Sales==0 ? 0 : $total_wait/($total_Sales/100);
	$total_recived_percent = 100-$total_no_percent-$total_ok_percent-$total_wait_percent;
}
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
	<div class="select_progress">
	    <div class="block1">
	    	<h1 style="font-size: 20px;font-weight: bold;">BÁO CÁO TỔNG QUAN</h1>
	    </div>
	    <div class="block2">
		    <div id="reportrange" class="list_div">
		        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
		        <span></span> <b class="caret"></b>
		    </div>
	    </div>
    </div>
    <div class="title_content"><span>Báo cáo doanh số</span></div>
    <div class="report_sales_fill">
    	<div class="block1">
    		<div>
	    		<select id="orderstatus" onchange="changefillter(this)">
	    			<option value="-1" <?php echo $Orderstatus==-1 ? "selected='selected'" : '' ; ?>>Tất cả tình trạng thanh toán</option>
	    			<option value="0" <?php echo $Orderstatus==0 ? "selected='selected'" : '' ; ?>>Chưa đối soát</option>
	    			<option value="1" <?php echo $Orderstatus==1 ? "selected='selected'" : '' ; ?>>Đã đối soát</option>
	    			<option value="2" <?php echo $Orderstatus==2 ? "selected='selected'" : '' ; ?>>Chờ nhận tiền</option>
	    			<option value="3" <?php echo $Orderstatus==3 ? "selected='selected'" : '' ; ?>>Đã nhận tiền</option>
	    		</select>
	    	</div>
    	</div>

    	<div class="block2 select_day_week_month">
    		<ul>
    			<li><a data='plotchart_primary' class='active'>Ngày</a></li>
    			<li><a data='plotchart_primary_week'>Tuần</a></li>
    			<li><a data='plotchart_primary_month'>Tháng</a></li>
    		</ul>
    	</div>
    </div>
    <div class="chart linechart" style="height:280px;overflow:hidden">
		<div id="plotchart_primary" class="plotchart_item_primary" style="width: 100%;height: 100%;font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
		<div id="plotchart_primary_week" class="plotchart_item_primary" style="display:none;width: 100%;height: 100%;font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
		<div id="plotchart_primary_month" class="plotchart_item_primary" style="display:none;width: 100%;height: 100%;font-size: 16px;line-height: 1.2em;font-family:tahoma"></div>
	</div>
	<div class="chart piechart">
		<div class="block1">
			<div class="list_item">
				<p class='title'>Số đơn hàng</p>
				<h3><?php echo is_array($total_Order) ? 0 : number_format($total_Order,0) ?></h3>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item1" class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>SL sản phẩm</p>
				<h3><?php echo number_format(count($products_order),0) ?></h3>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item3"  class="itemchart"></div>
				</div>
			</div>
			<div class="list_item">
				<p class='title'>Tổng doanh số</p>
				<h3><?php echo number_format($total_Sales,0) ?></h3>
				<div class="chart linechart" style="height:50px">
					<div id="plotchart_item2" class="itemchart"></div>
				</div>
			</div>
			
			<div class="list_item">
				<p class='title'>Đã đối soát</p>
				<h3><?php echo number_format($total_ok,0) ?></h3>
			</div>
			<div class="list_item">
				<p class='title'>Chờ nhận tiền</p>
				<h3><?php echo number_format($total_wait,0) ?></h3>
			</div>
			<div class="list_item">
				<p class='title'>Đã nhận tiền</p>
				<h3><?php echo number_format($total_recived,0) ?></h3>
			</div>
		</div>
		<div class="block2">
			<div class="description">
				<a><span style="background:#F00"></span> Chưa đối soát (<?php echo number_format($total_no).' - '.number_format($total_no_percent,1).'%' ?>)</a>
				<a><span style="background:#27c"></span> Đã đối soát (<?php echo number_format($total_ok).' - '.number_format($total_ok_percent,1).'%' ?>)</a>
				<a><span style="background:#ffd252"></span> Chờ thanh toán (<?php echo number_format($total_wait).' - '.number_format($total_wait_percent,1).'%' ?>)</a>
				<a><span style="background:#96ca59"></span> Đã nhận tiền (<?php echo number_format($total_recived).' - '.number_format($total_recived_percent,1).'%' ?>)</a>
				<p><?php echo date("M d,Y",strtotime($startday))." - ".date("M d,Y",strtotime($stopday)) ?></p>
			</div>
			<div>
                <canvas id="canvas_pie"></canvas>
            </div>
		</div>
	</div>
	<div class="details">
		<div class="block1">
			<p>Theo sản phẩm</p>
			<ul>
				<li><a onclick="show_table_report(this,'products_table')" class="active_menu_report">Sản phẩm</a></li>
			</ul>
			<p>Theo địa lý</p>
			<ul>
				<li><a onclick="show_table_report(this,'area_table')" class="active_menu_report">Theo vùng</a></li>
				<li><a onclick="show_table_report(this,'city_table')" class="active_menu_report">Theo thành phố</a></li>
				<li><a onclick="show_table_report(this,'branch_table')" class="active_menu_report">Theo chi nhánh</a></li>
			</ul>
			
		</div>
		<div class="block2">
			<div class="city_table table_report" style="display:none">
				<table>
					<tr>
						<th></th>
						<th>Thành phố</th>
						<th>SL đ/hàng</th>
						<th>SL s/phẩm</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$city = $this->db->query("select * from ttp_report_city")->result();
					if(count($city)>0){
						$i=1;
						$city_report_order_total = 0;
						$city_report_price_total = 0;
						foreach($city as $row){
							if(isset($city_order[$row->ID])){
								$city_report_order_total += $city_order[$row->ID]['sl'];
								$city_report_price_total += $city_order[$row->ID]['total'];
								$percent_by_city = $total_Sales==0 ? 0 : round($city_order[$row->ID]['total'] / ($total_Sales/100),2);
								echo '<tr>
										<td>'.$i.'</td>
										<td>'.$row->Title.'</td>
										<td>'.count($city_order[$row->ID]['order']).'</td>
										<td>'.$city_order[$row->ID]['sl'].'</td>
										<td>'.number_format($city_order[$row->ID]['total']).'</td>
										<td>'.$percent_by_city.'%</td>
									</tr>';
								$i++;
							}
						}
						echo '<tr>
										<td style="color:#FFF">1</td>
										<td>Tổng cộng</td>
										<td style="color:#FFF">1</td>
										<td>'.number_format($city_report_order_total).'</td>
										<td>'.number_format($city_report_price_total).'</td>
										<td>100%</td>
									</tr>';
					}
					?>
				</table>
			</div>
			<div class="area_table table_report">
				<table>
					<tr>
						<th></th>
						<th>Vùng</th>
						<th>SL đ/hàng</th>
						<th>SL s/phẩm</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$area = $this->db->query("select * from ttp_report_area")->result();
					$area_report_order_total = 0;
					$area_report_price_total = 0;
					if(count($area)>0){
						$i=1;
						foreach($area as $row){
							if(isset($area_order[$row->ID])){
								$area_report_order_total += $area_order[$row->ID]['sl'];
								$area_report_price_total += $area_order[$row->ID]['total'];
								$percent_by_area = round($area_order[$row->ID]['total'] / ($total_Sales/100),2);
								echo '<tr>
										<td>'.$i.'</td>
										<td>'.$row->Title.'</td>
										<td>'.count($area_order[$row->ID]['order']).'</td>
										<td>'.$area_order[$row->ID]['sl'].'</td>
										<td>'.number_format($area_order[$row->ID]['total']).'</td>
										<td>'.$percent_by_area.'%</td>
									</tr>';
								$i++;
							}
						}
						echo '<tr>
										<td style="color:#FFF">1</td>
										<td>Tổng cộng</td>
										<td style="color:#FFF">1</td>
										<td>'.$area_report_order_total.'</td>
										<td>'.number_format($area_report_price_total).'</td>
										<td>100%</td>
									</tr>';
					}
					?>
				</table>
			</div>
			<div class="branch_table table_report">
				<table>
					<tr>
						<th></th>
						<th>Chi nhánh</th>
						<th>SL đ/hàng</th>
						<th>SL s/phẩm</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					$branch = $this->db->query("select * from ttp_report_branch where SupplierID in(".implode(',',$supplier_list).")")->result();
					$branch_report_order_total = 0;
					$branch_report_price_total = 0;
					if(count($area)>0){
						$i=1;
						foreach($branch as $row){
							if(isset($branch_order[$row->ID])){
								$branch_report_order_total += $branch_order[$row->ID]['sl'];
								$branch_report_price_total += $branch_order[$row->ID]['total'];
								$percent_by_branch = round($branch_order[$row->ID]['total'] / ($total_Sales/100),2);
								echo '<tr>
										<td>'.$i.'</td>
										<td>'.$row->Title.'</td>
										<td>'.count($branch_order[$row->ID]['order']).'</td>
										<td>'.$branch_order[$row->ID]['sl'].'</td>
										<td>'.number_format($branch_order[$row->ID]['total']).'</td>
										<td>'.$percent_by_branch.'%</td>
									</tr>';
								$i++;
							}
						}
						echo '<tr>
										<td style="color:#FFF">1</td>
										<td>Tổng cộng</td>
										<td style="color:#FFF">1</td>
										<td>'.$branch_report_order_total.'</td>
										<td>'.number_format($branch_report_price_total).'</td>
										<td>100%</td>
									</tr>';
					}
					?>
				</table>
			</div>
			<div class="products_table table_report" style="display:block">
				<table>
					<tr>
						<th></th>
						<th>Sản phẩm</th>
						<th>SL đ.hàng</th>
						<th>SL s.phẩm</th>
						<th>Doanh số</th>
						<th>Tỷ trọng</th>
					</tr>
					<?php 
					
					if(count($products_order)>0){
						$i=1;
						$products_report_amount_total = 0;
						$products_report_price_total = 0;
						foreach($products_order as $row){
							$products_report_amount_total += $row['Total'];
							$products_report_price_total += $row['Price'];
							$percent_by_products = round($row['Price'] / ($total_Sales/100),2);
							echo '<tr>
									<td>'.$i.'</td>
									<td>'.$row['Title'].'</td>
									<td>'.number_format(count($row['Order'])).'</td>
									<td>'.number_format($row['Total']).'</td>
									<td>'.number_format($row['Price']).'</td>
									<td>'.$percent_by_products.'%</td>
								</tr>';
							$i++;
						}
						echo '<tr>
									<td style="color:#FFF">1</td>
									<td>Tổng cộng</td>
									<td style="color:#FFF">1</td>
									<td>'.number_format($products_report_amount_total).'</td>
									<td>'.number_format($products_report_price_total).'</td>
									<td>100%</td>
								</tr>';
					}
					?>
				</table>
			</div>
		</div>
	</div>
	<div class="over_lay"></div>
	<input type='hidden' id="baselink_report" value="<?php echo $base_link ?>" />
</div>

<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {
    	var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
		*******************************
		*	Filter by datepicker	  *
		*							  *
		*******************************
		*/
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
		    $(".over_lay").html("");
            $(".over_lay").removeClass("black");
            $(".over_lay").fadeIn();
		    var startday = picker.startDate.format('DD/MM/YYYY');
		    var stopday = picker.endDate.format('DD/MM/YYYY');
	    	var baselink = $("#baselink_report").val();
			$.ajax({
	            url: baselink+"set_day",
	            dataType: "html",
	            type: "POST",
	            data: "group1="+startday+" - "+stopday,
	            success: function(result){
	                location.reload();
	            }
	        });	
		});
    });
</script>
<!-- /datepicker -->

<!-- flot -->
<?php 
$arrpoint 				= array();
$showday 				= array();
$total_day 				= array();
$total_day_week 		= array();
$total_day_month 		= array();
$arrpoint_per_order 	= array();
$arrpoint_per_customer 	= array();
$arrpoint_per_new 		= array();
$arrpoint_per_products 	= array();
$arrpoint_per_abs 		= array();

$arrpoint_week 			= array();
$showday_week 			= array();

$arrpoint_month 		= array();
$showday_month 			= array();

$arr_month_total		= array();



$reset=0;
$reset_month=0;
$temp_count = 1;
$day_cache = date('d/m/Y',strtotime($startday));

$arr_point_temp_vs = array();
for($i=strtotime($startday); $i < strtotime($stopday)+1; $i=$i+3600*24) { 
	$temp_point_second = $temp_count-1;
	$strday = str_replace('-','',date('Y-m-d',$i));
	$arrpoint[] = isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	$arrpoint_per_order[] = isset($point_per_order[$strday]) ? $point_per_order[$strday] : 0 ;
	$arrpoint_per_customer[] = isset($point_per_customer[$strday]) ? $point_per_customer[$strday] : 0 ;
	$arrpoint_per_new[] = isset($point_per_new[$strday]) ? $point_per_new[$strday] : 0 ;
	if(isset($point_products_per_order[$strday]) && isset($point_products_per_order[$strday])){
		$arrpoint_per_products[] = $point_products_per_order[$strday]['Total']==0 ? 0 : round($point_products_per_order[$strday]['Amount']/$point_products_per_order[$strday]['Total'],1);
	}else{
		$arrpoint_per_products[] = 0;
	}
	if(isset($point_per_day[$strday]) && isset($point_per_order[$strday])){
		$arrpoint_per_abs[] = $point_per_order[$strday]==0 ? 0 : round($point_per_day[$strday]/$point_per_order[$strday],1);
	}else{
		$arrpoint_per_abs[] = 0;
	}
	$temp_value = $i*1000;
	$total_day[] = $temp_value;
	$show_day[] = $temp_value.":'".date('d/m/Y',$i)."'";
	$arr_point_temp_vs[] = $temp_value;

	/* 
	********************************
	*	Value of Week on chart     *
	*							   *
	********************************
	*/

	if($temp_count%7==1){
		$reset = $reset+1;
		$total_day_week[] = $i*1000;
		$k = $i+60*60*24*6;
		if(strtotime($stopday)<$k){
			$k = strtotime($stopday);
		}
		$showday_week[] = $temp_value.":' $day_cache - ".date('d/m/Y',$k)."'";	
		$day_cache = date('d/m/Y',$k+3600*24);
	}
	if(isset($arrpoint_week[$reset])){
		$arrpoint_week[$reset] += isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}else{
		$arrpoint_week[$reset] = isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}

	/* 
	********************************
	*	Value of Month on chart    *
	*							   *
	********************************
	*/
	$month = date('mY',$i);
	if(!isset($arr_month_total[$month])){
		$total_day_month[] = $i*1000;
		$j = $i+60*60*24*30;
		if(strtotime($stopday)<$j){
			$j = strtotime($stopday);
		}
		$showday_month[] = $temp_value.":' Tháng ".date('m/Y',$i)."'";
		$arr_month_total[$month] = "";
		$reset_month = $reset_month+1;
	}
	
	if(isset($arrpoint_month[$reset_month])){
		$arrpoint_month[$reset_month] += isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}else{
		$arrpoint_month[$reset_month] = isset($point_per_day[$strday]) ? $point_per_day[$strday] : 0 ;
	}

	$temp_count++;
}
/*
*	check if only 1 point
*/

if($reset_month<=1){
	$l = strtotime($stopday)*1000;
	$m = strtotime($startday)*1000;
	if($reset_month==1){
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
		$arrpoint_month[] = $arrpoint_month[1];
	}else{
		$total_day_month[] = $m;
		$arrpoint_month[1] = isset($arrpoint_month[0]) ? $arrpoint_month[0] : 0 ;
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
	}
	$reset_month=1;
}


/*
*	check if only 1 point
*/

if($reset_month<=1){
	$l = strtotime($stopday)*1000;
	$m = strtotime($startday)*1000;
	if($reset_month==1){
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
		$arrpoint_month[] = $arrpoint_month[1];
	}else{
		$total_day_month[] = $m;
		$arrpoint_month[1] = isset($arrpoint_month[0]) ? $arrpoint_month[0] : 0 ;
		$showday_month[0] = "$m:'".date('d/m/Y',strtotime($startday)).' - '.date('d/m/Y',strtotime($stopday))."'";
	}
	$reset_month=1;
}

/*
***************************
*	Tick date 			  *
*						  *
***************************
*/

$tickday = "1,'day'";
$tickweek = "1,'day'";
$tickmonth = "1,'month'";
if(count($total_day)>30 && count($total_day)<60){
	$tickday = "5,'day'";
	$tickweek = "5,'day'";
	$tickmonth = "10,'day'";
}elseif (count($total_day)>=60 && count($total_day)<120) {
	$tickday = "10,'day'";
	$tickweek = "10,'day'";
	$tickmonth = "1,'month'";
}elseif (count($total_day)>=120 && count($total_day)<240) {
	$tickday = "20,'day'";
	$tickweek = "20,'day'";
	$tickmonth = "1,'month'";
}elseif (count($total_day)>=240) {
	$tickday = "30,'day'";
	$tickweek = "30,'day'";
	$tickmonth = "1,'month'";
}elseif(count($total_day)<6){
	$tickweek = "1,'day'";
	$tickmonth = "1,'day'";
}elseif (count($total_day)>=6 && count($total_day)<=30) {
	$tickweek = "1,'day'";
	$tickmonth = "1,'day'";
}

?>
<script type="text/javascript">
    var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];
    var pointvalue = [<?php echo implode(',',$arrpoint); ?>];
    var pointday = [<?php echo implode(',',$total_day); ?>];
    var showday = {<?php echo implode(',',$show_day); ?>};
    var pointperorder = [<?php echo implode(',',$arrpoint_per_order); ?>];
    var pointpercustomer = [<?php echo implode(',',$arrpoint_per_customer); ?>];
    var pointpernew = [<?php echo implode(',',$arrpoint_per_new); ?>];
    var pointperproducts = [<?php echo implode(',',$arrpoint_per_products); ?>];
    var pointperabs = [<?php echo implode(',',$arrpoint_per_abs); ?>];

    var pointvalue_week = [<?php echo implode(',',$arrpoint_week) ?>];
    var pointday_week = [<?php echo implode(',',$total_day_week); ?>];
    var showday_week = {<?php echo implode(',',$showday_week); ?>};

    var pointvalue_month = [<?php echo implode(',',$arrpoint_month) ?>];
    var pointday_month = [<?php echo implode(',',$total_day_month); ?>];
    var showday_month = {<?php echo implode(',',$showday_month); ?>};
	

    $(function () {
        var d1 = [];
        var d1_vs = [];
        var d2 = [];
        var d2_vs = [];
        var d3 = [];
        var d3_vs = [];
        var d4 = [];
        var d4_vs = [];
        var d5 = [];
        var d5_vs = [];
        var d6 = [];
        var d6_vs = [];
        var d1_week = [];
        var d1_week_vs = [];
        var d1_month = [];
        var d1_month_vs = [];
        for (var i = 0; i < <?php echo count($arrpoint) ?>; i++) {
            d1.push([pointday[i], pointvalue[i]]);
            d2.push([pointday[i], pointperorder[i]]);
            d3.push([pointday[i], pointpercustomer[i]]);
            d4.push([pointday[i], pointpernew[i]]);
            d5.push([pointday[i], pointperproducts[i]]);
            d6.push([pointday[i], pointperabs[i]]);
        }

        for (var i = 0; i < <?php echo $reset ?>; i++) {
            d1_week.push([pointday_week[i], pointvalue_week[i]]);
        }

        for (var i = 0; i < <?php echo $reset_month ?>; i++) {
            d1_month.push([pointday_month[i], pointvalue_month[i]]);
        }

        var chartMinDate = d1[0][0];
        var chartMaxDate = d1[<?php echo count($arrpoint) ?>-1][0];
        var chartMinDate_week = d1_week[0][0];
        var chartMaxDate_week = d1_week[<?php echo $reset ?>-1][0];
        var chartMinDate_month = d1_month[0][0];
        var chartMaxDate_month = d1_month[<?php echo $reset_month ?>-1][0];

        var tickSize = [<?php echo $tickday ?>];
        var tickSize_week = [<?php echo $tickweek ?>];
        var tickSize_month = [<?php echo $tickmonth ?>];
        var tformat = "%d/%m/%y";

        var options = {
        	grid: {
                show: true,aboveData: true,color: "#3f3f3f",labelMargin: 10,axisMargin: 0,borderWidth: 0,borderColor: null,minBorderMargin: 5,clickable: true,hoverable: true,autoHighlight: true,mouseActiveRadius: 20
            },
            series: {
                lines: {
                	show: true,fill: true,lineWidth: 2,steps: false
                },
                points: {
                    show: true,radius: 2.5,symbol: "circle",lineWidth: 2.0
                }
            },
            legend: {
                position: "ne",margin: [0, -25],noColumns: 0,labelBoxBorderColor: null,labelFormatter: function (label, series) {return label + '&nbsp;&nbsp;';},width: 40,height: 1
            },
            colors: chartColours,shadowSize: 0,tooltip: true,
            tooltipOpts: {
                content: "%s: %y.0",xDateFormat: "%d/%m",shifts: {x: -30,y: -50},defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",minTickSize: tickSize,timeformat: tformat,min: chartMinDate,max: chartMaxDate
            }
        };

        var options_week = {
        	grid: {
                show: true,aboveData: true,color: "#3f3f3f",labelMargin: 10,axisMargin: 0,borderWidth: 0,borderColor: null,minBorderMargin: 5,clickable: true,hoverable: true,autoHighlight: true,mouseActiveRadius: 20
            },
            series: {
                lines: {
                	show: true,fill: true,lineWidth: 2,steps: false
                },
                points: {
                    show: true,radius: 3.5,symbol: "circle",lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",margin: [0, -25],noColumns: 0,labelBoxBorderColor: null,labelFormatter: function (label, series) {return label + '&nbsp;&nbsp;';},width: 40,height: 1
            },
            colors: chartColours,shadowSize: 0,tooltip: true,
            tooltipOpts: {
                content: "%s: %y.0",xDateFormat: "%d/%m",shifts: {x: -30,y: -50},defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",minTickSize: tickSize_week,timeformat: tformat,min: chartMinDate_week,max: chartMaxDate_week
            }
        };

        var options_month = {
        	grid: {
                show: true,aboveData: true,color: "#3f3f3f",labelMargin: 10,axisMargin: 0,borderWidth: 0,borderColor: null,minBorderMargin: 5,clickable: true,hoverable: true,autoHighlight: true,mouseActiveRadius: 20
            },
            series: {
                lines: {
                	show: true,fill: true,lineWidth: 2,steps: false
                },
                points: {
                    show: true,radius: 3.5,symbol: "circle",lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",margin: [0, -25],noColumns: 0,labelBoxBorderColor: null,labelFormatter: function (label, series) {return label + '&nbsp;&nbsp;';},width: 40,height: 1
            },
            colors: chartColours,shadowSize: 0,tooltip: true,
            tooltipOpts: {
                content: "%s: %y.0",xDateFormat: "%d/%m",shifts: {x: -30,y: -50},defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",minTickSize: tickSize_month,timeformat: tformat,min: chartMinDate_month,max: chartMaxDate_month
            }
        };
        var plot = $.plot($("#plotchart_primary"), 
        	[
        		{label: "Doanh số",data: d1,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"},type:"showday"}
        	], options);
        var plot = $.plot($("#plotchart_primary_week"), [{label: "Doanh số",data: d1_week,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], options_week);
        var plot = $.plot($("#plotchart_primary_month"), [{label: "Doanh số",data: d1_month,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], options_month);
		
		var optionsitem = {
            grid: {
                show: false
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 1,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 1,
                    symbol: "circle",
                    lineWidth: 1
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function (label, series) {
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: false,
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };

        var plot1 = $.plot($("#plotchart_item1"), [{data: d2,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot2 = $.plot($("#plotchart_item2"), [{data: d3,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
        var plot3 = $.plot($("#plotchart_item3"), [{data: d1,lines: {fillColor: "rgba(150, 202, 89, 0.12)"},points: {fillColor: "#fff"}}], optionsitem);
    });
	
	$("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "5px 10px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");

	$("#plotchart_primary").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			var width = window.innerWidth;
			if(width-item.pageX<250){
				item.pageX = item.pageX-250;
			}
			var type = item.series.type;
			if(type=="showday"){
				$("#tooltip").html("<b>Ngày " + showday[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ ")
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(300);
			}
			if(type=="showday_vs"){
				$("#tooltip").html("<b>Ngày " + showday_vs[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ ")
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(300);
			}
		} else {
			$("#tooltip").hide();
		}
	});

	$("#plotchart_primary_week").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			var width = window.innerWidth;
			if(width-item.pageX<250){
				item.pageX = item.pageX-250;
			}
			$("#tooltip").html("<b>Ngày " + showday_week[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ")
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(300);
		} else {
			$("#tooltip").hide();
		}
	});

	$("#plotchart_primary_month").bind("plothover", function (event, pos, item) {
		if (item) {
			var x = item.datapoint[0],
				y = item.datapoint[1].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
			var width = window.innerWidth;
			if(width-item.pageX<250){
				item.pageX = item.pageX-250;
			}
			$("#tooltip").html("<b>" + showday_month[x] + "</b><br>"+item.series.label +" đạt được "+  y +" đ")
				.css({top: item.pageY+5, left: item.pageX+5})
				.fadeIn(300);
		} else {
			$("#tooltip").hide();
		}
	});

	
</script>
<!-- /flot -->
<!-- /pie chart-->
<script>
	var sharePiePolorDoughnutData = [
            {
                value: <?php echo $total_no ?>,
                color: "#f00",
                highlight: "#f00",
                label: "Giá trị chưa đối soát"
        },
            {
                value: <?php echo $total_ok ?>,
                color: "#27c",
                highlight: "#27c",
                label: "Giá trị đã đối soát"
        },
            {
                value: <?php echo $total_wait ?>,
                color: "#ffd252",
                highlight: "#ffd252",
                label: "Giá trị chờ nhận tiền"
        },
            {
                value: <?php echo $total_recived ?>,
                color: "#96ca59",
                highlight: "#96ca59",
                label: "Giá trị đã nhận tiền"
        }
    ];

        $(document).ready(function () {
            window.myPie = new Chart(document.getElementById("canvas_pie").getContext("2d")).Pie(sharePiePolorDoughnutData, {
                responsive: true,
                tooltipFillColor: "rgba(51, 51, 51, 0.55)"
            });
        });
</script>
<!-- ajax compare -->
<script>
	var baselink = $("#baselink_report").val();
	$("#vs_sosanh").change(function(){
		var data = $(this).val();
		var kpi = $("#value_sosanh").val();
		var status = $("#orderstatus").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+data+"&KPI="+kpi+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	$("#orderstatus").change(function(){
		var data = $(this).val();
		var kpi = $("#value_sosanh").val();
		var targets = $("#vs_sosanh").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+targets+"&KPI="+kpi+"&Orderstatus="+data,
            success: function(result){
                window.location=result;
            }
        });
	});

	$("#value_sosanh").change(function(){
		var data = $(this).val();
		var status = $("#orderstatus").val();
		var targets = $("#vs_sosanh").val();
		$.ajax({
            url: baselink+"report_sales/set_session_compare",
            dataType: "html",
            type: "POST",
            data: "Target="+targets+"&KPI="+data+"&Orderstatus="+status,
            success: function(result){
                window.location=result;
            }
        });
	});

	function changefillter(ob){
        var payment = $(ob).val();
        window.location = "<?php echo $base_link ?>supplier_report_overview?payment="+payment;
    }
</script>
<style>
	.body_content .containner .piechart .block2 .description a {display:block;line-height: 24px;}
	.body_content .containner .details .block2 table tr:last-child td{padding: 15px 5px;font-weight:bold;}
</style>