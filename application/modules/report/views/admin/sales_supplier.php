<?php 
$segment = $this->uri->segment(4);
$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$payment_ok = $this->db->query("select * from ttp_report_payment_supplier where ProductsID=$ProductsID and SupplierID=$SupplierID")->result();
$arr_payment_ok = array();
if(count($payment_ok)>0){
    foreach($payment_ok as $row){
        $arr_payment_ok[$row->ProductsID][$row->ExportID][$row->SupplierID] = $row->Note;
    }
}

$arr_shipment = array();
$arr_shipment_title = array();
$shipment = $this->db->query("select DISTINCT c.ProductsID,c.ShipmentID,c.PriceCurrency,c.ValueCurrency,c.VAT,b.ShipmentCode from ttp_report_inventory_import a,ttp_report_perchaseorder_details c,ttp_report_shipment b where b.ID=c.ShipmentID and a.POID=c.POID and a.ProductionID=$SupplierID")->result();
if(count($shipment)>0){
    foreach($shipment as $row){
        $arr_shipment[$row->ProductsID][$row->ShipmentID] = ($row->PriceCurrency*$row->ValueCurrency) + ((($row->PriceCurrency*$row->ValueCurrency)/100)*$row->VAT);
        $arr_shipment_title[$row->ProductsID][$row->ShipmentID] = $row->ShipmentCode;
    }
}
$str = $TrademarkID!=0 ? " and a.TrademarkID=$TrademarkID" : "" ;
$str = $ProductsID!=0 ? " and a.ID=$ProductsID" : "" ;
$str_combo = $ProductsID!=0 ? " and a.ID=$ProductsID" : "" ;
$arr_products = array();
$products = $this->db->query("select a.MaSP,a.ID,a.Title,a.Donvi,d.MaXK,d.ID as ExportID,c.Total,c.Amount,c.Price,c.ShipmentID from ttp_report_products a,ttp_report_order b,ttp_report_orderdetails c,ttp_report_export_warehouse d where b.ID=d.OrderID and a.ID=c.ProductsID and b.ID=c.OrderID and date(d.DateTransferMoney)>='".date('Y-m-d',strtotime($startday))."' and date(d.DateTransferMoney)<='".date('Y-m-d',strtotime($stopday))."' and b.Status=$OrderType and a.VariantType in(0,1) and b.CustomerID!=9996 and b.KhoID=$WarehouseID $str")->result();
if(count($products)>0){
    foreach($products as $row){
        if($PaymentStatus==0){
            if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                if(!isset($arr_products[$row->ID])){
                    $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                    $arr_products[$row->ID]['Title'] = $row->Title;
                    $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                    $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                    $arr_products[$row->ID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                    $arr_products[$row->ID]['List'] = array(
                        0 => array(
                            'MaXK'  => $row->MaXK,
                            'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                            'Amount'=> $row->Amount,
                            'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                            'Type'  => 0,
                            'Title' => $row->Title,
                            'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                            'ExportID'=>$row->ExportID
                        )
                    );
                }else{
                    $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                    $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                    $arr_products[$row->ID]['List'][] = array(
                        'MaXK'  => $row->MaXK,
                        'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                        'Amount'=> $row->Amount,
                        'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                        'Type'  => 0,
                        'Title' => $row->Title,
                        'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                        'ExportID'=>$row->ExportID
                    );
                }
            }
        }
        $temp_payment = isset($arr_payment_ok[$row->ID][$row->ExportID][$SupplierID]) ? $arr_payment_ok[$row->ID][$row->ExportID][$SupplierID] : '' ;
        if($PaymentStatus==1){
            if($temp_payment!=''){
                if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                    if(!isset($arr_products[$row->ID])){
                        $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                        $arr_products[$row->ID]['Title'] = $row->Title;
                        $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                        $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                        $arr_products[$row->ID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                        $arr_products[$row->ID]['List'] = array(
                            0 => array(
                                'MaXK'  => $row->MaXK,
                                'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                'Amount'=> $row->Amount,
                                'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                'Type'  => 0,
                                'Title' => $row->Title,
                                'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                'ExportID'=>$row->ExportID
                            )
                        );
                    }else{
                        $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                        $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                        $arr_products[$row->ID]['List'][] = array(
                            'MaXK'  => $row->MaXK,
                            'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                            'Amount'=> $row->Amount,
                            'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                            'Type'  => 0,
                            'Title' => $row->Title,
                            'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                            'ExportID'=>$row->ExportID
                        );
                    }
                }
            }
        }

        if($PaymentStatus==2){
            if($temp_payment==''){
                if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                    if(!isset($arr_products[$row->ID])){
                        $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                        $arr_products[$row->ID]['Title'] = $row->Title;
                        $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                        $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                        $arr_products[$row->ID]['Total'] = $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount;
                        $arr_products[$row->ID]['List'] = array(
                            0 => array(
                                'MaXK'  => $row->MaXK,
                                'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                'Amount'=> $row->Amount,
                                'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                'Type'  => 0,
                                'Title' => $row->Title,
                                'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                'ExportID'=>$row->ExportID
                            )
                        );
                    }else{
                        $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                        $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount);
                        $arr_products[$row->ID]['List'][] = array(
                            'MaXK'  => $row->MaXK,
                            'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                            'Amount'=> $row->Amount,
                            'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                            'Type'  => 0,
                            'Title' => $row->Title,
                            'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                            'ExportID'=>$row->ExportID
                        );
                    }
                }
            }
        }

    }
}

$products = $this->db->query("select a.MaSP,a.ID,a.Title,f.Title as Combo,a.Donvi,d.MaXK,d.ID as ExportID,c.Amount,c.Price,c.ShipmentID from ttp_report_products a,ttp_report_order b,ttp_report_orderdetails_bundle c,ttp_report_export_warehouse d,ttp_report_orderdetails e,ttp_report_products f where e.OrderID=b.ID and e.ID=c.DetailsID and e.ProductsID=f.ID and b.ID=d.OrderID and a.ID=c.ProductsID and date(d.DateTransferMoney)>='".date('Y-m-d',strtotime($startday))."' and date(d.DateTransferMoney)<='".date('Y-m-d',strtotime($stopday))."' and b.Status=$OrderType and a.VariantType in(0,1) and b.CustomerID!=9996 and b.KhoID=$WarehouseID $str_combo")->result();
if(count($products)>0){
    foreach($products as $row){
        $temp_payment = isset($arr_payment_ok[$row->ID][$row->ExportID][$SupplierID]) ? $arr_payment_ok[$row->ID][$row->ExportID][$SupplierID] : '' ;
        if($PaymentStatus==0){
            if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                if(!isset($arr_products[$row->ID])){
                    $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                    $arr_products[$row->ID]['Title'] = $row->Title;
                    $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                    $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                    $arr_products[$row->ID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                    $arr_products[$row->ID]['List'] = array(
                        0 => array(
                            'MaXK'  => $row->MaXK,
                            'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                            'Amount'=> $row->Amount,
                            'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                            'Type'  => 1,
                            'Title' => $row->Combo,
                            'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                            'ExportID'=>$row->ExportID
                        )
                    );
                }else{
                    $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                    $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                    $arr_products[$row->ID]['List'][] = array(
                        'MaXK'  => $row->MaXK,
                        'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                        'Amount'=> $row->Amount,
                        'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                        'Type'  => 1,
                        'Title' => $row->Combo,
                        'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                        'ExportID'=>$row->ExportID
                    );
                }
            }
        }

        if($PaymentStatus==1){
            if($temp_payment!=''){
                if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                    if(!isset($arr_products[$row->ID])){
                        $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                        $arr_products[$row->ID]['Title'] = $row->Title;
                        $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                        $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                        $arr_products[$row->ID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                        $arr_products[$row->ID]['List'] = array(
                            0 => array(
                                'MaXK'  => $row->MaXK,
                                'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                'Amount'=> $row->Amount,
                                'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                'Type'  => 1,
                                'Title' => $row->Combo,
                                'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                'ExportID'=>$row->ExportID
                            )
                        );
                    }else{
                        $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                        $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                        $arr_products[$row->ID]['List'][] = array(
                            'MaXK'  => $row->MaXK,
                            'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                            'Amount'=> $row->Amount,
                            'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                            'Type'  => 1,
                            'Title' => $row->Combo,
                            'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                            'ExportID'=>$row->ExportID
                        );
                    }
                }
            }
        }

        if($PaymentStatus==2){
            if($temp_payment==''){
                if(isset($arr_shipment[$row->ID][$row->ShipmentID])){
                    if(!isset($arr_products[$row->ID])){
                        $arr_products[$row->ID]['MaSP'] = $row->MaSP;
                        $arr_products[$row->ID]['Title'] = $row->Title;
                        $arr_products[$row->ID]['Donvi'] = $row->Donvi;
                        $arr_products[$row->ID]['Totalamount'] = $row->Amount;
                        $arr_products[$row->ID]['Total'] = $row->Amount*$arr_shipment[$row->ID][$row->ShipmentID];
                        $arr_products[$row->ID]['List'] = array(
                            0 => array(
                                'MaXK'  => $row->MaXK,
                                'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                                'Amount'=> $row->Amount,
                                'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                                'Type'  => 1,
                                'Title' => $row->Combo,
                                'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                                'ExportID'=>$row->ExportID
                            )
                        );
                    }else{
                        $arr_products[$row->ID]['Totalamount'] = $arr_products[$row->ID]['Totalamount']+$row->Amount;
                        $arr_products[$row->ID]['Total'] = $arr_products[$row->ID]['Total']+($row->Amount*$arr_shipment[$row->ID][$row->ShipmentID]);
                        $arr_products[$row->ID]['List'][] = array(
                            'MaXK'  => $row->MaXK,
                            'Price' => $arr_shipment[$row->ID][$row->ShipmentID],
                            'Amount'=> $row->Amount,
                            'Total' => $arr_shipment[$row->ID][$row->ShipmentID]*$row->Amount,
                            'Type'  => 1,
                            'Title' => $row->Combo,
                            'ShipmentCode' => $arr_shipment_title[$row->ID][$row->ShipmentID],
                            'ExportID'=>$row->ExportID
                        );
                    }
                }
            }
        }

    }
}
?>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1><a style="<?php echo $segment=="report_supplier" ? "color:#f96868" : "" ; ?>" href="<?php echo base_url().ADMINPATH.'/report/report_sales/report_supplier' ?>">BÁO CÁO KINH DOANH NCC</a> | <a style="<?php echo $segment=="report_supplier_payment" ? "color:#f96868" : "" ; ?>" href="<?php echo base_url().ADMINPATH.'/report/report_sales/report_supplier_payment' ?>">BÁO CÁO CÔNG NỢ NCC</a></h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Thời gian báo cáo <span class='pull-right'>:</span> </label>
        <label class="col-xs-4"><?php echo "Từ <b style='margin-right:10px;margin-left:10px'>".date('d/m/Y',strtotime($startday))."</b> đến <b style='margin-left:10px'>".date('d/m/Y',strtotime($stopday)); ?></b></label>
        <div class="col-xs-6 text-right">
            <?php 
            $querystr = "?".http_build_query($_GET);
            ?>
            <a href="<?php echo base_url().ADMINPATH.'/report/report_sales/export_report_supplier'.$querystr ?>" class="btn btn-primary"><i class="fa fa-download"></i> Export excel</a>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Loại đơn hàng <span class='pull-right'>:</span></label>
        <div class="col-xs-4">
            <select name="OrderType" class="form-control" id="OrderType" onchange="changefillter()">
                <option value="0" <?php echo $OrderType==0 ? "selected='selected'" : "" ; ?>>Đơn hàng thành công</option>
                <option value="1" <?php echo $OrderType==1 ? "selected='selected'" : "" ; ?>>Đơn hàng hủy</option>
                <option value="7" <?php echo $OrderType==7 ? "selected='selected'" : "" ; ?>>Đơn hàng đang vận chuyển</option>
                <option value="9" <?php echo $OrderType==9 ? "selected='selected'" : "" ; ?>>Đơn hàng chờ vận chuyển</option>
            </select>
        </div>
        <label class="control-label col-xs-2 text-right">Trạng thái thanh toán : </label>
        <div class="col-xs-4">
            <select name="PaymentStatus" class="form-control" id="PaymentStatus" onchange="changefillter()">
                <option value="0" <?php echo $PaymentStatus==0 ? "selected='selected'" : "" ; ?>>Tất cả</option>
                <option value="1" <?php echo $PaymentStatus==1 ? "selected='selected'" : "" ; ?>>Đơn hàng đã thanh toán</option>
                <option value="2" <?php echo $PaymentStatus==2 ? "selected='selected'" : "" ; ?>>Đơn hàng chưa thanh toán</option>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Kho bán hàng <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="WarehouseID" onchange="changefillter()">
                <option value="0">-- Chọn kho bán hàng --</option>
                <?php 
                $warehouse = $this->db->query("select ID,MaKho from ttp_report_warehouse")->result();
                if(count($warehouse)>0){
                    foreach($warehouse as $row){
                        $selected = $row->ID==$WarehouseID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->MaKho</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label class="control-label col-xs-2 text-right">Tên nhà cung cấp : </label>
        <div class="col-xs-4">
            <select class="form-control" id="SupplierID" onchange="changefillter()">
                <option value="0">-- Chọn nhà cung cấp --</option>
                <?php 
                $supplier = $this->db->query("select ID,Title from ttp_report_production order by Title ASC")->result();
                if(count($supplier)>0){
                    foreach($supplier as $row){
                        $selected = $row->ID==$SupplierID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row">
        <label class="control-label col-xs-2">Thương hiệu <span class='pull-right'>:</span> </label>
        <div class="col-xs-4">
            <select class="form-control" id="TrademarkID" onchange="changefillter()">
                <option value="0">-- Tất cả thương hiệu --</option>
                <?php 
                if($SupplierID>0){
                    $trademark = $this->db->query("select DISTINCT a.ID,a.Title from ttp_report_trademark a,ttp_report_inventory_import b,ttp_report_inventory_import_details c,ttp_report_products d where a.ID=d.TrademarkID and d.ID=c.ProductsID and c.ImportID=b.ID and b.ProductionID=$SupplierID and b.KhoID=$WarehouseID order by a.Title ASC")->result();
                }else{
                    $trademark = $this->db->query("select ID,Title from ttp_report_trademark order by Title ASC")->result();
                }
                if(count($trademark)>0){
                    foreach($trademark as $row){
                        $selected = $row->ID==$TrademarkID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>$row->Title</option>";
                    }
                }
                ?>
            </select>
        </div>
        <label class="control-label col-xs-2 text-right">Loại sản phẩm : </label>
        <div class="col-xs-4">
            <select class="form-control" id="ProductsID" onchange="changefillter()">
                <option value="0">-- Tất cả sản phẩm --</option>
                <?php 
                if($SupplierID>0){
                    if($TrademarkID>0){
                        $productslist = $this->db->query("select DISTINCT d.ID,d.Title from ttp_report_inventory_import b,ttp_report_inventory_import_details c,ttp_report_products d where d.ID=c.ProductsID and c.ImportID=b.ID and b.ProductionID=$SupplierID and b.KhoID=$WarehouseID and d.TrademarkID=$TrademarkID order by d.Title ASC")->result();
                    }else{
                        $productslist = $this->db->query("select DISTINCT d.ID,d.Title from ttp_report_inventory_import b,ttp_report_inventory_import_details c,ttp_report_products d where d.ID=c.ProductsID and c.ImportID=b.ID and b.ProductionID=$SupplierID and b.KhoID=$WarehouseID order by d.Title ASC")->result();
                    }
                }else{
                    $productslist = (object)array();
                }
                if(count($productslist)>0){
                    foreach($productslist as $row){
                        $selected = $row->ID==$ProductsID ? "selected='selected'" : "" ;
                        echo "<option value='$row->ID' $selected>".$row->Title."</option>";
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin: 0px;">
        <table class="table table-bordered">
            <tr>
                <th>STT</th>
                <th>Mã SP</th>
                <th style="width:200px">Tên SP </th>
                <th>Đơn vị</th>
                <th>Tổng số lượng bán</th>
                <th>Tổng tiền thanh toán<br>(Gồm VAT)</th>
                <th style="width:120px">Số phiếu xuất kho bán hàng</th>
                <th style="width:50px">Loại</th>
                <th>Đơn giá <br>(Chưa VAT)</th>
                <th>Đơn giá <br>(Gồm VAT)</th>
                <th>Số lượng</th>
                <th>Thành tiền thanh toán <br>(Gồm VAT)</th>
                <th>Lô hàng</th>
                <?php echo $ProductsID>0 && $OrderType==0 ? "<th class='text-center' style='vertical-align:middle'><input type='checkbox' onchange='showbutton(this)' /><br>Mở thao tác thanh toán</th>" : "" ; ?>
            </tr>
            <?php 
            $arr_type = array(0=>"Lẻ",1=>"Combo");
            if(count($arr_products)>0){
                $i=1;
                $TotalAmount = 0;
                $TotalPrice = 0;
                foreach($arr_products as $key=>$row){
                    $rowspan = count($row['List'])+1;
                    $TotalAmount += $row['Totalamount'];
                    $temp = number_format($row['Total']);
                    $temp = str_replace(',','',$temp);
                    $TotalPrice = $TotalPrice+(int)$temp;
                    echo "<tr>";
                    echo "<td rowspan='$rowspan'>$i</td>";
                    echo "<td rowspan='$rowspan'>".$row['MaSP']."</td>";
                    echo "<td rowspan='$rowspan'>".$row['Title']."</td>";
                    echo "<td rowspan='$rowspan'>".$row['Donvi']."</td>";
                    echo "<td rowspan='$rowspan' class='text-right'><b>".number_format($row['Totalamount'])."</b></td>";
                    echo "<td rowspan='$rowspan' class='text-right'><b>".number_format($row['Total'])."</b></td>";
                    echo "</tr>";
                    if(count($row['List'])>0){
                        foreach($row['List'] as $item){
                            $payment_status = isset($arr_payment_ok[$key][$item['ExportID']][$SupplierID]) ? $arr_payment_ok[$key][$item['ExportID']][$SupplierID] : "" ;
                            $PriceVAT = $item['Price']/1.1;
                            echo "<tr>";
                            echo "<td>".$item['MaXK']."</td>";
                            echo "<td><a title='".$item['Title']."'>".$arr_type[$item['Type']]."</a></td>";
                            echo "<td class='text-right'>".number_format($PriceVAT)."</td>";
                            echo "<td class='text-right'>".number_format($item['Price'])."</td>";
                            echo "<td class='text-right'>".number_format($item['Amount'])."</td>";
                            echo "<td class='text-right'>".number_format($item['Total'])."</td>";
                            echo "<td class='text-right'>".$item['ShipmentCode']."</td>";
                            if($payment_status!=''){
                                echo "<td>$payment_status</td>";
                            }else{
                                echo $ProductsID>0 && $OrderType==0  ? "<td><a class='btn btn-default btnpayment hidden' onclick='active_payment(this,".$item['ExportID'].")'>Thanh toán</a></td>" : "";
                            }
                            echo "</tr>";
                        }
                    }
                    $i++;
                }
                $colspan= $ProductsID>0 && $OrderType==0 ? "<th></th>" : "" ;
                echo "<tr>
                        <th colspan='3' class='text-right'>TỔNG CỘNG</th>
                        <th class='text-right'></th>
                        <th></th>
                        <th class='text-right'>".number_format($TotalPrice)."</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class='text-right'>".number_format($TotalAmount)."</th>
                        <th></th>
                        $colspan
                        <th></th>
                    </tr>";
            }else{
                $colspan= $ProductsID>0 && $OrderType==0 ? 14 : 13 ;
                echo "<tr><td colspan='$colspan' class='text-center'>Không tìm thấy dữ liệu để báo cáo kết quả kinh doanh .</td></tr>";
            }
            ?>
        </table>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<style>
    .table tr th{background:#EEE;}
</style>
<script type="text/javascript">
    $(document).ready(function () {

        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });
    

    function changefillter(){
        var WarehouseID = $("#WarehouseID").val();
        var SupplierID = $("#SupplierID").val();
        var TrademarkID = $("#TrademarkID").val();
        var ProductsID = $("#ProductsID").val();
        var OrderType = $("#OrderType").val();
        var PaymentStatus = $("#PaymentStatus").val();
        window.location = "<?php echo base_url().ADMINPATH.'/report/report_sales/report_supplier?' ?>"+"WarehouseID="+WarehouseID+"&SupplierID="+SupplierID+"&TrademarkID="+TrademarkID+"&ProductsID="+ProductsID+"&OrderType="+OrderType+"&PaymentStatus="+PaymentStatus;
    }

    function active_payment(ob,ID){
        var getValue = prompt("Vui lòng điền thông tin đợt thanh toán ", "");
        if(getValue!=null){
            note = getValue;
            if(note!=''){
                $.ajax({
                    url: "<?php echo base_url().ADMINPATH.'/report/report_sales/active_payment'; ?>",
                    dataType: "html",
                    type: "POST",
                    data: "SupplierID=<?php echo $SupplierID ?>&OrderType=<?php echo $OrderType ?>&ProductsID=<?php echo $ProductsID ?>&ExportID="+ID+"&Note="+note,
                    success: function(result){
                        $(ob).parent('td').html(result);
                    }
                });
            }
        }
    }

    function showbutton(ob){
        if($(ob).is(':checked')){
            $(".btnpayment").removeClass('hidden');
        }else{
            $(".btnpayment").addClass('hidden');
        }
    }

</script>
