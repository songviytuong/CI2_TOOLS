<?php

class Orders_model extends CI_Model {

    var $tablename = 'ttp_report_order';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    
    function getRowResult($fields, $params = array()) {
        $this->db->select($fields);
        if (!empty($params['ID'])) {
            $this->db->where('ID', $params['ID']);
        }
        $result = $this->db->get($this->tablename)->row();
        if ($result) {
            return $result;
        }
    }

    function getOrders($params = array()) {
        $this->db->select('*');
        if (!empty($params['Status'])) {
            $this->db->where('Status', $params['Status']);
        }
        $result = $this->db->get($this->tablename)->result_array();
        return $result;
    }


}
