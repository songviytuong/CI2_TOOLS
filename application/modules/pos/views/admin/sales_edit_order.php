<div class="sub_menu">
	<div class="find_record">
		
	</div>
</div>
<div class="containner">
	<div class="order_info edit_order">
		<div class="row">
			<div class="col-xs-8">
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Mã hóa đơn</span>: <?php echo $data->MaDH ?></div>
				<?php 
				$arr = array(
					0=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Thành công</a>',
					1=>'<a class="status-button"><i class="fa fa-circle text-danger" aria-hidden="true"></i> Đã hủy</a>',
					7=>'<a class="status-button"><i class="fa fa-circle text-warning" aria-hidden="true"></i> Đang giao hàng</a>'
				);
				?>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Trạng thái</span>: <?php echo $arr[$data->Status]; ?></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Khách hàng</span>: <input type="text" id="order_name" value="<?php echo $data->Name ?>" /></div>
				<div class="col-xs-6"><span class="pull-left" style="width:120px;">Số điện thoại</span>: <input type="text" id="order_phone" value="<?php echo $data->Phone ?>" /></div>
				<div class="col-xs-6">
					<span class="pull-left" style="width:120px;">Nhận hàng</span>: 
					<select id="order_transporttype" onchange="sales_hidden_transport(this)">
						<option value="0" <?php echo $data->TransportID==0 ? 'selected="selected"' : '' ; ?>>Tại shop</option>
						<option value="1" <?php echo $data->TransportID==1 ? 'selected="selected"' : '' ; ?>>Giao tận nơi</option>
					</select>
				</div>
				<div class="col-xs-6 hidden-transport <?php echo $data->TransportID==0 ? 'hidden' : '' ; ?>">
					<span class="pull-left" style="width:120px;">Tỉnh thành</span>: 
					<select id="order_city">
						<option value="30">Hồ Chí Minh</option>
					</select>
				</div>
				<div class="col-xs-6 hidden-transport <?php echo $data->TransportID==0 ? 'hidden' : '' ; ?>">
					<span class="pull-left" style="width:120px;">Quận huyện</span>: 
					<select id="order_district">
						<?php 
						$district = $this->db->query("select * from ttp_report_district where CityID=30")->result();
						if(count($district)>0){
							foreach ($district as $key => $value) {
								$selected = $data->DistrictID==$value->ID ? 'selected="selected"' : '' ;
								echo "<option value='$value->ID' $selected>$value->Title</option>";
							}
						}
						?>
					</select>
				</div>
				<div class="col-xs-6 hidden-transport <?php echo $data->TransportID==0 ? 'hidden' : '' ; ?>">
					<span class="pull-left" style="width:120px;">Địa chỉ giao hàng</span>: 
					<input type="text" id="order_address" value="<?php echo $data->AddressOrder ?>" />
				</div>
				<div class="col-xs-6 hidden-transport <?php echo $data->TransportID==0 ? 'hidden' : '' ; ?>">
					<span class="pull-left" style="width:120px;">Đơn vị v/c</span>: 
					<select id="order_transportid">
						<?php 
						$transport = $this->db->query("select * from ttp_report_transport")->result();
						if(count($transport)>0){
							foreach ($transport as $key => $value) {
								$selected = $data->TransportID==$value->ID ? 'selected="selected"' : '' ;
								echo "<option value='$value->ID' $selected>$value->Title</option>";
							}
						}
						?>
					</select>
				</div>
				<div class="col-xs-6 hidden-transport <?php echo $data->TransportID==0 ? 'hidden' : '' ; ?>">
					<span class="pull-left" style="width:120px;">Nhân viên v/c</span>: 
					<select id="order_transfer" class="">
						<?php 
						$user_transport = $this->db->query("select a.UserName,a.ID from ttp_user a,ttp_user_transport b where a.ID=b.UserID")->result();
						if(count($user_transport)>0){
							$transfer = $this->db->query("select UserReciver from ttp_report_order_send_supplier where OrderID=$data->ID")->row();
							$transfer = $transfer ? $transfer->UserReciver : 0 ;
							echo $transfer;
							foreach ($user_transport as $key => $value) {
								$selected = $transfer==$value->ID ? 'selected="selected"' : '' ;
								echo "<option value='$value->ID' $selected>$value->UserName</option>";
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="col-xs-4">
				<textarea placeholder="Ghi chú ..." id="order_note"><?php echo $data->Note ?></textarea>
			</div>
		</div>
	</div>
	<div class="table_div" style="margin-top:20px">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã SKU</th>
				<th>Tên sản phẩm</th>
				<th>Đơn vị</th>
				<th>Số lượng</th>
				<th>Giá bán</th>
				<th>Đã giảm</th>
				<th>Thành tiền</th>
			</tr>
			<tr>
				<?php 
				$details = $this->db->query("select a.*,b.Title,b.MaSP,b.Donvi from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
				$total = 0;
				if(count($details)>0){
					$i=1;
					foreach ($details as $row) {
						$total = $total + ($row->Price*$row->Amount);
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td>'.$row->MaSP.'</td>
						<td>'.$row->Title.'</td>
						<td>'.$row->Donvi.'</td>
						<td>'.$row->Amount.'<a class="pull-right" onclick="sales_save_change_amount(this,'.$row->Amount.','.$row->ID.','.$data->ID.')"><i class="fa fa-pencil"></i></a></td>
						<td>'.number_format($row->Price).'<a class="pull-right" onclick="sales_save_change_price(this,'.$row->Price.','.$row->ID.','.$data->ID.')"><i class="fa fa-pencil"></i></a></td>
						<td>'.number_format($row->PriceDown).'</td>
						<td>'.number_format($row->Price*$row->Amount).'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>TỔNG CỘNG : </b></td>
				<td><b><?php echo number_format($total) ?></b></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>GIẢM GIÁ : </b></td>
				<td><b><?php echo number_format($data->Chietkhau) ?></b><a class="pull-right" onclick="sales_save_change_discount(this,<?php echo $data->Chietkhau.",".$data->ID ?>)"><i class="fa fa-pencil"></i></a></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>CHI PHÍ KHÁC : </b></td>
				<td><b><?php echo number_format($data->Chiphi) ?></b> <a onclick="sales_save_change_chiphi(this,<?php echo $data->Chiphi.",".$data->ID ?>)" class="pull-right"><i class="fa fa-pencil"></i></a></td>
			</tr>
			<tr>
				<td colspan="7" class="text-right"><b>THÀNH TIỀN PHẢI TRẢ : </b></td>
				<td><b><?php echo number_format($data->Total - $data->Chietkhau + $data->Chiphi) ?></b></td>
			</tr>
		</table>
	</div>
	<hr>
	<div class="row text-right">
		<div class="col-xs-12">
			<div class="col-xs-6"><div class="alert hidden text-left"></div></div>
			<div class="col-xs-6">
			<a class="btn btn-primary" onclick="sales_save_change_order(this,<?php echo $data->ID ?>)"><i class="fa fa-pencil"></i> Lưu thay đổi</a>
			<a class="btn btn-default" href="<?php echo ADMINPATH.'/pos/pos_sales/view_order/'.$data->ID ?>"><i class="fa fa-times"></i> Hủy bỏ</a>
			</div>
		</div>
	</div>
</div>
