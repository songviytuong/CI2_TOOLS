<div class="containner">
	<div class="disableedit" style="height:1000px"></div>
	<div class="status_progress">
    	<?php 
    	$status1 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point1 = $status1=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point1 = $data->Status==3 || $data->Status==5 || $data->Status==7  || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point1 ;

		$status2 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point2 = $status2=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point2 =  $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point2 ;    	

    	$status3 = $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point3 = $status3=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point3 =  $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point3 ;

    	$status4 = $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point4 = $status4=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point4 =  $data->Status==7 || $data->Status==8 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point4 ;

		$status5 = $data->Status==8 || $data->Status==7 || $data->Status==0 ? 'class="active"' : "";
    	$point5 = $status5=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point5 =  $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point5 ;    	
    	
    	$width = $data->Status==2 || $data->Status==4 || $data->Status==6 ? 20 : 0 ;
    	$width = $data->Status==3 ? 40 : $width ;
    	$width = $data->Status==5 ? 60 : $width ;
    	$width = $data->Status==9 ? 80 : $width ;
    	$width = $data->Status==7 || $data->Status==0 || $data->Status==8 ? 100 : $width ;

    	if($data->Status==1 || $data->Status==8){
    		$title_drop = $data->Status==8 ? "Bị trả về" : "Bị hủy" ;
    		echo "<div class='orderisdrop'><a>$title_drop</a><span><i class='fa fa-times'></i></span></div>";
    		echo '<div class="transfer_progress" style="width:100%;background:#000"></div>';
    	}else{
    	?>
    	<ul>
            <li class="active"><a>ĐH nháp</a><span class="ok"><i class="fa fa-check-circle"></i></span></li>
            <li <?php echo $status1 ?>><a>Đã chuyển sang kho</a><?php echo $point1 ?></li>
            <li <?php echo $status2 ?>><a>Kho xác nhận</a><?php echo $point2 ?></li>
            <li <?php echo $status3 ?>><a>Kế toán xác nhận</a><?php echo $point3 ?></li>
            <li <?php echo $status4 ?>><a>Đã giao vận chuyển</a><?php echo $point4 ?></li>
            <li <?php echo $status5 ?>><a>Hoàn tất</a><?php echo $point5 ?></li>
        </ul>
        <div class="transfer_progress" style="width:<?php echo $width ?>%"></div>
        <?php 
    	}
        ?>
    </div>
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href="<?php echo $base_link ?>">THÔNG TIN ĐƠN HÀNG</a> - <a style="color:#DE5252"><?php echo $data->MaDH ?></a></h1>
	    </div>
	    <div class="block2">
	    	<div class="block2">
		    	<div class="btn_group">
		    		<a class="btn btn-default btn-drop pull-right" onclick="show_cancelbox()" style="margin-right: 0px;">Hủy <i class="fa fa-caret-down"></i></a>
					<?php 
						$arr_url = array(
			    			2=>"save_from_kho",
			    			3=>"save_from_ketoan",
			    			4=>"save_from_dieuphoi",
			    			7=>"save_from_ketoan"
			    		);
						$next = array(2=>5,3=>9,4=>7,7=>0);
						$prev = array(2=>4,3=>6,4=>7,7=>6);
						if(array_key_exists($this->user->UserType,$next)){
							if($this->user->UserType==3){
				    			if($data->Payment==1){
					    			if($data->PaymentStatus==1){
						    			$check_export_warehouse = $this->db->query("select * from ttp_report_export_warehouse where OrderID='$data->ID'")->row();
						    			if($check_export_warehouse){
						    				echo $this->user->UserType==3 ? '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$next[$this->user->UserType].')"><i class="fa fa-check-square"></i> Duyệt</a>' : '';
						    			}
					    			}
				    			}else{
				    				$check_export_warehouse = $this->db->query("select * from ttp_report_export_warehouse where OrderID='$data->ID'")->row();
					    			if($check_export_warehouse){
					  		  			echo $this->user->UserType==3 ? '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$next[$this->user->UserType].')"><i class="fa fa-check-square"></i> Duyệt</a>' : '';
					    			}
				    			}
				    		}
							echo $this->user->UserType==2 || $this->user->UserType==3 || $this->user->UserType==7 ? '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$prev[$this->user->UserType].')"><i class="fa fa-reply-all"></i> Trả về</a>' : '';
							echo $this->user->UserType==2 || $this->user->UserType==3 ? '<a class="btn btn-primary btn-update disablebutton" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\','.$next[$this->user->UserType].')"><i class="fa fa-check-square"></i> Duyệt</a>' : '';
							echo $this->user->UserType==2 ? '<a class="btn btn-success btn-activeedit" data="0"><i class="fa fa-pencil"></i> Chỉnh sửa</a>' : '';
							echo $this->user->UserType==2 ? '<a class="btn btn-danger btn-cancel disablebutton" onclick="location.reload()"><i class="fa fa-undo"></i> Hủy bỏ</a>' : '';
							//echo $this->user->UserType==3 ? "<a class='btn btn-primary btn-print' href='".base_url().ADMINPATH."/report/import/printorder/$data->ID'><i class='fa fa-print'></i> In đơn hàng</a>" : "" ;
							echo $this->user->UserType==7 ? "<a class='btn btn-primary btn-print' onclick='confirmwarning(this)' data='".base_url().ADMINPATH."/report/import_order/acceptsingle/$data->ID'><i class='fa fa-check-square'></i> Duyệt</a>" : "" ;
							
							$onoff = @file_get_contents('log/status/onoff.txt');
							if($data->Payment==1){
			    				if($onoff==1){
			    					if($data->PaymentStatus==1){
				    					echo $this->user->UserType==3 && $data->Accept==2 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ; 
				    					echo $this->user->UserType==3 && $data->Accept==1 ? "<span>Đang chờ kiểm tra lại</span>" : "" ; 
				    					echo $this->user->UserType==3 && $data->Accept==0 ? "<a class='btn btn-success btn-money' href='".base_url().ADMINPATH."/report/import_order/accept_level1/$data->ID'><i class='fa fa-check-square'></i> Tôi đã kiểm tra</a>" : "";
				    				}else{
				    					echo $this->user->UserType==3 ? "<a class='btn btn-success btn-money' href='".$base_link."accept_money/$data->ID'><i class='fa fa-exchange'></i> Đã nhận tiền</a>" : "";
				    				}
			    				}else{
			    					if($data->PaymentStatus==1){
				    					echo $this->user->UserType==3 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ; 
				    				}else{
				    					echo $this->user->UserType==3 ? "<a class='btn btn-success btn-money' href='".$base_link."accept_money/$data->ID'><i class='fa fa-exchange'></i> Đã nhận tiền</a>" : "";
				    				}
			    				}
			    			}else{
			    				if($onoff==1){
				    				echo $this->user->UserType==3 && $data->Accept==2 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ; 
				    				echo $this->user->UserType==3 && $data->Accept==1 ? "<span>Đang chờ kiểm tra lại</span>" : "";
				    				echo $this->user->UserType==3 && $data->Accept==0 ? "<a class='btn btn-success btn-money' href='".base_url().ADMINPATH."/report/import_order/accept_level1/$data->ID'><i class='fa fa-check-square'></i> Tôi đã kiểm tra</a>" : "";
			    				}else{
			    					echo $this->user->UserType==3 ? "<a class='btn btn-primary btn-export' href='".base_url().ADMINPATH."/report/import/lapphieuxuatkho/$data->ID'><i class='fa fa-external-link-square'></i> Phiếu xuất kho</a>" : "" ;
			    				}
			    			}
			    			if($this->user->UserType==4){
			    				if($data->Status==9){
			    					echo '<a class="btn btn-danger btn-accept" type="submit" onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\',7)"><i class="fa fa-check-square"></i> Xuất kho</a>';
			    					echo '<div class="col-xs-7"><select id="doitacvanchuyen" class="form-control"><option value="">-- Chọn đối tác vận chuyển --</option>';
				    				$transport = $this->db->query("select * from ttp_report_transport where Publish = 1")->result();
				    				if(count($transport)>0){
                                                                    foreach($transport as $row){
                                                                        if($row->Api == 1){
                                                                            $api = "(API)";
                                                                        }else{
                                                                            $api = "";
                                                                        }
				    					$selected = $row->ID==$data->TransportID ? "selected='selected'" : '' ;
				    					echo "<option value=".$row->ID. "$selected>".$row->Title." $api</option>";
                                                                    }
				    				}
					    			echo '</select></div>';
			    				}elseif($data->Status==7 || $data->Status==11){
                                                                $refExits = $this->db->query("select TransportID,TransportRef from ttp_report_order where ID=$data->ID")->row();
                                                                $dis = "";
                                                                $style = "";
                                                                if(in_array($refExits->TransportID,$arr)){
                                                                    $dis = " disabled ";
                                                                    $style = ' style="display:none"';
                                                                }
                                                                
                                                                echo '<a class="btn btn-danger" type="submit"'.$dis.$style.' onclick="updatestatus(this,\''.$arr_url[$this->user->UserType].'\',0)"><i class="fa fa-check-square"></i> ĐH thành công</a>';
                                                                if(in_array($refExits->TransportID,$arr)){
                                                                    if(!empty($refExits->TransportRef)){
                                                                        echo '<a class="btn btn-success" type="button" onclick="transportInfo(this,'.$refExits->TransportID.')"><i class="fa fa-truck"></i> Xem thông tin</a>';
                                                                    }else{
                                                                        echo '<a class="btn btn-primary" type="button" onclick="deliveryTransport(this)"><i class="fa fa-truck"></i> Tạo vận đơn</a>';
                                                                    }
                                                                }
			    				}
			    			}
						}
					?>
					<div class="cancel_box">
						<p>Chọn lý do hủy đơn hàng</p>
						<select id="Lydohuydh" onchange="checkshowlydokhac(this)" class="form-control">
			    			<option value=''>-- Lý do hủy đơn hàng --</option>
			    			<?php 
			    			$reason = $this->db->query("select * from ttp_report_order_reason")->result();
			    			if(count($reason)>0){
			    				foreach($reason as $row){
			    					echo "<option value='$row->ID'>$row->Title</option>";
			    				}
			    			}
			    			?>
			    			<option value='0'>LÝ DO KHÁC</option>
			    		</select>
			    		<p class='lydokhac'><span>Lý do hủy khác : </span><input type='text' id='Lydokhac' class="form-control" placeholder="Điền lý do hủy khác ..." /></p>
			    		<a class='btn btn-danger btn-acceptcancel' onclick='send_cancel_request(this)' data='<?php echo base_url().ADMINPATH."/report/import_order/cancel_request/$data->ID"; ?>'><i class='fa fa-minus-circle'></i> Xác nhận hủy</a>
					</div>
				</div>
		    </div>
	    </div>
    </div>
    
    <div class="import_order_info">
    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
    		<input type="hidden" name="IDOrder" id="IDOrder" value="<?php echo $data->ID ?>" />
                <?php
                    $ExportID = $this->db->query("select ID from ttp_report_export_warehouse where OrderID=$data->ID")->row();
                    $ExportID = $ExportID ? $ExportID->ID : 0 ;
                ?>
    		<input type="hidden" name="IDExport" id="IDExport" value="<?php echo $ExportID ?>" />
	    	<div class="block1">
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Loại khách hàng</label>
    						<div class="col-xs-7">
		    					<input type="text" class="form-control" value="<?php echo $data->CustomerType==0 ? "Khách hàng mới" : "Khách hàng cũ" ; ?>" />
		    				</div>
	    				</div>
	    			</div>
	    			<?php if($this->user->UserType==3){ ?>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<a class="btn btn-default" onclick="searchcustomer(this)" style="position: relative;z-index: 999"><i class="fa fa-search" aria-hidden="true"></i> Kiểm tra khách hàng</a>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label for="" class="control-label col-xs-5">Hình ảnh đính kèm</label>
    						<div class="col-xs-7">
    							<?php 
								if(file_exists($data->ImageCMND)){
									echo "<a target='_blank' style='position: relative;border-radius: 3px;overflow: hidden;z-index:999999' href='".base_url().$data->ImageCMND."'><img style='height:36px;border-radius: 3px' src='".$data->ImageCMND."' /></a>";
								}
								?>
    						</div>
	    				</div>
	    			</div>
	    			<?php } ?>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Nhân viên bán hàng </label>
    						<div class="col-xs-7">
		    					<select name="UserID" id="SalesmanID" class="form-control" >
				    				<?php 
				    				echo "<option value='".$this->user->ID."'>".$data->UserName."</option>";
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Nguồn đơn hàng </label>
    						<div class="col-xs-7">
		    					<select name="SourceID" id="SourceID" class="form-control">	
				    				<?php 
				    				$source = $this->db->query("select * from ttp_report_source")->result();
				    				if(count($source)>0){
				    					foreach ($source as $row) {
				    						$selected = $row->ID==$data->SourceID ? "selected='selected'" : '' ;
	    									echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Kênh bán hàng </label>
    						<div class="col-xs-7">
		    					<select name="KenhbanhangID" id="KenhbanhangID" class="form-control">
				    				<option value='0'>-- Chọn kênh bán hàng --</option>
				    				<?php 
				    				$kenhbanhang = $this->db->query("select * from ttp_report_saleschannel")->result();
				    				if(count($kenhbanhang)>0){
				    					foreach($kenhbanhang as $row){
				    						$selected = $row->ID==$data->KenhbanhangID ? "selected='selected'" : '' ;
	    									echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row row4">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Tên khách hàng </label>
    						<div class="col-xs-7">
		    					<input name="CustomerID" id="CustomerID" value="<?php echo $data->CustomerID ?>" type="hidden" />
		    					<input type='text' name="Tenkhachhang" class="form-control" id="Tenkhachhang" value="<?php echo $data->Name ?>" required />
		    				</div>
	    				</div>
	    			</div>
					<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Ngày sinh </label>
    						<div class="col-xs-7">
		    					<input type='text' name="NTNS" id="NTNS" placeholder="dd/mm/yyyy" value="<?php echo date('d/m/Y',strtotime($data->Birthday)) ?>" class="form-control NTNS" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Số CMND </label>
    						<div class="col-xs-7">
		    					<input type='text' name="Age" class="form-control Age" value="<?php echo $data->CMND ?>" id="Age" />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-12" style="margin-top:10px">
	    				<div class="form-group">
	    					<div class='lichsugiaodich' <?php echo $this->user->UserType==3 ? 'style="position: relative;z-index: 999;"' : '' ; ?>><div class="history_cus"></div><a id='xemlichsugiaodich' data-status="down">Xem lịch sử giao dịch <b class='caret'></b></a></div>
	    				</div>
	    			</div>
	    		</div>

				<div class="row row7">
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Số di động 1: </label>
    						<div class="col-xs-7">
		    					<input type='text' name="Phone1" id="Phone1" class="form-control" value='<?php echo $data->Phone1 ?>' <?php echo $this->user->UserType==3 ? 'style="position: relative;z-index: 999;"' : '' ; ?> required />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Số di động 2: </label>
    						<div class="col-xs-7">
		    					<input type='text' name="Phone2" id="Phone2" class="form-control" value='<?php echo $data->Phone2 ?>' />
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">PT thanh toán</label>
    						<div class="col-xs-7">
                                <select name="PTthanhtoan" class="form-control" id="PTthanhtoan">
                                	<?php 
		    						$pay_status = $this->define_model->get_order_status('payment','order');
					                $arr_payment = array();
					                foreach($pay_status as $key=>$ite){
					                    $code = (int)$ite->code;
					                    $selected = $data->Payment==$code ? 'selected="selected"' : '' ;
					                    echo "<option value='".$code."' $selected>".$ite->name."</option>";
					                }
					                ?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>	
	    		</div>
				<div class="row row7">
	    			<?php 
	    			$tinhthanh = $this->db->query("select a.Title,a.ID,b.Title as AreaTitle,a.AreaID,a.DefaultWarehouse from ttp_report_city a,ttp_report_area b where a.AreaID=b.ID and a.ID=$data->CityID")->row();
	    			?>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Khu vực </label>
    						<div class="col-xs-7">
		    					<select id="Khuvuc" class="form-control">
				    				<option value="0">-- Chọn khu vực --</option>
				    				<?php 
				    				$area = $this->db->query("select * from ttp_report_area")->result();
				    				if(count($area)>0){
				    					foreach($area as $row){
				    						$selected= '';
				    						if($tinhthanh){
				    							$selected = $row->ID==$tinhthanh->AreaID ? "selected='selected'" : '' ;
				    						}
				    						echo "<option value='$row->ID' $selected>$row->Title</option>";
				    					}
				    				}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Tỉnh / Thành </label>
    						<div class="col-xs-7">
		    					<select id="Tinhthanh" class="form-control" name="CityID">
				    				<?php 
				    					$selected= '';
			    						if($tinhthanh){
			    							$selected = $row->ID==$tinhthanh->AreaID ? "selected='selected'" : '' ;
			    							echo "<option value='$tinhthanh->ID'>$tinhthanh->Title</option>";
			    						}else{
			    							echo '<option value="">-- Chọn tỉnh thành --</option>';
			    						}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
    						<label class="col-xs-5 control-label">Quận / Huyện </label>
    						<div class="col-xs-7">
		    					<select id="Quanhuyen" class="form-control" name="DistrictID">
				    				<?php 
				    					$selected= '';
				    					$quanhuyen = $this->db->query("select * from ttp_report_district where ID=$data->DistrictID")->row();
			    						if($quanhuyen){
			    							$selected = $row->ID==$data->DistrictID ? "selected='selected'" : '' ;
			    							echo "<option value='$quanhuyen->ID'>$quanhuyen->Title</option>";
			    						}else{
			    							echo '<option value="">-- Chọn quận huyện --</option>';
			    						}
				    				?>
				    			</select>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
				<div class="row row6">
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Địa chỉ giao hàng </label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Address" id="Address_order" class="form-control" value="<?php echo $data->AddressOrder ?>" <?php echo $this->user->UserType==3 ? 'style="position: relative;z-index: 999;"' : '' ; ?> required />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row row6">
	    			<div class="col-xs-12">
		    			<div class="form-group">
			    			<label class="col-xs-12 control-label">Ghi chú giao hàng</label>
			    			<div class="col-xs-12">
			    				<input type='text' name="Note" class="form-control" value='<?php echo $data->Note ?>' />
			    			</div>
		    			</div>
	    			</div>
	    		</div>
	    	</div>

	    	<?php 
    		if($this->user->UserType==2){
    		?>
	    	<!-- end block1 -->
	    	<div class="block2 row">
	    		<div class="col-xs-6">
	    			<div class="form-group">
		    			<div class="col-xs-12">
		    				<a class="btn btn-danger" id="add_products_to_order"><i class="fa fa-plus"></i> Thùng hàng</a>
			    			<ul>
								<li><a id='show_thaotac' class="btn btn-default">Thao tác <b class="caret"></b></a>
				    				<ul>
					    				<li><a id="edit_row_table"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</a></li>
					    				<li><a id="delete_row_table"><i class="fa fa-trash-o"></i> Xóa sản phẩm</a></li>
				    				</ul>
								</li>
							</ul>
						</div>
		    		</div>
		    	</div>
		    	<div class="col-xs-2"></div>
		    	<div class="col-xs-4">
		    		<div class="form-group">
		    			<label class="col-xs-4 control-label text-right">Kho lấy hàng </label>
		    			<div class="col-xs-7 pull-right">
		    				<select name="KhoID" id="KhoID" class="form-control" onchange="changewarehouse(this)">
			    				<option value="">-- Chọn Kho --</option>
			    				<?php 
			    				if($this->user->UserType==2 || $this->user->UserType==8){
			    					$khoresult = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
			    				}else{
			    					$khoresult = $this->db->query("select * from ttp_report_warehouse")->result();
			    				}
			    				if(count($khoresult)>0){
			    					foreach($khoresult as $row){
			    						$selected = '';
			    						if($data->KhoID>0){
			    							$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
			    						}else{
			    							if($tinhthanh){
			    								$selected = $tinhthanh->DefaultWarehouse==$row->ID ? "selected='selected'" : '' ;
			    							}
			    						}
			    						echo "<option value='$row->ID' $selected>$row->MaKho</option>";
			    					}
			    				}
			    				?>
			    			</select>
		    			</div>
		    		</div>
		    	</div>
	    	</div>
	    	<?php }else{
	    		echo "<h2 style='font-size:20px;font-weight:bold;margin-top:20px'>CHI TIẾT ĐƠN HÀNG</h2>";
	    	} ?>
	    	<div class="table_donhang table_data">
	    		<form id="tableform">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã sản phẩm</th>
		    				<th>Tên sản phẩm</th>
		    				<th>Lô</th>
		    				<th>Giá bán</th>
		    				<th>Số lượng</th>
		    				<th>% CK</th>
		    				<th>Giá trị CK</th>
		    				<th>Giá sau CK</th>
		    				<th>Thành tiền</th>
		    			</tr>
		    			<?php 
		    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.CategoriesID,b.VariantType from ttp_report_orderdetails a,ttp_report_products b where a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			$arrproducts = array();
		    			$temp = 0;
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					$Shipment = $this->db->query("select * from ttp_report_shipment where ID=$row->ShipmentID")->row();
		    					$ShipmentCode = $Shipment ? $Shipment->ShipmentCode : '--' ;
		    					$ShipmentCode = $row->VariantType==2 && $this->user->UserType==2 ? "<a onclick='viewcombo(this,$row->ID)'><i class='fa fa-table' aria-hidden='true'></i> Xem combo</a>" : $ShipmentCode ;
		    					$CategoriesID = json_decode($row->CategoriesID,true);
		    					$CategoriesID = is_array($CategoriesID) ? $CategoriesID : array() ;
		    					$categoriestemp = 62;
		    					echo "<tr>";
		    					if($this->user->UserType==2){
		    						echo "<td><input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='$row->ProductsID'>
	    								<input type='hidden' name='ProductsID[]' value='$row->ProductsID'></td>";
		    						if(in_array($categoriestemp, $CategoriesID)){
		    							$temp=1;
		    						}
		    					}else{
		    						echo "<td></td>";
		    					}
		    					echo "<td>$row->MaSP</td>";
		    					echo "<td>$row->Title</td>";
		    					echo "<td style='width:180px;text-align:left'><span class='ShipmentDefault' data-id='$row->ProductsID'>$ShipmentCode</span></td>";
			    				$giaban = $row->Price+$row->PriceDown;
		    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
		    					echo "<td><input type='number' class='dongia' value='$giaban' min='0' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>".number_format($giaban)."</span></td>";
		    					echo "<td><input type='number' name='Amount[]' class='soluong' value='$row->Amount' min='1' onchange='recal()' readonly='true' /></td>";
		    					echo "<td><input type='number' onchange='calrowchietkhau(this)' class='percentck' value='$phantramck' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>$phantramck</span></td>";
								echo "<td><input type='number' onchange='calrowchietkhau(this)' class='giack' value='$row->PriceDown' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->PriceDown)."</span></td>";
								echo "<td><input type='number' class='giasauCK' value='$row->Price' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Price)."</span></td>";
								echo "<td><input type='number' class='thanhtien' value='$row->Total' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Total)."</span></td>";
		    					echo "</tr>";
		    					$arrproducts[] = '"data'.$row->ProductsID.'"';
		    				}
		    			}
		    			
		    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
		    			$tongtienhang = $data->Total - $data->Chietkhau;
		    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi;
		    			?>
		    			<tr class="last">
		    				<td colspan="9">Tổng cộng</td>
		    				<td><input type="number" class="tongcong" readonly="true" value="<?php echo $data->Total ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong"><?php echo number_format($data->Total) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">% chiết khấu</td>
		    				<td><input type='number' class="phantramchietkhau" min="0" max="100" value="<?php echo $phantramchietkhau ?>" onchange="calchietkhau(this)" /></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Giá chiết khấu</td>
		    				<td><input type='number' class="giachietkhau" min="1" value="<?php echo $data->Chietkhau ?>" readonly="true" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiachietkhau"><?php echo number_format($data->Chietkhau) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng tiền hàng</td>
		    				<td><input type="number" class="tongtienhang" readonly="true" value="<?php echo $tongtienhang ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang"><?php echo number_format($tongtienhang) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Chi phí vận chuyển</td>
		    				<td><input type='number' class="chiphivanchuyen" value="<?php echo $data->Chiphi ?>" onchange="recal()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()"><?php echo number_format($data->Chiphi) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng giá trị thanh toán</td>
		    				<td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="<?php echo $tonggiatrithanhtoan ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan"><?php echo number_format($tonggiatrithanhtoan) ?></span></td>
		    				
		    			</tr>
		    		</table>
		    		<input type="hidden" name="OrderID" value="<?php echo $data->ID ?>" />
	    		</form>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
                            //->Get Trạng thái đơn hàng
                            $this->load->model('define_model', 'cfg');
                            $arr = $this->cfg->get_order_status('status','order');
                            $array_status = array();
                            foreach($arr as $key=>$ite){
                                $code = (int)$ite->code;
                                $array_status[$code] = $ite->name;
                            }
                            //-->End
                            echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
                            foreach($history as $row){
                                    echo "<tr>";
                                    echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
                                    echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
                                    echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
                                    echo "<td>$row->UserName</td>";
                                    echo "</tr>";
                            }
                            echo "</table>";
	    		}
	    		?>
	    	</div>
	    	<div class="block1">
	    		<?php 
	    		if($this->user->UserType==3 || $this->user->UserType==4){
	    		?>
	    		<div class="row row13">
	    			<span class="title">Kho xuất: </span>
	    			<span>
	    			<?php 
	    				$kho = $this->db->query("select b.Title as KhoTitle,b.MaKho from ttp_report_warehouse b where b.ID=$data->KhoID")->row();
	    				echo $kho ? $kho->KhoTitle." ($kho->MaKho)" : "Chưa chọn kho xuất" ;
	    			?>
	    			 </span>
	    		</div>
	    		<?php } ?>
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    		<input type="hidden" id="referer" value="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ; ?>" />
	    	</div>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<script type="text/javascript">

	var link = $("#baselink_report").val();
	function updatestatus(ob,url,status){
		if(status==5 || status==9){
			if (!confirm('BẠN CÓ CHẮC MUỐN DUYỆT ĐƠN HÀNG NÀY ??')) {
				return false;
			}
		}
		if(status==7){
			if (!confirm('BẠN CÓ CHẮC MUỐN XUẤT KHO ??')) {
				return false;
			}
		}
		if(status==4 || status==6){
			if (!confirm('BẠN CÓ CHẮC MUỐN TRẢ ĐƠN HÀNG NÀY VỀ CRM ??')) {
				return false;
			}
		}
		$(ob).addClass("saving");
		$(ob).find("i").hide();
		var tinhtrangdonhang = status;
		var ghichu = '';
		var getValue = prompt("Điền ghi chú thay đổi (nếu có) : ", "");
		if(getValue!=null){
			ghichu = getValue;
		}
		
		var temp = <?php echo $temp ?>;
		<?php 
		if($this->user->UserType==2){
		?>
		var KhoID = $("#KhoID").val();
		if(tinhtrangdonhang==5 && KhoID==''){
			alert("Vui lòng chọn tên kho .");
			$("#KhoID").focus();
			$(ob).removeClass("saving");
			return false;
		}
		<?php 
		}
		?>
		
		<?php 
		if($this->user->UserType==4){
		?>
		var doitacvanchuyen = $("#doitacvanchuyen").val();
		if(tinhtrangdonhang==7 || tinhtrangdonhang=="7"){
			if(doitacvanchuyen==''){
				alert("Vui lòng chọn đối tác vận chuyển");
				$(ob).removeClass("saving");
				$(ob).find("i").show();
				return false;
			}
		}
		<?php 
		}
		?>

		if(tinhtrangdonhang==4 || tinhtrangdonhang==6 || tinhtrangdonhang==8){
			if(ghichu==''){
				alert("Nội dung ghi chú là bắt buộc đối với hành động trả đơn hàng về .");
				$(ob).removeClass("saving");
				$(ob).find("i").show();
				return false;
			}
		}		
		$.ajax({
        	url: link+"import_order/"+url,
            dataType: "html",
            type: "POST",
            context: this,
            data: <?php echo $this->user->UserType==4 ? '"TransportID="+doitacvanchuyen+"&"+' : '' ; ?><?php echo $this->user->UserType==2 ? '"KhoID="+KhoID+"&"+' : '' ; ?>"Tinhtrangdonhang="+tinhtrangdonhang+"&Ghichu="+ghichu+"&OrderID=<?php echo $data->ID ?>",
            success: function(result){}
        }).always(function(){
            addfromkho(status);
        });
	};

	$("#close_overlay").click(function(){
		$(".over_lay").hide();
		disablescrollsetup();
	});

	$("#show_thaotac").click(function(){
		$(this).parent('li').find('ul').toggle();
	});

	$("#add_products_to_order").click(function(){
		var KhoID = $('#KhoID').val();
		if(KhoID=='' || KhoID==0){
			alert('Vui lòng chọn kho lấy hàng');
			$('#KhoID').focus();
			return false;
		}
		enablescrollsetup();
		$(".over_lay .box_inner .block2_inner").html("");
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner").css({'margin-top':'20px'});
			    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách sản phẩm");
			    	$(".over_lay .box_inner .block2_inner").html(result);        	
			    	$(".over_lay").removeClass('in');
			    	$(".over_lay").fadeIn('fast');
			    	$(".over_lay").addClass('in');
                }else{
                	disablescrollsetup();
                	alert("Kho hàng hiện tại không có sản phẩm");
                }
                $(this).removeClass('saving');
            }
        });
	});

	function input_search_products(ob){
		var data = $(ob).val();
		var KhoID = $('#KhoID').val();
		$.ajax({
        	url: link+"import_order/get_products",
            dataType: "html",
            type: "POST",
            context: this,
            data: "KhoID="+KhoID+"&Title="+data,
            success: function(result){
                if(result!='false'){
        			$(".over_lay .box_inner .block2_inner").html(result);        	
                }else{
                	alert("Không tìm thấy dữ liệu theo yêu cầu.");
                }
            }
        });        
	}

	var celldata = [<?php echo implode(',',$arrproducts) ?>];
	var sttrow = <?php echo count($arrproducts) ?>+1;
	function add_products(){
		$(".over_lay .box_inner .block2_inner .selected_products").each(function(){
			if(this.checked===true){
				var data_name = $(this).attr('data-name');
				var data_price = $(this).attr('data-price');
				var data_code = $(this).attr('data-code');
				var data_id = $(this).attr('data-id');
				if(jQuery.inArray( "data"+data_id, celldata )<0){
					var table = document.getElementById("table_data");
					var row = table.insertRow(sttrow);
					row.insertCell(0).innerHTML="<input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='"+data_id+"' /><input type='hidden' name='ProductsID[]' value='"+data_id+"' />";
					row.insertCell(1).innerHTML=data_code;
					row.insertCell(2).innerHTML=data_name;
					row.insertCell(3).innerHTML="<span class='ShipmentDefault' data-id='"+data_id+"'></span>";
					data_price = parseInt(data_price);
					row.insertCell(4).innerHTML="<input type='number' class='dongia' value='"+data_price+"' min='1' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(5).innerHTML="<input type='number' name='Amount[]' class='soluong' value='1' min='1' onchange='recal()' readonly='true' />";
					row.insertCell(6).innerHTML="<input type='number' onchange='calrowchietkhau(this)' class='percentck' value='0' min='0' readonly='true' style='display:none' /> <span class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(7).innerHTML="<input type='number' onchange='calrowchietkhau(this)' class='giack' value='0' min='0' readonly='true' style='display:none' /> <span class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>0</span>";
					row.insertCell(8).innerHTML="<input type='number' class='giasauCK' value='"+data_price+"' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					row.insertCell(9).innerHTML="<input type='number' class='thanhtien' value='"+data_price+"' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>"+data_price.format('a',3)+"</span>";
					sttrow=sttrow+1;
					celldata.push("data"+data_id);
				}
			}
		});
		loadshipmentdefault();
		$(".over_lay").hide();
		disablescrollsetup();
	}

	function checkshipment(ob){
		var amount = $(ob).val();
		var warehouse = $('#KhoID').val();
		var id = $(ob).parent('td').parent('tr').find('.ShipmentDefault').attr('data-id');
		$(ob).parent('td').parent('tr').find('.ShipmentDefault').load(link+"import_order/get_shipment_default/"+id+"/"+warehouse+"/"+amount);
		recal();
	}

	function loadshipmentdefault(){
		var warehouse = $('#KhoID').val();
		$(".ShipmentDefault").each(function(){
			var data = $(this).attr('data-id');
			var amount = $(this).parent('td').parent('tr').find('.soluong').val();
			$(this).load(link+"import_order/get_shipment_default/"+data+"/"+warehouse+"/"+amount);
		});
	}

	$("#delete_row_table").click(function(){
		$(this).parent('li').parent('ul').toggle();
		$("#table_data .selected_products").each(function(){
			if(this.checked===true){
				var data_id = $(this).attr('data-id');
				$(this).parent('td').parent('tr').remove();
				celldata.splice("data"+data_id, 1);
				sttrow = sttrow-1;
			}
		});
	});

	function changerow(ob){
		var parent = $(ob).parent('td').parent('tr');
		if(ob.checked===true){
			parent.find('span.showsoluong').hide();
			parent.find('span.showdongia').hide();
			parent.find('span.showpercentCK').hide();
			parent.find('span.showgiaCK').hide();
			parent.find('input[type="text"]').prop('readonly', false);
			parent.find('input.soluong').prop('readonly', false);
			parent.find('input.dongia').prop('readonly', true).show();
			parent.find('input.percentck').prop('readonly', false).show();
			parent.find('input.giack').prop('readonly', false).show();
			parent.find('input[type="text"]').addClass('border');
			parent.find('input.soluong').addClass('border');
			parent.find('input.dongia').addClass('border');
			parent.find('input.percentck').addClass('border');
			parent.find('input.giack').addClass('border');
			parent.find('input.giasauck').addClass('border');
		}else{
			parent.find('span.showsoluong').show();
			parent.find('span.showdongia').show();
			parent.find('span.showpercentCK').show();
			parent.find('span.showgiaCK').show();
			parent.find('input[type="text"]').prop('readonly', true);
			parent.find('input.soluong').prop('readonly', true);
			parent.find('input.dongia').prop('readonly', true).hide();
			parent.find('input.percentck').prop('readonly', true).hide();
			parent.find('input.giack').prop('readonly', true).hide();
			parent.find('input[type="text"]').removeClass('border');
			parent.find('input.soluong').removeClass('border');
			parent.find('input.dongia').removeClass('border');
			parent.find('input.percentck').removeClass('border');
			parent.find('input.giack').removeClass('border');
			parent.find('input.giasauck').removeClass('border');
		}
	}

	function checkfull(ob){
		if(ob.checked===true){
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').hide();
				parent.find('span.showdongia').hide();
				parent.find('span.showpercentCK').hide();
				parent.find('span.showgiaCK').hide();
				parent.find('input[type="text"]').prop('readonly', false);
				parent.find('input.soluong').prop('readonly', false);
				parent.find('input.dongia').prop('readonly', true).show();
				parent.find('input.percentck').prop('readonly', false).show();
				parent.find('input.giack').prop('readonly', false).show();
				parent.find('input[type="text"]').addClass('border');
				parent.find('input.soluong').addClass('border');
				parent.find('input.dongia').addClass('border');
				parent.find('input.percentck').addClass('border');
				parent.find('input.giack').addClass('border');
				parent.find('input.giasauck').addClass('border');
				parent.find('input[type="checkbox"]').prop("checked",true);
			});
		}else{
			$("#table_data .selected_products").each(function(){
				var parent = $(this).parent('td').parent('tr');
				parent.find('span.showsoluong').show();
				parent.find('span.showdongia').show();
				parent.find('span.showpercentCK').show();
				parent.find('span.showgiaCK').show();
				parent.find('input[type="text"]').prop('readonly', true);
				parent.find('input.soluong').prop('readonly', true);
				parent.find('input.dongia').prop('readonly', true).hide();
				parent.find('input.percentck').prop('readonly', true).hide();
				parent.find('input.giack').prop('readonly', true).hide();
				parent.find('input[type="text"]').removeClass('border');
				parent.find('input.soluong').removeClass('border');
				parent.find('input.dongia').removeClass('border');
				parent.find('input.percentck').removeClass('border');
				parent.find('input.giack').removeClass('border');
				parent.find('input.giasauck').removeClass('border');
				parent.find('input[type="checkbox"]').prop("checked",false);
			});
		}
	}

	function fillter_categories(ob){
		var data = $(ob).val();
		if(data==0){
			$(ob).parent().parent().parent().find("tr.trcategories").show();
		}else{
			$(ob).parent().parent().parent().find("tr.trcategories").hide();
			$(ob).parent().parent().parent().find("tr.categories_"+data).show();
		}
	}

	Number.prototype.format = function(n, x) {
	    var re = '(\\d)(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
	    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$1,');
	};

	function enablescrollsetup(){
		$(window).scrollTop(70);
		$("body").css({'height':'100%','overflow-y':'hidden'});
		h = window.innerHeight;
		h = h-230;
		$(".over_lay .box_inner .block2_inner").css({"max-height":h+"px"});
	}

	function disablescrollsetup(){
		$("body").css({'height':'auto','overflow-y':'scroll'});
	}

	function addfromkho(status){
		<?php 
		if($this->user->UserType==2){
		?>
		var tinhtrangdonhang = status;
		var KhoID = $("#KhoID").val();
		$("#tableform").append("<input type='hidden' name='WarehouseID' value='"+KhoID+"' />");
		if(tinhtrangdonhang==5){
			$.ajax({
	        	url: link+"import_order/add_from_kho",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: $("#tableform").serialize(),
	            success: function(result){
	            	if(result=="OK"){
	                	var links = $("#referer").val();
	                	if(links!=''){
	                		window.location = links;
	                	}else{
	                		window.location = link+"import_order";
	                	}
	                }else{
	                	alert("Có lỗi xảy ra trong quá trình truyền tải dữ liệu !. Vui lòng kiểm tra lại đường truyền .");
	                }
	            }
	        });
        }else{
        	window.location = link+"import_order";
        }
        <?php 
    	}else{
        ?>
        window.location = link+"import_order";
        <?php 
    	}
        ?>
	}
	
	function send_cancel_request(ob){
		var note = $("#Lydohuydh").val();
		var another = $("#Lydokhac").val();
		var links_request = $(ob).attr('data');
		if(note=='' || (note==0 && another=='')){
			alert("Vui lòng chọn lý do hủy đơn hàng !");
			$("#Lydohuydh").focus();
		}else{
			$(ob).addClass("saving");
			$(ob).find("i.fa").hide();
			$.ajax({
            	url: links_request,
	            dataType: "html",
	            type: "POST",
	            data: "ReasonID="+note+"&Note="+another,
	            success: function(result){
	            	location.reload();
	            }
	        });
		}
	}

	function checkshowlydokhac(ob){
		var data = $(ob).val();
		if(data==0){
			$(".lydokhac").show();
			$(".lydokhac").find('input').focus();
		}else{
			$(".lydokhac").hide();
		}
	}

	function show_cancelbox(){
		$(".cancel_box").toggle();
                $(".delivery_box").hide();
	}
        
        function show_deliverybox(){
		$(".delivery_box").toggle();
                $(".cancel_box").hide();
	}

	function confirmwarning(ob){
		if (!confirm('BẠN CÓ CHẮC MUỐN DUYỆT ĐƠN HÀNG NÀY VÀ CHUYỂN SANG ĐƠN HÀNG TIẾP THEO ??')) {
			return false;
		}
		$(ob).addClass("saving");
		$(ob).find("i").hide();
		var data = $(ob).attr('data');
		window.location=data;
	}

	function viewcombo(ob,ID){
		if(ID>0){
			enablescrollsetup();
			$(".over_lay .box_inner .block2_inner").html("");
			$.ajax({
	        	url: link+"import_order/get_combo_details",
	            dataType: "html",
	            type: "POST",
	            context: this,
	            data: "DetailsID="+ID,
	            success: function(result){
	            	if(result!="FALSE"){
	            		$(".over_lay .box_inner").css({'margin-top':'20px'});
				    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách chi tiết COMBO");
				    	$(".over_lay .box_inner .block2_inner").html(result);
				    	$(".over_lay").removeClass('in');
				    	$(".over_lay").fadeIn('fast');
				    	$(".over_lay").addClass('in');
	                }else{
	                	alert("Có lỗi xảy ra trong quá trình truyền tải dữ liệu !. Vui lòng kiểm tra lại đường truyền .");
	                }
	            }
	        });
        }
	}

	function changewarehouse(ob){
		var warehouse = $(ob).val();
		if(warehouse!=<?php echo $data->KhoID ?>){
			accept_changewarehouse();
		}
	}

	function accept_changewarehouse(){
		if (!confirm('BẠN CÓ CHẮC CHẮN MUỐN CHUYỂN KHO ??')) {
			$("#KhoID").val(<?php echo $data->KhoID ?>);
			return false;
		}else{
			$(".over_lay").fadeIn();
			$(".over_lay").html("<div class='loader' style='position:fixed;top:40%;left:50%'></div>");
			var WarehouseID = $("#KhoID").val();
			$.ajax({
	        	url: link+"import_order/change_warehouse",
	            dataType: "html",
	            type: "POST",
	            data: "ID=<?php echo $data->ID ?>&WarehouseID="+WarehouseID,
	            success: function(result){
	            	if(result=="OK"){
	            		alert("Thay đổi kho thành công !");
	            		location.reload();
	            	}else{
	            		alert(result);
	            		location.reload();
	            	}
	            }
	        });
		}
	}

	var statusloadhis = 0;
	$("#xemlichsugiaodich").click(function(){
		var datastatus = $("#xemlichsugiaodich").attr("data-status");
		if(datastatus=="down"){
			$("#xemlichsugiaodich").attr("data-status","up");
			$(this).html("Ẩn lịch sử giao dịch <i class='fa fa-caret-up'></i>");
		}else{
			$("#xemlichsugiaodich").attr("data-status","down");
			$(this).html("Xem lịch sử giao dịch <b class='caret'></b>");
		}
		var ID = <?php echo $data->CustomerID ?>;
		if(ID!='' && ID>0){
			if(statusloadhis==0){
				$.ajax({
	            	url: link+"import_order/get_history",
		            dataType: "html",
		            type: "POST",
		            context: this,
		            data: "ID="+ID+"&UserID=<?php echo $data->UserID ?>",
		            success: function(result){
		            	statusloadhis=1;
		                if(result!='false'){
			                $(".lichsugiaodich .history_cus").html(result);
		                }else{
		                	$(".lichsugiaodich .history_cus").html("<p>Không có lịch sử giao dịch của khách hàng này.</p>");
		                }
		            }
		        });
	        }
		}
	});

	function searchcustomer(ob){
		var keywords = $(ob).val();
		$.ajax({
        	url: link+"import_order/get_order_by_customer",
            dataType: "html",
            type: "POST",
            data: "keywords="+keywords,
            success: function(result){
            	$(".over_lay .box_inner").css({'margin-top':'20px','width':'1090px'});
		    	$(".over_lay .box_inner .block1_inner h1").html("Danh sách khách hàng");
		    	$(".over_lay .box_inner .block2_inner").html(result);
		    	$(".over_lay").removeClass('in');
		    	$(".over_lay").fadeIn('fast');
		    	$(".over_lay").addClass('in');
            }
        });
	}
        
        function deliveryTransport(ob){
            var fullname = $('#Tenkhachhang').val();
            var address = $('#Address_order').val();
            var phone1 = $('#Phone1').val();
            var phone2 = $('#Phone2').val();
            var phone = phone1 + phone2;
            var order_id = $('#IDOrder').val();
            var export_id = $('#IDExport').val();
            
            var district = $('#Quanhuyen').val();
            var pay_method = $('#PTthanhtoan').val();
                       
            $.ajax({
            url: link + "import_order/get_transport_form",
            dataType: "html",
            type: "POST",
            data: "total=<?=$tonggiatrithanhtoan;?>&pay_method="+pay_method+"&district="+district+"&address="+address+"&fullname="+fullname+"&phone="+phone+"&order_id="+order_id+"&export_id="+export_id,
            success: function(result){
            	$(".over_lay .box_inner").css({'margin-top':'20px','width':'70%'});
                $(".over_lay .box_inner .block1_inner h1").html("Tạo vận đơn");
                $(".over_lay .box_inner .block2_inner").html(result);
                $(".over_lay").removeClass('in');
                $(".over_lay").fadeIn('fast');
                $(".over_lay").addClass('in');
            }
        });
	}
        
        function transportInfo(ob){
            var order_id = $('#IDOrder').val();
            $.ajax({
            url: link+"import_order/get_transport_info",
            dataType: "html",
            type: "POST",
            data: "transport=<?=$data->TransportID;?>&order_id="+order_id,
            success: function(result){
            	$(".over_lay .box_inner").css({'margin-top':'20px','width':'30%'});
                $(".over_lay .box_inner .block1_inner h1").html("Thông tin vận chuyển");
                $(".over_lay .box_inner .block2_inner").html(result);
                $(".over_lay").removeClass('in');
                $(".over_lay").fadeIn('fast');
                $(".over_lay").addClass('in');
            }
        });
	}

</script>