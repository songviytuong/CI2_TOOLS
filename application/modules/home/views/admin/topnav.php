<div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li><a href='<?php echo $user->UserType==1 || $user->UserType==2 || $user->UserType==3 || $user->UserType==4 || $user->UserType==5 || $user->UserType==7 || $user->UserType==8 || $user->UserType==9 || $user->UserType==10 ? base_url() . ADMINPATH . "/report/import_order" : base_url() . ADMINPATH . "/report/report" ; ?>'>Report Tools</a></li>
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?php echo file_exists($user->Thumb) ? base_url().$user->Thumb : base_url()."public/admin/images/user.png" ; ?>" alt="<?php echo $user->UserName ?>"><?php echo $user->FirstName . " " .$user->LastName ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="<?php echo base_url() . ADMINPATH . "/home/profile"; ?>">Profile</a></li>
                                    <li><a href="<?php echo base_url() . ADMINPATH . "/logout"; ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>