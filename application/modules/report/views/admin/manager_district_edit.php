<div class="containner">
	<div class="manager">
		<form action="<?php echo $base_link.'update' ?>" method="POST">
			<input type="hidden" name="ID" value="<?php echo isset($data->ID) ? $data->ID : 0 ; ?>" />
			<div class="fillter_bar">
				<div class="block1">
					<h1>Chỉnh sửa quận huyện</h1>
				</div>
				<div class="block2">
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
			<div class="box_input">
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Tên quận / huyện</label>
					<div class='col-xs-4'><input type='text' class="form-control" name="Title" value="<?php echo isset($data->Title) ? $data->Title : '' ; ?>" required /></div>
				</div>
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Thuộc tỉnh thành</label>
					<div class='col-xs-4'>
						<select name="CategoriesID" class="form-control">
							<option value="0">-- Chọn tỉnh thành --</option>
							<?php 
							$categories = $this->db->query("select * from ttp_report_city")->result();
							if(count($categories)>0){
								foreach($categories as $row){
									$select = $row->ID==$data->CityID ? "selected='selected'" : '' ;
									echo "<option value='$row->ID' $select>$row->Title</option>";
								}
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Mã GHN</label>
					<div class='col-xs-4'><input type='text' class="form-control" name="GHN" value="<?php echo isset($data->GHN) ? $data->GHN : '' ; ?>" /></div>
				</div>
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Mã GoldTimes</label>
					<div class='col-xs-4'><input type='text' class="form-control" name="GoldTimes" value="<?php echo isset($data->GoldTimes) ? $data->GoldTimes : '' ; ?>" /></div>
				</div>
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Chi phí vận chuyển (dưới 500,000đ)</label>
					<div class='col-xs-4'><input type='number' class="form-control" name="PriceCost" value="<?php echo isset($data->PriceCost) ? $data->PriceCost : '' ; ?>" /></div>
				</div>
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Chi phí vận chuyển (từ 500,000đ - 2,000,000đ)</label>
					<div class='col-xs-4'><input type='number' class="form-control" name="PriceCost1" value="<?php echo isset($data->PriceCost1) ? $data->PriceCost1 : '' ; ?>" /></div>
				</div>
				<div class="row">
					<label class='col-xs-4 control-label text-right' style='padding-right:10px;'>Chi phí vận chuyển (trên 2,000,000đ)</label>
					<div class='col-xs-4'><input type='number' class="form-control" name="PriceCost2" value="<?php echo isset($data->PriceCost2) ? $data->PriceCost2 : '' ; ?>" /></div>
				</div>
			</div>
		</form>
	</div>
</div>
<style>
	.body_content .containner{min-height: 569px !important;}
	.row{margin:0px;}
</style>
