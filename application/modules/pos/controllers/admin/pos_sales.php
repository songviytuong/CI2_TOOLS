<?php 
class Pos_sales extends Admin_Controller { 
 	public $user;
 	public $classname="pos_sales";
    public $limit = 15;
    public $WarehouseID = 1;
    public $PosUser = 84;

    public function __construct() { 
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $session = $this->session->userdata('ttp_usercp');
		$this->user = $this->lib->get_user($session,$this->classname);
        $this->load->library('template');
        $this->template->set_template('report');
        $this->template->add_doctype();
        $warehouse = $this->session->userdata('branch');
        $this->WarehouseID = $warehouse!='' ? $warehouse : $this->WarehouseID ;
    }

    public function index(){
        $this->lib->check_permission_function($this->user);
        $files = scandir('log/pos/sales');
        if(count($files)==2){
            $txt = time();
            file_put_contents('log/pos/sales/draft_order_'.$txt.'.txt',json_encode(array('transport'=>0,'chiphi'=>0,'products'=>array(),'customer'=>array('Name'=>'','Phone'=>'','Payment'=>0,'Note'=>'',"Active"=>"0"))));
        }
        $this->template->add_title('Sales | POS Tools');
		$this->template->write_view('content','admin/sales');
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Bán hàng','function'=>'<a>Tạo đơn hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'sales'));
		$this->template->render();
	}

    public function view_order($id=0){
        $result = $this->db->query("select a.*,b.MaKho as WarehouseTitle,c.UserName from ttp_report_order a,ttp_report_warehouse b,ttp_user c where a.UserID=c.ID and a.KhoID=b.ID and a.ID=$id and UserID=".$this->user->ID)->row();
        if($result){
            $this->template->add_title('Sales | POS Tools');
            $this->template->write_view('content','admin/sales_order',array('data'=>$result));
            $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Bán hàng','function'=>'<a href="'.ADMINPATH.'/pos/pos_sales/bill">Hóa đơn bán hàng</a>','bonus'=>'<a>Thông tin hóa đơn</a>'));
            $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'bill'));
            $this->template->render();
        }
    }

    public function edit_order($id=0){
        $result = $this->db->query("select a.*,b.MaKho as WarehouseTitle,c.UserName from ttp_report_order a,ttp_report_warehouse b,ttp_user c where a.UserID=c.ID and a.KhoID=b.ID and a.ID=$id and UserID=".$this->user->ID)->row();
        if($result){
            $this->template->add_title('Sales | POS Tools');
            $this->template->write_view('content','admin/sales_edit_order',array('data'=>$result));
            $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Bán hàng','function'=>'<a href="'.ADMINPATH.'/pos/pos_sales/bill">Hóa đơn bán hàng</a>','bonus'=>'<a>Điều chỉnh thông tin hóa đơn</a>'));
            $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'bill'));
            $this->template->render();
        }
    }

    public function save_change_order(){
        $order = isset($_POST['id']) ? $_POST['id'] : 0 ;
        $name = isset($_POST['name']) ? $_POST['name'] : '' ;
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '' ;
        $city = isset($_POST['city']) ? $_POST['city'] : 0 ;
        $district = isset($_POST['district']) ? $_POST['district'] : 0 ;
        $address = isset($_POST['address']) ? $_POST['address'] : '' ;
        $transport = isset($_POST['transport']) ? $_POST['transport'] : 0 ;
        $transporttype = isset($_POST['transporttype']) ? $_POST['transporttype'] : 0 ;
        $transfer = isset($_POST['transfer']) ? $_POST['transfer'] : 0 ;
        $note = isset($_POST['note']) ? $_POST['note'] : '' ;
        $response = array('error'=>0,'message'=>'');
        if($name==''){
            $response['error'] = 1;
            $response['message'] = '<i class="fa fa-warning"></i> Vui lòng điền tên khách hàng';
            echo json_encode($response);
            return;
        }
        if($transporttype==1){
            if($address==''){
                $response['error'] = 1;
                $response['message'] = '<i class="fa fa-warning"></i> Vui lòng điền địa chỉ giao hàng';
                echo json_encode($response);
                return;
            }
        }
        if($response['error']==0){
            $sql = array('Name'=>$name,'Phone'=>$phone,'Note'=>$note);
            if($transporttype==1){
                $sql['CityID'] = $city;
                $sql['DistrictID'] = $district;
                $sql['AddressOrder'] = $address;
                $sql['TransportID'] = $transport;

                $this->db->query("update ttp_report_order_send_supplier set UserReciver=$transfer where OrderID=$order");
            }else{
                $sql['CityID'] = 30;
                $sql['DistrictID'] = 38;
                $sql['AddressOrder'] = 'C15 đường 15, phường Tân Thuận Tây';
                $sql['TransportID'] = 0;
            }
            $this->db->where('ID',$order);
            $this->db->update('ttp_report_order',$sql);
            $response['error'] = 0;
            $response['message'] = '<i class="fa fa-check"></i> Cập nhật thành công đơn hàng';
        }
        echo json_encode($response);
    }

    public function save_change_discount_order(){
        $order = isset($_POST['order']) ? (int)$_POST['order'] : 0 ;
        $discount = isset($_POST['discount']) ? (int)$_POST['discount'] : 0 ;
        $sql = array('Chietkhau'=>$discount);
        $this->db->where('ID',$order);
        $this->db->update('ttp_report_order',$sql);
    }

    public function save_change_price_order(){
        $order = isset($_POST['order']) ? (int)$_POST['order'] : 0 ;
        $id = isset($_POST['id']) ? (int)$_POST['id'] : 0 ;
        $price = isset($_POST['price']) ? (int)$_POST['price'] : 0 ;
        $result = $this->db->query("select * from ttp_report_orderdetails where ID=$id")->row();
        if($result){
            $pricedown = $result->PriceDown;
            if($result->Price>$price){
                $pricedown = $result->PriceDown+($result->Price-$price);
            }elseif($result->Price<$price){
                $pricedown = $result->PriceDown-($price-$result->Price);
                $pricedown = $pricedown>0 ? $pricedown : 0 ;
            }
            $sql = array('Price'=>$price,'PriceDown'=>$pricedown);
            $this->db->where('ID',$id);
            $this->db->update('ttp_report_orderdetails',$sql);

            $details  = $this->db->query("select * from ttp_report_orderdetails where OrderID=$order")->result();
            if(count($details)>0){
                $total = 0;
                foreach ($details as $key => $value) {
                    $total = $total+($value->Amount*$value->Price);
                }
                $this->db->where("ID",$order);
                $this->db->update("ttp_report_order",array('Total'=>$total));
            }
        }
    }

    public function save_change_amount_order(){
        $order = isset($_POST['order']) ? (int)$_POST['order'] : 0 ;
        $id = isset($_POST['id']) ? (int)$_POST['id'] : 0 ;
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 0 ;
        $result = $this->db->query("select * from ttp_report_orderdetails where ID=$id")->row();
        if($result && $amount>0){
            $sql = array('Amount'=>$amount);
            $this->db->where('ID',$id);
            $this->db->update('ttp_report_orderdetails',$sql);
            $data = $this->db->query("select Status,KhoID from ttp_report_order where ID=$order")->row();
            if($data){
                if($data->Status==0 || $data->Status==1){
                    $this->release_change_inventory($result->ProductsID,$result->ShipmentID,$data->KhoID,$result->Amount);
                    $this->change_inventory($result->ProductsID,$result->ShipmentID,$data->KhoID,$amount);
                }
            }
            $details  = $this->db->query("select * from ttp_report_orderdetails where OrderID=$order")->result();
            if(count($details)>0){
                $total = 0;
                foreach ($details as $key => $value) {
                    $total = $total+($value->Amount*$value->Price);
                }
                $this->db->where("ID",$order);
                $this->db->update("ttp_report_order",array('Total'=>$total));
            }
        }
    }

    public function save_change_chiphi_order(){
        $order = isset($_POST['order']) ? (int)$_POST['order'] : 0 ;
        $chiphi = isset($_POST['chiphi']) ? (int)$_POST['chiphi'] : 0 ;
        $sql = array('Chiphi'=>$chiphi);
        $this->db->where('ID',$order);
        $this->db->update('ttp_report_order',$sql);
    }

    public function load_details_order(){
        $order = isset($_POST['order']) ? (int)$_POST['order'] : 0 ;
        $data = $this->db->query("select * from ttp_report_order where ID=$order")->row();
        $this->load->view('admin/sales_order_details',array('data'=>$data));
    }

    public function export_data($currentday=0){
        if($currentday==0){
            $month = date('m');
            $year = date('Y');
            $result = $this->db->query("select a.ID,a.MaDH,a.Ngaydathang,a.Status,a.Name,a.Payment,a.Chietkhau,a.Chiphi,a.Total,b.Amount,b.Price,b.PriceDown,c.Title,c.MaSP from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and month(a.Ngaydathang)='$month' and year(a.Ngaydathang)='$year' and a.KhoID=".$this->WarehouseID)->result();
        }else{
            $result = $this->db->query("select a.ID,a.MaDH,a.Ngaydathang,a.Status,a.Name,a.Payment,a.Chietkhau,a.Chiphi,a.Total,b.Amount,b.Price,b.PriceDown,c.Title,c.MaSP from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where date(a.Ngaydathang)='".date('Y-m-d')."' and a.ID=b.OrderID and b.ProductsID=c.ID and a.KhoID=".$this->WarehouseID)->result();
        }
        if(count($result)>0){
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            if (PHP_SAPI == 'cli')
                die('This example should only be run from a Web Browser');
            require_once 'public/plugin/PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                                         ->setLastModifiedBy("Maarten Balliauw")
                                         ->setTitle("Office 2007 XLSX Test Document")
                                         ->setSubject("Office 2007 XLSX Test Document")
                                         ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                                         ->setKeywords("office 2007 openxml php")
                                         ->setCategory("Test result file");
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Ngày đặt hàng')
                        ->setCellValue('B1', 'Mã đơn hàng')
                        ->setCellValue('C1', 'Tên khách hàng')
                        ->setCellValue('D1', 'Trạng thái đơn hàng')
                        ->setCellValue('E1', 'Giá trị đơn hàng')
                        ->setCellValue('F1', 'Chiết khấu')
                        ->setCellValue('G1', 'Chi phí vận chuyển')
                        ->setCellValue('H1', 'Tổng phải thanh toán')
                        ->setCellValue('I1', 'Hình thức thanh toán');
            $arr_payment = array(0=>"COD",1=>"Chuyển khoản");
            $arr_type = $this->lib->get_config_define('status','order');
            $i=2;
            $order = 0;
            foreach($result as $row){
                if($row->ID!=$order){
                    $order = $row->ID; 
                    $status = isset($arr_type[$row->Status]) ? $arr_type[$row->Status] : "--" ;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A'.$i, date('d/m/Y',strtotime($row->Ngaydathang)))
                            ->setCellValue('B'.$i, $row->MaDH)
                            ->setCellValue('C'.$i, $row->Name)
                            ->setCellValue('D'.$i, $status)
                            ->setCellValue('E'.$i, $row->Total)
                            ->setCellValue('F'.$i, $row->Chietkhau)
                            ->setCellValue('G'.$i, $row->Chiphi)
                            ->setCellValue('H'.$i, $row->Total-$row->Chietkhau+$row->Chiphi)
                            ->setCellValue('I'.$i, $arr_payment[$row->Payment]);
                    $objPHPExcel->getActiveSheet()->getStyle("A".$i.":I$i")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => "4E90CA"
                        )
                    ));
                    $i++;
                    $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('C'.$i, "Mã sản phẩm")
                            ->setCellValue('D'.$i, "Tên sản phẩm")
                            ->setCellValue('E'.$i, "Giá bán")
                            ->setCellValue('F'.$i, "Chiết khấu")
                            ->setCellValue('G'.$i, "Giá sau chiết khấu")
                            ->setCellValue('H'.$i, "Số lượng")
                            ->setCellValue('I'.$i, "Tổng cộng");
                    $objPHPExcel->getActiveSheet()->getStyle("C".$i.":I$i")->getFill()->applyFromArray(array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'startcolor' => array(
                            'rgb' => "b8d1e6"
                        )
                    ));
                    $i++;
                }
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('C'.$i, $row->MaSP)
                        ->setCellValue('D'.$i, $row->Title)
                        ->setCellValue('E'.$i, $row->Price+$PriceDown)
                        ->setCellValue('F'.$i, $row->PriceDown)
                        ->setCellValue('G'.$i, $row->Price)
                        ->setCellValue('H'.$i, $row->Amount)
                        ->setCellValue('I'.$i, $row->Price*$row->Amount);
                $i++;
            }

            $objPHPExcel->getActiveSheet()->setTitle('RESULT_DATA');
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="DATA.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            header ('Cache-Control: cache, must-revalidate');
            header ('Pragma: public');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();
            $objWriter->save('php://output');
            exit;
        }else{
            echo "Not found data !";
        }
    }

    public function export_warehouse($orderid=0,$redirect=0){
        $result = $this->db->query("select * from ttp_report_export_warehouse where OrderID=".$orderid)->row();
        if(!$result){
            $order = $this->db->query("select * from ttp_report_order where ID=".$orderid)->row();
            if($order){
                $TKNO = '.................' ;
                $TKCO = '.................' ;
                $KPP = '' ;
                $hinhthucxuatkho = 1 ;
                $data = array(
                    'Lydoxuatkho'=>"Bán hàng tại pos",
                    'TKNO'       =>$TKNO,
                    'TKCO'       =>$TKCO,
                    'KPP'        =>$KPP,
                    'KhoID'      =>$order->KhoID,
                    'Hinhthucxuatkho'=>$hinhthucxuatkho
                );
                
                $thismonth = date('m',time());
                $thisyear = date('Y',time());
                $type = $order->OrderType==0 ? ' and TypeExport=0' : '' ;
                $type = $order->OrderType==1 ? ' and TypeExport=1' : $type ;
                $type = $order->OrderType==2 ? ' and TypeExport=2' : $type ;
                $type = $order->OrderType==3 ? ' and TypeExport=3' : $type ;
                $type = $order->OrderType==4 ? ' and TypeExport=4' : $type ;
                $type = $order->OrderType==5 ? ' and TypeExport=5' : $type ;
                $max = $this->db->query("select count(1) as max from ttp_report_export_warehouse where MONTH(Ngayxuatkho)=$thismonth and YEAR(Ngayxuatkho)=$thisyear and Hinhthucxuatkho=$hinhthucxuatkho $type")->row();
                $max = $max ? $max->max + 1 : 1 ;
                $thisyear = date('y',time());
                if($order->OrderType==0){
                    if($hinhthucxuatkho==2){
                        $thisyear = date('Y',time());
                        $hinhthucxuatkho = $hinhthucxuatkho==2 ? "TD" : $hinhthucxuatkho ;
                        if($thisyear=='2016' && $thismonth=='03'){
                            $max = $max+46;
                        }
                        $thisyear = date('y',time());
                        $max = "XKLO".$thisyear.$thismonth.".".str_pad($max, 5, '0', STR_PAD_LEFT);
                    }else{
                        $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                        $max = "BHOL".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    }
                    $data['TypeExport']=0;
                }
                if($order->OrderType==1 || $order->OrderType==2){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    if($order->OrderType==1){
                        $max = "BHGT".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                        $data['TypeExport']=1;
                    }
                    if($order->OrderType==2){
                        $thisyear1 = date('Y',time());
                        if($thisyear1=='2016' && $thismonth=='04'){
                            $max = $max-3;
                        }
                        $max = "BHMT".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                        $data['TypeExport']=2;
                    }
                }
                if($order->OrderType==3){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    $max = "BHGS".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    $data['TypeExport']=3;
                }
                if($order->OrderType==4){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    $max = "BHTD".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    $data['TypeExport']=4;
                }
                if($order->OrderType==5){
                    $hinhthucxuatkho = $hinhthucxuatkho==1 ? "TA" : "NB" ;
                    $max = "BHNB".$thisyear.$thismonth.".$hinhthucxuatkho.".str_pad($max, 5, '0', STR_PAD_LEFT);
                    $data['TypeExport']=5;
                }
                $data['OrderID']    = $orderid;
                $data['MaXK']       = $max;
                $data['Ngayxuatkho']= date("Y-m-d H:i:s",time());
                $data['UserID']     = $this->user->ID;
                $this->db->insert("ttp_report_export_warehouse",$data);
            }
        }
        if($redirect==0){
            redirect(ADMINPATH."/report/import/lapphieuxuatkho/".$orderid);
        }
    }

    public function overview(){
        $_GET['startday'] = isset($_GET['startday']) ? $_GET['startday'] : date('01/m/Y',time()) ;
        $startday = explode('/',$_GET['startday']);
        $startday = $startday[2].'-'.$startday[1].'-'.$startday[0];
        $_GET['stopday'] = isset($_GET['stopday']) ? $_GET['stopday'] : date('d/m/Y',time()) ;
        $stopday = explode('/',$_GET['stopday']);
        $stopday = $stopday[2].'-'.$stopday[1].'-'.$stopday[0];
        $result = $this->db->query("select a.ID,a.Status,a.Total,a.Chiphi,a.Chietkhau,b.Amount,b.ProductsID,b.Total as DetailsTotal,c.Title,c.MaSP,c.Donvi from ttp_report_order a,ttp_report_orderdetails b,ttp_report_products c where a.ID=b.OrderID and b.ProductsID=c.ID and date(a.Ngaydathang)>='".$startday."' and date(a.Ngaydathang)<='".$stopday."' and a.KhoID=".$this->WarehouseID)->result();
        $this->template->add_title('Sales | POS Tools');
        $this->template->write_view('content','admin/sales_report_overview',array(
            'data'=>$result,
            'startday'  => $startday,
            'stopday'   => $stopday
        ));
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Báo cáo','function'=>'<a>Báo cáo bán hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'overview'));
        $this->template->render();
    }

    public function change_branch(){
        $id = isset($_POST['id']) ? (int)$_POST['id'] : 1 ;
        $this->session->set_userdata('branch',$id);
    }

    public function bill(){
        $this->template->add_title('Bill | POS Tools');
        $page = $this->uri->segment(5);
        $start = is_numeric($page) ? $page : 0;
        if(!is_numeric($start)) $start=0;
        $limit_str = "limit $start,$this->limit";
        $nav = $this->db->query("select count(1) as nav from ttp_report_order where KhoID=".$this->WarehouseID)->row();
        $nav = $nav ? $nav->nav : 0;
        $result = $this->db->query("select * from ttp_report_order where KhoID=".$this->WarehouseID." order by ID DESC ".$limit_str)->result();
        $data = array(
            'data'  =>$result,
            'total' =>$nav,
            'start' =>$start,
            'limit' =>$this->limit,
            'nav'   =>$this->lib->nav(base_url().ADMINPATH.'/pos/pos_sales/bill',5,$nav,$this->limit)
        );
        $this->template->write_view('content','admin/sales_bill',$data);
        $this->template->write_view('header','admin/header',array('user'=>$this->user,'controllers'=>'Giao dịch','function'=>'<a>Hóa đơn bán hàng</a>'));
        $this->template->write_view('sitebar','admin/sitebar',array('user'=>$this->user,'controllers'=>$this->classname,'function'=>'bill'));
        $this->template->render();
    }

    public function draftorder(){
        $txt = time();
        file_put_contents('log/pos/sales/draft_order_'.$txt.'.txt',json_encode(array('transport'=>0,'chiphi'=>0,'products'=>array(),'customer'=>array('Name'=>'','Phone'=>'','Payment'=>0,'Note'=>'',"Active"=>"0",'Address'=>'','city'=>30,'district'=>32,'transfer'=>11,'transportid'=>1))));
        $response['error'] = 0;
        $response['order'] = $txt;
        echo json_encode($response);
    }

    public function remove_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : '' ;
        @unlink("log/pos/sales/draft_order_".$order.".txt");
        $files = scandir('log/pos/sales');
        $first = 0;
        if(count($files)>0){
            $i=1;
            foreach($files as $row){
                if($row!='.' && $row!='..'){
                    $name = explode('_',$row);
                    $name = str_replace('.txt','',$name[2]);
                    if($i==1){
                        $first = $name;
                    }
                    $i++;
                }
            }
        }
        echo $first;
    }

    public function get_products_by_barcode(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select ID from ttp_report_products where Barcode='$keywords'")->row();
        $response = array('error'=>1);
        if($result){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$result->ID])){
                $data['products'][$result->ID] = $data['products'][$result->ID] + 1;
            }else{
                $data['products'][$result->ID] = 1;
            }
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
            $response = array('error'=>0);
        }
        echo json_encode($response);
    }

    public function get_products_list(){
        $keywords = isset($_POST['keywords']) ? mysql_real_escape_string($_POST['keywords']) : '' ;
        $result = $this->db->query("select MaSP,Title,ID from ttp_report_products where (Title like '%$keywords%' or MaSP like '%$keywords%' or Barcode='$keywords') and VariantType<=1 limit 0,20")->result();
        if(count($result)>0){
            foreach ($result as $key => $value) {
                echo "<p><a onclick='sales_apply_products($value->ID)'>($value->MaSP) | $value->Title</a></p>";
            }
        }
    }

    public function reset_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['products'] = array();
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(!isset($data['products'])){
            $data['products'] = array();
        }
        if(!isset($data['customer'])){
            $data['customer'] = array('Name'=>'','Phone'=>'','Payment'=>0,'Note'=>'');
        }
        if(!isset($data['reduce'])){
            $data['reduce'] = 0;
        }
        if(isset($data['products'][$products])){
            $data['products'][$products] = $data['products'][$products]+1;
        }else{
            $data['products'][$products] = 1;
        }
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function remove_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        if(isset($data['products'][$products])){
            unset($data['products'][$products]);
        }
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function change_products_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 1 ;
        if($amount>0 && $products>0){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            if(isset($data['products'][$products])){
                $data['products'][$products] = $amount;
            }
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function change_products_price_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $products = isset($_POST['products']) ? (int)$_POST['products'] : 0 ;
        $price = isset($_POST['price']) ? $_POST['price'] : 0 ;
        if($price>=0 && $products>0){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['price'][$products] = $price;
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function load_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $this->load->view("admin/sales_draft_order_products",array('data'=>$data));
    }

    public function load_customers_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['transport'] = $data['transport'];
        echo json_encode($data['customer']);
    }

    public function apply_name_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $name = isset($_POST['name']) ? $_POST['name'] : '' ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['Name'] = $name;
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_phone_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '' ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['Phone'] = $phone;
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_customer_id(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $id = isset($_POST['id']) ? $_POST['id'] : '' ;
        $result = $this->db->query("select * from ttp_report_customer where ID=$id")->row();
        if($result){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['customer']['id'] = $id;
            $data['customer']['Name'] = $result->Name;
            $data['customer']['Phone'] = $result->Phone1;
            $data['customer']['Address'] = $result->Address;
            $response = array(
                'error'     =>0,
                'Name'      =>$result->Name,
                'Phone'     =>$result->Phone1,
                'Address'   =>$result->Address,
                'CityID'    =>$result->CityID,
                'DistrictID'=>$result->DistrictID,
                'GroupID'   =>$result->GroupID,
                'SourceID'  =>0,
                'KenhbanhangID'  =>0
            );
            $LastOrder = $this->db->query("select * from ttp_report_order where CustomerID=$id order by ID DESC")->row();
            if($LastOrder){
                $response['KenhbanhangID'] = $LastOrder->KenhbanhangID;
                $response['SourceID'] = $LastOrder->SourceID;
                $data['customer']['KenhbanhangID'] = $LastOrder->KenhbanhangID;
                $data['customer']['SourceID'] = $LastOrder->SourceID;
                $data['customer']['GroupID'] = $result->GroupID;
            }
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
        }else{
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['customer']['id'] = $id;
            $data['customer']['Name'] = '';
            $data['customer']['Phone'] = '';
            $data['customer']['Address'] = '';
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
            $response = array(
                'error'     => 1
            );
        }
        echo json_encode($response);
    }

    public function apply_address_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $address = isset($_POST['address']) ? $_POST['address'] : '' ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['Address'] = $address;
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_user_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $user = isset($_POST['user']) ? $_POST['user'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['transfer'] = $user;
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_transportid_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $transport = isset($_POST['transport']) ? $_POST['transport'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['transportid'] = $transport;
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_transport_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $transport = isset($_POST['transport']) ? $_POST['transport'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['transport'] = $transport;
        if($transport==1){
            $data['customer']['city'] = 30;
            $data['customer']['district'] = 32;
            $data['customer']['transfer'] = 11;
            $data['customer']['transportid'] = 1;
        }else{
            $data['customer']['transportid'] = 0;
        }
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function apply_district_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $district = isset($_POST['district']) ? $_POST['district'] : 0 ;
        $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
        $data = json_decode($data,true);
        $data['customer']['district'] = $district;
        file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
    }

    public function add_reduce_bill_draft(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $reduce = isset($_POST['reduce']) ? $_POST['reduce'] : 0 ;
        if($reduce>=0){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['reduce'] = $reduce;
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function add_chiphi_draftorder(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $chiphi = isset($_POST['chiphi']) ? $_POST['chiphi'] : 0 ;
        if($chiphi>=0){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $data['chiphi'] = $chiphi;
            file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
        }
    }

    public function print_order($id=0){
        $order = $this->db->query("select a.Name,a.Phone,a.Note,a.ID,a.MaDH,a.Ngaydathang,a.Total,a.Chietkhau,a.Chiphi,a.AddressOrder,b.Title as CityTitle,c.Title as DistrictTitle from ttp_report_order a,ttp_report_city b,ttp_report_district c where a.CityID=b.ID and a.DistrictID=c.ID and a.ID=$id")->row();
        if($order){
            $this->load->view("admin/sales_draft_order_print",array('data'=>$order));
        }
    }

    public function create_customer($name='',$Phone1='',$address='',$city=0,$district=0,$group=0){
        $customer = $this->db->query("select * from ttp_report_customer where Phone1='$Phone1'")->row();
        if($customer){
            $data_customer = array(
                'GroupID'   =>$group,
            );
            $this->db->where("ID",$customer->ID);
            $this->db->update("ttp_report_customer",$data_customer);
            $customer->IsNew = 1;
            return $customer;
        }else{
            $data_customer = array(
                'Name'      =>$name,
                'Address'   =>$address,
                'Phone1'    =>$Phone1,
                'UserID'    =>$this->user->ID,
                'GroupID'   =>$group,
                'DistrictID'=>$district,
                'CityID'    =>$city
            );
            $data_customer['Type'] = 0;
            $this->db->insert("ttp_report_customer",$data_customer);
            $CustomerID = $this->db->insert_id();
            $customer = $this->db->query("select * from ttp_report_customer where ID=$CustomerID")->row();
            $customer->IsNew = 0;
            return $customer;
        }
    }

    public function create_order(){
        $order = isset($_POST['order']) ? $_POST['order'] : '' ;
        $KenhbanhangID = isset($_POST['KenhbanhangID']) ? $_POST['KenhbanhangID'] : 1 ;
        $SourceID = isset($_POST['SourceID']) ? $_POST['SourceID'] : 1 ;
        $GroupID = isset($_POST['GroupID']) ? $_POST['GroupID'] : 1 ;
        $name = isset($_POST['name']) ? $_POST['name'] : '' ;
        $phone = isset($_POST['phone']) ? $_POST['phone'] : '' ;
        $Address = isset($_POST['address']) ? $_POST['address'] : '' ;
        $note = isset($_POST['note']) ? $_POST['note'] : '' ;
        $payment = isset($_POST['payment']) ? $_POST['payment'] : 1 ;
        $chiphi = isset($_POST['chiphi']) ? (int)$_POST['chiphi'] : 0 ;
        $chiphi = $chiphi>0 ? $chiphi : 0 ;
        $WarehouseID = $this->WarehouseID;
        $response = array("error"=>1,"message"=>"Tạo đơn hàng không thành công !");
        if($name!='' && $phone!=''){
            $data = file_get_contents('log/pos/sales/draft_order_'.$order.'.txt');
            $data = json_decode($data,true);
            $CityID = isset($data['customer']['city']) ? $data['customer']['city'] : 30;
            $DistrictID = isset($data['customer']['district']) ? $data['customer']['district'] : 32;
            $customer = $this->create_customer($name,$phone,$Address,$CityID,$DistrictID,$GroupID);
            if(isset($data['products']) && count($data['products'])>0){
                $transport = isset($data['transport']) ? $data['transport'] : 0;
                $transportid = isset($data['customer']['transportid']) ? $data['customer']['transportid'] : 0;
                $transfer = isset($data['customer']['transfer']) ? $data['customer']['transfer'] : 0;
                $monthuser = date("m",time());
                $yearuser = date("Y",time());
                $userorder = $this->db->query("select count(ID) as SL from ttp_report_order where MONTH(Ngaydathang)=$monthuser and YEAR(Ngaydathang)=$yearuser")->row();
                $yearuser = date("y",time());
                $userorder = $userorder ? $userorder->SL+1+$monthuser+$yearuser : 1+$monthuser+$yearuser ;
                $monyear = date('ym');
                $idbyUser = str_pad($userorder, 6, '0', STR_PAD_LEFT);
                $MaDH = 'DH'.$monyear."_".$idbyUser;
                $status = 0;
                $money = 0;
                if($transport==1){
                    if($Address==''){
                        $response = array("error"=>1,"message"=>"Tạo đơn hàng không thành công. Vui lòng điền địa chỉ giao hàng.");
                        echo json_encode($response);
                        return;
                    }
                    $status = 7;
                }else{
                    $Address = $customer->Address!='' ? $customer->Address : $Address ;
                    $money = 1;
                }
                $sql = array(
                    'MaDH'          => $MaDH,
                    'AddressOrder'  => $Address,
                    'UserID'        => $this->user->ID,
                    'CityID'        => $CityID,
                    'DistrictID'    => $DistrictID,
                    'CustomerID'    => $customer->ID,
                    'Note'          => $note,
                    'Ghichu'        => $note,
                    'SourceID'      => $SourceID,
                    'KenhbanhangID' => $KenhbanhangID,
                    'Name'          => $name,
                    'Phone'         => $phone,
                    'Status'        => $status,
                    'Chiphi'        => $chiphi,
                    'CustomerType'  => $customer->IsNew,
                    'TransportID'   => $transportid,
                    'Ngaydathang'   => date('Y-m-d H:i:s'),
                    'HistoryEdited' => date('Y-m-d H:i:s'),
                    'KhoID'         => $WarehouseID,
                    'SendSMS'       => 1,
                    'TransportMoney'=> $money
                );
                $this->db->insert("ttp_report_order",$sql);
                $orderid = $this->db->insert_id();
                $data_history = array(
                    'OrderID'   => $orderid,
                    'Thoigian'  => date('Y-m-d H:i:s'),
                    'Status'    => 0,
                    'Ghichu'    => 'Tạo đơn hàng từ pos',
                    'UserID'    => $this->user->ID
                );
                $this->db->insert('ttp_report_orderhistory',$data_history);
                
                $total_order = 0;
                $total_products = 0;
                $total_reduce = 0;
                if(isset($data['reduce'])){
                    $total_reduce = $data['reduce'];
                }

                $details_insert = array();
                $transfer_insert = array();
                $key = array_keys($data['products']);
                $total_products = count($key);
                $products = $this->db->query("select * from ttp_report_products where ID in(".implode(',',$key).")")->result();
                $this->db->query("update ttp_report_products set HasBuy=HasBuy+1 where ID in(".implode(',',$key).")");
                if(count($products)>0){
                    foreach ($products as $row) {
                        $amount = $data['products'][$row->ID];
                        $price = date('Y-m-d')>=date('Y-m-d',strtotime($row->SpecialStartday)) && date('Y-m-d')<=date('Y-m-d',strtotime($row->SpecialStopday)) ? $row->SpecialPrice : $row->Price ;
                        $diff = 0;
                        if(isset($data['price'][$row->ID])){
                            $diff = $price-$data['price'][$row->ID];
                            $price = $data['price'][$row->ID];
                        }
                        $total = $amount*$price;
                        $total_order += $total;
                        $shipment = $this->db->query("select ID from ttp_report_shipment where ProductsID=$row->ID")->row();
                        $shipment = $shipment ? $shipment->ID : 0 ;
                        $details_insert[] = "($orderid,$row->ID,$price,$amount,$total,$shipment,$diff)";
                        $time = date('Y-m-d H:i:s');
                        if($transport==1){
                            $transfer_insert[] = "($orderid,$row->ID,$row->RootPrice,$amount,$shipment,1,'$time',".$this->user->ID.",'$time',".$this->user->ID.",$transfer,$WarehouseID)";
                        }else{
                            $this->change_inventory($row->ID,$shipment,$WarehouseID,$amount);
                        }
                    }
                    if(count($details_insert)>0){
                        $sql = "insert into ttp_report_orderdetails(OrderID,ProductsID,Price,Amount,Total,ShipmentID,PriceDown) values".implode(',',$details_insert);
                        $this->db->query($sql);
                        $this->db->query("update ttp_report_products set HasBuy=HasBuy+1 where ID in(".implode(',',$key).")");
                    }
                    if(count($transfer_insert)){
                        $sql = "insert into ttp_report_order_send_supplier(OrderID,ProductsID,Price,Amount,ShipmentID,Status,TimeRequest,UserRequest,TimeAccept,UserAccept,UserReciver,WarehouseID) values".implode(',',$transfer_insert);
                        $this->db->query($sql);
                    }
                    $this->db->query("update ttp_report_order set Payment=$payment,Total=$total_order,SoluongSP=$total_products,Chietkhau=$total_reduce where ID=$orderid");
                }
                $this->export_warehouse($orderid,1);
                $response = array("error"=>0,"message"=>"Tạo đơn hàng thành công !","id"=>$orderid);
                $data["customer"]["Active"] = $orderid;
                $data["customer"]["GroupID"] = $customer->GroupID;
                $data["customer"]["KenhbanhangID"] = $KenhbanhangID;
                $data["customer"]["SourceID"] = $SourceID;
                file_put_contents('log/pos/sales/draft_order_'.$order.'.txt',json_encode($data));
            }else{
                $response = array("error"=>1,"message"=>"Tạo đơn hàng không thành công. Đơn hàng không có sản phẩm");
            }
        }
        echo json_encode($response);
    }

    public function drop_order($id=0){
        $result = $this->db->query("select * from ttp_report_order where ID=$id and UserID=".$this->user->ID)->row();
        if($result){
            if($result->Status==0){
                $details = $this->db->query("select * from ttp_report_orderdetails where OrderID=$id")->result();
                if(count($details)>0){
                    foreach ($details as $row) {
                        $this->release_change_inventory($row->ProductsID,$row->ShipmentID,$this->WarehouseID,$row->Amount);
                    }
                }
            }
            $this->db->query("update ttp_report_order set Status=1 where ID=".$id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function change_inventory($ProductsID,$ShipmentID,$WarehouseID,$Amount){
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1");
        if($check_exists){
            if($check_exists->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=Available-$Amount,OnHand=OnHand-$Amount,LastEdited=1 where ID=$check_exists->ID");
            }else{
                $Available = $check_exists->Available-$Amount;
                $OnHand = $check_exists->OnHand-$Amount;
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
            }
        }else{
            $Available = -$Amount;
            $OnHand = -$Amount;
            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
        }
    }

    public function release_change_inventory($ProductsID,$ShipmentID,$WarehouseID,$Amount){
        $check_exists = $this->db->query("select ID,DateInventory,Available,OnHand from ttp_report_inventory where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1 order by DateInventory DESC limit 0,1")->row();
        $this->db->query("update ttp_report_inventory set LastEdited=0 where ShipmentID=$ShipmentID and ProductsID=$ProductsID and WarehouseID=$WarehouseID and LastEdited=1");
        if($check_exists){
            if($check_exists->DateInventory==date('Y-m-d')){
                $this->db->query("update ttp_report_inventory set Available=Available+$Amount,OnHand=OnHand+$Amount,LastEdited=1 where ID=$check_exists->ID");
            }else{
                $Available = $check_exists->Available+$Amount;
                $OnHand = $check_exists->OnHand+$Amount;
                $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
            }
        }else{
            $Available = $Amount;
            $OnHand = $Amount;
            $this->db->query("insert into ttp_report_inventory(ProductsID,OnHand,Available,WarehouseID,ShipmentID,DateInventory,LastEdited) values($ProductsID,$OnHand,$Available,$WarehouseID,$ShipmentID,'".date('Y-m-d')."',1)");
        }
    }

    public function active_managermoney(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $state = isset($_POST['state']) ? $_POST['state'] : 0 ;
        if($state==0){
            $this->db->query("update ttp_report_order set ManagerMoney=1 where ID=$order");
            echo '<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Đã thu</a>';
        }else{
            $this->db->query("update ttp_report_order set ManagerMoney=0 where ID=$order");
            echo '<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa thu</a>';
        }
    }

    public function active_financemoney(){
        $order = isset($_POST['order']) ? $_POST['order'] : 0 ;
        $state = isset($_POST['state']) ? $_POST['state'] : 0 ;
        if($state==0){
            $this->db->query("update ttp_report_order set FinanceMoney=1 where ID=$order");
            echo '<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Đã thu</a>';
        }else{
            $this->db->query("update ttp_report_order set FinanceMoney=0 where ID=$order");
            echo '<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa thu</a>';
        }
    }

    public function get_customer_by_name_phone(){
        $name = isset($_POST['name']) ? mysql_real_escape_string($_POST['name']) : '' ;
        $phone = isset($_POST['phone']) ? mysql_real_escape_string($_POST['phone']) : '' ;
        if($name!='' || $phone!=''){
            $where = array();
            if($phone!=''){
                $where[] = "(a.Phone1 like '%$phone%' or a.Phone2 like '%$phone%')";
            }
            if($name!=''){
                $where[] = "a.Name like '%$name%'";
            }
            $where = count($where)>0 ? implode(' and ',$where) : "";
            if($where!=''){
                $result = $this->db->query("select a.*,b.Title as GroupTitle from ttp_report_customer a,ttp_report_customer_group b where a.GroupID=b.ID and $where limit 0,200")->result();
            }else{
                $result = $this->db->query("select a.*,b.Title as GroupTitle from ttp_report_customer a,ttp_report_customer_group b where a.GroupID=b.ID limit 0,200")->result();
            }
            if(count($result)>0){
                echo "<table><tr><th>Tên khách hàng</th><th>Số điện thoại</th><th>Địa chỉ</th><th>Nhóm khách hàng</th></tr>";
                foreach($result as $row){
                    $Phone1 = substr_replace($row->Phone1, str_repeat("x", 6), 0, 6);
                    if($row->Name!=''){
                    echo "<tr>";
                    echo "<td class='name_td'><a onclick='sales_select_this_custom(this,$row->ID)'>$row->Name</a></td>";
                    echo "<td><a onclick='sales_select_this_custom(this,$row->ID)'>".$Phone1."</a></td>";
                    echo "<td><a onclick='sales_select_this_custom(this,$row->ID)'>".$row->Address."</a></td>";
                    echo '<td style="width: 130px;"><a onclick="sales_select_this_custom(this,'.$row->ID.')">'.$row->GroupTitle.'</a></td>';
                    echo "</tr>";
                    }
                }
                echo "</table>";
            }
        }else{
            echo "";
        }

    }

    public function get_order_by_customer(){
        $id = isset($_POST['id']) ? (int)$_POST['id'] : 0 ;
        $result = $this->db->query("select * from ttp_report_order where CustomerID=$id order by ID DESC")->result();
        if(count($result)>0){
            $arr = $this->lib->get_config_define('status','order');
            echo "<table><tr><th>Mã DH</th><th>Giá trị</th><th>Đã giảm</th><th>Địa chỉ giao</th><th>Ngày đặt</th><th>Trạng thái</th></tr>";
            $links = ADMINPATH."/report/import_order/edit";
            foreach($result as $row){
                echo "<tr>";
                echo "<td><a target='_blank' href='$links/$row->ID'>$row->MaDH</a></td>";
                echo "<td><a target='_blank' href='$links/$row->ID'>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</a></td>";
                echo "<td><a target='_blank' href='$links/$row->ID'>".number_format($row->Chietkhau)."</a></td>";
                echo "<td style='width: 230px;'><a target='_blank' href='$links/$row->ID'>$row->AddressOrder</a></td>";
                echo "<td><a target='_blank' href='$links/$row->ID'>$row->Ngaydathang</a></td>";
                echo "<td><a target='_blank' href='$links/$row->ID'>".$arr[$row->Status]."</a></td>";
                echo "</tr>";
            }
            echo "</table>";
        }
    }

}
?>
