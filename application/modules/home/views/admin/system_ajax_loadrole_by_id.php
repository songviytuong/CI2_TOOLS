<script type="text/javascript" src="<?php echo base_url() ?>public/admin/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>public/admin/js/script.js"></script>
<?php 
    $result = $this->db->query("select * from ttp_sitetree")->result();
    if(count($result)>0){
        echo '<table class="table table-striped responsive-utilities jambo_table">';
        echo '<thead>
                <tr class="headings">
                    <th>STT</th>
                    <th>Module </th>
                    <th>Read</th>
                    <th>Write</th>
                    <th>Modify</th>
                    <th>Delete</th>
                    <th>Full</th>
                </tr>
            </thead><tbody>';
        $i=0;
        $permission = $data ? json_decode($data->DetailRole,true) : "" ;
        foreach($result as $row){
            $i++;
            $read='';
            $write='';
            $modify='';
            $delete='';
            $full='';
            $temp=0;
            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('r',$permission[$row->Classname])){
                $read="checked='checked'";
                $temp++;
            }
            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('w',$permission[$row->Classname])){
                $write="checked='checked'";
                $temp++;
            }
            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('m',$permission[$row->Classname])){
                $modify="checked='checked'";
                $temp++;
            }
            if(is_array($permission) && isset($permission[$row->Classname]) && @in_array('d',$permission[$row->Classname])){
                $delete="checked='checked'";
                $temp++;
            }
            if($temp==4){
                $full="checked='checked'";
            }
        ?>
        
            <tr class="even pointer">
                <td class="a-center ">
                    <?php echo $i; ?>
                </td>
                <td><?php echo $row->Title; ?></td>
                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="r" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $read ?> /></td>
                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="w" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $write ?> /></td>
                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="m" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $modify ?> /></td>
                <td><input type='checkbox' name="<?php echo $row->Classname ?>[]" value="d" class="<?php echo "checkbox_$row->Classname" ?>" <?php echo $delete ?> /></td>
                <td><input type='checkbox' class='checkfull' data="<?php echo $row->Classname ?>" <?php echo $full ?> /></td>
                
            </tr>

        <?php 
        }
        echo "</tbody></table>";
    }
?>