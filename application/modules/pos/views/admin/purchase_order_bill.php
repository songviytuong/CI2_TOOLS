<div class="sub_menu height">
	<div class="find_record">
		<?php 
		$to = $start+$limit>$total ? $total : $start+$limit ;
		?>
		Tìm thấy <?php echo number_format($total) ?></b> phiếu mua hàng.
		<a href="<?php echo ADMINPATH.'/pos/pos_inventory_import/export_data' ?>"><i class="fa fa-download"></i> Xuất file excel toàn bộ tháng này</a> | <a href="<?php echo ADMINPATH.'/pos/pos_inventory_import/export_data/1' ?>"><i class="fa fa-download"></i> Xuất file excel ngày hôm nay</a>
		<a class="btn btn-danger pull-right" style="margin-right:20px" href="<?php echo base_url().ADMINPATH.'/pos/pos_inventory_import/create' ?>"><i class="fa fa-plus"></i> Thực hiện nhập hàng</a>
	</div>
</div>
<div class="containner height">
	<div class="table_div">
		<table class="table">
			<tr>
				<th>STT</th>
				<th>Mã PO</th>
				<th style="width:200px;">Số chứng từ</th>
				<th style="width:120px;">Nhà cung cấp</th>
				<th>Giá trị</th>
				<th style="width:120px;">Đã giảm</th>
				<th>Ghi chú</th>
				<th>Ngày nhập</th>
				<th>Trạng thái</th>
			</tr>
			<tr>
				<?php 
				$arr = array(
					5=>'<a class="status-button"><i class="fa fa-circle text-success" aria-hidden="true"></i> Đã nhập</a>',
					0=>'<a class="status-button"><i class="fa fa-circle text-default" aria-hidden="true"></i> Nháp</a>'
				);
				if(count($data)>0){
					$i=$start+1;
					foreach ($data as $row) {
						$row->Status = isset($arr[$row->Status]) ? $arr[$row->Status] : '--' ;
						$row->Note = $row->Note=='' ? '--' : $row->Note ;
						$row->HardCode = $row->HardCode!='' ? $row->HardCode : '--' ;
						echo '<tr>
						<td><a>'.$i.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.$row->POCode.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.$row->HardCode.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.$row->Title.'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.number_format($row->TotalPrice-$row->Reduce).'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.number_format($row->Reduce).'</a></td>
						<td><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.$row->Note.'</a></td>
						<td style="width:120px;"><a href="'.ADMINPATH.'/pos/pos_inventory_import/view_order/'.$row->ID.'">'.date('d/m/Y H:i',strtotime($row->Created)).'</a></td>
						<td style="width:120px;">'.$row->Status.'</td>
						</tr>';
						$i++;
					}
				}
				?>
			</tr>
		</table>
		<?php echo str_replace('href="#"','',$nav); ?>
	</div>
</div>