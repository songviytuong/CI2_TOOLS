<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <?php echo $_title; ?>
        <link rel="shortcut icon" href="<?= OPERATION_URL; ?>/favicon.png">
        <link rel="icon" href="<?= OPERATION_URL; ?>/favicon.png" type="image/x-icon">
        <link href="<?= OPERATION_URL; ?>/vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
        <link href="<?= OPERATION_URL; ?>/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= OPERATION_URL; ?>/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
        <link href="<?= OPERATION_URL; ?>/dist/css/style.css" rel="stylesheet" type="text/css">
        <?php echo $_styles; ?>
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <!-- /Preloader -->
        <div class="wrapper">
            <?php
            $this->load->view('inc/top_menu');
            ?>

            <?php
            $this->load->view('inc/sidebar_left');
            ?>

            <?php
            $this->load->view('inc/sidebar_right');
            ?>

            <?php
            $this->load->view('inc/setting_panel');
            ?>

            <?php echo $content; ?>

            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/jquery/dist/jquery.min.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

            <!-- Data table JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

            <!-- Slimscroll JavaScript -->
            <script src="<?= OPERATION_URL; ?>/dist/js/jquery.slimscroll.js"></script>

            <!-- simpleWeather JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/moment/min/moment.min.js"></script>
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
            <script src="<?= OPERATION_URL; ?>/dist/js/simpleweather-data.js"></script>

            <!-- Progressbar Animation JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

            <!-- Fancy Dropdown JS -->
            <script src="<?= OPERATION_URL; ?>/dist/js/dropdown-bootstrap-extended.js"></script>

            <!-- Sparkline JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

            <!-- Owl JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

            <!-- ChartJS JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/chart.js/Chart.min.js"></script>

            <!-- Morris Charts JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/raphael/raphael.min.js"></script>
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/morris.js/morris.min.js"></script>
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

            <!-- Switchery JavaScript -->
            <script src="<?= OPERATION_URL; ?>/vendors/bower_components/switchery/dist/switchery.min.js"></script>

            <!-- Init JavaScript -->
            <script src="<?= OPERATION_URL; ?>/dist/js/init.js"></script>
            <script src="<?= OPERATION_URL; ?>/dist/js/dashboard-data.js"></script>

            <?php echo $_scripts; ?>

        </div>
        <!-- /#wrapper -->
    </body>
</html>
