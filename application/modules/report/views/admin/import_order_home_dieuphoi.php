<?php 

$numday_start_group1 = (strtotime(date('Y-m-d',time())) - strtotime($startday))/(3600*24);
$numday_stop_group1 = (strtotime(date('Y-m-d',time())) - strtotime($stopday))/(3600*24);

$total_tienhang = 0;
$total_chiphi   = 0;
$total_giatri   = 0;
$total_huy      = 0;
$month          = array();
$dayofmonth     = array();
$dayofmonth_real= array();
$result_fillter = array();
$fill_temp_value="";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner">
    <div class="import_select_progress">
        <div class="block1">
            <h1>DANH SÁCH ĐƠN HÀNG</h1>
        </div>
        <div class="block2">
            <div id="reportrange" class="list_div">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                <span></span> <b class="caret"></b>
            </div>
        </div>
    </div>
    <div class="import_orderlist">
        <div class="block2 row">
            <div class="block_2_1 col-xs-6">
                <a class="btn btn-danger" onclick="load_data_from_url(this,'<?php echo base_url().ADMINPATH."/report/import_order/load_box_sync_transport_status" ?>')"><i class="fa fa-refresh" aria-hidden="true"></i> Kiểm tra trạng thái</a>
            </div>
            <div class="block_2_2 col-xs-6">
                <button class="btn btn-default pull-right" style="margin-left:5px;" onclick="showtools()">Advanced Filter <b class="caret"></b></button>
                <a class="btn btn-primary" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export</a>
                <a class="btn btn-primary" role="button" onclick="import_data(this)"><i class="fa fa-upload"></i> Import</a>
            </div>
        </div>
        <div class="clear"></div>
        <div class="export_tools">
            <a href="<?php echo base_url().ADMINPATH."/report/import/export_order_logistics_excute" ?>">Đơn hàng đã xử lý</a>
            <a href="<?php echo base_url().ADMINPATH."/report/import/export_by_resultdata?TypeExport=1" ?>">Dữ liệu đang xem</a>
            <a href="<?php echo base_url().ADMINPATH."/report/import/data_export?transport=3" ?>">Mẫu Viettel</a>
            <a href="<?php echo base_url().ADMINPATH."/report/import/data_export?transport=2" ?>">Mẫu GHN</a>
            <a href="<?php echo base_url().ADMINPATH."/report/import/data_export?transport=1" ?>">Mẫu GoldTimes</a>
        </div>
        <div class="import_tools">
            <form action="<?php echo base_url().ADMINPATH."/report/import/update_status" ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="Image_upload" />
                <button type="submit">Upload</button>
            </form>
        </div>
        <div class="filltertools">
            <form action="<?php echo $base_link."setfillter" ?>" method="post">
                <?php 
                    $arr_fieldname = array(0=>"c.Name",1=>"a.CustomerType",2=>"e.ID",3=>"d.ID",4=>"b.ID",5=>"a.SoluongSP",6=>"a.Total",7=>"a.Chiphi",8=>"a.Status",9=>"a.UserID",10=>"a.TransportID",13=>"f.MaXK");
                    $arr_oparation = array(0=>'like',1=>'=',2=>'!=',3=>'>',4=>'<',5=>'>=',6=>'<=');
                    $arr_showfieldname = array(0=>"Tên khách hàng",1=>"Loại khách hàng",2=>"Khu vực",3=>"Tỉnh thành",4=>"Quận huyện",5=>"Số lượng sản phẩm",6=>"Tổng tiền hàng",7=>"Chi phí vận chuyển",8=>"Trạng thái đơn hàng",9=>"Nhân viên khởi tạo",10=>"Đối tác vận chuyển",13=>"Mã phiếu xuất kho");
                    $arr_showoparation = array(0=>'có chứa',1=>'bằng',2=>'khác',3=>'lớn hơn',4=>'nhỏ hơn',5=>'lớn hơn hoặc bằng',6=>'nhỏ hơn hoặc bằng');
                    $fill_data_arr = explode(" and ",$fill_data);
                    if(count($fill_data_arr)>0 && $fill_data!=''){
                        $temp_tools=0;
                        foreach($fill_data_arr as $row){
                            $param = explode(' ',$row,3);
                            $value_field = isset($param[0]) ? array_search($param[0],$arr_fieldname) : 10 ;
                            $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '' ;
                            $param_oparation=isset($param[1]) ? array_search($param[1],$arr_oparation) : 10 ;
                            $param_value = isset($param[2]) ? $param[2] : '' ;
                            $param_value = str_replace("\'","",$param_value);
                            $param_value = str_replace("'","",$param_value);
                            $param_value = str_replace("%","",$param_value);
                ?>
                <div class="row <?php echo $temp_tools==0 ? "base_row" : "" ; ?>">
                    <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools==0 ? "Chọn" : "Và" ; ?></label></div>
                    <div class="list_toolls col-xs-2">
                        <input type="hidden" class="FieldName" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0],$arr_fieldname) : '' ; ?>" />
                        <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field!='' ? $param_field : "Tên field" ; ?> <b class="caret"></b></label>
                        <ul class="dropdownbox">
                            <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                    <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                <ul>
                                    <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                    <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                    <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                </ul>
                            </li>
                            <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                <ul>
                                    <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                    <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                    <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                    <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                    <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                                    <li><a onclick="setfield(this,11,'maphieuxuatkho')">Mã phiếu xuất kho</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="list_toolls reciveroparation col-xs-2">
                        <select class="oparation form-control" name="FieldOparation[]">
                            <option value="1" <?php echo $param_oparation==1 ? "selected='selected'" : '' ; ?>>Bằng</option>
                            <option value="0" <?php echo $param_oparation==0 ? "selected='selected'" : '' ; ?>>Có chứa</option>
                            <option value="2" <?php echo $param_oparation==2 ? "selected='selected'" : '' ; ?>>Khác</option>
                            <option value="3" <?php echo $param_oparation==3 ? "selected='selected'" : '' ; ?>>Lớn hơn</option>
                            <option value="4" <?php echo $param_oparation==4 ? "selected='selected'" : '' ; ?>>Nhỏ hơn</option>
                            <option value="5" <?php echo $param_oparation==5 ? "selected='selected'" : '' ; ?>>Lớn hơn hoặc bằng</option>
                            <option value="6" <?php echo $param_oparation==6 ? "selected='selected'" : '' ; ?>>Nhỏ hơn hoặc bằng</option>
                        </select>
                    </div>
                    <div class="list_toolls reciverfillter col-xs-3">
                        <?php 
                        if($value_field==2 || $value_field==3 || $value_field==4){
                            if($value_field==2){
                                $result = $this->db->query("select * from ttp_report_area order by Title ASC")->result();    
                            }
                            if($value_field==3){
                                $result = $this->db->query("select * from ttp_report_city order by Title ASC")->result();    
                            }
                            if($value_field==4){
                                $result = $this->db->query("select * from ttp_report_district order by Title ASC")->result();    
                            }
                            if(count($result)>0){
                                echo "<select name='FieldText[]' class='form-control'>";
                                foreach($result as $row){
                                    $selected = $param_value==$row->ID ? "selected='selected'" : '' ;
                                    $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                    echo "<option value='$row->ID' $selected>$row->Title</option>";
                                }
                                echo "</select>";
                            }
                        }elseif ($value_field==8) {
                            echo "<select name='FieldText[]' class='form-control'>";
                            echo $param_value==9 ? "<option value='9' selected='selected'>Đơn hàng chuyển sang nv điều phối</option>" : "<option value='9'>Đơn hàng chuyển sang nv điều phối</option>" ;
                            echo $param_value==8 ? "<option value='8' selected='selected'>Đơn hàng bị trả về</option>" : "<option value='8'>Đơn hàng bị trả về</option>" ;
                            echo $param_value==7 ? "<option value='7' selected='selected'>Chuyển sang bộ phận giao hàng</option>" : "<option value='7'>Chuyển sang bộ phận giao hàng</option>" ;
                            echo $param_value==6 ? "<option value='6' selected='selected'>Đơn hàng bị trả về từ kế toán</option>" : "<option value='6'>Đơn hàng bị trả về từ kế toán</option>" ;
                            echo $param_value==5 ? "<option value='5' selected='selected'>Đơn hàng chờ kế toán duyệt</option>" : "<option value='5'>Đơn hàng chờ kế toán duyệt</option>" ;
                            echo $param_value==4 ? "<option value='4' selected='selected'>Đơn hàng bị trả về từ kho</option>" : "<option value='4'>Đơn hàng bị trả về từ kho</option>" ;
                            echo $param_value==3 ? "<option value='3' selected='selected'>Đơn hàng mới chờ kho duyệt</option>" : "<option value='3'>Đơn hàng mới chờ kho duyệt</option>" ;
                            echo $param_value==2 ? "<option value='2' selected='selected'>Đơn hàng nháp</option>" : "<option value='2'>Đơn hàng nháp</option>" ;
                            echo $param_value==0 ? "<option value='0' selected='selected'>Đơn hàng thành công</option>" : "<option value='0'>Đơn hàng thành công</option>" ;
                            echo $param_value==1 ? "<option value='1' selected='selected'>Đơn hàng hủy</option>" : "<option value='1'>Đơn hàng hủy</option>" ;
                            $fill_temp_value = $param_value==9 ? "Đơn hàng chuyển sang nv điều phối" : $fill_temp_value ;
                            $fill_temp_value = $param_value==8 ? "Đơn hàng bị trả về" : $fill_temp_value ;
                            $fill_temp_value = $param_value==7 ? "Chuyển sang bộ phận giao hàng" : $fill_temp_value ;
                            $fill_temp_value = $param_value==6 ? "Đơn hàng bị trả về từ kế toán" : $fill_temp_value ;
                            $fill_temp_value = $param_value==5 ? "Đơn hàng chờ kế toán duyệt" : $fill_temp_value ;
                            $fill_temp_value = $param_value==4 ? "Đơn hàng bị trả về từ kho" : $fill_temp_value ;
                            $fill_temp_value = $param_value==3 ? "Đơn hàng mới chờ kho duyệt" : $fill_temp_value ;
                            $fill_temp_value = $param_value==2 ? "Đơn hàng nháp" : $fill_temp_value ;
                            $fill_temp_value = $param_value==1 ? "Đơn hàng hủy" : $fill_temp_value ;
                            $fill_temp_value = $param_value==0 ? "Đơn hàng thành công" : $fill_temp_value ;
                            echo "</select>";
                        }elseif ($value_field==1) {
                            echo "<select name='FieldText[]' class='form-control'>
                                    <option value='0'>Khách hàng mới</option>
                                    <option value='1'>Khách hàng cũ</option>
                                </select>";
                            $fill_temp_value = $param_value==1 ? "Khách hàng cũ" : $fill_temp_value ;
                            $fill_temp_value = $param_value==0 ? "Khách hàng mới" : $fill_temp_value ;
                        }elseif ($value_field==10) {
                            echo "<select name='FieldText[]' class='form-control'>";
                            $transport = $this->db->query("select * from ttp_report_transport")->result();
                            foreach($transport as $row){
                                $selected = $param_value==$row->ID ? "selected='selected'" : '' ; 
                                $fill_temp_value = $param_value==$row->ID ? $row->Title : $fill_temp_value ;
                                echo "<option value='$row->ID' $selected>$row->Title</option>";
                            }
                            echo "</select>";
                        }else{
                            $fill_temp_value = $param_value ;
                            echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="'.$param_value.'" />';
                        }
                        ?>
                    </div>
                    <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                </div>
                <?php   
                            $temp_tools++;
                            $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':' ;
                            $result_fillter[] = $param_field." ".$showoparation." '<b>".$fill_temp_value."</b>'";
                        }
                    }else{
                ?>
                    <div class="row base_row">
                        <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                        <div class="list_toolls col-xs-2">
                            <input type="hidden" class="FieldName" name="FieldName[]" />
                            <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                            <ul class="dropdownbox">
                                <li><span onclick="showul(this)"><b class="caret"></b> Khách hàng</span>
                                    <ul>
                                        <li><a onclick="setfield(this,0,'tenkhachhang')">Tên khách hàng</a></li>
                                        <li><a onclick="setfield(this,1,'loaikhachhang')">Loại khách hàng</a></li>
                                    </ul>
                                </li>
                                <li><span onclick="showul(this)"><b class="caret"></b> Khu vực</span>
                                    <ul>
                                        <li><a onclick="setfield(this,2,'vung')">Vùng</a></li>
                                        <li><a onclick="setfield(this,3,'tinhthanh')">Tỉnh thành</a></li>
                                        <li><a onclick="setfield(this,4,'quanhuyen')">Quận huyện</a></li>
                                    </ul>
                                </li>
                                <li><span onclick="showul(this)"><b class="caret"></b> Đơn hàng</span>
                                    <ul>
                                        <li><a onclick="setfield(this,8,'trangthaidonhang')">Trạng thái đơn hàng</a></li>
                                        <li><a onclick="setfield(this,5,'soluongsanpham')">Số lượng sản phẩm/đơn hàng</a></li>
                                        <li><a onclick="setfield(this,6,'tongtienhang')">Tổng tiền hàng</a></li>
                                        <li><a onclick="setfield(this,7,'chiphivanchuyen')">Chi phí vận chuyển</a></li>
                                        <li><a onclick="setfield(this,10,'doitacvanchuyen')">Đối tác vận chuyển</a></li>
                                        <li><a onclick="setfield(this,13,'maphieuxuatkho')">Mã phiếu xuất kho</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation col-xs-2">
                            <select class="oparation form-control" name="FieldOparation[]">
                                <option value="1">Bằng</option>
                                <option value="0">Có chứa</option>
                                <option value="2">Khác</option>
                                <option value="3">Lớn hơn</option>
                                <option value="4">Nhỏ hơn</option>
                                <option value="5">Lớn hơn hoặc bằng</option>
                                <option value="6">Nhỏ hơn hoặc bằng</option>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter col-xs-3">
                            <input class="form-control" type="text" name="FieldText[]" id="textsearch" />
                        </div>
                        <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                <?php
                    }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="block3 table_data">
            <?php 
            $result_fillter = implode(" , ",$result_fillter);
            echo $result_fillter!='' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> ".$result_fillter."</div>" : '' ;
            $i=1;
            ?>
            <table id="table_data">
                <tr>
                    <th class="text-center">Ngày đặt hàng</th>
                    <th class="text-center">Mã XK</th>
                    <th class="text-center">Tên khách hàng</th>
                    <th class="text-center">Khu vực</th>
                    <th class="text-center">Tổng giá trị</th>
                    <th class="text-center">Tình trạng</th>
                </tr>
                <?php 
                $arr_status = $this->db->query("select * from ttp_define where `group`='status' and `type`='order'")->result();
                $array_status = array();
                $array_status_color = array();
                foreach($arr_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $array_status[$code] = $ite->name;
                    $array_status_color[$code] = $ite->color;
                }
                
                $type_status = $this->define_model->get_order_status('type','order');
                $arr_type = array();
                foreach($type_status as $key=>$ite){
                    $code = (int)$ite->code;
                    $arr_type[$code] = $ite->name;
                }
                $arr_orderID = array();
                
                $arr_money = array(
                    1=>'<i class="fa fa-circle text-success" aria-hidden="true"></i> Đã thu',
                    0=>'<i class="fa fa-circle text-default" aria-hidden="true"></i> Chưa thu'
                );

                if(count($data)>0){
                    foreach($data as $row){
                        $arr_orderID[] = $row->ID;
                        $tienhang = $row->Total-$row->Chietkhau;
                        $status = isset($array_status[$row->Status]) ? $array_status[$row->Status] : "--" ;
                        $total_huy += $row->Status==1 ? $row->Total : 0 ;
                        $total_tienhang += $tienhang;
                        $total_chiphi += $row->Chiphi;
                        $total_giatri += $row->Total;
                        $row->RealMoney = $row->RealMoney==0 ? 'Chưa thu' : number_format($row->RealMoney);
                        $row->TransportRef = $row->TransportRef=='' ? "<label onclick='EnterTransportCode(this,$row->ID)' class='label label-primary'>Click để nhập vận đơn</label>" : "<label class='label label-primary' onclick='EnterTransportCode(this,$row->ID)'>Mã vận đơn : $row->TransportRef</label>" ;
                        $row->TransportRef = $row->TransportStatus!='' ? $row->TransportRef.'<br><label class="label label-success">'.$row->TransportStatus.'</label>' : $row->TransportRef ;
                        $row->TransportRef = $row->Status!=0 && $row->Status!=1 ? '<br>'.$row->TransportRef : '' ;
                        echo "<tr class='row_order_$row->TransportID'>
                                <td style='width: 90px;'><a href='{$base_link}preview/$row->ID'>".date('d/m/Y',strtotime($row->Ngaydathang))."</a></td>
                                <td style='width:160px'><a href='".ADMINPATH."/report/import/lapphieuxuatkho/$row->ID'>$row->MaXK</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>$row->Name</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>  $row->Quanhuyen<br>$row->Thanhpho</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>".number_format($row->Total-$row->Chietkhau+$row->Chiphi)."</a></td>
                                <td><a href='{$base_link}preview/$row->ID'>$status</a>$row->TransportRef</td>
                            </tr>";
                        $i++;
                        $temp1 = date('mY',strtotime($row->Ngaydathang));
                        if(!isset($dayofmonth_real[$temp1])){
                            $dayofmonth_real[$temp1] = 1;
                        }else{
                            $dayofmonth_real[$temp1] = $dayofmonth_real[$temp1] +1;
                        }
                    }
                    $total_tienhang = $total-$chietkhau;
                    $total_giatri = $total + $chiphi-$chietkhau;
                    echo "<tr>
    		    	<td colspan='4' style='text-align:center;'><p><span style='font-size:17px;font-weight:bold;'>Tổng cộng</span> <br>Tìm thấy <b>".number_format($find)."</b> đơn hàng theo yêu cầu .</p></td>
    			            <td style='text-align:right'>".number_format($total_giatri)."</td>
                            <td></td>
                        </tr>";
                }else{
                    echo "<tr><td colspan='6'>Không tìm thấy đơn hàng.</td></tr>";
                }
                ?>
            </table>
            <?php 
                echo $nav;
            ?>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
<?php 
    $time_startday = strtotime($startday);
    $time_stopday = strtotime($stopday);
    $currday = strtotime(date('Y-m-d',time()));
?>
<div class="notify_current" style="display:none">
    <i class="fa fa-archive"></i><a class="numbernoti">1</a>
    <ul></ul>
</div>
<?php
    $bonus = ($this->user->Channel==0 || $this->user->Channel==2) && $this->user->UserType==4 ? " and OrderType in(0,3)" : "" ;
    $bonus = $this->user->Channel==1 && $this->user->UserType==4 ? " and OrderType in(1,2,4,5)" : $bonus ;
    $curdate = date('Y-m-d',time());
    $max = $this->db->query("select count(1) as SL from ttp_report_order where Status=9 and date(Ngaydathang)='$curdate' $bonus")->row();
    $slmax = $max ? $max->SL : 0 ;
?>
<script type="text/javascript">
    var baselink = $("#baselink_report").val();
    var resultmax = <?php echo $slmax ?>;
    var getcurrentorder = function(){
        $.ajax({
            url: baselink+"import/get_current_order_department",
            dataType: "html",
            type: "POST",
            data: "department=4&max="+resultmax,
            success: function(result){
                if(result!="false"){
                    var ob = jQuery.parseJSON(result);
                    content = ob.content;
                    for (var key in content) {
                        if (content.hasOwnProperty(key)) {
                            notifyNewOrder(content[key].user,content[key].image,content[key].title);
                        }
                    }
                    location.reload();
                }
            }
        });
    }
    setInterval(getcurrentorder,40000);
</script>    

<script type="text/javascript">
    function load_data_from_url(ob,url){
        $.get(url,function(result){
            $("#modal").modal("show");
            $("#modal .modal-content").html(result);
        },'html');
    }

    function EnterTransportCode(ob,OrderID){
        var TransportRef = prompt("Nhập mã vận đơn");
        if (TransportRef != null) {
            $.post(baselink+"import_order/enter_transport_ref",{TransportRef:TransportRef,ID:OrderID},function(result){
                if(result.error==false){
                    $(ob).html("Mã vận đơn : "+result.message);
                }else{
                    alert(result.message);
                }
            },'json');
        }
    }

    $(".numbernoti").click(function(){
        $(this).parent().find('ul').toggle();
    });

    $("#show_thaotac").click(function(event){
        event.stopPropagation();
        $(this).parent('li').find('ul').toggle();
    });

    $('html').click(function() {
        $("#mydiv_thaotac").hide();
        $(".dropdownbox").hide();
    });



    function send_ajax(num, index){
        if (index >= num ){
            $('.fa-refresh').removeClass("fa-spin");
            $('.sync-all').removeClass("disabled");
            return false;
        }
        var baselink = $("#baselink_report").val();
        var orderid = $('#sync_data_'+index).attr('data-id');
        $('#sync_data_'+index).removeClass("fa-question-circle");
        $('#sync_data_'+index).addClass("fa-refresh");
        $('#sync_data_'+index).addClass("fa-spin");
        $.ajax({
            url: baselink + "import_order/syncAllData",
            dataType: "JSON",
            type: "GET",
            context: this,
            data: "OrderID="+orderid,
            success: function(result){
                console.log(result.OrderID);
                if(result.Updated=="True"){
                    $('#sync_data_'+index).removeClass('text-warning').addClass('fa-check-circle');
                    $('#sync_data_'+index).html('Đã cập nhật');
                }else{
                    $('#sync_data_'+index).removeClass('fa-spin');
                }
            }
        }).always(function(){
            send_ajax(num,++index);
        });
    }
    
    //Sync All
    $('.sync-all').click(function(){
        $(this).addClass("disabled");
        var num = "<?php echo $i;?>";
        send_ajax(num, 1);
    });


    $(document).ready(function () {
        var cb = function (start, end, label) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        var optionSet1 = {
            startDate: <?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>,
            endDate: <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>,
            minDate: '01/01/2014',
            maxDate: '<?php echo date("m/d/Y",time()) ?>',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: false,
            timePicker: false,
            timePickerIncrement: 2,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(<?php echo $numday_start_group1==0 ? "moment()" : "moment().subtract($numday_start_group1, 'days')" ; ?>.format('MMMM D, YYYY') + ' - ' + <?php echo $numday_stop_group1==0 ? "moment()" : "moment().subtract($numday_stop_group1, 'days')" ; ?>.format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb2);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

        /*
        *******************************
        *   Filter by datepicker      *
        *                             *
        *******************************
        */
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            $(".over_lay").fadeIn();
            var startday = picker.startDate.format('DD/MM/YYYY');
            var stopday = picker.endDate.format('DD/MM/YYYY');
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import/set_day",
                dataType: "html",
                type: "POST",
                data: "group1="+startday+" - "+stopday,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown('slow');
                    }
                }
            }); 
        });
    });
    var baselink = $("#baselink_report").val();
    var getnewsorder = function(){
        var data = $("#newsdescription").html();
        $.ajax({
            url: baselink+"import/get_news_order",
            dataType: "html",
            type: "POST",
            data: "",
            success: function(result){
                if(data!=result){
                    $("#newsdescription").html(result);
                    $("#newsdescription").slideDown();
                }
            }
        });
    }

    setInterval(getnewsorder,30000);
	

    /*
    ****************************************
    *   Function of Tools Box              *
    *                                      *
    ****************************************
    */

    function showtools(){
        $(".filltertools").toggle();
    }

    function showdropdown(ob){
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function(){
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>"+baserow.html()+"</div>");
        $(".add_box_data .row:last-child").find('p.first-title').html(status);
    });

    function removerowfill(ob){
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob,code,fieldname){
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html()+'<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink+"import_order/load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName="+fieldname,
            success: function(result){
                $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob){
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname){
        if(fieldname=="vung" || fieldname=="tinhthanh" || fieldname=="quanhuyen" || fieldname=='loaikhachhang' || fieldname=='trangthaidonhang' || fieldname=='doitacvanchuyen' || fieldname=='user'|| fieldname=="maphieuxuatkho"){
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        }else{
            if(fieldname=="tenkhachhang"){
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            }else{
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    function export_data(ob){
        $(".export_tools").toggle();
        $(".import_tools").hide();
    }

    function import_data(ob){
        $(".import_tools").toggle();
        $(".export_tools").hide();
    }
    
    function sync_data(ob){
        var baselink = $("#baselink_report").val();
        window.location.assign(baselink+"/import_order/syncAll");
    }

    function checkfull(ob){
        if(ob.checked===true){
            $("#table_data .selected_order").prop('checked', true);
        }else{
            $("#table_data .selected_order").prop('checked', false);
        }
    }
    
    function export_row(ob){
        var url = $(ob).attr('template');
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            alert(datastr);
        }
    }

    function success_row(ob){
        $(ob).parent('li').parent('ul').toggle();
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            senddata("set_success",datastr);
        }
    }

    function cancel_row(ob){
        $(ob).parent('li').parent('ul').toggle();
        var data_js = [];
        $("#table_data .selected_order").each(function(){
            if(this.checked===true){
                var data_id = $(this).attr('data-id');
                data_js.push(data_id);
            }
        });
        if(data_js.length>0){
            var datastr = JSON.stringify(data_js);
            senddata("set_cancel",datastr);
        }
    }

    function senddata(url,data){
        if(url!='' && data!=''){
            $(".over_lay").fadeOut();
            var baselink = $("#baselink_report").val();
            $.ajax({
                url: baselink+"import_order/"+url,
                dataType: "html",
                type: "POST",
                data: "data="+data,
                success: function(result){
                    if(result=="OK"){
                        location.reload();
                    }else{
                        $(".over_lay").fadeOut();
                        $(".warning_message").slideDown();
                    }
                }
            }); 
        }else{
            $(".warning_message").slideDown();
        }
    }

    function notifyNewOrder(title,image,message){
        notify(title, {
            body: message,
            icon: image,
            onclick: function(e) {},
            onclose: function(e) {},
            ondenied: function(e) {}
        });
    }

</script>
