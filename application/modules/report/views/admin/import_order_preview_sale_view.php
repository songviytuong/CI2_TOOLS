<div class="containner">
	<div class="disableedit" style="height:1000px"></div>
	<div class="status_progress">
    	<?php 
    	$status1 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point1 = $status1=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point1 = $data->Status==3 || $data->Status==5 || $data->Status==7  || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point1 ;

		$status2 = $data->Status==3 || $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point2 = $status2=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point2 =  $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point2 ;    	

    	$status3 = $data->Status==5 || $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point3 = $status3=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point3 =  $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point3 ;

    	$status4 = $data->Status==7 || $data->Status==8 || $data->Status==9 || $data->Status==0 ? 'class="active"' : "";
    	$point4 = $status4=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point4 =  $data->Status==7 || $data->Status==8 || $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point4 ;

		$status5 = $data->Status==8 || $data->Status==7 || $data->Status==0 ? 'class="active"' : "";
    	$point5 = $status5=='' ? '<span></span>' : '<span class="current"><i class="fa fa-refresh"></i></span>';
    	$point5 =  $data->Status==0 ? '<span class="ok"><i class="fa fa-check-circle"></i></span>' : $point5 ;    	
    	
    	$width = $data->Status==2 || $data->Status==4 || $data->Status==6 ? 20 : 0 ;
    	$width = $data->Status==3 ? 40 : $width ;
    	$width = $data->Status==5 ? 60 : $width ;
    	$width = $data->Status==9 ? 80 : $width ;
    	$width = $data->Status==7 || $data->Status==0 || $data->Status==8 ? 100 : $width ;

    	if($data->Status==1 || $data->Status==8){
    		$title_drop = $data->Status==8 ? "Bị trả về" : "Bị hủy" ;
    		echo "<div class='orderisdrop'><a>$title_drop</a><span><i class='fa fa-times'></i></span></div>";
    		echo '<div class="transfer_progress" style="width:100%;background:#000"></div>';
    	}else{
    	?>
    	<ul>
            <li class="active"><a>ĐH nháp</a><span class="ok"><i class="fa fa-check-circle"></i></span></li>
            <li <?php echo $status1 ?>><a>Đã chuyển sang kho</a><?php echo $point1 ?></li>
            <li <?php echo $status2 ?>><a>Kho xác nhận</a><?php echo $point2 ?></li>
            <li <?php echo $status3 ?>><a>Kế toán xác nhận</a><?php echo $point3 ?></li>
            <li <?php echo $status4 ?>><a>Đã giao vận chuyển</a><?php echo $point4 ?></li>
            <li <?php echo $status5 ?>><a>Hoàn tất</a><?php echo $point5 ?></li>
        </ul>
        <div class="transfer_progress" style="width:<?php echo $width ?>%"></div>
        <?php 
    	}
        ?>
    </div>
	<div class="import_select_progress">
	    <div class="block1">
	    	<h1><a href="<?php echo $base_link ?>">THÔNG TIN ĐƠN HÀNG</a> - <a style="color:#DE5252"><?php echo $data->MaDH ?></a></h1>
	    </div>
	    <div class="block2">
	    </div>
    </div>

    <div class="import_order_info">
    		<input type='hidden' name="Tinhtrangdonhang" id="Status" value='<?php echo $data->Status ?>' />
    		<input type="hidden" name="IDOrder" value="<?php echo $data->ID ?>" />
	    	<div class="block1">
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
		    				<label class="col-xs-6 control-label">Người tạo đơn hàng </label>
		    				<label class="col-xs-6 control-label">: <?php echo $data->UserName ?></label>
		    			</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Ngày / giờ đặt hàng</label>
		    				<label class="col-xs-6 control-label">: <?php echo date('Y-m-d H:i:s',strtotime($data->Ngaydathang)) ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Ngày đề nghị giao hàng</label>
		    				<label class="col-xs-6 control-label">: <?php echo date('Y-m-d H:i:s',strtotime($data->Ngaydathang)) ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<?php 
	    			$tinhthanh = $this->db->query("select a.Title,a.ID,b.Title as AreaTitle,a.AreaID,a.DefaultWarehouse from ttp_report_city a,ttp_report_area b where a.AreaID=b.ID and a.ID=$data->CityID")->row();
	    			$quanhuyen = $this->db->query("select * from ttp_report_district where ID=$data->DistrictID")->row();
	    			?>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Khu vực</label>
							<label class="col-xs-6 control-label"> : <?php echo $tinhthanh->AreaTitle ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Tỉnh / Thành</label>
							<label class="col-xs-8 control-label"> : <?php echo $tinhthanh->Title ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Quận / Huyện</label>
							<label class="col-xs-7 control-label"> : <?php echo $quanhuyen->Title ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">PT thanh toán</label>
							<label class="col-xs-6 control-label"> : <?php echo $data->Payment==0 ? "COD" : "Chuyển khoản" ; ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Số điện thoại 1</label>
							<label class="col-xs-8 control-label"> : <?php echo $data->Phone1 ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-4 control-label">Số điện thoại 2</label>
							<label class="col-xs-7 control-label"> : <?php echo $data->Phone2 ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-4">
	    				<div class="form-group">
	    					<?php 
	    					switch ($data->OrderType) {
	    						case 1:
	    							$type = "GT" ;
	    							break;
	    						case 2:
	    							$type = "MT" ;
	    							break;
	    						case 4:
	    							$type = "TD" ;
	    							break;
	    						case 5:
	    							$type = "NB" ;
	    							break;
	    						default:
	    							$type = "--" ;
	    							break;
	    					}
	    					?>
	    					<label class="col-xs-6 control-label">Loại khách hàng</label>
	    					<label class="col-xs-6 control-label">: <?php echo $type ?></label>
	    				</div>
	    			</div>
	    			<div class="col-xs-4">
	    				<div class="form-group">
							<?php 
				    		if($data->OrderType==2){
				    			$SystemID = $this->db->query("select SystemID from ttp_report_customer a,ttp_report_order b where a.ID=b.CustomerID and b.ID=".$data->ID)->row();
				    			$SystemID = $SystemID ? $SystemID->SystemID : 0 ;
				    			$title_system = $this->db->query("select Title from ttp_report_system where ID=".$SystemID)->row();
				    			echo '<label class="col-xs-6 control-label">Hệ thống kênh bán hàng</label>';
				    			echo $title_system ? '<label class="col-xs-6 control-label">:'.$title_system->Title."</label>" : '<label class="col-xs-8 control-label">:--</label>' ;
				    		}
				    		?>
	    				</div>
	    			</div>
	    		</div>
				<div class="row">
					<div class="col-xs-4">
	    				<div class="form-group">
	    					<label class="col-xs-6 control-label">Mã đối tác</label>
	    					<label class="col-xs-6 control-label">: <?php echo $data->Code ?></label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Tên đối tác</label>
	    					<label class="col-xs-10 control-label">: <?php echo $data->Name ?></label>
						</div>
					</div>
	    			<input type='hidden' name="CustomerID" class="form-control" id="CustomerID" value="<?php echo $data->CustomerID ?>" required />
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Địa chỉ giao hàng</label>
	    					<label class="col-xs-10 control-label">: <?php echo $data->AddressOrder ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Ghi chú khách hàng</label>
	    					<label class="col-xs-10 control-label">: <?php echo $data->Note == '' ? '--' : $data->Note ; ?></label>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="row">
	    			<div class="col-xs-12">
	    				<div class="form-group">
	    					<label class="col-xs-2 control-label">Kho lấy hàng</label>
	    					<div class="col-xs-2">
	    						<select name="KhoID" id="KhoID" class="form-control" onchange="changewarehouse(this)">
									<?php 
									if($this->user->UserType==2 || $this->user->UserType==8){
										$khoresult = $this->db->query("select * from ttp_report_warehouse where Manager like '%\"".$this->user->ID."\"%'")->result();
									}else{
										$khoresult = $this->db->query("select * from ttp_report_warehouse")->result();
									}
									if(count($khoresult)>0){
										foreach($khoresult as $row){
											$selected = '';
											if($data->KhoID>0){
												$selected = $data->KhoID==$row->ID ? "selected='selected'" : '' ;
											}else{
												if($tinhthanh){
													$selected = $tinhthanh->DefaultWarehouse==$row->ID ? "selected='selected'" : '' ;
												}
											}
											echo "<option value='$row->ID' $selected>$row->MaKho</option>";
										}
									}
									?>
								</select>
	    					</div>
	    					<div class="col-xs-2" style='margin-left:10px'>
	    						<a class="btn btn-primary hidden accept_changewarehouse" onclick="accept_changewarehouse(this)"><i class="fa fa-retweet"></i> Xác nhận chuyển kho</a>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<div class="table_donhang table_data">
	    		<form id="tableform">
		    		<table class="table_data" id="table_data">
		    			<tr>
		    				<th><input type='checkbox' onclick='checkfull(this)' /></th>
		    				<th>Mã sản phẩm</th>
		    				<th>Tên sản phẩm</th>
		    				<th>Lô</th>
		    				<th>Giá bán</th>
		    				<th>Số lượng</th>
		    				<th>% CK</th>
		    				<th>Giá trị CK</th>
		    				<th>Giá sau CK</th>
		    				<th>Thành tiền</th>
		    			</tr>
		    			<?php 
		    			$details = $this->db->query("select a.*,b.Title,b.MaSP,b.CategoriesID,c.ShipmentCode from ttp_report_orderdetails a,ttp_report_products b,ttp_report_shipment c where a.ShipmentID=c.ID and a.ProductsID=b.ID and a.OrderID=$data->ID")->result();
		    			$arrproducts = array();
		    			$temp = 0;
		    			if(count($details)>0){
		    				foreach($details as $row){
		    					$CategoriesID = json_decode($row->CategoriesID,true);
		    					$CategoriesID = is_array($CategoriesID) ? $CategoriesID : array() ;
		    					$categoriestemp = 62;
		    					echo "<tr>";
		    					if($this->user->UserType==2){
		    						echo "<td><input type='checkbox' class='selected_products' onclick='changerow(this)' data-id='$row->ProductsID'>
	    								<input type='hidden' name='ProductsID[]' value='$row->ProductsID'></td>";
		    						if(in_array($categoriestemp, $CategoriesID)){
		    							$temp=1;
		    						}
		    					}else{
		    						echo "<td></td>";
		    					}
		    					$change_shipment = $this->user->UserType==2 ? "<a class='change_shipment' onclick='change_shipment(this,$row->ShipmentID,$row->ProductsID,$row->Amount,$row->ID)'></a><ul></ul>" : "" ;
		    					echo "<td>$row->MaSP</td>";
		    					echo "<td>$row->Title</td>";
		    					echo "<td style='width:180px;text-align:left'><span class='ShipmentDefault' data-id='$row->ProductsID'>$row->ShipmentCode</span>$change_shipment</td>";
			    				$giaban = $row->Price+$row->PriceDown;
		    					$phantramck = $row->PriceDown==0 ? 0 : round($row->PriceDown/($giaban/100),1);
		    					echo "<td><input type='number' class='dongia' value='$giaban' min='0' readonly='true' style='display:none' /> <span  class='showdongia' style='display:block;text-align:right;padding-right:15px'>".number_format($giaban,2)."</span></td>";
		    					echo "<td><input type='number' name='Amount[]' class='soluong' value='$row->Amount' min='1' onchange='recal()' readonly='true' /></td>";
		    					echo "<td><input type='number' onchange='calrowchietkhau(this)' class='percentck' value='$phantramck' min='0' readonly='true' style='display:none' /> <span  class='showpercentCK' style='display:block;text-align:right;padding-right:15px'>$phantramck</span></td>";
								echo "<td><input type='number' onchange='calrowchietkhau(this)' class='giack' value='$row->PriceDown' min='0' readonly='true' style='display:none' /> <span  class='showgiaCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->PriceDown,2)."</span></td>";
								echo "<td><input type='number' class='giasauCK' value='$row->Price' min='0' readonly='true' style='display:none' /> <span class='showgiasauCK' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Price,2)."</span></td>";
								echo "<td><input type='number' class='thanhtien' value='$row->Total' min='1' readonly='true' style='display:none' /> <span class='showthanhtien' style='display:block;text-align:right;padding-right:15px'>".number_format($row->Total,2)."</span></td>";
		    					echo "</tr>";
		    					$arrproducts[] = '"data'.$row->ProductsID.'"';
		    				}
		    			}
		    			
		    			$phantramchietkhau = $data->Total==0 ? 0 : round($data->Chietkhau/($data->Total/100));
		    			$tongtienhang = $data->Total - $data->Chietkhau;
		    			$tonggiatrithanhtoan = $tongtienhang+$data->Chiphi-$data->Reduce;
		    			?>
		    			<tr class="last">
		    				<td colspan="9">Tổng cộng</td>
		    				<td><input type="number" class="tongcong" readonly="true" value="<?php echo $data->Total ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongcong"><?php echo number_format($data->Total,2) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">% chiết khấu</td>
		    				<td><input type='number' class="phantramchietkhau" min="0" max="100" value="<?php echo number_format($phantramchietkhau,2) ?>" onchange="calchietkhau(this)" /></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Giá chiết khấu</td>
		    				<td><input type='number' class="giachietkhau" min="1" value="<?php echo $data->Chietkhau ?>" readonly="true" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiachietkhau"><?php echo number_format($data->Chietkhau,2) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng tiền hàng</td>
		    				<td><input type="number" class="tongtienhang" readonly="true" value="<?php echo $tongtienhang ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtongtienhang"><?php echo number_format($tongtienhang,2) ?></span></td>
		    				
		    			</tr>
		    			<?php 
		    			$reduce = $this->db->query("select a.*,b.Title from ttp_report_reduce_order a,ttp_report_reduce b where a.ReduceID=b.ID and a.OrderID=$data->ID")->result();
		    			$arr_reduce = array();
		    			if(count($reduce)>0){
		    				foreach($reduce as $row){
		    					echo "<tr>";
		    					echo "<td><input type='checkbox' class='selected_reduce' onclick='changerow(this)' data-id='$row->ReduceID' /><input type='hidden' name='ReduceID[]' value='$row->ReduceID' /></td>";
		    					echo "<td colspan='8'>$row->Title <input type='text' name='Timereduce[]' style='display:inline;width:500px' value='$row->TimeReduce' /></td>";
		    					echo "<td><input type='number' name='Valuereduce[]' class='Valuereduce' value='".$row->ValueReduce."' onchange='recal()' /></td>";
		    					echo "</tr>";
		    					$arr_reduce[]='"data'.$row->ReduceID.'"';
		    				}
		    			}
		    			?>
		    			<tr class="last">
		    				<td colspan="9">Tổng giảm trừ</td>
		    				<td><input type="number" class="tonggiamtru" readonly="true" value="<?php echo $data->Reduce ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showtonggiamtru"><?php echo number_format($data->Reduce,2) ?></span></td>
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Chi phí vận chuyển</td>
		    				<td><input type='number' class="chiphivanchuyen" value="<?php echo $data->Chiphi ?>" onchange="recal()" style="display:none" /><span id="showchiphivanchuyen" style="display:block;text-align:right;padding-right:15px;" onclick="showchiphi()"><?php echo number_format($data->Chiphi,2) ?></span></td>
		    				
		    			</tr>
		    			<tr class="last">
		    				<td colspan="9">Tổng giá trị thanh toán</td>
		    				<td><input type='number' class="tonggiatrithanhtoan" readonly="true" value="<?php echo $tonggiatrithanhtoan ?>" style="display:none" /> <span style="text-align:right;display:block;padding-right: 15px;" id="showgiatrithanhtoan"><?php echo number_format($tonggiatrithanhtoan,2) ?></span></td>
		    				
		    			</tr>
		    		</table>
		    		<input type="hidden" name="OrderID" value="<?php echo $data->ID ?>" />
	    		</form>
	    	</div>
	    	<div class="history_status">
	    		<h2>Lịch sử trạng thái đơn hàng</h2>
	    		<?php 
	    		$history = $this->db->query("select a.*,b.UserName from ttp_report_orderhistory a,ttp_user b where a.UserID=b.ID and a.OrderID=$data->ID")->result();
	    		if(count($history)>0){
	    			$array_status = array(
	                    10=>'Chờ xác nhận hủy',
	                    9=>'Chờ nhân viên điều phối',
	                    8=>'Đơn hàng bị trả về',
	                    7=>'Chuyển cho giao nhận',
	                    6=>'Đơn hàng bị trả về từ kế toán',
	                    5=>'Đơn hàng chờ kế toán duyệt',
	                    4=>'Đơn hàng bị trả về từ kho',
	                    3=>'Đơn hàng mới chờ kho duyệt',
	                    2=>'Đơn hàng nháp',
	                    0=>'Đơn hàng thành công',
	                    1=>'Đơn hàng hủy'
	                );
	    			echo "<table><tr><th>Trạng thái</th><th>Ngày / giờ</th><th>Ghi chú thay đổi</th><th>Người xử lý</th></tr>";
	    			foreach($history as $row){
	    				echo "<tr>";
	    				echo isset($array_status[$row->Status]) ? "<td>".$array_status[$row->Status]."</td>" : "<td>--</td>" ;
	    				echo "<td>".date('d/m/Y H:i:s',strtotime($row->Thoigian))."</td>";
	    				echo isset($row->Ghichu) ? "<td>".$row->Ghichu."</td>" : "<td>--</td>";
	    				echo "<td>$row->UserName</td>";
	    				echo "</tr>";
	    			}
	    			echo "</table>";
	    		}
	    		?>
	    	</div>
	    	<div class="block1">
	    		<?php 
	    		if($this->user->UserType==3 || $this->user->UserType==4){
	    		?>
	    		<div class="row row13">
	    			<span class="title">Kho xuất: </span>
	    			<span>
	    			<?php 
	    				$kho = $this->db->query("select b.Title as KhoTitle,b.MaKho from ttp_report_warehouse b where b.ID=$data->KhoID")->row();
	    				echo $kho ? $kho->KhoTitle." ($kho->MaKho)" : "Chưa chọn kho xuất" ;
	    			?>
	    			 </span>
	    		</div>
	    		<?php } ?>
	    		<input type="hidden" name="IsChangeOrder" id="IsChangeOrder" value="0" />
	    		<input type="hidden" id="referer" value="<?php echo isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '' ; ?>" />
	    	</div>
    </div>
    <div class="over_lay black">
    	<div class="box_inner">
    		<div class="block1_inner"><h1></h1><a id="close_overlay"><i class="fa fa-times"></i></a></div>
    		<div class="block2_inner"></div>
    	</div>
    </div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url().ADMINPATH."/report/" ?>" />
</div>