<?php
$this->load->model('products_model', 'products');
$this->load->model('users_model', 'users');
$result_fillter = array();
$fill_temp_value = "";
?>
<div class="warning_message"><span>Vui lòng kiểm tra lại dữ liệu trước khi gửi lên server</span><a id='close_message_warning'><i class="fa fa-times"></i></a></div>
<div class="containner opensitebar">
    <div class="import_select_progress">
        <div class="block1">
            <h1>Products List</h1>
        </div>
        <div class="block2">

        </div>
    </div>
    <div class="import_orderlist">
        <div class="block2">
            <div class="block_2_1">

            </div>
            <div class="block_2_2">
                <select class="btn btn-default" onchange="changeLimit(this);">
                    <?php
                    $pagelimit = $this->lib->get_config_define("global", "pagelimit", 1, "position");
                    foreach ($pagelimit as $row) {
                        if (in_array($row->code, array(10, 20, 30))) {
                            $selected = ($this->session->userdata("limit") == $row->code) ? "selected" : "";
                            ?>
                            <option value="<?= $row->code; ?>" <?= $selected; ?>><?= $row->name; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
                <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                    <button class="btn btn-default" onclick="showtools()">Filter <b class="fa fa-sort"></b></button>
                    <a class="btn btn-primary btn-success" href="<?php echo $base_link . 'add' ?>"><i class="fa fa-plus"></i> Add</a>
                    <a class="btn btn-primary" role="button" onclick="export_data(this)"><i class="fa fa-download"></i> Export</a>
                    <a class="btn btn-primary" role="button" onclick="import_data(this)"><i class="fa fa-upload"></i> Import</a>
                    <!--<a class="btn btn-default pull-right" role="button" href="assets/template/products.xlsx"><i class="fa fa-download"></i> Templates Import</a>-->
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                            Import Templates <span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a role="button" href="assets/template/products.xlsx">Products</a></li>
                            <li><a role="button" href="assets/template/products-meti.xlsx">Products From METI</a></li>
                            <li><a role="button" href="assets/template/photo-import.xlsx">Photo Import</a></li>
                        </ul>
                    </div>
                <?php endif; ?>
                <?php if ($this->user->IsAdmin): ?>
                    <a class="btn btn-danger" role="button" href="<?php echo $base_link . 'crawler' ?>"><i class="fa fa-upload"></i> Crawler</a>
                <?php endif; ?>
                <?php if ($allowassign): ?>
                    <button class="btn btn-default assign" role="button" <?= ($this->session->userdata("Assign") ? '' : 'disabled') ?>><i class="fa fa-group"></i> Assign</button>
                    <?php
                    if ($this->session->userdata("Assign") == FALSE):
                        $_Users = $this->users->defineUsersArray(array('ids' => isCollaborators()));
                        ?>
                        <select class="btn btn-default getOwner">
                            <option value="-1">-- Select --</option>
                            <?php
                            foreach ($_Users as $usr) {
                                $this->load->model('assign_model', 'assign');
                                $counter = $this->assign->getCounterAssign($usr['ID']);
                                $edited = $this->assign->getCounterAssignEdited($usr['ID']);
                                ?>
                                <option value="<?= $usr['ID'] ?>"><?= $usr['LastName'] . ' ', $usr['FirstName'] . ' (' . $usr['UserName'] . ' - ' . $edited . '/' . $counter . ')' ?></option>
                            <?php } ?>
                        </select>
                        <a class="btn btn-default applyAssign" role="button"><i class="fa fa-save"></i> Apply</a>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if (!in_array($this->user->ID, isCollaborators())): ?>
                    <small><a href='<?php echo base_url() . ADMINPATH . "/report/warehouse_products/showAll"; ?>'>Show all Products</a></small>
                <?php endif; ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="import_tools">
            <form action="<?php echo base_url() . ADMINPATH . "/report/warehouse_products/import_products" ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="Image_upload" />
                <button type="submit">Upload</button>
            </form>
        </div>
        <div class="export_tools_kt">
            <form action="<?php echo base_url() . ADMINPATH . "/report/warehouse_products/export_products" ?>" method="post" id="falseclass">
                <span>Xuất từ ngày: </span>
                <input type='text' name="Export_date" class="form-control date-picker" id="Export_date" value="<?php echo date('Y-m-d', time() - (3600 * 8)); ?>" required />
                <span>Đến ngày: </span>
                <input type='text' name="ExportStop_date" class="form-control date-picker" id="ExportStop_date" value="<?php echo date('Y-m-d', time() - (3600 * 8)); ?>" required />
                <span>Chọn loại báo cáo: </span>
                <select name="TypeExport">
                    <option value="1">Thông tin sản phẩm</option>
                </select>
                <button type="submit" class="btn btn-default">Xuất dữ liệu</button>
            </form>
        </div>
        <div class="filltertools">
            <form action="<?php echo $base_link . "setfillter" ?>" method="post">
                <?php
                $arr_fieldname = array(0 => "a.MaSP", 1 => "a.Title", 2 => "b.ID", 3 => "a.Price", 4 => "a.CurrentAmount", 5 => "a.Chimmoi", 6 => "a.VariantType", 7 => "a.Status", 8 => "a.TypeProducts", 9 => "a.Owner");
                $arr_oparation = array(0 => 'like', 1 => '=', 2 => '!=', 3 => '>', 4 => '<', 5 => '>=', 6 => '<=');
                $arr_showfieldname = array(0 => "Mã SKU", 1 => "Tên sản phẩm", 2 => "Thương hiệu", 3 => "Giá bán", 4 => "Số lượng tồn hiện tại", 5 => "Sản phẩm bán chạy", 6 => "Loại sản phẩm", 7 => "Cho phép hiển thị", 8 => "Loại ngành hàng", 9 => "Owner");
                $arr_showoparation = array(0 => 'có chứa', 1 => 'bằng', 2 => 'khác', 3 => 'lớn hơn', 4 => 'nhỏ hơn', 5 => 'lớn hơn hoặc bằng', 6 => 'nhỏ hơn hoặc bằng');
                $fill_data_arr = explode(" and ", $fill_data);
                $arr_field = array();
                if (count($fill_data_arr) > 0 && $fill_data != '') {
                    $temp_tools = 0;
                    foreach ($fill_data_arr as $row) {
                        $param = explode(' ', $row, 3);
                        $value_field = isset($param[0]) ? array_search($param[0], $arr_fieldname) : 10;
                        if (isset($param[0])) {
                            $arr_field[] = "'data" . array_search($param[0], $arr_fieldname) . "'";
                            $k = array_search($param[0], $arr_fieldname);
                        } else {
                            $k = 0;
                        }
                        $param_field = isset($arr_showfieldname[$value_field]) ? $arr_showfieldname[$value_field] : '';
                        $param_oparation = isset($param[1]) ? array_search($param[1], $arr_oparation) : 10;

                        $param_value = isset($param[2]) ? $param[2] : '';
                        $param_value = str_replace("\'", "", $param_value);
                        $param_value = str_replace("'", "", $param_value);
                        $param_value = str_replace("%", "", $param_value);
                        ?>
                        <div class="row <?php echo $temp_tools == 0 ? "base_row row$k" : " row$k"; ?>">
                            <div class="list_toolls col-xs-1"><label class="title first-title"><?php echo $temp_tools == 0 ? "Chọn" : "Và"; ?></label></div>
                            <div class="list_toolls col-xs-2">
                                <input type="hidden" class="FieldName form-control" name="FieldName[]" value="<?php echo isset($param[0]) ? array_search($param[0], $arr_fieldname) : ''; ?>" />
                                <label class="title second-title" onclick="showdropdown(this)"><?php echo $param_field != '' ? $param_field : "Tên field"; ?> <b class="caret"></b></label>
                                <ul class="dropdownbox" style='width: 215px;'>
                                    <li><a onclick="setfield(this, 0, 'masp')">Mã SKU</a></li>
                                    <li><a onclick="setfield(this, 1, 'title')">Tên sản phẩm</a></li>
                                    <li><a onclick="setfield(this, 2, 'trademark')">Thương hiệu</a></li>
                                    <li><a onclick="setfield(this, 3, 'price')">Giá bán lẻ</a></li>
                                    <li><a onclick="setfield(this, 4, 'currentamount')">Số lượng tồn kho</a></li>
                                    <li><a onclick="setfield(this, 5, 'bird')">Sản phẩm bán chạy</a></li>
                                    <li><a onclick="setfield(this, 6, 'variant')">Loại sản phẩm</a></li>
                                    <li><a onclick="setfield(this, 7, 'status')">Cho phép hiển thị</a></li>
                                    <li><a onclick="setfield(this, 8, 'typeproducts')">Loại ngành hàng</a></li>
                                    <?php if ($this->session->userdata("fillter_owner")) : ?>
                                        <li><a onclick="setfield(this, 9, 'owner')">Owner</a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <div class="list_toolls reciveroparation col-xs-2">
                                <select class="oparation form-control" name="FieldOparation[]">
                                    <?php if ($this->session->userdata("fillter_owner")) : ?>
                                        <option value="1" <?php echo $param_oparation == 1 ? "selected='selected'" : ''; ?>>Bằng</option>
                                    <?php else: ?>
                                        <option value="1" <?php echo $param_oparation == 1 ? "selected='selected'" : ''; ?>>Bằng</option>
                                        <option value="0" <?php echo $param_oparation == 0 ? "selected='selected'" : ''; ?>>Có chứa</option>
                                        <option value="2" <?php echo $param_oparation == 2 ? "selected='selected'" : ''; ?>>Khác</option>
                                        <option value="3" <?php echo $param_oparation == 3 ? "selected='selected'" : ''; ?>>Lớn hơn</option>
                                        <option value="4" <?php echo $param_oparation == 4 ? "selected='selected'" : ''; ?>>Nhỏ hơn</option>
                                        <option value="5" <?php echo $param_oparation == 5 ? "selected='selected'" : ''; ?>>Lớn hơn hoặc bằng</option>
                                        <option value="6" <?php echo $param_oparation == 6 ? "selected='selected'" : ''; ?>>Nhỏ hơn hoặc bằng</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="list_toolls reciverfillter col-xs-3">
                                <?php
                                if ($value_field == 2) {
                                    $trademark = $this->db->query("select ID,Title from ttp_report_trademark")->result();
                                    if (count($trademark) > 0) {
                                        echo "<select name='FieldText[]' class='form-control'>";
                                        foreach ($trademark as $row) {
                                            $selected = $param_value == $row->ID ? "selected='selected'" : '';
                                            $fill_temp_value = $param_value == $row->ID ? $row->Title : $fill_temp_value;
                                            echo "<option value='$row->ID' $selected>$row->Title</option>";
                                        }
                                        echo "</select>";
                                    }
                                } elseif ($value_field == 5 || $value_field == 7) {
                                    $fill_temp_value = $param_value == 1 ? "Yes" : "No";
                                    echo "<select name='FieldText[]' class='form-control'>";
                                    $selected0 = $param_value == 0 ? "selected='selected'" : '';
                                    $selected1 = $param_value == 1 ? "selected='selected'" : '';
                                    echo "<option value='1' $selected1>Yes</option>";
                                    echo "<option value='0' $selected0>No</option>";
                                    echo "</select>";
                                } elseif ($value_field == 6) {
                                    $fill_temp_value = $param_value == 0 ? "Sản phẩm đơn" : "";
                                    $fill_temp_value = $param_value == 1 ? "Sản phẩm biến thể" : $fill_temp_value;
                                    $fill_temp_value = $param_value == 2 ? "Sản phẩm bundle" : $fill_temp_value;
                                    $fill_temp_value = $param_value == 3 ? "Sản phẩm nhóm" : $fill_temp_value;
                                    echo "<select name='FieldText[]' class='form-control'>";
                                    $selected0 = $param_value == 0 ? "selected='selected'" : '';
                                    $selected1 = $param_value == 1 ? "selected='selected'" : '';
                                    $selected2 = $param_value == 2 ? "selected='selected'" : '';
                                    $selected3 = $param_value == 3 ? "selected='selected'" : '';
                                    echo "<option value='0' $selected0>Sản phẩm đơn</option>";
                                    echo "<option value='1' $selected1>Sản phẩm biến thể</option>";
                                    echo "<option value='2' $selected2>Sản phẩm Bundle</option>";
                                    echo "<option value='3' $selected3>Sản phẩm nhóm</option>";
                                    echo "</select>";
                                } elseif ($value_field == 8) {
                                    echo "<select name='FieldText[]' class='form-control'>";
                                    $productype = $this->lib->get_config_define("products", "productstype", 1, "code");
                                    foreach ($productype as $row) {
                                        $selected = $param_value == $row->code ? "selected='selected'" : '';
                                        $fill_temp_value = $param_value == $row->code ? $row->name : $fill_temp_value;
                                        echo "<option value='$row->code' $selected>$row->name</option>";
                                    }
                                    echo "</select>";
                                } elseif ($value_field == 9) {
                                    $Users = $this->users->defineUsers(array('userid' => $this->session->userdata("fillter_owner")));
                                    $fill_temp_value = $Users['FirstName'];
                                    echo "<select class='form-control' name=FieldText[] value=>";
                                    echo "<option value=" . $this->session->userdata("fillter_owner") . ">" . $fill_temp_value . "</option>";
                                    echo "</select>";
                                    //echo '<input type="text" name="FieldText[]" class="form-control" id="textsearch" value="' . $this->session->userdata("fillter_owner") . '" readonly>';
                                } else {
                                    $fill_temp_value = $param_value;
                                    echo '<input type="text" class="form-control" name="FieldText[]" id="textsearch" value="' . $param_value . '" />';
                                }
                                ?>
                            </div>
                            <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                        </div>
                        <?php
                        $temp_tools++;
                        $showoparation = isset($arr_showoparation[$param_oparation]) ? $arr_showoparation[$param_oparation] : ':';
                        $result_fillter[] = $param_field . " " . $showoparation . " '<b>" . $fill_temp_value . "</b>'";
                    }
                } else {
                    ?>
                    <div class="row base_row">
                        <div class="list_toolls col-xs-1"><label class="title first-title">Chọn</label></div>
                        <div class="list_toolls col-xs-2">
                            <input type="hidden" class="FieldName form-control" name="FieldName[]" />
                            <label class="title second-title" onclick="showdropdown(this)">Tên field <b class="caret"></b></label>
                            <ul class="dropdownbox" style='width: 215px;'>
                                <li><a onclick="setfield(this, 0, 'masp')">Mã SKU</a></li>
                                <li><a onclick="setfield(this, 1, 'title')">Tên sản phẩm</a></li>
                                <li><a onclick="setfield(this, 2, 'trademark')">Thương hiệu</a></li>
                                <li><a onclick="setfield(this, 3, 'price')">Giá bán lẻ</a></li>
                                <li><a onclick="setfield(this, 4, 'currentamount')">Số lượng tồn kho</a></li>
                                <li><a onclick="setfield(this, 5, 'bird')">Sản phẩm bán chạy</a></li>
                                <li><a onclick="setfield(this, 6, 'variant')">Loại sản phẩm</a></li>
                                <li><a onclick="setfield(this, 7, 'status')">Cho phép hiển thị</a></li>
                                <li><a onclick="setfield(this, 8, 'typeproducts')">Loại ngành hàng</a></li>
                                <?php if ($this->session->userdata("fillter_owner")) : ?>
                                    <li><a onclick="setfield(this, 9, 'owner')">Owner</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="list_toolls reciveroparation col-xs-2">
                            <select class="oparation form-control" name="FieldOparation[]">
                                <?php if ($this->session->userdata("fillter_owner")) : ?>
                                    <option value="1">Bằng</option>
                                <?php else: ?>
                                    <option value="1">Bằng</option>
                                    <option value="0">Có chứa</option>
                                    <option value="2">Khác</option>
                                    <option value="3">Lớn hơn</option>
                                    <option value="4">Nhỏ hơn</option>
                                    <option value="5">Lớn hơn hoặc bằng</option>
                                    <option value="6">Nhỏ hơn hoặc bằng</option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="list_toolls reciverfillter col-xs-3">
                            <input type="text" name="FieldText[]" class="form-control" id="textsearch" />
                        </div>
                        <a class='remove_row_x col-xs-1 btn' onclick='removerowfill(this)'><i class='fa fa-times'></i></a>
                    </div>
                    <?php
                }
                ?>
                <div class="add_box_data"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-11">
                                <a class="btn btn-default" id="add_field"><i class="fa fa-plus"></i> Thêm field</a>
                                <button class="btn btn-default" id="excute_fill"><i class="fa fa-search"></i> Lọc dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="block3 table_data">
            <?php
            $result_fillter = implode(" , ", $result_fillter);
            echo $result_fillter != '' ? "<div style='background: #f6f6f6;border: 1px solid #E1e1e1;border-bottom: 0px;padding: 5px 10px;''><b>Kết quả tìm kiếm cho bộ lọc :</b> " . $result_fillter . "</div>" : '';
            ?>
            <table id="table_data">
                <tr>
                    <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                        <th class="text-center checkall <?= ($this->session->userdata("Assign") ? 'hidden' : '') ?>"><input type="checkbox" onClick="toggle(this)" /></th>
                    <?php endif; ?>
                    <th>No.</th>
                    <th>SKU</th>
                    <th>Product Name</th>

                    <th style="width:150px;">Status</th>

                    <th>Level 1</th>
                    <th>Level 2</th>
                    <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                        <th class="text-center">Published</th>
                    <?php endif; ?>
                    <th class="text-right">Price</th>
                    <th>Owner</th>
                    <?php if ($this->user->IsAdmin) : ?><th></th><?php endif; ?>
                </tr>
                <?php if (!in_array($this->user->ID, isCollaborators())) : ?>
                    <tr>
                        <td class="checkall <?= ($this->session->userdata("Assign") ? 'hidden' : '') ?>"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <select class="form-control" onchange="collaborators_status(this)">
                                <option value="-1" selected="" disabled="">-- Status --</option>
                                <?php
                                $collaborators_status = $this->lib->get_config_define("products", "collaborators_status", 1, "code");
                                foreach ($collaborators_status as $_status) {
                                    ?>
                                    <option value="<?= $_status->code; ?>" <?= (($this->session->userdata("Collaborators") == $_status->name) ? 'selected' : '') ?>><?= $_status->name; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                <?php endif; ?>
                <?php
                $categories = $this->db->query("select ID,Title,Path from ttp_report_categories")->result();
                $arr_categories = array();
                if (count($categories) > 0) {
                    foreach ($categories as $row) {
                        $temp_path = explode('/', $row->Path);
                        $temp_path = isset($temp_path[0]) ? $temp_path[0] : $row->ID;
                        $arr_categories[$row->ID] = array('Title' => $row->Title, 'RootParent' => $temp_path);
                    }
                }



                $i = $start + 1;
                $arr_products = array();
                if (count($data) > 0) {
                    foreach ($data as $row) {
                        $this->load->model('assign_model', 'assign');
                        $status_assign = $this->assign->getAssignByProductID(array('pid' => $row->ID));
                        if ($status_assign != NULL) {
                            $owner_assign = $status_assign->Owner;
                        } else {
                            $owner_assign = 5;
                        }

                        $Users = $this->users->defineUsers(array('userid' => $owner_assign));

                        $arr_products[] = $row->ID;
                        $last_categories = json_decode($row->CategoriesID, true);

                        $path = "public/admin/images/";
                        $tit = "";
                        $bgcolor = "";
                        if (!empty($row->Alias) && (!empty($row->MetaTitle) && !empty($row->MetaDescription) && !empty($row->MetaKeywords))) {
                            $icon = "checked.png";
                            $bgcolor = $Users['Color'];
                        } else {
                            if ($row->Alias || (!empty($row->MetaTitle) && !empty($row->MetaDescription) && !empty($row->MetaKeywords))) {
                                $icon = "warning.png";
                                $tit = "Meta-SEO";
                            } else {
                                $icon = "unchecked.png";
                            }
                        }

                        $this->load->model('imported_model', 'imported');
                        $data_imported = $this->imported->getDataImport();

                        $status_import = "";
                        foreach ($data_imported as $item) {
                            $imps[] = $item->SKU;
                        }
                        if (!in_array($row->MaSP, $imps)) {
                            $_code = 1; //Draft
                        }

                        foreach ($data_imported as $item) {
                            if ($item->SKU === $row->MaSP) {
                                $_code = 2; //Photo Ready
                            }
                        }

                        if ($status_assign) {
                            $_code = $status_assign->Status;
                        }

                        #isExist
                        $this->load->model('assign_model', 'assign');
                        if ($row->ID) {
                            $isExist = $this->assign->isExist($row->ID);
                        }
                        if ($_code == 2 && $isExist) {
                            $_code = 1;
                        }

                        $where = array(
                            'code' => $_code,
                            'group' => 'products',
                            'type' => 'collaborators_status'
                        );
                        $status_define = $this->lib->getFieldsByValue('name', $where);
                        if ($this->session->userdata("Collaborators")) {
                            $status_import = $this->session->userdata("Collaborators");
                        } else {
                            $status_import = $status_define->name;
                        }

                        $status_import = ((($status_import == 'Approved') ? '<font color="blue">' . $status_import . '</font>' : (($status_import == 'Rejected') ? '<font color="red">' . $status_import . '</font>' : $status_import)));

                        echo "<tr id='checkboxes'>";
                        if (!in_array($this->user->ID, isCollaborators())) :
                            echo "<td style='text-align:center' class='checkall " . ($this->session->userdata("Assign") ? 'hidden' : '') . "'><input type='checkbox' name='ids' value='$row->ID' /></td>";
                        endif;
                        echo "<td style='text-align:center'>$i</td>
                                <td><a href='{$base_link}edit/$row->ID'>$row->MaSP</a></td>
                                <td><div class='text-left' style='float:left;'><a href='{$base_link}edit/$row->ID'>" . (!empty($row->Title) ? $row->Title : $row->ShortName) . "</a></div><div class='text-right'><img src=" . $path . $icon . " title=" . $tit . "></div></td>";

                        echo "<td>$status_import</td>";


                        echo "<td style='text-align:center'>";
                        $arr_exists = array();
                        if (is_array($last_categories) && count($last_categories) > 0) {
                            foreach ($last_categories as $value) {
                                $title_exists = isset($arr_categories[$value]['RootParent']) ? $arr_categories[$arr_categories[$value]['RootParent']]['Title'] : '';
                                if (!in_array($title_exists, $arr_exists)) {
                                    echo "<a class='categories_a' href='{$base_link}edit/$row->ID''>" . $title_exists . "</a>";
                                    $arr_exists[] = $title_exists;
                                }
                            }
                        }
                        echo "</td><td style='text-align:center'>";
                        if (is_array($last_categories) && count($last_categories) > 0) {
                            foreach ($last_categories as $value) {
                                $title_exists = isset($arr_categories[$value]['Title']) ? $arr_categories[$value]['Title'] : '';
                                if (!in_array($title_exists, $arr_exists)) {
                                    echo "<a class='categories_a' href='{$base_link}edit/$row->ID''>" . $title_exists . "</a>";
                                    $arr_exists[] = $title_exists;
                                }
                            }
                        }

                        if ($row->Published == 1) {
                            $icon = "<i class='fa fa-check-square' style='color:#090'></i>";
                            $tit = "";
                        } else {
                            $icon = "<i class='fa fa-check-square' style='color:#ddd'></i>";
                            $tit = "Active?";
                        }
                        $uid = $Users['ID'];
                        if ((!in_array($this->user->ID, isCollaborators()) && in_array($this->user->ID, isWriterManager())) || $this->user->IsAdmin) :
                            echo "</td><td style='text-align:center'><a class='verify_active btn_$row->ID' data-owner='$uid' data-id='$row->ID'>$icon</a></td>";
                        endif;
                        echo "<td style = 'text-align:right'>" . number_format($row->Price) . "</td>
                        <td" . ((in_array($Users['ID'], isCollaborators())) ? ' style="background-color:' . $Users['Color'] . ';"' : '') . ">" . $Users['FirstName'] . "</td>";

                        if ($this->user->IsAdmin) :
                            echo "<td style='text-align:center'><a title='Xóa sản phẩm này' href='{$base_link}delete/$row->ID' class='delete'><i class='fa fa-trash-o'></i></a></td>";
                        endif;
                        echo "</tr>";
                        $i++;
                    }

                    echo "<tr><td colspan='15'>Total: <b>" . number_format($find, 0) . "</b>";
                    if (in_array($this->user->ID, isCollaborators())):
                        $collaborators_status = $this->lib->get_config_define("products", "collaborators_status", 1, "code");
                        $this->load->model('assign_model', 'assign');
                        foreach ($collaborators_status as $_status) {
                            if (in_array($_status->code, array(1, 3, 4, 5))) {
                                if ($_status->code == 1) {
                                    $_status->code = 2; #Draft = Photo Ready
                                }
                                $color = "";
                                if ($_status->code == 4) {
                                    $color = "red";
                                } else if ($_status->code == 5) {
                                    $color = "blue";
                                }
                                echo " | " . $_status->name . ": <b><font color=$color>" . $this->assign->getCounterAssign($this->user->ID, $_status->code) . "</font></b>";
                            }
                        }
                    endif;

                    if (!in_array($this->user->ID, isCollaborators())):
                        echo " | Published: <b>" . number_format($this->products->getProductCounter('published'), 0) . "</b> | Draft: <b>" . number_format($this->products->getProductCounter('notedited'), 0) . "</b> | Add New: <b>" . number_format($this->products->getProductCounter('addnew'), 0) . "</b> | Photo Imported: <b>" . number_format($this->products->getProductCounter('photoready'), 0) . "</b> (<em>Last Imported: " . $this->products->getLatestImport() . "</em>)</td></tr>";
                    endif;
                } else {
                    echo "<tr><td colspan='15'>Không tìm thấy sản phẩm.</td></tr>";
                }
                ?>
            </table>
            <?php
            echo $nav;
            ?>
        </div>
        <style>
            .cashmoney {
                cursor: pointer;
            }
            .closed {
                display: none;
            }
        </style>
        <div class="block3 table_data">
            <table id="table_data">
                <thead>
                    <tr class="headings ">
                        <th class='col-lg-1 text-center'>User / Date</th>
                        <?php
                        for ($i = -1; $i < 1; $i++) {
                            $this_month = date('m', strtotime('+' . $i . " month"));
                            echo "<th class='col-lg-1 text-center'>" . date('M Y', strtotime('+' . $i . " month")) . "</th>";
                        }
                        ?>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $off = TRUE;
                    $full = isset($_GET['full']) ? $_GET['full'] : FALSE;
                    if ($off) {
                        #SELECT OWNER
                        $this->load->model('users_model', 'users');
                        $data_users = $this->cache->get('UserCollaborators!' . substr(md5($this->user->ID . 'UserCollaborators'), 27, 06));
                        if (!$data_users) {
                            $data_users = $this->users->getUsersByGroupID(2);
                            $this->cache->write($data_users, 'UserCollaborators!' . substr(md5($this->user->ID . 'UserCollaborators'), 27, 06), 300);
                        }
                        foreach ($data_users as $user) {
                            if ($user->ID == $this->user->ID || $full) {
                                ?>
                                <tr class="even pointer">
                                    <td><a class="filterByOwner" data-owner="<?= $user->ID; ?>"><?= $user->FirstName; ?></a><?php if (!in_array($this->user->ID, isCollaborators())): ?><div onclick="clickdown(<?= $user->ID; ?>)" style="float:right;cursor: pointer;"><i class="fa clickdown<?= $user->ID; ?> fa-angle-down"></i></div><?php endif; ?></td>
                                    <?php
                                    $path = "public/admin/images/";
                                    $icon = "unchecked.png";
                                    $cash_image = "";
                                    if (in_array($this->user->ID, isCollaborators()) && $user->ID == $this->user->ID) {
                                        $cash_image = "<img src='" . $path . $icon . "'/>";
                                    }

                                    for ($i = -1; $i < 1; $i++) {
                                        $this_month = date('m', strtotime('+' . $i . " month"));
                                        $this_year = date('Y');
                                        if ($this_month == 10 && $i != -1) {
                                            $this_year = date('Y', strtotime("+1 year"));
                                        } else {
                                            $this_year = date('Y');
                                        }
//                                        $done = $this->cache->get('done!' . substr(md5($this->user->ID . 'done'), 27, 06));
//                                        if (!$done) {
                                        $done = $this->products->getProductCounterByDate(array('done' => TRUE, 'Owner' => $user->ID, 'month' => $this_month, 'year' => $this_year));
//                                            $this->cache->write($done, 'done!' . substr(md5($this->user->ID . 'done'), 27, 06), 300);
//                                        }
//                                        $count = $this->cache->get('count!' . substr(md5($this->user->ID . 'count'), 27, 06));
//                                        if (!$count) {
                                        $count = $this->products->getProductCounterByDate(array('Owner' => $user->ID, 'month' => $this_month, 'year' => $this_year));
//                                            $this->cache->write($count, 'count!' . substr(md5($this->user->ID . 'count'), 27, 06), 300);
//                                        }
                                        ?>
                                        <td <?= ($i == 0) ? 'style="background-color:' . $user->Color . '"' : '' ?>><div style="float:left;"><?= $done; ?>/<?= $count; ?></div><?php if ($cash_image && $done > 0): ?><div style="float:right" class="cashmoney" data-bind="<?= $this_month . $this_year . '.' . $Users['ID']; ?>"><?= $cash_image; ?></div><?php endif; ?></td>
                                    <?php } ?>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php if (!in_array($this->user->ID, isCollaborators())): ?>
                                    <tr class="tr<?= $user->ID ?> hidden">
                                        <td colspan="20" style="background-color:#CFF7E3">
                                            <?php
                                            $collaborators_status = $this->lib->get_config_define("products", "collaborators_status", 1, "code");
                                            $this->load->model('assign_model', 'assign');
                                            echo "Total: <b>" . $this->assign->getCounterAssign($user->ID) . "</b>";
                                            foreach ($collaborators_status as $_status) {
                                                if (in_array($_status->code, array(1, 3, 4, 5))) {
                                                    if ($_status->code == 1) {
                                                        $_status->code = 2; #Draft = Photo Ready
                                                    }
                                                    $color = "";
                                                    if ($_status->code == 4) {
                                                        $color = "red";
                                                    } else if ($_status->code == 5) {
                                                        $color = "blue";
                                                    }
                                                    echo " | " . $_status->name . ": <b><font color=$color>" . $this->assign->getCounterAssign($user->ID, $_status->code) . "</font></b>";
                                                }
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="over_lay"></div>
    <input type='hidden' id="baselink_report" value="<?php echo base_url() . ADMINPATH . "/report/warehouse_products/" ?>" />
</div>
<style>
    .daterangepicker{width: auto;}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#Export_date,#ExportStop_date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'YYYY/MM/DD',
        });
    });

    function clickdown(ob) {
        $('.tr' + ob).toggleClass("hidden");
        if ($('.clickdown' + ob).hasClass("fa-angle-down")) {
            $('.clickdown' + ob).removeClass("fa-angle-down");
        } else {
            $('.clickdown' + ob).addClass("fa-angle-down");
        }
        $('.clickdown' + ob).toggleClass("fa-angle-up");
    }

    $('.cashmoney').click(function () {
        var baselink = $("#baselink_report").val();
        var bind = $(this).attr('data-bind');
        $.ajax({
            url: baselink + "cashmoney",
            dataType: "html",
            type: "POST",
            data: 'bind=' + bind,
            context: this,
            success: function (result) {
                console.log(result);
            }
        });
    });

    /*
     ****************************************
     *   Function of Tools Box              *
     *                                      *
     ****************************************
     */

    function showtools() {
        $(".filltertools").toggle();
    }

    function showdropdown(ob) {
        event.stopPropagation();
        $(ob).parent('div').find(".dropdownbox").toggle();
    }

    $("#add_field").click(function () {
        status = "Và";
        baserow = $(".base_row");
        $(".add_box_data").append("<div class='row'>" + baserow.html() + "</div>");
        $(".add_box_data .row:last-child").find('label.first-title').html(status);
    });

    function removerowfill(ob) {
        $(ob).parent("div.form-group").remove();
        $(ob).parent("div.row").remove();
    }

    function setfield(ob, code, fieldname) {
        $(ob).parent('li').parent('ul').parent('.list_toolls').find("input.FieldName").val(code);
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".second-title").html($(ob).html() + '<b class="caret"></b>');
        $(ob).parent('li').parent('ul').parent('.list_toolls').find(".dropdownbox").toggle();
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink + "load_fillter_by_type_and_field",
            dataType: "html",
            type: "POST",
            data: "FieldName=" + fieldname,
            success: function (result) {
                $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciverfillter').html(result);
            }
        });
        var data = showreciveroparation(fieldname);
        $(ob).parent('li').parent('ul').parent('.list_toolls').parent('div').find('.reciveroparation').html(data);
    }

    function showul(ob) {
        event.stopPropagation();
        $(ob).parent('li').find('ul').toggle();
    }

    function showreciveroparation(fieldname) {
        if (fieldname == "trademark" || fieldname == "bird" || fieldname == "variant" || fieldname == "status" || fieldname == "typeproducts") {
            return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option></select>';
        } else {
            if (fieldname == "masp" || fieldname == "title") {
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="0">Có chứa</option><option value="2">Khác</option></select>';
            } else {
                return '<select class="oparation form-control" name="FieldOparation[]"><option value="1">Bằng</option><option value="2">Khác</option><option value="3">Lớn hơn</option><option value="4">Nhỏ hơn</option><option value="5">Lớn hơn hoặc bằng</option><option value="6">Nhỏ hơn hoặc bằng</option></select>';
            }
        }
    }

    $(".fill_tr td a.title").click(function () {
        var type = $(this).attr('data');
        var baselink = $("#baselink_report").val();
        var parent = $(this).parent('div').find('.dropdown_tr');
        var isload = parseInt($(this).attr('isload'));
        if (isload == 0) {
            $.ajax({
                url: baselink + "get_data_fillter",
                dataType: "html",
                type: "POST",
                data: "Type=" + type,
                context: this,
                success: function (result) {
                    parent.html(result);
                }
            });
            $(this).attr('isload', 1);
        }
        parent.toggle();
    });

    var arr_field = [<?php echo implode(',', $arr_field) ?>];

    function set_fill_tr(ob, field) {
        if (jQuery.inArray("data" + field, arr_field) >= 0) {
            $(".filltertools form .row" + field).remove();
        }
        var data = $(ob).attr('value');
        $(".filltertools form").html("");
        $(".filltertools form").prepend("<input type='hidden' name='FieldName[]' value='" + field + "' />");
        $(".filltertools form").prepend("<input type='hidden' name='FieldOparation[]' value='1' />");
        $(".filltertools form").prepend("<input type='hidden' name='FieldText[]' value='" + data + "' />");
        $(".filltertools form").submit();
    }

    function export_data(ob) {
        $(".export_tools_kt").toggle();
        $(".import_tools").hide();
    }

    function import_data(ob) {
        $(".import_tools").toggle();
        $(".export_tools_kt").hide();
    }

    $("#falseclass").submit(function () {
        $(this).find('button').removeClass("saving");
    });

    $('.verify_active').click(function () {
        var baselink = $("#baselink_report").val();
        var id = $(this).attr('data-id');
        var owner = $(this).attr('data-owner');
        $.ajax({
            url: baselink + "setActive",
            dataType: "json",
            type: "POST",
            data: "ID=" + id + 'Owner' + owner,
            context: this,
            success: function (result) {
                $('.btn_' + id).html(result.img);
            }
        });
    });

    $('.filterByOwner').click(function () {
        var baselink = $("#baselink_report").val();
        var owner = $(this).attr('data-owner');
        $.ajax({
            url: baselink + "filterByOwner",
            dataType: "html",
            type: "POST",
            data: 'Owner=' + owner,
            context: this,
            success: function (result) {
                location.reload();
            }
        });
    });

    function collaborators_status(obj) {
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink + "collaboratorsStatus",
            dataType: "html",
            type: "POST",
            data: 'Code=' + obj.value,
            context: this,
            success: function () {
                location.reload();
            }
        });
    }

    function changeLimit(obj) {
        var baselink = $("#baselink_report").val();
        $.ajax({
            url: baselink + "changeLimit",
            dataType: "html",
            type: "POST",
            data: 'limit=' + obj.value,
            context: this,
            success: function () {
//                console.log(e);
                location.reload();
            }
        });
    }

    $('.assign').click(function () {
        var baselink = $("#baselink_report").val();
        //$('.checkall').removeClass('hidden');
        $.ajax({
            url: baselink + "assign",
            dataType: "html",
            type: "POST",
            context: this,
            success: function () {
                location.reload();
            }
        });
    });
    $('.applyAssign').click(function () {
        var baselink = $("#baselink_report").val();
        var owner = $('.getOwner').val();
        var selected = [];
        $('#checkboxes input:checked').each(function () {
            selected.push($(this).val());
        });
        if (owner == -1) {
            alert('Chọn để Assign!!!');
            return false;
        }
        if (selected.length == 0) {
            alert('Vui lòng chọn sản phẩm!!!');
            return false;
        } else {
            $.ajax({
                url: baselink + "applyAssign",
                dataType: "html",
                type: "POST",
                data: 'selected=' + selected + '&owner=' + owner,
                context: this,
                success: function () {
                    alert('Bạn đã Assign thành công!');
                    location.reload();
                }
            });
        }
    });


    function toggle(source) {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i] != source)
                checkboxes[i].checked = source.checked;
        }
    }
</script>
